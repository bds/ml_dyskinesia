# Table of contents
* [Introduction](#introduction)
* [GitLab](#gitlab)
* [Analysis](#analysis)
* [Data](#data)
* [Results](#results)
* [Manuscript](#manuscript)
* [Folder Structure](#folder)

# Introduction <a name="introduction"></a>
This document provides information about the script for predicting levodopa-induced dyskinesia using data from the LUXPARK, PPMI, and ICEBERG cohorts. It includes details on the scripts' location, functionality, methodology, and key components, presenting a comprehensive overview for users to predict levodopa-induced dyskinesia outcomes effectively.

# GitLab <a name="gitlab"></a>
The script is available on the GitLab <a href="https://gitlab.lcsb.uni.lu/bds/ml_dyskinesia">https://gitlab.lcsb.uni.lu/bds/ml_dyskinesia</a>.

# Analysis <a name="analysis"></a>
The primary scrip for predicting levodopa-induced dyskinesia in Parkinson's disease, called

> "00_ML_Analysis.py"

executes the following assignments:

1. <b> Loading Packages and Functions:</b> <br>
  The script begins by loading the required Python packages and functions for the analysis.
  
2. <b>Importing Metadata:</b> <br>
  Metadata related to variable information is imported, giving essential particulars for subsequent analyses.

3. <b>Data Processing:</b> <br>
  The script <code>Analysis/01 - Data Processing.py"</code> is executed to process data from the LUXPARK, PPMI, and ICEBERG cohorts.

4. <b>Cohorts Information:</b> <br>
  A script named
  <code>Analysis/01 - Data Processing - Cohorts Information.py</code>
  provides specific cohort details, such as the sample size and outcome variables used.

5. <b>Handling Outcome Variables:</b> <br>
  Technical terms such as <i>NP4DYSKI2_Y4, NP4DYSKI2_bs_year</i>, and <i>NP4DYSKI2_status</i> are utilized for machine learning classification and survival analysis. <br>
  <i>NP4DYSKI2_Y4</i> is the variable that represents levodopa-induced dyskinesia within 4 years for machine learning classification. <br>
  <i>NP4DYSKI2_bs_year</i> and <i>NP4DYSKI2_status</i> represent time-to-dyskinesia and the censor status for survival machine learning. <br>
  <i>NP4DYSKI2_Y4</i> applies the last observation carry backward, where <i>NP4DYSKI2 = 0</i>. <i>NP4DYSKI2_Y4 = 1</i> if dyskinesia occurs within 4 years.

6. <b>Missing Values Imputation:</b> <br>
  <code>Analysis/01 - Data Processing - Split Data and Missing Values Imputation.py</code>
  script utilizes imputation for handling missing data.

7. <b>Cross-Cohort Normalization:</b> <br>
  After imputation, cross-cohort normalization is implemented in R using the script
  <code>00_Normalization_Data.R</code>.
  Processed data is stored in the <i>Analysis/Picked Data</i> folder.

8. <b>Machine Learning Classification:</b> <br>
  The script
  <code>Analysis/04 - Classification - NP4DYSKI2_Y4.py</code>
  performs machine learning classification on the pre-processed data. The trained models are saved in the <code>Analysis/Models/</code> folder.

9. <b>Survival Machine Learning:</b> <br>
  Survival machine learning is implemented in
  <code>Analysis/08 - Survival Analysis - NP4DYSKI2_bs_year.py</code>.
  Trained survival models are also saved in the <code>Analysis/Models/</code> folder.

The script provides a comprehensive pipeline for predicting levodopa-induced dyskinesia, incorporating both classification and survival analysis techniques.

# Data <a name="data"></a>
The dataset for the analysis is stored in the <code>Data</code> folder. This folder contains important files, specifically <code>Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx</code>, which provides detailed information crucial for the analysis. These metadata files offer information into the variables for the comprehensive evaluation of levodopa-induced dyskinesia prediction across the PPMI, LUXPARK, and ICEBERG cohorts.

# Results <a name="results"></a>
The <code>Results</code> folder encapsulates a comprehensive compilation of outcomes derived from the levodopa-induced dyskinesia prediction analysis.

# Manuscript <a name="manuscript"></a>
In the <code>Manuscript</code> folder, you will find the manuscript that has been prepared and submitted to <i>Neuroscience Letters</i>, offering a detailed exploration of the research findings on levodopa-induced dyskinesia in Parkinson's disease.

# Folder Structure <a name="folder"></a>

```
00_ML_Analysis.py
00_Normalization_Data.R
00_Descriptive_Statistics_Binary_Outcome.R

├── Analysis
│   └── General Functions
│   └── ML - ADABOOST
│   └── ML - C45
│   └── ML - CATBOOST
│   └── ML - FIGS
│   └── ML - GBOOST
│   └── ML - GOSDT
│   └── ML - GREEDY
│   └── ML - HS
│   └── ML - XGBOOST
│   └── Models
│       └── ALL
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│       └── ICEBERG
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│       └── LEAVE_ONE_OUT
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│       └── LEAVE_ONE_OUT2
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│       └── LUXPARK
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│       └── PPMI
│           └── NP4DYSKI2_bs_year
│           └── NP4DYSKI2_Y4
│   └── Picked Data
│       └── ALL
│           └── NP4DYSKI2_bs_year
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│           └── NP4DYSKI2_Y4
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│       └── ICEBERG
│           └── NP4DYSKI2_bs_year
│               └── NONE
│           └── NP4DYSKI2_Y4
│               └── NONE
│       └── LEAVE_ONE_OUT
│           └── NP4DYSKI2_bs_year
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│           └── NP4DYSKI2_Y4
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│       └── LEAVE_ONE_OUT2
│           └── NP4DYSKI2_bs_year
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│           └── NP4DYSKI2_Y4
│               └── COMBAT
│               └── MCOMBAT
│               └── MEAN
│               └── NONE
│               └── QUANTILE
│               └── RATIOA
│               └── STANDARDIZE
│       └── LUXPARK
│           └── NP4DYSKI2_bs_year
│               └── NONE
│           └── NP4DYSKI2_Y4
│               └── NONE
│       └── PPMI
│           └── NP4DYSKI2_bs_year
│               └── NONE
│           └── NP4DYSKI2_Y4
│               └── NONE
│   └── Prediction
│       └── NP4DYSKI2_bs_year
│       └── NP4DYSKI2_Y4
│   └── Survival
│       └── Survival - COXPH
│       └── Survival - CWGBOOST
│       └── Survival - EXTRA
│       └── Survival - GBOOST
│       └── Survival - LSVM
│       └── Survival - NLSVM
│       └── Survival - RSF
│       └── Survival - TREE
```