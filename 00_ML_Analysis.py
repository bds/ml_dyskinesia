#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Import Required Libraries    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import numpy as np
import pandas as pd
import math
import json
import os
import time
import datetime
from datetime import datetime, timedelta
import missingno as msno
import statistics
import csv

import plotly
import plotly.offline as py
from plotly.graph_objs import *
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.figure_factory as ff

import seaborn as sns
import matplotlib as mpl
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

# Import the 3 dimensionality reduction methods
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

# Standardize the data
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures

# Import the packages for factor analysis
from sklearn.decomposition import FactorAnalysis
from factor_analyzer import FactorAnalyzer
from factor_analyzer.factor_analyzer import calculate_bartlett_sphericity
from factor_analyzer.factor_analyzer import calculate_kmo

# Correlation
import scipy as sp
from scipy.stats import spearmanr
from scipy.stats import kendalltau
from scipy.stats import pointbiserialr
from scipy.stats import rankdata
from scipy.stats import kruskal
from scipy.stats import chi2
from scipy.stats import fisher_exact
from scipy.stats import norm

import scikit_posthocs as sp

from sklearn.metrics import r2_score, f1_score
from sklearn.metrics import confusion_matrix, accuracy_score, roc_auc_score, roc_curve, auc
from sklearn.metrics import precision_recall_curve, precision_score, recall_score #, plot_precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import mean_squared_error, explained_variance_score, max_error, mean_absolute_error
from sklearn.metrics import mean_squared_log_error, median_absolute_error, r2_score
from sklearn.metrics import mean_squared_error, cohen_kappa_score, hamming_loss, jaccard_score, hinge_loss, log_loss, brier_score_loss #, plot_confusion_matrix
from sklearn.metrics import PrecisionRecallDisplay

from statistics import mean

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, RepeatedStratifiedKFold
from sklearn.model_selection import KFold, cross_val_predict, cross_val_score, RepeatedKFold, StratifiedKFold

from sklearn.linear_model import Lasso, Ridge, LassoCV, LassoLarsIC, ElasticNet, LinearRegression, LogisticRegression, BayesianRidge
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier

from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier

from sklearn.utils import resample
import random

import warnings
from sklearn.exceptions import ConvergenceWarning

from skglm import Lasso as Lasso_skglm
from skglm import ElasticNet as ElasticNet_skglm
from skglm import MCPRegression as MCPRegression_skglm
from sklearn.kernel_ridge import KernelRidge

from imodels import FIGSClassifier, FIGSRegressor
from imodels import HSTreeClassifier, HSTreeRegressor
from imodels import OptimalTreeClassifier  # (GOSDT)
from imodels import GreedyTreeClassifier, GreedyTreeRegressor
# from imodels import TaoTreeClassifier, TaoTreeRegressor
from imodels import C45TreeClassifier
# from gosdt import GOSDT


# from tpot import TPOTClassifier, TPOTRegressor

from joblib import Parallel, delayed

import pickle
import itertools

from sklearn.inspection import permutation_importance
from sklearn.inspection import partial_dependence #, plot_partial_dependence

from sklearn.ensemble import AdaBoostClassifier, AdaBoostRegressor
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor

import shap
# from BorutaShap import BorutaShap
from xgboost import XGBClassifier, XGBRegressor
from catboost import CatBoostClassifier, CatBoostRegressor

import eli5
from eli5.sklearn import PermutationImportance

from pySankey.sankey import sankey

from sklearn.feature_selection import RFECV, RFE
from sklearn.feature_selection import SequentialFeatureSelector
from sklearn.feature_selection import SelectFromModel

from lifelines import KaplanMeierFitter
from lifelines.statistics import multivariate_logrank_test
from matplotlib.offsetbox import AnchoredText
from lifelines.statistics import logrank_test
from lifelines.utils import median_survival_times

# import bootstrapped.bootstrap as bs
# import bootstrapped.stats_functions as bs_stats

from collections import Counter
from scipy.stats import rankdata

from statsmodels.stats.multitest import multipletests

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Initial Settings    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

warnings.filterwarnings(action='ignore', category=DeprecationWarning, message='`np.bool` is a deprecated alias')
warnings.filterwarnings(action='ignore', category=DeprecationWarning, message='`np.int` is a deprecated alias')
warnings.filterwarnings('ignore', category = ConvergenceWarning)

# Repository for the models, charts and tables
file_repo = "Analysis/Plots/"
file_repo_best = "Analysis/Plots/Best_Models/"
file_repo_match = "Analysis/Plots/Matching/"
file_pickle = "Analysis/Models/"
file_data = "Analysis/Pickle Data/"

# Avoid to set > 200
n_jobs = 100


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Loading Functions    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - Save Dataset into Pickle.py").read() )
exec( open("Analysis/Functions - Graphing.py").read() )
exec( open("Analysis/Functions - Random Undersampling.py").read() )

exec( open("Analysis/Functions - Data Spliting and Missing Values Imputation.py").read() )

# Classification
exec( open("Analysis/Functions - Machine Learning Classification.py").read() )
exec( open("Analysis/Functions - Machine Learning Regression.py").read() )
exec( open("Analysis/Functions - Stepwise Sequential Feature Selection.py").read() )
exec( open("Analysis/Functions - DeLong test.py").read() )

# Survival Analysis
exec( open("Analysis/Functions - Machine Learning Survival Analysis.py").read() )
exec( open("Analysis/Functions - Stepwise Sequential Feature Selection (Survival).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import Cross-cohort Data Dictionary    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_variables = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                              ,sheet_name = "Variables")

data_variables_luxpark = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                                        ,sheet_name = "Variables_LUXPARK")

data_variables_ppmi = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                                    ,sheet_name = "Variables_PPMI")

data_variables_iceberg = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                                        ,sheet_name = "Variables_ICEBERG")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import List of Data Renaming    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_rename = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                            ,sheet_name = "Rename")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import Data Variable Info    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_info = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                         ,sheet_name = "Info")

data_info_luxpark = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                         ,sheet_name = "Info_LUXPARK")

data_info_ppmi = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                         ,sheet_name = "Info_PPMI")

data_info_iceberg = pd.read_excel("Analysis/Data/Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx"
                         ,sheet_name = "Info_ICEBERG")

data_info_all_set = log_ratio_labels(data = data_info, var = 'Variable', label = 'Description')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Selected Variables for the Analysis    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Baseline variables (Common variables) [Dyskinesia]
vars_baseline_dyski = data_info[ data_info['Dyskinesia_type2'] == 'yes' ]['Variable'].tolist()
vars_baseline_dyski_excl = [x for x in vars_baseline_dyski if x not in ['NP4DYSKI2', 'levodopa']]  # To exclude baseline dyskinesia and levodopa medication

# Baseline variables of LUXPARK (All variables)
vars_baseline_luxpark = data_info_luxpark[ data_info_luxpark['Type'] == 'Baseline' ]['Variable'].tolist()

# Baseline variables of PPMI (All variables)
vars_baseline_ppmi = data_info_ppmi[ data_info_ppmi['Type'] == 'Baseline' ]['Variable'].tolist()

# Baseline variables of ICEBERG (All variables)
vars_baseline_iceberg = data_info_iceberg[ data_info_iceberg['Type'] == 'Baseline' ]['Variable'].tolist()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Selected Subset of Features for the Analysis    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/01 - Data Processing - Selected Subset Features.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data Processing - PPMI, LuxPARK, ICEBERG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/01 - Data Processing.py").read() )

data_cohort_ppmi = load_object(filename = "data_cohort_ppmi", file_repo = file_data)
data_cohort_ppmi_add = load_object(filename = "data_cohort_ppmi_add", file_repo = file_data)

data_cohort_luxpark = load_object(filename = "data_cohort_luxpark", file_repo = file_data)
data_cohort_luxpark_add = load_object(filename = "data_cohort_luxpark_add", file_repo = file_data)

data_cohort_iceberg = load_object(filename = "data_cohort_iceberg", file_repo = file_data)
data_cohort_iceberg_add = load_object(filename = "data_cohort_iceberg_add", file_repo = file_data)

data_cohort_comb = load_object(filename = "data_cohort_comb", file_repo = file_data)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data Processing - Cohorts Information    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/01 - Data Processing - Cohorts Information.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data Processing - Split Data + Missing Values Imputation    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/01 - Data Processing - Split Data and Missing Values Imputation.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data Processing - Imputed Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# NP4DYSKI2_Y4
data_dyski_imputed_luxpark = load_object(filename = "data_cohort_luxpark_imputed", file_repo = file_data+'LUXPARK/NP4DYSKI2_Y4/')
data_dyski_imputed_ppmi = load_object(filename = "data_cohort_ppmi_imputed", file_repo = file_data+'PPMI/NP4DYSKI2_Y4/')
data_dyski_imputed_iceberg = load_object(filename = "data_cohort_iceberg_imputed", file_repo = file_data+'ICEBERG/NP4DYSKI2_Y4/')
data_dyski_imputed_luxpark_ppmi = load_object(filename = "data_cohort_luxpark_ppmi_imputed", file_repo = file_data+'LEAVE_ONE_OUT/NP4DYSKI2_Y4/')
data_dyski_imputed_luxpark_iceberg = load_object(filename = "data_cohort_luxpark_iceberg_imputed", file_repo = file_data+'LEAVE_ONE_OUT2/NP4DYSKI2_Y4/')
data_dyski_imputed_comb = load_object(filename = "data_cohort_comb_imputed", file_repo = file_data+'ALL/NP4DYSKI2_Y4/')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data Processing - Normalization    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Run "00_Normalization_Data.R"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Machine Learning Classification & Survival Analysis - Dyskinesias [NP4DYSKI2_Y4, NP4DYSKI2_bs_year]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/04 - Classification - NP4DYSKI2_Y4.py").read() )
exec( open("Analysis/08 - Survival Analysis - NP4DYSKI2_bs_year.py").read() )
exec( open("Analysis/08 - Combining features from NP4DYSKI2_Y4 and NP4DYSKI2_bs_year.py").read() )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Correlation Analysis    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Correlation analysis on non-impute cohorts
exec( open("Analysis/07 - Correlation Analysis - NP4DYSKI2_Y4.py").read() )

# ANOVA and Kruskal-Wallis test for significance test of the continuous/ordinal variables across cohorts
exec( open("Analysis/07 - ANOVA and Kruskal-Wallis Test Across Cohorts - NP4DYSKI2_Y4.py").read() )
