# Table of contents
* [Introduction](#introduction)
* [Content](#content)
* [Data](#data)
* [Requirements](#requirements)
* [License](#license)

# Introduction <a name="introduction"></a>
This repository contains the code for statistical and machine learning (classification and survival) analyses described in the article "Levodopa-induced dyskinesia in Parkinson's disease: insights from cross-cohort prognostic analysis using machine learning".

This project conducted levodopa-induced dyskinesia analysis in Parkinson's disease to investigate the potential risk factors and to build predictive models based on three large-scale, longitudinal Parkinson's disease cohorts.

# Content <a name="content"></a>
The code covers the following main analysis steps:

1. Loading, preparation, processing, imputating missing values and normalizing of datasets
2. Machine learning classification to classify the likelihood of developing levodopa-induced dyskinesia symptom within 4-year
3. Time-to-event analysis to predict the time duration of levodopa-induced dyskinesia occurrence
4. Univariate significance tests between predictive features to the dyskinesia outcome

# Data <a name="data"></a>
The two datasets, LUXPARK and ICEBERG for this manuscript 
 are linked to the Luxembourg Parkinson’s Study and the Progression of Parkinson’s Disease Study in Paris, France, respectively, and their internal rules. Requests for access can be made to request.ncer-pd@uni.lu for LUXPARK and jean-christophe.corvol@aphp.fr or marie.vidailhet@aphp.fr for ICEBERG. The PPMI data are available via the PPMI website https://www.ppmi-info.org/.

# Requirements <a name="requirements"></a>
The code was written in Python and R. The packages are listed at the beginning of the corresponding Python and R scripts.

Find the code to initiate machine learning classification and survival machine learning to investigate the potential risk factors for levodopa-induced dyskinesia in Parkinson’s disease.

> "00_ML_Analysis.py"

Aim: To process the raw PD cohorts, implementing the missing values imputation, and machine learning prediction pipeline.

> "00_Normalization_Data.R"

Aim: To process the cohorts by implementing normalization to the continuous variables.

> "00_Descriptive_Statistics_Binary_Outcome.R"

Aim: To generate descriptive statistics tables with univariate significance tests between the predictive features to the outcome across the cohorts.

# License <a name="license"></a>
The code is available under the MIT License.
