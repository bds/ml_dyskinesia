#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Reset the Environment and Console    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

rm(list = ls())
cat("\014")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Install and Loading Packages    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# To install 'BiocManager' if it is yet installed
if( length(installed.packages()[,"Package"][installed.packages()[,"Package"] %in% c("BiocManager")]) == 0 ){
  install.packages("BiocManager", repos = "http://cran.us.r-project.org")
}

# To load the bioconductor packages
required.BiocPackages <- c( "sva"             # Surrogate variable analysis
                           ,"bapred"          # Batch effect removal and Addon normalization
                           ,"dendextend"      # Extending 'dendrogram' functionality in R
                           ,"affy"            # Methods for Affymetrix Oligonucleotide Arrays
                           ,"affyPLM"         # Methods for fitting probe-level models
                           ,"genefilter"      # Methods for filtering genes from high-throughput experiments
                           ,"BiocParallel"    # Bioconductor facilities for parallel evaluation
                           )

# To install the bioconductor packages if it is yet installed
lapply(required.BiocPackages, function(x){
  if( length(installed.packages()[,"Package"][installed.packages()[,"Package"] %in% x]) == 0 ){
    BiocManager::install(x)
  }
})

# To load the bioconductor packages
lapply(required.BiocPackages, require, character.only = TRUE)

# Function to install (if needed) and load the packages
installAndLoadPackages <- function(listOfRequiredPackages){
  new.packages <- listOfRequiredPackages[!(listOfRequiredPackages %in% installed.packages()[,"Package"])]
  if(length(new.packages)) install.packages(new.packages, dep=TRUE, repos = "http://cran.us.r-project.org")
  lapply(listOfRequiredPackages, require, character.only = TRUE)
}

# To download package from CRAN archive
if( !"gPCA" %in% installed.packages()[,"Package"] ){
  url <- "https://cran.r-project.org/src/contrib/Archive/gPCA/gPCA_1.0.tar.gz"
  pkgFile <- "gPCA_1.0.tar.gz"
  download.file(url = url, destfile = pkgFile)
  install.packages(pkgs = pkgFile, type = "source", repos = NULL)
  unlink(pkgFile)
}


# Tp download package from GitHub
if(!"devtools" %in% installed.packages()[,"Package"]){
  install.packages("devtools")
}
if(!"exploBATCH" %in% installed.packages()[,"Package"]){
  devtools::install_github("syspremed/exploBATCH")
}

# Packages to be loaded
required.packages = c(  "tidyverse"       # Includes packages ggplot2, dplyr, tidyr, readr
                                          # ,purrr, tibble, stringr, forcats
                       ,"reticulate"      # Interface to Python
                       ,"splitstackshape" # Stack and reshape datasets after splitting
                       ,"data.table"      # data.table
                       ,"gPCA"            # Batch effect detection via guided PCA
                       ,"exploBATCH"      # Discovering and correcting for batch effect using an approach in Nyamundanda et al (2017)
                       ,"parallel"        # Support for Parallel computation in R
                       )

# Load the required package for this script
installAndLoadPackages(listOfRequiredPackages = required.packages)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Define the Repository    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

repo_data_in <- "Analysis/Pickle Data/"
repo_doc <- "Analysis/Data/"


# HPC
# repo_data_in <- "/home/users/rloo/PhD_Analysis/Analysis/Pickle Data/ALL/"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Normalization and Addon Normalization    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

source("Analysis/Functions - Normalization and Addon Normalization.R")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Normalized the Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

source("Analysis/Functions - Data Normalization.R")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Log-Ratio    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

source("Analysis/Functions - Log-Ratio.R")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Define the outcome variable and type of analysis [To be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_outcome <- c( "NP4DYSKI2_Y4", "NP4DYSKI2_bs_year" )

ana_type <- c("ALL", "LEAVE_ONE_OUT", "LEAVE_ONE_OUT2")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Define type of looping [To be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ana_repeat <- ""
# ana_repeat <- "R100_"

if( ana_repeat == "" ){
  set_B_loop = 1
}else if( ana_repeat == "R100_" ){
  set_B_loop = 100
}else{
  set_B_loop = 1
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Continuous Variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_variable <- readxl::read_excel(paste0(repo_doc, "Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx")
                                    ,sheet = "Variables")
data_info <- readxl::read_excel(paste0(repo_doc, "Cross-cohort of PPMI and LuxPARK and ICEBERG.xlsx")
                                ,sheet = "Info")

vars_continuous <- data_variable %>%
  filter(`Variable Type` == 'Continuous') %>%
  select_at('PPMI Variable') %>%
  pull() %>%
  .[. %in% pull(data_info[data_info['Type']=='Baseline','Variable'])] %>%
  .[!. %in% vars_outcome]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Define the normalization methods    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

norm_methods <- c( "quantile"     # Quantile normalization
                  ,"mean"         # Mean-centering
                  ,"ratioa"       # Ratio-A
                  ,"standardize"  # Standardization
                  ,"combat"       # ComBat
                  ,"mcombat"      # M-ComBat
                  )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To perform the normalization in parallel    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for(i in 1:length(norm_methods)){
  
  for(outLoop in 1:length(vars_outcome)){
    
    for(anaLoop in 1:length(ana_type)){
      
      normalize_data( var_patno = 'PATNO'
                      ,var_outcome = vars_outcome[outLoop]
                      ,method = norm_methods[i]
                      ,run_addon = TRUE
                      ,B_loop = set_B_loop
                      ,k_loop = 5
                      ,m_loop = 3
                      ,ds_prefix = ana_repeat
                      ,vars_continuous = vars_continuous
                      ,repo_data_in = paste0("Analysis/Pickle Data/"
                                              ,ana_type[anaLoop],"/"
                                              ,toupper(vars_outcome[outLoop])
                                              ,"/NONE/")
                      ,repo_data_out = paste0("Analysis/Pickle Data/"
                                              ,ana_type[anaLoop],"/"
                                              ,toupper(vars_outcome[outLoop]), "/"
                                              ,toupper(norm_methods[i]), "/" )
      )
    }
  }
}

