# Table of contents
* [R packages](#rpackages)
* [Python packages](#pythonpackages)

# R packages <a name="rpackages"></a>
The R packages in R version 4.2.1 are listed below: <br>
- affy v1.74.0 <br>
- affyPLM v1.72.0 <br>
- bapred v1.1 <br>
- BiocParallel v1.30.4 <br>
- data.table v1.14.6 <br>
- dendextend v1.16.0 <br>
- exploBATCH v1.0 <br>
- genefilter v1.78.0 <br>
- gPCA v1.0 <br>
- parallel v4.2.1 <br>
- reticulate v1.26 <br>
- splitstackshape v1.4.8 <br>
- sva v3.44.0 <br>
- tidyverse v1.3.2 <br>

# Python packages <a name="pythonpackages"></a>
The Python packages available on High Performance Computing (HPC) with Python/3.8.6-GCCcore-10.2.0 are listed below: <br>
- catboost v1.1 <br>
- eli5 v0.13.0 <br>
- imodels v1.2.6 <br>
- joblib v1.2.0 <br>
- lifelines v0.27.4 <br>
- matplotlib v3.5.1 <br>
- missingno v0.5.1 <br>
- numpy v1.21.6 <br>
- pandas v1.4.2 <br>
- plotly v5.7.0 <br>
- pySankey v0.0.1 <br>
- scikit-survival v0.20.0 <br>
- scipy v1.8.0 <br>
- seaborn v0.11.2 <br>
- shap v0.41.0 <br>
- skglm v0.1 <br>
- sklearn v0.0 <br>
- xgboost v1.6.2
