#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    ANOVA/Kruskal–Wallis for comparing continuous variables across the cohort    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from scipy.stats import f_oneway
from statsmodels.stats.multicomp import pairwise_tukeyhsd
from scikit_posthocs import posthoc_dunn
from scipy.stats import shapiro

class compare_vars_cohorts:

    def __init__(self, data_luxpark, data_ppmi, data_iceberg, outcome, repo_output, alpha = 0.05) :

        self.data_luxpark = data_luxpark
        self.data_ppmi = data_ppmi
        self.data_iceberg = data_iceberg
        self.outcome = outcome
        self.repo_output = repo_output
        self.alpha = alpha
    
    def measure(self) :

        luxpark_data = self.data_luxpark
        ppmi_data = self.data_ppmi
        iceberg_data = self.data_iceberg

        # Variables of the cohorts to be compared
        vars_columns = luxpark_data.columns

        # Empty data frame
        df_results_comb = pd.DataFrame()

        for vars_loop in vars_columns :
            # Values of the cohort
            values_luxpark = luxpark_data.dropna(subset = [vars_loop])[vars_loop]
            values_luxpark = pd.to_numeric(values_luxpark, errors='coerce')

            values_ppmi = ppmi_data.dropna(subset = [vars_loop])[vars_loop]
            values_ppmi = pd.to_numeric(values_ppmi, errors='coerce')

            values_iceberg = iceberg_data.dropna(subset = [vars_loop])[vars_loop]
            values_iceberg = pd.to_numeric(values_iceberg, errors='coerce')
            
            # Unique values of the column
            num_unique_luxpark = len(np.unique(values_luxpark))
            num_unique_ppmi = len(np.unique(values_ppmi))
            num_unique_iceberg = len(np.unique(values_iceberg))

            if (num_unique_luxpark > 3) & (num_unique_ppmi > 3) & (num_unique_iceberg > 3) :
                # Normality test of the variable in 3 cohorts
                _, norm_luxpark = shapiro(values_luxpark)
                _, norm_ppmi = shapiro(values_ppmi)
                _, norm_iceberg = shapiro(values_iceberg)

                if (norm_luxpark < self.alpha) | (norm_ppmi < self.alpha) | (norm_iceberg < self.alpha) :
                    # Not met the normality assumption
                    # Perform Kruskal-Wallis test
                    kruskal_result = kruskal(values_luxpark, values_ppmi, values_iceberg)
                    kruskal_pval = kruskal_result.pvalue

                    # Perform pairwise Dunn's test with Bonferroni correction
                    dunn_result = posthoc_dunn([values_luxpark, values_ppmi, values_iceberg], p_adjust='bonferroni')

                    # Group_labels contains the labels for each group
                    group_labels = ['LuxPARK', 'PPMI', 'ICEBERG']

                    # Initialize lists to store results
                    results = []
                    # Convert dunn_result to a NumPy array (if it's not already)
                    dunn_result_array = np.array(dunn_result)

                    # Loop through the upper triangle of the matrix to avoid duplicate pairs
                    for i in range(len(group_labels)):
                        for j in range(i+1, len(group_labels)):
                            results.append((group_labels[i], group_labels[j], dunn_result_array[i, j]))

                    # Create a DataFrame with the results
                    dunn_df = pd.DataFrame(results, columns=['group1', 'group2', 'p-adj'])
                    dunn_df['p-adj'] = dunn_df['p-adj'].apply(lambda x: '{:.4e}'.format(x) if x < 0.001 else '{:.4f}'.format(x))
                    # Calculate the mean difference for each pair of groups
                    dunn_df['mean_diff (g1-g2)'] = [np.mean(values_luxpark) - np.mean(values_ppmi)
                                                    ,np.mean(values_luxpark) - np.mean(values_iceberg)
                                                    ,np.mean(values_ppmi) - np.mean(values_iceberg)]
                    dunn_df['order'] = list(range(1, len(group_labels)+1))

                    # Results table
                    df_results = pd.DataFrame({'Variable': [vars_loop]*len(group_labels)
                                               ,'ANOVA/Kruskal (p-value)': [kruskal_pval]*len(group_labels) })
                    df_results['ANOVA/Kruskal (p-value)'] = df_results['ANOVA/Kruskal (p-value)'].apply(lambda x: '{:.4e}'.format(x) if x < 0.001 else '{:.4f}'.format(x))
                    df_results = pd.concat([df_results, dunn_df], axis = 1)
                    df_results.sort_values(by = ['order'], ascending = True, inplace = True)
                    df_results.reset_index(drop = True, inplace = True)
                    df_results_comb = pd.concat([df_results_comb, df_results], ignore_index = True, axis = 0)
                
                else:
                    # Met the normality assumption
                    # Performing ANOVA
                    anova_result = f_oneway(values_luxpark, values_ppmi, values_iceberg)
                    anova_pval = anova_result.pvalue

                    # Perform pairwise test - Tukey's HSD test
                    all_data = np.concatenate([values_luxpark, values_ppmi, values_iceberg])
                    group_labels = ['LuxPARK']*len(values_luxpark) + ['PPMI']*len(values_ppmi) + ['ICEBERG']*len(values_iceberg)
                    tukey_result = pairwise_tukeyhsd( all_data, group_labels )

                    # Extract the data from the Tukey's HSD test
                    tukey_data = tukey_result._results_table.data
                    tukey_df = pd.DataFrame( data = tukey_data[1:], columns = tukey_data[0] )
                    tukey_df['p-adj'] = tukey_result.pvalues
                    tukey_df['p-adj'] = tukey_df['p-adj'].apply(lambda x: '{:.4e}'.format(x) if x < 0.001 else '{:.4f}'.format(x))

                    # Rearrange the group1 and group2
                    tukey_df.rename(columns = {'group1': 'group1_', 'group2': 'group2_'}, inplace = True)
                    tukey_df['order'] = 0
                    tukey_df['group1'] = tukey_df['group1_']
                    tukey_df['group2'] = tukey_df['group2_']
                    # LuxPARK vs. PPMI
                    tukey_df['group1'] = np.where( (tukey_df['group1_'] == 'PPMI') & (tukey_df['group2_'] == 'LuxPARK'), 'LuxPARK', tukey_df['group1'] )
                    tukey_df['group2'] = np.where( (tukey_df['group1_'] == 'PPMI') & (tukey_df['group2_'] == 'LuxPARK'), 'PPMI', tukey_df['group2'] )
                    tukey_df['order'] = np.where( ((tukey_df['group1_'] == 'PPMI') & (tukey_df['group2_'] == 'LuxPARK')) |
                                                    (tukey_df['group1_'] == 'LuxPARK') & (tukey_df['group2_'] == 'PPMI'), 1, tukey_df['order'] )
                    # LuxPARK vs. ICEBERG
                    tukey_df['group1'] = np.where( (tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'LuxPARK'), 'LuxPARK', tukey_df['group1'] )
                    tukey_df['group2'] = np.where( (tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'LuxPARK'), 'ICEBERG', tukey_df['group2'] )
                    tukey_df['order'] = np.where( ((tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'LuxPARK')) |
                                                    (tukey_df['group1_'] == 'LuxPARK') & (tukey_df['group2_'] == 'ICEBERG'), 2, tukey_df['order'] )
                    # PPMI vs. ICEBERG
                    tukey_df['group1'] = np.where( (tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'PPMI'), 'PPMI', tukey_df['group1'] )
                    tukey_df['group2'] = np.where( (tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'PPMI'), 'ICEBERG', tukey_df['group2'] )
                    tukey_df['order'] = np.where( ((tukey_df['group1_'] == 'ICEBERG') & (tukey_df['group2_'] == 'PPMI')) |
                                                    (tukey_df['group1_'] == 'PPMI') & (tukey_df['group2_'] == 'ICEBERG'), 3, tukey_df['order'] )
                    
                    # Calculate the mean difference for each pair of groups
                    tukey_df['mean_diff (g1-g2)'] = [np.mean(values_luxpark) - np.mean(values_ppmi)
                                                     ,np.mean(values_luxpark) - np.mean(values_iceberg)
                                                     ,np.mean(values_ppmi) - np.mean(values_iceberg)]
                    
                    # Results table
                    tukey_df = tukey_df[['group1', 'group2', 'p-adj', 'mean_diff', 'order']]
                    df_results = pd.DataFrame({'Variable': [vars_loop]*3
                                               ,'ANOVA/Kruskal (p-value)': [anova_pval]*3 })
                    df_results['ANOVA/Kruskal (p-value)'] = df_results['ANOVA/Kruskal (p-value)'].apply(lambda x: '{:.4e}'.format(x) if x < 0.001 else '{:.4f}'.format(x))
                    df_results = pd.concat([df_results, tukey_df], axis = 1)
                    df_results.sort_values(by = ['order'], ascending = True, inplace = True)
                    df_results.reset_index(drop = True, inplace = True)
                    df_results_comb = pd.concat([df_results_comb, df_results], ignore_index = True, axis = 0)
            else:
                pass
        
        # Save the output into HTML table
        df_results_comb.to_csv(self.repo_output+'ANOVA_Kruskal_Wallis_Across_Cohorts_'+self.outcome+'.csv')
        df_results_comb.to_html(self.repo_output+'ANOVA_Kruskal_Wallis_Across_Cohorts_'+self.outcome+'.html')
