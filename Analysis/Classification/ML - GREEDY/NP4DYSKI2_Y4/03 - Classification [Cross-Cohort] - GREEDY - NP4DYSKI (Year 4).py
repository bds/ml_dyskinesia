#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                            GREEDY - Dyskinesias (Year 4) - Multiple Cohorts                          ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Multiple cohorts) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_Y4'
list_model_prefix = ['greedy_dyski_y4'] * 3 + ['greedy_dyski_y4_excl'] * 3
file_repo_prefix_loop = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/'] * 2
list_vars_ml_set = [vars_baseline_dyski] * 3 + [vars_baseline_dyski_excl] * 3

model_set = 'greedy'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Multiple cohorts) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/03 - Classification - Method Settings (Multiple Cohorts).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Multiple cohorts)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set, model_prefix in zip(file_repo_prefix_loop, list_vars_ml_set, list_model_prefix) :
    file_repo_prefix = loop_file_repo
    print('Outcome (Multiple Cohorts): '+ set_outcome + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/03 - Classification - Machine Learning Prediction.py").read() )

    exec( open("Analysis/03 - Classification - Machine Learning Prediction - Part 2.py").read() )

