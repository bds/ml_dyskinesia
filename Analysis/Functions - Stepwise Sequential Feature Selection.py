def processSubset(X, y, X2, y2, feature_set, estimator) :

    ml_model = estimator
    ml_model.fit(X[feature_set], y) #, feature_names = feature_set)
    ml_preds = ml_model.predict(X2[feature_set])
    auc = roc_auc_score(y2, ml_preds, average = 'weighted')
    return({'model': ml_model, 'predictor': feature_set, 'AUC': auc})


def features_order(X, y, X2, y2, estimator) :

    features = []
    auc = []
    for p in X.columns :
        results = processSubset(X = X, y = y, X2 = X2, y2 = y2, feature_set = [p], estimator = estimator)
        features.append(results['predictor'][0])
        auc.append(results['AUC'])
    
    df_features = pd.DataFrame({'features': features, 'auc': auc})
    df_features.sort_values(by = 'auc', ascending = False, inplace = True)
    df_features.reset_index(drop = True, inplace = True)

    return({ 'features': df_features['features'].tolist(), 'auc': df_features['auc'].tolist() })


def forward(X, y, X2, y2, predictors, best_predictors, estimator) :

    remaining_predictors = [p for p in X.columns if p not in predictors]
    if len(remaining_predictors) > 0 :
        results = []
        for p in remaining_predictors :
            results.append( processSubset(X = X, y = y, X2 = X2, y2 = y2, feature_set = best_predictors + [p], estimator = estimator) )
        models = pd.DataFrame(results)

        best_model = models.loc[models['AUC'].argmax()]
    else :
        best_model = {'model': estimator, 'predictor': best_predictors, 'AUC': 0}
        predictors = best_predictors

    return({ 'best_model': best_model, 'predictors': predictors })



def backward(X, y, X2, y2, best_predictors, estimator):
    
    results = []
    for combo in itertools.combinations(best_predictors, len(best_predictors) - 1) :
        results.append( processSubset(X = X, y = y, X2 = X2, y2 = y2, feature_set = best_predictors, estimator = estimator) )
    models = pd.DataFrame(results)
   
    best_model = models.loc[models['AUC'].argmax()]
    return( best_model )



def stepwise_model(ds_X_train, ds_y_train, ds_X_test, ds_y_test, estimator, max_iter ) :

    # Variables with > 1 unique level
    vars_keep = [x for x in ds_X_train.columns if len(np.unique(ds_X_train[x])) > 1 ]
    ds_X_train = ds_X_train[vars_keep]
    ds_X_test = ds_X_test[vars_keep]

    Stepmodels = pd.DataFrame(columns = ['AUC', 'predictor', 'model'])

    fo = features_order(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, estimator = estimator)
    predictors_order = fo['features']

    ds_X_train = ds_X_train[predictors_order]
    ds_X_test = ds_X_test[predictors_order]

    predictors = [predictors_order[0]]
    Smodel_before = fo['auc'][0]
    best_predictors = [predictors_order[0]]

    cnt = 0
    max_iter_cnt = 0
    while (cnt < 1) & (max_iter_cnt <= max_iter) & (max_iter_cnt <= ds_X_train.shape[1]) :
        max_iter_cnt += 1
        Forward_result = forward(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, predictors = predictors, best_predictors = best_predictors, estimator = estimator)
        # print('forward')
        # Stepmodels.loc[icol] = Forward_result['best_model']
        Stepmodels = Forward_result['best_model']
        predictors = Forward_result['best_model']["predictor"]

        # Check if there is anything to remove
        Backward_result = backward(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, best_predictors = best_predictors, estimator = estimator)

        if Backward_result['AUC'] > Forward_result['best_model']['AUC'] :
            Stepmodels = Backward_result
            best_predictors = Backward_result['predictor']
            Smodel_before = Stepmodels['AUC']
            # print('backward')
        
        # if Stepmodels['AUC'] == Smodel_before :
        #     cnt = 1
        
        if Stepmodels['AUC'] > Smodel_before :
            Smodel_before = Stepmodels['AUC']
            best_predictors = Stepmodels['predictor']
        else:
            cnt = 1
    
    return( {'AUC': Smodel_before, 'predictors': best_predictors } )


def get_features_cv(ds_X, ds_y, estimator, repeat, seed_number, max_iter, cv) :

    selected_features = []
    selected_features_auc = []

    for B_loop in range(repeat) :
        if cv == 1 :
            kfold_split = StratifiedShuffleSplit(n_splits = cv, random_state = seed_number + 100*B_loop, test_size = 0.33)
        else :
            kfold_split = StratifiedShuffleSplit(n_splits = cv, random_state = seed_number + 100*B_loop, test_size = 1/cv)
        
        def func_features_cv_k( k, index ):
            train_index, test_index = index
            X_k_train, X_k_test = ds_X.iloc[train_index].reset_index(drop = True) , ds_X.iloc[test_index].reset_index(drop = True)
            y_k_train, y_k_test = ds_y[train_index].reset_index(drop = True) , ds_y[test_index].reset_index(drop = True)

            model_predictors = stepwise_model(ds_X_train = X_k_train, ds_y_train = y_k_train, ds_X_test = X_k_test, ds_y_test = y_k_test, estimator = estimator, max_iter = max_iter )
            best_predictors = model_predictors['predictors']
            auc_predictors = model_predictors['AUC']

            return({'best_predictors': best_predictors, 'auc_predictors': auc_predictors})
        
        features_cv_k = Parallel(n_jobs = 1)(
            delayed( func_features_cv_k )(k = k, index = (train_index, test_index) )
            for k, (train_index, test_index) in enumerate(kfold_split.split(ds_X, ds_y)) )
        

        for cv_k in range(len(features_cv_k)):
            selected_features.append( features_cv_k[0]['best_predictors'] )
            selected_features_auc.append( features_cv_k[0]['auc_predictors'] )


    
    mean_auc = np.percentile(selected_features_auc, 75)
    idx_auc = [x for x in range(len(selected_features_auc)) if selected_features_auc[x] >= mean_auc]
    ls_stepwise_features = [selected_features[x] for x in idx_auc]
    stepwise_features = []
    for sfts in ls_stepwise_features :
        stepwise_features += sfts
    stepwise_features = np.unique(stepwise_features).tolist()

    return(stepwise_features)


def stepwise_cv(ds_X, ds_y, estimator = FIGSClassifier(max_rules = 5), cv = 1, repeat = 1, seed_number = 890701, max_iter = 100 ) :

    stepwise_features = get_features_cv(ds_X, ds_y, estimator, repeat, seed_number, max_iter, cv)

    return(stepwise_features)
