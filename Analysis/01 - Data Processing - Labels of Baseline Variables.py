#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                            Update the label of the baseline variables                                                            ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Define the labels of dyskinesia in other outcomes
if (outcome_define != 'NP4DYSKI2_Y4') & (outcome_define != 'NP4DYSKI2_bs_year') :
    data_info_all = data_info_all_set.copy()
    data_info_all['Description'] = np.where( data_info_all['Variable'] == 'NP4DYSKI2', 'Dyskinesia', data_info_all['Description'] )

else :
    data_info_all = data_info_all_set.copy()
