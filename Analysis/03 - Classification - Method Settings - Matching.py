#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (to be updated)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_mtd_normalized = []
ls_mtd_undersamping = []
ls_mtd_matching = []
ls_mtd_feat_selection = []
ls_mtd_name = []

mtd_normalized = ['NONE']
mtd_undersamping = [False]
mtd_matching = [True]
mtd_feat_selection = ['', 'rfe', 'weight', 'boruta']

for method_normalized in mtd_normalized :
    for method_undersamping in mtd_undersamping :
        for method_matching in mtd_matching :
            for method_feat_selection in mtd_feat_selection :

                if(method_undersamping == False):
                    mtd_undersamp = ''

                elif(method_undersamping == True):
                    mtd_undersamp = '_undersampling'
                else:
                    pass

                if method_feat_selection == '' :
                    m_feat_selection = ''
                else :
                    m_feat_selection = '_' + method_feat_selection
                
                ls_mtd_normalized.append( method_normalized )
                ls_mtd_undersamping.append( method_undersamping )
                ls_mtd_matching.append( method_matching )
                ls_mtd_feat_selection.append( method_feat_selection )

                mtd_name = method_normalized.lower() + mtd_undersamp + m_feat_selection
                ls_mtd_name.append( mtd_name )
                # print('mtd_name: '+mtd_name)