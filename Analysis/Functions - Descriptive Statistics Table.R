#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####   Function - Demographics Characteristics Table   ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

demoByCohort <- function( in_ds,                            # Input dataset
                          vars_cohort,                      # Variable contained flag of the cohort
                          vars_cohort_val,                  # Value of the flag of variable 'vars_cohort'
                          vars_cohort_lbl,                  # Labels of variable 'vars_cohort'
                          vars,                             # Variable of characteristics to be shown in the output
                                                            # To calculate multiple variable into one, please use | and stated the condition,
                                                            # e.g. 'Heart failure etiology|Ischemic in (1)| `Non-ischemic` in (1)', the first one just the label
                          vars_lbl = NA,                    # The labels of 'vars' to be shown in the table
                          
                          overall_ttl_show = "yes",         # To show overall total at the top of the table: "yes" or "no"
                          var_lbl_show = "yes",             # If there is only 1 variable in the table, whether to show the variable name: "yes" or "no"
                          
                          vars_ttl = NA,                    # Categorical variable of 'vars' to be shown the total in the table, e.g. c(var1, var2)
                          incl_statCol = "yes",             # To include the column 'Statistics' in the final table: "yes" or "no"
                          col1_lbl = "Characteristics",     # Label of 1st column of the table
                          
                          add_lbl = NA,                     # Additional labels in the table
                          add_lbl_pos = NA,                 # Position of the additional labels vs 'vars': c(1, 4)
                          
                          incl_stat_test = "no",            # Only applicable if length of vars_cohort is at least 2
                                                            # Whether to perform the descriptive test.
                                                            # If 'vars' is continuous variable, will use Kolmogorow-Smirnov test to check the normality
                                                            # If normally distributed, t-test (unequal variance) will be performed.
                                                            # Otherwise Mann-Whitney U test will be used
                                                            # If 'vars' is categorical variable, Chi-square will be used
                                                            # But, if the expected value is < 0.25
                          test_cohort_num = c(1, 2),        # Which 2 of the 'vars_cohort' to perform descriptive test with 'vars' ?
                                                            # Please stat the index number of 'vars_cohort'. By default, the first 2 'vars_cohort'
                          vars_test = NA,                   # The descriptive test of 'vars' with pattern '|'
                                                            # If vars is 'Heart failure etiology|Ischemic in (1)| `Non-ischemic` in (1)'
                                                            # and you would like to perform descriptive for these 2 variables too
                                                            # then vars_test = 'Heart failure etiology|Ischemic in (1)| `Non-ischemic` in (1)'
                                                            # or you could also include other variable as well
                          chisq_adj = TRUE,                 # If `incl_stat_test` = "yes" then Chi-squared test will be performed to the categorical variable
                                                            # chisq_adj = TRUE  : Pearson's Chi-squared test with Yate's continuity correction
                                                            # chisq_adj = FALSE : Pearson's Chi-squared test
                          alpha = 0.05,                     # The significance level. By default, alpha = 0.05
                          pval_digit = 3,                   # Number of decimal places of the p-value of the descriptive test (if incl_stat_test = "yes")
                          sep_count_pct = "no"              # To separate count and percentage into different columns
                          ){
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  ####   Total Number of patient   ####
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  total_tbl <- lapply( 1:length(vars_cohort), function(x){
    
    vars_cohort_val_tmp <- vars_cohort_val
    
    demo_tbl <- data.table( 'demo_crit' = 'Number of patients'
                           ,'stat' = 'N'
                           ,'ordern' = 0
                           ,'subordern' = 1
                           ,'cohort_' = nrow(in_ds[in_ds[[vars_cohort[x]]] == vars_cohort_val_tmp[x]]) 
                           )
    setnames(demo_tbl, "cohort_", paste0("cohort_", x))
    
    assign( paste0("total_tbl_", x), demo_tbl, envir = as.environment(1) )
  } )
  
  # To combine the result of each cohort #
  if(length(vars_cohort) == 1){
    total_tbl_comb <- total_tbl_1
  } else{
    
    for(x in 1:(length(vars_cohort)-1)){
      if(x == 1){
        tbl_left <- data.table(eval(parse(text = paste0("total_tbl_", x))))
      } else{
        tbl_left <- total_tbl_comb
      }
      
      vars_tbl_right <- c("subordern", paste0("cohort_",x+1))
      tbl_right <- data.table(eval(parse(text = paste0("total_tbl_", x+1)))[,..vars_tbl_right])
      
      total_tbl_comb <- tbl_left[tbl_right, on = 'subordern']
    }
    
  }
  
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  ####   Descriptive Statistics of each variable   ####
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  demo_table <- lapply( 1:length(vars), function(y){
    vars_tbl <- lapply( 1:length(vars_cohort), function(x){
      
      if( grepl("\\|", vars[y] ) == FALSE ){
        if( class(in_ds[[vars[y]]]) == "numeric" | class(in_ds[[vars[y]]]) == "list" ){
          # Descriptive statistics of numeric variable #
          in_ds_tmp <- in_ds
          in_ds_tmp[["tmp_var"]] <- in_ds_tmp[[vars_cohort[x]]]
          in_ds_tmp <- in_ds_tmp %>% filter( tmp_var == vars_cohort_val[x] )
          
          if( class(in_ds_tmp[[vars[y]]]) == 'list' ){
            in_ds_tmp[[vars[y]]] <- unlist(in_ds_tmp[[vars[y]]])
          }
          
          vars_cohort_val_tmp <- vars_cohort_val
          
          var_tbl <- data.table( 'demo_crit' = c(vars_lbl[y]
                                                 ,''
                                                 ,''
                                                 ,''
                                                 ,'')
                                ,'stat' = c('n (non-missing)'
                                            ,'Mean (SD)'
                                            ,'Median'
                                            ,'Q1 - Q3'
                                            ,'Min - Max')
                                ,'ordern' = c(y, y, y, y, y)
                                ,'subordern' = c(1, 2, 3, 4, 5)
                                ,'cohort_' = c( paste0(nrow(in_ds_tmp[!is.na(in_ds_tmp[[vars[y]]]),]), ' (', round(nrow(in_ds_tmp[!is.na(in_ds_tmp[[vars[y]]]),])/nrow(in_ds_tmp)*100, 1), '%)')
                                               ,paste0(round(mean(in_ds_tmp[[vars[y]]], na.rm = TRUE), 1), ' (', round(sd(in_ds_tmp[[vars[y]]], na.rm = TRUE), 2), ')')
                                               ,round(median(in_ds_tmp[[vars[y]]], na.rm = TRUE), 1)
                                               ,paste0(round(quantile(in_ds_tmp[[vars[y]]], na.rm = TRUE, 0.25), 1), ' - ', round(quantile(in_ds_tmp[[vars[y]]], na.rm = TRUE, 0.75), 1))
                                               ,paste0(round(min(in_ds_tmp[[vars[y]]], na.rm = TRUE), 1), ' - ', round(max(in_ds_tmp[[vars[y]]], na.rm = TRUE), 1)))
                                )
          setnames(var_tbl, "cohort_", paste0("cohort_", x))
          
        } else if( class(in_ds[[vars[y]]]) %in% c("character", "factor") ){
          # Frequency table of character variable #
          in_ds_tmp <- in_ds
          in_ds_tmp[["tmp_var"]] <- in_ds_tmp[[vars_cohort[x]]]
          in_ds_tmp <- in_ds_tmp %>% filter( tmp_var == vars_cohort_val[x] )
          
          vars_cohort_val_tmp <- vars_cohort_val
          
          var_tbl <- data.table( 'demo_crit' = c(vars_lbl[y], names(table(in_ds[[vars[y]]])))
                                ,'stat' = c('', rep('n (%)', length(unique(in_ds[!is.na(in_ds[[vars[y]]]),][[vars[y]]]))) )
                                ,'ordern' = rep(y, (length(unique(in_ds[!is.na(in_ds[[vars[y]]]),][[vars[y]]]))+1) )
                                ,'subordern' = seq(1, length(unique(in_ds[!is.na(in_ds[[vars[y]]]),][[vars[y]]]))+1)
                                )
          
          var_tbl_cnt <- data.table(table(in_ds_tmp[[vars[y]]]))
          setnames(var_tbl_cnt, "V1", "demo_crit")
          setnames(var_tbl_cnt, "N", paste0('cohort_', x))
          
          var_tbl <- merge(var_tbl, var_tbl_cnt, on = "demo_crit", all.x = TRUE )
          
          
          var_tbl[[paste0('cohort_', x)]] <- ifelse(!is.na(var_tbl[[paste0('cohort_', x)]])
                                                    ,paste0(var_tbl[[paste0('cohort_', x)]], ' (', round(var_tbl[[paste0('cohort_', x)]]/nrow(in_ds_tmp[!is.na(in_ds_tmp[[vars[y]]]), ])*100, 1), '%)' )
                                                    ,'0 (0%)')
          var_tbl[[paste0('cohort_', x)]] <- ifelse(var_tbl[['demo_crit']] == vars_lbl[y]
                                                    ,paste0(nrow(in_ds_tmp[!is.na(in_ds_tmp[[vars[y]]]), ]), ' (', round(nrow(in_ds_tmp[!is.na(in_ds_tmp[[vars[y]]]), ])/nrow(in_ds_tmp)*100, 1), '%)')
                                                    ,var_tbl[[paste0('cohort_', x)]])
        } 
      }
      assign( paste0("var_tbl_", x), var_tbl, envir = as.environment(1) )
      
    } )
    
    # To combine the result of each cohort #
    if(length(vars_cohort) == 1){
      vars_tbl_comb <- var_tbl_1 %>%
        select_at( c("demo_crit", "stat", "ordern", "subordern", "cohort_1") )
    } else{
      
      for(tbl_i in 1:length(vars_cohort)){
        if(tbl_i == 1){
          vars_tbl_comb <- var_tbl_1
        } else{
          vars_tbl_merge <- eval(parse(text=paste0("var_tbl_", tbl_i)))
          vars_merge <- names(vars_tbl_merge)[!names(vars_tbl_merge) %in% c('demo_crit', 'stat')]
          vars_tbl_merge <- vars_tbl_merge[,..vars_merge]
          
          vars_tbl_comb <- vars_tbl_comb[vars_tbl_merge, on = c('ordern', 'subordern')]
          
        }
      }
      
    }
    
    return(vars_tbl_comb)
    
  } ) %>% rbindlist() %>%
    arrange(ordern, subordern )
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  ####   Rename the column name   ####
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  setnames(demo_table, "demo_crit", col1_lbl)
  setnames(demo_table, "stat", "Statistics")
  for(x in 1:length(vars_cohort)){
    setnames(demo_table, paste0("cohort_", x), vars_cohort_lbl[x])
  }
  final_out <- demo_table[, !c("ordern", "subordern"), with=FALSE]
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  ####   Return final table   ####
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  return(final_out)
  
}