#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_surv_models_compact_comparison() :

    model_vars_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_vars_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_vars = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_vars[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            
            model_vars_load[loop_ml_prefix] = model_vars
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_vars_comb[loop_file_repo] = model_vars_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    
    return({'model_vars' : model_vars_comb
            ,'best_ml_model' : best_ml_model_comb })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected of the optimal compact model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def compact_surv_features_combining( repo_prefix
                                    ,fig_repo_out
                                    ,fig_prefix
                                    ,fig_features
                                    ,list_ml_models
                                    ):
     
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_best = fig_features
    df_features_info = pd.DataFrame({'Variable': ['avg_score', 'num_vars', 'algorithm']
                                     ,'Description': ['Average score in CV', 'Number of features', 'Algorithm'] })
    
    df_table_score = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })
    df_table_method = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })

    df_features_pct = fig_features.copy()
    df_features_rank = fig_features.copy()
    
    for repo_prefix_loop in repo_prefix :

        ls_optimal_model_name = []
        ls_optimal_model_nvars = []
        ls_optimal_avg_cIndex = []
        ls_optimal_sd_cIndex = []
        ls_ds_optimal_features = []
        ls_optimal_holdout = []

        # Optimal model within algorithms
        ls_features_mtd = []
        ls_number_mtd = []
        for list_ml_models_loop in list_ml_models :
            
            ls_model_vars = ds_perm_compact_compare['model_vars'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]
            ls_best_ml_model = ds_perm_compact_compare['best_ml_model'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]

            # The method of the optimal model (Within 1 SD of max average AUC and minimal features)
            optimal_model_name = ls_model_vars['model_name'][ ls_model_vars['idx_optimal'] ]
            optimal_model_nvars = ls_model_vars['num_vars'][ ls_model_vars['idx_optimal'] ]
            optimal_avg_cIndex = ls_model_vars['model_avg_cIndex'][ ls_model_vars['idx_optimal'] ]
            optimal_sd_cIndex = statistics.stdev(ls_best_ml_model.all_model_cIndex_cv[ ls_best_ml_model.all_model_cIndex_test.index( max(ls_best_ml_model.all_model_cIndex_test) ) ])
            optimal_holdout = ls_best_ml_model.best_model_cIndex_val

            #### List of features
            num_vars = ls_model_vars['num_vars'][ ls_model_vars['idx_optimal'] ]
            model_features = ls_model_vars['model_selected_features'][ ls_model_vars['idx_optimal'] ]

            # The features of the optimal model for each cohort
            ds_optimal_features = pd.DataFrame({'Variable': model_features, repo_prefix_loop: [1 for x in range(num_vars)] })

            # The selected features for each cohort
            for mtd_loop in range(len(ls_model_vars['model_name'])) :
                # List of selected features
                ls_features_mtd += np.unique([x.replace('_0','').replace('_1','').replace('_2','').replace('_3','').replace('_4','')\
                                              .replace('_5','').replace('_6','').replace('_7','').replace('_8','').replace('_9','')\
                                                for x in ls_model_vars['model_selected_features'][mtd_loop]]).tolist()
            cnt_vars = Counter(ls_features_mtd)
            ls_number_mtd += [len(ls_model_vars['model_name'])]

            # Append the list
            ls_optimal_model_name.append( optimal_model_name )
            ls_optimal_model_nvars.append( optimal_model_nvars )
            ls_optimal_avg_cIndex.append( optimal_avg_cIndex )
            ls_ds_optimal_features.append( ds_optimal_features )
            ls_optimal_sd_cIndex.append( optimal_sd_cIndex )
            ls_optimal_holdout.append( optimal_holdout )
        
        # The percentage of the features being selected from each method and algorithm
        ls_ds_features_pct = pd.merge(fig_features
                                        ,pd.DataFrame({'Variable': [x for x in cnt_vars.keys()]
                                                        ,'cnt': [x for x in cnt_vars.values()]
                                                        ,'ttl': [sum(ls_number_mtd) for x in range(len(cnt_vars))] })
                                        ,how = 'left', on = ['Variable'])
        ls_ds_features_pct['pct'] = ls_ds_features_pct['cnt'] / ls_ds_features_pct['ttl']
        ls_ds_features_pct = ls_ds_features_pct[ ~ls_ds_features_pct['pct'].isnull() ]
        ls_ds_features_pct.drop(['cnt', 'ttl'], axis = 1, inplace = True)

        # Rank of the selection
        ls_ds_features_rank = ls_ds_features_pct.copy()
        ls_ds_features_rank['pct2'] = 1 - ls_ds_features_rank['pct']
        ls_ds_features_rank['rank'] = rankdata(ls_ds_features_rank['pct2'], method = 'min')
        ls_ds_features_rank.drop(['pct', 'pct2'], axis = 1, inplace = True)
        ls_ds_features_rank.rename(columns = {'rank': repo_prefix_loop.replace('/', '')+' (rank)'}, inplace = True)
        df_features_rank = pd.merge(df_features_rank, ls_ds_features_rank, how = 'left', on = ['Variable', 'Description'])

        # df_features_pct
        ls_ds_features_pct.rename(columns = {'pct': repo_prefix_loop.replace('/', '')+' (%)'}, inplace = True)
        df_features_pct = pd.merge(df_features_pct, ls_ds_features_pct, how = 'left', on = ['Variable', 'Description'])
        
        #### Optimal model (within 1 SD of max average AUC and minimal number of features across algorithms)
        # Maximum of average score in cross-validation
        max_avg_score = max(ls_optimal_avg_cIndex)
        # Index of ls_optimal_avg_cIndex within 1 SD from max_avg_score
        idx_std = [ x for x in range(len(ls_optimal_avg_cIndex)) if (ls_optimal_avg_cIndex[x] < max_avg_score + statistics.stdev(ls_optimal_avg_cIndex))
                   & (ls_optimal_avg_cIndex[x] > max_avg_score - statistics.stdev(ls_optimal_avg_cIndex)) ]
        # Index of ls_optimal_model_nvars with minimal number of features
        list_num_vars = [ ls_optimal_model_nvars[x] for x in idx_std ]
        if len(list_num_vars) > 1 :
            min_num_vars = min(list_num_vars)
        else :
            min_num_vars = list_num_vars[0]
        idx_model = [x for x in idx_std if ls_optimal_model_nvars[x] == min_num_vars][0]

        compact_model_nvars = ls_optimal_model_nvars[idx_model]
        compact_avg_cIndex = ls_optimal_avg_cIndex[idx_model]
        compact_features = ls_ds_optimal_features[idx_model]
        compact_info = pd.DataFrame({'Description': ['Average score in CV', 'Number of features', 'Algorithm']
                                     ,repo_prefix_loop: [compact_avg_cIndex, compact_model_nvars, list_ml_models[idx_model]] })
        
        # The data frame listed the features of the optimal model
        df_features_best = pd.merge(df_features_best, compact_features, how = 'left', on = ['Variable'])
        df_features_info = pd.merge(df_features_info, compact_info, how = 'left', on = ['Description'])

        # Summary table of the average score, hold-out score and method for each algorithm
        df_table_score = pd.merge(df_table_score, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_cIndex + [ ls_optimal_avg_cIndex[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cIndex + [ ls_optimal_sd_cIndex[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                }), how = 'left', on = ['Algorithm'] )
        
        df_table_method = pd.merge(df_table_method, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                  ,repo_prefix_loop.replace('/', '_')+'method': ls_optimal_model_name + [ ls_optimal_model_name[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_cIndex + [ ls_optimal_avg_cIndex[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cIndex + [ ls_optimal_sd_cIndex[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars': ls_optimal_model_nvars + [ ls_optimal_model_nvars[idx_model] ]
                                                                  }), how = 'left', on = ['Algorithm'] )
    
    # The data listed features of the optimal model, average score in CV, number of features and algorithm across cohort analyses
    compact_features_comb = pd.concat([df_features_info, df_features_best], axis = 0)
    compact_features_comb['count_single'] = compact_features_comb[[x for x in repo_prefix[0:3] ]].count(axis = 1)   # Count for single cohort analyses
    compact_features_comb['count_all'] = compact_features_comb[[x for x in repo_prefix]].count(axis = 1)            # Count for all the cohort analyses
    compact_features_comb = compact_features_comb[ compact_features_comb['count_single'] > 0 ]
    compact_features_comb.sort_values(by='count_single', ascending=False, inplace = True)
    compact_features_comb.reset_index(drop = True, inplace = True)

    # The average of relative percentage across cohort
    df_features_pct['pct_single'] = df_features_pct[[ x for x in df_features_pct.columns.tolist()[2:5] ]].mean(axis = 1)    # Average (%) for single cohort analyses
    df_features_pct['pct_all'] = df_features_pct[[ x for x in df_features_pct.columns.tolist()[2:] ]].mean(axis = 1)        # Average (%) for all the cohort analyses
    df_features_pct = df_features_pct[ ~df_features_pct['pct_all'].isnull() ]

    # The rank of relative rank of the selection across cohort
    df_features_rank['rank_single'] = df_features_rank[[ x for x in df_features_rank.columns.tolist()[2:5] ]].mean(axis = 1)    # Average (rank) for single cohort analyses
    df_features_rank['rank_all'] = df_features_rank[[ x for x in df_features_rank.columns.tolist()[2:] ]].mean(axis = 1)        # Average (rank) for all the cohort analyses
    df_features_rank = df_features_rank[ ~df_features_rank['rank_all'].isnull() ]

    # Combine df_features_pct (%) and df_features_rank (rank)
    df_features_pct_rank = pd.merge(df_features_pct, df_features_rank, how = 'left', on = ['Variable', 'Description'])
    df_features_pct_rank.sort_values(by = 'rank_single', inplace = True)

    # Save the table into .csv and .html
    df_table_score.to_csv(fig_repo_out+'Table_C_INDEX_Overall_Compact'+fig_prefix+'.csv')
    df_table_score.to_html(fig_repo_out+'Table_C_INDEX_Overall_Compact'+fig_prefix+'.html')

    df_table_method.to_csv(fig_repo_out+'Table_C_INDEX_Overall_Compact_Method'+fig_prefix+'.csv')
    df_table_method.to_html(fig_repo_out+'Table_C_INDEX_Overall_Compact_Method'+fig_prefix+'.html')

    compact_features_comb.to_csv(fig_repo_out+'Cross_cohort_compact_features_table'+fig_prefix+'.csv')
    compact_features_comb.to_html(fig_repo_out+'Cross_cohort_compact_features_table'+fig_prefix+'.html')

    df_features_pct_rank.to_csv(fig_repo_out+'Cross_cohort_average_rank_compact_features_table'+fig_prefix+'.csv')
    df_features_pct_rank.to_html(fig_repo_out+'Cross_cohort_average_rank_compact_features_table'+fig_prefix+'.html')