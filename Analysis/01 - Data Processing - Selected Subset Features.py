#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                Levodopa-Induced Dyskinesia (NP4DYSKI2_Y4 & NP4DYSKI2_year)                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4DYSKI2_Y4    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4DYSKI2_Y4]
vars_select_dyski_1 = data_info[ data_info['Dyskinesias_1'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_2 = data_info[ data_info['Dyskinesias_2'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_3 = data_info[ data_info['Dyskinesias_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4DYSKI2    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4DYSKI2]
vars_select_dyski_bs_1 = data_info[ data_info['Dyskinesias_BS'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4DYSKI2_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4DYSKI2_year]
vars_select_dyski_year_1 = data_info[ data_info['DYSKI_SURV_1'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_year_2 = data_info[ data_info['DYSKI_SURV_2'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_year_3 = data_info[ data_info['DYSKI_SURV_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4DYSKI2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4DYSKI2_year]
vars_select_dyski_bs_year_1 = data_info[ data_info['DYSKI_BS_SURV_1'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_bs_year_2 = data_info[ data_info['DYSKI_BS_SURV_2'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_bs_year_3 = data_info[ data_info['DYSKI_BS_SURV_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Common pre-selected variables for NP4DYSKI2_Y4 and NP4DYSKI2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_select_dyski_bs_common = data_info[ data_info['DYSKI_BS_SURV_Y4'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Confounder variables for NP4DYSKI2_Y4 and NP4DYSKI2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_select_dyski_bs_confounder_1 = data_info[ data_info['DYSKI_Confounder_1'] == 'yes' ]['Variable'].tolist()
vars_select_dyski_bs_confounder_2 = data_info[ data_info['DYSKI_Confounder_2'] == 'yes' ]['Variable'].tolist()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                   Motor Fluctuations (NP4FLCTX2_Y4 & NP4FLCTX2_bs_year)                                                   ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4FLCTX2_Y4    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4FLCTX2_Y4]
vars_select_flctx_1 = data_info[ data_info['Fluctuation_1'] == 'yes' ]['Variable'].tolist()
vars_select_flctx_2 = data_info[ data_info['Fluctuation_2'] == 'yes' ]['Variable'].tolist()
vars_select_flctx_3 = data_info[ data_info['Fluctuation_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP4FLCTX2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP4FLCTX2_year]
vars_select_flctx_bs_year_1 = data_info[ data_info['FLCTX_BS_SURV_1'] == 'yes' ]['Variable'].tolist()
vars_select_flctx_bs_year_2 = data_info[ data_info['FLCTX_BS_SURV_2'] == 'yes' ]['Variable'].tolist()
vars_select_flctx_bs_year_3 = data_info[ data_info['FLCTX_BS_SURV_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Common pre-selected variables for NP4FLCTX2_Y4 and NP4FLCTX2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_select_flctx_bs_common = data_info[ data_info['FLCTX_BS_SURV_Y4'] == 'yes' ]['Variable'].tolist()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                               Impact of Cognitive Impairment (NP1COG2_Y4 & NP1COG2_bs_year)                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP1COG2_Y4    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP1COG2_Y4]
vars_select_cogn_1 = data_info[ data_info['COGN_1'] == 'yes' ]['Variable'].tolist()
vars_select_cogn_2 = data_info[ data_info['COGN_2'] == 'yes' ]['Variable'].tolist()
vars_select_cogn_3 = data_info[ data_info['COGN_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Pre-selected variables for NP1COG2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Pre-select variables (Common variables) [NP1COG2_bs_year]
vars_select_cogn_bs_year_1 = data_info[ data_info['COGN_BS_SURV_1'] == 'yes' ]['Variable'].tolist()
vars_select_cogn_bs_year_2 = data_info[ data_info['COGN_BS_SURV_2'] == 'yes' ]['Variable'].tolist()
vars_select_cogn_bs_year_3 = data_info[ data_info['COGN_BS_SURV_3'] == 'yes' ]['Variable'].tolist()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Common pre-selected variables for NP1COG2_Y4 and NP1COG2_bs_year    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_select_cogn_bs_common = data_info[ data_info['COGN_BS_SURV_Y4'] == 'yes' ]['Variable'].tolist()