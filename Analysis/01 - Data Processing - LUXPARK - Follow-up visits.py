#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                         Function - Extract Follow-up Characteristics and outcome                                                         ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_luxpark_followup(data
                          ,vars
                          ,outcome
                          ,var_patid
                          ,var_event
                          ,event_out
                          ,varname_outcome
                          ,varname_status
                          ,varname_duration
                          ) :
    """
    @data               : data
    @vars               : baseline variables
    @outcome            : outcome variable
    @var_patid          : variable for patient id
    @var_event          : variable for event_id
    @event_out          : event_id for baseline to follow-up (year-4)
    @varname_outcome    : variable name of the outcome
    @varname_status     : variable name of the outcome status
    @varname_duration   : variable name of the time from baseline to outcome
    """

    # Convert duration of PD from diagnosis to visit_date, from month to year
    data_duration = data[['cdisc_dm_usubjd', 'sv_basic_date', 'question260_260']]
    data_duration['visit_date'] = data_duration['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
    data_duration['visit_date'] = pd.to_datetime(data_duration['visit_date'], format = '%Y-%m-%d')
    data_duration['duration2'] = np.where( (data_duration['question260_260'] > 0) & (~data_duration['question260_260'].isnull()) & (~data_duration['visit_date'].isnull())
                                          ,pd.DatetimeIndex(data_duration['visit_date']).year - data_duration['question260_260']
                                          ,np.nan )
    data_duration = data_duration[ (~data_duration['duration2'].isnull()) & (data_duration['duration2'] >= 0) ].reset_index(drop = True)
    data_duration = data_duration[['cdisc_dm_usubjd', 'visit_date', 'duration2']]
    data_duration.rename(columns = {'cdisc_dm_usubjd': var_patid}, inplace = True)

    # # Baseline data
    luxpark_bs = preprocess_luxpark(data = data, data_variables = data_variables, vars_info = data_variables_luxpark, event_yr = event_out[0])
    luxpark_bs.process_luxpark()
    luxpark_bs_derived = luxpark_bs.rename_luxpark()
    luxpark_bs_derived = pd.merge(luxpark_bs_derived, data_duration, how = 'left', on = [var_patid, 'visit_date'])
    luxpark_bs_derived.rename(columns = {'visit_date' : 'ENROLL_DATE'}, inplace = True)
    luxpark_bs_derived['duration'] = luxpark_bs_derived['duration2']
    luxpark_bs_derived = luxpark_bs_derived[ list(np.unique([var_patid, var_event] + vars + [outcome])) + ['ENROLL_DATE'] ]
    luxpark_bs_derived = luxpark_bs_derived[ ~luxpark_bs_derived[outcome].isnull() ].reset_index(drop = True)

    # # Year-1 data
    luxpark_y1 = preprocess_luxpark(data = data, data_variables = data_variables, vars_info = data_variables_luxpark, event_yr = event_out[1])
    luxpark_y1.process_luxpark()
    luxpark_y1_derived = luxpark_y1.rename_luxpark()
    luxpark_y1_derived = luxpark_y1_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    luxpark_y1_derived = luxpark_y1_derived[ ~luxpark_y1_derived[outcome].isnull() ].reset_index(drop = True)
    luxpark_y1_derived.rename(columns = {outcome: 'out_y1', 'visit_date': 'date_y1'}, inplace = True)

    # # Year-2 data
    luxpark_y2 = preprocess_luxpark(data = data, data_variables = data_variables, vars_info = data_variables_luxpark, event_yr = event_out[2])
    luxpark_y2.process_luxpark()
    luxpark_y2_derived = luxpark_y2.rename_luxpark()
    luxpark_y2_derived = luxpark_y2_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    luxpark_y2_derived = luxpark_y2_derived[ ~luxpark_y2_derived[outcome].isnull() ].reset_index(drop = True)
    luxpark_y2_derived.rename(columns = {outcome: 'out_y2', 'visit_date': 'date_y2'}, inplace = True)

    # # Year-3 data
    luxpark_y3 = preprocess_luxpark(data = data, data_variables = data_variables, vars_info = data_variables_luxpark, event_yr = event_out[3])
    luxpark_y3.process_luxpark()
    luxpark_y3_derived = luxpark_y3.rename_luxpark()
    luxpark_y3_derived = luxpark_y3_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    luxpark_y3_derived = luxpark_y3_derived[ ~luxpark_y3_derived[outcome].isnull() ].reset_index(drop = True)
    luxpark_y3_derived.rename(columns = {outcome: 'out_y3', 'visit_date': 'date_y3'}, inplace = True)

    # # Year-4 data
    luxpark_y4 = preprocess_luxpark(data = data, data_variables = data_variables, vars_info = data_variables_luxpark, event_yr = event_out[4])
    luxpark_y4.process_luxpark()
    luxpark_y4_derived = luxpark_y4.rename_luxpark()
    luxpark_y4_derived = luxpark_y4_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    luxpark_y4_derived = luxpark_y4_derived[ ~luxpark_y4_derived[outcome].isnull() ].reset_index(drop = True)
    luxpark_y4_derived.rename(columns = {outcome: 'out_y4', 'visit_date': 'date_y4'}, inplace = True)

    # The last 'visit_date'
    # luxpark_last_followup = data[['cdisc_dm_usubjd', 'sv_basic_date']]
    # luxpark_last_followup['visit_date'] = luxpark_last_followup['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
    # luxpark_last_followup['visit_date'] = pd.to_datetime(luxpark_last_followup['visit_date'], format = '%Y-%m-%d')
    # luxpark_last_followup.rename(columns = {'cdisc_dm_usubjd': var_patid}, inplace = True)
    # luxpark_last_followup.drop(['sv_basic_date'], axis = 1, inplace = True)
    # luxpark_last_followup = luxpark_last_followup[[var_patid, 'visit_date']].groupby(var_patid).max().reset_index()
    # luxpark_last_followup.rename(columns = {'visit_date': 'date_last'}, inplace = True)

    # # Outcome from Baseline to year-4 follow-up
    # # The follow-up outcome is the first event within 4-year
    luxpark_outcome = pd.merge( luxpark_bs_derived[[var_patid, outcome, 'ENROLL_DATE']], luxpark_y1_derived, how = 'left', on = [var_patid] )
    luxpark_outcome = pd.merge( luxpark_outcome, luxpark_y2_derived, how = 'left', on = [var_patid] )
    luxpark_outcome = pd.merge( luxpark_outcome, luxpark_y3_derived, how = 'left', on = [var_patid] )
    luxpark_outcome = pd.merge( luxpark_outcome, luxpark_y4_derived, how = 'left', on = [var_patid] )
    luxpark_outcome[varname_outcome] = np.where( (luxpark_outcome[outcome] == 1) |
                                                (luxpark_outcome['out_y1'] == 1) |
                                                (luxpark_outcome['out_y2'] == 1) |
                                                (luxpark_outcome['out_y3'] == 1) |
                                                (luxpark_outcome['out_y4'] == 1), 1, luxpark_outcome['out_y4'] )
    
    # @varname_status = 1
    if outcome == 'NP4DYSKI2' :

        data_all = data[ data['diag_ipd'] == 1 ]

        # Dyskinesias : u_q4_2, u_q4_1, u_q4_1_3, question273_273___17
        data_all.loc[ ((data_all['u_q4_2'] == 0) & (data_all['u_q4_1'] == 0) & (data_all['u_q4_1_3'] == 0)) | (data_all['question273_273___17'] == False), 'NP4DYSKI2' ] = 0
        data_all.loc[ (data_all['u_q4_2'] > 0) | (data_all['u_q4_1'] > 0) | (data_all['u_q4_1_3'] > 0) | (data_all['question273_273___17'] == True), 'NP4DYSKI2' ] = 1
    
    elif outcome == 'NP4FLCTX2' :

        data_all = data[ data['diag_ipd'] == 1 ]

        # Motor fluctuations
        data_all['NP4FLCTX2'] = np.where( (data_all['u_q4_3'] > 0) | (data_all['u_q4_4'] > 0) | (data_all['u_q4_5'] > 0), 1
                                         ,np.where( (data_all['u_q4_3'] == 0) | (data_all['u_q4_4'] == 0) | (data_all['u_q4_5'] == 0), 0, np.nan) )

    else :
        print("! ! ! Please update the data manipulation ! ! !")

    data_all.rename(columns = {'cdisc_dm_usubjd': var_patid}, inplace = True)
    data_all['visit_date'] = data_all['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
    data_all['visit_date'] = pd.to_datetime(data_all['visit_date'], format = '%Y-%m-%d')

    data_all_out = pd.merge(data_all[[var_patid, 'visit_date', outcome]], luxpark_bs_derived[[var_patid, 'ENROLL_DATE']], how = 'left', on = [var_patid])
    data_all_out = data_all_out[ (~data_all_out['visit_date'].isnull()) & (data_all_out['visit_date'] >= data_all_out['ENROLL_DATE']) ]

    # outcome == 1
    data_out_1 = data_all_out[ data_all_out[outcome] == 1 ][[var_patid, 'visit_date']]
    data_out1 = data_out_1.groupby(var_patid).min().reset_index()
    data_out1.reset_index(drop = True, inplace = True)
    data_out1[varname_status] = 1

    # outcome == 0
    data_out_0 = data_all_out[ data_all_out[outcome] == 0 ][[var_patid, 'visit_date']]
    data_out0 = data_out_0.groupby(var_patid).max().reset_index()
    data_out0 = data_out0[ ~data_out0[var_patid].isin(data_out1[var_patid]) ]
    data_out0.reset_index(drop = True, inplace = True)
    data_out0[varname_status] = 0

    # combine data_out1 and data_out0
    data_out_comb = pd.concat([data_out1, data_out0], axis = 0).reset_index(drop = True)
    data_out_comb = pd.merge(data_out_comb, luxpark_bs_derived[[var_patid, 'ENROLL_DATE']], how = 'left', on = [var_patid])

    # Duration Outcome (Calculate from date of enrollment)
    data_out_comb[varname_duration] = (data_out_comb['visit_date'] - data_out_comb['ENROLL_DATE'])/timedelta(days=365.25)
    
    # To keep the observation if outcome variables are not missing
    luxpark_outcome = pd.merge(luxpark_outcome, data_out_comb[[var_patid, varname_status, varname_duration]])
    luxpark_outcome = luxpark_outcome[ (~luxpark_outcome[varname_outcome].isnull()) & (~luxpark_outcome[varname_duration].isnull()) ][[var_patid, varname_outcome, varname_duration]]
    luxpark_outcome.reset_index(drop = True, inplace = True)

    # Merge the follow-up outcome from ppmi_outcome
    luxpark_comb_derived = pd.merge( luxpark_bs_derived, luxpark_outcome, on = [var_patid], how = 'left' )

    # # Keep the data with no missing values
    luxpark_comb_derived['count_miss'] = luxpark_comb_derived.isnull().sum(axis = 1)
    luxpark_comb_derived = luxpark_comb_derived[ luxpark_comb_derived['count_miss'] == 0 ]
    luxpark_comb_derived.drop(['count_miss', 'ENROLL_DATE'], axis = 1, inplace = True)
    luxpark_comb_derived.reset_index(drop = True, inplace = True)

    # Return the processed data
    return( luxpark_comb_derived )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Outcome : NP4DYSKI2_Y4                                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-1 vs Year-5 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_luxpark_y1.shape : (6, 15)
data_luxpark_y1 = process_luxpark_followup( data = data_luxpark
                                           ,vars = vars_select_dyski_bs_common
                                           ,outcome = 'NP4DYSKI2'
                                           ,var_patid = 'PATNO'
                                           ,var_event = 'EVENT_ID'
                                           ,event_out = ['Y1', 'Y2', 'Y3', 'Y4', 'Y5']
                                           ,varname_outcome = 'NP4DYSKI2_Y4'
                                           ,varname_status = 'NP4DYSKI2_status'
                                           ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-2 vs Year-6 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_luxpark_y2.shape : (1, 15)
data_luxpark_y2 = process_luxpark_followup( data = data_luxpark
                                           ,vars = vars_select_dyski_bs_common
                                           ,outcome = 'NP4DYSKI2'
                                           ,var_patid = 'PATNO'
                                           ,var_event = 'EVENT_ID'
                                           ,event_out = ['Y2', 'Y3', 'Y4', 'Y5', 'Y6']
                                           ,varname_outcome = 'NP4DYSKI2_Y4'
                                           ,varname_status = 'NP4DYSKI2_status'
                                           ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI data at follow-up as baseline    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_followup_luxpark_dyski = pd.concat([data_luxpark_y1
                                         ,data_luxpark_y2 ], axis = 0)
data_followup_luxpark_dyski['PATNO'] = 'LUXPARK_' + data_followup_luxpark_dyski['EVENT_ID'] + '_' + data_followup_luxpark_dyski['PATNO'].astype('str')
data_followup_luxpark_dyski.reset_index(drop = True, inplace = True)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Outcome : NP4FLCTX2_Y4                                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-1 vs Year-5 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_luxpark_y1.shape : (6, 14)
data_luxpark_y1 = process_luxpark_followup( data = data_luxpark
                                           ,vars = vars_select_flctx_bs_common
                                           ,outcome = 'NP4FLCTX2'
                                           ,var_patid = 'PATNO'
                                           ,var_event = 'EVENT_ID'
                                           ,event_out = ['Y1', 'Y2', 'Y3', 'Y4', 'Y5']
                                           ,varname_outcome = 'NP4FLCTX2_Y4'
                                           ,varname_status = 'NP4FLCTX2_status'
                                           ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-2 vs Year-6 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_luxpark_y2.shape : (1, 14)
data_luxpark_y2 = process_luxpark_followup( data = data_luxpark
                                           ,vars = vars_select_flctx_bs_common
                                           ,outcome = 'NP4FLCTX2'
                                           ,var_patid = 'PATNO'
                                           ,var_event = 'EVENT_ID'
                                           ,event_out = ['Y2', 'Y3', 'Y4', 'Y5', 'Y6']
                                           ,varname_outcome = 'NP4FLCTX2_Y4'
                                           ,varname_status = 'NP4FLCTX2_status'
                                           ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI data at follow-up as baseline    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_followup_luxpark_flctx = pd.concat([data_luxpark_y1
                                         ,data_luxpark_y2 ], axis = 0)
data_followup_luxpark_flctx['PATNO'] = 'LUXPARK_' + data_followup_luxpark_flctx['EVENT_ID'] + '_' + data_followup_luxpark_flctx['PATNO'].astype('str')
data_followup_luxpark_flctx.reset_index(drop = True, inplace = True)