#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Spearman and point-biserial correlation of each variables to the outcome    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from scipy.stats import spearmanr
from scipy.stats import pointbiserialr
from sklearn.metrics import matthews_corrcoef
from scipy.stats import fisher_exact
from scipy.stats import kendalltau

class Spearman_pb_outcome :
    def __init__(self
                 ,data
                 ,data_name
                 ,outcome
                 ,vars_name
                 ,repo_output ) :
        
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.vars_name = vars_name
        self.repo_output = repo_output

    def measure(self) :
        
        ds_mi_comb = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_mi_comb = pd.merge(ds_mi_comb, self.vars_name, how = 'left', on = ['Variable'])

        ds_pb_comb = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_pb_comb = pd.merge(ds_pb_comb, self.vars_name, how = 'left', on = ['Variable'])

        ds_mcc_comb = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_mcc_comb = pd.merge(ds_pb_comb, self.vars_name, how = 'left', on = ['Variable'])

        for loop_data, loop_data_name in zip(self.data, self.data_name) :
            
            loop_data = loop_data[ ~loop_data[self.outcome].isnull() ]
            n_samples = loop_data.shape[0]
            data_columns = [x for x in loop_data.columns if x != self.outcome ]

            
            rho_score = []
            p_value = []
            n_corr = []

            pb_score = []
            pb_p_value = []
            n_pb_corr = []

            mcc_score = []
            mcc_p_value = []
            n_mcc_corr = []

            for loop_vars in data_columns :
                loop_data_nmiss = loop_data[[loop_vars, self.outcome]]
                loop_data_nmiss = loop_data_nmiss[ (~loop_data_nmiss[loop_vars].isnull()) & (~loop_data_nmiss[self.outcome].isnull()) ]

                # Spearman correlation of the variable with the outcome variable
                if len(np.unique(loop_data_nmiss[loop_vars])) > 1 :
                    score, p = spearmanr(loop_data_nmiss[loop_vars], loop_data_nmiss[self.outcome])
                else :
                    score, p = np.nan, np.nan
                
                rho_score.append(score)
                p_value.append(p)
                n_corr.append(loop_data_nmiss.shape[0])
                
                # Point-biserial correlation of the variable continuous variable with the binary outcome variable
                if len(np.unique(loop_data_nmiss[loop_vars])) > 3 :
                    pb_corr, pb_p = pointbiserialr(loop_data_nmiss[self.outcome], loop_data_nmiss[loop_vars])
                else :
                    pb_corr, pb_p = np.nan, np.nan
                
                pb_score.append(pb_corr)
                pb_p_value.append(pb_p)
                n_pb_corr.append(loop_data_nmiss.shape[0])
                
                # Matthews Correlation Coefficient
                if len(np.unique(loop_data_nmiss[loop_vars])) == 2 :
                    mcc_corr = matthews_corrcoef(loop_data_nmiss[self.outcome], loop_data_nmiss[loop_vars])

                    _, mcc_p = fisher_exact( np.array(pd.crosstab(loop_data_nmiss[self.outcome], loop_data_nmiss[loop_vars])) )
                    
                else :
                    mcc_corr, mcc_p = np.nan, np.nan
                
                mcc_score.append(mcc_corr)
                mcc_p_value.append(mcc_p)
                n_mcc_corr.append(loop_data_nmiss.shape[0])
            
            # Combining the results of Spearman correlation
            ds_mi = pd.DataFrame({'Variable' : data_columns
                                  ,loop_data_name+' (n)': n_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_corr]
                                  ,loop_data_name+' (Correlation)': rho_score
                                  ,loop_data_name+' (p-value)': p_value})
            ds_mi_comb = pd.merge(ds_mi_comb, ds_mi, how = 'left', on = ['Variable'])

            # Combining the results of Point-biserial correlation
            ds_pb = pd.DataFrame({'Variable' : data_columns
                                  ,loop_data_name+' (n)': n_pb_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_pb_corr]
                                  ,loop_data_name+' (Correlation)': pb_score
                                  ,loop_data_name+' (p-value)': pb_p_value})
            ds_pb_comb = pd.merge(ds_pb_comb, ds_pb, how = 'left', on = ['Variable'])

            # Combining the results of Matthews Correlation Coefficient
            ds_mcc = pd.DataFrame({'Variable' : data_columns
                                  ,loop_data_name+' (n)': n_mcc_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_mcc_corr]
                                  ,loop_data_name+' (Correlation)': mcc_score
                                  ,loop_data_name+' (p-value)': mcc_p_value})
            ds_mcc_comb = pd.merge(ds_mcc_comb, ds_mcc, how = 'left', on = ['Variable'])

        # Save the output into HTML table
        ds_mi_comb.to_csv(self.repo_output+'Spearman_Correlation_'+self.outcome+'.csv')
        ds_mi_comb.to_html(self.repo_output+'Spearman_Correlation_'+self.outcome+'.html')

        ds_pb_comb.to_csv(self.repo_output+'Point_Biserial_Correlation_'+self.outcome+'.csv')
        ds_pb_comb.to_html(self.repo_output+'Point_Biserial_Correlation_'+self.outcome+'.html')

        ds_mcc_comb.to_csv(self.repo_output+'Matthews_Correlation_Coefficient_'+self.outcome+'.csv')
        ds_mcc_comb.to_html(self.repo_output+'Matthews_Correlation_Coefficient_'+self.outcome+'.html')
        

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Spearman correlation of each continuous variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class Correlation_features :
    def __init__(self
                 ,data
                 ,data_name
                 ,outcome
                 ,vars_name
                 ,repo_output ) :
        
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.vars_name = vars_name
        self.repo_output = repo_output

    def measure(self) :
        
        ds_corr_vars = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_corr_vars = pd.merge(ds_corr_vars, self.vars_name, how = 'left', on = ['Variable'])

        for loop_data, loop_data_name, ii in zip(self.data, self.data_name, range(len(self.data))) :

            loop_data = loop_data[ ~loop_data[self.outcome].isnull() ]
            n_samples = loop_data.shape[0]

            data_columns = [x for x in loop_data.columns if x != self.outcome ]

            var_i = []
            var_j = []
            rho_score = []
            p_value = []
            n_corr = []

            for loop_vars_i in range(len(data_columns)) :
                for loop_vars_j in range(len(data_columns)) :
                    if (loop_vars_j != loop_vars_i) :
                    # if (loop_vars_j > loop_vars_i) & (loop_vars_j != loop_vars_i) :
                        loop_data_nmiss = loop_data[[ data_columns[loop_vars_i], data_columns[loop_vars_j] ]]
                        loop_data_nmiss = loop_data_nmiss[ (~loop_data_nmiss[data_columns[loop_vars_i]].isnull()) & (~loop_data_nmiss[data_columns[loop_vars_j]].isnull()) ]

                        # Spearman correlation of two continuous variable
                        if (len(np.unique(loop_data_nmiss[data_columns[loop_vars_i]])) > 2) & (len(np.unique(loop_data_nmiss[data_columns[loop_vars_j]])) > 2) :
                            score, p = spearmanr(loop_data_nmiss[data_columns[loop_vars_i]], loop_data_nmiss[data_columns[loop_vars_j]])
                        else :
                            score, p = np.nan, np.nan
                        
                        var_i.append(data_columns[loop_vars_i])
                        var_j.append(data_columns[loop_vars_j])
                        rho_score.append(score)
                        p_value.append(p)
                        n_corr.append(loop_data_nmiss.shape[0])
                    
                    else :
                        pass

            # Combining the results of Spearman correlation
            if ii == 0 :
                ds_corr_temp = pd.merge(pd.DataFrame({'Feature_1': var_i, 'Feature_2': var_j })
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_1', 'Description': 'Feature 1'})
                                        ,on = ['Feature_1']
                                        ,how = "left" )
                ds_corr_temp = pd.merge(ds_corr_temp
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_2', 'Description': 'Feature 2'})
                                        ,on = ['Feature_2']
                                        ,how = "left" )
                ds_corr_comb = ds_corr_temp
            else :
                pass
            
            ds_corr = pd.DataFrame({'Feature_1' : var_i
                                  ,'Feature_2': var_j
                                  ,loop_data_name+' (n)': n_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_corr]
                                  ,loop_data_name+' (Correlation)': rho_score
                                  ,loop_data_name+' (p-value)': p_value})
            ds_corr = pd.merge(ds_corr_temp, ds_corr, on = ['Feature_1', 'Feature_2'], how = 'left')
            ds_corr_comb = pd.merge(ds_corr_comb, ds_corr, how = 'left', on = ['Feature_1', 'Feature_2', 'Feature 1', 'Feature 2'])


        # Save the output into HTML table
        ds_corr_comb.to_csv(self.repo_output+'Spearman_Correlation_Continuous_Features_'+self.outcome+'.csv')
        ds_corr_comb.to_html(self.repo_output+'Spearman_Correlation_Continuous_Features_'+self.outcome+'.html')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Point-biserial correlation of binary vs continuous variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class Point_biserial_features :
    def __init__(self
                 ,data
                 ,data_name
                 ,outcome
                 ,vars_name
                 ,repo_output ) :
        
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.vars_name = vars_name
        self.repo_output = repo_output

    def measure(self) :
        
        ds_corr_vars = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_corr_vars = pd.merge(ds_corr_vars, self.vars_name, how = 'left', on = ['Variable'])

        for loop_data, loop_data_name, ii in zip(self.data, self.data_name, range(len(self.data))) :

            loop_data = loop_data[ ~loop_data[self.outcome].isnull() ]
            n_samples = loop_data.shape[0]

            data_columns = [x for x in loop_data.columns if x != self.outcome ]

            var_i = []
            var_j = []
            rho_score = []
            p_value = []
            n_corr = []

            for loop_vars_i in range(len(data_columns)) :
                for loop_vars_j in range(len(data_columns)) :
                    if (loop_vars_j > loop_vars_i) & (loop_vars_j != loop_vars_i) :
                        loop_data_nmiss = loop_data[[ data_columns[loop_vars_i], data_columns[loop_vars_j] ]]
                        loop_data_nmiss = loop_data_nmiss[ (~loop_data_nmiss[data_columns[loop_vars_i]].isnull()) & (~loop_data_nmiss[data_columns[loop_vars_j]].isnull()) ]

                        # Point-biserial correlation of two continuous variable
                        if ( (len(np.unique(loop_data_nmiss[data_columns[loop_vars_i]])) == 2) & (len(np.unique(loop_data_nmiss[data_columns[loop_vars_j]])) > 2) |
                             (len(np.unique(loop_data_nmiss[data_columns[loop_vars_i]])) > 2) & (len(np.unique(loop_data_nmiss[data_columns[loop_vars_j]])) == 2)) :
                            score, p = pointbiserialr(loop_data_nmiss[data_columns[loop_vars_i]], loop_data_nmiss[data_columns[loop_vars_j]])
                        else :
                            score, p = np.nan, np.nan
                        
                        var_i.append(data_columns[loop_vars_i])
                        var_j.append(data_columns[loop_vars_j])
                        rho_score.append(score)
                        p_value.append(p)
                        n_corr.append(loop_data_nmiss.shape[0])
                    
                    else :
                        pass

            # Combining the results of point-biserial correlation
            if ii == 0 :
                ds_corr_temp = pd.merge(pd.DataFrame({'Feature_1': var_i, 'Feature_2': var_j })
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_1', 'Description': 'Feature 1'})
                                        ,on = ['Feature_1']
                                        ,how = "left" )
                ds_corr_temp = pd.merge(ds_corr_temp
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_2', 'Description': 'Feature 2'})
                                        ,on = ['Feature_2']
                                        ,how = "left" )
                ds_corr_comb = ds_corr_temp
            else :
                pass
            
            ds_corr = pd.DataFrame({'Feature_1' : var_i
                                  ,'Feature_2': var_j
                                  ,loop_data_name+' (n)': n_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_corr]
                                  ,loop_data_name+' (Correlation)': rho_score
                                  ,loop_data_name+' (p-value)': p_value})
            ds_corr = pd.merge(ds_corr_temp, ds_corr, on = ['Feature_1', 'Feature_2'], how = 'left')
            ds_corr_comb = pd.merge(ds_corr_comb, ds_corr, how = 'left', on = ['Feature_1', 'Feature_2', 'Feature 1', 'Feature 2'])


        # Save the output into HTML table
        ds_corr_comb.to_csv(self.repo_output+'Point_Biserial_Correlation_Binary_Continuous_Features_'+self.outcome+'.csv')
        ds_corr_comb.to_html(self.repo_output+'Point_Biserial_Correlation_Binary_Continuous_Features_'+self.outcome+'.html')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Kendall's tau correlation of each ordinal variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class Kendall_tau_features :
    def __init__(self
                 ,data
                 ,data_name
                 ,outcome
                 ,vars_name
                 ,repo_output ) :
        
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.vars_name = vars_name
        self.repo_output = repo_output

    def measure(self) :
        
        ds_corr_vars = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_corr_vars = pd.merge(ds_corr_vars, self.vars_name, how = 'left', on = ['Variable'])

        for loop_data, loop_data_name, ii in zip(self.data, self.data_name, range(len(self.data))) :

            loop_data = loop_data[ ~loop_data[self.outcome].isnull() ]
            n_samples = loop_data.shape[0]

            data_columns = [x for x in loop_data.columns if x != self.outcome ]

            var_i = []
            var_j = []
            rho_score = []
            p_value = []
            n_corr = []

            for loop_vars_i in range(len(data_columns)) :
                for loop_vars_j in range(len(data_columns)) :
                    if (loop_vars_j > loop_vars_i) & (loop_vars_j != loop_vars_i) :
                        loop_data_nmiss = loop_data[[ data_columns[loop_vars_i], data_columns[loop_vars_j] ]]
                        loop_data_nmiss = loop_data_nmiss[ (~loop_data_nmiss[data_columns[loop_vars_i]].isnull()) & (~loop_data_nmiss[data_columns[loop_vars_j]].isnull()) ]

                        # Kendall's tau correlation of two continuous variable
                        if ( 2 < len(np.unique(loop_data_nmiss[data_columns[loop_vars_i]])) < 6 ) & ( 2 < len(np.unique(loop_data_nmiss[data_columns[loop_vars_j]])) < 6 ) :
                            score, p = kendalltau(loop_data_nmiss[data_columns[loop_vars_i]], loop_data_nmiss[data_columns[loop_vars_j]])
                        else :
                            score, p = np.nan, np.nan
                        
                        var_i.append(data_columns[loop_vars_i])
                        var_j.append(data_columns[loop_vars_j])
                        rho_score.append(score)
                        p_value.append(p)
                        n_corr.append(loop_data_nmiss.shape[0])
                    
                    else :
                        pass

            # Combining the results of Kendall's tau correlation
            if ii == 0 :
                ds_corr_temp = pd.merge(pd.DataFrame({'Feature_1': var_i, 'Feature_2': var_j })
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_1', 'Description': 'Feature 1'})
                                        ,on = ['Feature_1']
                                        ,how = "left" )
                ds_corr_temp = pd.merge(ds_corr_temp
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_2', 'Description': 'Feature 2'})
                                        ,on = ['Feature_2']
                                        ,how = "left" )
                ds_corr_comb = ds_corr_temp
            else :
                pass
            
            ds_corr = pd.DataFrame({'Feature_1' : var_i
                                  ,'Feature_2': var_j
                                  ,loop_data_name+' (n)': n_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_corr]
                                  ,loop_data_name+' (Correlation)': rho_score
                                  ,loop_data_name+' (p-value)': p_value})
            ds_corr = pd.merge(ds_corr_temp, ds_corr, on = ['Feature_1', 'Feature_2'], how = 'left')
            ds_corr_comb = pd.merge(ds_corr_comb, ds_corr, how = 'left', on = ['Feature_1', 'Feature_2', 'Feature 1', 'Feature 2'])


        # Save the output into HTML table
        ds_corr_comb.to_csv(self.repo_output+'Kendall_Tau_Correlation_Ordinal_Features_'+self.outcome+'.csv')
        ds_corr_comb.to_html(self.repo_output+'Kendall_Tau_Correlation_Ordinal_Features_'+self.outcome+'.html')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Matthew's correlation correlation of each binary variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class MCC_features :
    def __init__(self
                 ,data
                 ,data_name
                 ,outcome
                 ,vars_name
                 ,repo_output ) :
        
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.vars_name = vars_name
        self.repo_output = repo_output

    def measure(self) :
        
        ds_corr_vars = pd.DataFrame({'Variable' : [x for x in self.data[0].columns if x != self.outcome ]})
        ds_corr_vars = pd.merge(ds_corr_vars, self.vars_name, how = 'left', on = ['Variable'])

        for loop_data, loop_data_name, ii in zip(self.data, self.data_name, range(len(self.data))) :

            loop_data = loop_data[ ~loop_data[self.outcome].isnull() ]
            n_samples = loop_data.shape[0]

            data_columns = [x for x in loop_data.columns if x != self.outcome ]

            var_i = []
            var_j = []
            rho_score = []
            p_value = []
            n_corr = []

            for loop_vars_i in range(len(data_columns)) :
                for loop_vars_j in range(len(data_columns)) :
                    if (loop_vars_j > loop_vars_i) & (loop_vars_j != loop_vars_i) :
                        loop_data_nmiss = loop_data[[ data_columns[loop_vars_i], data_columns[loop_vars_j] ]]
                        loop_data_nmiss = loop_data_nmiss[ (~loop_data_nmiss[data_columns[loop_vars_i]].isnull()) & (~loop_data_nmiss[data_columns[loop_vars_j]].isnull()) ]

                        # Spearman correlation of two continuous variable
                        if (len(np.unique(loop_data_nmiss[data_columns[loop_vars_i]])) == 2) & (len(np.unique(loop_data_nmiss[data_columns[loop_vars_j]])) == 2) :
                            score = matthews_corrcoef(loop_data_nmiss[data_columns[loop_vars_i]], loop_data_nmiss[data_columns[loop_vars_j]])
                            _, p = fisher_exact( np.array(pd.crosstab(loop_data_nmiss[data_columns[loop_vars_i]], loop_data_nmiss[data_columns[loop_vars_j]] ) )) 
                        else :
                            score, p = np.nan, np.nan
                        
                        var_i.append(data_columns[loop_vars_i])
                        var_j.append(data_columns[loop_vars_j])
                        rho_score.append(score)
                        p_value.append(p)
                        n_corr.append(loop_data_nmiss.shape[0])
                    
                    else :
                        pass

            # Combining the results of Spearman correlation
            if ii == 0 :
                ds_corr_temp = pd.merge(pd.DataFrame({'Feature_1': var_i, 'Feature_2': var_j })
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_1', 'Description': 'Feature 1'})
                                        ,on = ['Feature_1']
                                        ,how = "left" )
                ds_corr_temp = pd.merge(ds_corr_temp
                                        ,ds_corr_vars.rename(columns = {'Variable': 'Feature_2', 'Description': 'Feature 2'})
                                        ,on = ['Feature_2']
                                        ,how = "left" )
                ds_corr_comb = ds_corr_temp
            else :
                pass
            
            ds_corr = pd.DataFrame({'Feature_1' : var_i
                                  ,'Feature_2': var_j
                                  ,loop_data_name+' (n)': n_corr
                                  ,loop_data_name+' (%)': [x/n_samples*100 for x in n_corr]
                                  ,loop_data_name+' (Correlation)': rho_score
                                  ,loop_data_name+' (p-value)': p_value})
            ds_corr = pd.merge(ds_corr_temp, ds_corr, on = ['Feature_1', 'Feature_2'], how = 'left')
            ds_corr_comb = pd.merge(ds_corr_comb, ds_corr, how = 'left', on = ['Feature_1', 'Feature_2', 'Feature 1', 'Feature 2'])


        # Save the output into HTML table
        ds_corr_comb.to_csv(self.repo_output+'MCC_Correlation_Binary_Features_'+self.outcome+'.csv')
        ds_corr_comb.to_html(self.repo_output+'MCC_Correlation_Binary_Features_'+self.outcome+'.html')