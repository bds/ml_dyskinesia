#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Hosmer-Lemeshow test    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def HosmerLemeshow(true_y, proba_y, pred_y, Q = 10) :
    """
    Hosmer-Lemeshow Goodness of fit test

    Parameters
    ----------
    true_y : list
        true value of the response
    proba_y : list
        predicted proportion to the response
    pred_y : list
        predicted value of the response
    Q : int, optional
        The number of groups, 10 is recommended

    Returns
    -------
    HL_result : the result of the test, including Chi2-HL statistics and p-value
    """

    df_hl_y = pd.DataFrame({'true_y': true_y, 'proba_y': proba_y, 'pred_y': pred_y})
    df_hl_y.sort_values(by = 'proba_y', ascending = False, inplace = True)
    df_hl_y = df_hl_y.reset_index()
    df_hl_y['rank'] = list(range(df_hl_y.shape[0]))

    categories, bins = pd.cut(
        df_hl_y['rank']
        ,np.percentile(df_hl_y['rank'], np.linspace(0, 100, Q + 1))
        ,labels = False
        ,include_lowest = False
        ,retbins = True )

    meanprobs1 = np.zeros(Q)
    expevents1 = np.zeros(Q)
    obsevents1 = np.zeros(Q)
    meanprobs0 = np.zeros(Q)
    expevents0 = np.zeros(Q)
    obsevents0 = np.zeros(Q)

    expected_prop = df_hl_y['proba_y']
    observed_value = df_hl_y['true_y']

    for i in range(Q) :
        meanprobs1[i] = np.mean( expected_prop[categories == i] )
        expevents1[i] = np.sum( [categories == i] * np.array(meanprobs1[i]) )
        obsevents1[i] = np.sum( observed_value[categories == i] )
        meanprobs0[i] = np.mean(1-expected_prop[categories == i])
        expevents0[i] = np.sum( [categories == i] * np.array(meanprobs0[i]) )
        obsevents0[i] = np.sum( 1 - observed_value[categories == i] )
    
    data1={'meanprobs1':meanprobs1,'meanprobs0':meanprobs0}
    data2={'expevents1':expevents1,'expevents0':expevents0}
    data3={'obsevents1':obsevents1,'obsevents0':obsevents0}
    m=pd.DataFrame(data1)
    e=pd.DataFrame(data2)
    o=pd.DataFrame(data3)

    # If >20% of expected value is <5, then perform Fisher exact test
    if (any(expevents1 == 0)) | (any(expevents0 == 0)) :
        # Calculate the Hosmer-Lemeshow statistic (Fisher exact)
        fisher_value = []
        for i in range(Q):
            df_fisher = df_hl_y[categories==i]
            a = df_fisher[ (df_fisher['true_y'] == 1) & (df_fisher['pred_y'] == 1) ].shape[0]
            b = df_fisher[ (df_fisher['true_y'] == 0) & (df_fisher['pred_y'] == 1) ].shape[0]
            c = df_fisher[ (df_fisher['true_y'] == 1) & (df_fisher['pred_y'] == 0) ].shape[0]
            d = df_fisher[ (df_fisher['true_y'] == 0) & (df_fisher['pred_y'] == 0) ].shape[0]

            part1 = math.factorial(a+b) * math.factorial(c+d) * math.factorial(a+c) * math.factorial(b+d)
            part2 = math.factorial(a) * math.factorial(b) * math.factorial(c) * math.factorial(d) * math.factorial(a+b+c+d)
            fisher_value.append( part1/part2 )

            # fisher_table = np.array( [[a, b], [c, d]] )
            # fisher_exact( fisher_table, alternative = 'two-sided' )
        chisq_value = sum(fisher_value)
    else :
        # Calculate the Hosmer-Lemeshow statistic (Chi-Square)
        chisq_value = sum(sum((np.array(o)-np.array(e))**2/np.array(e)))

    if np.isnan(chisq_value) == True :
        chisq_value = np.nan_to_num(chisq_value)

    # p-value of the Hosmer-Lemeshow statistic (Q-2 degrees of freedom)
    # A large value of Chi-squared (with small p-value < 0.05) indicates poor fit
    # A small Chi-squared values (with larger p-value closer to 1) indicate a good model fit
    pvalue = 1-chi2.cdf(chisq_value,Q-2)

    return(chisq_value, pvalue)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Hosmer-Lemeshow of the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def model_goodness_fit( best_model
                       ,model_prefix
                       ,model_name
                       ,tbl_repo
                       ,tbl_prefix
                        ) :
    """
    Parameters:
    -----------
    model : dict
        model
    model_prefix : list
        prefix of the model
    model_name : list
        name of the model
    tbl_repo : str
        file repository of the models
    tbl_prefix : str
        prefix name of the output
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Hosmer-Lemeshow of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_model = []
    ls_model_name = []
    ls_chisq = []
    ls_pvalue = []
    
    ls_auc_cv = []
    ls_auc_ho = []

    for j in range(len( list(best_model.keys()) )) :
        ml_prefix = list( best_model.keys() )[j]
        ml_name = model_name[ model_prefix.index(ml_prefix) ]
        ml_y = best_model[ml_prefix].y_validate
        ml_y_proba = best_model[ml_prefix].best_model_proba
        ml_y_preds = best_model[ml_prefix].best_model_preds
        chisq_value, pvalue = HosmerLemeshow(true_y = ml_y, proba_y = ml_y_proba, pred_y = ml_y_preds, Q = 10)

        auc_cv = best_model[ml_prefix].best_model_auc_test
        auc_ho = best_model[ml_prefix].best_model_auc_val

        ls_model.append( ml_prefix )
        ls_model_name.append( ml_name )
        ls_chisq.append( chisq_value )
        ls_pvalue.append( pvalue )

        ls_auc_cv.append( auc_cv )
        ls_auc_ho.append( auc_ho )
        
    
    df_good_fit = pd.DataFrame({'model': ls_model
                                ,'Model': ls_model_name
                                ,'AUC (CV)': ls_auc_cv
                                ,'AUC (Hold-out)': ls_auc_ho
                                ,'Hosmer-Lemeshow': ls_chisq
                                ,'p-value': ls_pvalue })
    
    # Save the output into HTML table
    df_good_fit.to_csv(tbl_repo+'Hosmer-Lemeshow_table'+tbl_prefix+'.csv')
    df_good_fit.to_html(tbl_repo+'Hosmer-Lemeshow_table'+tbl_prefix+'.html')




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Stability of the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def model_stability( model
                    ,best_model
                    ,model_prefix
                    ,model_name
                    ,model_colors
                    ,fig_repo
                    ,fig_prefix ) :
    """
    Parameters:
    -----------
    model : dict
        model
    best_model : dict
        best_model
    model_prefix : list
        prefix of the model
    model_name : list
        name of the model
    model_colors : list
        colours of the model
    fig_repo : str
        file repository of the models
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Stability of the best model   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Boxplot of the stability of the best model in each machine learning algorithm
    ds_box = pd.DataFrame()
    for j in range(len( list(best_model.keys()) )) :
        ml_prefix = list( best_model.keys() )[j]
        ml_name = model_name[ model_prefix.index(ml_prefix) ]
        box_stability = best_model[ml_prefix].rho_stability

        df_box = pd.DataFrame({ 'Algorithm': [ml_name]*len(box_stability), 'Stability': box_stability })
        ds_box = pd.concat([ds_box, df_box], axis = 0)

    sns.set_style("whitegrid")

    ax_best = sns.boxplot(y="Algorithm", x="Stability"
                         ,data=ds_box, palette=model_colors[0:ds_box['Algorithm'].unique().shape[0]]
                         ,width=0.5, orient="h"
                         ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                         ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    # Save the boxplot of the stability
    plt.savefig(fig_repo+'boxplot_stability_best_model'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()

    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Stability of the all model   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Boxplot of the stability of the best model in each machine learning algorithm
    ds_feature_selection = pd.DataFrame()
    ds_undersampling = pd.DataFrame()

    for j in range(len( list(model.keys()) )):
        ml_prefix_all = list( model.keys() )[j]
        ml_name_all = model_name[ model_prefix.index(ml_prefix_all) ]

        def func_model_stability(k):
            ls_box_data = dict()
            ml_model = model[ml_prefix_all][k]
            ml_model_all = ml_model[list(ml_model.keys())[0]]
            ml_model_all.permutation_importance()
            
            ls_box_data[ list(ml_model.keys())[0] ] = ml_model_all.rho_stability
            return(ls_box_data)
        
        ls_box_data_all = Parallel(n_jobs = n_jobs)(
            delayed( func_model_stability )(k = k)
            for k in range(len( model[ ml_prefix_all ] )) )

        list_box_name_all = []
        list_box_data_all = []

        list_none = []
        list_rfe = []
        list_forward = []
        list_weight = []
        list_boruta = []
        list_stepwise = []
        list_undersampling = []
        list_no_undersampling = []
        for k in range(len( ls_box_data_all )) :
            list_box_name_all.append( list(ls_box_data_all[k].keys())[0] )
            list_box_data_all.append( ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]] )

            if list(ls_box_data_all[k].keys())[0] == 'none' :
                list_none = list_none + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            elif 'rfe' in list(ls_box_data_all[k].keys())[0] :
                list_rfe = list_rfe + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            elif 'forward' in list(ls_box_data_all[k].keys())[0] :
                list_forward = list_forward + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            elif 'weight' in list(ls_box_data_all[k].keys())[0] :
                list_weight = list_weight + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            elif 'boruta' in list(ls_box_data_all[k].keys())[0] :
                list_boruta = list_boruta + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            elif 'stepwise' in list(ls_box_data_all[k].keys())[0] :
                list_stepwise = list_stepwise + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            else :
                pass

            if 'undersampling' in list(ls_box_data_all[k].keys())[0] :
                list_undersampling = list_undersampling + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
            else:
                list_no_undersampling = list_no_undersampling + ls_box_data_all[k][list(ls_box_data_all[k].keys())[0]]
        
        fs = ['No Feature Selection']*len(list_none)+\
             ['Recursive Feature Elimination']*len(list_rfe) + ['Forward Selection']*len(list_forward) + ['Importance Weights']*len(list_weight) +\
             ['Boruta SHAP']*len(list_boruta) + ['Stepwise Feature Selection']*len(list_stepwise)
        score = list_none + list_rfe + list_forward + list_weight + list_boruta + list_stepwise
        fs_algm = [ml_name_all]*len(score)

        undersamp = ['No Undersampling']*len(list_no_undersampling) + ['Undersampling']*len(list_undersampling)
        score_undersamp = list_no_undersampling + list_undersampling
        undersamp_algm = [ml_name_all]*len(score_undersamp)

        df_feature_selection = pd.DataFrame({'Feature Selection': fs, 'Algorithm': fs_algm, 'Stability': score})
        df_undersampling = pd.DataFrame({'Undersampling': undersamp, 'Algorithm': undersamp_algm, 'Stability': score_undersamp})

        ds_feature_selection = pd.concat([ds_feature_selection, df_feature_selection], axis = 0)
        ds_undersampling = pd.concat([ds_undersampling, df_undersampling], axis = 0)


    # Boxplot of stability by feature selection method
    ax_fs = sns.boxplot(y="Algorithm", x="Stability", hue="Feature Selection"
                        ,data=ds_feature_selection, palette=model_colors[0:ds_feature_selection['Feature Selection'].unique().shape[0]]
                        ,width=0.5, orient="h"
                        ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                        ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the stability
    plt.savefig(fig_repo+'boxplot_stability_feature_selection'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()
    

    # Boxplot of stability of undersampling method
    ax_undersamp = sns.boxplot(y="Algorithm", x="Stability", hue="Undersampling"
                              ,data=ds_undersampling, palette=model_colors[0:ds_undersampling['Undersampling'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the stability
    plt.savefig(fig_repo+'boxplot_stability_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - ROC-AUC of the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def model_roc_auc(model
                 ,best_model
                 ,model_prefix
                 ,model_name
                 ,model_colors
                 ,fig_repo
                 ,fig_prefix ) :
    """
    Parameters:
    -----------
    model : dict
        model
    best_model : dict
        best_model
    model_prefix : list
        prefix of the model
    model_name : list
        name of the model
    model_colors : list
        colours of the model
    fig_repo : str
        file repository of the models
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    ROC-AUC of the best model   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Boxplot of the ROC-AUC of the best model in each machine learning algorithm
    ds_box = pd.DataFrame()
    for j in range(len( list(best_model.keys()) )) :
        ml_prefix = list( best_model.keys() )[j]
        ml_name = model_name[ model_prefix.index(ml_prefix) ]
        box_auc_test = best_model[ml_prefix].best_model_auc_test
        box_auc_val = best_model[ml_prefix].best_model_auc_val

        df_box = pd.DataFrame({'Algorithm': [ml_name] * 2
                              ,'Validation': ['Cross-validation', 'Hold-out validation']
                              ,'AUC score': [box_auc_test, box_auc_val]
                              })
        ds_box = pd.concat([ds_box, df_box], axis = 0)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Bar chart of AUC score for each algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def show_values(axs, orient="v", space=.01):
        def _single(ax):
            if orient == "v":
                for p in ax.patches:
                    _x = p.get_x() + p.get_width() / 2
                    _y = p.get_y() + p.get_height() + (p.get_height()*0.01)
                    value = '{:.2f}%'.format(p.get_height())
                    ax.text(_x, _y, value, ha="center") 
            elif orient == "h":
                for p in ax.patches:
                    _x = p.get_x() + p.get_width() + float(space)
                    _y = p.get_y() + p.get_height() - (p.get_height()*0.2)
                    value = '{:.2f}%'.format(p.get_width())
                    ax.text(_x, _y, value, ha="left")

        if isinstance(axs, np.ndarray):
            for idx, ax in np.ndenumerate(axs):
                _single(ax)
        else:
            _single(axs)

    ds_bar_pct = ds_box.copy()
    ds_bar_pct['AUC score'] = ds_bar_pct['AUC score'] * 100
    ds_bar_pct.sort_values(['Validation', 'AUC score'], ascending = [True, False], inplace = True)

    sns.set_style("white")
    fig, ax = plt.subplots()
    bar_auc = sns.barplot(x = 'AUC score', y = 'Algorithm', hue = 'Validation'
                ,data = ds_bar_pct
                ,palette = 'Blues', edgecolor = 'w', ax = ax)
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    ax.set_xlim(0, 110)
    
    show_values(bar_auc, "h", space=0.01)
    
    # Save the bar chart of the AUC score
    plt.savefig(fig_repo+'bar_auc_best_model'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Line chart of AUC score for each algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # for j in range(len( model_prefix )) :
    #     sns.set_style("white")
    #     fig, ax = plt.subplots()

    #     df_auc_val = ds_box[(ds_box['Validation']=='Hold-out validation') & (ds_box['Algorithm']==model_name[j] )]
    #     df_auc_val = df_auc_val[ df_auc_val['AUC score'] == df_auc_val['AUC score'].max() ]

    #     df_auc_test = ds_box[(ds_box['Validation']=='Cross-validation') & (ds_box['Algorithm']==model_name[j] )]

    #     sns.lineplot(data=ds_box[(ds_box['Validation']=='Cross-validation') & (ds_box['Algorithm']==model_name[j] )], y="AUC score", x="Index", hue='Validation'
    #                 ,palette = [model_colors[0]], linestyle='dotted', legend = True)
    #     sns.lineplot(data=ds_box[(ds_box['Validation']=='Hold-out validation') & (ds_box['Algorithm']==model_name[j] )], y="AUC score", x="Index", hue='Validation'
    #                 ,palette = [model_colors[0]], legend = True)

    #     leg = ax.legend()
    #     leg_lines = leg.get_lines()
    #     leg_lines[0].set_linestyle(':')
    #     sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))

    #     ax.set_ylim(0, 1)

    #     # AUC score of validation and testing
    #     ax.plot(df_auc_val['Index'], df_auc_val['AUC score'], 'o', ms = 15, mec = model_colors[2], mfc = 'none', mew = 2)

    #     plt.text(55, 0.15
    #             ,'ROC-AUC score (Cross-validation): '+str(round(df_auc_test['AUC score'].tolist()[0], 2))
    #             ,horizontalalignment = 'left', size = 'small', color = 'black' )
    #     plt.text(55, 0.10
    #             ,'ROC-AUC score (Hold-out): '+str(round(df_auc_val['AUC score'].tolist()[0], 2))
    #             ,horizontalalignment = 'left', size = 'small', color = 'black' )
        
    #     # Create a Rectangle patch
    #     rect = Rectangle((53, 0.075), 51, 0.12, linewidth = 0.5, edgecolor = 'black', facecolor = 'none' )
    #     ax.add_patch(rect)

    #     # Save the boxplot of the stability
    #     plt.savefig(fig_repo+'line_auc_best_model'+fig_prefix+'_'+model_prefix[j]+'.png', bbox_inches='tight', dpi=300)
    #     plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Line chart of AUC score for undersampling    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Boxplot of the AUC score of the best model in each machine learning algorithm
    ds_feature_selection = pd.DataFrame()
    ds_undersampling = pd.DataFrame()
    ds_fs_undersamp = pd.DataFrame()

    for j in range(len( list(model.keys()) )) :
        ml_prefix_all = list( model.keys() )[j]
        ml_name_all = model_name[ model_prefix.index(ml_prefix_all) ]

        list_none = []
        list_rfe = []
        list_forward = []
        list_weight = []
        list_boruta = []
        list_stepwise = []
        list_undersampling = []
        list_no_undersampling = []

        list_fs_undersamp = []
        list_fs = []
        list_undersamp = []

        for k in range(len( model[ml_prefix_all] )) :
            ml_model = model[ml_prefix_all][k]
            ml_model_key = list(ml_model.keys())[0]
            ml_model_all = ml_model[ ml_model_key ]
            
            ml_model_auc_test = ml_model_all.all_model_auc_cv[ ml_model_all.all_model_auc_test.index( ml_model_all.best_model_auc_test ) ]

            list_val_test = ['Cross-validation']*len(ml_model_auc_test)

            if ml_model_key == 'none' :
                list_none = list_none + ml_model_auc_test
            elif 'rfe' in ml_model_key :
                list_rfe = list_rfe + ml_model_auc_test
            elif 'forward' in ml_model_key :
                list_forward = list_forward + ml_model_auc_test
            elif 'weight' in ml_model_key :
                list_weight = list_weight + ml_model_auc_test
            elif 'boruta' in ml_model_key :
                list_boruta = list_boruta + ml_model_auc_test
            elif 'stepwise' in ml_model_key :
                list_stepwise = list_stepwise + ml_model_auc_test
            else :
                pass

            if 'undersampling' in ml_model_key :
                list_undersampling = list_undersampling + ml_model_auc_test
            else :
                list_no_undersampling = list_no_undersampling + ml_model_auc_test
            
            if ('undersampling' in ml_model_key) & ( ('rfe' in ml_model_key) | ('forward' in ml_model_key) | ('weight' in ml_model_key) | ('boruta' in ml_model_key) | ('stepwise' in ml_model_key) ) :
                list_fs_undersamp = list_fs_undersamp + ml_model_auc_test
            elif 'undersampling' in ml_model_key :
                list_undersamp = list_undersamp + ml_model_auc_test
            elif ('rfe' in ml_model_key) | ('forward' in ml_model_key) | ('weight' in ml_model_key) | ('boruta' in ml_model_key) | ('stepwise' in ml_model_key) :
                list_fs = list_fs + ml_model_auc_test

        fs = ['No Feature Selection']*len(list_none) + ['Recursive Feature Elimination']*len(list_rfe) + ['Forward Selection']*len(list_forward) +\
             ['Importance Weights']*len(list_weight) + ['Boruta SHAP']*len(list_boruta) + ['Stepwise Feature Selection']*len(list_stepwise)
        score = list_none + list_rfe + list_forward + list_weight + list_boruta + list_stepwise
        group = list_val_test*int(len(list_none)/len(list_val_test)) +\
                list_val_test*int(len(list_rfe)/len(list_val_test)) + list_val_test*int(len(list_forward)/len(list_val_test)) +\
                list_val_test*int(len(list_weight)/len(list_val_test)) + list_val_test*int(len(list_boruta)/len(list_val_test)) +\
                list_val_test*int(len(list_stepwise)/len(list_val_test))
        fs_algm = [ml_name_all]*len(score)

        undersamp = ['No Undersampling']*len(list_no_undersampling) + ['Undersampling']*len(list_undersampling)
        score_undersamp = list_no_undersampling + list_undersampling
        group_undersamp = list_val_test*int(len(list_no_undersampling)/len(list_val_test)) + list_val_test*int(len(list_undersampling)/len(list_val_test))
        undersamp_algm = [ml_name_all]*len(score_undersamp)

        fs_undersamp = ['None']*len(list_none) + ['Feature Selection + Undersampling']*len(list_fs_undersamp) +\
                        ['Feature Selection']*len(list_fs) + ['Undersampling']*len(list_undersamp)
        score_fs_undersamp = list_none + list_fs_undersamp + list_fs + list_undersamp
        group_fs_undersamp = list_val_test*int(len(list_none)/len(list_val_test)) +\
                             list_val_test*int(len(list_fs_undersamp)/len(list_val_test)) +\
                             list_val_test*int(len(list_fs)/len(list_val_test)) +\
                             list_val_test*int(len(list_undersamp)/len(list_val_test))
        fs_undersamp_algm = [ml_name_all]*len(score_fs_undersamp)

        df_feature_selection = pd.DataFrame({'Feature Selection': fs, 'Algorithm': fs_algm, 'Cross-validation AUC score': score, 'Validation': group})
        df_undersampling = pd.DataFrame({'Undersampling': undersamp, 'Algorithm': undersamp_algm, 'Cross-validation AUC score': score_undersamp, 'Validation': group_undersamp})
        df_fs_undersamp = pd.DataFrame({'Method': fs_undersamp, 'Algorithm': fs_undersamp_algm, 'Cross-validation AUC score': score_fs_undersamp, 'Validation': group_fs_undersamp})

        ds_feature_selection = pd.concat([ds_feature_selection, df_feature_selection], axis = 0)
        ds_undersampling = pd.concat([ds_undersampling, df_undersampling], axis = 0)
        ds_fs_undersamp = pd.concat([ds_fs_undersamp, df_fs_undersamp], axis = 0)
    

    # Boxplot of AUC score by feature selection method
    sns.set_style("white")
    ax_fs = sns.boxplot(y="Algorithm", x="Cross-validation AUC score", hue="Feature Selection"
                        ,data=ds_feature_selection
                        ,palette=model_colors[0:ds_feature_selection['Feature Selection'].unique().shape[0]]
                        ,width=0.5, orient="h"
                        ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                        ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the AUC score
    plt.savefig(fig_repo+'boxplot_auc_feature_selection'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()
    

    # Boxplot of AUC score of undersampling method
    sns.set_style("white")
    ax_undersamp = sns.boxplot(y="Algorithm", x="Cross-validation AUC score", hue="Undersampling"
                              ,data=ds_undersampling
                              ,palette=model_colors[0:ds_undersampling['Undersampling'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the AUC score
    plt.savefig(fig_repo+'boxplot_auc_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()

    # Boxplot of AUC score of feature selection and undersampling method
    sns.set_style("white")
    ax_fs_undersamp = sns.boxplot(y="Algorithm", x="Cross-validation AUC score", hue="Method"
                              ,data=ds_fs_undersamp
                              ,palette=model_colors[0:ds_fs_undersamp['Method'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the AUC score
    plt.savefig(fig_repo+'boxplot_auc_feature_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()
        


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    ROC curve and Precision-Recall curve for each algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_name = []
    ls_fpr = []
    ls_tpr = []
    ls_auc = []
    ls_pr = []
    ls_rc = []

    for j in range(len(best_model.keys())) :
        roc_model = best_model[ list(best_model.keys())[j] ]
        roc_y_pred_proba = roc_model.best_model_y_pred_proba[:,1]
        roc_y_pred = roc_model.best_model_y_pred
        roc_y_test = roc_model.best_model_y_val
        roc_auc_val = roc_model.best_model_auc_val

        fpr, tpr, _ = roc_curve(roc_y_test, roc_y_pred_proba)

        pr, rc, _ = precision_recall_curve(roc_y_test, roc_y_pred_proba)

        ls_name.append(model_name[j])
        ls_fpr.append(fpr)
        ls_tpr.append(tpr)
        ls_auc.append(roc_auc_val)

        ls_pr.append(pr)
        ls_rc.append(rc)
    
    df_roc_curve = pd.DataFrame({'Algorithm': ls_name, 'fpr': ls_fpr, 'tpr': ls_tpr, 'AUC': ls_auc})
    df_roc_curve.sort_values(['AUC'], ascending = False, inplace = True)
    df_roc_curve = df_roc_curve.reset_index(drop = True)

    df_pr_rc = pd.DataFrame({'Algorithm': ls_name, 'Precision': ls_pr, 'Recall': ls_rc, 'AUC': ls_auc})
    df_pr_rc.sort_values(['AUC'], ascending = False, inplace = True)
    df_pr_rc = df_pr_rc.reset_index(drop = True)

    
    # Create ROC curve
    sns.set_style("white")
    for j in range(df_roc_curve.shape[0]) :
        if j == 0 :
            plt.plot(df_roc_curve['fpr'][j], df_roc_curve['tpr'][j]
                    ,color = model_colors[j]
                    ,label = df_roc_curve['Algorithm'][j] + ': ' + '{:.2f}'.format(round(df_roc_curve['AUC'][j], 2)) )
        else :
            plt.plot(df_roc_curve['fpr'][j], df_roc_curve['tpr'][j]
                    ,linestyle='dotted'
                    ,color = model_colors[j]
                    ,label = df_roc_curve['Algorithm'][j] + ': ' + '{:.2f}'.format(round(df_roc_curve['AUC'][j], 2)) )

    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.legend(loc = 4)
    
    plt.text(0.6, 0.4
            ,'ROC-AUC score (Hold-out)'
            ,horizontalalignment = 'left', size = 'small', color = 'black' )

    # Save the ROC-curve of the AUC score
    plt.savefig(fig_repo+'roc_curve'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    # Create Precision-Recall curve
    sns.set_style("white")
    for j in range(df_pr_rc.shape[0]) :
        if j == 0 :
            plt.plot(df_pr_rc['Recall'][j], df_pr_rc['Precision'][j]
                    ,color = model_colors[j]
                    ,label = df_pr_rc['Algorithm'][j] + ': ' + '{:.2f}'.format(round(df_pr_rc['AUC'][j], 2)) )
        else :
            plt.plot(df_pr_rc['Recall'][j], df_pr_rc['Precision'][j]
                    ,linestyle='dotted'
                    ,color = model_colors[j]
                    ,label = df_pr_rc['Algorithm'][j] + ': ' + '{:.2f}'.format(round(df_pr_rc['AUC'][j], 2)) )
        
    plt.ylabel('Precision')
    plt.xlabel('Recall')
    plt.legend(loc = 3)

    plt.text(-0.03, 0.5
            ,'ROC-AUC score (Hold-out)'
            ,horizontalalignment = 'left', size = 'small', color = 'black' )
    
    # Save the precision-recall curve
    plt.savefig(fig_repo+'pr_curve'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Hosmer-Lemeshow Goodness of fit    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # for j in range(len(model.keys())) :
    #     
    #     # Model
    #     ms_model = model[ list(model.keys())[j] ]
    # 
    #     # Model by method
    #     for m in range(len(ms_model)) :
    #         ms_mtd_mdl = ms_model[m]
    #         ms_mtd_name = list(ms_mtd_mdl.keys())[0]
    # 
    #         ms_mtd_model = ms_mtd_mdl[ms_mtd_name]
    #         ms_mtd_model.performance_metric()
    #         true_y = ms_mtd_model.best_model_y_val.tolist()
    #         proba_y = ms_mtd_model.best_model_y_pred_proba[:,1].tolist()
    #         pred_y = ms_mtd_model.best_model_y_pred
    # 
    #         hl_chi2, hl_pvalue = HosmerLemeshow(true_y = true_y, proba_y = proba_y)
    # 
    #         print(list(model.keys())[j] + ' - ' + ms_mtd_name + ': ' + str(hl_chi2) + ' - ' + str(hl_pvalue))

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Standard deviation of the AUC in cross-validation    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_sd_comb = pd.DataFrame()
    df_undersamp_fs_comb = pd.DataFrame()
    df_box_fs_undersamp_comb = pd.DataFrame()

    for j in range(len(model.keys())) :
        # Model
        sd_model = model[ list(model.keys())[j] ]
        sd_model_name = [model_name[i] for i in range(len(model_prefix)) if list(model.keys())[j] == model_prefix[i]][0]
        
        list_none = []
        list_undersamp = []
        list_fs = []
        list_undersamp_fs = []

        # Model by method
        df_sd = pd.DataFrame()
        for m in range(len(sd_model)) :
            sd_mtd_mdl = sd_model[m]
            sd_mtd_name = list(sd_mtd_mdl.keys())[0]

            sd_mtd_model = sd_mtd_mdl[sd_mtd_name]
            sd_auc_k = sd_mtd_model.all_model_auc_cv[ sd_mtd_model.all_model_auc_test.index(sd_mtd_model.best_model_auc_test) ]

            sd_mean = statistics.mean(sd_auc_k)
            sd_se = statistics.stdev(sd_auc_k)

            if sd_mtd_name == 'none' :
                sd_undersamp = ''
                sd_feature = ''

                list_none = list_none + sd_auc_k

            elif 'undersampling_boruta' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = "Boruta SHAP"

                list_undersamp_fs = list_undersamp_fs + sd_auc_k
            
            elif 'undersampling_stepwise' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = "Stepwise Feature Selection"

                list_undersamp_fs = list_undersamp_fs + sd_auc_k

            elif 'undersampling_rfe' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = 'Recursive feature elimination'

                list_undersamp_fs = list_undersamp_fs + sd_auc_k

            elif 'undersampling_weight' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = 'Importance weight'

                list_undersamp_fs = list_undersamp_fs + sd_auc_k

            elif 'none_undersampling' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = ''

                list_undersamp = list_undersamp + sd_auc_k

            elif 'none_rfe' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Recursive feature elimination'

                list_fs = list_fs + sd_auc_k

            elif 'none_weight' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Importance weight'

                list_fs = list_fs + sd_auc_k

            elif 'none_boruta' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Boruta SHAP'

                list_fs = list_fs + sd_auc_k
            
            elif 'none_stepwise' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Stepwise Feature Selection'

                list_fs = list_fs + sd_auc_k

            else :
                sd_undersamp = ''
                sd_feature = ''
            
            df_sd = pd.concat([df_sd, pd.DataFrame({'Algorithm': sd_model_name
                                                    ,'Undersampling': [sd_undersamp]
                                                    ,'Feature Selection': [sd_feature]
                                                    ,'Mean of AUC': [sd_mean]
                                                    ,'SD of AUC': [sd_se]}) ], axis = 0)
        df_sd_comb = pd.concat([df_sd_comb, df_sd], axis = 0)
        
        # Mean and standard error of the standard deviation of AUC score in cross-validation
        df_undersamp_fs = pd.DataFrame({'Algorithm': [sd_model_name, sd_model_name, sd_model_name, sd_model_name]
                                        ,'Method': ['None', 'Undersampling', 'Feature selection', 'Undersampling + Feature selection']
                                        ,'Mean of SD': [statistics.mean(list_none), statistics.mean(list_undersamp), statistics.mean(list_fs), statistics.mean(list_undersamp_fs)]
                                        ,'SE': [statistics.stdev(list_none), statistics.stdev(list_undersamp), statistics.stdev(list_fs), statistics.stdev(list_undersamp_fs)] })
        
        df_undersamp_fs_comb = pd.concat([df_undersamp_fs_comb, df_undersamp_fs], axis = 0)


        df_box_fs_undersamp = pd.DataFrame({'Algorithm': [sd_model_name] * (len(list_none)+len(list_undersamp)+len(list_fs)+len(list_undersamp_fs))
                                            ,'Method': ['None']*len(list_none) + ['Undersampling']*len(list_undersamp) + ['Feature selection']*len(list_fs) + ['Undersampling + Feature selection']*len(list_undersamp_fs)
                                            ,'Standard deviation of AUC score': list_none + list_undersamp + list_fs + list_undersamp_fs })
        df_box_fs_undersamp_comb = pd.concat([df_box_fs_undersamp_comb, df_box_fs_undersamp], axis = 0)

    df_sd_comb.to_html(fig_repo+'STD_AUC_tbl_'+fig_prefix+'_1.html')
    df_undersamp_fs_comb.to_html(fig_repo+'STD_AUC_tbl_'+fig_prefix+'_2.html')


    sns.set_style("white")
    ax_fs_undersamp = sns.boxplot(y="Algorithm", x="Standard deviation of AUC score", hue="Method"
                              ,data=df_box_fs_undersamp_comb
                              ,palette=model_colors[0:df_box_fs_undersamp_comb['Method'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the AUC score
    plt.savefig(fig_repo+'boxplot_std_auc_feature_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Deviance residual & Pearson residual of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_resid_comb = pd.DataFrame()
    df_miss_comb = pd.DataFrame()

    for j in range(len(best_model.keys())) :
        resid_ml_name = list(best_model.keys())[j]
        resid_model_name = [model_name[i] for i in range(len(model_prefix)) if resid_ml_name == model_prefix[i]][0]
        
        true_y = best_model[ resid_ml_name ].best_model_y_val
        pred_y = best_model[ resid_ml_name ].best_model_y_pred
        proba_y = best_model[ resid_ml_name ].best_model_y_pred_proba[:,1]

        df_resid = pd.DataFrame({'Algorithm': [resid_model_name]*len(true_y), 'true_y': true_y, 'proba_y': proba_y, 'pred_y': pred_y})
        df_resid['part_1'] = np.where( (df_resid['true_y'] == 0) | (df_resid['proba_y'] == 0), 0, np.log10(df_resid['true_y']/df_resid['proba_y']) )
        df_resid['part_2'] = np.where( (1-df_resid['true_y'] == 0) | (1-df_resid['proba_y'] == 0), 0, np.log10( (1-df_resid['true_y'])/(1-df_resid['proba_y']) ) )
        df_resid['part_3'] = np.where( (df_resid['true_y'] - df_resid['proba_y']) > 0, 1, -1 )
        df_resid['Deviance residual'] = np.sqrt( 2 * ( (df_resid['true_y']*df_resid['part_1']) + ((1-df_resid['true_y']) * df_resid['part_2']) ) ) * df_resid['part_3']

        df_resid['Pearson residual'] = np.where( (df_resid['true_y']-df_resid['proba_y']==0) | (df_resid['proba_y']==0) | (1-df_resid['proba_y']==0), 0
                                                , (df_resid['true_y']-df_resid['proba_y']) / np.sqrt( df_resid['proba_y'] * (1-df_resid['proba_y']) ) )
        
        df_resid['Missclassification'] = np.where( df_resid['true_y'] != df_resid['pred_y'], 1, 0 )

        df_resid.drop(['part_1', 'part_2', 'part_3'], axis = 1, inplace = True)

        df_resid_1 = df_resid[['Algorithm', 'Deviance residual']]
        df_resid_1['Residual'] = 'Deviance residuals'
        df_resid_1.rename(columns = {'Deviance residual': 'Residuals'}, inplace = True)

        df_resid_2 = df_resid[['Algorithm', 'Pearson residual']]
        df_resid_2['Residual'] = 'Pearson residuals'
        df_resid_2.rename(columns = {'Pearson residual': 'Residuals'}, inplace = True)

        df_resid_3 = df_resid[['Algorithm', 'Missclassification']]


        df_resid_comb = pd.concat([df_resid_comb, pd.concat([df_resid_1, df_resid_2], axis = 0) ], axis = 0)
        df_miss_comb = pd.concat([df_miss_comb, df_resid_3], axis = 0)
    

    sns.set_style("white")
    fig, ax = plt.subplots()
    plt.axvline(x = 0, color = '#ebebeb', linestyle = '-')
    ax_box_resid = sns.boxplot(y="Algorithm", x="Residuals", hue="Residual"
                              ,data=df_resid_comb
                              ,palette=model_colors[0:df_resid_comb['Residual'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_box_resid, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the residuals
    plt.savefig(fig_repo+'boxplot_residuals'+fig_prefix+'.png', bbox_inches='tight', dpi=300)

    ax.set_xlim(-5, 5)
    plt.savefig(fig_repo+'boxplot_residuals_cut_outlier'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    # Table of Deviance and Pearson residuals
    tbl_df_resid_mean = df_resid_comb.groupby(['Algorithm', 'Residual']).mean().reset_index()
    tbl_df_resid_mean.rename(columns = {'Residuals': 'Mean of residual'}, inplace = True)

    tbl_df_resid_std = df_resid_comb.groupby(['Algorithm', 'Residual']).std().reset_index()
    tbl_df_resid_std.rename(columns = {'Residuals': 'Standard deviation of residual'}, inplace = True)

    tbl_df_resid_comb = pd.merge(tbl_df_resid_mean, tbl_df_resid_std, on = ['Algorithm', 'Residual'], how = 'left')
    tbl_df_resid_comb.to_html(fig_repo+'STD_residual_tbl_'+fig_prefix+'.html')

    # Table of missclassification
    tbl_df_miss = df_miss_comb.groupby(['Algorithm']).mean().reset_index()
    tbl_df_miss.to_html(fig_repo+'Missclassification_tbl_'+fig_prefix+'.html')

    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dunn test of the residual - Post-hoc test for Kruskall-Wallis test    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Null hypothesis of Dunn test: There is no difference between groups
    # Alternative hypothesis of Dunn test: There is a difference between groups

    # dunn_model_name = []
    # ls_dunn_deviance = []
    # ls_dunn_pearson = []
    # for j in range( len(df_resid_comb['Algorithm'].unique()) ) :
        
    #     dunn_ml_name = df_resid_comb['Algorithm'].unique().tolist()[j]
    #     dunn_model_name.append( dunn_ml_name )

    #     # List of Deviance residuals
    #     ls_dunn_deviance.append( df_resid_comb[ (df_resid_comb['Algorithm'] == dunn_ml_name) & (df_resid_comb['Residual'] == 'Deviance residuals') ]['Residuals'].tolist() )

    #     # List of Pearson residuals
    #     ls_dunn_pearson.append( df_resid_comb[ (df_resid_comb['Algorithm'] == dunn_ml_name) & (df_resid_comb['Residual'] == 'Pearson residuals') ]['Residuals'].tolist() )

    # # Table of the algorithm name
    # tbl_ml_name = pd.DataFrame({'Algorithm': dunn_model_name})

    # # Dunn test of Deviance residuals
    # dunn_deviance_test = pd.DataFrame(data = sp.posthoc_dunn(ls_dunn_deviance, p_adjust = 'bonferroni')).reset_index()
    # dunn_deviance_test.drop(['index'], axis = 1, inplace = True)
    # dunn_deviance_test.columns = dunn_model_name
    # tbl_dunn_deviance = pd.concat([tbl_ml_name, dunn_deviance_test], axis = 1)
    # tbl_dunn_deviance.to_html(fig_repo+'Dunn_deviance_test_tbl'+fig_prefix+'.html')

    # # Dunn test of Pearson residuals
    # dunn_pearson_test = pd.DataFrame(data = sp.posthoc_dunn(ls_dunn_pearson, p_adjust = 'bonferroni')).reset_index()
    # dunn_pearson_test.drop(['index'], axis = 1, inplace = True)
    # dunn_pearson_test.columns = dunn_model_name
    # tbl_dunn_pearson = pd.concat([tbl_ml_name, dunn_pearson_test], axis = 1)
    # tbl_dunn_pearson.to_html(fig_repo+'Dunn_pearson_test_tbl'+fig_prefix+'.html')

    # ###  Heatmap of Dunn test of Deviance residuals   ###
    # dv_lbl = dunn_model_name
    # dv_lbl_num = [i+1 for i in range(len(dv_lbl))]
    # dv_tbl = tbl_dunn_deviance[dv_lbl]

    # dv_tbl_text = dv_tbl.copy()
    # for col_loop in dv_lbl :
    #     dv_tbl_text[col_loop] = np.where( dv_tbl_text[col_loop] < 0.001, '<0.001', dv_tbl_text[col_loop].round(3).astype('str') )
    # dv_text = np.array(dv_tbl_text)
    # for loop_row in range(dv_text.shape[0]) :
    #     for loop_col in range(dv_text.shape[1]) :
    #         if loop_row == loop_col :
    #             dv_text[loop_row, loop_col] = ''

    # sns.set_style("white")
    # fig, ax = plt.subplots()
    # im = ax.imshow(dv_tbl, cmap="Wistia")

    # # Show all ticks and label them with the respective list entries
    # ax.set_xticks(np.arange(len(dv_lbl)) )
    # ax.set_yticks(np.arange(len(dv_lbl)) )

    # ax.set_xticklabels(labels = dv_lbl_num)
    # ax.set_yticklabels(labels = dv_lbl)

    # # Create colorbar
    # cbar = ax.figure.colorbar(im, ax=ax)
    # cbar.ax.set_ylabel('', rotation=-90, va="top")
    # cbar.mappable.set_clim(0.0, 1.0)

    # textcolors=("grey", "black")
    # threshold = 0.05
    # # Loop over data dimensions and create text annotations.
    # for i in range(len(dv_lbl)):
    #     for j in range(len(dv_lbl)):
    #         text = ax.text(j, i, dv_text[i, j], # '{:.3f}'.format(dv_text[i, j]),
    #                     ha="center", va="center", color=textcolors[int(im.norm( np.array(dv_tbl)[i, j]) > threshold)] )
    # fig.tight_layout()

    # plt.savefig(fig_repo+'Heatmap_dunn_test_deviance'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    # plt.clf()


    # ###  Heatmap of Dunn test of Pearson residuals   ###
    # ps_lbl = dunn_model_name
    # ps_lbl_num = [i+1 for i in range(len(ps_lbl))]
    # ps_tbl = tbl_dunn_pearson[ps_lbl]

    # ps_tbl_text = ps_tbl.copy()
    # for col_loop in ps_lbl :
    #     ps_tbl_text[col_loop] = np.where( ps_tbl_text[col_loop] < 0.001, '<0.001', ps_tbl_text[col_loop].round(3).astype('str') )
    # ps_text = np.array(ps_tbl_text)
    # for loop_row in range(ps_text.shape[0]) :
    #     for loop_col in range(ps_text.shape[1]) :
    #         if loop_row == loop_col :
    #             ps_text[loop_row, loop_col] = ''

    # sns.set_style("white")
    # fig, ax = plt.subplots()
    # im = ax.imshow(ps_tbl, cmap="Wistia")

    # # Show all ticks and label them with the respective list entries
    # ax.set_xticks(np.arange(len(ps_lbl)) )
    # ax.set_yticks(np.arange(len(ps_lbl)) )

    # ax.set_xticklabels(labels = ps_lbl_num)
    # ax.set_yticklabels(labels = ps_lbl)

    # # Create colorbar
    # cbar = ax.figure.colorbar(im, ax=ax)
    # cbar.ax.set_ylabel('', rotation=-90, va="top")
    # cbar.mappable.set_clim(0.0, 1.0)

    # textcolors=("grey", "black")
    # threshold = 0.05
    # # Loop over data dimensions and create text annotations.
    # for i in range(len(ps_lbl)):
    #     for j in range(len(ps_lbl)):
    #         text = ax.text(j, i, ps_text[i, j], # '{:.3f}'.format(dv_text[i, j]),
    #                     ha="center", va="center", color=textcolors[int(im.norm( np.array(ps_tbl)[i, j]) > threshold)] )
    # fig.tight_layout()

    # plt.savefig(fig_repo+'Heatmap_dunn_test_pearson'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    # plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Performance metrics of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_auc = []
    ls_acc = []
    ls_f1 = []
    ls_pr = []
    ls_rc = []
    ls_mcc = []

    df_pm = pd.DataFrame()

    for j in range(len(best_model)) :
        pm_ml_name = list( best_model.keys() )[j]
        pm_model_name = [ model_name[i] for i in range(len(model_prefix)) if pm_ml_name == model_prefix[i] ][0]

        ls_auc.append( best_model[pm_ml_name].best_model_auc_val )
        ls_acc.append( best_model[pm_ml_name].best_model_accuracy )
        ls_f1.append( best_model[pm_ml_name].best_model_fscore )
        ls_pr.append( best_model[pm_ml_name].best_model_precision )
        ls_rc.append( best_model[pm_ml_name].best_model_recall )
        ls_mcc.append( best_model[pm_ml_name].best_model_mcc )

        ds_pm = pd.DataFrame({'Algorithm': [pm_model_name]
                            ,'ROC-AUC': [best_model[pm_ml_name].best_model_auc_val]
                            ,'Accuracy': [best_model[pm_ml_name].best_model_accuracy]
                            ,'F-score': [best_model[pm_ml_name].best_model_fscore]
                            ,'Precision': [best_model[pm_ml_name].best_model_precision]
                            ,'Recall': [best_model[pm_ml_name].best_model_recall]
                            ,'MCC': [best_model[pm_ml_name].best_model_mcc] })
        df_pm = pd.concat([df_pm, ds_pm], axis = 0)
    
    df_pm.to_html(fig_repo+'Performance_metric_tbl'+fig_prefix+'.html')

    df_pm.sort_values(by = 'ROC-AUC', ascending = False, inplace = True)

    # Heatmap of the performance metric
    pm_tbl = df_pm.copy()
    pm_tbl.drop(['Algorithm'], axis = 1, inplace = True)

    sns.set_style("white")
    fig, ax = plt.subplots()
    im = ax.imshow(pm_tbl, cmap="Wistia")

    # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(pm_tbl.shape[1]) )
    ax.set_yticks(np.arange(pm_tbl.shape[0]) )

    ax.set_xticklabels(labels = pm_tbl.columns.tolist() )
    ax.set_yticklabels(labels = df_pm['Algorithm'].tolist() )

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel('', rotation=-90, va="top")
    cbar.mappable.set_clim(0.0, 1.0)

    textcolors=("grey", "black")
    threshold = 0.5
    # Loop over data dimensions and create text annotations.
    for i in range( pm_tbl.shape[0] ):
        for j in range( pm_tbl.shape[1] ):
            text = ax.text(j, i, '{:.2f}'.format( np.array(pm_tbl)[i, j] ),
                        ha="center", va="center", color=textcolors[int(im.norm( np.array(pm_tbl)[i, j]) > threshold)] )
    fig.tight_layout()

    plt.savefig(fig_repo+'Heatmap_performance_metric'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()



def model_survival(input_data
                  ,best_model
                  ,fig_repo
                  ,fig_prefix
                  ,outcome_title
                  ,input_data_info #= data_info
                  ,vars_encoding = []
                  ,var_outcome = 'NP4DYSKI2'
                  ,strata_colors = ['#7E539C', '#0075B5', '#00AB9E', '#87C9AC', '#9BB531', '#FFDC60', '#F9AF1F', '#F59D9B'] ) :
    """
    Parameters
    ----------
    input_data : pd.DataFrame()
        input data
    input_data_info : pd.DataFrame()
        variables info of the input data
    best_model : pickle (model)
        model
    var_outcome : str
        variable of the outcome
    outcome_title : str
        name of the outcome (y-axis label of the KM plot)
    vars_encoding : list
        list of categorical variables to be encoded
    strata_colors : list
        list of the colours
    fig_repo : str
        file repository of the output
    fig_prefix : str
        prefix of the output name
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    The best model (highest AUC)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_ml_name = []
    ls_ml_auc = []
    for j in range(len(best_model)) :
        ml_name = list(best_model.keys())[j]
        ls_ml_name.append(ml_name)
        ls_ml_auc.append(best_model[ml_name].best_model_auc_val)

    best_ml_name = ls_ml_name[ ls_ml_auc.index(max(ls_ml_auc)) ]

    
    shap_var = best_model[best_ml_name].shap_values.feature_names
    shap_score = np.abs(best_model[best_ml_name].shap_values.values).mean(0)
    df_shap = pd.DataFrame({'shap_var': shap_var, 'shap_score': shap_score})
    df_shap = df_shap[df_shap['shap_score'] > 0]
    df_shap.sort_values(by=['shap_score'], ascending = False, inplace = True)

    vars_strata = df_shap['shap_var'].tolist()
    


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To define the time & event    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Variables of the outcome at baseline and follow-up visits
    vars_out = [var_outcome+'_ORI', var_outcome+'_Y1_ORI', var_outcome+'_Y2_ORI', var_outcome+'_Y3_ORI', var_outcome+'_Y4_ORI', var_outcome+'_Y5_ORI']

    # Data with time & event
    df_survival = input_data.copy()

    df_survival = df_survival[ ( ~np.isnan(df_survival[vars_out[0]]) ) | ( ~np.isnan(df_survival[vars_out[1]]) ) | ( ~np.isnan(df_survival[vars_out[2]]) ) | \
                                ( ~np.isnan(df_survival[vars_out[3]]) ) | ( ~np.isnan(df_survival[vars_out[4]]) ) | ( ~np.isnan(df_survival[vars_out[5]]) ) ]
    # df_survival = df_survival[ ( ~np.isnan(df_survival[vars_out[0]]) ) & ( ~np.isnan(df_survival[vars_out[1]]) ) & ( ~np.isnan(df_survival[vars_out[2]]) ) &\
    #                             ( ~np.isnan(df_survival[vars_out[3]]) ) & ( ~np.isnan(df_survival[vars_out[4]]) ) & ( ~np.isnan(df_survival[vars_out[5]]) ) ]
    df_survival['EVENT'] = np.where( df_survival[vars_out].sum(axis = 1) > 0, 1, 0 )
    df_survival['TIME'] = np.where( df_survival[vars_out[0]] > 0, 0
                                    ,np.where( df_survival[vars_out[1]] > 0, 1
                                                ,np.where( df_survival[vars_out[2]] > 0, 2
                                                            ,np.where( df_survival[vars_out[3]] > 0, 3
                                                                        ,np.where( df_survival[vars_out[4]] > 0, 4
                                                                                    ,np.where( df_survival[vars_out[5]] > 0, 5, np.nan ) ) ) ) ) )
    df_survival['TIME'] = np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[5]])), 5
                                    ,np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[4]])), 4
                                                ,np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[3]])), 3
                                                            ,np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[2]])), 2
                                                                        ,np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[1]])), 1
                                                                                    ,np.where( (df_survival['EVENT'] == 0) & (~np.isnan(df_survival[vars_out[0]])), 0, df_survival['TIME'] ) ) ) ) ) )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To derive the variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    df_survival['gen'] = np.where( df_survival['gen'] == 1, 'Male', np.where( df_survival['gen'] == 2, 'Female', '' ) )

    # Yes or No
    df_survival['fampd_old'] = np.where( df_survival['fampd_old'] == 1, 'Yes', np.where( df_survival['fampd_old'] == 2, 'No', '' ) )

    # Yes (>0) or No (=0)
    ls_vars_score = input_data_info[(input_data_info['Category'] == 'MDS-UPDRS') & (input_data_info['Type'] == 'Baseline') &
                                    (~input_data_info['Variable'].isin(['updrs1_score', 'updrs2_score', 'updrs3_score'
                                                                        ,'updrs3_score_on', 'updrs4_score', 'updrs_totscore'
                                                                        ,'updrs_totscore_on']))]['Variable'].tolist()
    for var_score in ls_vars_score :
        df_survival[var_score] = np.where( df_survival[var_score] > 0, 'Yes', np.where( df_survival[var_score] == 0, 'No', '' ) )
    
    # Stage 0 - 5
    df_survival['NHY'] = np.where( df_survival['NHY']==0, 'Stage 0'
                                ,np.where( df_survival['NHY']==1, 'Stage 1'
                                        ,np.where( df_survival['NHY']==2, 'Stage 2'
                                                ,np.where( (df_survival['NHY'] >= 3) & (df_survival['NHY'] <= 5), 'Stage 3-5', '' ) ) ) )

    # 1 = Yes, 0 = No
    ls_yn = ['rem_cat', 'LRRK2', 'GBA', 'levodopa', 'DXBRADY', 'DXTREMOR', 'DXRIGID', 'TMTACMPL']
    for var_yn in ls_yn :
        df_survival[var_yn] = np.where( df_survival[var_yn] == 0, 'No', np.where( df_survival[var_yn] == 1, 'Yes', '' ) )
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    One-hot encoding    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    if len(vars_encoding) > 0 :

        list_vars_encode = [x for x in df_survival.columns.tolist() if x in vars_encoding]

        df_survival_encode = input_data.copy()
        df_survival_encode = df_survival_encode[['PATNO']+list_vars_encode]

        for eLoop in range(len(list_vars_encode)) :
            df_survival_encode[ list_vars_encode[eLoop] ] = df_survival_encode[ list_vars_encode[eLoop] ].astype('Int32')
            df_survival_encode = pd.get_dummies(df_survival_encode, columns = [ list_vars_encode[eLoop] ], prefix = [ list_vars_encode[eLoop] ])
        
        for var_eLoop in list( set(df_survival_encode.columns.tolist()) - set(['PATNO']) ) :
            df_survival_encode[var_eLoop] = np.where( df_survival_encode[var_eLoop] == 1, 'Yes', np.where( df_survival_encode[var_eLoop] == 0, 'No', 'Missing' ) )
        
        df_survival = pd.merge(df_survival, df_survival_encode, on = ['PATNO'], how = 'left')


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Kaplan-Meier plot of feature of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # List of the variables
    list_vars_km = [ input_data_info[input_data_info['Description'] == loop]['Variable'].tolist()[0] for loop in vars_strata ]
    list_vars_km2 = [x for x in list_vars_km if type(df_survival[x][0]) == str ]
    list_vars_strata = [vars_strata[x] for x in range(len(list_vars_km)) if type(df_survival[list_vars_km[x]][0]) == str ]

    if len(list_vars_km2) >= 6 :
        list_vars_km2 = list_vars_km2[0:6]
        list_vars_strata = list_vars_strata[0:6]
    else :
        list_vars_km2 = list_vars_km2[0:len(list_vars_km2)]
        list_vars_strata = list_vars_strata[0:len(list_vars_km2)]

    # Set up subplot grid
    fig_nrow = math.ceil(len(list_vars_km2)/2)
    if len(list_vars_strata) > 0 :
        fig, axes = plt.subplots(nrows = fig_nrow, ncols = 2, 
                            sharex = True, sharey = True,
                            figsize=(20, 15) )
    else :
        fig, axes = plt.subplots()

    sns.set_style("white")
    
    if len(list_vars_strata) > 0 :
        ## Plot KM curve for each categorical variable
        def km_curves(feature, t='TIME', event='EVENT', df=df_survival, ax=None):
            for cat, loop in zip(sorted(df[feature].unique(), reverse=True), range(len(df[feature].unique())) ) :
                idx = df[feature] == cat
                kmf = KaplanMeierFitter(label=cat)
                kmf.fit(df[idx][t], event_observed=df[idx][event])
                kmf.plot(ax=ax, label=cat, ci_show=False, c=strata_colors[loop])
        
        # Loop over each variable to plot stratified survival curves and compare them by the log-rank test
        for cat, lbl, ax in zip(list_vars_km2, list_vars_strata, axes.flatten()):
            df_tmp_survival = df_survival[(df_survival[cat] != '')]
            if len(df_tmp_survival[cat].unique()) < 5 :
                km_curves(feature=cat, t='TIME', event='EVENT', df = df_tmp_survival, ax=ax)
                ax.legend(loc='lower left', prop=dict(size=22))  # size=48
                ax.set_title(lbl, pad=20, fontsize=22)  # fontsize=56
                p = multivariate_logrank_test(df_tmp_survival['TIME'], df_tmp_survival[cat], df_tmp_survival['EVENT'])
                if p.p_value < 0.001:
                    p_text = 'Log-rank test: <0.001'
                else:
                    p_text = 'Log-rank test: '+'{:.3f}'.format(p.p_value)
                ax.add_artist(AnchoredText( p_text, frameon=False, loc='upper right', prop=dict(size=20)))  # size=46
                ax.set_xlabel('Follow-up (years)', fontsize = 20)  # fontsize=40
                ax.set_ylabel('Probability of non-occurrence ('+outcome_title+')', fontsize = 20)  # fontsize=40
            else :
                kmf = KaplanMeierFitter(label=lbl)
                kmf.fit(df_tmp_survival['TIME'], event_observed=df_tmp_survival['EVENT'])
                kmf.plot(ax=ax, label='', ci_show=False, c=strata_colors[0])
                ax.set_title(lbl, pad=20, fontsize=22)  # fontsize=56
                ax.set_xlabel('Follow-up (years)', fontsize = 20)  # fontsize=40
                ax.set_ylabel('Probability of non-occurrence ('+outcome_title+')', fontsize = 20)  # fontsize=40
        
        ## Format subplots
        fig.subplots_adjust(wspace = 0.2, hspace = 0.2, bottom = 0.2)

        fig.tight_layout()
    
    fig.savefig(fig_repo+'KM_plot'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def model_features( model           # The model object
                   ,model_prefix    # The prefix of the model
                   ,model_name      # The name of the model
                   ,fig_features    # Feature names
                   ,fig_repo        # Repository of the output
                   ,fig_prefix      # The prefix name of the output
                   ) :
    
    model_keys = list( model.keys() )

    def model_features_loop_1(model_loop):

        model_key = model_keys[model_loop]
        model_key_name = [model_name[i] for i in range(len(model_prefix)) if model_prefix[i] == model_key][0]
        model_shaps = model[model_key]

        def model_features_loop_2(ml_loop):

            ml_shap = model_shaps[ml_loop]
            
            ml_shap_prefix = list(ml_shap.keys())[0]
            model_shap = ml_shap[ ml_shap_prefix ]

            print('SHAP of model :'+model_key_name+' -- '+ml_shap_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Validation data    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            # ds_validate = load_object(filename = 'validate'+model_shap.addon_Xtest, file_repo = model_shap.data_repo)

            # X_validate = ds_validate.drop([model_shap.vars_patid, model_shap.outcome], axis = 1)
            # y_validate = ds_validate[model_shap.outcome]

            X_validate = model_shap.X_validate
            y_validate = model_shap.y_validate

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    One-hot encoding on validation data    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            # if len(model_shap.vars_encoding) > 0 :

            #     list_val_vars_encoding = [x for x in X_validate.columns if x in model_shap.vars_encoding]

            #     for eLoop in range(len(list_val_vars_encoding)) :
                    
            #         X_validate[ list_val_vars_encoding[eLoop] ] = X_validate[ list_val_vars_encoding[eLoop] ].map(int)
            #         X_validate = pd.get_dummies(X_validate, columns = [ list_val_vars_encoding[eLoop] ], prefix = [ list_val_vars_encoding[eLoop] ])

            #     vars_ml_ana = model_shap.best_model_features
            #     col_X_validate = X_validate.columns.tolist()
            #     if len(list(set(vars_ml_ana) - set(col_X_validate))) > 0 :
            #         for col_encode in list(set(vars_ml_ana) - set(col_X_validate)) :
            #             X_validate[col_encode] = 0
            
            # else :
            #     vars_ml_ana = [x for x in model_shap.best_model_features if x in ds_validate.columns.tolist() ]

            # X_validate = X_validate[vars_ml_ana]

            try :
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    SHAP values of the best model    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                # Fits the explainer
                explainer = shap.Explainer(model_shap.best_model.predict, X_validate)

                # Calculate the SHAP values - It takes some time
                max_evals = X_validate.shape[1] * 2 + 2500
                shap_values = explainer(X_validate, max_evals = max_evals )

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Feature importance (SHAP values)    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                vals = np.abs(shap_values.values).mean(0)
                feature_importance = pd.DataFrame(list(zip(X_validate.columns.tolist(), vals)),
                                                columns=['col_name','feature_importance_vals'])
                feature_importance = feature_importance[feature_importance['feature_importance_vals'] != 0]
                feature_importance.sort_values(by=['feature_importance_vals'], ascending=False, inplace=True)
                feature_importance = feature_importance.reset_index().drop(['index'], axis = 1)

                idx_shap = [idx for idx in range(len(vals)) if vals[idx] > 0]
                shap_values.values = shap_values.values[:, idx_shap]
                shap_values.data = shap_values.data[:, idx_shap]
                shap_values.feature_names = [shap_values.feature_names[x] for x in idx_shap]

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Feature names    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                ml_shap_name = [data_rename['Rename'][i] for i in range(data_rename.shape[0]) if data_rename['Name'][i] == ml_shap_prefix ][0]

                ls_features = pd.DataFrame({'Variable': shap_values.feature_names, ml_shap_name: np.abs(shap_values.values).mean(0)})
                ls_features_variable = ls_features['Variable'].tolist()
                for i_fv in range(30) :
                    ls_features_variable = [ x.replace('_'+str(i_fv), '') if '_'+str(i_fv) in x else x for x in ls_features_variable ]
                ls_features['Variable'] = ls_features_variable
                ls_features = ls_features.groupby(['Variable']).sum().reset_index()
            
            except Exception :
                
                ml_shap_name = [data_rename['Rename'][i] for i in range(data_rename.shape[0]) if data_rename['Name'][i] == ml_shap_prefix ][0]
                ls_features = pd.DataFrame({'Variable': X_validate.columns.tolist()
                                            ,ml_shap_name: [0 for xx in range(X_validate.shape[1])] })

            ls_auc = pd.DataFrame({'Variable': ['AUC score'], 'Description': ['AUC score'], ml_shap_name: [format(model_shap.best_model_auc_val, '.3f')] })

            # if ml_loop == 0 :
            #     df_auc = ls_auc
            #     df_features = pd.merge( fig_features, ls_features, how = 'left', on = ['Variable'] )
            # else :
            #     df_auc = pd.merge(df_auc, ls_auc, how = 'left', on = ['Variable', 'Description'])
            #     df_features = pd.merge( df_features, ls_features, how = 'left', on = ['Variable'] )

            print('[DONE] SHAP of model :'+model_key_name+' -- '+ml_shap_prefix)
            
            return( {'ls_auc': ls_auc
                     ,'ls_features': ls_features} )

        
        ds_features = Parallel(n_jobs = 1)(
            delayed( model_features_loop_2 )(ml_loop = ml_loop)
            for ml_loop in range(len(model_shaps)) )
        
        for ml_loop_p in range(len(ds_features)) :
            if ml_loop_p == 0 :
                df_auc = ds_features[ml_loop_p]['ls_auc']
                df_features = pd.merge( fig_features, ds_features[ml_loop_p]['ls_features'], how = 'left', on = ['Variable'] )
            else :
                df_auc = pd.merge(df_auc, ds_features[ml_loop_p]['ls_auc'], how = 'left', on = ['Variable', 'Description'])
                df_features = pd.merge( df_features, ds_features[ml_loop_p]['ls_features'], how = 'left', on = ['Variable'] )
        
        df_features['sum'] = round(df_features.mean(axis = 1), 5)
        df_features['count'] = round(df_features.drop(['Variable', 'Description', 'sum'], axis = 1).count(axis = 1), 0)
        df_features['mean'] = round(df_features['sum'] / df_features['count'], 4)
        df_features['total'] = len(model_shaps)
        df_features.sort_values(['count'], ascending = False, inplace = True)
        df_features = df_features[df_features['count'] > 0]
        df_features.reset_index(drop = True, inplace = True)

        df_auc_features = pd.concat([df_auc, pd.DataFrame().reindex_like(df_auc), df_features], axis = 0)
        df_auc_features.drop(['Variable'], axis = 1, inplace = True)
        df_auc_features.rename(columns = {'Description': ' '}, inplace = True)
        df_auc_features.reset_index(drop = True, inplace = True)

        # Save the output into HTML table
        df_features.to_csv(fig_repo+'SHAP_features_table'+fig_prefix+'_'+model_key+'.csv')
        df_features.to_html(fig_repo+'SHAP_features_table'+fig_prefix+'_'+model_key+'.html')

        df_auc.to_csv(fig_repo+'AUC_table'+fig_prefix+'_'+model_key+'.csv')
        df_auc.to_html(fig_repo+'AUC_table'+fig_prefix+'_'+model_key+'.html')

        df_auc_features.to_csv(fig_repo+'AUC_SHAP_features_table'+fig_prefix+'_'+model_key+'.csv')
        df_auc_features.to_html(fig_repo+'AUC_SHAP_features_table'+fig_prefix+'_'+model_key+'.html')

        ls_features_comb = df_features[['Variable', 'sum', 'count', 'total']]
        ls_features_comb[model_key_name] = round(df_features['sum'] / df_features['count'], 4)
        ls_features_comb.rename(columns = {'sum': 'sum_'+model_key, 'count': 'count_'+model_key, 'total': 'total_'+model_key}, inplace = True)

        return(ls_features_comb)

    njobs_loop1 = len(model)
    ds_features_comb = Parallel(n_jobs = njobs_loop1)(
        delayed( model_features_loop_1 )(model_loop = model_loop)
        for model_loop in range(len(model)) )
    
    for model_loop_p in range(len(ds_features_comb)) :
        if model_loop_p == 0 :
            df_features_comb = pd.merge(fig_features, ds_features_comb[model_loop_p], how = 'left', on = ['Variable'] )
        else :
            df_features_comb = pd.merge(df_features_comb, ds_features_comb[model_loop_p], how = 'left', on = ['Variable'])
    
    df_features_comb['sum'] = df_features_comb[['sum_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['count'] = df_features_comb[['count_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['total'] = df_features_comb[['total_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['pct'] = df_features_comb['count'] / df_features_comb['total'] * 100
    df_features_comb['mean'] = df_features_comb['sum'] / df_features_comb['count']
    df_features_comb.sort_values(['count'], ascending = False, inplace = True)
    df_features_comb = df_features_comb[df_features_comb['mean'] > 0]
    df_features_comb.reset_index(drop = True, inplace = True)

    df_features_comb.to_csv(fig_repo+'AUC_SHAP_features_table_comb'+fig_prefix+'.csv')
    df_features_comb.to_html(fig_repo+'AUC_SHAP_features_table_comb'+fig_prefix+'.html')



def features_combining( repo_prefix
                       ,fig_repo
                       ,fig_repo_out
                       ,fig_outcome
                       ,fig_prefix
                       ,fig_features
                       ,list_ml_models
                       ,cutoff
                       ):
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the overall selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_overall = fig_features
    for repo_prefix_loop in repo_prefix :
        ds_features_overall = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'AUC_SHAP_features_table_comb'+fig_prefix+'.html' )[0]
        ds_features_overall = ds_features_overall[['Variable', 'Description', 'pct']]
        ds_features_overall.rename(columns = {'pct': repo_prefix_loop.replace('/', '')}, inplace = True)
        
        df_features_overall = pd.merge(df_features_overall, ds_features_overall, how = 'left', on = ['Variable', 'Description'])
    
    df_features_overall = df_features_overall[ df_features_overall.sum(axis = 1) > 0 ]
    # df_features_overall['Average of %'] = df_features_overall.sum(axis=1) / len(repo_prefix)
    df_features_overall['Average of %'] = df_features_overall.mean(axis = 1)
    df_features_overall.sort_values(by = ['Average of %'], ascending = False, inplace = True)
    df_features_overall.reset_index(drop = True, inplace = True)

    df_features_overall.to_csv(fig_repo_out+'Cross_cohort_average_features_table'+fig_prefix+'.csv')
    df_features_overall.to_html(fig_repo_out+'Cross_cohort_average_features_table'+fig_prefix+'.html')


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_best = fig_features
    df_features_best_comb = fig_features
    for repo_prefix_loop in repo_prefix :

        list_features_auc = []
        for list_ml_models_loop in list_ml_models :
            ds_features_best_auc = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'AUC_SHAP_features_table'+fig_prefix+'_'+list_ml_models_loop+'.html' )[0]
            ds_features_best_auc = ds_features_best_auc[ ds_features_best_auc['Unnamed: 1'] == 'AUC score' ]
            ds_features_best_auc.drop(['sum', 'count', 'mean', 'total'], axis = 1, inplace = True)
            ds_features_best_auc_2 = pd.DataFrame(ds_features_best_auc.max()).reset_index()
            features_auc = ds_features_best_auc.max(axis = 1).tolist()[0]
            list_features_auc.append(features_auc)
            best_method = ds_features_best_auc_2[ ds_features_best_auc_2[0] == features_auc ]['index'].tolist()[0]

            ds_features_best = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'SHAP_features_table'+fig_prefix+'_'+list_ml_models_loop+'.html' )[0]
            ds_features_best = ds_features_best[['Variable', 'Description', best_method]]
            ds_features_best.rename(columns = {best_method: list_ml_models_loop}, inplace = True)

            df_features_best = pd.merge(df_features_best, ds_features_best, how = 'left', on = ['Variable', 'Description'])
    
        df_features_best = df_features_best[ df_features_best.sum(axis = 1) > 0 ]
        best_algmn = list_ml_models[ list_features_auc.index( max(list_features_auc) ) ]
        df_features_best = df_features_best[['Variable', 'Description', best_algmn]]
        df_features_best[best_algmn] = np.where( df_features_best[best_algmn] > cutoff, df_features_best[best_algmn], np.NaN )
        df_features_best.rename(columns = {best_algmn: repo_prefix_loop.replace('/', '') }, inplace = True)
    
        df_features_best_comb = pd.merge(df_features_best_comb, df_features_best, how = 'left', on = ['Variable', 'Description'] )
    
    df_features_best_comb['count'] = df_features_best_comb[[x.replace('/', '') for x in repo_prefix]].count(axis = 1)
    df_features_best_comb = df_features_best_comb[ df_features_best_comb['count'] > 1 ]
    df_features_best_comb.sort_values(by = ['count', 'Description'], ascending = [False, True], inplace = True)
    df_features_best_comb.reset_index(drop = True, inplace = True)

    df_features_best_comb.to_csv(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+'.csv')
    df_features_best_comb.to_html(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+'.html')




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - One-hot encoding    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_one_hot_encoding(ds_Xtrain, ds_Xtest, vars_encode = []) :

    if len(vars_encode) > 0 :

        list_vars_encoding = [x for x in ds_Xtrain.columns if x in vars_encode]

        for eLoop in range(len(list_vars_encoding)) :
            
            ds_Xtrain[ list_vars_encoding[eLoop] ] = ds_Xtrain[ list_vars_encoding[eLoop] ].map(int)
            ds_Xtrain = pd.get_dummies(ds_Xtrain, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])

            ds_Xtest[ list_vars_encoding[eLoop] ] = ds_Xtest[ list_vars_encoding[eLoop] ].map(int)
            ds_Xtest = pd.get_dummies(ds_Xtest, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])
            
        # Encoded variables that are missed in ds_Xtest
        col_Xtrain = ds_Xtrain.columns.tolist()
        col_Xtest = ds_Xtest.columns.tolist()
        col_encode_add = list(set(col_Xtrain) - set(col_Xtest))

        if len(col_encode_add) > 0 :
            for col_add in col_encode_add :
                ds_Xtest[col_add] = 0
            
        else :
            pass

        ds_Xtest = ds_Xtest[col_Xtrain]
    
    return( ds_Xtrain, ds_Xtest )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - AUC deviation by cohorts    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class cohort_auc_deviation:

    def __init__(self
                 ,model
                 ,fig_repo = ''
                 ,fig_prefix = '' ) :
        
        self.model = model
        self.best_model = model.best_model
        self.ds_validate = load_object(filename = 'validate', file_repo = model.data_repo)
        self.ds_training = load_object(filename = 'training', file_repo = model.data_repo)
        self.ls_cohort = ['LUXPARK' if 'LUXPARK' in x else 'PPMI' if 'PPMI' in x else 'ICEBERG' for x in self.ds_validate['PATNO'] ]

        vars_keep = [x for x in self.ds_validate.columns if x in model.vars_ml]
        _, ds_X = func_one_hot_encoding(ds_Xtrain = self.ds_training[vars_keep], ds_Xtest = self.ds_validate[vars_keep], vars_encode = model.vars_encoding)
        self.ds_X = ds_X[model.best_model_features]
        self.ds_y = self.ds_validate[model.outcome]

        self.fig_repo = fig_repo
        self.fig_prefix = fig_prefix


    def score_deviation(self) :

        model_preds = self.best_model.predict(self.ds_X)
        model_auc = roc_auc_score(self.ds_y, model_preds, average = 'weighted')

        ls_cohort = np.unique(self.ls_cohort).tolist()
        
        # The included cohort
        list_auc_score_cohort = []
        ds_pred_proba_comb = pd.DataFrame()
        for incl_loop in ls_cohort :
            cohort_included = [x for x in range(len(self.ls_cohort)) if self.ls_cohort[x] in incl_loop]
            X_validate_ds = self.ds_X[ self.ds_X.index.isin(cohort_included) ]
            y_validate_ds = self.ds_y[ self.ds_y.index.isin(cohort_included) ]

            # Predicted outcome
            preds_y = self.best_model.predict(X_validate_ds)
            preds_proba_y = self.best_model.predict_proba(X_validate_ds)[:,1].tolist()
            
            # Data frame of the included cohort and predicted probability of the outcome
            ds_pred_proba = pd.DataFrame({'Cohort': [incl_loop] * len(preds_proba_y), 'Predicted Outcome': preds_proba_y, 'Actual Outcome': y_validate_ds})
            ds_pred_proba_comb = pd.concat([ds_pred_proba_comb, ds_pred_proba], axis = 0)

            # AUC score
            list_auc_score_cohort.append( roc_auc_score(y_validate_ds, preds_y, average = 'weighted') )
        
        # Data frame of the included cohort and AUC score
        ds_auc_deviation = pd.DataFrame({'Cohort': ls_cohort, 'AUC score': list_auc_score_cohort, 'AUC deviation': list_auc_score_cohort - model_auc})
        ds_auc_deviation.sort_values(by = ['AUC deviation'], ascending = [False], inplace = True)
        ds_auc_deviation['AUC deviation'] = [format(x, '.3f') for x in ds_auc_deviation['AUC deviation']]
        ds_auc_deviation['AUC deviation'] = ds_auc_deviation['AUC deviation'].astype(float)

        ds_pred_proba_comb = pd.merge(ds_pred_proba_comb, ds_auc_deviation[['Cohort', 'AUC deviation']], how = 'left', on = ['Cohort'])
        ds_pred_proba_comb.sort_values(by = 'AUC deviation', ascending = False, inplace = True)

        # Multiple plots
        fig, axs = plt.subplots(ncols = 2)
        # Adjust the subplot layout parameters
        fig.subplots_adjust(hspace=0.125, wspace=0.125)

        # Bar plot
        sns.set_style("white")
        # fig, ax = plt.subplots()
        cols = ['#19A864' if (x > 0) else '#EF4F83' for x in ds_auc_deviation['AUC deviation'].tolist() ]
        sns.barplot(data = ds_auc_deviation, x = 'AUC deviation', y = 'Cohort', orient='h', palette=cols, ax = axs[0])
        axs[0].bar_label(axs[0].containers[0], label_type = 'edge', padding = -50)
        plt.legend([], [], frameon=False)

        # Beeswarm plot of the actual & predicted outcome
        sns.set_style("white")
        # fig, ax = plt.subplots()
        beeplot = sns.swarmplot(data = ds_pred_proba_comb, x = "Predicted Outcome", y = "Cohort", hue = "Actual Outcome", ax = axs[1])
        beeplot.axvline(0.5)
        plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
        plt.ylabel('')
        beeplot.set(yticklabels=[])
        plt.savefig(self.fig_repo+'AUC_deviation'+self.fig_prefix+'.png', bbox_inches='tight', dpi=300)
        plt.clf()



#****************************************************************************************************************************************************************************

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function to generate table with AUC score    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def generate_tables_auc(influential = False):

    for loop_ml_prefix in ml_prefix :

        tbl_auc_overall_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_auc_none_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_auc_norm_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_auc_influential_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})

        for loop_file_repo in file_repo_prefix_loop :

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_classifier = dict()
            best_ml_model = dict()

            ds_auc_overall_comb = pd.DataFrame()
            ds_auc_none_comb = pd.DataFrame()
            ds_auc_norm_comb = pd.DataFrame()
            ds_auc_influential_comb = pd.DataFrame()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_classifier[ ls_ml_models[ml_loop] ] = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                # Overall AUC score
                max_avg_auc = best_ml_model[ ls_ml_models[ml_loop] ].best_model_auc_test
                idx_max_avg_auc = best_ml_model[ ls_ml_models[ml_loop] ].all_model_auc_test.index(max_avg_auc)
                sd_auc = statistics.stdev(best_ml_model[ ls_ml_models[ml_loop] ].all_model_auc_cv[idx_max_avg_auc])
                holdout_auc = best_ml_model[ ls_ml_models[ml_loop] ].best_model_auc_val
                cohort_name = ' (' + file_repo_prefix.replace('/', '') + ')'

                ds_auc_overall = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                               ,'Average AUC'+cohort_name: [max_avg_auc]
                                               ,'SD AUC'+cohort_name: [sd_auc]
                                               ,'Hold-out AUC'+cohort_name: [holdout_auc] })
                ds_auc_overall_comb = pd.concat([ds_auc_overall_comb, ds_auc_overall], axis = 0)

                # AUC score by methods
                model_combine = model_classifier[ ls_ml_models[ml_loop] ]

                model_keys = []
                model_combine_auc = []
                for none_loop in range(len(model_combine)) :
                    model_keys += list(model_combine[none_loop].keys())
                    model_combine_auc += [model_combine[none_loop][model_keys[none_loop]].best_model_auc_test]

                # AUC score without normalization
                model_none_keys = [i for i in range(len(model_keys)) if 'none' in model_keys[i]]

                if len(model_none_keys) > 0 :
                    model_auc_none = [model_combine_auc[i] for i in model_none_keys]
                    max_auc_none = max(model_auc_none)
                    idx_model_none = model_none_keys[ model_auc_none.index(max_auc_none) ]
                    model_none_method = model_keys[ idx_model_none ]
                    model_none = model_combine[idx_model_none][model_none_method]
                    sd_none_auc = statistics.stdev( model_none.all_model_auc_cv[ model_none.all_model_auc_test.index(model_none.best_model_auc_test) ] )

                    ds_auc_none = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                                ,'Method'+cohort_name: [model_none_method]
                                                ,'Average AUC'+cohort_name: [model_none.best_model_auc_test]
                                                ,'SD AUC'+cohort_name: [sd_none_auc]
                                                ,'Hold-out AUC'+cohort_name: [model_none.best_model_auc_val]
                                                })
                    ds_auc_none_comb = pd.concat([ds_auc_none_comb, ds_auc_none], axis = 0)
                else :
                    pass

                # AUC score with normalization
                model_norm_keys = [i for i in range(len(model_keys)) if 'none' not in model_keys[i]]

                if len(model_norm_keys) > 0 :
                    model_auc_norm = [model_combine_auc[i] for i in model_norm_keys]
                    max_auc_norm = max(model_auc_norm)
                    idx_model_norm = model_norm_keys[ model_auc_norm.index(max_auc_norm) ]
                    model_norm_method = model_keys[ idx_model_norm ]
                    model_norm = model_combine[idx_model_norm][model_norm_method]
                    sd_norm_auc = statistics.stdev( model_norm.all_model_auc_cv[ model_norm.all_model_auc_test.index(model_norm.best_model_auc_test) ] )

                    ds_auc_norm = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                                ,'Method'+cohort_name: [model_norm_method]
                                                ,'Average AUC'+cohort_name: [model_norm.best_model_auc_test]
                                                ,'SD AUC'+cohort_name: [sd_norm_auc]
                                                ,'Hold-out AUC'+cohort_name: [model_norm.best_model_auc_val] })
                    ds_auc_norm_comb = pd.concat([ds_auc_norm_comb, ds_auc_norm], axis = 0)
                
                else :
                    pass

                if influential == True :
                    best_ml_model_inf = load_object(filename = "ml_model_"+ml_model_prefix+"_influential", file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    
                    idx_max_auc = best_ml_model_inf.all_model_auc_test.index( best_ml_model_inf.best_model_auc_test )
                    sd_auc = statistics.stdev(best_ml_model_inf.all_model_auc_cv[idx_max_auc])

                    ds_auc_influential = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                                       ,'Average AUC'+cohort_name: [best_ml_model_inf.best_model_auc_test]
                                                       ,'SD AUC'+cohort_name: [sd_auc]
                                                       ,'Hold-out AUC'+cohort_name: [best_ml_model_inf.best_model_auc_val]
                                                       })
                    ds_auc_influential_comb = pd.concat([ds_auc_influential_comb, ds_auc_influential], axis = 0)

                else :
                    pass

            
            # Combine the tables from different cohort
            tbl_auc_overall_comb = pd.merge(tbl_auc_overall_comb, ds_auc_overall_comb, how = 'left', on = ['Algorithm'])
            tbl_auc_overall_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Overall'+loop_ml_prefix+'.csv')
            tbl_auc_overall_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Overall'+loop_ml_prefix+'.html')

            if len(model_none_keys) > 0 :
                tbl_auc_none_comb = pd.merge(tbl_auc_none_comb, ds_auc_none_comb, how = 'left', on = ['Algorithm'])
                tbl_auc_none_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Without_Normalization'+loop_ml_prefix+'.csv')
                tbl_auc_none_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Without_Normalization'+loop_ml_prefix+'.html')
            else :
                pass

            if len(model_norm_keys) > 0 :
                tbl_auc_norm_comb = pd.merge(tbl_auc_norm_comb, ds_auc_norm_comb, how = 'left', on = ['Algorithm'])
                tbl_auc_norm_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Normalization'+loop_ml_prefix+'.csv')
                tbl_auc_norm_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Normalization'+loop_ml_prefix+'.html')
            else :
                pass

            if influential == True :
                tbl_auc_influential_comb = pd.merge(tbl_auc_influential_comb, ds_auc_influential_comb, how = 'left', on = ['Algorithm'])
                tbl_auc_influential_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Influential'+loop_ml_prefix+'.csv')
                tbl_auc_influential_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_AUC_Influential'+loop_ml_prefix+'.html')
            else :
                pass









#****************************************************************************************************************************************************************************


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_models_comparision() :

    model_classifier_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_classifier_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_classifier = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_classifier[ ls_ml_models[ml_loop] ] = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            
            model_classifier_load[loop_ml_prefix] = model_classifier
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_classifier_comb[loop_file_repo] = model_classifier_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    
    return({'model_classifier' : model_classifier_comb
            ,'best_ml_model' : best_ml_model_comb })





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function to generate outputs for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def generate_outputs_comparison(ds_load) :

    list_perm = []
    for loop_file_repo in file_repo_prefix_loop :
        for loop_ml_prefix in ml_prefix :
                list_perm.append( [loop_file_repo] + [loop_ml_prefix] )
    
    def gen_outputs(loop) :

        file_repo_prefix = list_perm[loop][0]
        loop_ml_prefix = list_perm[loop][1]
        print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        model_classifier = ds_load['model_classifier'][ list_perm[loop][0] ][ list_perm[loop][1] ]
        best_ml_model = ds_load['best_ml_model'][ list_perm[loop][0] ][ list_perm[loop][1] ]

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    AUC of the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        ml_colors = ['#7E539C', '#0075B5', '#00AB9E', '#87C9AC', '#9BB531', '#FFDC60', '#F9AF1F', '#F59D9B', '#744700']
        model_roc_auc(model = model_classifier, best_model = best_ml_model, model_prefix = ls_ml_models, model_name = ls_ml_model_name, model_colors = ml_colors
                    ,fig_repo = file_repo+file_repo_prefix+set_outcome+'/Comparison/', fig_prefix = loop_ml_prefix )
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Categorical variables to be encoded    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        vars_category = data_variables[data_variables['Variable Type'].isin(['Ordinal', 'Nominal'])]['PPMI Variable']
        vars_category = [ x for x in vars_category if x in data_info[data_info['Type'] == 'Baseline']['Variable'].tolist() ]

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Kaplan-Meier plot    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if file_repo_prefix == 'LUXPARK/' :
            km_df = data_cohort_luxpark
        elif file_repo_prefix == 'PPMI/' :
            km_df = data_cohort_ppmi
        elif file_repo_prefix == 'ICEBERG/' :
            km_df = data_cohort_iceberg
        else :
            km_df = data_cohort_comb

        ml_colors = ['#7E539C', '#0075B5', '#00AB9E', '#87C9AC', '#9BB531', '#FFDC60', '#F9AF1F', '#F59D9B', '#744700']
        # model_survival(input_data = km_df, best_model = best_ml_model, input_data_info = data_info, var_outcome = set_outcome_baseline, strata_colors = ml_colors
        #                 ,vars_encoding = vars_category
        #                 ,fig_repo = file_repo+file_repo_prefix+set_outcome+'/Comparison/', fig_prefix = loop_ml_prefix
        #                 ,outcome_title = 'LID')

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Selected features    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        model_features(model = model_classifier
                    ,model_prefix = ls_ml_models
                    ,model_name = ls_ml_model_name
                    ,fig_features = data_info[['Variable', 'Description']]
                    ,fig_repo = file_repo+file_repo_prefix+set_outcome+'/Features by Methods/'
                    ,fig_prefix = loop_ml_prefix )
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    AUC deviation    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if file_repo_prefix in ['ALL/'] :
            list_ml_auc = []
            for ml_auc in list(best_ml_model.keys()) :
                list_ml_auc.append( best_ml_model[ml_auc].best_model_auc_test )
            
            max_ml_auc = max(list_ml_auc)
            idx_ml_auc = list_ml_auc.index(max_ml_auc)
            best_ml_auc = best_ml_model[ list(best_ml_model.keys())[idx_ml_auc] ]

            deviation = cohort_auc_deviation(model = best_ml_auc, fig_repo = file_repo+file_repo_prefix+set_outcome+'/', fig_prefix = loop_ml_prefix)
            deviation.score_deviation()

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Goodness of fit    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        model_goodness_fit(best_model = best_ml_model
                            ,model_prefix = ls_ml_models
                            ,model_name = ls_ml_model_name
                            ,tbl_repo = file_repo+file_repo_prefix+set_outcome+'/Comparison/'
                            ,tbl_prefix = loop_ml_prefix )
        
        print('[DONE] Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)



    njobs_gen_outputs = len(list_perm)

    Parallel(n_jobs = njobs_gen_outputs)(
        delayed( gen_outputs )(loop = loop)
        for loop in range(len(list_perm)) )


##############################################################################################################################################################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_models_avg_cv_comparison(ana_type = "") :

    model_vars_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_vars_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_vars = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                # if ana_type == "" :
                #     # List of 'model_name', 'num_vars', 'model_avg_auc', 'model_selected_features', 'idx_optimal' of the algorithm
                #     model_vars[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                #     # Model with highest average AUC score
                #     best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                if (ana_type == "_unnormalized") & (loop_file_repo in file_repo_prefix_multi) :
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_auc']

                    # Unnormalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_auc'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_auc': [ml_loaded['model_avg_auc'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected
                    
                    # Optimal unnormalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select
                
                elif (ana_type == "_normalized") & (loop_file_repo in file_repo_prefix_multi):
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_auc']

                    # Normalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_auc'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_auc': [ml_loaded['model_avg_auc'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected

                    # Optimal Normalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select
                
                else :
                    # List of 'model_name', 'num_vars', 'model_avg_auc', 'model_selected_features', 'idx_optimal' of the algorithm
                    model_vars[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                    # Model with highest average AUC score
                    best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            

            model_vars_load[loop_ml_prefix] = model_vars
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_vars_comb[loop_file_repo] = model_vars_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    
    return({'model_vars' : model_vars_comb
            ,'best_ml_model' : best_ml_model_comb })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected of the optimal compact model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def avg_cv_features_combining( repo_prefix
                              ,fig_repo_out
                              ,fig_prefix
                              ,fig_prefix_add
                              ,fig_features
                              ,list_ml_models
                               ):
     
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_best = fig_features.copy()
    df_features_info = pd.DataFrame({'Variable': ['avg_score', 'num_vars', 'algorithm']
                                     ,'Description': ['Average score in CV', 'Number of features', 'Algorithm'] })
    
    df_table_score = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })
    df_table_method = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })

    df_features_optimal_model = fig_features.copy()

    df_feats_perm_importance = fig_features.copy()

    ls_ds_optimal_features_cohort = dict()
    
    for repo_prefix_loop in repo_prefix[0]+repo_prefix[1] :

        ls_optimal_model_name = []
        ls_optimal_model_nvars = []
        ls_optimal_model_important = []
        ls_optimal_avg_auc = []
        ls_optimal_sd_cv = []
        ls_ds_optimal_features = []
        ls_optimal_holdout = []

        # Optimal model within algorithms
        ls_features_mtd = []
        ls_number_mtd = []

        ls_cnt_feats_train = []
        ls_cnt_vars = []
        ls_selected_features = []

        ls_ds_optimal_features_cohort[repo_prefix_loop] = dict()

        for list_ml_models_loop in list_ml_models :
            
            ls_model_vars = ds_perm_avg_cv_compare['model_vars'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]
            ls_best_ml_model = ds_perm_avg_cv_compare['best_ml_model'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]

            # The method of the optimal model (highest average AUC score in cross-validation)
            idx_optimal = ls_model_vars['model_avg_auc'].index( max(ls_model_vars['model_avg_auc']) )

            optimal_model_name = ls_model_vars['model_name'][ idx_optimal ]
            optimal_model_nvars = ls_model_vars['num_vars'][ idx_optimal ]
            optimal_avg_auc = ls_model_vars['model_avg_auc'][ idx_optimal ]
            # optimal_sd_cv = statistics.stdev(ls_best_ml_model.all_model_auc_cv[ ls_best_ml_model.all_model_auc_test.index( max(ls_best_ml_model.all_model_auc_test) ) ])
            optimal_sd_cv = statistics.stdev(ls_best_ml_model.all_model_auc_cv[0]['kfold_auc'])
            optimal_holdout = ls_best_ml_model.best_model_auc_val

            #### List of features
            # num_vars = ls_model_vars['num_vars'][ idx_optimal ]
            # model_features = ls_model_vars['model_selected_features'][ idx_optimal ]
            perm = abs(permutation_importance(ls_best_ml_model.best_model
                                              ,pd.concat([ls_best_ml_model.X_training, ls_best_ml_model.X_validate], axis = 0)
                                              ,pd.concat([ls_best_ml_model.y_training, ls_best_ml_model.y_validate], axis = 0)
                                              ,scoring = 'balanced_accuracy'
                                              ,random_state = ls_best_ml_model.seed_number).importances_mean)
            
            # Features with predictive impact in the model
            model_features = [ ls_model_vars['model_selected_features'][ idx_optimal ][x] for x in range(len(perm)) if perm[x] != 0 ]
            num_vars = len(model_features)

            # Permutation importance of the features
            model_features_important = [ perm[x] for x in range(len(perm)) if perm[x] != 0 ]
            model_features_tmp = [x.replace('_0','').replace('_1','').replace('_2','').replace('_3','').replace('_4','')\
                                              .replace('_5','').replace('_6','').replace('_7','').replace('_8','').replace('_9','')\
                                                for x in model_features]
            df_model_features_important = pd.DataFrame({'Variable':model_features_tmp, 'perm_important': model_features_important})
            df_model_features_important['rank'] = df_model_features_important['perm_important'].abs().rank(ascending=False)
            
            # Features with predictive impact in the model
            model_selected_features = [x.replace('_0', '').replace('_1', '').replace('_2', '').replace('_3', '').replace('_4', '').replace('_5', '').replace('_6', '').replace('_7', '').replace('_8', '').replace('_9', '') for x in model_features ]
            model_selected_features = np.unique(model_selected_features).tolist()
            
            # The features of the optimal model for each cohort
            kfold_candidates = ls_best_ml_model.all_model_auc_cv[0]['kfold_candidates']
            model_features_cv = []
            for kk in range(len(kfold_candidates)) :
                unique_kfold_candidates = [x.replace('_0', '').replace('_1', '').replace('_2', '').replace('_3', '').replace('_4', '').replace('_5', '').replace('_6', '').replace('_7', '').replace('_8', '').replace('_9', '') for x in kfold_candidates[kk] ]
                unique_kfold_candidates = np.unique(unique_kfold_candidates).tolist()
                model_features_cv += unique_kfold_candidates
            
            frequency_dict = {}
            for element in model_features_cv:
                if element in frequency_dict:
                    frequency_dict[element] += 1
                else:
                    frequency_dict[element] = 1
            # Percentage of selected features in each cross-validation
            # ds_optimal_features = pd.DataFrame({'Variable': list(frequency_dict.keys()), repo_prefix_loop: [x/ls_best_ml_model.k_loop*100 for x in list(frequency_dict.values()) ] })
            ds_optimal_features = pd.DataFrame({'Variable': list(frequency_dict.keys()), repo_prefix_loop: [x/ls_best_ml_model.k_loop*100 for x in list(frequency_dict.values()) ] })

            # The selected features for each cohort
            for mtd_loop in range(len(ls_model_vars['model_name'])) :
                # List of selected features
                ls_features_mtd += np.unique([x.replace('_0','').replace('_1','').replace('_2','').replace('_3','').replace('_4','')\
                                              .replace('_5','').replace('_6','').replace('_7','').replace('_8','').replace('_9','')\
                                                for x in ls_model_vars['model_selected_features'][mtd_loop]]).tolist()
            cnt_vars = Counter(ls_features_mtd)
            ls_number_mtd += [len(ls_model_vars['model_name'])]
        
            # Append the list
            ls_optimal_model_name.append( optimal_model_name )
            # ls_optimal_model_nvars.append( optimal_model_nvars )
            ls_optimal_model_nvars.append( num_vars )               # Number of predictive features (based on permutation importance)
            ls_optimal_model_important.append( df_model_features_important ) # List of predictors and permutation importance
            ls_optimal_avg_auc.append( optimal_avg_auc )
            ls_optimal_sd_cv.append( optimal_sd_cv )
            ls_ds_optimal_features.append( ds_optimal_features )
            ls_optimal_holdout.append( optimal_holdout )
            ls_cnt_vars.append( cnt_vars )                          # Number of features (count)
            ls_cnt_feats_train.append( len(perm) )                  # Number of features trained
            ls_selected_features.append(model_selected_features)

            ls_ds_optimal_features_cohort[repo_prefix_loop][list_ml_models_loop] = ds_optimal_features


        #### Optimal model (highest average AUC score in cross-validation)
        # Index of ls_optimal_model_nvars with minimal number of features
        idx_model = ls_optimal_avg_auc.index( max(ls_optimal_avg_auc) )

        ml_model_nvars = ls_optimal_model_nvars[idx_model]
        ml_avg_auc = ls_optimal_avg_auc[idx_model]
        ml_features = ls_ds_optimal_features[idx_model]
        ml_info = pd.DataFrame({'Description': ['Average score in CV', 'Number of features', 'Algorithm']
                                     ,repo_prefix_loop: [ml_avg_auc, ml_model_nvars, list_ml_models[idx_model]] })
        
        # Selected features of the optimal model
        opt_ml_selected_features = pd.DataFrame({'Variable': ls_selected_features[idx_model], repo_prefix_loop: [1 for x in range(len(ls_selected_features[idx_model]))] })
        df_features_optimal_model = pd.merge(df_features_optimal_model, opt_ml_selected_features, how = 'left', on = ['Variable'])
        
        # The data frame listed the features of the optimal model
        df_features_best = pd.merge(df_features_best, ml_features, how = 'left', on = ['Variable'])
        df_features_info = pd.merge(df_features_info, ml_info, how = 'left', on = ['Description'])

        # Summary table of the average score, hold-out score and method for each algorithm
        df_table_score = pd.merge(df_table_score, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_auc + [ ls_optimal_avg_auc[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cv + [ ls_optimal_sd_cv[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                }), how = 'left', on = ['Algorithm'] )
        
        df_table_method = pd.merge(df_table_method, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                  ,repo_prefix_loop.replace('/', '_')+'method': ls_optimal_model_name + [ ls_optimal_model_name[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_auc + [ ls_optimal_avg_auc[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cv + [ ls_optimal_sd_cv[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_predictive': ls_optimal_model_nvars + [ ls_optimal_model_nvars[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_train' : ls_cnt_feats_train + [ ls_cnt_feats_train[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_unique_predictive': [len(x) for x in ls_selected_features] + [ len(ls_selected_features[idx_model]) ]
                                                                  }), how = 'left', on = ['Algorithm'] )
        
        # Ranking of the predictors based on permutation importance
        df_feats_perm_importance1 = ls_optimal_model_important[idx_model]
        df_feats_perm_importance1 = df_feats_perm_importance1.groupby('Variable')['rank'].mean().reset_index()
        df_feats_perm_importance2 = pd.merge(fig_features, df_feats_perm_importance1, on = ['Variable'], how = 'left')
        df_feats_perm_importance2 = df_feats_perm_importance2[df_feats_perm_importance2['rank']>=0]
        df_feats_perm_importance2.rename(columns = {'rank': repo_prefix_loop}, inplace = True)
        df_feats_perm_importance = pd.merge(df_feats_perm_importance, df_feats_perm_importance2, on = ['Variable', 'Description'], how = 'left')
        
    # To summarize the average of feature selection in CV
    ml_features_algorithm = fig_features.copy()
    for repo_prefix_loop in repo_prefix[0]+repo_prefix[1] :

        ds_algm = fig_features.copy()
        for list_ml_models_loop in list_ml_models :
            ls_features_algm = ls_ds_optimal_features_cohort[repo_prefix_loop][list_ml_models_loop]
            ls_features_algm = ls_features_algm.rename(columns={repo_prefix_loop:str(list_ml_models_loop)})
            ds_algm = pd.merge(ds_algm, ls_features_algm,on = ['Variable'], how = 'left')
        
        # Average the average across algorithms
        ds_algm = ds_algm.fillna(0)
        ds_algm['avg_'+repo_prefix_loop] = ds_algm[list_ml_models].mean(axis = 1)
        ds_algm.drop(list_ml_models, axis = 1, inplace = True)
        ds_algm.rename(columns = {'avg_'+repo_prefix_loop: repo_prefix_loop}, inplace = True)
        ds_algm = ds_algm[ ds_algm[repo_prefix_loop] != 0 ]

        ml_features_algorithm = pd.merge(ml_features_algorithm, ds_algm, on = ['Variable', 'Description'], how = 'left')

    # Average across cohort
    ml_features_algorithm = ml_features_algorithm.fillna(0)
    ml_features_algorithm['avg_single'] = ml_features_algorithm[repo_prefix[0]].mean(axis = 1)
    ml_features_algorithm['avg_all'] = ml_features_algorithm[repo_prefix[1]].mean(axis = 1)
    ml_features_algorithm = ml_features_algorithm[ (ml_features_algorithm['avg_single'] != 0) | (ml_features_algorithm['avg_all'] != 0) ]
    ml_features_algorithm.sort_values(by = ['avg_single', 'avg_all'], ascending = False, inplace = True)
    ml_features_algorithm.reset_index(drop = True, inplace = True)

    
    # The data listed features of the optimal model, average score in CV, number of features and algorithm across cohort analyses
    ml_features_comb = pd.concat([df_features_info, df_features_best], axis = 0)
    ml_features_comb['count_single'] = ml_features_comb[[x for x in repo_prefix[0] ]].count(axis = 1)   # Count for single cohort analyses
    ml_features_comb['count_all'] = ml_features_comb[[x for x in repo_prefix[0]+repo_prefix[1] ]].count(axis = 1)            # Count for all the cohort analyses
    ml_features_comb = ml_features_comb[ ml_features_comb['count_all'] > 0 ]
    ml_features_comb['avg_single'] = ml_features_comb[[x for x in repo_prefix[0] ]].sum(axis = 1)                           # Average % for single cohort analyses
    ml_features_comb['avg_single'] = pd.to_numeric(ml_features_comb['avg_single'], errors='coerce') / len(repo_prefix[0])
    ml_features_comb['avg_all'] = ml_features_comb[[x for x in repo_prefix[0]+repo_prefix[1] ]].sum(axis = 1)               # Average % for all the cohort analyses
    ml_features_comb['avg_all'] = pd.to_numeric(ml_features_comb['avg_all'], errors='coerce') / len(repo_prefix[0]+repo_prefix[1])
    
    ml_features_comb['avg_single'] = np.where(ml_features_comb['Variable'].isin(['algorithm', 'avg_score', 'num_vars']), 200, ml_features_comb['avg_single'])
    ml_features_comb['avg_all'] = np.where(ml_features_comb['Variable'].isin(['algorithm', 'avg_score', 'num_vars']), 200, ml_features_comb['avg_all'])

    ml_features_comb.sort_values(by=['avg_single', 'count_single', 'avg_all', 'count_all'], ascending=False, inplace = True)
    ml_features_comb.reset_index(drop = True, inplace = True)

    # The data listed the features with predictive impact in the optimal model
    df_features_optimal_model['count_single'] = df_features_optimal_model[[x for x in repo_prefix[0] ]].count(axis = 1)                # Count for single cohort analyses
    df_features_optimal_model['count_all'] = df_features_optimal_model[[x for x in repo_prefix[0]+repo_prefix[1] ]].count(axis = 1)    # Count for all the cohort analyses
    df_features_optimal_model = df_features_optimal_model[ df_features_optimal_model['count_all'] > 0 ]
    df_features_optimal_model.sort_values(by='count_single', ascending=False, inplace = True)
    df_features_optimal_model.reset_index(drop = True, inplace = True)
    
    # Save the table into .csv and .html
    df_table_score.to_csv(fig_repo_out+'Table_AUC_Overall'+fig_prefix+fig_prefix_add+'.csv')
    df_table_score.to_html(fig_repo_out+'Table_AUC_Overall'+fig_prefix+fig_prefix_add+'.html')

    df_table_method.to_csv(fig_repo_out+'Table_AUC_Overall_Method'+fig_prefix+fig_prefix_add+'.csv')
    df_table_method.to_html(fig_repo_out+'Table_AUC_Overall_Method'+fig_prefix+fig_prefix_add+'.html')

    df_features_optimal_model.to_csv(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+fig_prefix_add+'.csv')
    df_features_optimal_model.to_html(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+fig_prefix_add+'.html')

    ml_features_comb.to_csv(fig_repo_out+'Cross_cohort_features_CV_table'+fig_prefix+fig_prefix_add+'.csv')
    ml_features_comb.to_html(fig_repo_out+'Cross_cohort_features_CV_table'+fig_prefix+fig_prefix_add+'.html')

    ml_features_algorithm.to_csv(fig_repo_out+'Cross_cohort_features_CV_table_algorithms'+fig_prefix+fig_prefix_add+'.csv')
    ml_features_algorithm.to_html(fig_repo_out+'Cross_cohort_features_CV_table_algorithms'+fig_prefix+fig_prefix_add+'.html')

    return({'df_feats_perm_importance': df_feats_perm_importance        # Rank of the predictors based on permutation importance
            })