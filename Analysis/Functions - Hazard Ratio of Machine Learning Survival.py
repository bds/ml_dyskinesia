#~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load Packages    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from lifelines.statistics import logrank_test
import matplotlib as mpl
mpl.rc('figure', max_open_warning = 0)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To Train the Survival Model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_surv_model_shap_hr(X_data, y_data, smodel_set_hr, best_model_max_features, seed_number):
    # To train the model
    if smodel_set_hr == 'rsf' :
        bootstrap_smodel = RandomSurvivalForest(max_depth = best_model_max_features, n_estimators = 20, n_jobs = 2, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'gboost' :
        bootstrap_smodel = GradientBoostingSurvivalAnalysis(max_depth = best_model_max_features, n_estimators = 20, learning_rate=1.0, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'cwgboost' :
        bootstrap_smodel = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=1.0, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'coxph' :
        bootstrap_smodel = CoxPHSurvivalAnalysis(alpha = best_model_max_features)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'coxnet' :
        bootstrap_smodel = CoxnetSurvivalAnalysis(l1_ratio = best_model_max_features, alpha_min_ratio=0.01, fit_baseline_model = True)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'extra' :
        bootstrap_smodel = ExtraSurvivalTrees(max_depth = best_model_max_features, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'fastsvm' :
        bootstrap_smodel = FastSurvivalSVM(max_iter = 1000, tol = 1e-5, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)
    
    elif smodel_set_hr == 'naive_svm' :
        bootstrap_smodel = NaiveSurvivalSVM(alpha = best_model_max_features, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)

    elif smodel_set_hr == 'surv_tree' :
        bootstrap_smodel = SurvivalTree(max_depth = best_model_max_features, random_state = seed_number)
        bootstrap_smodel.fit(X_data, y_data)

    else :
        pass

    return bootstrap_smodel


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Hazards Ratio Measurement (Split based on log-rank test) + KM Plot    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def surv_logrank(model, ds_X, ds_y, cols_exclude, year_limit = 6) :

    # Build a list of Hazards Ratio indexing by j columns
    colIndexes = ds_X.shape[1]
    modelHR = dict()
    HR_test = dict()
    HR_pval = dict()
    KM_plot = dict()

    med_time1 = dict()
    lower_ci_median_time1 = dict()
    upper_ci_median_time1 = dict()
    med_time2 = dict()
    lower_ci_median_time2 = dict()
    upper_ci_median_time2 = dict()

    # To set the year limit
    year_limit_index = [x for x in range(len(ds_y[set_outcome[0]])) if ds_y[set_outcome[0]][x] <= year_limit]
    ds_X = ds_X.iloc[ year_limit_index, : ].reset_index(drop = True)
    ds_y = ds_y[ year_limit_index ]
    
    for j in range( colIndexes ) :
        print('colIndexes : '+str(j)+' '+ds_X.columns[j])
        curCol = ds_X.columns[j]

        # if continuous, find the threshold with largest log-rank value
        if( ds_X[ curCol ].unique().shape[0] >= 2 ) :
            
            # Potential threshold for the variable
            if (ds_X[ curCol ].unique().shape[0] > 2) :
                q10 = np.percentile(ds_X[ curCol ], 10).round().astype('int')
                q90 = np.percentile(ds_X[ curCol ], 90).round().astype('int')
            else :
                q10 = ds_X[ curCol ].min().round().astype('int')
                q90 = ds_X[ curCol ].max().round().astype('int')

            ls_col_int = [ x for x in range( q10, q90+1 ) ]

            if (len(ls_col_int) == 1) & (ls_col_int[0] == ds_X[ curCol ].min() ) :
                ls_col_int = [ min( [x for x in ds_X[ curCol ].unique() if x != ls_col_int[0] ] ) ]
            
            ls_logrank = []
            pv_logrank = []

            ls_var_index_1 = []
            ls_var_index_2 = []

            for loop_logrank in ls_col_int :
                # Time and status of group 1 and group 2
                list_var = ds_X[curCol].tolist()

                # Index and values of group 1
                var_index_1 = [ x for x in range(len(list_var)) if list_var[x] < loop_logrank ]
                ls_var_index_1.append(var_index_1)

                time_group1 = ds_y[var_index_1][set_outcome[0]]
                event_group1 = ds_y[var_index_1][set_outcome[1]].astype('int')

                # Index and values of group 2
                var_index_2 = [ x for x in range(len(list_var)) if list_var[x] >= loop_logrank ]
                ls_var_index_2.append(var_index_2)

                time_group2 = ds_y[var_index_2][set_outcome[0]]
                event_group2 = ds_y[var_index_2][set_outcome[1]].astype('int')

                # Perform the log-rank test
                # logrank_test = logrank_test(durations_A = time_group1, event_observed_A = event_group1, durations_B = time_group2, event_observed_B = event_group2 )
                logrank = logrank_test(durations_A = time_group1, event_observed_A = event_group1, durations_B = time_group2, event_observed_B = event_group2 ).test_statistic
                pval_lr = logrank_test(durations_A = time_group1, event_observed_A = event_group1, durations_B = time_group2, event_observed_B = event_group2 ).p_value
                ls_logrank.append( logrank )
                pv_logrank.append( pval_lr )
            
            # Threshold with the largest logrank
            cutoff_var = ls_col_int[ ls_logrank.index( max(ls_logrank) ) ]
            logrank_stats = ls_logrank[ ls_logrank.index( max(ls_logrank) ) ]
            logrank_pval = pv_logrank[ ls_logrank.index( max(ls_logrank) ) ]

        else :
            cutoff_var = ds_X[ curCol ].max()
            logrank_stats = np.nan
            logrank_pval = np.nan
        
        # Label for Kaplan-Meier Plot
        km_label = pd.DataFrame(data_info_all[ data_info_all['Variable'] == curCol ])['Description'].tolist()[0]

        # Kaplan-Meier Plot

        # Labels
        if( ds_X[ curCol ].unique().shape[0] > 2 ) :
            label_km1 = km_label+r'$ \geq $'+str(cutoff_var)    # >= cutoff_var
            label_km2 = km_label+' < '+str(cutoff_var)          # < cutoff_var
        else:
            label_km1 = km_label+' = Yes'                       # == 1
            label_km2 = km_label+' = No'                        # == 0

        if len(ds_X[curCol].unique()) > 1 :
            plt.figure()
            fig = plt.figure(figsize=(12, 5), linewidth = 0.5) #, edgecolor = 'black')
            plt.style.context("seaborn-white")
            plt.axes().set_facecolor('#F8F8F7')
            
            if cutoff_var != 0 :
                if ( len(ds_y[ds_X[curCol] >= cutoff_var ]) > 3 ) & ( len(ds_y[ds_X[curCol] < cutoff_var ]) > 3 ) :
                    kmf_true = 1
                    kmf1 = KaplanMeierFitter()
                    kmf1.fit(durations = ds_y[ds_X[curCol] >= cutoff_var ][set_outcome[0]]
                            ,event_observed = ds_y[ds_X[curCol] >= cutoff_var ][set_outcome[1]]
                            ,label = label_km1 )

                    kmf2 = KaplanMeierFitter()
                    kmf2.fit(durations = ds_y[ds_X[curCol] < cutoff_var ][set_outcome[0]]
                            ,event_observed = ds_y[ds_X[curCol] < cutoff_var ][set_outcome[1]]
                            ,label = label_km2 )
                else :
                    kmf_true = 0
            else :
                if ( len(ds_y[ds_X[curCol] > cutoff_var ]) > 3 ) & ( len(ds_y[ds_X[curCol] <= cutoff_var ]) > 3 ) :
                    kmf_true = 1
                    kmf1 = KaplanMeierFitter()
                    kmf1.fit(durations = ds_y[ds_X[curCol] > cutoff_var ][set_outcome[0]]
                            ,event_observed = ds_y[ds_X[curCol] > cutoff_var ][set_outcome[1]]
                            ,label = label_km1 )

                    kmf2 = KaplanMeierFitter()
                    kmf2.fit(durations = ds_y[ds_X[curCol] <= cutoff_var ][set_outcome[0]]
                            ,event_observed = ds_y[ds_X[curCol] <= cutoff_var ][set_outcome[1]]
                            ,label = label_km2 )
                else :
                    kmf_true = 0

            if kmf_true == 1 :
                # Plot the survival curves
                kmf1.plot_survival_function()
                kmf2.plot_survival_function()

                plt.title(label_outcome)
                plt.xlabel("Years")
                plt.ylabel("Event-free probability")

                # Median survival time
                median_time1 = kmf1.median_survival_time_
                median_time2 = kmf2.median_survival_time_

                # CI of median survival time
                ci_median_time1 = median_survival_times(kmf1.confidence_interval_)
                ci_median_time2 = median_survival_times(kmf2.confidence_interval_)
            else :
                plt.figure()
                fig = plt.figure(figsize=(12, 5), linewidth = 0.5) #, edgecolor = 'black')
                plt.style.context("seaborn-white")
                plt.axes().set_facecolor('#F8F8F7')

                kmf3 = KaplanMeierFitter()
                kmf3.fit(durations = ds_y[set_outcome[0]]
                        ,event_observed = ds_y[set_outcome[1]]
                        ,label = km_label )
                
                # Plot the survival curves
                kmf3.plot_survival_function()

                plt.title(label_outcome)
                plt.xlabel("Years")
                plt.ylabel("Event-free probability")

                # Median survival time
                median_time1 = kmf3.median_survival_time_
                median_time2 = np.nan

                # CI of median survival time
                ci_median_time1 = median_survival_times(kmf3.confidence_interval_)
                ci_median_time2 = pd.DataFrame({'a': [np.nan], 'b': [np.nan]})
        
        else :
            plt.figure()
            fig = plt.figure(figsize=(12, 5), linewidth = 0.5) #, edgecolor = 'black')
            plt.style.context("seaborn-white")
            plt.axes().set_facecolor('#F8F8F7')

            kmf3 = KaplanMeierFitter()
            kmf3.fit(durations = ds_y[set_outcome[0]]
                    ,event_observed = ds_y[set_outcome[1]]
                    ,label = km_label )
            
            # Plot the survival curves
            kmf3.plot_survival_function()

            plt.title(label_outcome)
            plt.xlabel("Years")
            plt.ylabel("Event-free probability")

            # Median survival time
            median_time1 = kmf3.median_survival_time_
            median_time2 = np.nan

            # CI of median survival time
            ci_median_time1 = median_survival_times(kmf3.confidence_interval_)
            ci_median_time2 = pd.DataFrame({'a': [np.nan], 'b': [np.nan]})

        # Keep the information into dictionary
        modelHR[curCol] = cutoff_var
        HR_test[curCol] = logrank_stats
        HR_pval[curCol] = logrank_pval
        KM_plot[curCol] = fig

        med_time1[curCol] = median_time1
        lower_ci_median_time1[curCol] = ci_median_time1.iloc[0,0]
        upper_ci_median_time1[curCol] = ci_median_time1.iloc[0,1]
        med_time2[curCol] = median_time2
        lower_ci_median_time2[curCol] = ci_median_time2.iloc[0,0]
        upper_ci_median_time2[curCol] = ci_median_time2.iloc[0,1]
        
    
    return({'modelHR': modelHR
            ,'HR_test': HR_test
            ,'HR_pval': HR_pval
            ,'KM_plot': KM_plot

            ,'median_time1': med_time1
            ,'lower_ci_median_time1': lower_ci_median_time1
            ,'upper_ci_median_time1': upper_ci_median_time1
            ,'median_time2': med_time2
            ,'lower_ci_median_time2': lower_ci_median_time2
            ,'upper_ci_median_time2': upper_ci_median_time2
            })



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Hazards Ratio Measurement    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def surv_HR(model, ds_X, ds_X_ori, cols_exclude, threshold_var = None ) :

    # Calculate the SHAP values
    explainer = shap.Explainer(model.predict, ds_X)
    shap_values = explainer(ds_X)
    vals = np.abs(shap_values.values).mean(0)

    # Build a list of Hazards Ratio indexing by j columns
    colIndexes = ds_X.shape[1]
    modelHR = dict()
    modelHR_lr = dict()

    for j in range( colIndexes ) :
        curSHAP = shap_values.values[:, j]
        curCol = ds_X.columns[j]

        # if continuous, split by median
        if ( ds_X[ curCol ].unique().shape[0] > 2 ) :
            colMed = ds_X[curCol].median()
            if (colMed != 0) & (ds_X[curCol].min() != 0) :
                colHR = np.mean( np.exp( curSHAP[ ds_X[curCol] >= colMed ] ) ) / np.mean( np.exp( curSHAP[ ds_X[curCol] < colMed ] ) )
            else :
                colHR = np.mean( np.exp( curSHAP[ ds_X[curCol] > colMed ] ) ) / np.mean( np.exp( curSHAP[ ds_X[curCol] <= colMed ] ) )
        
        # else, binary (1 or 0)
        elif (ds_X[ curCol ].unique().shape[0] == 2) :
            colHR = np.mean( np.exp( curSHAP[ ds_X[curCol] == 1 ] ) ) / np.mean( np.exp( curSHAP[ ds_X[curCol] == 0 ] ) )
        
        else :
            colHR = np.nan
        
        # if continuous, split by defined threshold
        cutoff_lr = threshold_var[curCol]
        if ( ds_X_ori[ curCol ].unique().shape[0] > 2 ) :
            colHR_lr = np.mean( np.exp( curSHAP[ ds_X_ori[curCol] >= cutoff_lr ] ) ) / np.mean( np.exp( curSHAP[ ds_X_ori[curCol] < cutoff_lr ] ) )
        
        # else, binary (1 or 0)
        elif (ds_X_ori[ curCol ].unique().shape[0] == 2) :
            colHR_lr = np.mean( np.exp( curSHAP[ ds_X_ori[curCol] == 1 ] ) ) / np.mean( np.exp( curSHAP[ ds_X_ori[curCol] == 0 ] ) )
        
        else :
            colHR_lr = np.nan
        
        if curCol not in cols_exclude :
            modelHR[ curCol ] = colHR
            modelHR_lr[ curCol ] = colHR_lr
        else:
            pass
    
    return( dict({'modelHR': modelHR, 'modelHR_lr': modelHR_lr}) )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To measure the Hazard Ratio for each cohort and algorithm    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Hazard Ratio measurement
def func_HR_measure(BLoop):
    loop_file_repo = ls_BLoop[BLoop][0]
    loop_model_set = ls_BLoop[BLoop][1]
    loop_model_prefix = ls_BLoop[BLoop][2]

    model_prefix = loop_model_set + loop_model_prefix
    file_repo_prefix = loop_file_repo
    # model_set = loop_model_set
    
    print('Outcome (Hazards Ratio): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)
    print('Model :'+loop_model_set)
    
    optimal_model = ls_BLoop[BLoop][3]
    
    # Time-to-event model
    best_smodel = optimal_model.best_model

    # Validation set
    smodel_X = optimal_model.X_validate
    smodel_y = optimal_model.y_validate

    # To obtain unnormalized X_validate
    unnormalized_repo = optimal_model.data_repo.replace(optimal_model.normalize_method, "NONE")
    unnormalized_validate = load_object(filename = optimal_model.pickle_prefix+'validate', file_repo = unnormalized_repo)

    # To split the unnormalized data into X and y
    vars_ml_ana = [x for x in optimal_model.vars_ml if x in unnormalized_validate.columns.tolist() ]
    unnormalized_X_validate, unnormalized_y_validate = get_x_y(data_frame = unnormalized_validate[vars_ml_ana + [optimal_model.var_status, optimal_model.var_time]]
                                                               ,attr_labels = [optimal_model.var_status, optimal_model.var_time], pos_label = 1 )
    
    # One-hot encoding into unnormalized X
    vars_encoding = [x for x in optimal_model.vars_encoding if x in unnormalized_X_validate.columns.tolist() ]
    unnormalized_X_validate, none = func_one_hot_encoding(ds_Xtrain = unnormalized_X_validate, ds_Xtest = unnormalized_X_validate, vars_encode = vars_encoding)
    
    # Permutation importance
    smodel_set = optimal_model.surv_model_set
    if smodel_set == 'median_time' :
        df_features_importance = pd.DataFrame()
    else :
        features_importance = permutation_importance(best_smodel, smodel_X, smodel_y, n_repeats = 15, random_state = optimal_model.seed_number)
        df_features_importance = pd.DataFrame({feats: features_importance[feats] for feats in ('importances_mean', 'importances_std',) }
                                            ,index = smodel_X.columns )
        df_features_importance['abs_importances_mean'] = df_features_importance['importances_mean'].abs()
        df_features_importance = df_features_importance[ df_features_importance['importances_mean'] != 0 ]
        df_features_importance.sort_values(by = 'abs_importances_mean', ascending = False, inplace = True)
        df_features_importance.drop(['abs_importances_mean'], axis = 1, inplace = True)

    # smodel_features = list( optimal_model.features_importance.reset_index()['index'] )
    # best_smodel_features = smodel_features
    best_smodel_features = list( df_features_importance.reset_index()['index'] )
    if len(best_smodel_features) == 0 :
        best_smodel_features = list( smodel_X.columns )
    else:
        pass

    # Excluding the variable
    col_excl = [ x for x in smodel_X.columns if len(np.unique(smodel_X[x])) == 1 ]

    # To define the threshold of continuous variable by using log-rank test
    logrank_smodel = func_surv_model_shap_hr(X_data = smodel_X
                                             ,y_data = smodel_y
                                             ,smodel_set_hr = smodel_set
                                             ,best_model_max_features = optimal_model.best_model_max_features
                                             ,seed_number = optimal_model.seed_number )
    threshold_all = surv_logrank( model = logrank_smodel, ds_X = unnormalized_X_validate, ds_y = smodel_y, cols_exclude = col_excl )
    threshold = threshold_all['modelHR']
    logrank_stat = threshold_all['HR_test']
    logrank_pvalue = threshold_all['HR_pval']
    km_plot = threshold_all['KM_plot']

    median_time1 = threshold_all['median_time1']
    lower_ci_median_time1 = threshold_all['lower_ci_median_time1']
    upper_ci_median_time1 = threshold_all['upper_ci_median_time1']

    median_time2 = threshold_all['median_time2']
    lower_ci_median_time2 = threshold_all['lower_ci_median_time2']
    upper_ci_median_time2 = threshold_all['upper_ci_median_time2']

    def bootstrapping_hr(hr_loop) :
        samp = hr_loop + 1
        # Sample with replacement
        X_train = smodel_X.sample(n = smodel_X.shape[0], random_state = samp, replace = True)[best_smodel_features]  # Sample with replacement - num rows
        y_train = smodel_y[ X_train.index ]

        X_test = smodel_X.drop( X_train.index )[best_smodel_features]
        y_test = smodel_y[ X_test.index ]

        # To train the model
        bootstrap_smodel = func_surv_model_shap_hr(X_data = X_train
                                                ,y_data = y_train
                                                ,smodel_set_hr = smodel_set
                                                ,best_model_max_features = optimal_model.best_model_max_features
                                                ,seed_number = optimal_model.seed_number )
        
        # Hazards ratio measurement
        hr_measure_all = surv_HR( model = bootstrap_smodel
                                ,ds_X = X_train
                                ,ds_X_ori = unnormalized_X_validate.iloc[ X_train.index, : ][X_train.columns]
                                ,cols_exclude = col_excl
                                ,threshold_var = threshold )

        # Hazards ratio measurement (split the continuous variable by median)
        hr_measure = hr_measure_all['modelHR']

        # Hazards ratio measurement (split the continuous variable by threshold based on log-rank test)
        hr_measure_lr = hr_measure_all['modelHR_lr']

        return({'HazRatios' : hr_measure, 'HazRatios_lr' : hr_measure_lr})
    

    # To measure the Hazard Ratio for each cohort and algorithm
    bootstrap_loop = 1000 # 1000
    n_jobs_set = 10
    time_start = datetime.now()
    bootstrap_hr = Parallel(n_jobs = n_jobs_set)( # n_jobs)(
        delayed( bootstrapping_hr )(hr_loop = hr_loop)
        for hr_loop in range(bootstrap_loop) )
    time_end = datetime.now()
    print("Total running time: " + str(time_end - time_start))

    check_nan1 = np.unique([x for x in range(len(bootstrap_hr)) for y in list(bootstrap_hr[x]['HazRatios'].keys()) if ~np.isnan(bootstrap_hr[x]['HazRatios'][y])])
    check_nan2 = np.unique([x for x in range(len(bootstrap_hr)) for y in list(bootstrap_hr[x]['HazRatios_lr'].keys()) if ~np.isnan(bootstrap_hr[x]['HazRatios_lr'][y])])

    HazRatios = [bootstrap_hr[x]['HazRatios'] for x in check_nan1]
    HazRatios_lr = [bootstrap_hr[x]['HazRatios_lr'] for x in check_nan2]

    while (len(HazRatios) < bootstrap_loop) | (len(HazRatios_lr) < bootstrap_loop) :

        tmp_bootstrap_loop = max([bootstrap_loop - len(HazRatios), bootstrap_loop - len(HazRatios_lr)])
        # if tmp_bootstrap_loop < n_jobs :
        #     tmp_n_jobs = tmp_bootstrap_loop

        tmp_bootstrap_hr = Parallel(n_jobs = n_jobs_set)(
            delayed( bootstrapping_hr )(hr_loop = hr_loop)
            for hr_loop in range(tmp_bootstrap_loop) )

        tmp_check_nan1 = np.unique([x for x in range(len(tmp_bootstrap_hr)) for y in list(tmp_bootstrap_hr[x]['HazRatios'].keys()) if ~np.isnan(tmp_bootstrap_hr[x]['HazRatios'][y])])
        tmp_check_nan2 = np.unique([x for x in range(len(tmp_bootstrap_hr)) for y in list(tmp_bootstrap_hr[x]['HazRatios_lr'].keys()) if ~np.isnan(tmp_bootstrap_hr[x]['HazRatios_lr'][y])])

        tmp_HazRatios = [tmp_bootstrap_hr[x]['HazRatios'] for x in tmp_check_nan1]
        tmp_HazRatios_lr = [tmp_bootstrap_hr[x]['HazRatios_lr'] for x in tmp_check_nan2]

        HazRatios += tmp_HazRatios
        HazRatios_lr += tmp_HazRatios_lr
    
    if (len(HazRatios) > bootstrap_loop) | (len(HazRatios_lr) > bootstrap_loop) :
        HazRatios = HazRatios[0:bootstrap_loop]
        HazRatios_lr = HazRatios_lr[0:bootstrap_loop]
    
    # Hazards ratio : [Median, Mean, LD, UB] at 5% significance level
    alpha = 0.05
    HR_summaries = dict()
    HR_summaries_lr = dict()
    HR_key = list( HazRatios[0].keys() )

    for key_i in HR_key :
        ls_hr = []
        ls_hr_lr = []
        
        for samp in range(len(HazRatios)) :
            ls_hr.append( HazRatios[samp][key_i] )
            ls_hr_lr.append( HazRatios_lr[samp][key_i] )
        
        # Bias-corrected and accelerated (BCa) confidence interval [split by median]
        bca_mean = np.mean(ls_hr)
        bca_se = np.std(ls_hr, ddof = 1)
        z_score_bca = norm.ppf(1 - alpha/2)
        lower_bca = bca_mean - z_score_bca * bca_se
        upper_bca = bca_mean + z_score_bca * bca_se

        # Bias-corrected and accelerated (BCa) confidence interval [split by log-rank test]
        bca_mean2 = np.mean(ls_hr_lr)
        bca_se2 = np.std(ls_hr_lr, ddof = 1)
        z_score_bca2 = norm.ppf(1 - alpha/2)
        lower_bca2 = bca_mean2 - z_score_bca2 * bca_se2
        upper_bca2 = bca_mean2 + z_score_bca2 * bca_se2

        # Asymptotic Normal Approximation [split by median]
        norm_mean = np.mean(ls_hr)
        norm_se = np.std(ls_hr, ddof = 1)
        z_score_norm = norm.ppf(1 - alpha/2)
        lower_norm = norm_mean - z_score_norm * norm_se
        upper_norm = norm_mean + z_score_norm * norm_se

        # Asymptotic Normal Approximation [split by log-rank-test]
        norm_mean2 = np.mean(ls_hr_lr)
        norm_se2 = np.std(ls_hr_lr, ddof = 1)
        z_score_norm2 = norm.ppf(1 - alpha/2)
        lower_norm2 = norm_mean2 - z_score_norm2 * norm_se2
        upper_norm2 = norm_mean2 + z_score_norm2 * norm_se2
        
        # To summarize the median, mean and CI of the hazard ratio into a table [split by median]
        ls_hr.sort()
        ls_summaries = [ np.median(ls_hr)                           # [0]  Median
                        ,np.mean(ls_hr)                             # [1]  Mean
                        ,np.percentile(ls_hr, alpha/2*100)          # [2]  Lower bound (Percentile)
                        ,np.percentile(ls_hr, 100-alpha/2*100)      # [3]  Upper bound (Percentile)
                        ,lower_bca                                  # [4]  Lower bound (BCa)
                        ,upper_bca                                  # [5]  Upper bound (BCa)
                        ,lower_norm                                 # [6]  Lower bound (Norm)
                        ,upper_norm                                 # [7]  Upper bound (Norm)
                        ,"<"                                        # [8]  Splitting
                        ,"median"                                   # [9]  Threshold
                        ,""                                         # [10] Log-rank test statistic
                        ,""                                         # [11] Log-rank test (p-value)
                        ,""                                         # [12] Median survival time (higher values group)
                        ,""                                         # [13] Lower CI of median survival time (higher values group)
                        ,""                                         # [14] Upper CI of median survival time (higher values group)
                        ,""                                         # [15] Median survival time (lower values group)
                        ,""                                         # [16] Lower CI of median survival time (lower values group)
                        ,""                                         # [17] Upper CI of median survival time (lower values group)
                        ]
        
        # To summarize the median, mean and CI of the hazard ratio into a table [split by log-rank test]
        ls_hr_lr.sort()
        ls_summaries_lr = [ np.median(ls_hr_lr)                           # [0]  Median
                           ,np.mean(ls_hr_lr)                             # [1]  Mean
                           ,np.percentile(ls_hr_lr, alpha/2*100)          # [2]  Lower bound (Percentile)
                           ,np.percentile(ls_hr_lr, 100-alpha/2*100)      # [3]  Upper bound (Percentile)
                           ,lower_bca2                                    # [4]  Lower bound (BCa)
                           ,upper_bca2                                    # [5]  Upper bound (BCa)
                           ,lower_norm2                                   # [6]  Lower bound (Norm)
                           ,upper_norm2                                   # [7]  Upper bound (Norm)
                           ,"<"                                           # [8]  Splitting
                           ,threshold[key_i]                              # [9]  Threshold
                           ,logrank_stat[key_i]                           # [10] Log-rank test statistic
                           ,logrank_pvalue[key_i]                         # [11] Log-rank test (p-value)
                           ,median_time1[key_i]                           # [12] Median survival time (higher values group)
                           ,lower_ci_median_time1[key_i]                  # [13] Lower CI of median survival time (higher values group)
                           ,upper_ci_median_time1[key_i]                  # [14] Upper CI of median survival time (higher values group)
                           ,median_time2[key_i]                           # [15] Median survival time (lower values group)
                           ,lower_ci_median_time2[key_i]                  # [16] Lower CI of median survival time (lower values group)
                           ,upper_ci_median_time2[key_i]                  # [17] Upper CI of median survival time (lower values group)
                           ]
        
        HR_summaries[key_i] = ls_summaries
        HR_summaries_lr[key_i] = ls_summaries_lr

    # Merge variable label, HR_summaries, data_info_all [split by median]
    ds_cols = data_info_all[ data_info_all['Variable'].isin(list(HR_summaries.keys())) ].reset_index(drop = True)
    ds_hr = pd.DataFrame({'Variable'                        : list(HR_summaries.keys())
                        ,'Hazard Ratio (Median)'            : [ HR_summaries[x][0] for x in list(HR_summaries.keys()) ]
                        ,'Hazard Ratio (Mean)'              : [ HR_summaries[x][1] for x in list(HR_summaries.keys()) ]
                        ,'Lower CI (Percentile)'            : [ HR_summaries[x][2] for x in list(HR_summaries.keys()) ]
                        ,'Upper CI (Percentile)'            : [ HR_summaries[x][3] for x in list(HR_summaries.keys()) ]
                        ,'Lower CI (BCa)'                   : [ HR_summaries[x][4] for x in list(HR_summaries.keys()) ]
                        ,'Upper CI (BCa)'                   : [ HR_summaries[x][5] for x in list(HR_summaries.keys()) ]
                        ,'Lower CI (Norm)'                  : [ HR_summaries[x][6] for x in list(HR_summaries.keys()) ]
                        ,'Upper CI (Norm)'                  : [ HR_summaries[x][7] for x in list(HR_summaries.keys()) ]
                        ,'Split'                            : [ HR_summaries[x][8] for x in list(HR_summaries.keys()) ]
                        ,'Threshold'                        : [ HR_summaries[x][9] for x in list(HR_summaries.keys()) ]
                        ,'Log-rank test statistics'         : [ HR_summaries[x][10] for x in list(HR_summaries.keys()) ]
                        ,'Log-rank (p-value)'               : [ HR_summaries[x][11] for x in list(HR_summaries.keys()) ]
                        ,'Median Survival Time (Group 1)'   : [ HR_summaries[x][12] for x in list(HR_summaries.keys()) ]
                        ,'Lower CI (Group 1)'               : [ HR_summaries[x][13] for x in list(HR_summaries.keys()) ]
                        ,'Upper CI (Group 1)'               : [ HR_summaries[x][14] for x in list(HR_summaries.keys()) ]
                        ,'Median Survival Time (Group 2)'   : [ HR_summaries[x][15] for x in list(HR_summaries.keys()) ]
                        ,'Lower CI (Group 2)'               : [ HR_summaries[x][16] for x in list(HR_summaries.keys()) ]
                        ,'Upper CI (Group 2)'               : [ HR_summaries[x][17] for x in list(HR_summaries.keys()) ]
                        } )
    data_HR = pd.merge(ds_cols, ds_hr, how = 'left', on = ['Variable'])

    # Merge variable label, HR_summaries, data_info_all [split by log-rank test]
    ds_hr_lr = pd.DataFrame({'Variable'                             : list(HR_summaries_lr.keys())
                             ,'Hazard Ratio (Median)'               : [ HR_summaries_lr[x][0] for x in list(HR_summaries_lr.keys()) ]
                             ,'Hazard Ratio (Mean)'                 : [ HR_summaries_lr[x][1] for x in list(HR_summaries_lr.keys()) ]
                             ,'Lower CI (Percentile)'               : [ HR_summaries_lr[x][2] for x in list(HR_summaries_lr.keys()) ]
                             ,'Upper CI (Percentile)'               : [ HR_summaries_lr[x][3] for x in list(HR_summaries_lr.keys()) ]
                             ,'Lower CI (BCa)'                      : [ HR_summaries_lr[x][4] for x in list(HR_summaries_lr.keys()) ]
                             ,'Upper CI (BCa)'                      : [ HR_summaries_lr[x][5] for x in list(HR_summaries_lr.keys()) ]
                             ,'Lower CI (Norm)'                     : [ HR_summaries_lr[x][6] for x in list(HR_summaries_lr.keys()) ]
                             ,'Upper CI (Norm)'                     : [ HR_summaries_lr[x][7] for x in list(HR_summaries_lr.keys()) ]
                             ,'Split'                               : [ HR_summaries_lr[x][8] for x in list(HR_summaries_lr.keys()) ]
                             ,'Threshold'                           : [ HR_summaries_lr[x][9] for x in list(HR_summaries_lr.keys()) ]
                             ,'Log-rank test statistics'            : [ HR_summaries_lr[x][10] for x in list(HR_summaries_lr.keys()) ]
                             ,'Log-rank (p-value)'                  : [ HR_summaries_lr[x][11] for x in list(HR_summaries_lr.keys()) ]
                             ,'Median Survival Time (Group 1)'      : [ HR_summaries_lr[x][12] for x in list(HR_summaries_lr.keys()) ]
                             ,'Lower CI (Group 1)'                  : [ HR_summaries_lr[x][13] for x in list(HR_summaries_lr.keys()) ]
                             ,'Upper CI (Group 1)'                  : [ HR_summaries_lr[x][14] for x in list(HR_summaries_lr.keys()) ]
                             ,'Median Survival Time (Group 2)'      : [ HR_summaries_lr[x][15] for x in list(HR_summaries_lr.keys()) ]
                             ,'Lower CI (Group 2)'                  : [ HR_summaries_lr[x][16] for x in list(HR_summaries_lr.keys()) ]
                             ,'Upper CI (Group 2)'                  : [ HR_summaries_lr[x][17] for x in list(HR_summaries_lr.keys()) ]
                             } )
    data_HR_lr = pd.merge(ds_cols, ds_hr_lr, how = 'left', on = ['Variable'])

    # Combine data_HR and data_HR_lr
    data_HR_comb = pd.concat([data_HR, data_HR_lr], axis = 0).reset_index(drop = True)

    # Print the information
    print('[DONE] Outcome (Hazards Ratio): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('[DONE] Cohort: '+file_repo_prefix)
    print('[DONE] Model :'+loop_model_set)

    return({'data_HR': data_HR_comb
            ,'KM_plot': km_plot
            })