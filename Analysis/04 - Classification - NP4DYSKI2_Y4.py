#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Machine learning Classification    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# To load the file for the labels of the baseline variables in the model
outcome_define = 'NP4DYSKI2_Y4'
exec( open("Analysis/01 - Data Processing - Labels of Baseline Variables.py").read() )

# Label at x-axis for SHAP values plot
shap_x_label = ['No dyskinesia', 'Dyskinesia']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Machine Learning Classification - Dyskinesias [NP4DYSKI2_Y4]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# FIGS - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - FIGS - NP4DYSKI.py").read() )

# ADABOOST - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - ADABOOST - NP4DYSKI.py").read() )

# C4.5 - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - C45 - NP4DYSKI.py").read() )

# CATBOOST - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - CATBOOST - NP4DYSKI.py").read() )

# GBOOST - Dyskinesias (Gradient Boosting)
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - GBOOST - NP4DYSKI.py").read() )

# GOSDT - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - GOSDT - NP4DYSKI.py").read() )

# GREEDY - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - GREEDY - NP4DYSKI.py").read() )

# HS - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - HS - NP4DYSKI.py").read() )

# XGBOOST - Dyskinesias
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/04 - Classification - XGBOOST - NP4DYSKI.py").read() )

exec( open("Analysis/Prediction/NP4DYSKI2_Y4/05 - Classification - Performance Comparison - NP4DYSKI2.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/05 - Classification - Performance Comparison on Cross-Validation - NP4DYSKI2.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/05 - Classification - AUC Comparison (DeLong test) - NP4DYSKI2.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/05 - Classification - Stability - NP4DYSKI2.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/06 - Classification - Decision Curve Analysis - NP4DYSKI2.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_Y4/07 - LEDD Significance Test - NP4DYSKI2.py").read() )
