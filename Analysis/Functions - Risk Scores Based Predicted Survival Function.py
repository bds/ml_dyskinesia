#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load the packages    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import numbers
from sklearn.preprocessing import normalize as f_normalize
from sklearn.utils._param_validation import Interval
from sksurv.linear_model.coxph import BreslowEstimator
from sksurv.util import check_array_survival


class svm_predict_survival_function:
    """Gradient boosting with component-wise least squares as base learner.

    See the :ref:`User Guide </user_guide/boosting.ipynb>` and [1]_ for further description.

    Parameters
    ----------
    Attributes
    ----------
    """

    _parameter_constraints = {
        # "loss": [StrOptions(frozenset(LOSS_FUNCTIONS.keys()))],
        "learning_rate": [Interval(numbers.Real, 0.0, None, closed="left")],
        "n_estimators": [Interval(numbers.Integral, 1, None, closed="left")],
        "subsample": [Interval(numbers.Real, 0.0, 1.0, closed="right")],
        "warm_start": ["boolean"],
        "dropout_rate": [Interval(numbers.Real, 0.0, 1.0, closed="left")],
        "random_state": ["random_state"],
        "verbose": ["verbose"],
    }

    def __init__(
        self,
        *,
        # loss="coxph",
        learning_rate=0.1,
        n_estimators=100,
        subsample=1.0,
        warm_start=False,
        dropout_rate=0,
        random_state=None,
        verbose=0,
    ):
        # self.loss = loss
        self.n_estimators = n_estimators
        self.learning_rate = learning_rate
        self.subsample = subsample
        self.warm_start = warm_start
        self.dropout_rate = dropout_rate
        self.random_state = random_state
        self.verbose = verbose

    @property

    def _init_state(self):
        self.estimators_ = np.empty(self.n_estimators, dtype=object)

        self.train_score_ = np.zeros(self.n_estimators, dtype=np.float64)
        # do oob?
        if self.subsample < 1.0:
            self.oob_improvement_ = np.zeros(self.n_estimators, dtype=np.float64)
            self.oob_scores_ = np.zeros(self.n_estimators, dtype=np.float64)
            self.oob_score_ = np.nan

        if self.dropout_rate > 0:
            self._scale = np.ones(int(self.n_estimators), dtype=float)

    def fit(self, svm_model, X, y, sample_weight=None):
        """Fit estimator.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Data matrix

        y : structured array, shape = (n_samples,)
            A structured array containing the binary event indicator
            as first field, and time of event or time of censoring as
            second field.

        sample_weight : array-like, shape = (n_samples,), optional
            Weights given to each sample. If omitted, all samples have weight 1.

        Returns
        -------
        self
        """
        self.svm_model = svm_model
        event, time = check_array_survival(X, y)
        self._set_baseline_model(X, event, time)
        return self

    def _set_baseline_model(self, X, event, time):
        risk_scores = self.svm_model.predict(X)
        self._baseline_model = BreslowEstimator().fit(risk_scores, event, time)

    def _get_baseline_model(self):
        if self._baseline_model is None:
            raise ValueError("`fit` must be called with the loss option set to 'coxph'.")
        return self._baseline_model

    def _predict_function(self, func_name, baseline_model, prediction, return_array):
        fns = getattr(baseline_model, func_name)(prediction)

        if not return_array:
            return fns

        times = baseline_model.unique_times_
        arr = np.empty((prediction.shape[0], times.shape[0]), dtype=float)
        for i, fn in enumerate(fns):
            arr[i, :] = fn(times)
        return arr
    
    def _predict_survival_function(self, baseline_model, prediction, return_array):
        """Return survival functions.
        Parameters
        ----------
        baseline_model : sksurv.linear_model.coxph.BreslowEstimator
            Estimator of baseline survival function.
        prediction : array-like, shape=(n_samples,)
            Predicted risk scores.
        return_array : bool
            If True, return a float array of the survival function
            evaluated at the unique event times, otherwise return
            an array of :class:`sksurv.functions.StepFunction` instances.
        Returns
        -------
        survival : ndarray
        """
        return self._predict_function("get_survival_function", baseline_model, prediction, return_array)

    def predict_survival_function(self, X, return_array=False):
        """Predict survival function.

        Only available if :meth:`fit` has been called with `loss = "coxph"`.

        The survival function for an individual
        with feature vector :math:`x` is defined as

        .. math::

            S(t \\mid x) = S_0(t)^{\\exp(f(x)} ,

        where :math:`f(\\cdot)` is the additive ensemble of base learners,
        and :math:`S_0(t)` is the baseline survival function,
        estimated by Breslow's estimator.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Data matrix.

        return_array : boolean, default: False
            If set, return an array with the probability
            of survival for each `self.unique_times_`,
            otherwise an array of :class:`sksurv.functions.StepFunction`.

        Returns
        -------
        survival : ndarray
            If `return_array` is set, an array with the probability of
            survival for each `self.unique_times_`, otherwise an array of
            length `n_samples` of :class:`sksurv.functions.StepFunction`
            instances will be returned.
        """
        return self._predict_survival_function(self._get_baseline_model(), self.svm_model.predict(X), return_array)