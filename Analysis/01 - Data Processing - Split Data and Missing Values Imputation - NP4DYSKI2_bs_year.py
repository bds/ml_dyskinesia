#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                       Levodopa-induced dyskinesia year and status (NP4DYSKI2_bs_year & NP4DYSKI2_status)                                                   ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_luxpark_temp = data_cohort_luxpark[ (~data_cohort_luxpark['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_luxpark['NP4DYSKI2_status'].isnull()) & (~data_cohort_luxpark['NP4DYSKI2_Y4'].isnull()) ]

data_luxpark_impute = split_impute(data = data_luxpark_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                                  ,outcome = 'NP4DYSKI2_status'
                                  ,bs_outcome = 'NP4DYSKI2'
                                  ,vars_cat = vars_category_common
                                  ,save_data_repo = file_data+'LUXPARK/NP4DYSKI2_bs_year/NONE/' )
data_luxpark_impute.split_n_impute(n_jobs = n_jobs)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_ppmi_temp = data_cohort_ppmi[ (~data_cohort_ppmi['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_status'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_Y4'].isnull()) ]

data_ppmi_impute = split_impute(data = data_ppmi_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                               ,outcome = 'NP4DYSKI2_status'
                               ,bs_outcome = 'NP4DYSKI2'
                               ,vars_cat = vars_category_common
                               ,save_data_repo = file_data+'PPMI/NP4DYSKI2_bs_year/NONE/' )
data_ppmi_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - ICEBERG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_iceberg_temp = data_cohort_iceberg[ (~data_cohort_iceberg['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_status'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_Y4'].isnull()) ]

data_iceberg_impute = split_impute(data = data_iceberg_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                                  ,outcome = 'NP4DYSKI2_status'
                                  ,bs_outcome = 'NP4DYSKI2'
                                  ,vars_cat = vars_category_common
                                  ,save_data_repo = file_data+'ICEBERG/NP4DYSKI2_bs_year/NONE/' )
data_iceberg_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI + LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_cohort_ppmi_luxpark = pd.concat([data_cohort_ppmi, data_cohort_luxpark])
data_ppmi_luxpark_temp = data_cohort_ppmi_luxpark[ (~data_cohort_ppmi_luxpark['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_ppmi_luxpark['NP4DYSKI2_status'].isnull()) & (~data_cohort_ppmi_luxpark['NP4DYSKI2_Y4'].isnull()) ]
data_iceberg_temp = data_cohort_iceberg[ (~data_cohort_iceberg['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_status'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_Y4'].isnull()) ]

data_loo_impute = split_impute(data = data_ppmi_luxpark_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                              ,val_data = data_iceberg_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                              ,outcome = 'NP4DYSKI2_status'
                              ,bs_outcome = 'NP4DYSKI2'
                              ,vars_cat = vars_category_common
                              ,save_data_repo = file_data+'LEAVE_ONE_OUT/NP4DYSKI2_bs_year/NONE/' )
data_loo_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - ICEBERG + LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_cohort_iceberg_luxpark = pd.concat([data_cohort_iceberg, data_cohort_luxpark])
data_iceberg_luxpark_temp = data_cohort_iceberg_luxpark[ (~data_cohort_iceberg_luxpark['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_iceberg_luxpark['NP4DYSKI2_status'].isnull()) & (~data_cohort_iceberg_luxpark['NP4DYSKI2_Y4'].isnull()) ]
data_ppmi_temp = data_cohort_ppmi[ (~data_cohort_ppmi['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_status'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_Y4'].isnull()) ]

data_loo_impute = split_impute(data = data_iceberg_luxpark_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                              ,val_data = data_ppmi_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                              ,outcome = 'NP4DYSKI2_status'
                              ,bs_outcome = 'NP4DYSKI2'
                              ,vars_cat = vars_category_common
                              ,save_data_repo = file_data+'LEAVE_ONE_OUT2/NP4DYSKI2_bs_year/NONE/' )
data_loo_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI + LuxPARK + ICEBERG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_comb_temp = data_cohort_comb[ (~data_cohort_comb['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_comb['NP4DYSKI2_status'].isnull()) & (~data_cohort_comb['NP4DYSKI2_Y4'].isnull()) ]

data_comb_impute = split_impute(data = data_comb_temp.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']]
                               ,outcome = 'NP4DYSKI2_status'
                               ,bs_outcome = 'NP4DYSKI2'
                               ,vars_cat = vars_category_common
                               ,save_data_repo = file_data+'ALL/NP4DYSKI2_bs_year/NONE/' )
data_comb_impute.split_n_impute(n_jobs = n_jobs)
