#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Calculating Percentage of Missingness    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_select = data_info[ data_info['Type'].isin( ['Baseline', 'Data type'] ) ]['Variable']
data_comb_miss = data_comb[ vars_select ]


# Missing values in both PPMI and LuxPARK cohorts
data_miss_info = data_comb_miss.isnull().sum().reset_index()\
                    .rename(columns = {'index': 'Variable', 0: 'miss_sum'})
data_miss_info['miss_pct'] = data_miss_info['miss_sum']/data_comb_miss.shape[0] * 100
# data_miss_info = data_miss_info[data_miss_info['miss_sum'] > 0]
data_miss_info = data_miss_info.sort_values(['miss_sum', 'Variable'], ascending = [False, True]).reset_index()
data_miss_info['rank'] = data_miss_info.index
data_miss_info = data_miss_info.merge(data_variables.loc[data_variables['No'] > 0, ['PPMI Variable', 'PPMI Description']], left_on = ['Variable'], right_on = ['PPMI Variable'])\
                    .rename(columns = {'PPMI Description': 'Description'})\
                    .drop(['PPMI Variable'], axis = 1)


# Missing values in PPMI
data_miss_info1 = data_comb_miss[data_comb_miss['dt_type'] == 'PPMI'].isnull().sum().reset_index()\
                    .rename(columns = {'index': 'Variable', 0: 'miss_sum'})
data_miss_info1['miss_pct'] = data_miss_info1['miss_sum']/data_comb_miss.shape[0] * 100
data_miss_info1 = data_miss_info1.merge(data_miss_info[['Variable', 'Description', 'rank']])
data_miss_info1 = data_miss_info1.sort_values('rank', ascending = False)
data_miss_info1['Hover Text'] = "PPMI:" + data_miss_info1['miss_pct'].round(decimals = 2).map(str) + "%"


# Missing values in LuxPARK
data_miss_info2 = data_comb_miss[data_comb_miss['dt_type'] == 'LuxPARK'].isnull().sum().reset_index()\
                    .rename(columns = {'index': 'Variable', 0: 'miss_sum'})
data_miss_info2['miss_pct'] = data_miss_info2['miss_sum']/data_comb_miss.shape[0] * 100
data_miss_info2 = data_miss_info2.merge(data_miss_info[['Variable', 'Description', 'rank']])
data_miss_info2 = data_miss_info2.sort_values('rank', ascending = False)
data_miss_info2['Hover Text'] = data_miss_info2['Description'] + "<br>LuxPARK: " + data_miss_info2['miss_pct'].round(decimals = 2).map(str) + "%"


# Missing values in ICEBERG
data_miss_info3 = data_comb_miss[data_comb_miss['dt_type'] == 'ICEBERG'].isnull().sum().reset_index()\
                    .rename(columns = {'index': 'Variable', 0: 'miss_sum'})
data_miss_info3['miss_pct'] = data_miss_info3['miss_sum']/data_comb_miss.shape[0] * 100
data_miss_info3 = data_miss_info3.merge(data_miss_info[['Variable', 'Description', 'rank']])
data_miss_info3 = data_miss_info3.sort_values('rank', ascending = False)
data_miss_info3['Hover Text'] = data_miss_info3['Description'] + "<br>ICEBERG: " + data_miss_info3['miss_pct'].round(decimals = 2).map(str) + "%"


# Merge the missing values info from PPMI, LuxPARK, and ICEBERG
data_miss_info_comb = data_miss_info1.merge(data_miss_info2, on = ['Variable', 'rank'], how = 'left', suffixes = ['_PPMI', '_LuxPARK'])
data_miss_info_comb = data_miss_info_comb.merge(data_miss_info3, on = ['Variable', 'rank'], how = 'left', suffixes = ['', '_ICEBERG'])



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Stacked bar chart to display percentage of missing values for each variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

fig = go.Figure()
fig.add_trace(go.Bar(
  y = data_miss_info1['Variable'],
  x = data_miss_info1['miss_pct'],
  text = data_miss_info1['Hover Text'],
  hoverinfo = "text",
  name = 'PPMI',
  orientation = 'h',
  marker = dict(
    color = 'rgba(246, 78, 139, 0.6)',
    line = dict(color = 'rgba(246, 78, 139, 0.8)', width = 1)
  )
))

fig.add_trace(go.Bar(
  y = data_miss_info2['Variable'],
  x = data_miss_info2['miss_pct'],
  text = data_miss_info2['Hover Text'],
  hoverinfo = "text",
  name = 'LuxPARK',
  orientation = 'h',
  marker = dict(
    color = 'rgba(13, 155, 251, 0.6)',
    line = dict(color = 'rgba(50, 91, 222, 0.8)', width = 1)
  )
))

fig.add_trace(go.Bar(
  y = data_miss_info3['Variable'],
  x = data_miss_info3['miss_pct'],
  text = data_miss_info3['Hover Text'],
  hoverinfo = "text",
  name = 'ICEBERG',
  orientation = 'h',
  marker = dict(
    color = 'rgba(130, 155, 251, 0.6)',
    line = dict(color = 'rgba(50, 91, 222, 0.8)', width = 1)
  )
))

fig.update_layout(barmode = 'stack', autosize = False, height = 2100, hovermode = "y unified")
py.plot(fig, filename = file_repo+"Missing_values_bar.html", auto_open = False)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####   Heatmap to show correlation of each variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

dt_vars_miss = data_comb.isnull().sum().reset_index().rename(columns = {0:'n_miss'}).sort_values(['n_miss'], ascending = False)
vars_miss = dt_vars_miss[dt_vars_miss['n_miss'] > 0]['index'].to_list()
data_miss_cor = data_comb[vars_miss]

# Replace not nan as 1, nan as 0
data_miss_corr = data_miss_cor.notnull().astype('int')


# To show the correlation of missingness in heatmap (only applicable to the variables with missing values)
# plot_heatmap(data = data_comb, filename = "Analysis/Plots/Missing_values_heatmap.html")
plot_heatmap(data = data_miss_corr, filename = file_repo+"Missing_values_heatmap.html", width = 850)