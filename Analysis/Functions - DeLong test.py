import pandas as pd
import numpy as np
import scipy.stats

# AUC comparison adapted from
# https://github.com/Netflix/vmaf/
# https://github.com/yandexdataschool/roc_comparison/tree/master
def compute_midrank(x):
    """Computes midranks.
    Args:
       x - a 1D numpy array
    Returns:
       array of midranks
    """
    J = np.argsort(x)
    Z = x[J]
    N = len(x)
    T = np.zeros(N, dtype=np.float)
    i = 0
    while i < N:
        j = i
        while j < N and Z[j] == Z[i]:
            j += 1
        T[i:j] = 0.5*(i + j - 1)
        i = j
    T2 = np.empty(N, dtype=np.float)
    # Note(kazeevn) +1 is due to Python using 0-based indexing
    # instead of 1-based in the AUC formula in the paper
    T2[J] = T + 1
    return T2


def fastDeLong(predictions_sorted_transposed, label_1_count):
    """
    The fast version of DeLong's method for computing the covariance of
    unadjusted AUC.
    Args:
       predictions_sorted_transposed: a 2D numpy.array[n_classifiers, n_examples]
          sorted such as the examples with label "1" are first
    Returns:
       (AUC value, DeLong covariance)
    Reference:
     @article{sun2014fast,
       title={Fast Implementation of DeLong's Algorithm for
              Comparing the Areas Under Correlated Receiver Operating Characteristic Curves},
       author={Xu Sun and Weichao Xu},
       journal={IEEE Signal Processing Letters},
       volume={21},
       number={11},
       pages={1389--1393},
       year={2014},
       publisher={IEEE}
     }
    """
    # Short variables are named as they are in the paper
    m = label_1_count
    n = predictions_sorted_transposed.shape[1] - m
    positive_examples = predictions_sorted_transposed[:, :m]
    negative_examples = predictions_sorted_transposed[:, m:]
    k = predictions_sorted_transposed.shape[0]

    tx = np.empty([k, m], dtype=np.float)
    ty = np.empty([k, n], dtype=np.float)
    tz = np.empty([k, m + n], dtype=np.float)
    for r in range(k):
        tx[r, :] = compute_midrank(positive_examples[r, :])
        ty[r, :] = compute_midrank(negative_examples[r, :])
        tz[r, :] = compute_midrank(predictions_sorted_transposed[r, :])
    aucs = tz[:, :m].sum(axis=1) / m / n - float(m + 1.0) / 2.0 / n
    v01 = (tz[:, :m] - tx[:, :]) / n
    v10 = 1.0 - (tz[:, m:] - ty[:, :]) / m
    sx = np.cov(v01)
    sy = np.cov(v10)
    delongcov = sx / m + sy / n
    return aucs, delongcov


def calc_pvalue(aucs, sigma):
   """Computes log(10) of p-values.
   Args:
       aucs: 1D array of AUCs
       sigma: AUC DeLong covariances
   Returns:
       log10(pvalue)
   """
   if np.diff(aucs)[0] != 0 :
      l = np.array([[1, -1]])
      z = np.abs(np.diff(aucs)) / np.sqrt(np.dot(np.dot(l, sigma), l.T))
      log10_pvalue = np.log10(2) + scipy.stats.norm.logsf(z, loc=0, scale=1) / np.log(10)
   else :
      log10_pvalue = 0
   return log10_pvalue


def compute_ground_truth_statistics(ground_truth):
    assert np.array_equal(np.unique(ground_truth), [0, 1])
    order = (-ground_truth).argsort()
    label_1_count = int(ground_truth.sum())
    return order, label_1_count


def delong_roc_variance(ground_truth, predictions):
    """
    Computes ROC AUC variance for a single set of predictions
    Args:
       ground_truth: np.array of 0 and 1
       predictions: np.array of floats of the probability of being class 1
    """
    order, label_1_count = compute_ground_truth_statistics(ground_truth)
    predictions_sorted_transposed = predictions[np.newaxis, order]
    aucs, delongcov = fastDeLong(predictions_sorted_transposed, label_1_count)
    assert len(aucs) == 1, "There is a bug in the code, please forward this to the developers"
    return aucs[0], delongcov


def delong_roc_test(ground_truth, predictions_one, predictions_two):
    """
    Computes log(p-value) for hypothesis that two ROC AUCs are different
    Args:
       ground_truth: np.array of 0 and 1
       predictions_one: predictions of the first model,
          np.array of floats of the probability of being class 1
       predictions_two: predictions of the second model,
          np.array of floats of the probability of being class 1
    """
    order, label_1_count = compute_ground_truth_statistics(ground_truth)
    predictions_sorted_transposed = np.vstack((predictions_one, predictions_two))[:, order]
    aucs, delongcov = fastDeLong(predictions_sorted_transposed, label_1_count)
    return calc_pvalue(aucs, delongcov)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                                                        To generate results of DeLong test for comparing AUC score                                                          ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def auc_delong_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    model_classifier_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_classifier_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_classifier = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_classifier[ ls_ml_models[ml_loop] ] = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            
            model_classifier_load[loop_ml_prefix] = model_classifier
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_classifier_comb[loop_file_repo] = model_classifier_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    DeLong test of all optimal model in the same cohort   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ds_cohort_comb = pd.DataFrame()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        best_ml_cohort = best_ml_model_comb[loop_file_repo]

        ls_cohort_proba = []
        ls_cohort_y = []
        for loop_ml_prefix in ml_prefix :
            
            #### Actual and predicted outcome of the optimal model in each cohort and the model with different set of features ----------
            ls_test_cohort_proba = []
            ls_test_cohort_y = []
            ls_auc = []
            for ml_loop in range(len(ls_ml_models)) :
                ls_test_cohort_proba.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].best_model_proba )
                ls_test_cohort_y.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].y_validate.tolist() )
                ls_auc.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].best_model_auc_test )
            
            # Actual and predicted outcome of the optimal model in each cohort (exclude the trivial model)
            ls_auc2 = ls_auc.copy()
            ls_cohort_proba.append( ls_test_cohort_proba[ls_auc2.index(max(ls_auc2))] )
            ls_cohort_y.append( ls_test_cohort_y[ls_auc2.index(max(ls_auc2))] )

        #### Actual and predicted outcome of the optimal model with different set of features ----------
        ds_cohort = pd.DataFrame({'Cohort' : loop_file_repo, 'Prefix' : ml_prefix})
        for loop_ii in range(len(ls_cohort_proba)) :
            list_log_pvalue = []
            list_pvalue = []
            for loop_jj in range(len(ls_cohort_proba)) :
                log_pvalue = delong_roc_test(ground_truth = np.array((ls_cohort_y[loop_ii]))
                                            ,predictions_one = np.array(ls_cohort_proba[loop_ii])
                                            ,predictions_two = np.array(ls_cohort_proba[loop_jj]) )
                list_log_pvalue.append( log_pvalue )
                list_pvalue.append( math.exp(log_pvalue) )
            
            df_cohort = pd.DataFrame({'Prefix' : ml_prefix, ml_prefix[loop_ii] : list_pvalue })
            ds_cohort = pd.merge(ds_cohort, df_cohort, how = 'left', on = ['Prefix'])
        
        # Data frame of the DeLong test (by cohort, prefix)
        ds_cohort_comb = pd.concat([ds_cohort_comb, ds_cohort], axis = 0)
    
    ds_cohort_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Cohort'+'.csv')
    ds_cohort_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Cohort'+'.html')







#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                                     To generate results of DeLong test for comparing AUC score (Normalized vs Unnormalized)                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def auc_normalized_delong_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    p_value = dict()
    norm_method = dict()
    for loop_file_repo in file_repo_prefix_multi :
        print('loop_file_repo : '+ loop_file_repo)

        p_value[loop_file_repo] = dict()
        norm_method[loop_file_repo] = dict()
        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_normalized_ml_model = []
            best_unnormalized_ml_model = []
            best_normalized_ml_model_score = []
            best_unnormalized_ml_model_score = []
            for ml_loop in range(len(ls_ml_models)) :
                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix
                ml_classifier = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                normalized_ml_score = []
                unnormalized_ml_score = []
                normalized_ml_model = []
                unnormalized_ml_model = []
                for ls_norm in range(len(ml_classifier)) :
                    ml_keys = list(ml_classifier[ls_norm].keys())[0]
                    if 'none' in ml_keys :
                        unnormalized_ml_score.append( ml_classifier[ls_norm][ml_keys].best_model_auc_test )
                        unnormalized_ml_model.append( ml_classifier[ls_norm][ml_keys] )
                    else :
                        normalized_ml_score.append( ml_classifier[ls_norm][ml_keys].best_model_auc_test )
                        normalized_ml_model.append( ml_classifier[ls_norm][ml_keys] )
                
                # Highest average score in CV
                best_normalized_ml_model.append( normalized_ml_model[ normalized_ml_score.index(max(normalized_ml_score)) ] )
                best_normalized_ml_model_score.append( max(normalized_ml_score) )
                best_unnormalized_ml_model.append( unnormalized_ml_model[ unnormalized_ml_score.index(max(unnormalized_ml_score)) ] )
                best_unnormalized_ml_model_score.append( max(unnormalized_ml_score) )

            best_normalized_model = best_normalized_ml_model[ best_normalized_ml_model_score.index(max(best_normalized_ml_model_score)) ]
            best_unnormalized_model = best_unnormalized_ml_model[ best_unnormalized_ml_model_score.index(max(best_unnormalized_ml_model_score)) ]

            # DeLong test
            log_pvalue = delong_roc_test(ground_truth = np.array(best_unnormalized_model.y_validate)
                                         ,predictions_one = np.array(best_unnormalized_model.best_model_proba)
                                         ,predictions_two = np.array(best_normalized_model.best_model_proba) )
            exp_log_pvalue = math.exp(log_pvalue)
            p_value[loop_file_repo][loop_ml_prefix] = exp_log_pvalue
            norm_method[loop_file_repo][loop_ml_prefix] = best_normalized_model.normalize_method
    
    # Table of DeLong test for each cohort, normalized vs. unnormalized
    col_names = []
    for x in ml_prefix :
        col_names.append( 'norm_method'+x )
        col_names.append(  'p_value'+x )

    df = pd.DataFrame(columns=['Cohort'] + col_names)
    for cohort, values in p_value.items():
        df = df.append({
            'Cohort': cohort.strip('/'),
            col_names[0]: norm_method[cohort][ml_prefix[0]],
            col_names[1]: values[ml_prefix[0]],
            col_names[2]: norm_method[cohort][ml_prefix[1]],
            col_names[3]: values[ml_prefix[1]]
            }, ignore_index=True)
        
    df.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Unnormalized_vc_Normalized'+'.csv')
    df.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Unnormalized_vc_Normalized'+'.html')




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                             To generate results of DeLong test for comparing AUC score (All Normalized vs Unnormalized) eith Benjamini-Hochberg                            ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def auc_all_normalized_delong_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    pd_norm_pval_comb = pd.DataFrame()

    p_value = dict()
    norm_method = dict()
    for loop_file_repo in file_repo_prefix_multi :
        print('loop_file_repo : '+ loop_file_repo)

        p_value[loop_file_repo] = dict()
        norm_method[loop_file_repo] = dict()
        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_normalized_model = dict()
            best_normalized_model_score = dict()

            # none, combat, mcombat, mean, quantile, ratioa, standardize
            list_normalized_ml = ['none', 'combat', 'mcombat', 'mean', 'quantile', 'ratioa', 'standardize']
            normalized_ml_score = dict()
            normalized_ml_model = dict()
            for ls_normalized_ml in list_normalized_ml :
                normalized_ml_score[ls_normalized_ml] = []
                normalized_ml_model[ls_normalized_ml] = []

            for ml_loop in range(len(ls_ml_models)) :
                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix
                ml_classifier = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                for ls_norm in range(len(ml_classifier)) :
                    ml_keys = list(ml_classifier[ls_norm].keys())[0]
                    # To extract the optimal model and AUC score for each normalization method
                    for ls_normalized_ml in list_normalized_ml :
                        if ls_normalized_ml in ml_keys:
                            normalized_ml_score[ls_normalized_ml].append( ml_classifier[ls_norm][ml_keys].best_model_auc_test )
                            normalized_ml_model[ls_normalized_ml].append( ml_classifier[ls_norm][ml_keys] )
                
            # Highest average score in CV
            for ls_normalized_ml in list_normalized_ml :
                idx_max_cv_auc = normalized_ml_score[ls_normalized_ml].index( max(normalized_ml_score[ls_normalized_ml]) )
                best_normalized_model[ls_normalized_ml] = normalized_ml_model[ls_normalized_ml][idx_max_cv_auc]
                best_normalized_model_score[ls_normalized_ml] = normalized_ml_score[ls_normalized_ml][idx_max_cv_auc]

            # DeLong test
            norm_mtd_1_comb = []
            norm_mtd_2_comb = []
            p_value_comb = []

            score_mtd_1 = []
            score_mtd_2 = []
            sd_mtd_1 = []
            sd_mtd_2 = []
            out_score_mtd_1 = []
            out_score_mtd_2 = []

            for norm_mtd_1 in ['none'] :
                for norm_mtd_2 in list_normalized_ml :
                    if list_normalized_ml.index(norm_mtd_1) < list_normalized_ml.index(norm_mtd_2) :
                        norm_mtd_1_comb.append( norm_mtd_1 )
                        norm_mtd_2_comb.append( norm_mtd_2 )
                        log_pvalue = delong_roc_test(ground_truth = np.array(best_normalized_model[norm_mtd_1].y_validate)
                                                 ,predictions_one = np.array(best_normalized_model[norm_mtd_1].best_model_proba)
                                                 ,predictions_two = np.array(best_normalized_model[norm_mtd_2].best_model_proba) )
                        exp_log_pvalue = math.exp(log_pvalue)
                        p_value_comb.append( exp_log_pvalue )

                        # Cross-validated scores
                        score_mtd_1.append( best_normalized_model[norm_mtd_1].best_model_auc_test )
                        score_mtd_2.append( best_normalized_model[norm_mtd_2].best_model_auc_test )
                        # Standard deviation of the cross-validated scores
                        sd_mtd_1.append( statistics.stdev( best_normalized_model[norm_mtd_1].all_model_auc_test ) )
                        sd_mtd_2.append( statistics.stdev( best_normalized_model[norm_mtd_2].all_model_auc_test ) )
                        # Hold-out scores
                        out_score_mtd_1.append( best_normalized_model[norm_mtd_1].best_model_auc_val )
                        out_score_mtd_2.append( best_normalized_model[norm_mtd_2].best_model_auc_val )
                    else :
                        pass

            # Correct for multiple hypothesis testing using Benjamini-Hochberg method
            rejected, corrected_p_values, _, _ = multipletests(p_value_comb, method='fdr_bh')
            
            # Normalization method, p-values and corrected p-values into DataFrame
            pd_norm_pval = pd.DataFrame({'Model': loop_ml_prefix
                                         ,'Cohort' : loop_file_repo.replace('/', '')
                                         ,'Method 1' : [x.upper() for x in norm_mtd_1_comb]
                                         ,'Method 2' : [x.upper() for x in norm_mtd_2_comb]
                                         ,'p-values' : p_value_comb
                                         ,'Corrected p-values' : corrected_p_values
                                         ,'CV AUC (Method 1)' : score_mtd_1
                                         ,'CV AUC (Method 2)' : score_mtd_2
                                         ,'SD AUC (Method 1)' : sd_mtd_1
                                         ,'SD AUC (Method 2)' : sd_mtd_2
                                         ,'Hold-out AUC (Method 1)' : out_score_mtd_1
                                         ,'Hold-out AUC (Method 2)' : out_score_mtd_2
                                         })
            
            # Table of DeLong test for each cohort, normalized vs. unnormalized
            pd_norm_pval_comb = pd.concat([pd_norm_pval_comb, pd_norm_pval], axis = 0)
        
    pd_norm_pval_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Unnormalized_vc_ALL_Normalized'+'.csv')
    pd_norm_pval_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'DeLong_AUC_Comparison_by_Unnormalized_vc_ALL_Normalized'+'.html')





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                             To generate AUC scores of unbiased and reduced model in cross-validation                                                         #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To find the model with highest average performance metric based on selected methods    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_find_best_ml_model(model, keyword, excl_keyword = None) :

    # List of methods
    list_keyword = [list(model[x].keys())[0] for x in range(len(model))]
    if excl_keyword == None :
        list_keyword_select = [list_keyword[x] for x in range(len(list_keyword)) if (keyword in list_keyword[x])]
        list_keyword_index = [x for x in range(len(list_keyword)) if (keyword in list_keyword[x])]
    else:
        list_keyword_select = list_keyword
        for keyi in range(len(excl_keyword)) :
            list_keyword_select = [list_keyword_select[x] for x in range(len(list_keyword_select)) if (keyword in list_keyword_select[x]) & (excl_keyword[keyi] not in list_keyword_select[x]) ]
        
        list_keyword_index = [list_keyword.index(x) for x in list_keyword_select if x in list_keyword]
    
    # List of performance metrics for each methods [To be checked]
    list_score_cv = [model[x][list(model[x].keys())[0]].all_model_auc_cv[
                        model[x][list(model[x].keys())[0]].max_features.index( model[x][list(model[x].keys())[0]].best_model_max_features ) ]
                     for x in list_keyword_index ]
    
    list_avg_score_cv = [max(model[x][list(model[x].keys())[0]].all_model_auc_test) for x in list_keyword_index ]

    list_avg_score_index = list_avg_score_cv.index( max(list_avg_score_cv) )
    list_keyword_select_index = list_keyword_select[list_avg_score_index]
    list_model_index = list_keyword_index[list_avg_score_index]
    
    return({'best_model' : model[list_model_index][list_keyword_select_index]
            ,'score_cv' : list_score_cv[list_avg_score_index]
            })

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To plot the boxplot of performance metric of the optimal models by cohort    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def unbiased_reduced_auc():

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    model_unnormalized_comb = dict()
    model_normalized_comb = dict()
    # model_normalized_addon_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        best_unnormalized_load = dict()
        best_normalized_load = dict()
        # best_normalized_addon_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :
            print('loop_ml_prefix : '+ loop_ml_prefix)
            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_unnormalized_ml_model = dict()
            best_normalized_ml_model = dict()
            # best_normalized_addon_ml_model = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                loaded_model = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                loaded_best_model = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
                    best_unnormalized_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = 'none', excl_keyword = None)
                    best_normalized_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = '', excl_keyword = ['none', 'addon'])
                    # best_normalized_addon_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = 'addon', excl_keyword = ['none'])
                else :
                    pass
                
                best_ml_model[ ls_ml_models[ml_loop] ] = dict({'best_model' : loaded_best_model
                                                               ,'score_cv' : loaded_best_model.all_model_auc_cv[ loaded_best_model.max_features.index( loaded_best_model.best_model_max_features ) ] })
                
                
            
            if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
                mean_unnormalized = [ mean(best_unnormalized_ml_model[x]['score_cv']) for x in list(best_unnormalized_ml_model.keys()) ]
                mean_normalized = [ mean(best_normalized_ml_model[x]['score_cv']) for x in list(best_normalized_ml_model.keys()) ]
                # mean_normalized_addon = [ mean(best_normalized_addon_ml_model[x]['score_cv']) for x in list(best_normalized_addon_ml_model.keys()) ]

                mean_unnormalized_index = mean_unnormalized.index( max(mean_unnormalized) )
                mean_normalized_index = mean_normalized.index( max(mean_normalized) )
                # mean_normalized_addon_index = mean_normalized_addon.index( max(mean_normalized_addon) )

                best_unnormalized_load[loop_ml_prefix] = best_unnormalized_ml_model[ list(best_unnormalized_ml_model.keys())[mean_unnormalized_index] ]
                best_normalized_load[loop_ml_prefix] = best_normalized_ml_model[ list(best_normalized_ml_model.keys())[mean_normalized_index] ]
                # best_normalized_addon_load[loop_ml_prefix] = best_normalized_addon_ml_model[ list(best_normalized_addon_ml_model.keys())[mean_normalized_addon_index] ]
            else :
                pass
            
            mean_ml_model = [ mean(best_ml_model[x]['score_cv']) for x in list(best_ml_model.keys()) ]
            best_ml_model_load[loop_ml_prefix] = best_ml_model[ list(best_ml_model.keys())[mean_ml_model.index( max(mean_ml_model) )] ]
        
        if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
            model_unnormalized_comb[loop_file_repo] = best_unnormalized_load
            model_normalized_comb[loop_file_repo] = best_normalized_load
            # model_normalized_addon_comb[loop_file_repo] = best_normalized_addon_load
        else :
            pass

        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Boxplot of unbiased vs reduced of performance metric in cross-validation for each cohort    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def define_box_properties(plot_name, color_code, label):
        for k, v in plot_name.items():
            plt.setp(plot_name.get(k), color=color_code)
            
        # use plot function to draw a small line to name the legend.
        plt.plot([], c=color_code, label=label)
        plt.legend()

    list_cohort_inverse = [ file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] for x in range(len(file_repo_prefix_loop)) ]
    list_cohort_name_inverse = [ file_repo_prefix_name[ len(file_repo_prefix_name)-x-1 ] for x in range(len(file_repo_prefix_name)) ]

    for loop_ml_prefix in ml_prefix :
        if loop_ml_prefix == ml_prefix[0] :
            unbiased = [ best_ml_model_comb[x][loop_ml_prefix]['score_cv'] for x in list_cohort_inverse ]
        
        else :
            reduced = [ best_ml_model_comb[x][loop_ml_prefix]['score_cv'] for x in list_cohort_inverse ]

            fig = plt.figure(figsize=(12, 5), linewidth = 0.5)
            plt.style.context("seaborn-white")
            plt.axes().set_facecolor('#F8F8F7')

            unbiased_plot = plt.boxplot(unbiased, vert = False, positions = np.array(np.arange(len(unbiased)))*2.0+0.35
                                        ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})
            reduced_plot = plt.boxplot(reduced, vert = False, positions = np.array(np.arange(len(reduced)))*2.0-0.35
                                       ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})

            # setting colors for each groups
            define_box_properties(unbiased_plot, '#D7191C', ml_prefix_label[0])
            define_box_properties(reduced_plot, '#2C7BB6', ml_prefix_label[ml_prefix.index(loop_ml_prefix)])

            # Set the y label values
            plt.yticks(np.arange(0, len(list_cohort_name_inverse) * 2, 2), list_cohort_name_inverse)

            # set the limit for x axis
            plt.ylim(-2, len(list_cohort_name_inverse)*2)

            # set the limit for x axis
            ls_values = []
            for xx in unbiased :
                ls_values += xx
            for xx in reduced :
                ls_values += xx

            min_xlim = round(min(ls_values), 1)-0.1
            max_xlim = min([round(max(ls_values))+0.1, 1.0])
            plt.xlim(min_xlim, max_xlim)

            fig.savefig(file_repo+'CROSS_COHORT/'+set_outcome+'/Performance_Metric/Unbiased_vs_Reduced'+loop_ml_prefix+'.png', bbox_inches='tight', dpi=300)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Boxplot of unnormalized, normalized, and addon normalized of performance metric in cross-validation for each cohort    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    list_norm_inverse  = [ file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] for x in range(len(file_repo_prefix_loop))
                          if file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] in list(model_unnormalized_comb.keys()) ]
    list_norm_name_inverse = [file_repo_prefix_name[ file_repo_prefix_loop.index(x) ] for x in list_norm_inverse]

    for loop_ml_prefix in ml_prefix :
        unnormalized = [ model_unnormalized_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]
        normalized = [ model_normalized_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]
        # normalized_addon = [ model_normalized_addon_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]

        fig_norm = plt.figure(figsize=(12, 5), linewidth = 0.5)
        plt.style.context("seaborn-white")
        plt.axes().set_facecolor('#F8F8F7')

        unnormalized_plot = plt.boxplot(unnormalized, vert = False, positions = np.array(np.arange(len(unnormalized)))*3.0+0.7
                                        ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})
        normalized_plot = plt.boxplot(normalized, vert = False, positions = np.array(np.arange(len(normalized)))*3.0-0
                                      ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})

        # setting colors for each groups
        define_box_properties(unnormalized_plot, '#D7191C', 'Unnormalized')
        define_box_properties(normalized_plot, '#2C7BB6', 'Normalized')
        # define_box_properties(normalized_addon_plot, '#6929F3', 'Addon Normalized')

        # Set the y label values
        plt.yticks(np.arange(0, len(list_norm_name_inverse) * 3, 3), list_norm_name_inverse)

        # set the limit for x axis
        plt.ylim(-3, len(list_norm_name_inverse)*3)

        # set the limit for x axis
        ls_norm_values = []
        for xx in unnormalized :
            ls_norm_values += xx
        for xx in normalized :
            ls_norm_values += xx
        # for xx in normalized_addon :
        #     ls_norm_values += xx

        min_norm_xlim = round(min(ls_norm_values), 1)-0.1
        max_norm_xlim = min([round(max(ls_norm_values))+0.1, 1.0])
        plt.xlim(min_norm_xlim, max_norm_xlim)

        fig_norm.savefig(file_repo+'CROSS_COHORT/'+set_outcome+'/Performance_Metric/Unnormalized_vs_Normalized'+loop_ml_prefix+'.png', bbox_inches='tight', dpi=300)
