#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import LuxPARK Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from datetime import datetime, timedelta

data_luxpark = pd.read_json("Analysis/Data/LuxPARK/lux_park_clinical.json")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    LuxPARK Data Processing    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
class preprocess_luxpark:

        def __init__(self, data, data_variables, vars_info, event_yr = "BL"):
                
                self.data = data
                self.data_variables = data_variables
                self.vars_info = vars_info
                self.event_yr = event_yr
                

        

        def process_luxpark(self):

                data_variables = self.data_variables
                data = self.data
               
                # Inclusion and exclusion criteria
                data_inclusion = data[  (data['inc_q1___1'] == True)
                                        & (data['inc_q1___2'] == True)
                                        & (data['inc_q1___3'] == False)
                                        & (data['inc_q1___4'] == True)
                                        & (data['inc_q2___1'] == False)
                                        & (data['inc_q2___2'] == False)
                                        & (data['inc_q2___3'] == False)
                                        & (data['inc_q2___4'] == False)
                                        & (data['inc_q2___5'] == False)
                                        & (data['inc_q2___6'] == False)
                                        & (data['inclusionexclusion_criteria_complete'] != '0')]
                
                # To extract non-control subject
                data_inclusion = data_inclusion[ (data_inclusion['diag_control'] == 0) ]

                # To include the patient with idiopathic Parkinson's disease (IPD)
                data_inclusion = data_inclusion[data_inclusion['diag_ipd'] == 1]

                # To include the subjects fulfil the inclusion criteria
                data = data[ data['cdisc_dm_usubjd'].isin(np.unique(data_inclusion['cdisc_dm_usubjd']).tolist()) ]

                # To convert the variables into datetime
                # data['sv_basic_date'] = data['sv_basic_date'].apply(lambda d: datetime.datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
                data['sv_basic_date'] = data['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
                data['YEAR'] = pd.DatetimeIndex(data['sv_basic_date']).year

                # To obtain max(sv_basic_date) or dm_death_date
                data_dm = data[['cdisc_dm_usubjd', 'dm_death', 'dm_death_date']].dropna()
                data_dm['dm_death_date'] = data_dm['dm_death_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
                data_dm2 = data_dm[data_dm['dm_death'] == 1].groupby('cdisc_dm_usubjd').agg('min').reset_index()

                data_fu = data[['cdisc_dm_usubjd', 'dm_death', 'sv_basic_date']].groupby('cdisc_dm_usubjd').agg('max').reset_index()
                data_fu = data_fu.merge(data_dm2, how = 'left', on = ['cdisc_dm_usubjd'], suffixes = ['', '_dm'])
                data_fu['LAST_FOLLOW_UP_DATE'] = np.where((data_fu['dm_death'] == 1) & (data_fu['dm_death_dm'] == 1), data_fu['dm_death_date'], data_fu['sv_basic_date'])
                data_fu = data_fu[['cdisc_dm_usubjd', 'dm_death', 'LAST_FOLLOW_UP_DATE']]

                # Rank of the date by subject
                data = data.sort_values(['cdisc_dm_usubjd', 'sv_basic_date'])
                data['sv_rank'] = data.groupby(['cdisc_dm_usubjd'])['sv_basic_date'].rank(ascending = True)

                # EVENT ID
                data_bs_id = data[ data['sv_rank'] == 1 ][['cdisc_dm_usubjd', 'sv_basic_date']]
                data_bs_id.rename(columns = {'sv_basic_date': 'baseline_date'}, inplace = True)
                data_event_id = pd.merge(data_bs_id, data[['cdisc_dm_usubjd', 'sv_basic_date', 'sv_rank']], how = 'left', on = ['cdisc_dm_usubjd'])
                data_event_id['diff_days'] = ( pd.DatetimeIndex(data_event_id['sv_basic_date']) - pd.DatetimeIndex(data_event_id['baseline_date']) ) / np.timedelta64(1, 'D')
                data_event_id.loc[ data_event_id['sv_rank'] == 1, 'EVENT_ID'] = "BL"
                data_event_id.loc[ (data_event_id['diff_days'] > 0) & (data_event_id['diff_days'] <= (365.25 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y1"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25) & (data_event_id['diff_days'] <= (365.25*2 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y2"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*2) & (data_event_id['diff_days'] <= (365.25*3 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y3"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*3) & (data_event_id['diff_days'] <= (365.25*4 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y4"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*4) & (data_event_id['diff_days'] <= (365.25*5 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y5"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*5) & (data_event_id['diff_days'] <= (365.25*6 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y6"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*6) & (data_event_id['diff_days'] <= (365.25*7 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y7"
                data_event_id.loc[ (data_event_id['diff_days'] > 365.25*7) & (data_event_id['diff_days'] <= (365.25*8 + 60)) & (data_event_id['EVENT_ID'].isnull()), 'EVENT_ID'] = "Y8"
                data_event_id = data_event_id[['cdisc_dm_usubjd', 'sv_basic_date', 'EVENT_ID']]

                # To extract baseline information
                data = pd.merge( data, data_event_id, how = 'left', on = ['cdisc_dm_usubjd', 'sv_basic_date'] )
                # data = data[data['sv_rank'] == 1]
                data = data[ data['EVENT_ID'] == self.event_yr ]

                # Keep the data with minimum sv_basic_date (baseline)
                dt_min_date = data[['cdisc_dm_usubjd', 'sv_basic_date']]
                dt_min_date = dt_min_date.groupby('cdisc_dm_usubjd')['sv_basic_date'].max().reset_index()
                data = pd.merge(dt_min_date, data, how = "left", on = ['cdisc_dm_usubjd', 'sv_basic_date'])

                # Duration of PD from diagnosis to enrollment (year)
                data['duration'] = np.where( (data['question260_260'] > 0) & (~data['question260_260'].isnull()) ,pd.DatetimeIndex(data['sv_basic_date']).year - data['question260_260'], np.nan )

                # Age of onset
                data['age'] = data['sv_age'] - data['duration']

                # Recode the values
                data['gdsl_15'].replace(to_replace = 0, value = 1, inplace = True)
                data['gdsl_21'].replace(to_replace = 1, value = 0, inplace = True)
                data['gdsl_7'].replace(to_replace = 0, value = 1, inplace = True)
                data['gdsl_9'].replace(to_replace = 0, value = 1, inplace = True)
                data['gdsl_1'].replace(to_replace = 0, value = 1, inplace = True)

                # Recode 1.5 as 1, 2.5 as 2, and 6 as missing
                data['hoehn_and_yahr_staging'].replace(to_replace = 1.5, value = 1, inplace = True)
                data['hoehn_and_yahr_staging'].replace(to_replace = 2.5, value = 2, inplace = True)
                data['hoehn_and_yahr_staging'].replace(to_replace = 6, value = np.nan, inplace = True)
                # data['hoehn_and_yahr_staging'].replace(to_replace = 6, value = pd.NA, inplace = True)
                data['hoehn_and_yahr_staging'] = pd.to_numeric(data['hoehn_and_yahr_staging'], downcast = 'float')

                # Recode 1 to 0, 0 to 1
                data['tmt_gt_5min'].replace(to_replace = 1, value = 2, inplace = True)
                data['tmt_gt_5min'].replace(to_replace = 0, value = 1, inplace = True)
                data['tmt_gt_5min'].replace(to_replace = 2, value = 0, inplace = True)

                # MDS-UPDRS Part III (OFF)
                # Note: this part will be ignored because no off state assessment in LUXPARK
                # But keep the code to ensure the code will still be working
                data_mds3 = data[['cdisc_dm_usubjd', 'sv_basic_date'
                                ,'u_q3_3c', 'u_q3_3c_1'
                                ,'u_q3_14', 'u_q3_2', 'u_q3_11', 'u_q3_4_2', 'u_q3_4_1', 'u_q3_10', 'u_q3_5_2', 'u_q3_5_1', 'u_q3_16_2'
                                ,'u_q3_16_1', 'u_q3_8_2', 'u_q3_8_1', 'u_q3_13', 'u_q3_6_2', 'u_q3_6_1', 'u_q3_12', 'u_q3_15_2'
                                ,'u_q3_15_1', 'u_q3_3_5', 'u_q3_3_3', 'u_q3_3_1', 'u_q3_3_4', 'u_q3_3_2', 'u_q3_9', 'u_q3_17_5'
                                ,'u_q3_17_4', 'u_q3_17_2', 'u_q3_17_3', 'u_q3_17_1', 'u_q3_18', 'u_q3_1', 'u_q3_7_2', 'u_q3_7_1' ]]
                data_mds3 = data_mds3[ ((data_mds3['u_q3_3c'] == 1) & (data_mds3['u_q3_3c_1'] >= 360)) | (data_mds3['u_q3_3c'] != 1) ]
                data_mds3.rename(columns = {'u_q3_14': 'NP3BRADY'       ,'u_q3_2': 'NP3FACXP'       ,'u_q3_11': 'NP3FRZGT'
                                        ,'u_q3_4_2': 'NP3FTAPL'      ,'u_q3_4_1': 'NP3FTAPR'     ,'u_q3_10': 'NP3GAIT'
                                        ,'u_q3_5_2': 'NP3HMOVL'      ,'u_q3_5_1': 'NP3HMOVR'     ,'u_q3_16_2': 'NP3KTRML'
                                        ,'u_q3_16_1': 'NP3KTRMR'     ,'u_q3_8_2': 'NP3LGAGL'     ,'u_q3_8_1': 'NP3LGAGR'
                                        ,'u_q3_13': 'NP3POSTR'       ,'u_q3_6_2': 'NP3PRSPL'     ,'u_q3_6_1': 'NP3PRSPR'
                                        ,'u_q3_12': 'NP3PSTBL'       ,'u_q3_15_2': 'NP3PTRML'    ,'u_q3_15_1': 'NP3PTRMR'
                                        ,'u_q3_3_5': 'NP3RIGLL'      ,'u_q3_3_3': 'NP3RIGLU'     ,'u_q3_3_1': 'NP3RIGN'
                                        ,'u_q3_3_4': 'NP3RIGRL'      ,'u_q3_3_2': 'NP3RIGRU'     ,'u_q3_9': 'NP3RISNG'
                                        ,'u_q3_17_5': 'NP3RTALJ'     ,'u_q3_17_4': 'NP3RTALL'    ,'u_q3_17_2': 'NP3RTALU'
                                        ,'u_q3_17_3': 'NP3RTARL'     ,'u_q3_17_1': 'NP3RTARU'    ,'u_q3_18': 'NP3RTCON'
                                        ,'u_q3_1': 'NP3SPCH'         ,'u_q3_7_2': 'NP3TTAPL'     ,'u_q3_7_1': 'NP3TTAPR'
                                                }, inplace = True)
                data_mds3 = data_mds3.drop(['u_q3_3c', 'u_q3_3c_1'], axis = 1)
                data = pd.merge(data, data_mds3, how = "left", on = ['cdisc_dm_usubjd', 'sv_basic_date'])

                data['updrs3_score'] = data['NP3BRADY'] + data['NP3FACXP'] + data['NP3FRZGT'] +\
                                        data['NP3FTAPL'] + data['NP3FTAPR'] + data['NP3GAIT'] +\
                                        data['NP3HMOVL'] + data['NP3HMOVR'] + data['NP3KTRML'] +\
                                        data['NP3KTRMR'] + data['NP3LGAGL'] + data['NP3LGAGR'] +\
                                        data['NP3POSTR'] + data['NP3PRSPL'] + data['NP3PRSPR'] +\
                                        data['NP3PSTBL'] + data['NP3PTRML'] + data['NP3PTRMR'] +\
                                        data['NP3RIGLL'] + data['NP3RIGLU'] + data['NP3RIGN'] +\
                                        data['NP3RIGRL'] + data['NP3RIGRU'] + data['NP3RISNG'] +\
                                        data['NP3RTALJ'] + data['NP3RTALL'] + data['NP3RTALU'] +\
                                        data['NP3RTARL'] + data['NP3RTARU'] + data['NP3RTCON'] +\
                                        data['NP3SPCH']  + data['NP3TTAPL'] + data['NP3TTAPR']

                # MDS-UPDRS Part III (ON)
                data_mds3_on = data[['cdisc_dm_usubjd', 'sv_basic_date'
                                        ,'u_q3_3c', 'u_q3_3c_1'
                                        ,'u_q3_14', 'u_q3_2', 'u_q3_11', 'u_q3_4_2', 'u_q3_4_1', 'u_q3_10', 'u_q3_5_2', 'u_q3_5_1', 'u_q3_16_2'
                                        ,'u_q3_16_1', 'u_q3_8_2', 'u_q3_8_1', 'u_q3_13', 'u_q3_6_2', 'u_q3_6_1', 'u_q3_12', 'u_q3_15_2'
                                        ,'u_q3_15_1', 'u_q3_3_5', 'u_q3_3_3', 'u_q3_3_1', 'u_q3_3_4', 'u_q3_3_2', 'u_q3_9', 'u_q3_17_5'
                                        ,'u_q3_17_4', 'u_q3_17_2', 'u_q3_17_3', 'u_q3_17_1', 'u_q3_18', 'u_q3_1', 'u_q3_7_2', 'u_q3_7_1']]
                # data_mds3_on = data_mds3_on[ (data_mds3_on['u_q3_3c'] == 1) & (data_mds3_on['u_q3_3c_1'] < 360) ]
                data_mds3_on.rename(columns = {'u_q3_14': 'NP3BRADY_ON'       ,'u_q3_2': 'NP3FACXP_ON'       ,'u_q3_11': 'NP3FRZGT_ON'
                                                ,'u_q3_4_2': 'NP3FTAPL_ON'      ,'u_q3_4_1': 'NP3FTAPR_ON'     ,'u_q3_10': 'NP3GAIT_ON'
                                                ,'u_q3_5_2': 'NP3HMOVL_ON'      ,'u_q3_5_1': 'NP3HMOVR_ON'     ,'u_q3_16_2': 'NP3KTRML_ON'
                                                ,'u_q3_16_1': 'NP3KTRMR_ON'     ,'u_q3_8_2': 'NP3LGAGL_ON'     ,'u_q3_8_1': 'NP3LGAGR_ON'
                                                ,'u_q3_13': 'NP3POSTR_ON'       ,'u_q3_6_2': 'NP3PRSPL_ON'     ,'u_q3_6_1': 'NP3PRSPR_ON'
                                                ,'u_q3_12': 'NP3PSTBL_ON'       ,'u_q3_15_2': 'NP3PTRML_ON'    ,'u_q3_15_1': 'NP3PTRMR_ON'
                                                ,'u_q3_3_5': 'NP3RIGLL_ON'      ,'u_q3_3_3': 'NP3RIGLU_ON'     ,'u_q3_3_1': 'NP3RIGN_ON'
                                                ,'u_q3_3_4': 'NP3RIGRL_ON'      ,'u_q3_3_2': 'NP3RIGRU_ON'     ,'u_q3_9': 'NP3RISNG_ON'
                                                ,'u_q3_17_5': 'NP3RTALJ_ON'     ,'u_q3_17_4': 'NP3RTALL_ON'    ,'u_q3_17_2': 'NP3RTALU_ON'
                                                ,'u_q3_17_3': 'NP3RTARL_ON'     ,'u_q3_17_1': 'NP3RTARU_ON'    ,'u_q3_18': 'NP3RTCON_ON'
                                                ,'u_q3_1': 'NP3SPCH_ON'         ,'u_q3_7_2': 'NP3TTAPL_ON'     ,'u_q3_7_1': 'NP3TTAPR_ON'
                                                }, inplace = True)
                data_mds3_on = data_mds3_on.drop(['u_q3_3c', 'u_q3_3c_1'], axis = 1)
                data = pd.merge(data, data_mds3_on, how = "left", on = ['cdisc_dm_usubjd', 'sv_basic_date'])

                data['updrs3_score_on'] = data['NP3BRADY_ON'] + data['NP3FACXP_ON'] + data['NP3FRZGT_ON'] +\
                                        data['NP3FTAPL_ON'] + data['NP3FTAPR_ON'] + data['NP3GAIT_ON'] +\
                                        data['NP3HMOVL_ON'] + data['NP3HMOVR_ON'] + data['NP3KTRML_ON'] +\
                                        data['NP3KTRMR_ON'] + data['NP3LGAGL_ON'] + data['NP3LGAGR_ON'] +\
                                        data['NP3POSTR_ON'] + data['NP3PRSPL_ON'] + data['NP3PRSPR_ON'] +\
                                        data['NP3PSTBL_ON'] + data['NP3PTRML_ON'] + data['NP3PTRMR_ON'] +\
                                        data['NP3RIGLL_ON'] + data['NP3RIGLU_ON'] + data['NP3RIGN_ON'] +\
                                        data['NP3RIGRL_ON'] + data['NP3RIGRU_ON'] + data['NP3RISNG_ON'] +\
                                        data['NP3RTALJ_ON'] + data['NP3RTALL_ON'] + data['NP3RTALU_ON'] +\
                                        data['NP3RTARL_ON'] + data['NP3RTARU_ON'] + data['NP3RTCON_ON'] +\
                                        data['NP3SPCH_ON']  + data['NP3TTAPL_ON'] + data['NP3TTAPR_ON']
                
                # Family history of Parkinson's disease
                data['fampd_old'] = np.where( (data['question219_219']==1) | (data['rfqu_fh_father4']==1) | (data['rfqu_fh_mother4']==1) |\
                                                (data['rfqu_fh_brother1_4']==1) | (data['rfqu_fh_brother2_4']==1) | (data['rfqu_fh_brother3_4']==1) | (data['rfqu_fh_brother4_4']==1) |\
                                                 (data['rfqu_fh_brother5_4']==1) | (data['rfqu_fh_brother6_4']==1) | (data['rfqu_fh_brother7_4']==1) | (data['rfqu_fh_brother8_4']==1) |\
                                                  (data['rfqu_fh_sister1_4']==1) | (data['rfqu_fh_sister2_4']==1) | (data['rfqu_fh_sister3_4']==1) | (data['rfqu_fh_sister4_4']==1) |\
                                                   (data['rfqu_fh_sister5_4']==1) | (data['rfqu_fh_sister6_4']==1) | (data['rfqu_fh_sister7_4']==1) | (data['rfqu_fh_sister8_4']==1) |\
                                                    (data['rfqu_fh_child1_4']==1) | (data['rfqu_fh_child2_4']==1) | (data['rfqu_fh_child3_4']==1) | (data['rfqu_fh_child4_4']==1) |\
                                                     (data['rfqu_fh_child5_4']==1) | (data['rfqu_fh_child6_4']==1) | (data['rfqu_fh_child7_4']==1) | (data['rfqu_fh_child8_4']==1), 1\
                                                      ,np.where( (data['question219_219']==0) | (data['rfqu_fh_father4']==2) | (data['rfqu_fh_mother4']==2) |\
                                                                (data['rfqu_fh_brother1_4']==2) | (data['rfqu_fh_brother2_4']==2) | (data['rfqu_fh_brother3_4']==2) | (data['rfqu_fh_brother4_4']==2) |\
                                                                (data['rfqu_fh_brother5_4']==2) | (data['rfqu_fh_brother6_4']==2) | (data['rfqu_fh_brother7_4']==2) | (data['rfqu_fh_brother8_4']==2) |\
                                                                (data['rfqu_fh_sister1_4']==2) | (data['rfqu_fh_sister2_4']==2) | (data['rfqu_fh_sister3_4']==2) | (data['rfqu_fh_sister4_4']==2) |\
                                                                (data['rfqu_fh_sister5_4']==2) | (data['rfqu_fh_sister6_4']==2) | (data['rfqu_fh_sister7_4']==2) | (data['rfqu_fh_sister8_4']==2) |\
                                                                (data['rfqu_fh_child1_4']==2) | (data['rfqu_fh_child2_4']==2) | (data['rfqu_fh_child3_4']==2) | (data['rfqu_fh_child4_4']==2) |\
                                                                (data['rfqu_fh_child5_4']==2) | (data['rfqu_fh_child6_4']==2) | (data['rfqu_fh_child7_4']==2) | (data['rfqu_fh_child8_4']==2), 0, np.nan ) )

                # To define a variable to be same as PPMI cohort
                data['APPRDX'] = 1
                # data['EVENT_ID'] = "BL"

                data['updrs_totscore'] = data['u_part1_score'] + data['u_part2_score'] + data['updrs3_score'] + data['u_part4_score']
                data['updrs_totscore_on'] = data['u_part1_score'] + data['u_part2_score'] + data['updrs3_score_on'] + data['u_part4_score']

                data['scopa_sex'] = data['scopa_subscore_sexwomen'] + data['scopa_subscore_sexmen']
                data['scopa'] = data['scopa_subscore_gastro'] + data['scopa_subscore_urinary'] +\
                                data['scopa_subscore_cardiovasc'] + data['scopa_subscore_thermo'] +\
                                data['scopa_sex']
                
                data['rem_cat'] = pd.cut(data['rem_score'], bins = [-1, 4, 20], labels = [0, 1])

                # To extract LRRK2 mutation
                data.loc[ data['control_q7___1'] == True, 'LRRK2' ] = 1
                data.loc[ data['control_q7___1'] == False, 'LRRK2' ] = 0
                
                # To extract GBA mutation
                data.loc[ data['control_q7___2'] == True, 'GBA' ] = 1
                data.loc[ data['control_q7___2'] == False, 'GBA' ] = 0
                
                # To extract Levodopa treatment
                data['levodopa'] = np.where( (data['u_q3_3c']==1) | (data['meds_n04b___1']==1) | (data['standup_test4']==1) | (data['fogac_test4']==1), 1
                                            ,np.where( (~data['u_q3_3c'].isnull()) | (~data['meds_n04b___1'].isnull()) | (~data['standup_test4'].isnull()) | (~data['fogac_test4'].isnull()), 0, np.nan ) )

                # To extract initial motor symptom - Bradykinesia
                data.loc[ (data['question262_262___3']==False) & (data['question262_262___4']==False) & (data['question262_262___5']==False)\
                        | (data['question262_262___6']==False) & (data['question262_262___7']==False) & (data['question262_262___8']==False)\
                        | (data['question262_262___9']==False) & (data['question262_262___10']==False) & (data['question262_262___11']==False), 'DXBRADY' ] = 0
                data.loc[ (data['question262_262___3']==True) | (data['question262_262___4']==True) | (data['question262_262___5']==True)\
                        | (data['question262_262___6']==True) | (data['question262_262___7']==True) | (data['question262_262___8']==True)\
                        | (data['question262_262___9']==True) | (data['question262_262___10']==True) | (data['question262_262___11']==True), 'DXBRADY' ] = 1
                
                # To extract initial motor symptom - Resting Tremor
                data.loc[ data['question262_262___1'] == True, 'DXTREMOR' ] = 1
                data.loc[ data['question262_262___1'] == False, 'DXTREMOR' ] = 0

                # To extract initial motor symptom - Rigidity
                data.loc[ data['question262_262___12'] == True, 'DXRIGID' ] = 1
                data.loc[ data['question262_262___12'] == False, 'DXRIGID' ] = 0

                # Last follow up date
                data = data.merge(data_fu, how = 'left', on = ['cdisc_dm_usubjd'], suffixes = ['', '_status'])
                # data['LAST_FOLLOW_UP_DATE'] = data['LAST_FOLLOW_UP_DATE'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
                data['LAST_FOLLOW_UP_DATE'] = pd.to_datetime(data['LAST_FOLLOW_UP_DATE'], format = '%Y-%m-%d')
                data['sv_basic_date'] = pd.to_datetime(data['sv_basic_date'], format = '%Y-%m-%d')

                # To measure number of days from first visit to last follow-up or death
                data['FOLLOW_UP_DAYS'] = data['LAST_FOLLOW_UP_DATE'] - data['sv_basic_date']
                data['FOLLOW_UP_DAYS'] = data['FOLLOW_UP_DAYS'].dt.days

                # Dyskinesias : u_q4_2, u_q4_1, u_q4_1_3, question273_273___17
                data.loc[ ((data['u_q4_2'] == 0) & (data['u_q4_1'] == 0) & (data['u_q4_1_3'] == 0)) | (data['question273_273___17'] == False), 'NP4DYSKI2' ] = 0
                data.loc[ (data['u_q4_2'] > 0) | (data['u_q4_1'] > 0) | (data['u_q4_1_3'] > 0) | (data['question273_273___17'] == True), 'NP4DYSKI2' ] = 1

                # Motor fluctuation
                data['NP4FLCTX2'] = np.where( (data['u_q4_3'] > 0) | (data['u_q4_4'] > 0) | (data['u_q4_5'] > 0), 1
                                             ,np.where( (data['u_q4_3'] == 0) | (data['u_q4_4'] == 0) | (data['u_q4_5'] == 0), 0, np.nan) )
                
                # Impact of cognitive impairment in daily living
                data['NP1COG2'] = np.where( data['u_q1_1'].isin([0, 1]), 0, np.where( data['u_q1_1'].isin([2, 3, 4]), 1, np.nan ) )

                # Gait disorder
                data['GAITRSP'] = np.where( data['question262_262___15'] == True, 1, np.where( data['question262_262___15'] == False, 0, np.nan ) )

                # DXRIGID = 1 or DXBRADY = 1
                data['DXGRB'] = np.where( (data['DXRIGID'] == 1) | (data['DXBRADY'] == 1), 1
                                         ,np.where( (data['DXRIGID'] == 0) | (data['DXBRADY'] == 0), 0, np.nan ) )
                
                # Motor fluctuation composite score
                # data['fluctuation'] = np.where( (~data['u_q4_3'].isnull()) & (~data['u_q4_4'].isnull()) & (~data['u_q4_5'].isnull())
                #                         ,data['u_q4_3'] + data['u_q4_4'] + data['u_q4_5'], np.nan )
                data['fluctuation_mean'] = data[['u_q4_3', 'u_q4_4', 'u_q4_5']].mean(axis = 1)
                data['tmp_u_q4_3'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['u_q4_3'].isnull() ), data['fluctuation_mean'], data['u_q4_3'] )
                data['tmp_u_q4_4'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['u_q4_4'].isnull() ), data['fluctuation_mean'], data['u_q4_4'] )
                data['tmp_u_q4_5'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['u_q4_5'].isnull() ), data['fluctuation_mean'], data['u_q4_5'] )
                data['fluctuation'] = np.where( ~data['fluctuation_mean'].isnull(), data[['tmp_u_q4_3', 'tmp_u_q4_4', 'tmp_u_q4_5']].sum(axis = 1), np.nan )
                
                # MDS-UPDRS III - Rigidity
                # Missing value imputation: average of MDS-UPDRS III Rigidity
                data['rigid_mean'] = data[['NP3RIGLL_ON', 'NP3RIGLU_ON', 'NP3RIGN_ON', 'NP3RIGRL_ON', 'NP3RIGRU_ON']].mean(axis = 1)
                data['NP3RIGLL_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGLL_ON'].isnull() ), data['rigid_mean'], data['NP3RIGLL_ON'] )
                data['NP3RIGLU_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGLU_ON'].isnull() ), data['rigid_mean'], data['NP3RIGLU_ON'] )
                data['NP3RIGN_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGN_ON'].isnull() ), data['rigid_mean'], data['NP3RIGN_ON'] )
                data['NP3RIGRL_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGRL_ON'].isnull() ), data['rigid_mean'], data['NP3RIGRL_ON'] )
                data['NP3RIGRU_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGRU_ON'].isnull() ), data['rigid_mean'], data['NP3RIGRU_ON'] )

                # Rigidity upper extremities score
                data['rigidity_upper'] = np.where( ~data['rigid_mean'].isnull(), data[['NP3RIGLU_ON', 'NP3RIGRU_ON']].sum(axis = 1), np.nan )

                # Rigidity lower extremities score
                data['rigidity_lower'] = np.where( ~data['rigid_mean'].isnull(), data[['NP3RIGLL_ON', 'NP3RIGRL_ON']].sum(axis = 1), np.nan )

                # Total rigidity score
                data['rigidity_sum'] = np.where( ~data['rigid_mean'].isnull()
                                                ,data[['NP3RIGLL_ON', 'NP3RIGLU_ON', 'NP3RIGN_ON', 'NP3RIGRL_ON', 'NP3RIGRU_ON']].sum(axis = 1)
                                                ,np.nan )
                
                # MDS-UPDRS III : Bradykinesia
                data['brady_mean'] = data[['NP3FACXP_ON', 'NP3FTAPL_ON', 'NP3FTAPR_ON', 'NP3HMOVL_ON', 'NP3HMOVR_ON', 'NP3PRSPL_ON', 'NP3PRSPR_ON', 'NP3TTAPL_ON', 'NP3TTAPR_ON'
                                        ,'NP3LGAGL_ON', 'NP3LGAGR_ON', 'NP3BRADY_ON']].mean(axis = 1)
                data['tmp_NP3FACXP_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FACXP_ON'].isnull() ), data['brady_mean'], data['NP3FACXP_ON'] )
                data['tmp_NP3FTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FTAPL_ON'].isnull() ), data['brady_mean'], data['NP3FTAPL_ON'] )
                data['tmp_NP3FTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FTAPR_ON'].isnull() ), data['brady_mean'], data['NP3FTAPR_ON'] )
                data['tmp_NP3HMOVL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3HMOVL_ON'].isnull() ), data['brady_mean'], data['NP3HMOVL_ON'] )
                data['tmp_NP3HMOVR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3HMOVR_ON'].isnull() ), data['brady_mean'], data['NP3HMOVR_ON'] )
                data['tmp_NP3PRSPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3PRSPL_ON'].isnull() ), data['brady_mean'], data['NP3PRSPL_ON'] )
                data['tmp_NP3PRSPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3PRSPR_ON'].isnull() ), data['brady_mean'], data['NP3PRSPR_ON'] )
                data['tmp_NP3TTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3TTAPL_ON'].isnull() ), data['brady_mean'], data['NP3TTAPL_ON'] )
                data['tmp_NP3TTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3TTAPR_ON'].isnull() ), data['brady_mean'], data['NP3TTAPR_ON'] )
                data['tmp_NP3LGAGL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3LGAGL_ON'].isnull() ), data['brady_mean'], data['NP3LGAGL_ON'] )
                data['tmp_NP3LGAGR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3LGAGR_ON'].isnull() ), data['brady_mean'], data['NP3LGAGR_ON'] )
                data['tmp_NP3BRADY_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3BRADY_ON'].isnull() ), data['brady_mean'], data['NP3BRADY_ON'] )

                # Bradykinesia score
                data['bradykinesia'] = np.where( ~data['brady_mean'].isnull()
                                                ,data[['tmp_NP3FACXP_ON', 'tmp_NP3FTAPL_ON', 'tmp_NP3FTAPR_ON', 'tmp_NP3HMOVL_ON', 'tmp_NP3HMOVR_ON', 'tmp_NP3PRSPL_ON'
                                                , 'tmp_NP3PRSPR_ON', 'tmp_NP3TTAPL_ON', 'tmp_NP3TTAPR_ON', 'tmp_NP3LGAGL_ON', 'tmp_NP3LGAGR_ON', 'tmp_NP3BRADY_ON']].sum(axis = 1)
                                                ,np.nan )

                # MDS-UPDRS III : Axial symptoms
                data['NP2WALK'] = data['u_q2_12']
                data['NP2FREZ'] = data['u_q2_13']
                data['axial_mean'] = data[['NP2WALK', 'NP2FREZ', 'NP3SPCH_ON', 'NP3RISNG_ON', 'NP3FRZGT_ON', 'NP3PSTBL_ON', 'NP3POSTR_ON']].mean(axis = 1)
                data['tmp_NP2WALK'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP2WALK'].isnull() ), data['axial_mean'], data['NP2WALK'] )
                data['tmp_NP2FREZ'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP2FREZ'].isnull() ), data['axial_mean'], data['NP2FREZ'] )
                data['tmp_NP3SPCH_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3SPCH_ON'].isnull() ), data['axial_mean'], data['NP3SPCH_ON'] )
                data['tmp_NP3RISNG_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3RISNG_ON'].isnull() ), data['axial_mean'], data['NP3RISNG_ON'] )
                data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3FRZGT_ON'].isnull() ), data['axial_mean'], data['NP3FRZGT_ON'] )
                data['tmp_NP3PSTBL_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3PSTBL_ON'].isnull() ), data['axial_mean'], data['NP3PSTBL_ON'] )
                data['tmp_NP3POSTR_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3POSTR_ON'].isnull() ), data['axial_mean'], data['NP3POSTR_ON'] )

                # Axial symptoms score
                data['axial'] = np.where( ~data['axial_mean'].isnull()
                                                ,data[['tmp_NP2WALK', 'tmp_NP2FREZ', 'tmp_NP3SPCH_ON', 'tmp_NP3RISNG_ON', 'tmp_NP3FRZGT_ON', 'tmp_NP3PSTBL_ON', 'tmp_NP3POSTR_ON']].sum(axis = 1)
                                                ,np.nan )
        
                # MDS-UPDRS III : Axial symptoms (selective)
                data['axial2_mean'] = data[['NP2WALK', 'NP3RISNG_ON', 'NP3PSTBL_ON', 'NP3POSTR_ON']].mean(axis = 1)
                data['tmp2_NP2WALK'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP2WALK'].isnull() ), data['axial2_mean'], data['NP2WALK'] )
                data.drop(['NP2WALK'], axis = 1, inplace = True)
                data['tmp2_NP3RISNG_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3RISNG_ON'].isnull() ), data['axial2_mean'], data['NP3RISNG_ON'] )
                data['tmp2_NP3PSTBL_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3PSTBL_ON'].isnull() ), data['axial2_mean'], data['NP3PSTBL_ON'] )
                data['tmp2_NP3POSTR_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3POSTR_ON'].isnull() ), data['axial2_mean'], data['NP3POSTR_ON'] )

                # Axial symptoms score
                data['axial2'] = np.where( ~data['axial2_mean'].isnull()
                                                ,data[['tmp2_NP2WALK', 'tmp2_NP3RISNG_ON', 'tmp2_NP3PSTBL_ON', 'tmp2_NP3POSTR_ON']].sum(axis = 1)
                                                ,np.nan )
        
                # MDS-UPDRS III : Freezing of gait
                data['gait_mean'] = data[['NP2FREZ', 'NP3FRZGT_ON']].mean(axis = 1)
                data['tmp_NP2FREZ'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['NP2FREZ'].isnull() ), data['gait_mean'], data['NP2FREZ'] )
                data.drop(['NP2FREZ'], axis = 1, inplace = True)
                data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['NP3FRZGT_ON'].isnull() ), data['gait_mean'], data['NP3FRZGT_ON'] )

                # Freezing of gait score
                data['freezing_gait'] = np.where( ~data['gait_mean'].isnull()
                                                ,data[['tmp_NP2FREZ', 'tmp_NP3FRZGT_ON']].sum(axis = 1)
                                                ,np.nan )
        
                # Tremor
                data['NP2TRMR'] = data['u_q2_10']
                # Mean of tremor is only based on average from MDS-UPDRS 3.17
                data['tremor_mean'] = data[['NP3RTALJ_ON', 'NP3RTALL_ON', 'NP3RTALU_ON', 'NP3RTARL_ON', 'NP3RTARU_ON']].mean(axis = 1)
                data['tmp_NP2TRMR'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP2TRMR'].isnull() ), data['tremor_mean'], data['NP2TRMR'] )
                data.drop(['NP2TRMR'], axis = 1, inplace = True)
                data['tmp_NP3PTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3PTRML_ON'].isnull() ), data['tremor_mean'], data['NP3PTRML_ON'] )
                data['tmp_NP3PTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3PTRMR_ON'].isnull() ), data['tremor_mean'], data['NP3PTRMR_ON'] )
                data['tmp_NP3KTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3KTRML_ON'].isnull() ), data['tremor_mean'], data['NP3KTRML_ON'] )
                data['tmp_NP3KTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3KTRMR_ON'].isnull() ), data['tremor_mean'], data['NP3KTRMR_ON'] )
                data['tmp_NP3RTALJ_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALJ_ON'].isnull() ), data['tremor_mean'], data['NP3RTALJ_ON'] )
                data['tmp_NP3RTALL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALL_ON'].isnull() ), data['tremor_mean'], data['NP3RTALL_ON'] )
                data['tmp_NP3RTALU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALU_ON'].isnull() ), data['tremor_mean'], data['NP3RTALU_ON'] )
                data['tmp_NP3RTARL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTARL_ON'].isnull() ), data['tremor_mean'], data['NP3RTARL_ON'] )
                data['tmp_NP3RTARU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTARU_ON'].isnull() ), data['tremor_mean'], data['NP3RTARU_ON'] )
                data['tmp_NP3RTCON_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTCON_ON'].isnull() ), data['tremor_mean'], data['NP3RTCON_ON'] )

                # Tremor score
                data['tremor'] = np.where( ~data['tremor_mean'].isnull()
                                                ,data[['tmp_NP2TRMR', 'tmp_NP3PTRML_ON', 'tmp_NP3PTRMR_ON', 'tmp_NP3KTRML_ON', 'tmp_NP3KTRMR_ON', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON'
                                                ,'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                                ,np.nan )
        
                # Rest tremor score
                data['rest_tremor_score'] = np.where( ~data['tremor_mean'].isnull()
                                                ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON'
                                                        ,'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                                ,np.nan )
        
                # Rest tremor amplitude score
                data['tremor_amplitude'] = np.where( ~data['tremor_mean'].isnull()
                                                ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON']].sum(axis = 1)
                                                ,np.nan )
                
                # Dyskinesia composite score
                data['dyski_mean'] = data[['u_q4_1', 'u_q4_2']].mean(axis = 1)
                data['tmp_u_q4_1'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['u_q4_1'].isnull() ), data['dyski_mean'], data['u_q4_1'] )
                data['tmp_u_q4_2'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['u_q4_2'].isnull() ), data['dyski_mean'], data['u_q4_2'] )
                data['dyski_score'] = np.where( ~data['dyski_mean'].isnull(), data[['tmp_u_q4_1', 'tmp_u_q4_2']].sum(axis = 1), np.nan )

                # Mild cognitive impairment based on MoCA score
                data['MoCA26'] = np.where( (~data['question113_113'].isnull()) & (data['question113_113'] < 26), 1
                                          ,np.where( (~data['question113_113'].isnull()), 0, data['question113_113'] ) )
                
                # Levodopa equivalence daily dose (LEDD)
                data[['levodopa', 'meds_da_ledd']].value_counts()
                data['LEDD'] = np.where(data['levodopa'] == 0, 0, data['meds_da_ledd'])


                self.data_process = data
                self.data_event_id = data_event_id
                # return data

        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    LuxPARK Data Processing - To add non-common variables    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        def process_luxpark_all(self):
                
                data = self.data_process.copy()
                vars_info = self.vars_info

                # Working environment
                data['work_agriculture'] = np.where(data['dm_worked_in___1'] == True, 1, np.where(data['dm_worked_in___1'] == False, 0, np.nan ))
                data['work_pesticide'] = np.where(data['dm_worked_in___2'] == True, 1, np.where(data['dm_worked_in___2'] == False, 0, np.nan ))
                data['work_metallurgy'] = np.where(data['dm_worked_in___3'] == True, 1, np.where(data['dm_worked_in___3'] == False, 0, np.nan ))

                # History of present illness - Medical history
                data['md_brain_tumor'] = np.where(data['question210_210___1'] == True, 1, np.where(data['question210_210___1'] == False, 0, np.nan ))
                data['md_meningitis'] = np.where(data['question210_210___2'] == True, 1, np.where(data['question210_210___2'] == False, 0, np.nan ))
                data['md_multiple_sclerosis'] = np.where(data['question210_210___3'] == True, 1, np.where(data['question210_210___3'] == False, 0, np.nan ))
                data['md_neuropathy'] = np.where(data['question210_210___4'] == True, 1, np.where(data['question210_210___4'] == False, 0, np.nan ))
                data['md_nph'] = np.where(data['question210_210___5'] == True, 1, np.where(data['question210_210___5'] == False, 0, np.nan ))
                data['md_seizures'] = np.where(data['question210_210___6'] == True, 1, np.where(data['question210_210___6'] == False, 0, np.nan ))
                data['md_stroke'] = np.where(data['question210_210___7'] == True, 1, np.where(data['question210_210___7'] == False, 0, np.nan ))
                data['md_brain_injury'] = np.where(data['question210_210___8'] == True, 1, np.where(data['question210_210___8'] == False, 0, np.nan ))

                data['md_melanoma'] = np.where(data['question211_211___1'] == True, 1, np.where(data['question211_211___1'] == False, 0, np.nan ))
                data['md_prostate_cancer'] = np.where(data['question211_211___2'] == True, 1, np.where(data['question211_211___2'] == False, 0, np.nan ))

                data['md_diabetes'] = np.where(data['question212_212___1'] == True, 1, np.where(data['question212_212___1'] == False, 0, np.nan ))
                data['md_gout'] = np.where(data['question212_212___2'] == True, 1, np.where(data['question212_212___2'] == False, 0, np.nan ))
                data['md_hypercholesterolemia'] = np.where(data['question212_212___3'] == True, 1, np.where(data['question212_212___3'] == False, 0, np.nan ))
                data['md_hyperthyroidism'] = np.where(data['question212_212___4'] == True, 1, np.where(data['question212_212___4'] == False, 0, np.nan ))
                data['md_hyperuricemia'] = np.where(data['question212_212___5'] == True, 1, np.where(data['question212_212___5'] == False, 0, np.nan ))
                data['md_kidney_stones'] = np.where(data['question212_212___6'] == True, 1, np.where(data['question212_212___6'] == False, 0, np.nan ))
                data['md_vitamin_d_deficiency'] = np.where(data['question212_212___7'] == True, 1, np.where(data['question212_212___7'] == False, 0, np.nan ))

                data['md_leg_movements_sleep'] = np.where(data['question213_213___1'] == True, 1, np.where(data['question213_213___1'] == False, 0, np.nan ))
                data['md_rem_sleep'] = np.where(data['question213_213___2'] == True, 1, np.where(data['question213_213___2'] == False, 0, np.nan ))
                data['md_rest_leg'] = np.where(data['question213_213___3'] == True, 1, np.where(data['question213_213___3'] == False, 0, np.nan ))
                data['md_sleep_apnea'] = np.where(data['question213_213___4'] == True, 1, np.where(data['question213_213___4'] == False, 0, np.nan ))

                data['md_cardiovascular'] = np.where(data['question214_214___1'] == True, 1, np.where(data['question214_214___1'] == False, 0, np.nan ))
                data['md_hypertension'] = np.where(data['question214_214___2'] == True, 1, np.where(data['question214_214___2'] == False, 0, np.nan ))

                data['md_bells_palsy'] = np.where(data['question215_215___1'] == True, 1, np.where(data['question215_215___1'] == False, 0, np.nan ))
                data['md_rheumatoid_arthritis'] = np.where(data['question215_215___2'] == True, 1, np.where(data['question215_215___2'] == False, 0, np.nan ))
                data['md_sjogrens'] = np.where(data['question215_215___3'] == True, 1, np.where(data['question215_215___3'] == False, 0, np.nan ))
                data['md_systemic_lupus'] = np.where(data['question215_215___4'] == True, 1, np.where(data['question215_215___4'] == False, 0, np.nan ))

                data['md_anxiety'] = np.where(data['question216_216___1'] == True, 1, np.where(data['question216_216___1'] == False, 0, np.nan ))
                data['md_bipolar'] = np.where(data['question216_216___2'] == True, 1, np.where(data['question216_216___2'] == False, 0, np.nan ))
                data['md_depression'] = np.where(data['question216_216___3'] == True, 1, np.where(data['question216_216___3'] == False, 0, np.nan ))
                data['md_schizophrenia'] = np.where(data['question216_216___4'] == True, 1, np.where(data['question216_216___4'] == False, 0, np.nan ))

                data['oophorectomy_surgical'] = np.where(data['question258_258___1'] == True, 1, np.where(data['question258_258___1'] == False, 0, np.nan ))

                # Initial motor symptoms
                data['initial_postural_tremor'] = np.where(data['question262_262___2'] == True, 1, np.where(data['question262_262___2'] == False, 0, np.nan ))
                data['initial_freezing'] = np.where(data['question262_262___13'] == True, 1, np.where(data['question262_262___13'] == False, 0, np.nan ))
                data['initial_falls'] = np.where(data['question262_262___14'] == True, 1, np.where(data['question262_262___14'] == False, 0, np.nan ))
                data['initial_gait'] = np.where(data['question262_262___15'] == True, 1, np.where(data['question262_262___15'] == False, 0, np.nan ))

                # Tremor predominant side
                data['tremor_predominant'] = np.where( (data['question268_268'] == 1) | (data['question268_268'] == 2) | (data['question268_268'] == 3), data['question268_268'], np.nan )

                # Tremor location
                data['tremor_head'] = np.where(data['question264_264___1'] == True, 1, np.where(data['question264_264___1'] == False, 0, np.nan ))
                data['tremor_face'] = np.where(data['question264_264___2'] == True, 1, np.where(data['question264_264___2'] == False, 0, np.nan ))
                data['tremor_lips'] = np.where(data['question264_264___3'] == True, 1, np.where(data['question264_264___3'] == False, 0, np.nan ))
                data['tremor_chin'] = np.where(data['question264_264___4'] == True, 1, np.where(data['question264_264___4'] == False, 0, np.nan ))
                data['tremor_right_hand'] = np.where(data['question264_264___5'] == True, 1, np.where(data['question264_264___5'] == False, 0, np.nan ))
                data['tremor_left_hand'] = np.where(data['question264_264___6'] == True, 1, np.where(data['question264_264___6'] == False, 0, np.nan ))
                data['tremor_right_foot'] = np.where(data['question264_264___7'] == True, 1, np.where(data['question264_264___7'] == False, 0, np.nan ))
                data['tremor_left_foot'] = np.where(data['question264_264___8'] == True, 1, np.where(data['question264_264___8'] == False, 0, np.nan ))

                # Rigidity
                data['rigidity_predominant'] = np.where( (data['question265_265'] == 1) | (data['question265_265'] == 2) | (data['question265_265'] == 3), data['question265_265'], np.nan )

                # Bradykinasia
                data['bradykinesia_predominant'] = np.where( (data['question266_266'] == 1) | (data['question266_266'] == 2) | (data['question266_266'] == 3), data['question266_266'], np.nan )

                # Falls
                data['falls_left'] = np.where(data['question267_267___1'] == True, 1, np.where(data['question267_267___1'] == False, 0, np.nan ))
                data['falls_right'] = np.where(data['question267_267___2'] == True, 1, np.where(data['question267_267___2'] == False, 0, np.nan ))
                data['falls_back'] = np.where(data['question267_267___3'] == True, 1, np.where(data['question267_267___3'] == False, 0, np.nan ))
                data['falls_forward'] = np.where(data['question267_267___4'] == True, 1, np.where(data['question267_267___4'] == False, 0, np.nan ))
                data['falls_all'] = np.where(data['question267_267___5'] == True, 1, np.where(data['question267_267___5'] == False, 0, np.nan ))

                # Initial Non-Motor Symptoms
                data['initial_cognitive'] = np.where(data['question269_269___1'] == True, 1, np.where(data['question269_269___1'] == False, 0, np.nan ))
                data['initial_psychiatric'] = np.where(data['question269_269___2'] == True, 1, np.where(data['question269_269___2'] == False, 0, np.nan ))
                data['initial_rem_disorder'] = np.where(data['question269_269___3'] == True, 1, np.where(data['question269_269___3'] == False, 0, np.nan ))
                data['initial_rest_leg'] = np.where(data['question269_269___4'] == True, 1, np.where(data['question269_269___4'] == False, 0, np.nan ))
                data['initial_leg_move_sleep'] = np.where(data['question269_269___5'] == True, 1, np.where(data['question269_269___5'] == False, 0, np.nan ))
                data['initial_sleep_apnea'] = np.where(data['question269_269___6'] == True, 1, np.where(data['question269_269___6'] == False, 0, np.nan ))
                data['initial_insomia'] = np.where(data['question269_269___7'] == True, 1, np.where(data['question269_269___7'] == False, 0, np.nan ))
                data['initial_excess_sleepness'] = np.where(data['question269_269___8'] == True, 1, np.where(data['question269_269___8'] == False, 0, np.nan ))
                data['initial_anosmie'] = np.where(data['question269_269___9'] == True, 1, np.where(data['question269_269___9'] == False, 0, np.nan ))
                data['initial_weight_loss'] = np.where(data['question269_269___10'] == True, 1, np.where(data['question269_269___10'] == False, 0, np.nan ))
                data['initial_anhidrosis'] = np.where(data['question269_269___11'] == True, 1, np.where(data['question269_269___11'] == False, 0, np.nan ))
                data['initial_hyperhydrosis'] = np.where(data['question269_269___12'] == True, 1, np.where(data['question269_269___12'] == False, 0, np.nan ))
                data['initial_seorrhea'] = np.where(data['question269_269___13'] == True, 1, np.where(data['question269_269___13'] == False, 0, np.nan ))
                data['initial_orthostatism'] = np.where(data['question269_269___14'] == True, 1, np.where(data['question269_269___14'] == False, 0, np.nan ))
                data['initial_syncope'] = np.where(data['question269_269___15'] == True, 1, np.where(data['question269_269___15'] == False, 0, np.nan ))
                data['initial_dysphagia'] = np.where(data['question269_269___16'] == True, 1, np.where(data['question269_269___16'] == False, 0, np.nan ))
                data['initial_constipation'] = np.where(data['question269_269___17'] == True, 1, np.where(data['question269_269___17'] == False, 0, np.nan ))
                data['initial_fecal_incontinance'] = np.where(data['question269_269___18'] == True, 1, np.where(data['question269_269___18'] == False, 0, np.nan ))
                data['initial_urinary_incontinance'] = np.where(data['question269_269___19'] == True, 1, np.where(data['question269_269___19'] == False, 0, np.nan ))

                # Psychiatric Symptoms
                data['psy_psychosis'] = np.where(data['question270_270___1'] == True, 1, np.where(data['question270_270___1'] == False, 0, np.nan ))
                data['psy_compulsions'] = np.where(data['question270_270___2'] == True, 1, np.where(data['question270_270___2'] == False, 0, np.nan ))
                data['psy_depression'] = np.where(data['question270_270___3'] == True, 1, np.where(data['question270_270___3'] == False, 0, np.nan ))
                
                # Type of Dyskinesias
                data['dyski_disphasic'] = np.where(data['question282_282___1'] == True, 1, np.where(data['question282_282___1'] == False, 0, np.nan ))
                data['dyski_end_dose'] = np.where(data['question282_282___2'] == True, 1, np.where(data['question282_282___2'] == False, 0, np.nan ))
                data['dyski_continuous'] = np.where(data['question282_282___3'] == True, 1, np.where(data['question282_282___3'] == False, 0, np.nan ))
                data['dyski_peak_dose'] = np.where(data['question282_282___4'] == True, 1, np.where(data['question282_282___4'] == False, 0, np.nan ))

                # CT scan
                data['ct_normal'] = np.where(data['question296_296___1'] == True, 1, np.where(data['question296_296___1'] == False, 0, np.nan ))
                data['ct_basal_ganglia'] = np.where(data['question296_296___2'] == True, 1, np.where(data['question296_296___2'] == False, 0, np.nan ))
                data['ct_midbrain'] = np.where(data['question296_296___3'] == True, 1, np.where(data['question296_296___3'] == False, 0, np.nan ))
                data['ct_cortical'] = np.where(data['question296_296___4'] == True, 1, np.where(data['question296_296___4'] == False, 0, np.nan ))
                data['ct_white'] = np.where(data['question296_296___5'] == True, 1, np.where(data['question296_296___5'] == False, 0, np.nan ))
                data['ct_hypdorcephalus'] = np.where(data['question296_296___6'] == True, 1, np.where(data['question296_296___6'] == False, 0, np.nan ))
                data['ct_atrophy'] = np.where(data['question296_296___7'] == True, 1, np.where(data['question296_296___7'] == False, 0, np.nan ))

                # MRI
                data['mri_normal'] = np.where(data['question300_300___1'] == True, 1, np.where(data['question300_300___1'] == False, 0, np.nan ))
                data['mri_basal_ganglia'] = np.where(data['question300_300___2'] == True, 1, np.where(data['question300_300___2'] == False, 0, np.nan ))
                data['mri_midbrain'] = np.where(data['question300_300___3'] == True, 1, np.where(data['question300_300___3'] == False, 0, np.nan ))
                data['mri_cortical'] = np.where(data['question300_300___4'] == True, 1, np.where(data['question300_300___4'] == False, 0, np.nan ))
                data['mri_white'] = np.where(data['question300_300___5'] == True, 1, np.where(data['question300_300___5'] == False, 0, np.nan ))
                data['mri_hypdorcephalus'] = np.where(data['question300_300___6'] == True, 1, np.where(data['question300_300___6'] == False, 0, np.nan ))
                data['mri_atrophy'] = np.where(data['question300_300___7'] == True, 1, np.where(data['question300_300___7'] == False, 0, np.nan ))

                # DaTscan
                data['datscan_normal'] = np.where(data['question304_304___1'] == True, 1, np.where(data['question304_304___1'] == False, 0, np.nan ))
                data['datscan_right_striatum'] = np.where(data['question304_304___2'] == True, 1, np.where(data['question304_304___2'] == False, 0, np.nan ))
                data['datscan_left_striatum'] = np.where(data['question304_304___3'] == True, 1, np.where(data['question304_304___3'] == False, 0, np.nan ))
                data['datscan_right_caudate'] = np.where(data['question304_304___4'] == True, 1, np.where(data['question304_304___4'] == False, 0, np.nan ))
                data['datscan_left_putamen'] = np.where(data['question304_304___5'] == True, 1, np.where(data['question304_304___5'] == False, 0, np.nan ))
                data['datscan_right_putamen'] = np.where(data['question304_304___6'] == True, 1, np.where(data['question304_304___6'] == False, 0, np.nan ))
                data['datscan_left_caudate'] = np.where(data['question304_304___7'] == True, 1, np.where(data['question304_304___7'] == False, 0, np.nan ))
                
                # PD Surgical
                data['pd_surgical_dbs'] = np.where( (data['question851_851___1'] == True) | (data['op_dbs'] == 1), 1, 0)

                # Medication - Dopaminergic Agents
                data['med_levodopa'] = np.where(data['meds_n04b___1'] == 1, 1, 0)
                data['med_amantadine'] = np.where(data['meds_n04b___2'] == 1, 1, 0)
                data['med_bromocriptine'] = np.where(data['meds_n04b___3'] == 1, 1, 0)
                data['med_ropinirole'] = np.where(data['meds_n04b___4'] == 1, 1, 0)
                data['med_pramipexole'] = np.where(data['meds_n04b___5'] == 1, 1, 0)
                data['med_piribedil'] = np.where(data['meds_n04b___6'] == 1, 1, 0)
                data['med_rotigotine'] = np.where(data['meds_n04b___7'] == 1, 1, 0)
                data['med_selegiline'] = np.where(data['meds_n04b___8'] == 1, 1, 0)
                data['med_rasagiline'] = np.where(data['meds_n04b___9'] == 1, 1, 0)
                data['med_tolcapone'] = np.where(data['meds_n04b___10'] == 1, 1, 0)
                data['med_entacapone'] = np.where(data['meds_n04b___11'] == 1, 1, 0)

                # Medication - Antidepressants
                data['med_imipramine'] = np.where(data['meds_n06a'] == 1, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_clomipramine'] = np.where(data['meds_n06a'] == 2, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_amitriptyline'] = np.where(data['meds_n06a'] == 3, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_nortriptyline'] = np.where(data['meds_n06a'] == 4, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_doxepin'] = np.where(data['meds_n06a'] == 5, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_dosulepin'] = np.where(data['meds_n06a'] == 6, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_fluoxetine'] = np.where(data['meds_n06a'] == 7, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_citalopram'] = np.where(data['meds_n06a'] == 8, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_paroxetine'] = np.where(data['meds_n06a'] == 9, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_sertraline'] = np.where(data['meds_n06a'] == 10, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_fluvoxamine'] = np.where(data['meds_n06a'] == 11, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_escitalopram'] = np.where(data['meds_n06a'] == 12, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_mirtazapine'] = np.where(data['meds_n06a'] == 13, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_bupropion'] = np.where(data['meds_n06a'] == 14, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_tianeptine'] = np.where(data['meds_n06a'] == 15, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_venlafaxine'] = np.where(data['meds_n06a'] == 16, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_milnacipran'] = np.where(data['meds_n06a'] == 17, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_reboxetine'] = np.where(data['meds_n06a'] == 18, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_duloxetine'] = np.where(data['meds_n06a'] == 19, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))
                data['med_agomelatine'] = np.where(data['meds_n06a'] == 20, 1, np.where(~data['meds_n06a'].isnull(), 0, np.nan))

                # Percentage of Dyskinesia
                data['pct_dyski'] = np.where( data['u_q4_1'] > 0, data['u_q4_1_3'], np.where( data['u_q4_1'] == 0, 0, np.nan ) )

                # Percentage of Off State
                data['pct_off_state'] = np.where( data['u_q4_3'] > 0, data['u_q4_3_3'], np.where( data['u_q4_3'] == 0, 0, np.nan ) )

                # Percentage of Off Dystonia
                data['pct_dystonia'] = np.where( data['u_q4_6'] > 0, data['u_q4_6_3'], np.where( data['u_q4_6'] == 0, 0, np.nan ) )

                # Parkinsonism
                data['pd_rigidity'] = np.where( data['question805_805'] == 1, 1, np.where( data['question805_805'] == 2, 0, np.nan ) )
                data['pd_bradykinesia'] = np.where( data['question806_806'] == 1, 1, np.where( data['question806_806'] == 2, 0, np.nan ) )
                data['pd_tremor'] = np.where( data['question807_807'] == 1, 1, np.where( data['question807_807'] == 2, 0, np.nan ) )
                data['pd_impaired_postural'] = np.where( data['question808_808'] == 1, 1, np.where( data['question808_808'] == 2, 0, np.nan ) )

                # Levodopa Response
                data['levodopa_response_minimal'] = np.where( data['question892_892'] == 1, 1, np.where( ~data['question892_892'].isnull(), 0, np.nan ) )
                data['levodopa_response_more'] = np.where( data['question892_892'] == 2, 1, np.where( ~data['question892_892'].isnull(), 0, np.nan ) )
                data['levodopa_response_poor'] = np.where( data['question892_892'] == 3, 1, np.where( ~data['question892_892'].isnull(), 0, np.nan ) )

                # Other Dopaminergic Response
                data['dopaminergic_response_minimal'] = np.where( data['question893_893'] == 1, 1, np.where( data['question893_893'] == 2, 0, np.nan ) )

                # LBCRS (Lewy Body Composite Risk Score)
                data['lbcrs_q1'] = np.where(data['lbcrs_q1'] == True, 1, np.where(data['lbcrs_q1'] == False, 0, np.nan ))
                data['lbcrs_q2'] = np.where(data['lbcrs_q2'] == True, 1, np.where(data['lbcrs_q2'] == False, 0, np.nan ))
                data['lbcrs_q3'] = np.where(data['lbcrs_q3'] == True, 1, np.where(data['lbcrs_q3'] == False, 0, np.nan ))
                data['lbcrs_q4'] = np.where(data['lbcrs_q4'] == True, 1, np.where(data['lbcrs_q4'] == False, 0, np.nan ))
                data['lbcrs_q5'] = np.where(data['lbcrs_q5'] == True, 1, np.where(data['lbcrs_q5'] == False, 0, np.nan ))
                data['lbcrs_q6'] = np.where(data['lbcrs_q6'] == True, 1, np.where(data['lbcrs_q6'] == False, 0, np.nan ))
                data['lbcrs_q7'] = np.where(data['lbcrs_q7'] == True, 1, np.where(data['lbcrs_q7'] == False, 0, np.nan ))
                data['lbcrs_q8'] = np.where(data['lbcrs_q8'] == True, 1, np.where(data['lbcrs_q8'] == False, 0, np.nan ))
                data['lbcrs_q9'] = np.where(data['lbcrs_q9'] == True, 1, np.where(data['lbcrs_q9'] == False, 0, np.nan ))
                data['lbcrs_q10'] = np.where(data['lbcrs_q10'] == True, 1, np.where(data['lbcrs_q10'] == False, 0, np.nan ))

                # Trail Making Test
                data['tmt_5m'] = np.where( (data['tmt_gt_5min']==1) | (data['tmt_b_gt_5m']==1), 1, np.where( (data['tmt_gt_5min']==0) | (data['tmt_b_gt_5m']==0), 0, np.nan ) )

                # Purdue Handedness
                data['handedness_left'] = np.where( data['purdue_handedness'] == 1, 1, np.where( ~data['purdue_handedness'].isnull(), 0, np.nan ) )
                data['handedness_right'] = np.where( data['purdue_handedness'] == 2, 1, np.where( ~data['purdue_handedness'].isnull(), 0, np.nan ) )
                data['handedness_ambidexterity'] = np.where( data['purdue_handedness'] == 3, 1, np.where( ~data['purdue_handedness'].isnull(), 0, np.nan ) )

                # Head injury or concussion
                data['rfqu_head1'] = np.where( (data['rfqu_head1'] == 1) | (data['rfqu_head1'] == 2), 1, np.where(data['rfqu_head1'] == 3, 0, np.nan) )

                # Smoking and tobacco
                data['rfqu_smoking1'] = np.where( data['rfqu_smoking1'] == 1, 1, np.where( data['rfqu_smoking1'] == 2, 0, np.nan ) )
                data['rfqu_smoking2'] = np.where( data['rfqu_smoking2'] == 1, 1, np.where( data['rfqu_smoking2'] == 2, 0, np.nan ) )
                data['rfqu_smoking3'] = np.where( data['rfqu_smoking3'] == 1, 1, np.where( data['rfqu_smoking3'] == 2, 0, np.nan ) )

                # Caffeine
                data['rfqu_caffeine1'] = np.where( data['rfqu_caffeine1'] == 1, 1, np.where( data['rfqu_caffeine1'] == 2, 0, np.nan ) )
                data['rfqu_caffeine2'] = np.where( data['rfqu_caffeine2'] == 1, 1, np.where( data['rfqu_caffeine2'] == 2, 0, np.nan ) )
                data['rfqu_caffeine3'] = np.where( data['rfqu_caffeine3'] == 1, 1, np.where( data['rfqu_caffeine3'] == 2, 0, np.nan ) )
                data['rfqu_caffeine4'] = np.where( data['rfqu_caffeine4'] == 1, 1, np.where( data['rfqu_caffeine4'] == 2, 0, np.nan ) )

                # Pesticides at work
                data['rfqu_pesticides'] = np.where( (data['rfqu_pest_work1'] == 1) | (data['rfqu_pest_nonwork1'] == 1), 1, np.where( (data['rfqu_pest_work1'] == 2) | (data['rfqu_pest_nonwork1'] == 2), 0, np.nan ) )
                data['rfqu_pesticides_sick'] = np.where( (data['rfqu_pest_work2'] == 1) | (data['rfqu_pest_nonwork2'] == 1), 1, np.where( (data['rfqu_pest_work2'] == 2) | (data['rfqu_pest_nonwork2'] == 2), 0, np.nan ) )

                # Anti-inflammatory medication history
                data['antiinflamm'] = np.where( (data['rfqu_antiinflamm1'] == 1) | (data['rfqu_antiinflamm2'] == 1) | (data['rfqu_antiinflamm3'] == 1), 1, np.where( (data['rfqu_antiinflamm1'] == 2) | (data['rfqu_antiinflamm2'] == 2) | (data['rfqu_antiinflamm3'] == 2), 0, np.nan ) )

                # Toxican Exposure
                data['toxicant'] = np.where( (data['rfqu_toxicant1'] == 1) | (data['rfqu_toxicant2'] == 1) | (data['rfqu_toxicant3'] == 1) |
                                                (data['rfqu_toxicant4'] == 1) | (data['rfqu_toxicant5'] == 1) | (data['rfqu_toxicant6'] == 1), 1
                                            ,np.where( (data['rfqu_toxicant1'] == 2) | (data['rfqu_toxicant2'] == 2) | (data['rfqu_toxicant3'] == 2) |
                                                        (data['rfqu_toxicant4'] == 2) | (data['rfqu_toxicant5'] == 2) | (data['rfqu_toxicant6'] == 2), 0, np.nan ) )
                
                # Female health history
                data['pregnancy'] = np.where( data['rfqu_female1'] == 1, 1, np.where( data['rfqu_female1'] == 2, 0, np.nan ) )

                # Family history of tremor
                data['md_fam_tremor'] = np.where( (data['question513_513']==1) | (data['rfqu_fh_tremor1']==1) | (data['rfqu_fh_tremor2']==1) | (data['rfqu_fh_tremor3']==1) | (data['rfqu_fh_tremor5']==1), 1
                                                      ,np.where( (data['question513_513']==0) | (data['rfqu_fh_tremor1']==2) | (data['rfqu_fh_tremor2']==2) | (data['rfqu_fh_tremor3']==2) | (data['rfqu_fh_tremor5']==2), 0, np.nan ) )

                # Family history of dementia or Alzheimer
                data['md_fam_dementia'] = np.where( (data['question514_514']==1) | (data['rfqu_fh_dementia1']==1) | (data['rfqu_fh_dementia2']==1) | (data['rfqu_fh_dementia3']==1) | (data['rfqu_fh_dementia5']==1), 1
                                                   ,np.where( (data['question514_514']==0) | (data['rfqu_fh_dementia1']==2) | (data['rfqu_fh_dementia2']==2) | (data['rfqu_fh_dementia3']==2) | (data['rfqu_fh_dementia5']==2), 0, np.nan ) )

                # Quality of Life (Difficulty mobility)
                data['qol_mobility_never'] = np.where( data['pdq39_q1'] == 0, 1, np.where( ~data['pdq39_q1'].isnull(), 0, np.nan ) )
                data['qol_mobility_occasional'] = np.where( (data['pdq39_q1'] == 1) | (data['pdq39_q1'] == 2), 1, np.where( ~data['pdq39_q1'].isnull(), 0, np.nan ) )
                data['qol_mobility_often'] = np.where( (data['pdq39_q1'] == 3) | (data['pdq39_q1'] == 4), 1, np.where( ~data['pdq39_q1'].isnull(), 0, np.nan ) )

                # Quality of Life (Difficulty on housework)
                data['qol_housework_never'] = np.where( data['pdq39_q2'] == 0, 1, np.where( ~data['pdq39_q2'].isnull(), 0, np.nan ) )
                data['qol_housework_occasional'] = np.where( (data['pdq39_q2'] == 1) | (data['pdq39_q2'] == 2), 1, np.where( ~data['pdq39_q2'].isnull(), 0, np.nan ) )
                data['qol_housework_often'] = np.where( (data['pdq39_q2'] == 3) | (data['pdq39_q2'] == 4), 1, np.where( ~data['pdq39_q2'].isnull(), 0, np.nan ) )

                # Quality of Life (Difficulty Carrying Bags of Shopping)
                data['qol_shopping_never'] = np.where( data['pdq39_q3'] == 0, 1, np.where( ~data['pdq39_q3'].isnull(), 0, np.nan ) )
                data['qol_shopping_occasional'] = np.where( (data['pdq39_q3'] == 1) | (data['pdq39_q3'] == 2), 1, np.where( ~data['pdq39_q3'].isnull(), 0, np.nan ) )
                data['qol_shopping_often'] = np.where( (data['pdq39_q3'] == 3) | (data['pdq39_q3'] == 4), 1, np.where( ~data['pdq39_q3'].isnull(), 0, np.nan ) )

                # Quality of Life (Difficulty of walking)
                data['qol_walking_never'] = np.where( (data['pdq39_q4'] == 0) & (data['pdq39_q5'] == 0), 1, np.where( (~data['pdq39_q4'].isnull()) | (~data['pdq39_q4'].isnull()), 0, np.nan ) )
                data['qol_walking_often'] = np.where( (data['pdq39_q4'] == 3) | (data['pdq39_q4'] == 4) | (data['pdq39_q5'] == 3) | (data['pdq39_q5'] == 4), 1
                                                          ,np.where( (~data['pdq39_q4'].isnull()) | (~data['pdq39_q4'].isnull()) | (~data['pdq39_q5'].isnull()) | (~data['pdq39_q5'].isnull()), 0, np.nan ) )
                data['qol_walking_occasional'] = np.where( (data['qol_walking_often'] != 1) & ((data['pdq39_q4'] == 1) | (data['pdq39_q4'] == 2) | (data['pdq39_q5'] == 1) | (data['pdq39_q5'] == 2)), 1
                                                          ,np.where( (~data['pdq39_q4'].isnull()) | (~data['pdq39_q4'].isnull()) | (~data['pdq39_q5'].isnull()) | (~data['pdq39_q5'].isnull()), 0, np.nan ) )
                
                # Quality of Life (Difficulty of getting around)
                data['qol_get_around_never'] = np.where( (data['pdq39_q6'] == 0) & (data['pdq39_q7'] == 0), 1, np.where( (~data['pdq39_q6'].isnull()) | (~data['pdq39_q7'].isnull()), 0, np.nan ) )
                data['qol_get_around_often'] = np.where( (data['pdq39_q6'] == 3) | (data['pdq39_q6'] == 4) | (data['pdq39_q7'] == 3) | (data['pdq39_q7'] == 4), 1
                                                          ,np.where( (~data['pdq39_q6'].isnull()) | (~data['pdq39_q6'].isnull()) | (~data['pdq39_q7'].isnull()) | (~data['pdq39_q7'].isnull()), 0, np.nan ) )
                data['qol_get_around_occasional'] = np.where( (data['qol_get_around_often'] != 1) & ((data['pdq39_q6'] == 1) | (data['pdq39_q6'] == 2) | (data['pdq39_q7'] == 1) | (data['pdq39_q7'] == 2)), 1
                                                          ,np.where( (~data['pdq39_q6'].isnull()) | (~data['pdq39_q6'].isnull()) | (~data['pdq39_q7'].isnull()) | (~data['pdq39_q7'].isnull()), 0, np.nan ) )
                
                # Quality of Life (Needed accompany when went out)
                data['qol_accompany_never'] = np.where( data['pdq39_q8'] == 0, 1, np.where( ~data['pdq39_q8'].isnull(), 0, np.nan ) )
                data['qol_accompany_occasional'] = np.where( (data['pdq39_q8'] == 1) | (data['pdq39_q8'] == 2), 1, np.where( ~data['pdq39_q8'].isnull(), 0, np.nan ) )
                data['qol_accompany_often'] = np.where( (data['pdq39_q8'] == 3) | (data['pdq39_q8'] == 4), 1, np.where( ~data['pdq39_q8'].isnull(), 0, np.nan ) )

                # Quality of Life (Felt frightened or worried falling)
                data['qol_frightened_never'] = np.where( data['pdq39_q9'] == 0, 1, np.where( ~data['pdq39_q9'].isnull(), 0, np.nan ) )
                data['qol_frightened_occasional'] = np.where( (data['pdq39_q9'] == 1) | (data['pdq39_q9'] == 2), 1, np.where( ~data['pdq39_q9'].isnull(), 0, np.nan ) )
                data['qol_frightened_often'] = np.where( (data['pdq39_q9'] == 3) | (data['pdq39_q9'] == 4), 1, np.where( ~data['pdq39_q9'].isnull(), 0, np.nan ) )

                # Activities of daily living (Difficulty washing yourself)
                data['adl_washing_never'] = np.where( data['pdq39_q11'] == 0, 1, np.where( ~data['pdq39_q11'].isnull(), 0, np.nan ) )
                data['adl_washing_occasional'] = np.where( (data['pdq39_q11'] == 1) | (data['pdq39_q11'] == 2), 1, np.where( ~data['pdq39_q11'].isnull(), 0, np.nan ) )
                data['adl_washing_often'] = np.where( (data['pdq39_q11'] == 3) | (data['pdq39_q11'] == 4), 1, np.where( ~data['pdq39_q11'].isnull(), 0, np.nan ) )

                # Activities of daily living (Difficulty dressing yourself)
                data['adl_dressing_never'] = np.where( data['pdq39_q12'] == 0, 1, np.where( ~data['pdq39_q12'].isnull(), 0, np.nan ) )
                data['adl_dressing_occasional'] = np.where( (data['pdq39_q12'] == 1) | (data['pdq39_q12'] == 2), 1, np.where( ~data['pdq39_q12'].isnull(), 0, np.nan ) )
                data['adl_dressing_often'] = np.where( (data['pdq39_q12'] == 3) | (data['pdq39_q12'] == 4), 1, np.where( ~data['pdq39_q12'].isnull(), 0, np.nan ) )

                # Activities of daily living (Problems doing up buttons or shoes laces)
                data['adl_button_never'] = np.where( data['pdq39_q13'] == 0, 1, np.where( ~data['pdq39_q13'].isnull(), 0, np.nan ) )
                data['adl_button_occasional'] = np.where( (data['pdq39_q13'] == 1) | (data['pdq39_q13'] == 2), 1, np.where( ~data['pdq39_q13'].isnull(), 0, np.nan ) )
                data['adl_button_often'] = np.where( (data['pdq39_q13'] == 3) | (data['pdq39_q13'] == 4), 1, np.where( ~data['pdq39_q13'].isnull(), 0, np.nan ) )

                # Activities of daily living (Problems writing clearly)
                data['adl_writing_never'] = np.where( data['pdq39_q14'] == 0, 1, np.where( ~data['pdq39_q14'].isnull(), 0, np.nan ) )
                data['adl_writing_occasional'] = np.where( (data['pdq39_q14'] == 1) | (data['pdq39_q14'] == 2), 1, np.where( ~data['pdq39_q14'].isnull(), 0, np.nan ) )
                data['adl_writing_often'] = np.where( (data['pdq39_q14'] == 3) | (data['pdq39_q14'] == 4), 1, np.where( ~data['pdq39_q14'].isnull(), 0, np.nan ) )

                # Activities of daily living (Difficulty cutting up the food)
                data['adl_cutting_never'] = np.where( data['pdq39_q15'] == 0, 1, np.where( ~data['pdq39_q15'].isnull(), 0, np.nan ) )
                data['adl_cutting_occasional'] = np.where( (data['pdq39_q15'] == 1) | (data['pdq39_q15'] == 2), 1, np.where( ~data['pdq39_q15'].isnull(), 0, np.nan ) )
                data['adl_cutting_often'] = np.where( (data['pdq39_q15'] == 3) | (data['pdq39_q15'] == 4), 1, np.where( ~data['pdq39_q15'].isnull(), 0, np.nan ) )

                # Activities of daily living (Difficulty holding a drink)
                data['adl_hold_drink_never'] = np.where( data['pdq39_q16'] == 0, 1, np.where( ~data['pdq39_q16'].isnull(), 0, np.nan ) )
                data['adl_hold_drink_occasional'] = np.where( (data['pdq39_q16'] == 1) | (data['pdq39_q16'] == 2), 1, np.where( ~data['pdq39_q16'].isnull(), 0, np.nan ) )
                data['adl_hold_drink_often'] = np.where( (data['pdq39_q16'] == 3) | (data['pdq39_q16'] == 4), 1, np.where( ~data['pdq39_q16'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt Depressed)
                data['emo_depression_never'] = np.where( data['pdq39_q17'] == 0, 1, np.where( ~data['pdq39_q17'].isnull(), 0, np.nan ) )
                data['emo_depression_occasional'] = np.where( (data['pdq39_q17'] == 1) | (data['pdq39_q17'] == 2), 1, np.where( ~data['pdq39_q17'].isnull(), 0, np.nan ) )
                data['emo_depression_often'] = np.where( (data['pdq39_q17'] == 3) | (data['pdq39_q17'] == 4), 1, np.where( ~data['pdq39_q17'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt isolated and lonely)
                data['emo_lonely_never'] = np.where( data['pdq39_q18'] == 0, 1, np.where( ~data['pdq39_q18'].isnull(), 0, np.nan ) )
                data['emo_lonely_occasional'] = np.where( (data['pdq39_q18'] == 1) | (data['pdq39_q18'] == 2), 1, np.where( ~data['pdq39_q18'].isnull(), 0, np.nan ) )
                data['emo_lonely_often'] = np.where( (data['pdq39_q18'] == 3) | (data['pdq39_q18'] == 4), 1, np.where( ~data['pdq39_q18'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt weepy or tearful)
                data['emo_weepy_never'] = np.where( data['pdq39_q19'] == 0, 1, np.where( ~data['pdq39_q19'].isnull(), 0, np.nan ) )
                data['emo_weepy_occasional'] = np.where( (data['pdq39_q19'] == 1) | (data['pdq39_q19'] == 2), 1, np.where( ~data['pdq39_q19'].isnull(), 0, np.nan ) )
                data['emo_weepy_often'] = np.where( (data['pdq39_q19'] == 3) | (data['pdq39_q19'] == 4), 1, np.where( ~data['pdq39_q19'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt angry or bitter)
                data['emo_angry_never'] = np.where( data['pdq39_q20'] == 0, 1, np.where( ~data['pdq39_q20'].isnull(), 0, np.nan ) )
                data['emo_angry_occasional'] = np.where( (data['pdq39_q20'] == 1) | (data['pdq39_q20'] == 2), 1, np.where( ~data['pdq39_q20'].isnull(), 0, np.nan ) )
                data['emo_angry_often'] = np.where( (data['pdq39_q20'] == 3) | (data['pdq39_q20'] == 4), 1, np.where( ~data['pdq39_q20'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt anxious)
                data['emo_anxious_never'] = np.where( data['pdq39_q21'] == 0, 1, np.where( ~data['pdq39_q21'].isnull(), 0, np.nan ) )
                data['emo_anxious_occasional'] = np.where( (data['pdq39_q21'] == 1) | (data['pdq39_q21'] == 2), 1, np.where( ~data['pdq39_q21'].isnull(), 0, np.nan ) )
                data['emo_anxious_often'] = np.where( (data['pdq39_q21'] == 3) | (data['pdq39_q21'] == 4), 1, np.where( ~data['pdq39_q21'].isnull(), 0, np.nan ) )

                # Emotional Well Being (Felt worried about future)
                data['emo_future_never'] = np.where( data['pdq39_q22'] == 0, 1, np.where( ~data['pdq39_q22'].isnull(), 0, np.nan ) )
                data['emo_future_occasional'] = np.where( (data['pdq39_q22'] == 1) | (data['pdq39_q22'] == 2), 1, np.where( ~data['pdq39_q22'].isnull(), 0, np.nan ) )
                data['emo_future_often'] = np.where( (data['pdq39_q22'] == 3) | (data['pdq39_q22'] == 4), 1, np.where( ~data['pdq39_q22'].isnull(), 0, np.nan ) )

                # Stigma (Conceal Parkinson's from people)
                data['stigma_conceal_pd_never'] = np.where( data['pdq39_q23'] == 0, 1, np.where( ~data['pdq39_q23'].isnull(), 0, np.nan ) )
                data['stigma_conceal_pd_occasional'] = np.where( (data['pdq39_q23'] == 1) | (data['pdq39_q23'] == 2), 1, np.where( ~data['pdq39_q23'].isnull(), 0, np.nan ) )
                data['stigma_conceal_pd_often'] = np.where( (data['pdq39_q23'] == 3) | (data['pdq39_q23'] == 4), 1, np.where( ~data['pdq39_q23'].isnull(), 0, np.nan ) )

                # Stigma (Avoided situations involve eating or drinking in public)
                data['stigma_avoid_never'] = np.where( data['pdq39_q24'] == 0, 1, np.where( ~data['pdq39_q24'].isnull(), 0, np.nan ) )
                data['stigma_avoid_occasional'] = np.where( (data['pdq39_q24'] == 1) | (data['pdq39_q24'] == 2), 1, np.where( ~data['pdq39_q24'].isnull(), 0, np.nan ) )
                data['stigma_avoid_often'] = np.where( (data['pdq39_q24'] == 3) | (data['pdq39_q24'] == 4), 1, np.where( ~data['pdq39_q24'].isnull(), 0, np.nan ) )

                # Stigma (Felt embarrased in public)
                data['stigma_embarrased_never'] = np.where( data['pdq39_q25'] == 0, 1, np.where( ~data['pdq39_q25'].isnull(), 0, np.nan ) )
                data['stigma_embarrased_occasional'] = np.where( (data['pdq39_q25'] == 1) | (data['pdq39_q25'] == 2), 1, np.where( ~data['pdq39_q25'].isnull(), 0, np.nan ) )
                data['stigma_embarrased_often'] = np.where( (data['pdq39_q25'] == 3) | (data['pdq39_q25'] == 4), 1, np.where( ~data['pdq39_q25'].isnull(), 0, np.nan ) )

                # Stigma (Worried reaction from people)
                data['stigma_reaction_never'] = np.where( data['pdq39_q26'] == 0, 1, np.where( ~data['pdq39_q26'].isnull(), 0, np.nan ) )
                data['stigma_reaction_occasional'] = np.where( (data['pdq39_q26'] == 1) | (data['pdq39_q26'] == 2), 1, np.where( ~data['pdq39_q26'].isnull(), 0, np.nan ) )
                data['stigma_reaction_often'] = np.where( (data['pdq39_q26'] == 3) | (data['pdq39_q26'] == 4), 1, np.where( ~data['pdq39_q26'].isnull(), 0, np.nan ) )

                # Cognitive Impairment (Unexpected fallen asleep)
                data['cogn_asleep_never'] = np.where( data['pdq39_q30'] == 0, 1, np.where( ~data['pdq39_q30'].isnull(), 0, np.nan ) )
                data['cogn_asleep_occasional'] = np.where( (data['pdq39_q30'] == 1) | (data['pdq39_q30'] == 2), 1, np.where( ~data['pdq39_q30'].isnull(), 0, np.nan ) )
                data['cogn_asleep_often'] = np.where( (data['pdq39_q30'] == 3) | (data['pdq39_q30'] == 4), 1, np.where( ~data['pdq39_q30'].isnull(), 0, np.nan ) )

                # Cognitive Impairment (Problems with concentration)
                data['cogn_concentration_never'] = np.where( data['pdq39_q31'] == 0, 1, np.where( ~data['pdq39_q31'].isnull(), 0, np.nan ) )
                data['cogn_concentration_occasional'] = np.where( (data['pdq39_q31'] == 1) | (data['pdq39_q31'] == 2), 1, np.where( ~data['pdq39_q31'].isnull(), 0, np.nan ) )
                data['cogn_concentration_often'] = np.where( (data['pdq39_q31'] == 3) | (data['pdq39_q31'] == 4), 1, np.where( ~data['pdq39_q31'].isnull(), 0, np.nan ) )

                # Cognitive Impairment (Felt bad memory)
                data['cogn_memory_never'] = np.where( data['pdq39_q32'] == 0, 1, np.where( ~data['pdq39_q32'].isnull(), 0, np.nan ) )
                data['cogn_memory_occasional'] = np.where( (data['pdq39_q32'] == 1) | (data['pdq39_q32'] == 2), 1, np.where( ~data['pdq39_q32'].isnull(), 0, np.nan ) )
                data['cogn_memory_often'] = np.where( (data['pdq39_q32'] == 3) | (data['pdq39_q32'] == 4), 1, np.where( ~data['pdq39_q32'].isnull(), 0, np.nan ) )

                # Cognitive Impairment (Had distressing dreams or hallucinations)
                data['cogn_hallucination_never'] = np.where( data['pdq39_q33'] == 0, 1, np.where( ~data['pdq39_q33'].isnull(), 0, np.nan ) )
                data['cogn_hallucination_occasional'] = np.where( (data['pdq39_q33'] == 1) | (data['pdq39_q33'] == 2), 1, np.where( ~data['pdq39_q33'].isnull(), 0, np.nan ) )
                data['cogn_hallucination_often'] = np.where( (data['pdq39_q33'] == 3) | (data['pdq39_q33'] == 4), 1, np.where( ~data['pdq39_q33'].isnull(), 0, np.nan ) )



                # To rename the variables
                data.rename( columns = { 'cdisc_dm_usubjd': 'PATNO'
                                        ,'cdisc_dm_sex': 'gender'
                                        ,'status_weight': 'weight'
                                        ,'status_height': 'height'
                                        ,'status_bmi': 'bmi'
                                        ,'fampd_old': 'family_history'
                                        ,'u_q1_4': 'mds1_anxious'
                                        ,'u_q1_5': 'mds1_apathy'
                                        ,'u_q1_1': 'mds1_cognitive'
                                        ,'u_q1_6': 'mds1_dopa'
                                        ,'u_q1_3': 'mds1_depressed'
                                        ,'u_q1_2': 'mds1_hallucination'
                                        ,'u_q1_11': 'mds1_constipation'
                                        ,'u_q1_13': 'mds1_fatigue'
                                        ,'u_q1_12': 'mds1_headedness'
                                        ,'u_q1_9': 'mds1_pain'
                                        ,'u_q1_8': 'mds1_sleep_day'
                                        ,'u_q1_7': 'mds1_sleep_night'
                                        ,'u_q1_10': 'mds1_urinary'
                                        ,'u_q2_5': 'mds2_dressing'
                                        ,'u_q2_4': 'mds2_eat'
                                        ,'u_q2_13': 'mds2_freezing'
                                        ,'u_q2_8': 'mds2_hobby'
                                        ,'u_q2_7': 'mds2_handwriting'
                                        ,'u_q2_6': 'mds2_hygiene'
                                        ,'u_q2_11': 'mds2_bed'
                                        ,'u_q2_2': 'mds2_saliva'
                                        ,'u_q2_1': 'mds2_speech'
                                        ,'u_q2_3': 'mds2_chewing'
                                        ,'u_q2_10': 'mds2_tremor'
                                        ,'u_q2_9': 'mds2_turn'
                                        ,'u_q2_12': 'mds2_walk'
                                        ,'NP3BRADY': 'mds3_global_move'                 ,'NP3BRADY_ON': 'mds3_global_move_on'
                                        ,'NP3FACXP': 'mds3_facial'                      ,'NP3FACXP_ON': 'mds3_facial_on'
                                        ,'NP3FRZGT': 'mds3_freeze_gait'                 ,'NP3FRZGT_ON': 'mds3_freeze_gait_on'
                                        ,'NP3FTAPL': 'mds3_finger_tap_left'             ,'NP3FTAPL_ON': 'mds3_finger_tap_left_on'
                                        ,'NP3FTAPR': 'mds3_finger_tap_right'            ,'NP3FTAPR_ON': 'mds3_finger_tap_right_on'
                                        ,'NP3GAIT': 'mds3_gait'                         ,'NP3GAIT_ON': 'mds3_gait_on'
                                        ,'NP3HMOVL': 'mds3_hand_move_left'              ,'NP3HMOVL_ON': 'mds3_hand_move_left_on'
                                        ,'NP3HMOVR': 'mds3_hand_move_right'             ,'NP3HMOVR_ON': 'mds3_hand_move_right_on'
                                        ,'NP3KTRML': 'mds3_tremor_left'                 ,'NP3KTRML_ON': 'mds3_tremor_left_on'
                                        ,'NP3KTRMR': 'mds3_tremor_right'                ,'NP3KTRMR_ON': 'mds3_tremor_right_on'
                                        ,'NP3LGAGL': 'mds3_leg_left'                    ,'NP3LGAGL_ON': 'mds3_leg_left_on'
                                        ,'NP3LGAGR': 'mds3_leg_right'                   ,'NP3LGAGR_ON': 'mds3_leg_right_on'
                                        ,'NP3POSTR': 'mds3_posture'                     ,'NP3POSTR_ON': 'mds3_posture_on'
                                        ,'NP3PRSPL': 'mds3_pronation_left'              ,'NP3PRSPL_ON': 'mds3_pronation_left_on'
                                        ,'NP3PRSPR': 'mds3_pronation_right'             ,'NP3PRSPR_ON': 'mds3_pronation_right_on'
                                        ,'NP3PSTBL': 'mds3_stability'                   ,'NP3PSTBL_ON': 'mds3_stability_on'
                                        ,'NP3PTRML': 'mds3_postural_tremor_left'        ,'NP3PTRML_ON': 'mds3_postural_tremor_left_on'
                                        ,'NP3PTRMR': 'mds3_postural_tremor_right'       ,'NP3PTRMR_ON': 'mds3_postural_tremor_right_on'
                                        ,'NP3RIGLL': 'mds3_rigidity_lle'                ,'NP3RIGLL_ON': 'mds3_rigidity_lle_on'
                                        ,'NP3RIGLU': 'mds3_rigidity_lue'                ,'NP3RIGLU_ON': 'mds3_rigidity_lue_on'
                                        ,'NP3RIGN': 'mds3_rigidity_neck'                ,'NP3RIGN_ON': 'mds3_rigidity_neck_on'
                                        ,'NP3RIGRL': 'mds3_rigidity_rle'                ,'NP3RIGRL_ON': 'mds3_rigidity_rle_on'
                                        ,'NP3RIGRU': 'mds3_rigidity_rue'                ,'NP3RIGRU_ON': 'mds3_rigidity_rue_on'
                                        ,'NP3RISNG': 'mds3_arising_chair'               ,'NP3RISNG_ON': 'mds3_arising_chair_on'
                                        ,'NP3RTALJ': 'mds3_tremor_lip'                  ,'NP3RTALJ_ON': 'mds3_tremor_lip_on'
                                        ,'NP3RTALL': 'mds3_tremor_lle'                  ,'NP3RTALL_ON': 'mds3_tremor_lle_on'
                                        ,'NP3RTALU': 'mds3_tremor_lue'                  ,'NP3RTALU_ON': 'mds3_tremor_lue_on'
                                        ,'NP3RTARL': 'mds3_tremor_rle'                  ,'NP3RTARL_ON': 'mds3_tremor_rle_on'
                                        ,'NP3RTARU': 'mds3_tremor_rue'                  ,'NP3RTARU_ON': 'mds3_tremor_rue_on'
                                        ,'NP3RTCON': 'mds3_constancy_tremor'            ,'NP3RTCON_ON': 'mds3_constancy_tremor_on'
                                        ,'NP3SPCH': 'mds3_speech'                       ,'NP3SPCH_ON': 'mds3_speech_on'
                                        ,'NP3TTAPL': 'mds3_toe_tap_left'                ,'NP3TTAPL_ON': 'mds3_toe_tap_left_on'
                                        ,'NP3TTAPR': 'mds3_toe_tap_right'               ,'NP3TTAPR_ON': 'mds3_toe_tap_right_on'
                                        ,'u_q4_2': 'mds4_dyskinesia'
                                        ,'u_q4_6': 'mds4_pain_dystonia'
                                        ,'u_q4_4': 'mds4_functional_motor'
                                        ,'u_q4_5': 'mds4_complexity_motor'
                                        ,'u_q4_3': 'mds4_time_off'
                                        ,'u_q4_1': 'mds4_time_dyskinesia'
                                        ,'question113_113': 'moca'
                                        ,'tmt_gt_5min': 'trail_making_max'
                                        ,'DXBRADY': 'initial_bradykinesia'
                                        ,'DXTREMOR': 'initial_tremor'
                                        ,'DXRIGID': 'initial_rigidity'
                                        ,'question212b_212b': 'md_liver_dysfunction'
                                        ,'question850_850': 'pd_surgical'
                                        ,'question809_809': 'med_failed_levodopa'
                                        ,'question812_812': 'pd2_abrupt'
                                        ,'question813_813': 'pd2_strokes'
                                        ,'question814_814': 'pd2_head_injury'
                                        ,'question815_815': 'pd2_neuroleptic'
                                        ,'question816_816': 'img_hydrocephalus'
                                        ,'question817_817': 'img_white_matter'
                                        ,'question818_818': 'img_midbrain_tumor'
                                        ,'question819_819': 'img_midbrain_infarct'
                                        ,'question820_820': 'img_cerebellar'
                                        ,'purdue_4_1': 'handedness_left_avg'
                                        ,'purdue_4_2': 'handedness_right_avg'
                                        ,'purdue_4_3': 'handedness_both_avg'
                                        ,'purdue_4_5': 'handedness_assembly_avg'
                                        ,'pdq39_q1_q10_subscore': 'qol_score'
                                        ,'pdq39_q11_q16_subscore': 'adl_score'
                                        ,'pdq39_q17_q22_subscore': 'emo_score'
                                        ,'pdq39_q23_q26_score': 'stigma_score'
                                        ,'pdq39_q30_q33_score': 'cogn_score'
                                        ,'pdq39_q34_q36_score': 'communication_score'
                                        ,'pdq39_q36_q39_score': 'body_discomfort_score'
                                        ,'pdq39_score': 'qol_total_score'
                                        ,'meds_n04a': 'med_anticholinergic'
                                        ,'meds_n06a': 'med_antidepressants'
                                        ,'question840_840': 'final_diag_parkinsonism'
                                        },inplace = True )
                

                # To keep the selected variables
                vars_keep = vars_info['LuxPARK Variable'].dropna()
                vars_keep = vars_keep[~vars_keep.isin(['inc_q1', 'inc_q2', 'diag_control', 'inclusionexclusion_criteria_complete'])]
                data = data[vars_keep]

                # To return the processed data
                self.data_process_add = data
                return data



        def rename_luxpark(self):

                data = self.data_process.copy()
                data_variables = self.data_variables

                # To rename the variables
                data.rename( columns = { 'cdisc_dm_usubjd': 'PATNO'\
                                        ,'sv_basic_date': 'visit_date'\
                                        ,'cdisc_dm_sex': 'gen'\
                                        ,'status_weight': 'WGTKG'\
                                        ,'status_height': 'HTCM'\
                                        ,'status_bmi': 'BMI'\
                                        ,'u_q1_4': 'NP1ANXS'\
                                        ,'u_q1_5': 'NP1APAT'\
                                        ,'u_q1_1': 'NP1COG'\
                                        ,'u_q1_6': 'NP1DDS'\
                                        ,'u_q1_3': 'NP1DPRS'\
                                        ,'u_q1_2': 'NP1HALL'\
                                        ,'u_q1_11': 'NP1CNST'\
                                        ,'u_q1_13': 'NP1FATG'\
                                        ,'u_q1_12': 'NP1LTHD'\
                                        ,'u_q1_9': 'NP1PAIN'\
                                        ,'u_q1_8': 'NP1SLPD'\
                                        ,'u_q1_7': 'NP1SLPN'\
                                        ,'u_q1_10': 'NP1URIN'\
                                        ,'u_q2_5': 'NP2DRES'\
                                        ,'u_q2_4': 'NP2EAT'\
                                        ,'u_q2_13': 'NP2FREZ'\
                                        ,'u_q2_8': 'NP2HOBB'\
                                        ,'u_q2_7': 'NP2HWRT'\
                                        ,'u_q2_6': 'NP2HYGN'\
                                        ,'u_q2_11': 'NP2RISE'\
                                        ,'u_q2_2': 'NP2SALV'\
                                        ,'u_q2_1': 'NP2SPCH'\
                                        ,'u_q2_3': 'NP2SWAL'\
                                        ,'u_q2_10': 'NP2TRMR'\
                                        ,'u_q2_9': 'NP2TURN'\
                                        ,'u_q2_12': 'NP2WALK'\
                                        ,'u_q4_2': 'NP4DYSKI'\
                                        ,'u_q4_6': 'NP4DYSTN'\
                                        ,'u_q4_4': 'NP4FLCTI'\
                                        ,'u_q4_5': 'NP4FLCTX'\
                                        ,'u_q4_3': 'NP4OFF'\
                                        ,'u_q4_1': 'NP4WDYSK'\
                                        ,'u_part1_score': 'updrs1_score'\
                                        ,'u_part2_score': 'updrs2_score'\
                                        ,'u_part4_score': 'updrs4_score'\
                                        ,'hoehn_and_yahr_staging': 'NHY'\
                                        ,'question113_113': 'moca'\
                                        ,'scopa_subscore_gastro': 'scopa_gi'\
                                        ,'scopa_subscore_urinary': 'scopa_ur'\
                                        ,'scopa_subscore_cardiovasc': 'scopa_cv'\
                                        ,'scopa_subscore_thermo': 'scopa_therm'\
                                        ,'bentons_score': 'bjlot'\
                                        ,'s_and_e_percent': 'MSEADLG'\
                                        ,'dm_death_status': 'DEATH_STATUS'\
                                        ,'tmt_gt_5min': 'TMTACMPL'},inplace = True )
                
                # To define the data type
                data['dt_type'] = 'LuxPARK'

                # To keep the selected variables
                data = data[data_variables['PPMI Variable'].dropna()]

                # To return the processed data
                return data
        


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    LuxPARK Data Processing - To add non-common variables    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        def luxpark_event_id(self):
                
                data = self.data.copy()
                data_event_id = self.data_event_id.copy()

                # To convert visit date to datetime
                data['sv_basic_date'] = data['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))

                # To obtain EVENT_ID from data_event_id
                data = pd.merge(data, data_event_id, how = 'left', on = ['cdisc_dm_usubjd', 'sv_basic_date'])

                return(data)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    LuxPARK Data Processing - NP4DYSKI (u_q4_2), NP1DPRS (u_q1_3), NP1COG (u_q1_1)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_luxpark_outcome(data, baseline_data):
    
    # Inclusion and exclusion criteria
    data = data[  (data['inc_q1___1'] == True)
                & (data['inc_q1___2'] == True)
                & (data['inc_q1___3'] == False)
                & (data['inc_q1___4'] == True)
                & (data['inc_q2___1'] == False)
                & (data['inc_q2___2'] == False)
                & (data['inc_q2___3'] == False)
                & (data['inc_q2___4'] == False)
                & (data['inc_q2___5'] == False)
                & (data['inc_q2___6'] == False)
                & (data['inclusionexclusion_criteria_complete'] != '0')]
    
    # To extract non-control subject
    data = data[ (data['diag_control'] == 0) ]

    # MoCA scores
    data.rename(columns = {'question113_113': 'moca'}, inplace = True)

    # Cognitive impairment in daily living [NP1COG = u_q1_1]
    # 0 - No impact to daily life due to cognitive impairment   : NP1COG == 0 | NP1COG == 1
    # 1 - Daily life impact due to cognitive impairment         : NP1COG >= 2
    data['NP1COG2'] = np.where( data['u_q1_1'].isin([0, 1]), 0, np.where( data['u_q1_1'].isin([2, 3, 4]), 1, np.nan ) )
    # NP1COG2 is not derived in baseline_data
    # baseline_data['NP1COG2'] = np.where( baseline_data['NP1COG'].isin([0, 1]), 0, np.where( baseline_data['NP1COG'].isin([2, 3, 4]), 1, np.nan ) )

    # Mild cognitive impairment [MoCA26 = question113_113]
    # 0 - No mild cognitive impairment : MoCA26 >= 26
    # 1 - Mild cognitive impairment    : MoCA26 < 26
    data['MoCA26'] = np.where( (~data['moca'].isnull()) & (data['moca'] < 26), 1
                              ,np.where( (~data['moca'].isnull()), 0, data['moca'] ) ) 
    
    # To keep variables with necessary variables
    vars_keep1 = ['cdisc_dm_usubjd', 'sv_basic_date', 'EVENT_ID'
                  ,'u_q4_1', 'u_q4_2', 'u_q4_3', 'u_q4_4', 'u_q4_5'
                  ,'u_q4_1_3'
                  ,'u_q1_1', 'u_q1_3'
                  ,'question273_273___17'
                  ,'moca'
                  ,'NP1COG2'
                  ,'MoCA26' ]
    vars_keep2 = ['question260_260']  # question260_260 (year of onset)
    data = data[vars_keep1 + vars_keep2]
    
    # To rename the patient ID
    data.rename(columns = {'cdisc_dm_usubjd': 'PATNO'}, inplace = True)
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Date of baseline visit    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vars_data_keep = ['NP4DYSKI', 'NP4DYSKI2', 'NP1DPRS', 'NP1COG', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']
    
    data_bs = baseline_data[['PATNO', 'visit_date']+vars_data_keep]

    ## Dyskinesias : NP4DYSKI, NP4WDYSK, u_q4_1_3, question273_273___17
    # data_bs.loc[ ((data_bs['NP4DYSKI'] == 0) & (data_bs['NP4WDYSK'] == 0) & (data_bs['u_q4_1_3'] == 0)) | (data_bs['question273_273___17'] == False), 'NP4DYSKI2' ] = 0
    # data_bs.loc[ (data_bs['NP4DYSKI'] > 0) | (data_bs['NP4WDYSK'] > 0) | (data_bs['u_q4_1_3'] > 0) | (data_bs['question273_273___17'] == True), 'NP4DYSKI2' ] = 1
    data_bs = data_bs[['PATNO', 'visit_date']+vars_data_keep]

    data_all = pd.merge(data_bs[['PATNO', 'visit_date']], data, how = 'left', on = ['PATNO'])
    # data_all['sv_basic_date'] = data_all['sv_basic_date'].apply(lambda d: datetime.fromtimestamp(int(d)/1000).strftime('%Y-%m-%d %H:%M:%S'))
    data_all['diff_days'] = ((pd.DatetimeIndex(data_all['sv_basic_date']) - pd.DatetimeIndex(data_all['visit_date'])) / np.timedelta64(1, 'D'))

    data_all.loc[ ((data_all['u_q4_2'] == 0) & (data_all['u_q4_1'] == 0) & (data_all['u_q4_1_3'] == 0)) | (data_all['question273_273___17'] == False), 'NP4DYSKI2' ] = 0
    data_all.loc[ (data_all['u_q4_2'] > 0) | (data_all['u_q4_1'] > 0) | (data_all['u_q4_1_3'] > 0) | (data_all['question273_273___17'] == True), 'NP4DYSKI2' ] = 1
    
    # Motor fluctuation
    # data_all['NP4FLCTX2'] = np.where( data_all['u_q4_5'].isin([1, 2, 3, 4]), 1, np.where( data_all['u_q4_5'] == 0, 0, np.nan )  )
    data_all['NP4FLCTX2'] = np.where( (data_all['u_q4_3'] > 0) | (data_all['u_q4_4'] > 0) | (data_all['u_q4_5'] > 0)
                                     ,1
                                     ,np.where( (data_all['u_q4_3'] == 0) | (data_all['u_q4_4'] == 0) | (data_all['u_q4_5'] == 0), 0, np.nan ) )

    # Motor fluctuation composite score
    data_all['fluctuation'] = np.where( (~data_all['u_q4_3'].isnull()) & (~data_all['u_q4_4'].isnull()) & (~data_all['u_q4_5'].isnull())
                                       ,data_all['u_q4_3'] + data_all['u_q4_4'] + data_all['u_q4_5'], np.nan )

    data_all_keep = data_all.copy()
    data_all = data_all[ [x if x != 'cdisc_dm_usubjd' else 'PATNO' for x in vars_keep1 ] + ['NP4DYSKI2', 'NP4FLCTX2', 'fluctuation'] ]



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 1    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # data_y1 = data_all[ (data_all['diff_days'] > 0) & (data_all['diff_days'] <= 365.25) ]
    data_y1 = data_all[ data_all['EVENT_ID'] == 'Y1' ]
    data_y1 = data_y1.groupby(['PATNO'])[['u_q4_2', 'NP4DYSKI2', 'u_q1_3', 'u_q1_1', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']].agg('max').reset_index()
    
    # rename the variables
    data_y1.rename(columns = {'u_q4_2': 'NP4DYSKI_Y1'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y1'
                             ,'u_q1_3': 'NP1DPRS_Y1'
                             ,'u_q1_1': 'NP1COG_Y1'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y1'
                             ,'fluctuation': 'fluctuation_Y1'
                             ,'moca': 'moca_Y1'
                             ,'NP1COG2': 'NP1COG2_Y1'
                             ,'MoCA26': 'MoCA26_Y1' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 2    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # data_y2 = data_all[ (data_all['diff_days'] > 365.25) & (data_all['diff_days'] <= (365.25*2)) ]
    data_y2 = data_all[ data_all['EVENT_ID'] == 'Y2' ]
    data_y2 = data_y2.groupby(['PATNO'])[['u_q4_2', 'NP4DYSKI2', 'u_q1_3', 'u_q1_1', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']].agg('max').reset_index()
    
    # rename the variables
    data_y2.rename(columns = {'u_q4_2': 'NP4DYSKI_Y2'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y2'
                             ,'u_q1_3': 'NP1DPRS_Y2'
                             ,'u_q1_1': 'NP1COG_Y2'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y2'
                             ,'fluctuation': 'fluctuation_Y2'
                             ,'moca': 'moca_Y2'
                             ,'NP1COG2': 'NP1COG2_Y2'
                             ,'MoCA26': 'MoCA26_Y2' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 3    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # data_y3 = data_all[ (data_all['diff_days'] > (365.25*2)) & (data_all['diff_days'] <= (365.25*3)) ]
    data_y3 = data_all[ data_all['EVENT_ID'] == 'Y3' ]
    data_y3 = data_y3.groupby(['PATNO'])[['u_q4_2', 'NP4DYSKI2', 'u_q1_3', 'u_q1_1', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']].agg('max').reset_index()
    
    # rename the variables
    data_y3.rename(columns = {'u_q4_2': 'NP4DYSKI_Y3'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y3'
                             ,'u_q1_3': 'NP1DPRS_Y3'
                             ,'u_q1_1': 'NP1COG_Y3'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y3'
                             ,'fluctuation': 'fluctuation_Y3'
                             ,'moca': 'moca_Y3'
                             ,'NP1COG2': 'NP1COG2_Y3'
                             ,'MoCA26': 'MoCA26_Y3' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 4    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # data_y4 = data_all[ (data_all['diff_days'] > (365.25*3)) & (data_all['diff_days'] <= (365.25*4)) ]
    data_y4 = data_all[ data_all['EVENT_ID'] == 'Y4' ]
    data_y4 = data_y4.groupby(['PATNO'])[['u_q4_2', 'NP4DYSKI2', 'u_q1_3', 'u_q1_1', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']].agg('max').reset_index()
    
    # rename the variables
    data_y4.rename(columns = {'u_q4_2': 'NP4DYSKI_Y4'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y4'
                             ,'u_q1_3': 'NP1DPRS_Y4'
                             ,'u_q1_1': 'NP1COG_Y4'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y4'
                             ,'fluctuation': 'fluctuation_Y4'
                             ,'moca': 'moca_Y4'
                             ,'NP1COG2': 'NP1COG2_Y4'
                             ,'MoCA26': 'MoCA26_Y4' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 5    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # data_y5 = data_all[ (data_all['diff_days'] > (365.25*4)) & (data_all['diff_days'] <= (365.25*5)) ]
    data_y5 = data_all[ data_all['EVENT_ID'] == 'Y5' ]
    data_y5 = data_y5.groupby(['PATNO'])[['u_q4_2', 'NP4DYSKI2', 'u_q1_3', 'u_q1_1', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']].agg('max').reset_index()
    
    # rename the variables
    data_y5.rename(columns = {'u_q4_2': 'NP4DYSKI_Y5'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y5'
                             ,'u_q1_3': 'NP1DPRS_Y5'
                             ,'u_q1_1': 'NP1COG_Y5'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y5'
                             ,'fluctuation': 'fluctuation_Y5'
                             ,'moca': 'moca_Y5'
                             ,'NP1COG2': 'NP1COG2_Y5'
                             ,'MoCA26': 'MoCA26_Y5' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To merge the variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_comb = data_bs.drop(['visit_date'], axis = 1)
    data_comb = pd.merge(data_comb, data_y1, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y2, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y3, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y4, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y5, how = 'left', on = ['PATNO'])


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####   Original variable of the follow-up variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_ORI'] = data_comb['NP4DYSKI2']
    data_comb['NP4DYSKI2_Y1_ORI'] = data_comb['NP4DYSKI2_Y1']
    data_comb['NP4DYSKI2_Y2_ORI'] = data_comb['NP4DYSKI2_Y2']
    data_comb['NP4DYSKI2_Y3_ORI'] = data_comb['NP4DYSKI2_Y3']
    data_comb['NP4DYSKI2_Y4_ORI'] = data_comb['NP4DYSKI2_Y4']
    data_comb['NP4DYSKI2_Y5_ORI'] = data_comb['NP4DYSKI2_Y5']

    data_comb['NP4FLCTX2_ORI'] = data_comb['NP4FLCTX2']
    data_comb['NP4FLCTX2_Y1_ORI'] = data_comb['NP4FLCTX2_Y1']
    data_comb['NP4FLCTX2_Y2_ORI'] = data_comb['NP4FLCTX2_Y2']
    data_comb['NP4FLCTX2_Y3_ORI'] = data_comb['NP4FLCTX2_Y3']
    data_comb['NP4FLCTX2_Y4_ORI'] = data_comb['NP4FLCTX2_Y4']
    data_comb['NP4FLCTX2_Y5_ORI'] = data_comb['NP4FLCTX2_Y5']

    data_comb['NP1COG2_ORI'] = data_comb['NP1COG2']
    data_comb['NP1COG2_Y1_ORI'] = data_comb['NP1COG2_Y1']
    data_comb['NP1COG2_Y2_ORI'] = data_comb['NP1COG2_Y2']
    data_comb['NP1COG2_Y3_ORI'] = data_comb['NP1COG2_Y3']
    data_comb['NP1COG2_Y4_ORI'] = data_comb['NP1COG2_Y4']
    data_comb['NP1COG2_Y5_ORI'] = data_comb['NP1COG2_Y5']

    data_comb['MoCA26_ORI'] = data_comb['MoCA26']
    data_comb['MoCA26_Y1_ORI'] = data_comb['MoCA26_Y1']
    data_comb['MoCA26_Y2_ORI'] = data_comb['MoCA26_Y2']
    data_comb['MoCA26_Y3_ORI'] = data_comb['MoCA26_Y3']
    data_comb['MoCA26_Y4_ORI'] = data_comb['MoCA26_Y4']
    data_comb['MoCA26_Y5_ORI'] = data_comb['MoCA26_Y5']


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Calculate the maximum of the binary outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_Y1'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1']].max(axis = 1)
    data_comb['NP4FLCTX2_Y1'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1']].max(axis = 1)
    data_comb['NP1COG2_Y1']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1']].max(axis = 1)
    data_comb['MoCA26_Y1']   = data_comb[['MoCA26'  , 'MoCA26_Y1']].max(axis = 1)

    data_comb['NP4DYSKI2_Y2'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2']].max(axis = 1)
    data_comb['NP4FLCTX2_Y2'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2']].max(axis = 1)
    data_comb['NP1COG2_Y2']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2']].max(axis = 1)
    data_comb['MoCA26_Y2']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2']].max(axis = 1)

    data_comb['NP4DYSKI2_Y3'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3']].max(axis = 1)
    data_comb['NP4FLCTX2_Y3'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3']].max(axis = 1)
    data_comb['NP1COG2_Y3']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3']].max(axis = 1)
    data_comb['MoCA26_Y3']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3']].max(axis = 1)

    data_comb['NP4DYSKI2_Y4'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4']].max(axis = 1)
    data_comb['NP4FLCTX2_Y4'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4']].max(axis = 1)
    data_comb['NP1COG2_Y4']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4']].max(axis = 1)
    data_comb['MoCA26_Y4']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4']].max(axis = 1)

    data_comb['NP4DYSKI2_Y5'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4', 'NP4DYSKI2_Y5']].max(axis = 1)
    data_comb['NP4FLCTX2_Y5'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4', 'NP4FLCTX2_Y5']].max(axis = 1)
    data_comb['NP1COG2_Y5']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4'  , 'NP1COG2_Y5']].max(axis = 1)
    data_comb['MoCA26_Y5']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4'  , 'MoCA26_Y5']].max(axis = 1)



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ###    Follow-up binary outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # If the maximum value of the follow-up outcome is 0 and no follow-up record, keep it as missing
    data_comb['NP4DYSKI2_Y1'] = np.where( (data_comb['NP4DYSKI2_Y1']==0) & (data_comb['NP4DYSKI2_Y1_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y1'] )
    data_comb['NP4FLCTX2_Y1'] = np.where( (data_comb['NP4FLCTX2_Y1']==0) & (data_comb['NP4FLCTX2_Y1_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y1'] )
    data_comb['NP1COG2_Y1']   = np.where( (data_comb['NP1COG2_Y1']==0)   & (data_comb['NP1COG2_Y1_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y1'] )
    data_comb['MoCA26_Y1']   = np.where( (data_comb['MoCA26_Y1']==0)   & (data_comb['MoCA26_Y1_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y1'] )

    data_comb['NP4DYSKI2_Y2'] = np.where( (data_comb['NP4DYSKI2_Y2']==0) & (data_comb['NP4DYSKI2_Y2_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y2'] )
    data_comb['NP4FLCTX2_Y2'] = np.where( (data_comb['NP4FLCTX2_Y2']==0) & (data_comb['NP4FLCTX2_Y2_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y2'] )
    data_comb['NP1COG2_Y2']   = np.where( (data_comb['NP1COG2_Y2']==0)   & (data_comb['NP1COG2_Y2_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y2'] )
    data_comb['MoCA26_Y2']   = np.where( (data_comb['MoCA26_Y2']==0)   & (data_comb['MoCA26_Y2_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y2'] )

    data_comb['NP4DYSKI2_Y3'] = np.where( (data_comb['NP4DYSKI2_Y3']==0) & (data_comb['NP4DYSKI2_Y3_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y3'] )
    data_comb['NP4FLCTX2_Y3'] = np.where( (data_comb['NP4FLCTX2_Y3']==0) & (data_comb['NP4FLCTX2_Y3_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y3'] )
    data_comb['NP1COG2_Y3']   = np.where( (data_comb['NP1COG2_Y3']==0)   & (data_comb['NP1COG2_Y3_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y3'] )
    data_comb['MoCA26_Y3']   = np.where( (data_comb['MoCA26_Y3']==0)   & (data_comb['MoCA26_Y3_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y3'] )

    data_comb['NP4DYSKI2_Y4'] = np.where( (data_comb['NP4DYSKI2_Y4']==0) & (data_comb['NP4DYSKI2_Y4_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y4'] )
    data_comb['NP4FLCTX2_Y4'] = np.where( (data_comb['NP4FLCTX2_Y4']==0) & (data_comb['NP4FLCTX2_Y4_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y4'] )
    data_comb['NP1COG2_Y4']   = np.where( (data_comb['NP1COG2_Y4']==0)   & (data_comb['NP1COG2_Y4_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y4'] )
    data_comb['MoCA26_Y4']   = np.where( (data_comb['MoCA26_Y4']==0)   & (data_comb['MoCA26_Y4_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y4'] )

    data_comb['NP4DYSKI2_Y5'] = np.where( (data_comb['NP4DYSKI2_Y5']==0) & (data_comb['NP4DYSKI2_Y5_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y5'] )
    data_comb['NP4FLCTX2_Y5'] = np.where( (data_comb['NP4FLCTX2_Y5']==0) & (data_comb['NP4FLCTX2_Y5_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y5'] )
    data_comb['NP1COG2_Y5']   = np.where( (data_comb['NP1COG2_Y5']==0)   & (data_comb['NP1COG2_Y5_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y5'] )
    data_comb['MoCA26_Y5']   = np.where( (data_comb['MoCA26_Y5']==0)   & (data_comb['MoCA26_Y5_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y5'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Last observation carried forward (LOCF)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried forward for outcome > 0 (Yes)
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2'] > 0), 'NP4DYSKI2_Y1'] = data_comb['NP4DYSKI2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2'] > 0), 'NP4FLCTX2_Y1'] = data_comb['NP4FLCTX2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull())   & (data_comb['NP1COG2'] > 0)  , 'NP1COG2_Y1']   = data_comb['NP1COG2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull())   & (data_comb['MoCA26'] > 0)  , 'MoCA26_Y1']   = data_comb['MoCA26']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] > 0), 'NP4DYSKI2_Y2'] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] > 0), 'NP4FLCTX2_Y2'] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull())   & (data_comb['NP1COG2_Y1'] > 0)  , 'NP1COG2_Y2']   = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull())   & (data_comb['MoCA26_Y1'] > 0)  , 'MoCA26_Y2']   = data_comb['MoCA26_Y1']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y2'] > 0), 'NP4DYSKI2_Y3'] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y2'] > 0), 'NP4FLCTX2_Y3'] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull())   & (data_comb['NP1COG2_Y2'] > 0)  , 'NP1COG2_Y3']   = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull())   & (data_comb['MoCA26_Y2'] > 0)  , 'MoCA26_Y3']   = data_comb['MoCA26_Y2']
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y3'] > 0), 'NP4DYSKI2_Y4'] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y3'] > 0), 'NP4FLCTX2_Y4'] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull())   & (data_comb['NP1COG2_Y3'] > 0)  , 'NP1COG2_Y4']   = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull())   & (data_comb['MoCA26_Y3'] > 0)  , 'MoCA26_Y4']   = data_comb['MoCA26_Y3']
    
    # Year 5
    data_comb.loc[ (data_comb['NP4DYSKI2_Y5'].isnull()) & (data_comb['NP4DYSKI2_Y4'] > 0), 'NP4DYSKI2_Y5'] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y5'].isnull()) & (data_comb['NP4FLCTX2_Y4'] > 0), 'NP4FLCTX2_Y5'] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y5'].isnull())   & (data_comb['NP1COG2_Y4'] > 0)  , 'NP1COG2_Y5']   = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y5'].isnull())   & (data_comb['MoCA26_Y4'] > 0)  , 'MoCA26_Y5']   = data_comb['MoCA26_Y4']

    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Next Observation Carried Backward (NOCB)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried backward for outcome = 0 (NO)
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y5'] == 0), 'NP4DYSKI2_Y4' ] = data_comb['NP4DYSKI2_Y5']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y5'] == 0), 'NP4FLCTX2_Y4' ] = data_comb['NP4FLCTX2_Y5']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull())   & (data_comb['NP1COG2_Y5'] == 0)  , 'NP1COG2_Y4' ]   = data_comb['NP1COG2_Y5']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull())   & (data_comb['MoCA26_Y5'] == 0)  , 'MoCA26_Y4' ]   = data_comb['MoCA26_Y5']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y4'] == 0), 'NP4DYSKI2_Y3' ] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y4'] == 0), 'NP4FLCTX2_Y3' ] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull())   & (data_comb['NP1COG2_Y4'] == 0)  , 'NP1COG2_Y3' ]   = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull())   & (data_comb['MoCA26_Y4'] == 0)  , 'MoCA26_Y3' ]   = data_comb['MoCA26_Y4']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y3'] == 0), 'NP4DYSKI2_Y2' ] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y3'] == 0), 'NP4FLCTX2_Y2' ] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull())   & (data_comb['NP1COG2_Y3'] == 0)  , 'NP1COG2_Y2' ]   = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull())   & (data_comb['MoCA26_Y3'] == 0)  , 'MoCA26_Y2' ]   = data_comb['MoCA26_Y3']
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2_Y2'] == 0), 'NP4DYSKI2_Y1' ] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2_Y2'] == 0), 'NP4FLCTX2_Y1' ] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull())   & (data_comb['NP1COG2_Y2'] == 0)  , 'NP1COG2_Y1' ]   = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull())   & (data_comb['MoCA26_Y2'] == 0)  , 'MoCA26_Y1' ]   = data_comb['MoCA26_Y2']
    
    # Baseline (to replace to data_luxpark)
    data_comb.loc[ (data_comb['NP4DYSKI2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] == 0), 'NP4DYSKI2' ] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] == 0), 'NP4FLCTX2' ] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2'].isnull())   & (data_comb['NP1COG2_Y1'] == 0)  , 'NP1COG2' ]   = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26'].isnull())   & (data_comb['MoCA26_Y1'] == 0)  , 'MoCA26' ]   = data_comb['MoCA26_Y1']


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Additional Outcome - NP4DYSKI2_LEDD    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    patno_ledd = baseline_data[ baseline_data['levodopa'] == 1 ]['PATNO'].tolist()
    data_comb['NP4DYSKI2_LEDD'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y1'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y1'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y2'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y3'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y3'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y4'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y4'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y5'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y5'], np.nan )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4DYSKI2_status    : dyskinesia (1 - Yes; 0 - No)
    # 2. NP4DYSKI2_year      : Number of years from PD diagnosis to dyskinesia development
    # 3. excl_NP4DYSKI2_year : Exclude the patient with dyskinesia before or on baseline or no follow-up visit

    data_all_dyski_onset = data_all_keep[['PATNO', 'question260_260']]
    data_all_dyski_onset = data_all_dyski_onset[ ~data_all_dyski_onset['question260_260'].isnull() ]
    data_all_dyski_onset.groupby(['PATNO']).min(['question260_260']).reset_index(inplace = True)

    data_all_dyski = data_all_keep[['PATNO', 'visit_date', 'sv_basic_date', 'NP4DYSKI2']]
    data_all_dyski = pd.merge( data_all_dyski, data_all_dyski_onset, how = 'left', on = ['PATNO'] )
    data_all_dyski = data_all_dyski[ ~data_all_dyski['NP4DYSKI2'].isnull() ]
    data_all_dyski['year'] = data_all_dyski['sv_basic_date'].str[:4].map(float)
    data_all_dyski['sv_basic_date'] = pd.DatetimeIndex(data_all_dyski['sv_basic_date'])
    
    data_all_dyski['NP4DYSKI2_year'] = np.where( (~data_all_dyski['year'].isnull()) & (~data_all_dyski['question260_260'].isnull()) & (data_all_dyski['question260_260'] != 0 ),
                                                (data_all_dyski['year'] - data_all_dyski['question260_260']), np.nan ) # /timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_year'].isnull()) & (data_all_dyski['NP4DYSKI2_year'] < 0), 'NP4DYSKI2_year' ] = 0

    # Minimum and maximum year of visit_date and sv_basic_date
    # If minimum or maximum year of visit_date and sv_basic_date = 0 and NP4DYSKI4 = 1 then excl_NP4DYSKI2_year = 1 (Yes)
    data_diff_year = data_all_dyski.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['sv_basic_date'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                                (data_diff_year['sv_basic_date'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4DYSKI2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4DYSKI2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from year of onset to dyskinesia
    data_dyski_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ].reset_index(drop = True)
    data_dyski_1 = data_dyski_1.groupby(['PATNO']).min(['NP4DYSKI2_year']).reset_index()

    # Maximum days from date of onset to visit date (without dyskinesia)
    data_dyski_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_1['PATNO'])) ].reset_index(drop = True)
    data_dyski_0 = data_dyski_0.groupby(['PATNO']).max(['NP4DYSKI2_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_comb = pd.concat([data_dyski_0, data_dyski_1], axis = 0)
    data_dyski_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_comb.reset_index(drop = True, inplace = True)
    data_dyski_comb.rename(columns = {'NP4DYSKI2': 'NP4DYSKI2_status'}, inplace = True)
    data_dyski_comb = data_dyski_comb[['PATNO', 'NP4DYSKI2_status', 'NP4DYSKI2_year']]
    data_dyski_comb['excl_NP4DYSKI2_year'] = np.where(data_dyski_comb['PATNO'].isin(patno_excl_NP4DYSKI2_year), 1, 0)

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4DYSKI2_bs_year      : Number of years from date of enrollment to dyskinesia development

    data_all_dyski['NP4DYSKI2_bs_year'] = np.where( (~data_all_dyski['sv_basic_date'].isnull()) & (~data_all_dyski['visit_date'].isnull()),
                                                (data_all_dyski['sv_basic_date'] - data_all_dyski['visit_date'])/timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_bs_year'].isnull()) & (data_all_dyski['NP4DYSKI2_bs_year'] < 0), 'NP4DYSKI2_bs_year' ] = 0

    # Minimum days from baseline visit date to dyskinesia
    data_dyski_bs_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_1 = data_dyski_bs_1.groupby(['PATNO']).min(['NP4DYSKI2_bs_year']).reset_index()

    # Maximum days from baseline visit date to dyskinesia
    data_dyski_bs_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_bs_1['PATNO'])) ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_0 = data_dyski_bs_0.groupby(['PATNO']).max(['NP4DYSKI2_bs_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_bs_comb = pd.concat([data_dyski_bs_0, data_dyski_bs_1], axis = 0)
    data_dyski_bs_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_bs_comb.reset_index(drop = True, inplace = True)
    data_dyski_bs_comb = data_dyski_bs_comb[['PATNO', 'NP4DYSKI2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_bs_comb, how = 'left', on = ['PATNO'] )



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4FLCTX2_status    : Motor fluctuations (1 - Yea; 0 - No)
    # 2. NP4FLCTX2_year      : Number of years from PD diagnosis to motor fluctuations development
    # 3. excl_NP4FLCTX2_year : Exclude the patient with motor fluctuations before or on baseline or no follow-up visit

    data_all_flctx_onset = data_all_keep[['PATNO', 'question260_260']]
    data_all_flctx_onset = data_all_flctx_onset[ ~data_all_flctx_onset['question260_260'].isnull() ]
    data_all_flctx_onset.groupby(['PATNO']).min(['question260_260']).reset_index(inplace = True)

    data_all_flctx = data_all_keep[['PATNO', 'visit_date', 'sv_basic_date', 'NP4FLCTX2']]
    data_all_flctx = pd.merge( data_all_flctx, data_all_flctx_onset, how = 'left', on = ['PATNO'] )
    data_all_flctx = data_all_flctx[ ~data_all_flctx['NP4FLCTX2'].isnull() ]
    data_all_flctx['year'] = data_all_flctx['sv_basic_date'].str[:4].map(float)
    data_all_flctx['sv_basic_date'] = pd.DatetimeIndex(data_all_flctx['sv_basic_date'])
    
    data_all_flctx['NP4FLCTX2_year'] = np.where( (~data_all_flctx['year'].isnull()) & (~data_all_flctx['question260_260'].isnull()) & (data_all_flctx['question260_260'] != 0 ),
                                                (data_all_flctx['year'] - data_all_flctx['question260_260']), np.nan ) # /timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_year'].isnull()) & (data_all_flctx['NP4FLCTX2_year'] < 0), 'NP4FLCTX2_year' ] = 0

    # Minimum and maximum year of visit_date and sv_basic_date
    # If minimum or maximum year of visit_date and sv_basic_date = 0 and NP4FLCTX2 = 1 then excl_NP4FLCTX2_year = 1 (Yes)
    data_diff_year = data_all_flctx.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['sv_basic_date'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                                (data_diff_year['sv_basic_date'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4FLCTX2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4FLCTX2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from year of onset to motor fluctuations
    data_flctx_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ].reset_index(drop = True)
    data_flctx_1 = data_flctx_1.groupby(['PATNO']).min(['NP4FLCTX2_year']).reset_index()

    # Maximum days from date of onset to visit date (without motor fluctuations)
    data_flctx_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_1['PATNO'])) ].reset_index(drop = True)
    data_flctx_0 = data_flctx_0.groupby(['PATNO']).max(['NP4FLCTX2_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_comb = pd.concat([data_flctx_0, data_flctx_1], axis = 0)
    data_flctx_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_comb.reset_index(drop = True, inplace = True)
    data_flctx_comb.rename(columns = {'NP4FLCTX2': 'NP4FLCTX2_status'}, inplace = True)
    data_flctx_comb = data_flctx_comb[['PATNO', 'NP4FLCTX2_status', 'NP4FLCTX2_year']]
    data_flctx_comb['excl_NP4FLCTX2_year'] = np.where(data_flctx_comb['PATNO'].isin(patno_excl_NP4FLCTX2_year), 1, 0)

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4FLCTX2_bs_year      : Number of years from date of enrollment to motor fluctuations development

    data_all_flctx['NP4FLCTX2_bs_year'] = np.where( (~data_all_flctx['sv_basic_date'].isnull()) & (~data_all_flctx['visit_date'].isnull()),
                                                (data_all_flctx['sv_basic_date'] - data_all_flctx['visit_date'])/timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_bs_year'].isnull()) & (data_all_flctx['NP4FLCTX2_bs_year'] < 0), 'NP4FLCTX2_bs_year' ] = 0

    # Minimum days from baseline visit date to motor fluctuations
    data_flctx_bs_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_1 = data_flctx_bs_1.groupby(['PATNO']).min(['NP4FLCTX2_bs_year']).reset_index()

    # Maximum days from baseline visit date to motor fluctuations
    data_flctx_bs_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_bs_1['PATNO'])) ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_0 = data_flctx_bs_0.groupby(['PATNO']).max(['NP4FLCTX2_bs_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_bs_comb = pd.concat([data_flctx_bs_0, data_flctx_bs_1], axis = 0)
    data_flctx_bs_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_bs_comb.reset_index(drop = True, inplace = True)
    data_flctx_bs_comb = data_flctx_bs_comb[['PATNO', 'NP4FLCTX2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_bs_comb, how = 'left', on = ['PATNO'] )




    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive Impairment in Daily Living Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP1COG2_status    : Cognitive impairment in daily living impact (1 - Yes; 0 - No)
    # 2. NP1COG2_year      : Number of years from PD diagnosis to cognitive impairment in daily living
    # 3. excl_NP1COG2_year : Exclude the patient with cognitive impairment in daily living before or on baseline or no follow-up visit

    data_all_cogn_onset = data_all_keep[['PATNO', 'question260_260']]
    data_all_cogn_onset = data_all_cogn_onset[ ~data_all_cogn_onset['question260_260'].isnull() ]
    data_all_cogn_onset.groupby(['PATNO']).min(['question260_260']).reset_index(inplace = True)

    data_all_cogn = data_all_keep[['PATNO', 'visit_date', 'sv_basic_date', 'NP1COG2']]
    data_all_cogn = pd.merge( data_all_cogn, data_all_cogn_onset, how = 'left', on = ['PATNO'] )
    data_all_cogn = data_all_cogn[ ~data_all_cogn['NP1COG2'].isnull() ]
    data_all_cogn['year'] = data_all_cogn['sv_basic_date'].str[:4].map(float)
    data_all_cogn['sv_basic_date'] = pd.DatetimeIndex(data_all_cogn['sv_basic_date'])
    
    data_all_cogn['NP1COG2_year'] = np.where( (~data_all_cogn['year'].isnull()) & (~data_all_cogn['question260_260'].isnull()) & (data_all_cogn['question260_260'] != 0 ),
                                                (data_all_cogn['year'] - data_all_cogn['question260_260']), np.nan ) # /timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_year'].isnull()) & (data_all_cogn['NP1COG2_year'] < 0), 'NP1COG2_year' ] = 0

    # Minimum and maximum year of visit_date and sv_basic_date
    # If minimum or maximum year of visit_date and sv_basic_date = 0 and NP1COG2_year = 1 then excl_NP1COG2_year_year = 1 (Yes)
    data_diff_year = data_all_cogn.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['sv_basic_date'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                                (data_diff_year['sv_basic_date'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP1COG2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP1COG2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from year of onset to cognitive impairment
    data_cogn_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ].reset_index(drop = True)
    data_cogn_1 = data_cogn_1.groupby(['PATNO']).min(['NP1COG2_year']).reset_index()

    # Maximum days from date of onset to visit date (without cognitive impairment)
    data_cogn_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_1['PATNO'])) ].reset_index(drop = True)
    data_cogn_0 = data_cogn_0.groupby(['PATNO']).max(['NP1COG2_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_comb = pd.concat([data_cogn_0, data_cogn_1], axis = 0)
    data_cogn_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_comb.reset_index(drop = True, inplace = True)
    data_cogn_comb.rename(columns = {'NP1COG2': 'NP1COG2_status'}, inplace = True)
    data_cogn_comb = data_cogn_comb[['PATNO', 'NP1COG2_status', 'NP1COG2_year']]
    data_cogn_comb['excl_NP1COG2_year'] = np.where(data_cogn_comb['PATNO'].isin(patno_excl_NP1COG2_year), 1, 0)

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive impairment in Daily Living Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP1COG2_bs_year      : Number of years from date of enrollment to cognitive impairment in daily living impact

    data_all_cogn['NP1COG2_bs_year'] = np.where( (~data_all_cogn['sv_basic_date'].isnull()) & (~data_all_cogn['visit_date'].isnull()),
                                                (data_all_cogn['sv_basic_date'] - data_all_cogn['visit_date'])/timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_bs_year'].isnull()) & (data_all_cogn['NP1COG2_bs_year'] < 0), 'NP1COG2_bs_year' ] = 0

    # Minimum days from baseline visit date to cognitive impairment
    data_cogn_bs_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_1 = data_cogn_bs_1.groupby(['PATNO']).min(['NP1COG2_bs_year']).reset_index()

    # Maximum days from baseline visit date to cognitive impairment
    data_cogn_bs_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_bs_1['PATNO'])) ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_0 = data_cogn_bs_0.groupby(['PATNO']).max(['NP1COG2_bs_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_bs_comb = pd.concat([data_cogn_bs_0, data_cogn_bs_1], axis = 0)
    data_cogn_bs_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_bs_comb.reset_index(drop = True, inplace = True)
    data_cogn_bs_comb = data_cogn_bs_comb[['PATNO', 'NP1COG2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild Cognitive Impairment (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. MoCA26_status    : Mild cognitive impairment (1 - Yes; 0 - No)
    # 2. MoCA26_year      : Number of years from PD diagnosis to mild cognitive impairment
    # 3. excl_MoCA26_year : Exclude the patient with mild cognitive impairment before or on baseline or no follow-up visit

    data_all_moca_onset = data_all_keep[['PATNO', 'question260_260']]
    data_all_moca_onset = data_all_moca_onset[ ~data_all_moca_onset['question260_260'].isnull() ]
    data_all_moca_onset.groupby(['PATNO']).min(['question260_260']).reset_index(inplace = True)

    data_all_moca = data_all_keep[['PATNO', 'visit_date', 'sv_basic_date', 'MoCA26']]
    data_all_moca = pd.merge( data_all_moca, data_all_moca_onset, how = 'left', on = ['PATNO'] )
    data_all_moca = data_all_moca[ ~data_all_moca['MoCA26'].isnull() ]
    data_all_moca['year'] = data_all_moca['sv_basic_date'].str[:4].map(float)
    data_all_moca['sv_basic_date'] = pd.DatetimeIndex(data_all_moca['sv_basic_date'])
    
    data_all_moca['MoCA26_year'] = np.where( (~data_all_moca['year'].isnull()) & (~data_all_moca['question260_260'].isnull()) & (data_all_moca['question260_260'] != 0 ),
                                                (data_all_moca['year'] - data_all_moca['question260_260']), np.nan ) # /timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_year'].isnull()) & (data_all_moca['MoCA26_year'] < 0), 'MoCA26_year' ] = 0

    # Minimum and maximum year of visit_date and sv_basic_date
    # If minimum or maximum year of visit_date and sv_basic_date = 0 and MoCA26_year = 1 then excl_MoCA26_year_year = 1 (Yes)
    data_diff_year = data_all_moca.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['sv_basic_date'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                                (data_diff_year['sv_basic_date'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['MoCA26'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_MoCA26_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from year of onset to mild cognitive impairment
    data_moca_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ].reset_index(drop = True)
    data_moca_1 = data_moca_1.groupby(['PATNO']).min(['MoCA26_year']).reset_index()

    # Maximum days from date of onset to visit date (without mild cognitive impairment)
    data_moca_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_1['PATNO'])) ].reset_index(drop = True)
    data_moca_0 = data_moca_0.groupby(['PATNO']).max(['MoCA26_year']).reset_index()

    # Combine the data_moca_0 and data_moca_1
    data_moca_comb = pd.concat([data_moca_0, data_moca_1], axis = 0)
    data_moca_comb.sort_values(['PATNO'], inplace = True)
    data_moca_comb.reset_index(drop = True, inplace = True)
    data_moca_comb.rename(columns = {'MoCA26': 'MoCA26_status'}, inplace = True)
    data_moca_comb = data_moca_comb[['PATNO', 'MoCA26_status', 'MoCA26_year']]
    data_moca_comb['excl_MoCA26_year'] = np.where(data_moca_comb['PATNO'].isin(patno_excl_MoCA26_year), 1, 0)

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild Cognitive Impairment (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. MoCA26_bs_year      : Number of years from date of enrollment to mild cognitive impairment

    data_all_moca['MoCA26_bs_year'] = np.where( (~data_all_moca['sv_basic_date'].isnull()) & (~data_all_moca['visit_date'].isnull()),
                                                (data_all_moca['sv_basic_date'] - data_all_moca['visit_date'])/timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_bs_year'].isnull()) & (data_all_moca['MoCA26_bs_year'] < 0), 'MoCA26_bs_year' ] = 0

    # Minimum days from baseline visit date to mild cognitive impairment
    data_moca_bs_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_1 = data_moca_bs_1.groupby(['PATNO']).min(['MoCA26_bs_year']).reset_index()

    # Maximum days from baseline visit date to mild cognitive impairment
    data_moca_bs_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_bs_1['PATNO'])) ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_0 = data_moca_bs_0.groupby(['PATNO']).max(['MoCA26_bs_year']).reset_index()

    # Combine the data_moca_bs_0 and data_moca_bs_1
    data_moca_bs_comb = pd.concat([data_moca_bs_0, data_moca_bs_1], axis = 0)
    data_moca_bs_comb.sort_values(['PATNO'], inplace = True)
    data_moca_bs_comb.reset_index(drop = True, inplace = True)
    data_moca_bs_comb = data_moca_bs_comb[['PATNO', 'MoCA26_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return the processed data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    return(data_comb)
