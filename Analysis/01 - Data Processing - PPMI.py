#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import and Process PPMI Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from datetime import timedelta

exec( open("Analysis/01 - Preprocessing PPMI.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI Data Processing    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class preprocess_ppmi:

    def __init__(self, data, data_date, vars_info, event_yr = "BL"):

        global data_variables

        self.data = data
        self.data_variables = data_variables
        self.data_date = data_date
        self.vars_info = vars_info
        self.event_yr = event_yr


    def process_ppmi(self):

        data = self.data.copy()
        data_variables = self.data_variables.copy()
        data_date = self.data_date.copy()

        # To extract only the baseline data (EVENT_ID = 'BL') of Parkinson's disease patients (APPRDX = 1)
        data.rename(columns = {'COHORT': 'APPRDX'}, inplace = True)
        # data = data[ (data['EVENT_ID'] == "BL") & (data['APPRDX'] == 1) ]
        data = data[ (data['EVENT_ID'] == self.event_yr) & (data['APPRDX'] == 1) ]

        # To convert the columns to date
        # data['visit_date'] = pd.to_datetime('01/' + data['ENROLL_DATE'])  # format = '%b%Y'
        data['YEAR'] = pd.DatetimeIndex(data['visit_date']).year

        data['gen'] = np.where( data['SEX'] == 1, 1, np.where( data['SEX'] == 0, 2, np.nan ) )

        # Convert duration of PD from diagnosis to enrollment, from month to year
        data['duration'] = ( data['ENROLL_DATE'] - pd.to_datetime('01/' + data['PDDXDT'])) / timedelta(days=365.25)

        # Age at enrollment
        data['age'] = data['ENROLL_AGE'] - data['duration']

        data['BMI'] = data['WGTKG'] * 10000 / (data['HTCM']**2)  # kg/m^2

        data['updrs_totscore'] = data[['NP1RTOT', 'NP2PTOT', 'NP3TOT', 'NP4TOT']].sum(axis = 1)
        data['updrs_totscore_on'] = data[['NP1RTOT', 'NP2PTOT', 'NP3TOT_ON', 'NP4TOT']].sum(axis = 1)

        data['scopa'] = data[['scopa_gi', 'scopa_ur', 'scopa_cv', 'scopa_therm', 'scopa_sex']].max(axis = 1)
        data['scopa'] = np.where( (data['scopa_gi'].isnull()) & (data['scopa_ur'].isnull()) & (data['scopa_cv'].isnull()) & (data['scopa_therm'].isnull()) & (data['scopa_sex'].isnull()), np.nan, data['scopa'] )

        data['REM'] = data[['DRMVIVID', 'DRMAGRAC', 'DRMNOCTB', 'SLPLMBMV', 'SLPINJUR', 'DRMVERBL', 'DRMFIGHT', 'DRMUMV', 'DRMOBJFL', 'MVAWAKEN', 'DRMREMEM', 'SLPDSTRB'
                            ,'STROKE', 'HETRA', 'PARKISM', 'RLS', 'NARCLPSY', 'DEPRS', 'EPILEPSY', 'BRNINFM']].sum(axis = 1)
        data['rem_cat'] = np.where( data['REM'] < 5, 0, np.where( data['REM'] >= 5, 1, np.nan ) )

        data['LRRK2'] = np.where( (data['ENRLLRRK2'] == 1) | (data['LRRK2_POS'] == 1), 1, np.where( (data['ENRLLRRK2'] == 0) | (data['LRRK2_POS'] == 0), 0, np.nan ) )
        data['GBA'] = np.where( (data['ENRLGBA'] == 1) | (data['GBA_POS'] == 1), 1, np.where( (data['ENRLGBA'] == 0) | (data['GBA_POS'] == 0), 0, np.nan ) )

        # FEATCLRLEV = 1 or FEATNOLEVO = 1 or levodopa_enroll = 1
        data['levodopa'] = np.where( (data['FEATCLRLEV'] == 1) | (data['FEATNOLEVO'] == 1) | (data['levodopa_enroll'] == 1), 1, 0 )

        data['DEATH_STATUS'] = np.where( data['ALIVE'] == 1, 0, np.where( data['ALIVE'] == 0, 1, np.nan ) )

        data_last_fu = data_date.groupby(['PATNO'])['visit_date'].max().reset_index()
        data_last_fu.rename(columns = {'visit_date': 'LAST_FOLLOW_UP_DATE'}, inplace = True)
        data = pd.merge(data, data_last_fu, how = 'left', on = ['PATNO'])

        data['FOLLOW_UP_DAYS'] = (data['LAST_FOLLOW_UP_DATE'] - data['ENROLL_DATE'] ) / timedelta(days=1)

        # Dyskinesias
        data['NP4DYSKI2'] = np.where( (data['NP4DYSKI'] > 0) | (data['NP4WDYSK'] > 0) | (data['NP4WDYSKDEN'] > 0) | (data['FEATDYSKIN'] == 1) | (data['DYSKPRES'] == 1), 1
                                     ,np.where( (data['NP4DYSKI'] == 0) | (data['NP4WDYSK'] == 0) | (data['FEATDYSKIN'] == 0) | (data['DYSKPRES'] == 0), 0, np.nan ) )
        
        data['NP1COG2'] = np.where( data['NP1COG'].isin([0, 1]), 0, np.where( data['NP1COG'].isin([2, 3, 4]), 1, np.nan ) )
        
        # Motor fluctuation
        # NP4OFF        - Time spent in the OFF state
        # NP4FLCTI      - Functional impact of fluctuation
        # NP4FLCTX      - Complexity of motor fluctuations
        # FEATMTRFLC    - Motor fluctuation
        data['NP4FLCTX2'] = np.where( (data['NP4OFF'] > 0) | data['NP4FLCTI'] > 0 | (data['NP4FLCTX'] > 0) | (data['FEATMTRFLC'] == 1), 1
                                     ,np.where( (data['NP4OFF'] == 0) | (data['NP4FLCTI'] == 0) | (data['NP4FLCTX'] == 0) | (data['FEATMTRFLC'] == 0), 0, np.nan ) )
        
        # Initial symptom of rigidity or bradykinesia - DXRIGID, DXBRADY
        data['DXGRB'] = np.where( (data['DXRIGID'] == 1) | (data['DXBRADY'] == 1), 1
                                 ,np.where( (data['DXRIGID'] == 0) | (data['DXBRADY'] == 0), 0, np.nan ) )
        
        # Motor fluctuation composite score
        # data['fluctuation'] = np.where( (~data['NP4FLCTI'].isnull()) & (~data['NP4FLCTX'].isnull()) & (~data['NP4OFF'].isnull())
        #                                ,data['NP4FLCTI'] + data['NP4FLCTX'] + data['NP4OFF'], np.nan )
        data['fluctuation_mean'] = data[['NP4FLCTI', 'NP4FLCTX', 'NP4OFF']].mean(axis = 1)
        data['tmp_NP4FLCTI'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['NP4FLCTI'].isnull() ), data['fluctuation_mean'], data['NP4FLCTI'] )
        data['tmp_NP4FLCTX'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['NP4FLCTX'].isnull() ), data['fluctuation_mean'], data['NP4FLCTX'] )
        data['tmp_NP4OFF'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['NP4OFF'].isnull() ), data['fluctuation_mean'], data['NP4OFF'] )
        data['fluctuation'] = np.where( ~data['fluctuation_mean'].isnull(), data[['tmp_NP4FLCTI', 'tmp_NP4FLCTX', 'tmp_NP4OFF']].sum(axis = 1), np.nan )

        # MDS-UPDRS III - Rigidity
        # Missing value imputation: average of MDS-UPDRS III Rigidity
        data['rigid_mean'] = data[['NP3RIGLL_ON', 'NP3RIGLU_ON', 'NP3RIGN_ON', 'NP3RIGRL_ON', 'NP3RIGRU_ON']].mean(axis = 1)
        data['NP3RIGLL_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGLL_ON'].isnull() ), data['rigid_mean'], data['NP3RIGLL_ON'] )
        data['NP3RIGLU_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGLU_ON'].isnull() ), data['rigid_mean'], data['NP3RIGLU_ON'] )
        data['NP3RIGN_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGN_ON'].isnull() ), data['rigid_mean'], data['NP3RIGN_ON'] )
        data['NP3RIGRL_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGRL_ON'].isnull() ), data['rigid_mean'], data['NP3RIGRL_ON'] )
        data['NP3RIGRU_ON'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['NP3RIGRU_ON'].isnull() ), data['rigid_mean'], data['NP3RIGRU_ON'] )

        # Rigidity upper extremities score
        data['rigidity_upper'] = np.where( ~data['rigid_mean'].isnull(), data[['NP3RIGLU_ON', 'NP3RIGRU_ON']].sum(axis = 1), np.nan )

        # Rigidity lower extremities score
        data['rigidity_lower'] = np.where( ~data['rigid_mean'].isnull(), data[['NP3RIGLL_ON', 'NP3RIGRL_ON']].sum(axis = 1), np.nan )

        # Total rigidity score
        data['rigidity_sum'] = np.where( ~data['rigid_mean'].isnull()
                                        ,data[['NP3RIGLL_ON', 'NP3RIGLU_ON', 'NP3RIGN_ON', 'NP3RIGRL_ON', 'NP3RIGRU_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Bradykinesia
        data['brady_mean'] = data[['NP3FACXP_ON', 'NP3FTAPL_ON', 'NP3FTAPR_ON', 'NP3HMOVL_ON', 'NP3HMOVR_ON', 'NP3PRSPL_ON', 'NP3PRSPR_ON', 'NP3TTAPL_ON', 'NP3TTAPR_ON'
                                   ,'NP3LGAGL_ON', 'NP3LGAGR_ON', 'NP3BRADY_ON']].mean(axis = 1)
        data['tmp_NP3FACXP_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FACXP_ON'].isnull() ), data['brady_mean'], data['NP3FACXP_ON'] )
        data['tmp_NP3FTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FTAPL_ON'].isnull() ), data['brady_mean'], data['NP3FTAPL_ON'] )
        data['tmp_NP3FTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3FTAPR_ON'].isnull() ), data['brady_mean'], data['NP3FTAPR_ON'] )
        data['tmp_NP3HMOVL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3HMOVL_ON'].isnull() ), data['brady_mean'], data['NP3HMOVL_ON'] )
        data['tmp_NP3HMOVR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3HMOVR_ON'].isnull() ), data['brady_mean'], data['NP3HMOVR_ON'] )
        data['tmp_NP3PRSPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3PRSPL_ON'].isnull() ), data['brady_mean'], data['NP3PRSPL_ON'] )
        data['tmp_NP3PRSPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3PRSPR_ON'].isnull() ), data['brady_mean'], data['NP3PRSPR_ON'] )
        data['tmp_NP3TTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3TTAPL_ON'].isnull() ), data['brady_mean'], data['NP3TTAPL_ON'] )
        data['tmp_NP3TTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3TTAPR_ON'].isnull() ), data['brady_mean'], data['NP3TTAPR_ON'] )
        data['tmp_NP3LGAGL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3LGAGL_ON'].isnull() ), data['brady_mean'], data['NP3LGAGL_ON'] )
        data['tmp_NP3LGAGR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3LGAGR_ON'].isnull() ), data['brady_mean'], data['NP3LGAGR_ON'] )
        data['tmp_NP3BRADY_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['NP3BRADY_ON'].isnull() ), data['brady_mean'], data['NP3BRADY_ON'] )

        # Bradykinesia score
        data['bradykinesia'] = np.where( ~data['brady_mean'].isnull()
                                        ,data[['tmp_NP3FACXP_ON', 'tmp_NP3FTAPL_ON', 'tmp_NP3FTAPR_ON', 'tmp_NP3HMOVL_ON', 'tmp_NP3HMOVR_ON', 'tmp_NP3PRSPL_ON'
                                               , 'tmp_NP3PRSPR_ON', 'tmp_NP3TTAPL_ON', 'tmp_NP3TTAPR_ON', 'tmp_NP3LGAGL_ON', 'tmp_NP3LGAGR_ON', 'tmp_NP3BRADY_ON']].sum(axis = 1)
                                        ,np.nan )

        # MDS-UPDRS III : Axial symptoms
        data['axial_mean'] = data[['NP2WALK', 'NP2FREZ', 'NP3SPCH_ON', 'NP3RISNG_ON', 'NP3FRZGT_ON', 'NP3PSTBL_ON', 'NP3POSTR_ON']].mean(axis = 1)
        data['tmp_NP2WALK'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP2WALK'].isnull() ), data['axial_mean'], data['NP2WALK'] )
        data['tmp_NP2FREZ'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP2FREZ'].isnull() ), data['axial_mean'], data['NP2FREZ'] )
        data['tmp_NP3SPCH_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3SPCH_ON'].isnull() ), data['axial_mean'], data['NP3SPCH_ON'] )
        data['tmp_NP3RISNG_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3RISNG_ON'].isnull() ), data['axial_mean'], data['NP3RISNG_ON'] )
        data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3FRZGT_ON'].isnull() ), data['axial_mean'], data['NP3FRZGT_ON'] )
        data['tmp_NP3PSTBL_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3PSTBL_ON'].isnull() ), data['axial_mean'], data['NP3PSTBL_ON'] )
        data['tmp_NP3POSTR_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['NP3POSTR_ON'].isnull() ), data['axial_mean'], data['NP3POSTR_ON'] )

        # Axial symptoms score
        data['axial'] = np.where( ~data['axial_mean'].isnull()
                                        ,data[['tmp_NP2WALK', 'tmp_NP2FREZ', 'tmp_NP3SPCH_ON', 'tmp_NP3RISNG_ON', 'tmp_NP3FRZGT_ON', 'tmp_NP3PSTBL_ON', 'tmp_NP3POSTR_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Axial symptoms (selective)
        data['axial2_mean'] = data[['NP2WALK', 'NP3RISNG_ON', 'NP3PSTBL_ON', 'NP3POSTR_ON']].mean(axis = 1)
        data['tmp2_NP2WALK'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP2WALK'].isnull() ), data['axial2_mean'], data['NP2WALK'] )
        data['tmp2_NP3RISNG_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3RISNG_ON'].isnull() ), data['axial2_mean'], data['NP3RISNG_ON'] )
        data['tmp2_NP3PSTBL_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3PSTBL_ON'].isnull() ), data['axial2_mean'], data['NP3PSTBL_ON'] )
        data['tmp2_NP3POSTR_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['NP3POSTR_ON'].isnull() ), data['axial2_mean'], data['NP3POSTR_ON'] )

        # Axial symptoms score
        data['axial2'] = np.where( ~data['axial2_mean'].isnull()
                                        ,data[['tmp2_NP2WALK', 'tmp2_NP3RISNG_ON', 'tmp2_NP3PSTBL_ON', 'tmp2_NP3POSTR_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Freezing of gait
        data['gait_mean'] = data[['NP2FREZ', 'NP3FRZGT_ON']].mean(axis = 1)
        data['tmp_NP2FREZ'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['NP2FREZ'].isnull() ), data['gait_mean'], data['NP2FREZ'] )
        data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['NP3FRZGT_ON'].isnull() ), data['gait_mean'], data['NP3FRZGT_ON'] )

        # Freezing of gait score
        data['freezing_gait'] = np.where( ~data['gait_mean'].isnull()
                                        ,data[['tmp_NP2FREZ', 'tmp_NP3FRZGT_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # Tremor
        # Mean of tremor is only based on average from MDS-UPDRS 3.17
        data['tremor_mean'] = data[['NP3RTALJ_ON', 'NP3RTALL_ON', 'NP3RTALU_ON', 'NP3RTARL_ON', 'NP3RTARU_ON']].mean(axis = 1)
        data['tmp_NP2TRMR'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP2TRMR'].isnull() ), data['tremor_mean'], data['NP2TRMR'] )
        data['tmp_NP3PTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3PTRML_ON'].isnull() ), data['tremor_mean'], data['NP3PTRML_ON'] )
        data['tmp_NP3PTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3PTRMR_ON'].isnull() ), data['tremor_mean'], data['NP3PTRMR_ON'] )
        data['tmp_NP3KTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3KTRML_ON'].isnull() ), data['tremor_mean'], data['NP3KTRML_ON'] )
        data['tmp_NP3KTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3KTRMR_ON'].isnull() ), data['tremor_mean'], data['NP3KTRMR_ON'] )
        data['tmp_NP3RTALJ_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALJ_ON'].isnull() ), data['tremor_mean'], data['NP3RTALJ_ON'] )
        data['tmp_NP3RTALL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALL_ON'].isnull() ), data['tremor_mean'], data['NP3RTALL_ON'] )
        data['tmp_NP3RTALU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTALU_ON'].isnull() ), data['tremor_mean'], data['NP3RTALU_ON'] )
        data['tmp_NP3RTARL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTARL_ON'].isnull() ), data['tremor_mean'], data['NP3RTARL_ON'] )
        data['tmp_NP3RTARU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTARU_ON'].isnull() ), data['tremor_mean'], data['NP3RTARU_ON'] )
        data['tmp_NP3RTCON_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['NP3RTCON_ON'].isnull() ), data['tremor_mean'], data['NP3RTCON_ON'] )

        # Tremor score
        data['tremor'] = np.where( ~data['tremor_mean'].isnull()
                                        ,data[['tmp_NP2TRMR', 'tmp_NP3PTRML_ON', 'tmp_NP3PTRMR_ON', 'tmp_NP3KTRML_ON', 'tmp_NP3KTRMR_ON', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON'
                                               ,'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                               ,np.nan )
        
        # Rest tremor score
        data['rest_tremor_score'] = np.where( ~data['tremor_mean'].isnull()
                                             ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON'
                                                    ,'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                             ,np.nan )
        
        # Rest tremor amplitude score
        data['tremor_amplitude'] = np.where( ~data['tremor_mean'].isnull()
                                             ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON']].sum(axis = 1)
                                             ,np.nan )
        
        # Dyskinesia composite score
        data['dyski_mean'] = data[['NP4DYSKI', 'NP4WDYSK']].mean(axis = 1)
        data['tmp_NP4DYSKI'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['NP4DYSKI'].isnull() ), data['dyski_mean'], data['NP4DYSKI'] )
        data['tmp_NP4WDYSK'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['NP4WDYSK'].isnull() ), data['dyski_mean'], data['NP4WDYSK'] )
        data['dyski_score'] = np.where( ~data['dyski_mean'].isnull(), data[['tmp_NP4DYSKI', 'tmp_NP4WDYSK']].sum(axis = 1), np.nan )
        
        # Mild cognitive impairment based on MoCA score
        data['MoCA26'] = np.where( (~data['MCATOT'].isnull()) & (data['MCATOT'] < 26), 1
                                  ,np.where( (~data['MCATOT'].isnull()), 0, data['MCATOT'] ) )
        
        # Levodopa Equivalence Daily Dose (within 365 days if EVENT_ID != "BL")
        data_ledd_ = data_ledd[~data_ledd['LEDD'].str.contains('LD')]
        data_ledd_['LEDD'] = pd.to_numeric(data_ledd_['LEDD'], errors='coerce')
        data_ledd_['INFODT'] = pd.to_datetime(data_ledd_['INFODT'], format='%m/%Y')
        data_ledd_1 = data_ledd_.groupby(['PATNO', 'EVENT_ID', 'INFODT'])['LEDD'].sum().reset_index()  # Summation of LEDD
        data_ledd_1 = pd.merge(data_ledd_1, data[['PATNO', 'ENROLL_DATE']], on = ['PATNO'], how = 'left')
        data_ledd_1['DIFF'] = (data_ledd_1['INFODT'] - data_ledd_1['ENROLL_DATE']).abs().dt.days # Calculate the absolute difference in days
        data_ledd_1 = data_ledd_1[(data_ledd_1['EVENT_ID'] == 'BL') | ( (~data_ledd_1['DIFF'].isna()) & (data_ledd_1['DIFF'] < 365) )]
        data_ledd_1['DIFF_2'] = np.where(data_ledd_1['EVENT_ID'] == 'BL', 0, data_ledd_1['DIFF'])
        data_ledd_1 = data_ledd_1.sort_values(by=['PATNO', 'DIFF_2']) # Sort by 'PATNO' and 'DIFF_2'
        data_ledd_1 = data_ledd_1.drop_duplicates(subset=['PATNO'], keep='first') # Drop duplicates, keeping only the first occurrence for each 'PATNO'
        data_ledd_1.reset_index(drop=True, inplace=True)
        data = pd.merge(data, data_ledd_1[['PATNO', 'LEDD']], on = ['PATNO'], how = 'left')
        data['LEDD'] = np.where( data['levodopa'] == 0, 0, data['LEDD'] )

        # To rename the variables
        data.rename(columns = {'ANYFAMPD': 'fampd_old'
                               ,'NP1RTOT': 'updrs1_score'
                               ,'NP2PTOT': 'updrs2_score'
                               ,'NP3TOT': 'updrs3_score'
                               ,'NP3TOT_ON': 'updrs3_score_on'
                               ,'NP4TOT': 'updrs4_score'
                               ,'MCATOT': 'moca'
                               ,'JLO_TOTRAW': 'bjlot'
                               }, inplace = True)
        
        # To define the data type
        data['dt_type'] = 'PPMI'

        # Return the processed data
        self.data_process = data
        # return data
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    PPMI Data Processing - To add non-common variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def process_ppmi_all(self):
            
        data = self.data_process.copy()
        vars_info = self.vars_info

        # Ordinal (0, 1, 2, 3, 4) into binary (0, 1)
        data['freezing'] = np.where( (data['FRZGT1W'].isin([1,2,3,4])) | (data['FRZGT12M'].isin([1,2,3,4])), 1, np.where( (data['FRZGT1W'] == 0) | (data['FRZGT12M'] == 0), 0, np.nan ) )
        data['falls'] = np.where( (data['FLNFR1W'].isin([1,2,3,4])) | (data['FLNFR12M'].isin([1,2,3,4])), 1, np.where( (data['FLNFR1W'] == 0) | (data['FLNFR12M'] == 0), 0, np.nan ) )

        # Amantadine
        data['amantadine'] = np.where( data['amantadine_enroll'] == 1, 1, 0 )

        # Ordinal (1, 2, 3, 4, 5) into binary [5: Yes; 1, 2, 3, 4: No]
        vars_qol = ['NQMOB37', 'NQMOB30', 'NQMOB26', 'NQMOB32', 'NQMOB25', 'NQMOB33', 'NQMOB31', 'NQMOB28'
                    ,'NQUEX29', 'NQUEX20', 'NQUEX44', 'NQUEX36', 'NQUEX30', 'NQUEX28', 'NQUEX33', 'NQUEX37']
        for var_loop in vars_qol :
            data[var_loop] = np.where( data[var_loop] == 5, 1, np.where( data[var_loop].isin([1, 2, 3, 4]), 0, np.nan ) )

        # To keep the selected variables
        vars_keep = vars_info['PPMI Variable'].dropna()
        data = data[vars_keep]

        # To return the processed data
        self.data_process_add = data
        return data
    


    def rename_ppmi(self):

        data = self.data_process.copy()
        data_variables = self.data_variables

        # To keep the selected variables
        data = data[data_variables['PPMI Variable'].dropna()]

        # To return the processed data
        return data




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI Data Processing - NP4DYSKI, NP4WDYSK, NP1DPRS, NP1COG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_ppmi_outcome(data, baseline_data):

    data_all = data[ data['COHORT'] == 1 ]

    # Dyskinesia
    data_all['NP4DYSKI2'] = np.where( (data_all['NP4DYSKI'] > 0) | (data_all['NP4WDYSK'] > 0) | (data_all['NP4WDYSKDEN'] > 0) | (data_all['FEATDYSKIN'] == 1) | (data_all['DYSKPRES'] == 1), 1
                                     , np.where( (data_all['NP4DYSKI'] == 0) | (data_all['NP4WDYSK'] == 0) | (data_all['NP4WDYSKDEN'] == 0) | (data_all['FEATDYSKIN'] == 0) | (data_all['DYSKPRES'] == 0), 0, np.nan ) )
    
    # Motor fluctuation
    data_all['NP4FLCTX2'] = np.where( (data_all['NP4OFF'] > 0) | (data_all['NP4FLCTI'] > 0) | (data_all['NP4FLCTX'] > 0) | (data_all['FEATMTRFLC'] == 1)
                                     ,1
                                     ,np.where( (data_all['NP4OFF'] == 0) | (data_all['NP4FLCTI'] == 0) | (data_all['NP4FLCTX'] == 0) | (data_all['FEATMTRFLC'] == 0), 0, np.nan ) )

    # Motor fluctuation composite score
    data_all['fluctuation'] = np.where( (~data_all['NP4FLCTI'].isnull()) & (~data_all['NP4FLCTX'].isnull()) & (~data_all['NP4OFF'].isnull())
                                       ,data_all['NP4FLCTI'] + data_all['NP4FLCTX'] + data_all['NP4OFF'], np.nan )

    # Mild cognitive impairment based on MoCA score [MoCA < 26]
    data_all['MoCA26'] = np.where( (~data_all['MCATOT'].isnull()) & (data_all['MCATOT'] < 26), 1
                                  ,np.where( (~data_all['MCATOT'].isnull()), 0, data_all['MCATOT'] ) )
    
    # MoCA score
    data_all.rename(columns = { 'MCATOT': 'moca' }, inplace = True)

    # Cognitive impairment in daily living
    # 0 - No impact to daily life due to cognitive impairment   : NP1COG == 0 | NP1COG == 1
    # 1 - Daily life impact due to cognitive impairment         : NP1COG >= 2
    data_all['NP1COG2'] = np.where( data_all['NP1COG'].isin([0, 1]), 0, np.where( data_all['NP1COG'].isin([2, 3, 4]), 1, np.nan ) )
    # NP1COG2 is not derived in baseline_data
    # baseline_data['NP1COG2'] = np.where( baseline_data['NP1COG'].isin([0, 1]), 0, np.where( baseline_data['NP1COG'].isin([2, 3, 4]), 1, np.nan ) )

    # Copy the data into data_all_keep
    data_all_keep = data_all.copy()

    # Keep the patient ID, event ID, and outcome related variables
    vars_data_all_keep = ['NP4DYSKI', 'NP4DYSKI2', 'NP1DPRS', 'NP1COG', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']
    data_all = data_all[['PATNO', 'EVENT_ID']+vars_data_all_keep]

    # Parkinson's disease patients (APPRDX = 1)
    data_all = data_all[data['COHORT'] == 1]
    # data_all['PATNO'] = data_all['PATNO'].astype(str)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Date of baseline visit    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_bs = baseline_data[['PATNO']+vars_data_all_keep]
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    EVENT_ID = 'V04' : Vist Year 1    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y1 = data_all[ data_all['EVENT_ID'] == "V04" ]
    
    # rename the variables
    data_y1.rename(columns = {'NP4DYSKI': 'NP4DYSKI_Y1'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y1'
                             ,'NP1DPRS': 'NP1DPRS_Y1'
                             ,'NP1COG': 'NP1COG_Y1'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y1'
                             ,'fluctuation': 'fluctuation_Y1'
                             ,'moca': 'moca_Y1'
                             ,'MoCA26': 'MoCA26_Y1'
                             ,'NP1COG2': 'NP1COG2_Y1' }
                             ,inplace = True)
    data_y1.drop(['EVENT_ID'], axis = 1, inplace = True)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    EVENT_ID = 'V06' : Visit Year 2    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y2 = data_all[ data_all['EVENT_ID'] == "V06" ]
    
    # rename the variables
    data_y2.rename(columns = {'NP4DYSKI': 'NP4DYSKI_Y2'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y2'
                             ,'NP1DPRS': 'NP1DPRS_Y2'
                             ,'NP1COG': 'NP1COG_Y2'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y2'
                             ,'fluctuation': 'fluctuation_Y2'
                             ,'moca': 'moca_Y2'
                             ,'MoCA26': 'MoCA26_Y2'
                             ,'NP1COG2': 'NP1COG2_Y2' }
                             ,inplace = True)
    data_y2.drop(['EVENT_ID'], axis = 1, inplace = True)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    EVENT_ID = 'V08' : Visit Year 3    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y3 = data_all[ data_all['EVENT_ID'] == "V08" ]
    
    # rename the variables
    data_y3.rename(columns = {'NP4DYSKI': 'NP4DYSKI_Y3'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y3'
                             ,'NP1DPRS': 'NP1DPRS_Y3'
                             ,'NP1COG': 'NP1COG_Y3'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y3'
                             ,'fluctuation': 'fluctuation_Y3'
                             ,'moca': 'moca_Y3'
                             ,'MoCA26': 'MoCA26_Y3'
                             ,'NP1COG2': 'NP1COG2_Y3' }
                             ,inplace = True)
    data_y3.drop(['EVENT_ID'], axis = 1, inplace = True)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    EVENT_ID = 'V10' : Visit Year 4    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y4 = data_all[ data_all['EVENT_ID'] == "V10" ]
    
    # rename the variables
    data_y4.rename(columns = {'NP4DYSKI': 'NP4DYSKI_Y4'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y4'
                             ,'NP1DPRS': 'NP1DPRS_Y4'
                             ,'NP1COG': 'NP1COG_Y4'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y4'
                             ,'fluctuation': 'fluctuation_Y4'
                             ,'moca': 'moca_Y4'
                             ,'MoCA26': 'MoCA26_Y4'
                             ,'NP1COG2': 'NP1COG2_Y4' }
                             ,inplace = True)
    data_y4.drop(['EVENT_ID'], axis = 1, inplace = True)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    EVENT_ID = 'V12' : Visit Year 5    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y5 = data_all[ data_all['EVENT_ID'] == "V12" ]
    
    # rename the variables
    data_y5.rename(columns = {'NP4DYSKI': 'NP4DYSKI_Y5'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y5'
                             ,'NP1DPRS': 'NP1DPRS_Y5'
                             ,'NP1COG': 'NP1COG_Y5'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y5'
                             ,'fluctuation': 'fluctuation_Y5'
                             ,'moca': 'moca_Y5'
                             ,'MoCA26': 'MoCA26_Y5'
                             ,'NP1COG2': 'NP1COG2_Y5' }
                             ,inplace = True)
    data_y5.drop(['EVENT_ID'], axis = 1, inplace = True)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To merge the variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_comb = baseline_data[['PATNO']]
    data_comb = pd.merge(data_comb, data_bs, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y1, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y2, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y3, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y4, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y5, how = 'left', on = ['PATNO'])


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####   Original variable of the follow-up variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_ORI'] = data_comb['NP4DYSKI2']
    data_comb['NP4DYSKI2_Y1_ORI'] = data_comb['NP4DYSKI2_Y1']
    data_comb['NP4DYSKI2_Y2_ORI'] = data_comb['NP4DYSKI2_Y2']
    data_comb['NP4DYSKI2_Y3_ORI'] = data_comb['NP4DYSKI2_Y3']
    data_comb['NP4DYSKI2_Y4_ORI'] = data_comb['NP4DYSKI2_Y4']
    data_comb['NP4DYSKI2_Y5_ORI'] = data_comb['NP4DYSKI2_Y5']

    data_comb['NP4FLCTX2_ORI'] = data_comb['NP4FLCTX2']
    data_comb['NP4FLCTX2_Y1_ORI'] = data_comb['NP4FLCTX2_Y1']
    data_comb['NP4FLCTX2_Y2_ORI'] = data_comb['NP4FLCTX2_Y2']
    data_comb['NP4FLCTX2_Y3_ORI'] = data_comb['NP4FLCTX2_Y3']
    data_comb['NP4FLCTX2_Y4_ORI'] = data_comb['NP4FLCTX2_Y4']
    data_comb['NP4FLCTX2_Y5_ORI'] = data_comb['NP4FLCTX2_Y5']

    data_comb['NP1COG2_ORI'] = data_comb['NP1COG2']
    data_comb['NP1COG2_Y1_ORI'] = data_comb['NP1COG2_Y1']
    data_comb['NP1COG2_Y2_ORI'] = data_comb['NP1COG2_Y2']
    data_comb['NP1COG2_Y3_ORI'] = data_comb['NP1COG2_Y3']
    data_comb['NP1COG2_Y4_ORI'] = data_comb['NP1COG2_Y4']
    data_comb['NP1COG2_Y5_ORI'] = data_comb['NP1COG2_Y5']

    data_comb['MoCA26_ORI'] = data_comb['MoCA26']
    data_comb['MoCA26_Y1_ORI'] = data_comb['MoCA26_Y1']
    data_comb['MoCA26_Y2_ORI'] = data_comb['MoCA26_Y2']
    data_comb['MoCA26_Y3_ORI'] = data_comb['MoCA26_Y3']
    data_comb['MoCA26_Y4_ORI'] = data_comb['MoCA26_Y4']
    data_comb['MoCA26_Y5_ORI'] = data_comb['MoCA26_Y5']
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Calculate the maximum of the outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_Y1'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1']].max(axis = 1)
    data_comb['NP4FLCTX2_Y1'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1']].max(axis = 1)
    data_comb['NP1COG2_Y1']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1']].max(axis = 1)
    data_comb['MoCA26_Y1']   = data_comb[['MoCA26'  , 'MoCA26_Y1']].max(axis = 1)

    data_comb['NP4DYSKI2_Y2'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2']].max(axis = 1)
    data_comb['NP4FLCTX2_Y2'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2']].max(axis = 1)
    data_comb['NP1COG2_Y2']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2']].max(axis = 1)
    data_comb['MoCA26_Y2']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2']].max(axis = 1)

    data_comb['NP4DYSKI2_Y3'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3']].max(axis = 1)
    data_comb['NP4FLCTX2_Y3'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3']].max(axis = 1)
    data_comb['NP1COG2_Y3']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3']].max(axis = 1)
    data_comb['MoCA26_Y3']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3']].max(axis = 1)

    data_comb['NP4DYSKI2_Y4'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4']].max(axis = 1)
    data_comb['NP4FLCTX2_Y4'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4']].max(axis = 1)
    data_comb['NP1COG2_Y4']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4']].max(axis = 1)
    data_comb['MoCA26_Y4']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4']].max(axis = 1)

    data_comb['NP4DYSKI2_Y5'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4', 'NP4DYSKI2_Y5']].max(axis = 1)
    data_comb['NP4FLCTX2_Y5'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4', 'NP4FLCTX2_Y5']].max(axis = 1)
    data_comb['NP1COG2_Y5']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4'  , 'NP1COG2_Y5']].max(axis = 1)
    data_comb['MoCA26_Y5']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4'  , 'MoCA26_Y5']].max(axis = 1)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ###    Follow-up binary outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # If the maximum value of the follow-up outcome is 0 and no follow-up record, keep it as missing
    data_comb['NP4DYSKI2_Y1'] = np.where( (data_comb['NP4DYSKI2_Y1']==0) & (data_comb['NP4DYSKI2_Y1_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y1'] )
    data_comb['NP4FLCTX2_Y1'] = np.where( (data_comb['NP4FLCTX2_Y1']==0) & (data_comb['NP4FLCTX2_Y1_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y1'] )
    data_comb['NP1COG2_Y1']   = np.where( (data_comb['NP1COG2_Y1']  ==0) & (data_comb['NP1COG2_Y1_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y1'] )
    data_comb['MoCA26_Y1']   = np.where( (data_comb['MoCA26_Y1']  ==0) & (data_comb['MoCA26_Y1_ORI'].isnull())  , np.nan, data_comb['MoCA26'] )

    data_comb['NP4DYSKI2_Y2'] = np.where( (data_comb['NP4DYSKI2_Y2']==0) & (data_comb['NP4DYSKI2_Y2_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y2'] )
    data_comb['NP4FLCTX2_Y2'] = np.where( (data_comb['NP4FLCTX2_Y2']==0) & (data_comb['NP4FLCTX2_Y2_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y2'] )
    data_comb['NP1COG2_Y2']   = np.where( (data_comb['NP1COG2_Y2']  ==0) & (data_comb['NP1COG2_Y2_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y2'] )
    data_comb['MoCA26_Y2']   = np.where( (data_comb['MoCA26_Y2']  ==0) & (data_comb['MoCA26_Y2_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y2'] )

    data_comb['NP4DYSKI2_Y3'] = np.where( (data_comb['NP4DYSKI2_Y3']==0) & (data_comb['NP4DYSKI2_Y3_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y3'] )
    data_comb['NP4FLCTX2_Y3'] = np.where( (data_comb['NP4FLCTX2_Y3']==0) & (data_comb['NP4FLCTX2_Y3_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y3'] )
    data_comb['NP1COG2_Y3']   = np.where( (data_comb['NP1COG2_Y3']  ==0) & (data_comb['NP1COG2_Y3_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y3'] )
    data_comb['MoCA26_Y3']   = np.where( (data_comb['MoCA26_Y3']  ==0) & (data_comb['MoCA26_Y3_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y3'] )

    data_comb['NP4DYSKI2_Y4'] = np.where( (data_comb['NP4DYSKI2_Y4']==0) & (data_comb['NP4DYSKI2_Y4_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y4'] )
    data_comb['NP4FLCTX2_Y4'] = np.where( (data_comb['NP4FLCTX2_Y4']==0) & (data_comb['NP4FLCTX2_Y4_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y4'] )
    data_comb['NP1COG2_Y4']   = np.where( (data_comb['NP1COG2_Y4']  ==0) & (data_comb['NP1COG2_Y4_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y4'] )
    data_comb['MoCA26_Y4']   = np.where( (data_comb['MoCA26_Y4']  ==0) & (data_comb['MoCA26_Y4_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y4'] )

    data_comb['NP4DYSKI2_Y5'] = np.where( (data_comb['NP4DYSKI2_Y5']==0) & (data_comb['NP4DYSKI2_Y5_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y5'] )
    data_comb['NP4FLCTX2_Y5'] = np.where( (data_comb['NP4FLCTX2_Y5']==0) & (data_comb['NP4FLCTX2_Y5_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y5'] )
    data_comb['NP1COG2_Y5']   = np.where( (data_comb['NP1COG2_Y5']  ==0) & (data_comb['NP1COG2_Y5_ORI'].isnull())  , np.nan, data_comb['NP1COG2_Y5'] )
    data_comb['MoCA26_Y5']   = np.where( (data_comb['MoCA26_Y5']  ==0) & (data_comb['MoCA26_Y5_ORI'].isnull())  , np.nan, data_comb['MoCA26_Y5'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Last observation carried forward (LOCF)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried forward for outcome > 0 (Yes)
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2'] > 0), 'NP4DYSKI2_Y1'] = data_comb['NP4DYSKI2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2'] > 0), 'NP4FLCTX2_Y1'] = data_comb['NP4FLCTX2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull())   & (data_comb['NP1COG2'] > 0)  , 'NP1COG2_Y1']   = data_comb['NP1COG2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull())   & (data_comb['MoCA26'] > 0)  , 'MoCA26_Y1']   = data_comb['MoCA26']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] > 0), 'NP4DYSKI2_Y2'] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] > 0), 'NP4FLCTX2_Y2'] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull())   & (data_comb['NP1COG2_Y1'] > 0)  , 'NP1COG2_Y2']   = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull())   & (data_comb['MoCA26_Y1'] > 0)  , 'MoCA26_Y2']   = data_comb['MoCA26_Y1']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y2'] > 0), 'NP4DYSKI2_Y3'] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y2'] > 0), 'NP4FLCTX2_Y3'] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull())   & (data_comb['NP1COG2_Y2'] > 0)  , 'NP1COG2_Y3']   = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull())   & (data_comb['MoCA26_Y2'] > 0)  , 'MoCA26_Y3']   = data_comb['MoCA26_Y2']
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y3'] > 0), 'NP4DYSKI2_Y4'] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y3'] > 0), 'NP4FLCTX2_Y4'] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull())   & (data_comb['NP1COG2_Y3'] > 0)  , 'NP1COG2_Y4']   = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull())   & (data_comb['MoCA26_Y3'] > 0)  , 'MoCA26_Y4']   = data_comb['MoCA26_Y3']
    
    # Year 5
    data_comb.loc[ (data_comb['NP4DYSKI2_Y5'].isnull()) & (data_comb['NP4DYSKI2_Y4'] > 0), 'NP4DYSKI2_Y5'] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y5'].isnull()) & (data_comb['NP4FLCTX2_Y4'] > 0), 'NP4FLCTX2_Y5'] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y5'].isnull())   & (data_comb['NP1COG2_Y4'] > 0)  , 'NP1COG2_Y5']   = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y5'].isnull())   & (data_comb['MoCA26_Y4'] > 0)  , 'MoCA26_Y5']   = data_comb['MoCA26_Y4']
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Next Observation Carried Backward (NOCB)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried backward for outcome = 0 (NO)
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y5'] == 0), 'NP4DYSKI2_Y4' ] = data_comb['NP4DYSKI2_Y5']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y5'] == 0), 'NP4FLCTX2_Y4' ] = data_comb['NP4FLCTX2_Y5']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull())   & (data_comb['NP1COG2_Y5'] == 0)  , 'NP1COG2_Y4' ]   = data_comb['NP1COG2_Y5']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull())   & (data_comb['MoCA26_Y5'] == 0)  , 'MoCA26_Y4' ]   = data_comb['MoCA26_Y5']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y4'] == 0), 'NP4DYSKI2_Y3' ] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y4'] == 0), 'NP4FLCTX2_Y3' ] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull())   & (data_comb['NP1COG2_Y4'] == 0)  , 'NP1COG2_Y3' ]   = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull())   & (data_comb['MoCA26_Y4'] == 0)  , 'MoCA26_Y3' ]   = data_comb['MoCA26_Y4']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y3'] == 0), 'NP4DYSKI2_Y2' ] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y3'] == 0), 'NP4FLCTX2_Y2' ] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull()) & (data_comb['NP1COG2_Y3'] == 0), 'NP1COG2_Y2' ] = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull()) & (data_comb['MoCA26_Y3'] == 0), 'MoCA26_Y2' ] = data_comb['MoCA26_Y3']
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2_Y2'] == 0), 'NP4DYSKI2_Y1' ] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2_Y2'] == 0), 'NP4FLCTX2_Y1' ] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull())   & (data_comb['NP1COG2_Y2']   == 0), 'NP1COG2_Y1' ]   = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull())   & (data_comb['MoCA26_Y2']   == 0), 'MoCA26_Y1' ]   = data_comb['MoCA26_Y2']
    
    # Baseline (to replace to data_ppmi)
    data_comb.loc[ (data_comb['NP4DYSKI2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] == 0), 'NP4DYSKI2' ] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] == 0), 'NP4FLCTX2' ] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2'].isnull())   & (data_comb['NP1COG2_Y1']   == 0), 'NP1COG2' ]   = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26'].isnull())   & (data_comb['MoCA26_Y1']   == 0), 'MoCA26' ]   = data_comb['MoCA26_Y1']
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Additional Outcome - NP4DYSKI2_LEDD    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    patno_ledd = baseline_data[ baseline_data['levodopa'] == 1 ]['PATNO'].tolist()
    data_comb['NP4DYSKI2_LEDD'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y1'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y1'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y2'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y3'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y3'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y4'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y4'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y5'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y5'], np.nan )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4DYSKI2_status : dyskinesia (1 - Yes; 0 - No)
    # 2. NP4DYSKI2_year   : Number of years from PD diagnosis to dyskinesia development
    # 3. excl_NP4DYSKI2_year : Exclude the patient with dyskinesia before or on baseline or no follow-up visit

    data_all_keep['PDDXDT'] = pd.to_datetime('01/' + data_all_keep['PDDXDT'])
    data_all_dyski = data_all_keep[['PATNO', 'ENROLL_DATE', 'PDDXDT', 'visit_date', 'NP4DYSKI2']]
    data_all_dyski = data_all_dyski[ ~data_all_dyski['NP4DYSKI2'].isnull() ]
    
    data_all_dyski['NP4DYSKI2_year'] = np.where( (~data_all_dyski['visit_date'].isnull()) & (~data_all_dyski['PDDXDT'].isnull()),
                                                (data_all_dyski['visit_date'] - data_all_dyski['PDDXDT'])/timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_year'].isnull()) & (data_all_dyski['NP4DYSKI2_year'] < 0), 'NP4DYSKI2_year' ] = 0

    # Minimum and maximum year difference of visit_date and ENROLL_DATE
    # If minimum or maximum year difference of visit_date and ENROLL_DATE = 0 and NP4DYSKI2 = 1 then excl_NP4DYSKI2_year = 1 (Yes)
    data_diff_year = data_all_dyski.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['ENROLL_DATE'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                           (data_diff_year['ENROLL_DATE'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4DYSKI2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4DYSKI2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to dyskinesia
    data_dyski_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ]
    data_dyski_1 = data_dyski_1.groupby(['PATNO']).min(['NP4DYSKI2_year']).reset_index()

    # Maximum days from date of onset to visit date (without dyskinesia)
    data_dyski_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_1['PATNO'])) ]
    data_dyski_0 = data_dyski_0.groupby(['PATNO']).max(['NP4DYSKI2_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_comb = pd.concat([data_dyski_0, data_dyski_1], axis = 0)
    data_dyski_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_comb.reset_index(drop = True, inplace = True)
    data_dyski_comb.rename(columns = {'NP4DYSKI2': 'NP4DYSKI2_status'}, inplace = True)
    data_dyski_comb['excl_NP4DYSKI2_year'] = np.where(data_dyski_comb['PATNO'].isin(patno_excl_NP4DYSKI2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_comb, how = 'left', on = ['PATNO'] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4DYSKI2_bs_year      : Number of years from date of enrollment to dyskinesia development

    data_all_dyski['NP4DYSKI2_bs_year'] = np.where( (~data_all_dyski['visit_date'].isnull()) & (~data_all_dyski['ENROLL_DATE'].isnull()),
                                                (data_all_dyski['visit_date'] - data_all_dyski['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_bs_year'].isnull()) & (data_all_dyski['NP4DYSKI2_bs_year'] < 0), 'NP4DYSKI2_bs_year' ] = 0

    # Minimum days from baseline visit date to dyskinesia
    data_dyski_bs_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_1 = data_dyski_bs_1.groupby(['PATNO']).min(['NP4DYSKI2_bs_year']).reset_index()

    # Maximum days from baseline visit date to dyskinesia
    data_dyski_bs_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_bs_1['PATNO'])) ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_0 = data_dyski_bs_0.groupby(['PATNO']).max(['NP4DYSKI2_bs_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_bs_comb = pd.concat([data_dyski_bs_0, data_dyski_bs_1], axis = 0)
    data_dyski_bs_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_bs_comb.reset_index(drop = True, inplace = True)
    data_dyski_bs_comb = data_dyski_bs_comb[['PATNO', 'NP4DYSKI2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_bs_comb, how = 'left', on = ['PATNO'] )



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4FLCTX2_status : motor fluctuations (1 - Yes; 0 - No)
    # 2. NP4FLCTX2_year   : Number of years from PD diagnosis to motor fluctuations development
    # 3. excl_NP4FLCTX2_year : Exclude the patient with motor fluctuations before or on baseline or no follow-up visit

    # data_all_keep['PDDXDT'] = pd.to_datetime('01/' + data_all_keep['PDDXDT'])
    data_all_flctx = data_all_keep[['PATNO', 'ENROLL_DATE', 'PDDXDT', 'visit_date', 'NP4FLCTX2']]
    data_all_flctx = data_all_flctx[ ~data_all_flctx['NP4FLCTX2'].isnull() ]
    
    data_all_flctx['NP4FLCTX2_year'] = np.where( (~data_all_flctx['visit_date'].isnull()) & (~data_all_flctx['PDDXDT'].isnull()),
                                                (data_all_flctx['visit_date'] - data_all_flctx['PDDXDT'])/timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_year'].isnull()) & (data_all_flctx['NP4FLCTX2_year'] < 0), 'NP4FLCTX2_year' ] = 0

    # Minimum and maximum year difference of visit_date and ENROLL_DATE
    # If minimum or maximum year difference of visit_date and ENROLL_DATE = 0 and NP4FLCTX2 = 1 then excl_NP4FLCTX2_year = 1 (Yes)
    data_diff_year = data_all_flctx.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['ENROLL_DATE'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                           (data_diff_year['ENROLL_DATE'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4FLCTX2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4FLCTX2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to motor fluctuations
    data_flctx_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ]
    data_flctx_1 = data_flctx_1.groupby(['PATNO']).min(['NP4FLCTX2_year']).reset_index()

    # Maximum days from date of onset to visit date (without motor fluctuations)
    data_flctx_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_1['PATNO'])) ]
    data_flctx_0 = data_flctx_0.groupby(['PATNO']).max(['NP4FLCTX2_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_comb = pd.concat([data_flctx_0, data_flctx_1], axis = 0)
    data_flctx_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_comb.reset_index(drop = True, inplace = True)
    data_flctx_comb.rename(columns = {'NP4FLCTX2': 'NP4FLCTX2_status'}, inplace = True)
    data_flctx_comb['excl_NP4FLCTX2_year'] = np.where(data_flctx_comb['PATNO'].isin(patno_excl_NP4FLCTX2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4FLCTX2_bs_year      : Number of years from date of enrollment to motor fluctuations development

    data_all_flctx['NP4FLCTX2_bs_year'] = np.where( (~data_all_flctx['visit_date'].isnull()) & (~data_all_flctx['ENROLL_DATE'].isnull()),
                                                (data_all_flctx['visit_date'] - data_all_flctx['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_bs_year'].isnull()) & (data_all_flctx['NP4FLCTX2_bs_year'] < 0), 'NP4FLCTX2_bs_year' ] = 0

    # Minimum days from baseline visit date to motor fluctuations
    data_flctx_bs_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_1 = data_flctx_bs_1.groupby(['PATNO']).min(['NP4FLCTX2_bs_year']).reset_index()

    # Maximum days from baseline visit date to motor fluctuations
    data_flctx_bs_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_bs_1['PATNO'])) ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_0 = data_flctx_bs_0.groupby(['PATNO']).max(['NP4FLCTX2_bs_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_bs_comb = pd.concat([data_flctx_bs_0, data_flctx_bs_1], axis = 0)
    data_flctx_bs_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_bs_comb.reset_index(drop = True, inplace = True)
    data_flctx_bs_comb = data_flctx_bs_comb[['PATNO', 'NP4FLCTX2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_bs_comb, how = 'left', on = ['PATNO'] )




    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive Impairment in Daily Living Impact Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP1COG2_status     : Cognitive impairment in daily living impact (1 - Yes; 0 - No)
    # 2. NP1COG2_year       : Number of years from PD diagnosis to cognitive impairment in daily living impact
    # 3. excl_NP1COG2_year  : Exclude the patient with cognitive impairment (NP1COG) before or on baseline or no follow-up visit

    data_all_cogn = data_all_keep[['PATNO', 'ENROLL_DATE', 'PDDXDT', 'visit_date', 'NP1COG2']]
    data_all_cogn = data_all_cogn[ ~data_all_cogn['NP1COG2'].isnull() ]
    
    data_all_cogn['NP1COG2_year'] = np.where( (~data_all_cogn['visit_date'].isnull()) & (~data_all_cogn['PDDXDT'].isnull()),
                                             (data_all_cogn['visit_date'] - data_all_cogn['PDDXDT'])/timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_year'].isnull()) & (data_all_cogn['NP1COG2_year'] < 0), 'NP1COG2_year' ] = 0

    # Minimum and maximum year difference of visit_date and ENROLL_DATE
    # If minimum or maximum year difference of visit_date and ENROLL_DATE = 0 and NP1COG2 = 1 then excl_NP1COG2_year = 1 (Yes)
    data_diff_year = data_all_cogn.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['ENROLL_DATE'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                           (data_diff_year['ENROLL_DATE'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP1COG2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP1COG2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to cognitive impairment
    data_cogn_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ]
    data_cogn_1 = data_cogn_1.groupby(['PATNO']).min(['NP1COG2_year']).reset_index()

    # Maximum days from date of onset to visit date (without cognitive impairment)
    data_cogn_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_1['PATNO'])) ]
    data_cogn_0 = data_cogn_0.groupby(['PATNO']).max(['NP1COG2_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_comb = pd.concat([data_cogn_0, data_cogn_1], axis = 0)
    data_cogn_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_comb.reset_index(drop = True, inplace = True)
    data_cogn_comb.rename(columns = {'NP1COG2': 'NP1COG2_status'}, inplace = True)
    data_cogn_comb['excl_NP1COG2_year'] = np.where(data_cogn_comb['PATNO'].isin(patno_excl_NP1COG2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive Impairment in Daily Living Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP1COG2_bs_year      : Number of years from date of enrollment to cognitive impairment in daily living impact

    data_all_cogn['NP1COG2_bs_year'] = np.where( (~data_all_cogn['visit_date'].isnull()) & (~data_all_cogn['ENROLL_DATE'].isnull()),
                                                (data_all_cogn['visit_date'] - data_all_cogn['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_bs_year'].isnull()) & (data_all_cogn['NP1COG2_bs_year'] < 0), 'NP1COG2_bs_year' ] = 0

    # Minimum days from baseline visit date to cognitive impairment in daily living impact
    data_cogn_bs_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_1 = data_cogn_bs_1.groupby(['PATNO']).min(['NP1COG2_bs_year']).reset_index()

    # Maximum days from baseline visit date to cognitive impairment
    data_cogn_bs_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_bs_1['PATNO'])) ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_0 = data_cogn_bs_0.groupby(['PATNO']).max(['NP1COG2_bs_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_bs_comb = pd.concat([data_cogn_bs_0, data_cogn_bs_1], axis = 0)
    data_cogn_bs_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_bs_comb.reset_index(drop = True, inplace = True)
    data_cogn_bs_comb = data_cogn_bs_comb[['PATNO', 'NP1COG2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild Cognitive Impairment Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. MoCA26_status     : Mild cognitive impairment (1 - Yes; 0 - No)
    # 2. MoCA26_year       : Number of years from PD diagnosis to mild cognitive impairment
    # 3. excl_MoCA26_year  : Exclude the patient with mild cognitive impairment (MoCA26) before or on baseline or no follow-up visit

    data_all_moca = data_all_keep[['PATNO', 'ENROLL_DATE', 'PDDXDT', 'visit_date', 'MoCA26']]
    data_all_moca = data_all_moca[ ~data_all_moca['MoCA26'].isnull() ]
    
    data_all_moca['MoCA26_year'] = np.where( (~data_all_moca['visit_date'].isnull()) & (~data_all_moca['PDDXDT'].isnull()),
                                             (data_all_moca['visit_date'] - data_all_moca['PDDXDT'])/timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_year'].isnull()) & (data_all_moca['MoCA26_year'] < 0), 'MoCA26_year' ] = 0

    # Minimum and maximum year difference of visit_date and ENROLL_DATE
    # If minimum or maximum year difference of visit_date and ENROLL_DATE = 0 and NP1COG2 = 1 then excl_NP1COG2_year = 1 (Yes)
    data_diff_year = data_all_moca.copy()
    data_diff_year['diff_year'] = np.where( (~data_diff_year['ENROLL_DATE'].isnull()) & (~data_diff_year['visit_date'].isnull()),
                                           (data_diff_year['ENROLL_DATE'] - data_diff_year['visit_date'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['MoCA26'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_MoCA26_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to mild cognitive impairment
    data_moca_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ]
    data_moca_1 = data_moca_1.groupby(['PATNO']).min(['MoCA26_year']).reset_index()

    # Maximum days from date of onset to visit date (without cognitive impairment)
    data_moca_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_1['PATNO'])) ]
    data_moca_0 = data_moca_0.groupby(['PATNO']).max(['MoCA26_year']).reset_index()

    # Combine the data_moca_0 and data_moca_1
    data_moca_comb = pd.concat([data_moca_0, data_moca_1], axis = 0)
    data_moca_comb.sort_values(['PATNO'], inplace = True)
    data_moca_comb.reset_index(drop = True, inplace = True)
    data_moca_comb.rename(columns = {'MoCA26': 'MoCA26_status'}, inplace = True)
    data_moca_comb['excl_MoCA26_year'] = np.where(data_moca_comb['PATNO'].isin(patno_excl_MoCA26_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild Cognitive Impairment (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. MoCA26_bs_year      : Number of years from date of enrollment to mild cognitive impairment

    data_all_moca['MoCA26_bs_year'] = np.where( (~data_all_moca['visit_date'].isnull()) & (~data_all_moca['ENROLL_DATE'].isnull()),
                                                (data_all_moca['visit_date'] - data_all_moca['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_bs_year'].isnull()) & (data_all_moca['MoCA26_bs_year'] < 0), 'MoCA26_bs_year' ] = 0

    # Minimum days from baseline visit date to mild cognitive impairment
    data_moca_bs_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_1 = data_moca_bs_1.groupby(['PATNO']).min(['MoCA26_bs_year']).reset_index()

    # Maximum days from baseline visit date to mild cognitive impairment
    data_moca_bs_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_bs_1['PATNO'])) ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_0 = data_moca_bs_0.groupby(['PATNO']).max(['MoCA26_bs_year']).reset_index()

    # Combine the data_moca_0 and data_moca_1
    data_moca_bs_comb = pd.concat([data_moca_bs_0, data_moca_bs_1], axis = 0)
    data_moca_bs_comb.sort_values(['PATNO'], inplace = True)
    data_moca_bs_comb.reset_index(drop = True, inplace = True)
    data_moca_bs_comb = data_moca_bs_comb[['PATNO', 'MoCA26_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return the processed data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    return(data_comb)
