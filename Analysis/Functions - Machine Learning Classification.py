class ml_classification:
    """
    ml_classification - Classification in machine learning
    Parameters
    ----------
    data_repo : str
        Repository of the processed dataset
    outcome : str
        Outcome variable
    vars_ml : list
        List of variables to be included in the prediction
    normalize_method : str
        Normalization method
    n_jobs : int
        Number of job for parallel computing (default: n_jobs = 2)
    B_loop : int
        Number of loop for Bootstrapping cross-validation
    k_loop : int
        Number of k for k-fold cross-validation (repeated)
    m_loop : int
        Number of m for m-fold nested cross-validation
    ml_model_set : str
        Machine learning model:
        ['figs', 'catboost', 'hs', 'gosdt', 'greedy', 'xgboost', 'tao', 'c45', 'adaboost', 'gboost']
    max_features : int
        Maximum features to be included in the model
    addon_Xtest : boolean
        To define whether the Xtest is processed with addon normalization
    undersampling : boolean
        To define whether to perform the undersampling
    feature_selection : str
        Feature selection:
        ['rfe', 'backward', 'forward', 'weight', 'boruta', 'stepwise']
    vars_encoding : list
        List of categorical variables for one-hot encoding
    seed_number : int
        To define the seed number for classifier
    vars_patid : str
        Variable of patient ID
    matching : boolean
        To perform a matching on the training data
    influential : boolean
        To perform influential diagnostics on the training data
    """

    def __init__(self
                ,data_repo
                ,outcome
                ,vars_ml
                ,normalize_method = 'NONE'
                ,n_jobs = 2
                ,B_loop = 1 # 100
                ,k_loop = 5
                ,m_loop = 3
                ,parallel_params = True
                ,ml_model_set = 'figs'
                ,max_features = [10]
                ,regularization = [0, 0.001, 0.005, 0.01, 0.05, 0.1, 0.2, 0.3]
                ,addon_Xtest = ''
                ,undersampling = False
                ,feature_selection = None
                ,vars_encoding = []
                ,seed_number = 890701
                ,vars_patid = 'PATNO'
                ,matching = False
                ,influential = False
                ,pickle_prefix = "" ) :
        
        self.data_repo = data_repo
        self.outcome = outcome
        self.vars_ml = vars_ml
        self.normalize_method = normalize_method
        self.n_jobs = n_jobs
        self.B_loop = B_loop
        self.k_loop = k_loop
        self.m_loop = m_loop
        self.parallel_params = parallel_params
        self.ml_model_set = ml_model_set
        self.max_features = max_features
        self.regularization = regularization
        self.addon_Xtest = addon_Xtest
        self.undersampling = undersampling
        self.feature_selection = feature_selection
        self.vars_encoding = vars_encoding
        self.seed_number = seed_number
        self.vars_patid = vars_patid
        self.matching = matching
        self.influential = influential
        self.pickle_prefix = pickle_prefix

    def classifier(self) :
        """
        Fits classification model
        Parameters
        ----------
        Returns
        best_model : best model
        best_k_model_auc_test : AUC score of each cross-validation
        best_model_auc_val : AUC score (validation) of the best model
        best_model_auc_test : AUC score (testing) of the best model
        best_model_B_k : B and k of the best model
        best_model_max_features : max_depth of the best model
        best_model_features : feature names of the best model

        all_model : model in each B_loop
        all_model_auc_val : AUC score (validation) in each B_loop
        all_model_auc_test : AUC score (testing) in each B_loop
        all_model_B_k : B and k of the model in each B_loop
        all_model_max_features : max_depth in each B_loop
        all_model_features : feature names in each B_loop
        """

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Initiate the lists    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # best_val_model = []
        # best_k_model_auc_test = []
        # best_val_model_auc_test = []
        # best_val_model_auc_val = []
        # best_val_model_k = []
        # best_val_model_max_features = []
        # best_val_model_features = []


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read the validation data    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        ds_validate = load_object(filename = self.pickle_prefix+'validate'+self.addon_Xtest, file_repo = self.data_repo)
        ds_training = load_object(filename = self.pickle_prefix+'training', file_repo = self.data_repo)

        if 'LOGRATIO' in self.normalize_method :
            vars_ml_ana = [ x for x in log_ratio_vars(self.vars_ml) if x in ds_training.columns.tolist() ]
        
        else :
            vars_ml_ana = [x for x in self.vars_ml if x in ds_training.columns.tolist() ]
            vars_ml_ana = [x for x in vars_ml_ana if x in ds_validate.columns.tolist()]
        
        # Undersampling in the training set
        if self.undersampling == True :
            if len(ds_training[self.outcome].value_counts().unique().tolist()) > 1 :
                ds_training = underSamplingBinary(data = ds_training, var_outcome = self.outcome, targetBalance = 0.5)
            else :
                pass
        else :
            pass

        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    ADBSACAN and propensity score matching    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if self.matching == True :
            ds_training_comb = ds_training.copy()
            ds_training_comb = ds_training_comb[['PATNO']+vars_ml_ana+[self.outcome]]
            ds_training_comb['batch'] = [ 1 if 'LUXPARK' in x else( 2 if 'PPMI' in x else 3 ) for x in ds_training_comb['PATNO'] ]
            ps_training = batchPsmDensity(data = ds_training_comb, batch_group = 'batch', indx = 'PATNO', target = 'NP4DYSKI2_Y4', pct_exclude = 0.10, min_samples = 10)
            ps_training.merge_density_ps()
            ps_training.knn_matched(matcher = 'propensity_iptw')

            ds_training = ps_training.matched_data

        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Validation and Training set    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # Validation
        X_validate = ds_validate.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana]
        y_validate = ds_validate[self.outcome]

        # Training
        X_training = ds_training.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana]
        y_training = ds_training[self.outcome]


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Function - One-hot encoding    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        def func_one_hot_encoding(ds_Xtrain, ds_Xtest, vars_encode = []) :

            if len(vars_encode) > 0 :

                list_vars_encoding = [x for x in ds_Xtrain.columns if x in vars_encode]

                for eLoop in range(len(list_vars_encoding)) :
                    
                    ds_Xtrain[ list_vars_encoding[eLoop] ] = ds_Xtrain[ list_vars_encoding[eLoop] ].map(int)
                    ds_Xtrain = pd.get_dummies(ds_Xtrain, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])

                    ds_Xtest[ list_vars_encoding[eLoop] ] = ds_Xtest[ list_vars_encoding[eLoop] ].map(int)
                    ds_Xtest = pd.get_dummies(ds_Xtest, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])
                    
                # Encoded variables that are missed in ds_Xtest
                col_Xtrain = ds_Xtrain.columns.tolist()
                col_Xtest = ds_Xtest.columns.tolist()
                col_encode_add = list(set(col_Xtrain) - set(col_Xtest))

                if len(col_encode_add) > 0 :
                    for col_add in col_encode_add :
                        ds_Xtest[col_add] = 0
                    
                else :
                    pass

                ds_Xtest = ds_Xtest[col_Xtrain]
            
            return( ds_Xtrain, ds_Xtest )
        

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Function - Feature selection    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        def func_feature_selection(ds_Xtrain, ds_Xtest, ds_y_train, fs_estimator = DecisionTreeClassifier()):

            if( self.feature_selection == 'rfe' ) :

                #******************** Note *********************#
                # Weight importance does not support FIGS, HS

                # Recursive Feature Elimination (RFE)
                if ds_Xtrain.shape[1] > 100:
                    step_rfe = 5
                elif ds_Xtrain.shape[1] > 50 & ds_Xtrain.shape[1] <= 100 :
                    step_rfe = 3
                else :
                    step_rfe = 1
                
                # if self.ml_model_set == 'xgboost' :
                #     selector = RFE(estimator = fs_estimator, step = step_rfe)
                # else:
                #     selector = RFECV(estimator = fs_estimator, step = step_rfe, cv = 5)
                selector = RFECV(estimator = fs_estimator, step = step_rfe, cv = 5)
                
                selector = selector.fit(ds_Xtrain, ds_y_train)
                selector_rank = selector.ranking_.tolist()
                selector_rank_index = [ i for i in range(len(selector_rank)) if selector_rank[i] == 1 ]
                selector_columns = [ ds_Xtrain.columns.tolist()[i] for i in range(ds_Xtrain.shape[1]) if i in selector_rank_index ]

                if len(selector_columns) < 2:
                    selector_columns = ds_Xtrain.columns.tolist()

                ds_Xtrain = ds_Xtrain[selector_columns]
                ds_Xtest = ds_Xtest[selector_columns]

            # elif( self.feature_selection == 'backward' ) :

            #     # Sequential Feature Selection (Backward selection)
            #     selector = SequentialFeatureSelector(fs_estimator, direction = 'backward', n_jobs = -1, scoring = 'roc_auc', n_features_to_select = 10)
            #     selector = selector.fit(ds_Xtrain, ds_y_train)
            #     selector_rank = selector.get_support()
            #     selector_rank_index = [ i for i in range(len(selector_rank)) if selector_rank[i] == True ]
            #     selector_columns = [ ds_Xtrain.columns.tolist()[i] for i in range(ds_Xtrain.shape[1]) if i in selector_rank_index ]

            #     if len(selector_columns) < 2:
            #         selector_columns = ds_Xtrain.columns.tolist()

            #     ds_Xtrain = ds_Xtrain[selector_columns]
            #     ds_Xtest = ds_Xtest[selector_columns]

            # elif( self.feature_selection == 'forward' ) :

            #     # Sequential Feature Selection (Forward selection)
            #     selector = SequentialFeatureSelector(fs_estimator, direction = 'forward', n_jobs = -1, scoring = 'roc_auc', n_features_to_select = 10)
            #     selector = selector.fit(ds_Xtrain, ds_y_train)
            #     selector_rank = selector.get_support()
            #     selector_rank_index = [ i for i in range(len(selector_rank)) if selector_rank[i] == True ]
            #     selector_columns = [ ds_Xtrain.columns.tolist()[i] for i in range(ds_Xtrain.shape[1]) if i in selector_rank_index ]

            #     if len(selector_columns) < 2:
            #         selector_columns = ds_Xtrain.columns.tolist()

            #     ds_Xtrain = ds_Xtrain[selector_columns]
            #     ds_Xtest = ds_Xtest[selector_columns]
            
            elif( self.feature_selection == 'weight' ) :

                #******************** Note *********************#
                # Weight importance does not support FIGS, HS

                # Feature Selection based on importance weights
                selector = SelectFromModel(estimator = fs_estimator)
                selector = selector.fit(ds_Xtrain, ds_y_train)
                selector_rank = selector.get_support()
                selector_rank_index = [ i for i in range(len(selector_rank)) if selector_rank[i] == True ]
                selector_columns = [ ds_Xtrain.columns.tolist()[i] for i in range(ds_Xtrain.shape[1]) if i in selector_rank_index ]

                if len(selector_columns) < 2:
                    selector_columns = ds_Xtrain.columns.tolist()

                ds_Xtrain = ds_Xtrain[selector_columns]
                ds_Xtest = ds_Xtest[selector_columns]
            
            elif( self.feature_selection == 'boruta' ) :

                #******************** Note *********************#
                # Boruta SHAP does not support FIGS, AdaBoost, HS

                # Boruta-SHAP feature selection
                selector = BorutaShap(model = fs_estimator, importance_measure = 'shap', classification = True)
                selector.fit(ds_Xtrain, ds_y_train, n_trials = 25, random_state = self.seed_number)

                # Display features to be removed
                features_to_remove = selector.features_to_remove

                # Important + tentative features
                selector_columns = ds_Xtrain.columns[ ~ds_Xtrain.columns.isin(features_to_remove) ]

                if len(selector_columns) < 2:
                    selector_columns = ds_Xtrain.columns.tolist()

                ds_Xtrain = ds_Xtrain[selector_columns]
                ds_Xtest = ds_Xtest[selector_columns]
            
            elif( self.feature_selection == 'stepwise' ) :
                
                if self.ml_model_set == 'xgboost' :
                    max_iter_set = 10
                else :
                    max_iter_set = 25

                # Stepwise (forward and backward) feature selection
                selector_columns = stepwise_cv(ds_X = ds_Xtrain, ds_y = ds_y_train, estimator = fs_estimator, max_iter = max_iter_set)

                if len(selector_columns) < 2:
                    selector_columns = ds_Xtrain.columns.tolist()

                ds_Xtrain = ds_Xtrain[selector_columns]
                ds_Xtest = ds_Xtest[selector_columns]
            
            else:
                pass

            return(ds_Xtrain, ds_Xtest)
        

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Function - To fit the model with hyperparameters    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if (self.ml_model_set == 'gosdt') & (len(self.max_features) > 1) :
            self.max_features = self.regularization
        elif ((self.ml_model_set == 'xgboost') | (self.ml_model_set == 'catboost')) & (len(self.max_features) > 1) :
            self.max_features = self.max_features[0:2]
        else :
            pass

        def func_cv_loop(BLoop) :
        # kfold_auc_BLoop = []
        # for BLoop in range(self.B_loop) :
            kfold_auc_m = []
            kfold_features_m = []
            kfold_perm_imp_m = []
            kfold_opt_hp_m =[]
            kfold_candidates = []
            kfold_auc = []
            # print('BLoop: '+ str(BLoop))
            # print('BLoop: '+str(BLoop)+' ; normalize_method: '+self.normalize_method+' ; addon: '+self.addon_Xtest+' ; undersampling: '+str(self.undersampling)+' ; fs: '+self.feature_selection)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the training and testing data in k-fold cross-validation (repeated)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            for kLoop in range(self.k_loop) :
                # print('kLoop: ' + str(kLoop))
                
                # Training set
                ds_train = load_object(filename = self.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1), file_repo = self.data_repo)

                # Testing set
                ds_test = load_object(filename = self.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+self.addon_Xtest, file_repo = self.data_repo)

                if 'LOGRATIO' in self.normalize_method :
                    vars_ml_ana = [ x for x in log_ratio_vars(self.vars_ml) if x in ds_train.columns.tolist() ]
                    
                else :
                    vars_ml_ana = [x for x in self.vars_ml if x in ds_train.columns.tolist() ]
                    vars_ml_ana = [x for x in vars_ml_ana if x in ds_test.columns.tolist() ]

                # Undersampling in the training set
                if self.undersampling == True :
                    if len(ds_train[self.outcome].value_counts().unique().tolist()) > 1 :
                        ds_train = underSamplingBinary(data = ds_train, var_outcome = self.outcome, targetBalance = 0.5)
                    else:
                        pass
                else :
                    pass

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    ADBSACAN and propensity score matching    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                if self.matching == True :
                    ds_train_comb = ds_train.copy()
                    ds_train_comb = ds_train_comb[['PATNO']+vars_ml_ana+[self.outcome]]
                    ds_train_comb['batch'] = [ 1 if 'LUXPARK' in x else( 2 if 'PPMI' in x else 3 ) for x in ds_train_comb['PATNO'] ]
                    ps_train = batchPsmDensity(data = ds_train_comb, batch_group = 'batch', indx = 'PATNO', target = 'NP4DYSKI2_Y4', pct_exclude = 0.10, min_samples = 10)
                    ps_train.merge_density_ps()
                    ps_train.knn_matched(matcher = 'propensity_iptw')

                    ds_train = ps_train.matched_data
                
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Training and testing set    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                X_train = ds_train.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana]
                y_train = ds_train[self.outcome]

                X_test = ds_test.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana]
                y_test = ds_test[self.outcome]


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    One-hot encoding    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                
                X_train, X_test = func_one_hot_encoding(ds_Xtrain = X_train, ds_Xtest = X_test, vars_encode = self.vars_encoding)


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Feature selection    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                # X_train, X_test = func_feature_selection(ds_Xtrain = X_train, ds_Xtest = X_test, ds_y_train = y_train, fs_estimator = fs_estimator_set)


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Influential diagnostics by ADSCAN and Cook's distance    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                if self.influential == True :
                    influential_measure = adscan_cook(ds_X = X_train, ds_y = y_train)
                    X_train, y_train = influential_measure.measure()


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    n_jobs for max_depth loops    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                # if self.n_jobs > len(self.max_features) :
                #     n_jobs_params = len(self.max_features)
                # else :
                #     n_jobs_params = self.n_jobs
                n_jobs_params = 1


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Load the data for nested cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                ls_X_train_m = dict()
                ls_X_test_m = dict()
                ls_y_train_m = dict()
                ls_y_test_m = dict()
                for mLoop in range(self.m_loop) :
                    # Load the train and test set for the nested cross-validation
                    train_m = load_object(filename = self.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1)+'_m'+str(mLoop+1), file_repo = self.data_repo)
                    test_m = load_object(filename = self.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+'_m'+str(mLoop+1), file_repo = self.data_repo)

                    # Undersampling in the training set
                    if self.undersampling == True :
                        if len(train_m[self.outcome].value_counts().unique().tolist()) > 1 :
                            train_m = underSamplingBinary(data = train_m, var_outcome = self.outcome, targetBalance = 0.5)
                        else :
                            pass
                    else :
                        pass

                    # List of variables from the features
                    vars_train_test = np.unique(train_m.columns.tolist() + test_m.columns.tolist()).tolist()
                    vars_ml_ana_m = [x for x in self.vars_ml if x in vars_train_test ]
                    vars_ml_ana_m = [x for x in vars_ml_ana_m if x in train_m.columns.tolist()]
                    vars_ml_ana_m = [x for x in vars_ml_ana_m if x in test_m.columns.tolist()]

                    # Training and testing set
                    X_train_m = train_m.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana_m]
                    y_train_m = train_m[self.outcome]

                    # Testing set
                    X_test_m = test_m.drop([self.vars_patid, self.outcome], axis = 1)[vars_ml_ana_m]
                    y_test_m = test_m[self.outcome]

                    # One-hot encoding
                    X_train_m, X_test_m = func_one_hot_encoding(ds_Xtrain = X_train_m, ds_Xtest = X_test_m, vars_encode = self.vars_encoding)

                    ls_X_train_m[mLoop] = X_train_m
                    ls_X_test_m[mLoop] = X_test_m
                    ls_y_train_m[mLoop] = y_train_m
                    ls_y_test_m[mLoop] = y_test_m
                
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Hyperparameters for each nested cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ls_hp_m = []
                for mLoop in range(self.m_loop) :
                    for hp in self.max_features :
                        ls_hp_m.append( [mLoop, hp] )

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    To fit the model (k-fold)    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                if self.ml_model_set == 'figs' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = FIGSClassifier(max_rules = hp )#, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ml_model = FIGSClassifier(max_rules = hp )#, random_state = self.seed_number)
                        ml_model.fit(X_train_m2, ls_y_train_m[m], feature_names = X_train_m2.columns.tolist() )
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')

                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                        
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                        
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = FIGSClassifier(max_rules = opt_hp_m )#, random_state = self.seed_number)
                    ml_model.fit(X_train[candidate_features_tmp], y_train, feature_names = candidate_features_tmp )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                elif self.ml_model_set == 'catboost' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        if hp <= 16 :
                            fs_estimator_set = CatBoostClassifier(iterations = 10, learning_rate = 1, loss_function='Logloss', verbose=True, random_seed = self.seed_number, max_depth = hp)
                            X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                            ml_model = CatBoostClassifier(iterations = 10, learning_rate = 1, loss_function='Logloss', verbose=True, random_seed = self.seed_number, max_depth = hp)
                            ml_model.fit(X_train_m2, ls_y_train_m[m])
                            ml_preds = ml_model.predict(X_test_m2)
                            rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')

                            # list of features
                            perm = abs( permutation_importance(ml_model
                                        ,X_test_m2
                                        ,ls_y_test_m[m]
                                        ,scoring = 'balanced_accuracy'
                                        ,random_state = self.seed_number ).importances_mean )
                            select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                            perm_imp_m = [x for x in perm if x != 0]

                        else :
                            rocAuc = 0
                            select_features_m = ['']
                            perm_imp_m = [0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = CatBoostClassifier(iterations = 10, learning_rate = 1, loss_function='Logloss', verbose=True, random_seed = self.seed_number, max_depth = opt_hp_m)
                    ml_model.fit(X_train[candidate_features_tmp], y_train )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                elif self.ml_model_set == 'hs' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = HSTreeClassifier(estimator_ = FIGSClassifier(max_rules = hp))
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ensemble = FIGSClassifier(max_rules = hp)
                        ml_model = HSTreeClassifier(estimator_ = ensemble )
                        ml_model.fit(X_train_m2, ls_y_train_m[m], feature_names = X_train_m2.columns.tolist() )
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')

                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ensemble = FIGSClassifier(max_rules = opt_hp_m)
                    ml_model = HSTreeClassifier(estimator_ = ensemble )
                    ml_model.fit(X_train[candidate_features_tmp], y_train, feature_names = candidate_features_tmp )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                elif self.ml_model_set == 'gosdt' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = OptimalTreeClassifier(random_state = self.seed_number, balance = True, regularization = hp)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ml_model = OptimalTreeClassifier(random_state = self.seed_number, balance = True, regularization = hp)
                        ml_model.fit( X_train_m2, ls_y_train_m[m] )
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')

                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                        
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = OptimalTreeClassifier(random_state = self.seed_number, balance = True, regularization = opt_hp_m)
                    ml_model.fit( X_train[candidate_features_tmp], y_train )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                elif self.ml_model_set == 'greedy' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = GreedyTreeClassifier(max_depth = hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ml_model = GreedyTreeClassifier(max_depth = hp, random_state = self.seed_number)
                        ml_model.fit(X_train_m2, ls_y_train_m[m], feature_names = X_train_m2.columns.tolist() )
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')
                        
                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})

                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )

                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = GreedyTreeClassifier(max_depth = opt_hp_m, random_state = self.seed_number)
                    ml_model.fit(X_train[candidate_features_tmp], y_train, feature_names = candidate_features_tmp )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                
                elif self.ml_model_set == 'xgboost' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = XGBClassifier(random_state = self.seed_number, max_depth = hp, learning_rate = 1, n_jobs = 2)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ml_model = XGBClassifier(random_state = self.seed_number, max_depth = hp, learning_rate = 1, n_jobs = 2)
                        ml_model.fit(X_train_m2, ls_y_train_m[m])
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')

                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = XGBClassifier(random_state = self.seed_number, max_depth = opt_hp_m, learning_rate = 1, n_jobs = 2)
                    ml_model.fit(X_train[candidate_features_tmp], y_train )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop

                
                elif self.ml_model_set == 'c45' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = C45TreeClassifier(max_rules = hp)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)
                        
                        ml_model = C45TreeClassifier(max_rules = hp)
                        ml_model.fit(X_train_m2, ls_y_train_m[m], feature_names = X_train_m2.columns.tolist() )
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')
                        
                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})

                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = C45TreeClassifier(max_rules = opt_hp_m)
                    ml_model.fit(X_train[candidate_features_tmp], y_train, feature_names = candidate_features_tmp )
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop

                
                elif self.ml_model_set == 'adaboost' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = AdaBoostClassifier(n_estimators = hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)
                        
                        ml_model = AdaBoostClassifier(n_estimators = hp, random_state = self.seed_number)
                        ml_model.fit(X_train_m2, ls_y_train_m[m])
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')
                        
                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})

                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = AdaBoostClassifier(n_estimators = opt_hp_m, random_state = self.seed_number)
                    ml_model.fit(X_train[candidate_features_tmp], y_train)
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop


                elif self.ml_model_set == 'gboost' :

                    def func_model_max_features(n_feats) :

                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        
                        fs_estimator_set = GradientBoostingClassifier(n_estimators = hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m], ds_Xtest = ls_X_test_m[m], ds_y_train = ls_y_train_m[m], fs_estimator = fs_estimator_set)

                        ml_model = GradientBoostingClassifier(n_estimators = hp, random_state = self.seed_number)
                        ml_model.fit(X_train_m2, ls_y_train_m[m])
                        ml_preds = ml_model.predict(X_test_m2)
                        rocAuc = roc_auc_score(ls_y_test_m[m], ml_preds, average = 'weighted')
                    
                        # list of features
                        perm = abs( permutation_importance(ml_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,scoring = 'balanced_accuracy'
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]
                        
                        return({'rocAuc': rocAuc, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})

                    
                    if self.parallel_params == True :
                        auc_max_features = Parallel(n_jobs = n_jobs_params)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    else :
                        auc_max_features = []
                        for n_feats in range(len(ls_hp_m)) :
                            auc_max_features.append( func_model_max_features(n_feats = n_feats) )
                    
                    # Average score of each hyperparameter
                    ls_auc_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, Lauc in zip(ls_hp_m, auc_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_auc_m:
                            ls_auc_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_auc_m[Lkey].append(Lauc['rocAuc'])
                        ls_features_m[Lkey].append(Lauc['select_features_m'])
                        ls_perm_imp_m[Lkey].append(Lauc['perm_imp_m'])
                    
                    ls_avg_auc_m = []
                    for avg_m in list(ls_auc_m.keys()) :
                        ls_avg_auc_m.append( mean(ls_auc_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_auc_m.index(max(ls_avg_auc_m))]

                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    ml_model = GradientBoostingClassifier(n_estimators = opt_hp_m, random_state = self.seed_number)
                    ml_model.fit(X_train[candidate_features_tmp], y_train)
                    ml_preds = ml_model.predict(X_test[candidate_features_tmp])
                    rocAuc = roc_auc_score(y_test, ml_preds, average = 'weighted')  # Score for outer loop with optimal hyperparameter defined in the inner loop

                
                elif self.ml_model_set == 'mean' :
                    ds_preds = y_train.value_counts().reset_index()
                    val_preds = ds_preds[ ds_preds['index'] == 1 ][self.outcome].tolist()[0] / y_train.shape[0]
                    ml_proba = np.array([val_preds] * len(y_test))
                    ml_preds = [1 if x > 0.5 else 0 for x in ml_proba]
                    
                    auc_max_features = []
                    for n_feats in self.max_features :
                        auc_max_features.append( roc_auc_score(y_test, ml_preds, average = 'weighted') )

                else :
                    pass

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    AUC score from k-fold cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                # kfold_auc.append(auc_max_features)
                kfold_auc_m.append( ls_auc_m )
                kfold_features_m.append( ls_features_m )
                kfold_perm_imp_m.append( ls_perm_imp_m )
                kfold_opt_hp_m.append( opt_hp_m )
                kfold_candidates.append( candidate_features )
                kfold_auc.append( rocAuc )

            return({'kfold_auc_m': kfold_auc_m
                    ,'kfold_features_m': kfold_features_m
                    ,'kfold_perm_imp_m': kfold_perm_imp_m
                    ,'kfold_opt_hp_m': kfold_opt_hp_m
                    ,'kfold_candidates': kfold_candidates
                    ,'kfold_auc': kfold_auc
                    })

            # kfold_auc_BLoop.append(kfold_auc_dict)

        # n_jobs for BLoop
        if self.n_jobs > self.B_loop :
            n_jobs_BLoop = self.B_loop
        else :
            n_jobs_BLoop = self.n_jobs
        

        if self.parallel_params == True :
            kfold_auc_BLoop = Parallel(n_jobs = n_jobs_BLoop)(
                delayed( func_cv_loop )(BLoop = BLoop)
                for BLoop in range(self.B_loop) )
        
        else :
            kfold_auc_BLoop = []
            for BLoop in range(self.B_loop) :
                kfold_auc_BLoop.append( func_cv_loop(BLoop = BLoop) )
        
        
        auc_overall_list = []
        opt_hp_list = []
        candidates_list = []
        for auc_overall_loop in range(len(kfold_auc_BLoop)) :
            auc_overall_list += kfold_auc_BLoop[auc_overall_loop]['kfold_auc']
            opt_hp_list += kfold_auc_BLoop[auc_overall_loop]['kfold_opt_hp_m']
            candidates_list += kfold_auc_BLoop[auc_overall_loop]['kfold_candidates']
        

        # The optimal hyperparameter with highest AUC score at repeated cross-validation
        best_model_auc_cv = max(auc_overall_list)
        best_model_auc_cv_idx = auc_overall_list.index(best_model_auc_cv)
        best_max_depth = opt_hp_list[best_model_auc_cv_idx]
        best_candidates = candidates_list[best_model_auc_cv_idx]

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Retrain the model with the optimal hyperparameter with highest AUC score in repeated cross-validation    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    One-hot encoding    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        X_training, X_validate = func_one_hot_encoding(ds_Xtrain = X_training, ds_Xtest = X_validate, vars_encode = self.vars_encoding)

        if len(best_candidates) > 0 :
            best_candidates = [x for x in best_candidates if x in X_training.columns.tolist()]
            best_candidates = [x for x in best_candidates if x in X_validate.columns.tolist()]

            X_training = X_training[best_candidates]
            X_validate = X_validate[best_candidates]
        else :
            pass

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Influential diagnostics by ADSCAN and Cook's distance    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if self.influential == True :
            influential_measure = adscan_cook(ds_X = X_training, ds_y = y_training)
            X_training, y_training = influential_measure.measure()


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Retrain the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # Retrain the model
        if self.ml_model_set == 'figs' :
            # To fit FIGS model
            best_model = FIGSClassifier(max_rules = best_max_depth )#, random_state = self.seed_number)
            best_model.fit(X_training, y_training, feature_names = X_training.columns.tolist() )
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')

        elif self.ml_model_set == 'catboost' :
            # To fit CatBoost model
            best_model = CatBoostClassifier(iterations = 10, learning_rate = 1, loss_function='Logloss', verbose=True, random_seed = self.seed_number, max_depth = best_max_depth )
            best_model.fit(X_training, y_training)
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'hs' :
            # To fit Hierarchical Shrinkage (HS)
            ensemble = FIGSClassifier(max_rules = best_max_depth)
            # ensemble = XGBClassifier(random_state = self.seed_number, max_depth = best_max_depth )
            best_model = HSTreeClassifier(estimator_=ensemble)# max_leaf_nodes = max_features)
            best_model.fit(X_training, y_training, feature_names = X_training.columns.tolist() )
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'gosdt' :
            # To fit GOSDT model
            best_model = OptimalTreeClassifier(balance = True, regularization = best_max_depth, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'greedy' :
            # To fit GREEDY model
            best_model = GreedyTreeClassifier(random_state = self.seed_number, max_depth = best_max_depth )
            best_model.fit(X_training, y_training, feature_names = X_training.columns.tolist() )
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')

        elif self.ml_model_set == 'xgboost' :
            # To fit XGBoost model
            best_model = XGBClassifier(random_state = self.seed_number, max_depth = best_max_depth, learning_rate = 1, n_jobs = 2 )
            best_model.fit(X_training, y_training)
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'c45' :
            # To fit C4.5 model
            best_model = C45TreeClassifier(max_rules = best_max_depth )
            best_model.fit(X_training, y_training, feature_names = X_training.columns.tolist() )
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'adaboost' :
            # To fit AdaBoost model
            best_model = AdaBoostClassifier(n_estimators = best_max_depth, random_state = self.seed_number )
            best_model.fit(X_training, y_training)
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')

        elif self.ml_model_set == 'gboost' :
            # To fit Gradient Boost model
            best_model = GradientBoostingClassifier(n_estimators = best_max_depth, random_state = self.seed_number )
            best_model.fit(X_training, y_training)
            best_model_preds = best_model.predict(X_validate)
            best_model_proba = best_model.predict_proba(X_validate)[:,1]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')
        
        elif self.ml_model_set == 'mean' :
            best_model = []
            ds_preds = y_training.value_counts().reset_index()
            best_model_proba = [ds_preds[ ds_preds['index'] == 1 ][self.outcome].tolist()[0] / y_training.shape[0]] * len(y_validate)
            best_model_preds = [1 if x > 0.5 else 0 for x in best_model_proba]
            best_model_auc_val = roc_auc_score(y_validate, best_model_preds, average = 'weighted')

        else :
            pass

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Return best model information    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        self.best_model = best_model                          # The final model trained on the entire training set
        self.best_model_auc_val = best_model_auc_val          # Hold-out AUC score
        self.best_model_auc_test = mean(auc_overall_list)     # Average AUC score in the 5-fold cross-validation
        self.best_model_max_features = best_max_depth         # Optimal hyperparameter used to train the final model
        self.best_model_features = best_candidates            # Candidate features used to train the final model

        self.best_model_preds = best_model_preds              # Predicted outcome based on hold-out test set
        self.best_model_proba = best_model_proba              # Predicted probability based on hold-out test set

        self.all_model_auc_test = auc_overall_list            # List AUC score on 5-fold cross-validation
        self.all_model_auc_cv = kfold_auc_BLoop               # List of AUC score, hyperparameter and candidate features in nested CV

        self.X_validate = X_validate
        self.y_validate = y_validate

        self.X_training = X_training
        self.y_training = y_training

        self.ds_validate = ds_validate
        self.ds_training = ds_training

        return({ 'best_model': best_model
                ,'best_model_auc_val': best_model_auc_val
                ,'best_model_auc_test': best_model_auc_cv
                ,'best_model_max_features': best_max_depth
                ,'best_model_features': best_candidates

                ,'best_model_preds': best_model_preds
                ,'best_model_proba': best_model_proba

                ,'all_model_auc_test': auc_overall_list
                ,'all_model_auc_cv': kfold_auc_BLoop
                })


    def performance_metric(self) :
        """
        Performance metric of the best model
        Parameters
        ----------
        Returns
        best_model_precision : Precision of the best model
        best_model_recall : Recall of the best model
        best_model_fscore : F-score of the best model
        best_model_accuracy : Accuracy of the best model
        best_model_balanced_accuracy : Balanced accuracy of the best model
        best_model_mcc : MCC of the best model
        best_model_confusion_mat : Confusion matrix of the best model
        best_model_y_val: Outcome of validation set
        best_model_y_pred: Predicted outcome in validation
        best_model_y_pred_proba: Probability of the predicted outcome in validation
        """

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Validation data    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        X_validate = self.X_validate
        y_validate = self.y_validate

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Prediction of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        model_preds = self.best_model.predict(X_validate)
        model_preds_proba = self.best_model.predict_proba(X_validate)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Precision, recall, F-score of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        precision_val, recall_val, fscore_val, _ = precision_recall_fscore_support(y_validate, model_preds, average = 'weighted')


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Accuracy of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        accuracy_val = accuracy_score(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Balanced accuracy of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        balanced_accuracy_val = balanced_accuracy_score(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Matthews Correlation Coefficient (MCC) of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        mcc_val = matthews_corrcoef(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Confusion matrix of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        confusion_mat = confusion_matrix(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Cohen's Kappa of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        cohen_kappa_val = cohen_kappa_score(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Hamming Loss of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        hamming_loss_val = hamming_loss(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Jaccard Similarity Coefficient score of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        jaccard_val = jaccard_score(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Hinge Loss of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        hinge_loss_val = hinge_loss(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Log Loss of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        log_loss_val = log_loss(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Brier Score Loss of the best model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        brier_score_loss_val = brier_score_loss(y_validate, model_preds)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Return best model information    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        self.best_model_precision = precision_val
        self.best_model_recall = recall_val
        self.best_model_fscore = fscore_val
        self.best_model_accuracy = accuracy_val
        self.best_model_balanced_accuracy = balanced_accuracy_val
        self.best_model_mcc = mcc_val
        self.best_model_confusion_mat = confusion_mat
        self.best_model_cohen_kappa = cohen_kappa_val
        self.best_model_hamming_loss = hamming_loss_val
        self.best_model_jaccard = jaccard_val
        self.best_model_hinge_loss = hinge_loss_val
        self.best_model_log_loss = log_loss_val
        self.best_model_brier_score_loss = brier_score_loss_val
        self.best_model_y_val = y_validate
        self.best_model_y_pred = model_preds
        self.best_model_y_pred_proba = model_preds_proba
    

    def permutation_importance(self) :    # To be removed
        """
        Permutation importance in each boostrapping cross-validation
        Parameters
        ----------
        Returns
        rho_stability : Spearman correlation coefficient to the average
        pval_stability : p-values of Spearman correlation coefficient to the average
        """

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Stability of the permutation importance    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        perm_importance_df = pd.DataFrame({'feature': self.vars_ml})

        for BLoop in range(self.B_loop):

            ds_validate = load_object(filename = self.pickle_prefix+'validate'+self.addon_Xtest, file_repo = self.data_repo)
            # vars_ml_ana = [x for x in self.all_model_features[BLoop] if x in ds_validate.columns.tolist() ]

            X_validate = ds_validate.drop([self.vars_patid, self.outcome], axis = 1) # [vars_ml_ana]
            y_validate = ds_validate[self.outcome]


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    One-hot encoding on validation data    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            if len(self.vars_encoding) > 0 :

                list_val_vars_encoding = [x for x in X_validate.columns if x in self.vars_encoding]

                for eLoop in range(len(list_val_vars_encoding)) :
                    
                    X_validate[ list_val_vars_encoding[eLoop] ] = X_validate[ list_val_vars_encoding[eLoop] ].map(int)
                    X_validate = pd.get_dummies(X_validate, columns = [ list_val_vars_encoding[eLoop] ], prefix = [ list_val_vars_encoding[eLoop] ])

                # vars_ml_ana = [x for x in self.best_model_features if x in X_validate.columns.tolist() ]
                vars_ml_ana = self.all_model_features[BLoop]
                col_X_validate = X_validate.columns.tolist()
                if len(list(set(vars_ml_ana) - set(col_X_validate))) > 0 :
                    for col_encode in list(set(vars_ml_ana) - set(col_X_validate)) :
                        X_validate[col_encode] = 0
            
            else :
                # vars_ml_ana = [x for x in self.best_model_features if x in ds_validate.columns.tolist() ]
                vars_ml_ana = [x for x in self.all_model_features[BLoop] if x in ds_validate.columns.tolist() ]

            X_validate = X_validate[vars_ml_ana]


            if (self.ml_model_set == 'catboost') | (self.ml_model_set == 'xgboost') | (self.ml_model_set == 'gboost') | (self.ml_model_set == 'adaboost') :
                perm_importance = self.all_model[BLoop].feature_importances_
            
            else :
                perm_importance = abs( permutation_importance(self.all_model[BLoop], X_validate, y_validate, scoring = 'balanced_accuracy').importances_mean )
            
            perm_importance_df = pd.merge( perm_importance_df, pd.DataFrame({'feature': X_validate.columns.tolist(), 'B_'+str(BLoop): perm_importance }), how = 'left' )

        perm_importance_array = np.array(perm_importance_df.fillna(0).drop('feature', axis = 1))

        # Overall mean of permutation importance of each feature
        overall_mean_features = np.mean(perm_importance_array, axis = 1)

        rho_stability = []
        pval_stability = []
        for BLoop in range(self.B_loop) :
            rho, pval = spearmanr(overall_mean_features[overall_mean_features != 0], perm_importance_array[:,BLoop][overall_mean_features != 0])
            if np.isnan(rho) == True:
                rho = 0
            if np.isnan(pval) == True:
                pval = 1
            rho_stability.append(rho)
            pval_stability.append(pval)
        

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Return best model information    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        self.rho_stability = rho_stability
        self.pval_stability = pval_stability



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To create the SHAP values plot    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def shap_values(model, fig_repo, fig_name, fig_features, max_vars = 15, fig_size = [12, 5], max_evals_shap = None ) :
    """
    SHAP values of the best model
    Parameters
    ----------
    fig_repo : Repository of the figure
    fig_name : Filename of the figure
    fig_features : DataFrame of feature names
    max_vars : Maximum number of features to be shown in the SHAP values plot
    fig_size : Figure size
    ----------
    Returns
    shap_values : SHAP values of the best model
    feature_shap_values : Feature names
    feature_importance : Feature importance based on SHAP values

    fig_beeswarm : Beeswarm plot for SHAP values
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Validation data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    X_validate = pd.concat([model.X_validate, model.X_training], axis = 0)
    y_validate = pd.concat([model.y_validate, model.y_training], axis = 0)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    SHAP values of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Fits the explainer
    explainer = shap.Explainer(model.best_model.predict, X_validate)

    # Calculate the SHAP values - It takes some time
    if max_evals_shap == None :
        max_evals = X_validate.shape[1] * 2 + 2500
    else :
        max_evals = max_evals_shap
    
    shap_values = explainer(X_validate, max_evals = max_evals )
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Feature importance (SHAP values)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vals = np.abs(shap_values.values).mean(0)
    feature_importance = pd.DataFrame(list(zip(X_validate.columns.tolist(), vals)),
                                    columns=['col_name','feature_importance_vals'])
    feature_importance = feature_importance[feature_importance['feature_importance_vals'] != 0]
    feature_importance.sort_values(by=['feature_importance_vals'], ascending=False, inplace=True)
    feature_importance = feature_importance.reset_index().drop(['index'], axis = 1)

    # idx_shap = [idx for idx in range(len(vals)) if vals[idx] > 0]
    sorted_idx_shap = [i for i in np.argsort(vals)[::-1]]
    idx_shap = [x for x in sorted_idx_shap if vals[x] > 0][0:max_vars]

    shap_values.values = shap_values.values[:, idx_shap]
    shap_values.data = shap_values.data[:, idx_shap]
    shap_values.feature_names = [shap_values.feature_names[x] for x in idx_shap]


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Feature names    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_fig_features = pd.merge( pd.DataFrame({'Variable': shap_values.feature_names}), fig_features, how = 'left', on = ['Variable'] )['Description'].tolist()
    shap_values.feature_names = ls_fig_features


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Beeswarm plot of SHAP values    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Maximum number of features to be shown
    num_vars = min(max_vars, feature_importance.shape[0])

    # Adjusting the 'shap_x_label' label
    if (num_vars > 10) & (num_vars <= 15) :
        adjust_text_y = -1.8 + (15-num_vars)*0.05
    elif (num_vars >3) & (num_vars <=10) :
        adjust_text_y = -1.8 + (15-num_vars)*0.1
    elif num_vars == 3 :
        adjust_text_y = -0.5
    elif num_vars == 2 :
        adjust_text_y = -0.4
    elif num_vars == 1 :
        adjust_text_y = -0.25
    else :
        adjust_text_y = -0.75

    # SHAP values plot
    plt.figure()
    plt.style.context("seaborn-white")
    plt.axes().set_facecolor('#F8F8F7')
    fig_beeswarm = shap.plots.beeswarm(shap_values, max_display = num_vars, plot_size = fig_size, show = False)
    plt.annotate(shap_x_label[0]
                ,xy = (plt.xlim()[0], -1)
                ,xytext = (plt.xlim()[0], plt.ylim()[0]+adjust_text_y)
                ,fontsize = 10
                ,ha = 'center'
                ,va = 'bottom')
    plt.annotate(shap_x_label[1]
                ,xy = (plt.xlim()[1], -1)
                ,xytext = (plt.xlim()[1], plt.ylim()[0]+adjust_text_y)
                ,fontsize = 10
                ,ha = 'center'
                ,va = 'bottom')
    plt.savefig(fig_repo+'shap_beeswarm_'+fig_name+'.png', bbox_inches='tight', dpi=1500)#, dpi=300)#, dpi=2000)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return best model information    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    return({'shap_values': shap_values
            ,'feature_shap_values': X_validate.columns.tolist()
            ,'feature_importance': feature_importance
            })

