#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                      One-shot nonparametric approach                                                                      ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Comparing two correlated C indices with right-censored survival outcome
# Testing the difference between two correlated overall C indices
# One-shot nonparametric approach adapted from
# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4314453/
# https://cran.r-project.org/web/packages/compareC/compareC.pdf

import scipy.stats
import numpy as np
import math

def csign(X_i, Censor_i, X_j, Censor_j) :
    if X_i > X_j :
        if (Censor_i==1) & (Censor_j==1) :
            return(1)
        if (Censor_i==1) & (Censor_j==0) :
            return(0)
        if (Censor_i==0) & (Censor_j==1) :
            return(1)
        if (Censor_i==0) & (Censor_j==0) :
            return(0)
    
    if X_i < X_j :
        if (Censor_i==1) & (Censor_j==1) :
            return(-1)
        if (Censor_i==1) & (Censor_j==0) :
            return(-1)
        if (Censor_i==0) & (Censor_j==1) :
            return(0)
        if (Censor_i==0) & (Censor_j==0) :
            return(0)
    
    if X_i == X_j :
        if (Censor_i==1) & (Censor_j==1) :
            return(0)
        if (Censor_i==1) & (Censor_j==0) :
            return(-1)
        if (Censor_i==0) & (Censor_j==1) :
            return(1)
        if (Censor_i==0) & (Censor_j==0) :
            return(0)

def sign(score) :
    if score >= 0 :
        score1 = 1
    else :
        score1 = 0
    
    if score <= 0 :
        score2 = 1
    else :
        score2 = 0
    
    score_sign = score1 - score2
    return(score_sign)

def TauXX(timeX, statusX) :
    nobs = len(timeX)
    est_tXX = 0
    for i in range(nobs) :
        for j in range(nobs) :
            if j != i :
                est_tXX += csign(timeX[i],statusX[i],timeX[j],statusX[j])*csign(timeX[i],statusX[i],timeX[j],statusX[j])
            else :
                pass
    output = est_tXX/nobs/(nobs-1)
    return(output)

def TauXY(timeX, statusX, scoreY) :
    nobs = len(timeX)
    est_tXY = 0
    for i in range(nobs) :
        for j in range(nobs) :
            if j != i :
                est_tXY += csign(timeX[i],statusX[i],timeX[j],statusX[j])*sign(scoreY[i]-scoreY[j])
            else :
                pass
    output = est_tXY/nobs/(nobs-1)
    return(output)

def VarTauXX(timeX, statusX) :
    nobs = len(timeX)
    temp = 0
    temp_s1 = 0
    temp_s2 = 0
    temp_s3 = 0
    for i in range(nobs) :
        temp_s1_j = 0
        temp_s3_j = 0
        for j in range(nobs) :
            if j != i :
                temp = csign(timeX[i],statusX[i],timeX[j],statusX[j])*csign(timeX[i],statusX[i],timeX[j],statusX[j])
                temp_s1_j += temp
                temp_s3_j += temp*temp
            else :
                pass
        temp_s1 += 4*temp_s1_j*temp_s1_j
        temp_s3 += 2*temp_s3_j
        temp_s2 += temp_s1_j
    temp_s2 = temp_s2/nobs/(nobs-1)*temp_s2*(2*nobs-3)*(-2)
    output = (temp_s1-temp_s3+temp_s2)/nobs/(nobs-1)/(nobs-2)/(nobs-3)
    return(output)

def VarTauXY(timeX, statusX, scoreY) :
    nobs = len(timeX)
    temp = 0
    temp_s1 = 0
    temp_s2 = 0
    temp_s3 = 0
    for i in range(nobs) :
        temp_s1_j = 0
        temp_s3_j = 0
        for j in range(nobs) :
            if j != i :
                temp = csign(timeX[i],statusX[i],timeX[j],statusX[j])*sign(scoreY[i]-scoreY[j])
                temp_s1_j += temp
                temp_s3_j += temp*temp
            else :
                pass
        temp_s1 += 4*temp_s1_j*temp_s1_j
        temp_s3 += 2*temp_s3_j
        temp_s2 += temp_s1_j
    temp_s2 = temp_s2/nobs/(nobs-1)*temp_s2*(2*nobs-3)*(-2)
    output = (temp_s1-temp_s3+temp_s2)/nobs/(nobs-1)/(nobs-2)/(nobs-3)
    return(output)

def CovTauXXXY(timeX, statusX, scoreY) :
    nobs = len(timeX)
    temp_XX = 0
    temp_XY = 0
    temp_s1 = 0
    temp_s2 = 0
    temp_s3 = 0
    temp_s4 = 0
    for i in range(nobs) :
        temp_sXX_j = 0
        temp_sXY_j = 0
        temp_sXXXY_j = 0
        for j in range(nobs) :
            if j != i :
                temp_XX = csign(timeX[i],statusX[i],timeX[j],statusX[j])*csign(timeX[i],statusX[i],timeX[j],statusX[j])
                temp_XY = csign(timeX[i],statusX[i],timeX[j],statusX[j])*sign(scoreY[i]-scoreY[j])
                temp_sXX_j   += temp_XX
                temp_sXY_j   += temp_XY
                temp_sXXXY_j += temp_XX*temp_XY
            else :
                pass
        temp_s1 += 4*temp_sXX_j*temp_sXY_j
        temp_s3 += 2*temp_sXXXY_j
        temp_s2 += temp_sXX_j
        temp_s4 += temp_sXY_j
    output = (temp_s1-temp_s3+(2*nobs-3)*(-2)*temp_s2*temp_s4/nobs/(nobs-1))/nobs/(nobs-1)/(nobs-2)/(nobs-3)
    return(output)

def CovTauXYXZ(timeX, statusX, scoreY, scoreZ) :
    nobs = len(timeX)
    temp_XY = 0
    temp_XZ = 0
    temp_s1 = 0
    temp_s2 = 0
    temp_s3 = 0
    temp_s4 = 0
    for i in range(nobs) :
        temp_sXY_j = 0
        temp_sXZ_j = 0
        temp_sXYXZ_j = 0
        for j in range(nobs) :
            if j != i :
                temp_XY = csign(timeX[i],statusX[i],timeX[j],statusX[j])*sign(scoreY[i]-scoreY[j])
                temp_XZ = csign(timeX[i],statusX[i],timeX[j],statusX[j])*sign(scoreZ[i]-scoreZ[j])
                temp_sXY_j   += temp_XY
                temp_sXZ_j   += temp_XZ
                temp_sXYXZ_j += temp_XY*temp_XZ
            else :
                pass
        temp_s1 += 4*temp_sXY_j*temp_sXZ_j
        temp_s3 += 2*temp_sXYXZ_j
        temp_s2 += temp_sXY_j
        temp_s4 += temp_sXZ_j
    output = (temp_s1-temp_s3+(2*nobs-3)*(-2)*temp_s2*temp_s4/nobs/(nobs-1))/nobs/(nobs-1)/(nobs-2)/(nobs-3)
    return(output)

def estC(timeX, statusX, scoreY) :
    Tau_XX = TauXX(timeX, statusX)
    Tau_XY = TauXY(timeX, statusX, scoreY)
    output = (Tau_XY / Tau_XX + 1) / 2
    return(output)

def vardiffC(timeX, statusX, scoreY, scoreZ) :
    t11 = TauXX(timeX, statusX)
    t12 = TauXY(timeX, statusX, scoreY)
    t13 = TauXY(timeX, statusX, scoreZ)
    var_t11 = VarTauXX(timeX, statusX)
    var_t12 = VarTauXY(timeX, statusX, scoreY)
    var_t13 = VarTauXY(timeX, statusX, scoreZ)
    cov_t1112 = CovTauXXXY(timeX, statusX, scoreY)
    cov_t1113 = CovTauXXXY(timeX, statusX, scoreZ)
    cov_t1213 = CovTauXYXZ(timeX, statusX, scoreY, scoreZ)

    m1 = [1/t11, -t12/t11**2]
    m2 = [[var_t12, cov_t1112], [cov_t1112, var_t11]]
    m3 = [1/t11, -t12/t11**2]
    est_varCxy = 1/4 * np.matmul( np.matmul(m1, m2), m3 )

    m4 = [1/t11, -t13/t11**2]
    m5 = [[var_t13, cov_t1113], [cov_t1113, var_t11]]
    m6 = [1/t11, -t13/t11**2]
    est_varCxz = 1/4 * np.matmul( np.matmul(m4, m5), m6 )

    m7 = [1/t11, -t12/t11**2]
    m8 = [[cov_t1213, cov_t1112], [cov_t1113, var_t11]]
    m9 = [1/t11, -t13/t11**2]
    est_cov = 1/4 * np.matmul( np.matmul(m7, m8), m9 )

    est_vardiff_c = est_varCxy + est_varCxz - 2 * est_cov

    return({'est_vardiff_c' : est_vardiff_c
            ,'est_varCxy' : est_varCxy
            ,'est_varCxz' : est_varCxz
            ,'est_cov' : est_cov })

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - compareC    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# This is a function used to test if the difference in two correlated overall C indices is statistically significant

def compareC(timeX, statusX, scoreY, scoreZ) :
    """
    Parameters
    ----------
    timeX :
        The vector of actual survival time X, one survival time for each observation
    statusX :
        The matching vector of event indicator for time X, 1 if occurred and 0 otherwise
    scoreY :
        The vector of the first measured biomarker or score Y, one for each of the same observations
    scoreZ :
        The vector of the second measured biomarker or score Z, one for each of the same observations
    
    ----------
    Return
    ----------
    est_c :
        The estimated two C indices
    est_diff_c :
        The estimated difference of the two C indices, i.e., C_XY - C_XZ
    est_vardiff_c :
        The estimated variance of the difference of two C indices
    est_varCxy :
        The estimated variance of the C index for score Y
    est_varCxz :
        The estimated variance of the C index for score Z
    est_cov :
        The estimated covariance between the two C indices for score Y and that for score Z
    zscore :
        Z score (test statistic) for hypothesis testing
    pval :
        P value for te comparison of two C indices
    """

    est_c = [estC(timeX, statusX, scoreY), estC(timeX, statusX, scoreZ)]
    est_diff_c = est_c[0] - est_c[1]
    tmpout = vardiffC(timeX, statusX, scoreY, scoreZ)
    if est_diff_c == 0 :
        zscore = 0
    else :
        zscore = est_diff_c / math.sqrt( abs(tmpout['est_vardiff_c']) )
    # pval = (1.0 - scipy.stats.norm(0, 1).cdf( abs(zscore) ))
    pval = scipy.stats.norm.sf(abs(zscore))*2   # p-value type: two-tails

    return({'est_c' : est_c                                 # The estimated two C indices
            ,'est_diff_c' : est_diff_c                      # The estimated difference of the two C indices, i.e., C_XY - C_XZ
            ,'est_vardiff_c' : tmpout['est_vardiff_c']      # The estimated variance of the difference of two C indices
            ,'est_varCxy' : tmpout['est_varCxy']            # The estimated variance of the C index for score Y
            ,'est_varCxz' : tmpout['est_varCxz']            # The estimated variance of the C index for score Z
            ,'est_cov' : tmpout['est_cov']                  # The estimated covariance between the two C indices for score Y and that for score Z
            ,'zscore' : zscore                              # Z score (test statistic) for hypothesis testing
            ,'pval' : pval                                  # P value for te comparison of two C indices
            })




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                                            To generate results of one-shot nonparametric approach for comparing C indices                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def one_shot_nonparametric_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    model_survival_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_survival_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :
            print('loop_ml_prefix : '+ loop_ml_prefix)
            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            
            model_survival = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_survival[ ls_ml_models[ml_loop] ] = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            
            model_survival_load[loop_ml_prefix] = model_survival
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_survival_comb[loop_file_repo] = model_survival_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    One-shot nonparametric approach of all optimal model in the same cohort   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ds_test_cohort_comb = pd.DataFrame()
    ds_subset_cohort_comb = pd.DataFrame()
    ds_cohort_comb = pd.DataFrame()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        best_ml_cohort = best_ml_model_comb[loop_file_repo]

        ls_cohort_proba = []
        ls_cohort_y = []
        for loop_ml_prefix in ml_prefix :
            
            #### Actual and predicted outcome of the optimal model in each cohort and the model with different set of features ----------
            ls_test_cohort_proba = []
            ls_test_cohort_y = []
            ls_c_index = []
            for ml_loop in range(len(ls_ml_models)) :
                ls_test_cohort_proba.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].best_model_predictions )
                ls_test_cohort_y.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].y_validate.tolist() )
                ls_c_index.append( best_ml_cohort[loop_ml_prefix][ ls_ml_models[ml_loop] ].best_model_cIndex_test )

            # Actual and predicted outcome of the optimal model in each cohort (exclude the trivial model)
            ls_c_index2 = ls_c_index.copy()
            ls_cohort_proba.append( ls_test_cohort_proba[ls_c_index2.index(max(ls_c_index2))] )
            ls_cohort_y.append( ls_test_cohort_y[ls_c_index2.index(max(ls_c_index2))] )
        
        #### Actual and predicted outcome of the optimal model with different set of features ----------
        ds_cohort = pd.DataFrame({'Cohort' : loop_file_repo, 'Prefix' : ml_prefix})
        for loop_ii in range(len(ls_cohort_proba)) :
            list_pvalue = []
            for loop_jj in range(len(ls_cohort_proba)) :
                pvalue = compareC(timeX = np.array((ls_cohort_y[loop_ii]))[:,1]
                                  ,statusX = np.array((ls_cohort_y[loop_ii]))[:,0]
                                  ,scoreY = np.array(ls_cohort_proba[loop_ii])
                                  ,scoreZ = np.array(ls_cohort_proba[loop_jj]) )['pval']
                list_pvalue.append( pvalue )
            
            df_cohort = pd.DataFrame({'Prefix' : ml_prefix, ml_prefix[loop_ii] : list_pvalue })
            ds_cohort = pd.merge(ds_cohort, df_cohort, how = 'left', on = ['Prefix'])
        
        # Data frame of the one-shot nonparametric approach test (by cohort, prefix)
        ds_cohort_comb = pd.concat([ds_cohort_comb, ds_cohort], axis = 0)
    
    ds_cohort_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Cohort'+'.csv')
    ds_cohort_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Cohort'+'.html')









#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                              To generate results of One-shot norparametric test for comparing C-index score (Normalized vs Unnormalized)                                   ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def normalized_one_shot_nonparametric_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    p_value = dict()
    norm_method = dict()
    for loop_file_repo in file_repo_prefix_multi :
        print('loop_file_repo : '+ loop_file_repo)

        p_value[loop_file_repo] = dict()
        norm_method[loop_file_repo] = dict()
        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_normalized_ml_model = []
            best_unnormalized_ml_model = []
            best_normalized_ml_model_score = []
            best_unnormalized_ml_model_score = []
            for ml_loop in range(len(ls_ml_models)) :
                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix
                ml_classifier = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                normalized_ml_score = []
                unnormalized_ml_score = []
                normalized_ml_model = []
                unnormalized_ml_model = []
                for ls_norm in range(len(ml_classifier)) :
                    ml_keys = list(ml_classifier[ls_norm].keys())[0]
                    if 'none' in ml_keys :
                        unnormalized_ml_score.append( ml_classifier[ls_norm][ml_keys].best_model_cIndex_test )
                        unnormalized_ml_model.append( ml_classifier[ls_norm][ml_keys] )
                    else :
                        normalized_ml_score.append( ml_classifier[ls_norm][ml_keys].best_model_cIndex_test )
                        normalized_ml_model.append( ml_classifier[ls_norm][ml_keys] )
                
                # Highest average score in CV
                best_normalized_ml_model.append( normalized_ml_model[ normalized_ml_score.index(max(normalized_ml_score)) ] )
                best_normalized_ml_model_score.append( max(normalized_ml_score) )
                best_unnormalized_ml_model.append( unnormalized_ml_model[ unnormalized_ml_score.index(max(unnormalized_ml_score)) ] )
                best_unnormalized_ml_model_score.append( max(unnormalized_ml_score) )

            best_normalized_model = best_normalized_ml_model[ best_normalized_ml_model_score.index(max(best_normalized_ml_model_score)) ]
            best_unnormalized_model = best_unnormalized_ml_model[ best_unnormalized_ml_model_score.index(max(best_unnormalized_ml_model_score)) ]

            # One-shot nonparametric test
            pvalue_ = compareC(timeX = np.array(best_unnormalized_model.time_validate)
                              ,statusX = np.array(best_unnormalized_model.status_validate)
                              ,scoreY = np.array(best_unnormalized_model.best_model_predictions)
                              ,scoreZ = np.array(best_normalized_model.best_model_predictions) )['pval']

            
            p_value[loop_file_repo][loop_ml_prefix] = pvalue_
            norm_method[loop_file_repo][loop_ml_prefix] = best_normalized_model.normalize_method
    
    # Table of DeLong test for each cohort, normalized vs. unnormalized
    col_names = []
    for x in ml_prefix :
        col_names.append( 'norm_method'+x )
        col_names.append(  'p_value'+x )

    df = pd.DataFrame(columns=['Cohort'] + col_names)
    for cohort, values in p_value.items():
        df = df.append({
            'Cohort': cohort.strip('/'),
            col_names[0]: norm_method[cohort][ml_prefix[0]],
            col_names[1]: values[ml_prefix[0]],
            col_names[2]: norm_method[cohort][ml_prefix[1]],
            col_names[3]: values[ml_prefix[1]]
            }, ignore_index=True)
        
    df.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Unnormalized_vc_Normalized'+'.csv')
    df.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Unnormalized_vc_Normalized'+'.html')






#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#####                      To generate results of One-shot nonparametric test for comparing C-index (All Normalized vs Unnormalized) eith Benjamini-Hochberg                     ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def all_normalized_one_shot_nonparametric_test() :

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    pd_norm_pval_comb = pd.DataFrame()

    p_value = dict()
    norm_method = dict()
    for loop_file_repo in file_repo_prefix_multi :
        print('loop_file_repo : '+ loop_file_repo)

        p_value[loop_file_repo] = dict()
        norm_method[loop_file_repo] = dict()
        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)


            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_normalized_model = dict()
            best_normalized_model_score = dict()

            # none, combat, mcombat, mean, quantile, ratioa, standardize
            list_normalized_ml = ['none', 'combat', 'mcombat', 'mean', 'quantile', 'ratioa', 'standardize']
            normalized_ml_score = dict()
            normalized_ml_model = dict()
            for ls_normalized_ml in list_normalized_ml :
                normalized_ml_score[ls_normalized_ml] = []
                normalized_ml_model[ls_normalized_ml] = []

            for ml_loop in range(len(ls_ml_models)) :
                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix
                ml_classifier = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                for ls_norm in range(len(ml_classifier)) :
                    ml_keys = list(ml_classifier[ls_norm].keys())[0]
                    # To extract the optimal model and AUC score for each normalization method
                    for ls_normalized_ml in list_normalized_ml :
                        if ls_normalized_ml in ml_keys:
                            normalized_ml_score[ls_normalized_ml].append( ml_classifier[ls_norm][ml_keys].best_model_cIndex_test )
                            normalized_ml_model[ls_normalized_ml].append( ml_classifier[ls_norm][ml_keys] )
                
            # Highest average score in CV
            for ls_normalized_ml in list_normalized_ml :
                idx_max_cv_auc = normalized_ml_score[ls_normalized_ml].index( max(normalized_ml_score[ls_normalized_ml]) )
                best_normalized_model[ls_normalized_ml] = normalized_ml_model[ls_normalized_ml][idx_max_cv_auc]
                best_normalized_model_score[ls_normalized_ml] = normalized_ml_score[ls_normalized_ml][idx_max_cv_auc]

            # DeLong test
            norm_mtd_1_comb = []
            norm_mtd_2_comb = []
            p_value_comb = []

            score_mtd_1 = []
            score_mtd_2 = []
            sd_mtd_1 = []
            sd_mtd_2 = []
            out_score_mtd_1 = []
            out_score_mtd_2 = []

            for norm_mtd_1 in ['none'] :
                for norm_mtd_2 in list_normalized_ml :
                    if list_normalized_ml.index(norm_mtd_1) < list_normalized_ml.index(norm_mtd_2) :
                        norm_mtd_1_comb.append( norm_mtd_1 )
                        norm_mtd_2_comb.append( norm_mtd_2 )
                        # One-shot nonparametric test
                        pvalue_ = compareC(timeX = np.array(best_normalized_model[norm_mtd_1].time_validate)
                                        ,statusX = np.array(best_normalized_model[norm_mtd_1].status_validate)
                                        ,scoreY = np.array(best_normalized_model[norm_mtd_1].best_model_predictions)
                                        ,scoreZ = np.array(best_normalized_model[norm_mtd_2].best_model_predictions) )['pval']
                        p_value_comb.append( pvalue_ )

                        # Cross-validated scores
                        score_mtd_1.append( best_normalized_model[norm_mtd_1].best_model_cIndex_test )
                        score_mtd_2.append( best_normalized_model[norm_mtd_2].best_model_cIndex_test )
                        # Standard deviation of the cross-validated scores
                        sd_mtd_1.append( statistics.stdev( best_normalized_model[norm_mtd_1].all_model_cIndex_test ) )
                        sd_mtd_2.append( statistics.stdev( best_normalized_model[norm_mtd_2].all_model_cIndex_test ) )
                        # Hold-out scores
                        out_score_mtd_1.append( best_normalized_model[norm_mtd_1].best_model_cIndex_val )
                        out_score_mtd_2.append( best_normalized_model[norm_mtd_2].best_model_cIndex_val )
                    else :
                        pass

            # Correct for multiple hypothesis testing using Benjamini-Hochberg method
            rejected, corrected_p_values, _, _ = multipletests(p_value_comb, method='fdr_bh')
            
            # Normalization method, p-values and corrected p-values into DataFrame
            pd_norm_pval = pd.DataFrame({'Model': loop_ml_prefix
                                         ,'Cohort' : loop_file_repo.replace('/', '')
                                         ,'Method 1' : [x.upper() for x in norm_mtd_1_comb]
                                         ,'Method 2' : [x.upper() for x in norm_mtd_2_comb]
                                         ,'p-values' : p_value_comb
                                         ,'Corrected p-values' : corrected_p_values
                                         ,'CV C-index (Method 1)' : score_mtd_1
                                         ,'CV C-index (Method 2)' : score_mtd_2
                                         ,'SD C-index (Method 1)' : sd_mtd_1
                                         ,'SD C-index (Method 2)' : sd_mtd_2
                                         ,'Hold-out C-index (Method 1)' : out_score_mtd_1
                                         ,'Hold-out C-index (Method 2)' : out_score_mtd_2
                                         })
            
            # Table of DeLong test for each cohort, normalized vs. unnormalized
            pd_norm_pval_comb = pd.concat([pd_norm_pval_comb, pd_norm_pval], axis = 0)
        
    pd_norm_pval_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Unnormalized_vc_All_Normalized'+'.csv')
    pd_norm_pval_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'One_Shot_Nonparametric_C_Indices_Comparison_by_Unnormalized_vc_All_Normalized'+'.html')








#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                            To generate C indices of unbiased and reduced model in cross-validation                                                           #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To find the model with highest average performance metric based on selected methods    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_find_best_ml_model(model, keyword, excl_keyword = None) :

    # List of methods
    list_keyword = [list(model[x].keys())[0] for x in range(len(model))]
    if excl_keyword == None :
        list_keyword_select = [list_keyword[x] for x in range(len(list_keyword)) if (keyword in list_keyword[x])]
        list_keyword_index = [x for x in range(len(list_keyword)) if (keyword in list_keyword[x])]
    else:
        list_keyword_select = list_keyword
        for keyi in range(len(excl_keyword)) :
            list_keyword_select = [list_keyword_select[x] for x in range(len(list_keyword_select)) if (keyword in list_keyword_select[x]) & (excl_keyword[keyi] not in list_keyword_select[x]) ]
        
        list_keyword_index = [list_keyword.index(x) for x in list_keyword_select if x in list_keyword]
    
    # List of performance metrics for each methods
    list_score_cv = [model[x][list(model[x].keys())[0]].all_model_cIndex_cv[model[x][list(model[x].keys())[0]].max_features.index( model[x][list(model[x].keys())[0]].best_model_max_features )]
                     for x in list_keyword_index ]
    
    list_avg_score_cv = [max(model[x][list(model[x].keys())[0]].all_model_cIndex_test) for x in list_keyword_index ]

    list_avg_score_index = list_avg_score_cv.index( max(list_avg_score_cv) )
    list_keyword_select_index = list_keyword_select[list_avg_score_index]
    list_model_index = list_keyword_index[list_avg_score_index]
    
    return({'best_model' : model[list_model_index][list_keyword_select_index]
            ,'score_cv' : list_score_cv[list_avg_score_index]
            })

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - To plot the boxplot of performance metric of the optimal models by cohort    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def unbiased_reduced_cindex():

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the model from each cohort, model, and algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    model_unnormalized_comb = dict()
    model_normalized_comb = dict()
    # model_normalized_addon_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        best_unnormalized_load = dict()
        best_normalized_load = dict()
        # best_normalized_addon_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :
            print('loop_ml_prefix : '+ loop_ml_prefix)
            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            best_unnormalized_ml_model = dict()
            best_normalized_ml_model = dict()
            # best_normalized_addon_ml_model = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                loaded_model = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                loaded_best_model = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
                    best_unnormalized_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = 'none', excl_keyword = None)
                    best_normalized_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = '', excl_keyword = ['none', 'addon'])
                    # best_normalized_addon_ml_model[ ls_ml_models[ml_loop] ] = func_find_best_ml_model(model = loaded_model, keyword = 'addon', excl_keyword = ['none'])
                else :
                    pass
                
                best_ml_model[ ls_ml_models[ml_loop] ] = dict({'best_model' : loaded_best_model
                                                               ,'score_cv' : loaded_best_model.all_model_cIndex_cv[ loaded_best_model.max_features.index( loaded_best_model.best_model_max_features ) ] })
                
                
            
            if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
                mean_unnormalized = [ mean(best_unnormalized_ml_model[x]['score_cv']) for x in list(best_unnormalized_ml_model.keys()) ]
                mean_normalized = [ mean(best_normalized_ml_model[x]['score_cv']) for x in list(best_normalized_ml_model.keys()) ]
                # mean_normalized_addon = [ mean(best_normalized_addon_ml_model[x]['score_cv']) for x in list(best_normalized_addon_ml_model.keys()) ]

                mean_unnormalized_index = mean_unnormalized.index( max(mean_unnormalized) )
                mean_normalized_index = mean_normalized.index( max(mean_normalized) )
                # mean_normalized_addon_index = mean_normalized_addon.index( max(mean_normalized_addon) )

                best_unnormalized_load[loop_ml_prefix] = best_unnormalized_ml_model[ list(best_unnormalized_ml_model.keys())[mean_unnormalized_index] ]
                best_normalized_load[loop_ml_prefix] = best_normalized_ml_model[ list(best_normalized_ml_model.keys())[mean_normalized_index] ]
                # best_normalized_addon_load[loop_ml_prefix] = best_normalized_addon_ml_model[ list(best_normalized_addon_ml_model.keys())[mean_normalized_addon_index] ]
            else :
                pass
            
            mean_ml_model = [ mean(best_ml_model[x]['score_cv']) for x in list(best_ml_model.keys()) ]
            best_ml_model_load[loop_ml_prefix] = best_ml_model[ list(best_ml_model.keys())[mean_ml_model.index( max(mean_ml_model) )] ]
        
        if loop_file_repo not in ['LUXPARK/', 'PPMI/', 'ICEBERG/'] :
            model_unnormalized_comb[loop_file_repo] = best_unnormalized_load
            model_normalized_comb[loop_file_repo] = best_normalized_load
            # model_normalized_addon_comb[loop_file_repo] = best_normalized_addon_load
        else :
            pass

        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Boxplot of unbiased vs reduced of performance metric in cross-validation for each cohort    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def define_box_properties(plot_name, color_code, label):
        for k, v in plot_name.items():
            plt.setp(plot_name.get(k), color=color_code)
            
        # use plot function to draw a small line to name the legend.
        plt.plot([], c=color_code, label=label)
        plt.legend()

    list_cohort_inverse = [ file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] for x in range(len(file_repo_prefix_loop)) ]
    list_cohort_name_inverse = [ file_repo_prefix_name[ len(file_repo_prefix_name)-x-1 ] for x in range(len(file_repo_prefix_name)) ]

    for loop_ml_prefix in ml_prefix :
        if loop_ml_prefix == ml_prefix[0] :
            unbiased = [ best_ml_model_comb[x][loop_ml_prefix]['score_cv'] for x in list_cohort_inverse ]
        
        else :
            reduced = [ best_ml_model_comb[x][loop_ml_prefix]['score_cv'] for x in list_cohort_inverse ]

            fig = plt.figure(figsize=(12, 5), linewidth = 0.5)
            plt.style.context("seaborn-white")
            plt.axes().set_facecolor('#F8F8F7')

            unbiased_plot = plt.boxplot(unbiased, vert = False, positions = np.array(np.arange(len(unbiased)))*2.0+0.35
                                        ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})
            reduced_plot = plt.boxplot(reduced, vert = False, positions = np.array(np.arange(len(reduced)))*2.0-0.35
                                       ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})

            # setting colors for each groups
            define_box_properties(unbiased_plot, '#D7191C', ml_prefix_label[0])
            define_box_properties(reduced_plot, '#2C7BB6', ml_prefix_label[ml_prefix.index(loop_ml_prefix)])

            # Set the y label values
            plt.yticks(np.arange(0, len(list_cohort_name_inverse) * 2, 2), list_cohort_name_inverse)

            # set the limit for x axis
            plt.ylim(-2, len(list_cohort_name_inverse)*2)

            # set the limit for x axis
            ls_values = []
            for xx in unbiased :
                ls_values += xx
            for xx in reduced :
                ls_values += xx

            min_xlim = round(min(ls_values), 1)-0.1
            max_xlim = min([round(max(ls_values))+0.1, 1.0])
            plt.xlim(min_xlim, max_xlim)

            fig.savefig(file_repo+'CROSS_COHORT/'+set_outcome+'/Performance_Metric/Unbiased_vs_Reduced'+loop_ml_prefix+'.png', bbox_inches='tight', dpi=300)

            # set the title
            # plt.title('Grouped boxplot using matplotlib')
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Boxplot of unnormalized, normalized, and addon normalized of performance metric in cross-validation for each cohort    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    list_norm_inverse  = [ file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] for x in range(len(file_repo_prefix_loop))
                          if file_repo_prefix_loop[ len(file_repo_prefix_loop)-x-1 ] in list(model_unnormalized_comb.keys()) ]
    list_norm_name_inverse = [file_repo_prefix_name[ file_repo_prefix_loop.index(x) ] for x in list_norm_inverse]

    for loop_ml_prefix in ml_prefix :
        unnormalized = [ model_unnormalized_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]
        normalized = [ model_normalized_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]
        # normalized_addon = [ model_normalized_addon_comb[x][loop_ml_prefix]['score_cv'] for x in list_norm_inverse ]

        fig_norm = plt.figure(figsize=(12, 5), linewidth = 0.5)
        plt.style.context("seaborn-white")
        plt.axes().set_facecolor('#F8F8F7')

        unnormalized_plot = plt.boxplot(unnormalized, vert = False, positions = np.array(np.arange(len(unnormalized)))*3.0+0.7
                                        ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})
        normalized_plot = plt.boxplot(normalized, vert = False, positions = np.array(np.arange(len(normalized)))*3.0-0
                                      ,widths = 0.6, showmeans=True, meanline = True, meanprops={"linewidth": 0.7})
        
        # setting colors for each groups
        define_box_properties(unnormalized_plot, '#D7191C', 'Unnormalized')
        define_box_properties(normalized_plot, '#2C7BB6', 'Normalized')
        
        # Set the y label values
        plt.yticks(np.arange(0, len(list_norm_name_inverse) * 3, 3), list_norm_name_inverse)

        # set the limit for x axis
        plt.ylim(-3, len(list_norm_name_inverse)*3)

        # set the limit for x axis
        ls_norm_values = []
        for xx in unnormalized :
            ls_norm_values += xx
        for xx in normalized :
            ls_norm_values += xx
        # for xx in normalized_addon :
        #     ls_norm_values += xx

        min_norm_xlim = round(min(ls_norm_values), 1)-0.1
        max_norm_xlim = min([round(max(ls_norm_values))+0.1, 1.0])
        plt.xlim(min_norm_xlim, max_norm_xlim)

        fig_norm.savefig(file_repo+'CROSS_COHORT/'+set_outcome+'/Performance_Metric/Unnormalized_vs_Normalized'+loop_ml_prefix+'.png', bbox_inches='tight', dpi=300)
