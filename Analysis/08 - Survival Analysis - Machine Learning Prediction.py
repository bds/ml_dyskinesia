#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Categorical variables to be encoded    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_category = data_variables[data_variables['Variable Type'].isin(['Ordinal', 'Nominal'])]['PPMI Variable']
vars_category = [ x for x in vars_category if x in data_info[data_info['Type'] == 'Baseline']['Variable'].tolist() ]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#### Hyperparameter tuning    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Set the max_features
ls_mtd_max_features_ = [1, 2, 3, 4, 5, 10]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To perform the model prediction    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Function - Survival Analysis
def func_ml_model_survival(i) :
    
    mtd_name = ls_mtd_name[i]
    print('Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name)

    if mtd_name != 'stability' :
        B_loop_set = 1
        pickle_prefix_set = ""
        ls_mtd_max_features = ls_mtd_max_features_
    else :
        B_loop_set = B_loop_set_
        pickle_prefix_set = pickle_prefix_set_
        ls_mtd_max_features = ls_mtd_max_features_cv

    model_survival = dict()

    time_start = datetime.now()
    surv_model = ml_survival_cox(data_repo = file_data+file_repo_prefix+set_outcome[0]+'/'+ls_mtd_normalized[i]+'/'
                                ,var_time = set_outcome[0]
                                ,var_status = set_outcome[1]
                                ,vars_ml = vars_ml_set
                                ,BLoop = B_loop_set
                                ,normalize_method = ls_mtd_normalized[i]
                                ,feature_selection = ls_mtd_feat_selection[i]
                                ,addon_Xtest = ls_mtd_addon[i]
                                ,undersampling = ls_mtd_undersamping[i]
                                ,surv_model_set = model_set
                                ,vars_encoding = vars_category
                                ,max_features = ls_mtd_max_features
                                ,pickle_prefix = pickle_prefix_set )
    
    surv_model.measure()
    model_survival[mtd_name] = surv_model

    # save_object(data = model_survival, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/temp/' )
    time_end = datetime.now()

    print('[Done] Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name + " Total running time: " + str(time_end - time_start))
    print('[Done] Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name + " Current time: " + str(time_end))

    return(model_survival)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To train the models    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# To perform risk prediction in survival analysis
time_start = datetime.now()
print('SURVIVAL ANALYSIS')

mtd_loop = [x for x in range(len(ls_mtd_name))]

# mtd_loop = []
# mtd_loop = mtd_loop[26:]   #####

if len(mtd_loop) > 0 :
    n_jobs_risk = min([n_jobs, len(mtd_loop)])
    model_risk = Parallel(n_jobs = n_jobs_risk)(
        delayed( func_ml_model_survival )(i = i)
        for i in mtd_loop )
    
    # To save the individual separately
    for idx in range(len(model_risk)) :
        mtd_name = list(model_risk[idx].keys())[0]
        save_model = model_risk[idx][mtd_name]
        save_object(data = save_model, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/temp/' )
    del model_risk
else :
    pass

time_end = datetime.now()
print("Total running time: " + str(time_end - time_start))



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Combining models    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

model_risk = []
# To combine the models
for mtd_name in ls_mtd_name :
    ml_risk = dict()
    print('Save Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name)
    ml_model_surv = load_object(filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/temp/')
    ml_risk[mtd_name] = ml_model_surv
    model_risk.append( ml_risk )

save_object(data = model_risk, filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )
model_risk = load_object(filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )
