#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Concordance index of the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_surv_models_comparision() :

    model_surv_comb = dict()
    best_surv_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_surv_load = dict()
        best_surv_model_load = dict()

        for loop_ml_prefix in ml_prefix :
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_surv = dict()
            best_surv_model = dict()

            for ml_loop in range(len(ls_ml_models)) :
                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix
                
                model_surv[ ls_ml_models[ml_loop] ] = load_object(filename = 'model_'+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/')
                best_surv_model[ ls_ml_models[ml_loop] ] = load_object(filename = 'ml_model_'+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/')
            
            model_surv_load[loop_ml_prefix] = model_surv
            best_surv_model_load[loop_ml_prefix] = best_surv_model
        
        model_surv_comb[loop_file_repo] = model_surv_load
        best_surv_model_comb[loop_file_repo] = best_surv_model_load

    return({'model_surv' : model_surv_comb
            ,'best_surv_model' : best_surv_model_comb})


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - C-index of the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def model_c_index(model
                 ,best_model
                 ,model_prefix
                 ,model_name
                 ,model_colors
                 ,fig_repo
                 ,fig_prefix ) :
    """
    Parameters:
    -----------
    model : dict
        model
    best_model : dict
        best_model
    model_prefix : list
        prefix of the model
    model_name : list
        name of the model
    model_colors : list
        colours of the model
    fig_repo : str
        file repository of the models
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    C-index of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Boxplot of the C-index of the best model in each machine learning algorithm
    ds_box = pd.DataFrame()
    for j in range(len( list(best_model.keys()) )) :
        ml_prefix = list( best_model.keys() )[j]
        ml_name = model_name[ model_prefix.index(ml_prefix) ]
        box_auc_test = best_model[ml_prefix].best_model_cIndex_test
        box_auc_val = best_model[ml_prefix].best_model_cIndex_val

        df_box = pd.DataFrame({'Algorithm': [ml_name] * 2
                              ,'Validation': ['Cross-validation', 'Hold-out validation']
                              ,'C-index': [box_auc_test, box_auc_val]
                              })
        ds_box = pd.concat([ds_box, df_box], axis = 0)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Bar chart of c-index for each algorithm    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def show_values(axs, orient="v", space=.01):
        def _single(ax):
            if orient == "v":
                for p in ax.patches:
                    _x = p.get_x() + p.get_width() / 2
                    _y = p.get_y() + p.get_height() + (p.get_height()*0.01)
                    value = '{:.2f}%'.format(p.get_height())
                    ax.text(_x, _y, value, ha="center") 
            elif orient == "h":
                for p in ax.patches:
                    _x = p.get_x() + p.get_width() + float(space)
                    _y = p.get_y() + p.get_height() - (p.get_height()*0.2)
                    value = '{:.2f}%'.format(p.get_width())
                    ax.text(_x, _y, value, ha="left")

        if isinstance(axs, np.ndarray):
            for idx, ax in np.ndenumerate(axs):
                _single(ax)
        else:
            _single(axs)
    
    ds_bar_pct = ds_box.copy()
    ds_bar_pct['C-index'] = ds_bar_pct['C-index'] * 100
    ds_bar_pct.sort_values(['Validation', 'C-index'], ascending = [True, False], inplace = True)

    sns.set_style("white")
    fig, ax = plt.subplots()
    bar_auc = sns.barplot(x = 'C-index', y = 'Algorithm', hue = 'Validation'
                ,data = ds_bar_pct
                ,palette = 'Blues', edgecolor = 'w', ax = ax)
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    ax.set_xlim(0, 110)

    show_values(bar_auc, "h", space=0.01)

    # Save the bar chart of the AUC score
    plt.savefig(fig_repo+'bar_c_index_best_model'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Line chart of C-index for each undersampling    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Boxplot of the AUC score of the best model in each machine learning algorithm
    ds_feature_selection = pd.DataFrame()
    ds_undersampling = pd.DataFrame()
    ds_fs_undersamp = pd.DataFrame()

    for j in range(len( list(model.keys()) )) :
        ml_prefix_all = list( model.keys() )[j]
        ml_name_all = model_name[ model_prefix.index(ml_prefix_all) ]

        list_none = []
        list_rfe = []
        list_forward = []
        list_weight = []
        list_boruta = []
        list_stepwise = []
        list_undersampling = []
        list_no_undersampling = []

        list_fs_undersamp = []
        list_fs = []
        list_undersamp = []

        for k in range(len( model[ml_prefix_all] )) :
            ml_model = model[ml_prefix_all][k]
            ml_model_key = list(ml_model.keys())[0]
            ml_model_all = ml_model[ ml_model_key ]
            
            ml_model_c_index_test = ml_model_all.all_model_cIndex_cv[ ml_model_all.all_model_cIndex_test.index( ml_model_all.best_model_cIndex_test ) ]

            list_val_test = ['Cross-validation']*len(ml_model_c_index_test)

            if ml_model_key == 'none' :
                list_none = list_none + ml_model_c_index_test
            elif 'rfe' in ml_model_key :
                list_rfe = list_rfe + ml_model_c_index_test
            elif 'forward' in ml_model_key :
                list_forward = list_forward + ml_model_c_index_test
            elif 'weight' in ml_model_key :
                list_weight = list_weight + ml_model_c_index_test
            elif 'boruta' in ml_model_key :
                list_boruta = list_boruta + ml_model_c_index_test
            elif 'stepwise' in ml_model_key :
                list_stepwise = list_stepwise + ml_model_c_index_test
            else :
                pass

            if 'undersampling' in ml_model_key :
                list_undersampling = list_undersampling + ml_model_c_index_test
            else :
                list_no_undersampling = list_no_undersampling + ml_model_c_index_test
            
            if ('undersampling' in ml_model_key) & ( ('rfe' in ml_model_key) | ('forward' in ml_model_key) | ('weight' in ml_model_key) | ('boruta' in ml_model_key) | ('stepwise' in ml_model_key) ) :
                list_fs_undersamp = list_fs_undersamp + ml_model_c_index_test
            elif 'undersampling' in ml_model_key :
                list_undersamp = list_undersamp + ml_model_c_index_test
            elif ('rfe' in ml_model_key) | ('forward' in ml_model_key) | ('weight' in ml_model_key) | ('boruta' in ml_model_key) | ('stepwise' in ml_model_key) :
                list_fs = list_fs + ml_model_c_index_test

        fs = ['No Feature Selection']*len(list_none) + ['Recursive Feature Elimination']*len(list_rfe) + ['Forward Selection']*len(list_forward) +\
             ['Importance Weights']*len(list_weight) + ['Boruta SHAP']*len(list_boruta) + ['Stepwise Feature Selection']*len(list_stepwise)
        score = list_none + list_rfe + list_forward + list_weight + list_boruta + list_stepwise
        group = list_val_test*int(len(list_none)/len(list_val_test)) +\
                list_val_test*int(len(list_rfe)/len(list_val_test)) + list_val_test*int(len(list_forward)/len(list_val_test)) +\
                list_val_test*int(len(list_weight)/len(list_val_test)) + list_val_test*int(len(list_boruta)/len(list_val_test)) +\
                list_val_test*int(len(list_stepwise)/len(list_val_test))
        fs_algm = [ml_name_all]*len(score)

        undersamp = ['No Undersampling']*len(list_no_undersampling) + ['Undersampling']*len(list_undersampling)
        score_undersamp = list_no_undersampling + list_undersampling
        group_undersamp = list_val_test*int(len(list_no_undersampling)/len(list_val_test)) + list_val_test*int(len(list_undersampling)/len(list_val_test))
        undersamp_algm = [ml_name_all]*len(score_undersamp)

        fs_undersamp = ['None']*len(list_none) + ['Feature Selection + Undersampling']*len(list_fs_undersamp) +\
                        ['Feature Selection']*len(list_fs) + ['Undersampling']*len(list_undersamp)
        score_fs_undersamp = list_none + list_fs_undersamp + list_fs + list_undersamp
        group_fs_undersamp = list_val_test*int(len(list_none)/len(list_val_test)) +\
                             list_val_test*int(len(list_fs_undersamp)/len(list_val_test)) +\
                             list_val_test*int(len(list_fs)/len(list_val_test)) +\
                             list_val_test*int(len(list_undersamp)/len(list_val_test))
        fs_undersamp_algm = [ml_name_all]*len(score_fs_undersamp)

        df_feature_selection = pd.DataFrame({'Feature Selection': fs, 'Algorithm': fs_algm, 'Cross-validation C-index': score, 'Validation': group})
        df_undersampling = pd.DataFrame({'Undersampling': undersamp, 'Algorithm': undersamp_algm, 'Cross-validation C-index': score_undersamp, 'Validation': group_undersamp})
        df_fs_undersamp = pd.DataFrame({'Method': fs_undersamp, 'Algorithm': fs_undersamp_algm, 'Cross-validation C-index': score_fs_undersamp, 'Validation': group_fs_undersamp})

        ds_feature_selection = pd.concat([ds_feature_selection, df_feature_selection], axis = 0)
        ds_undersampling = pd.concat([ds_undersampling, df_undersampling], axis = 0)
        ds_fs_undersamp = pd.concat([ds_fs_undersamp, df_fs_undersamp], axis = 0)
    

    # Boxplot of C-index by feature selection method
    sns.set_style("white")
    ax_fs = sns.boxplot(y="Algorithm", x="Cross-validation C-index", hue="Feature Selection"
                        ,data=ds_feature_selection
                        ,palette=model_colors[0:ds_feature_selection['Feature Selection'].unique().shape[0]]
                        ,width=0.5, orient="h"
                        ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                        ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the C-index
    plt.savefig(fig_repo+'boxplot_c_index_feature_selection'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()

    # Boxplot of C-index of undersampling method
    sns.set_style("white")
    ax_undersamp = sns.boxplot(y="Algorithm", x="Cross-validation C-index", hue="Undersampling"
                              ,data=ds_undersampling
                              ,palette=model_colors[0:ds_undersampling['Undersampling'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the C-index
    plt.savefig(fig_repo+'boxplot_c_index_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()

    # Boxplot of C-index of feature selection and undersampling method
    sns.set_style("white")
    ax_fs_undersamp = sns.boxplot(y="Algorithm", x="Cross-validation C-index", hue="Method"
                              ,data=ds_fs_undersamp
                              ,palette=model_colors[0:ds_fs_undersamp['Method'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the C-index
    plt.savefig(fig_repo+'boxplot_c_index_feature_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Standard deviation of the C-index in cross-validation    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_sd_comb = pd.DataFrame()
    df_undersamp_fs_comb = pd.DataFrame()
    df_box_fs_undersamp_comb = pd.DataFrame()

    for j in range(len(model.keys())) :
        # Model
        sd_model = model[ list(model.keys())[j] ]
        sd_model_name = [model_name[i] for i in range(len(model_prefix)) if list(model.keys())[j] == model_prefix[i]][0]
        
        list_none = []
        list_undersamp = []
        list_fs = []
        list_undersamp_fs = []

        # Model by method
        df_sd = pd.DataFrame()
        for m in range(len(sd_model)) :
            sd_mtd_mdl = sd_model[m]
            sd_mtd_name = list(sd_mtd_mdl.keys())[0]

            sd_mtd_model = sd_mtd_mdl[sd_mtd_name]
            sd_c_index_k = sd_mtd_model.all_model_cIndex_cv[ sd_mtd_model.all_model_cIndex_test.index(sd_mtd_model.best_model_cIndex_test) ]

            sd_mean = statistics.mean(sd_c_index_k)
            sd_se = statistics.stdev(sd_c_index_k)

            if sd_mtd_name == 'none' :
                sd_undersamp = ''
                sd_feature = ''

                list_none = list_none + sd_c_index_k

            elif 'undersampling_boruta' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = "Boruta SHAP"

                list_undersamp_fs = list_undersamp_fs + sd_c_index_k
            
            elif 'undersampling_stepwise' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = "Stepwise Feature Selection"

                list_undersamp_fs = list_undersamp_fs + sd_c_index_k

            elif 'undersampling_rfe' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = 'Recursive feature elimination'

                list_undersamp_fs = list_undersamp_fs + sd_c_index_k

            elif 'undersampling_weight' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = 'Importance weight'

                list_undersamp_fs = list_undersamp_fs + sd_c_index_k

            elif 'none_undersampling' in sd_mtd_name :
                sd_undersamp = 'Undersampling'
                sd_feature = ''

                list_undersamp = list_undersamp + sd_c_index_k

            elif 'none_rfe' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Recursive feature elimination'

                list_fs = list_fs + sd_c_index_k

            elif 'none_weight' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Importance weight'

                list_fs = list_fs + sd_c_index_k

            elif 'none_boruta' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Boruta SHAP'

                list_fs = list_fs + sd_c_index_k
            
            elif 'none_stepwise' in sd_mtd_name :
                sd_undersamp = ''
                sd_feature = 'Stepwise Feature Selection'

                list_fs = list_fs + sd_c_index_k

            else :
                sd_undersamp = ''
                sd_feature = ''
            
            df_sd = pd.concat([df_sd, pd.DataFrame({'Algorithm': sd_model_name
                                                    ,'Undersampling': [sd_undersamp]
                                                    ,'Feature Selection': [sd_feature]
                                                    ,'Mean of C-index': [sd_mean]
                                                    ,'SD of C-index': [sd_se]}) ], axis = 0)
        df_sd_comb = pd.concat([df_sd_comb, df_sd], axis = 0)
        
        # Mean and standard error of the standard deviation of C-index in cross-validation
        mean_fs1 = [statistics.mean(list_none) if len(list_none) > 0 else np.nan ][0]
        mean_fs2 = [statistics.mean(list_undersamp) if len(list_undersamp) > 0 else np.nan ][0]
        mean_fs3 = [statistics.mean(list_fs) if len(list_fs) > 0 else np.nan ][0]
        mean_fs4 = [statistics.mean(list_undersamp_fs) if len(list_undersamp_fs) > 0 else np.nan ][0]

        se_fs1 = [statistics.stdev(list_none) if len(list_none) > 0 else np.nan ][0]
        se_fs2 = [statistics.stdev(list_undersamp) if len(list_undersamp) > 0 else np.nan ][0]
        se_fs3 = [statistics.stdev(list_fs) if len(list_fs) > 0 else np.nan ][0]
        se_fs4 = [statistics.stdev(list_undersamp_fs) if len(list_undersamp_fs) > 0 else np.nan ][0]

        df_undersamp_fs = pd.DataFrame({'Algorithm': [sd_model_name, sd_model_name, sd_model_name, sd_model_name]
                                        ,'Method': ['None', 'Undersampling', 'Feature selection', 'Undersampling + Feature selection']
                                        ,'Mean of SD': [mean_fs1, mean_fs2, mean_fs3, mean_fs4]
                                        ,'SE': [se_fs1, se_fs2, se_fs3, se_fs4] })
        
        df_undersamp_fs_comb = pd.concat([df_undersamp_fs_comb, df_undersamp_fs], axis = 0)


        df_box_fs_undersamp = pd.DataFrame({'Algorithm': [sd_model_name] * (len(list_none)+len(list_undersamp)+len(list_fs)+len(list_undersamp_fs))
                                            ,'Method': ['None']*len(list_none) + ['Undersampling']*len(list_undersamp) + ['Feature selection']*len(list_fs) + ['Undersampling + Feature selection']*len(list_undersamp_fs)
                                            ,'Standard deviation of C-index': list_none + list_undersamp + list_fs + list_undersamp_fs })
        df_box_fs_undersamp_comb = pd.concat([df_box_fs_undersamp_comb, df_box_fs_undersamp], axis = 0)

    df_sd_comb.to_html(fig_repo+'STD_C_INDEX_tbl_'+fig_prefix+'_1.html')
    df_undersamp_fs_comb.to_html(fig_repo+'STD_C_INDEX_tbl_'+fig_prefix+'_2.html')


    sns.set_style("white")
    ax_fs_undersamp = sns.boxplot(y="Algorithm", x="Standard deviation of C-index", hue="Method"
                              ,data=df_box_fs_undersamp_comb
                              ,palette=model_colors[0:df_box_fs_undersamp_comb['Method'].unique().shape[0]]
                              ,width=0.5, orient="h"
                              ,medianprops={"color": "coral", "alpha": 0.7}, showcaps=False, notch=True
                              ,flierprops={"color": "#e7298a", "alpha": 0.5, "marker": "D"} )
    sns.move_legend(ax_fs_undersamp, "upper left", bbox_to_anchor=(1, 1))
    # Save the boxplot of the C-index
    plt.savefig(fig_repo+'boxplot_std_c_index_feature_undersampling'+fig_prefix+'.png', bbox_inches='tight', dpi=300)
    plt.clf()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def surv_model_features( model           # The model object
                        ,model_prefix    # The prefix of the model
                        ,model_name      # The name of the model
                        ,fig_features    # Feature names
                        ,fig_repo        # Repository of the output
                        ,fig_prefix      # The prefix name of the output
                        ) :
    
    model_keys = list( model.keys() )

    def model_features_loop_1(model_loop):

        model_key = model_keys[model_loop]
        model_key_name = [model_name[i] for i in range(len(model_prefix)) if model_prefix[i] == model_key][0]
        model_shaps = model[model_key]

        print('Model features : ' + model_key + ' (' + model_key_name + ')')

        def model_features_loop_2(ml_loop):

            ml_shap = model_shaps[ml_loop]
            
            ml_shap_prefix = list(ml_shap.keys())[0]
            model_shap = ml_shap[ ml_shap_prefix ]

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Validation data    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            X_validate = model_shap.X_validate
            y_validate = model_shap.y_validate

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    SHAP values of the best model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            try :
                # Fits the explainer
                explainer = shap.Explainer(model_shap.best_model.predict, X_validate)

                # Calculate the SHAP values - It takes some time
                max_evals = X_validate.shape[1] * 2 + 2500
                shap_values = explainer(X_validate, max_evals = max_evals )
                vals = np.abs(shap_values.values).mean(0)

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Feature importance (SHAP values)    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                
                feature_importance = pd.DataFrame(list(zip(X_validate.columns.tolist(), vals)),
                                                columns=['col_name','feature_importance_vals'])
                feature_importance = feature_importance[feature_importance['feature_importance_vals'] != 0]
                feature_importance.sort_values(by=['feature_importance_vals'], ascending=False, inplace=True)
                feature_importance = feature_importance.reset_index().drop(['index'], axis = 1)

                idx_shap = [idx for idx in range(len(vals)) if vals[idx] > 0]
                shap_values.values = shap_values.values[:, idx_shap]
                shap_values.data = shap_values.data[:, idx_shap]
                shap_values.feature_names = [shap_values.feature_names[x] for x in idx_shap]

                shap_variables = shap_values.feature_names
                shap_mean_values = np.abs(shap_values.values).mean(0)
            
            except Exception :
                shap_variables = X_validate.columns.tolist()
                shap_mean_values = [0 for idx in range(len(shap_variables))]

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Feature names    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            ml_shap_name = [data_rename['Rename'][i] for i in range(data_rename.shape[0]) if data_rename['Name'][i] == ml_shap_prefix ][0]

            ls_features = pd.DataFrame({'Variable': shap_variables, ml_shap_name: shap_mean_values })
            ls_features_variable = ls_features['Variable'].tolist()
            for i_fv in range(30) :
                ls_features_variable = [ x.replace('_'+str(i_fv), '') if '_'+str(i_fv) in x else x for x in ls_features_variable ]
            ls_features['Variable'] = ls_features_variable
            ls_features = ls_features.groupby(['Variable']).sum().reset_index()

            ls_c_index = pd.DataFrame({'Variable': ['C-index'], 'Description': ['C-index'], ml_shap_name: [format(model_shap.best_model_cIndex_val, '.3f')] })
            
            return( {'ls_c_index': ls_c_index
                     ,'ls_features': ls_features} )

        print('[DONE] Model features : ' + model_key + ' (' + model_key_name + ')')
        
        ds_features = Parallel(n_jobs = 1)(
            delayed( model_features_loop_2 )(ml_loop = ml_loop)
            for ml_loop in range(len(model_shaps)) )
        
        for ml_loop_p in range(len(ds_features)) :
            if ml_loop_p == 0 :
                df_c_index = ds_features[ml_loop_p]['ls_c_index']
                df_features = pd.merge( fig_features, ds_features[ml_loop_p]['ls_features'], how = 'left', on = ['Variable'] )
            else :
                df_c_index = pd.merge(df_c_index, ds_features[ml_loop_p]['ls_c_index'], how = 'left', on = ['Variable', 'Description'])
                df_features = pd.merge( df_features, ds_features[ml_loop_p]['ls_features'], how = 'left', on = ['Variable'] )
        
        df_features['sum'] = round(df_features.mean(axis = 1), 5)
        df_features['count'] = round(df_features.drop(['Variable', 'Description', 'sum'], axis = 1).count(axis = 1), 0)
        df_features['mean'] = round(df_features['sum'] / df_features['count'], 4)
        df_features['total'] = len(model_shaps)
        df_features.sort_values(['count'], ascending = False, inplace = True)
        df_features = df_features[df_features['count'] > 0]
        df_features.reset_index(drop = True, inplace = True)

        df_c_index_features = pd.concat([df_c_index, pd.DataFrame().reindex_like(df_c_index), df_features], axis = 0)
        df_c_index_features.drop(['Variable'], axis = 1, inplace = True)
        df_c_index_features.rename(columns = {'Description': ' '}, inplace = True)
        df_c_index_features.reset_index(drop = True, inplace = True)

        # Save the output into HTML table
        df_features.to_csv(fig_repo+'SHAP_features_table'+fig_prefix+'_'+model_key+'.csv')
        df_features.to_html(fig_repo+'SHAP_features_table'+fig_prefix+'_'+model_key+'.html')

        df_c_index.to_csv(fig_repo+'C_INDEX_table'+fig_prefix+'_'+model_key+'.csv')
        df_c_index.to_html(fig_repo+'C_INDEX_table'+fig_prefix+'_'+model_key+'.html')

        df_c_index_features.to_csv(fig_repo+'C_INDEX_SHAP_features_table'+fig_prefix+'_'+model_key+'.csv')
        df_c_index_features.to_html(fig_repo+'C_INDEX_SHAP_features_table'+fig_prefix+'_'+model_key+'.html')

        ls_features_comb = df_features[['Variable', 'sum', 'count', 'total']]
        ls_features_comb[model_key_name] = round(df_features['sum'] / df_features['count'], 4)
        ls_features_comb.rename(columns = {'sum': 'sum_'+model_key, 'count': 'count_'+model_key, 'total': 'total_'+model_key}, inplace = True)

        return(ls_features_comb)

        
    ds_features_comb = Parallel(n_jobs = n_jobs)(
        delayed( model_features_loop_1 )(model_loop = model_loop)
        for model_loop in range(len(model)) )
    
    for model_loop_p in range(len(ds_features_comb)) :
        if model_loop_p == 0 :
            df_features_comb = pd.merge(fig_features, ds_features_comb[model_loop_p], how = 'left', on = ['Variable'] )
        else :
            df_features_comb = pd.merge(df_features_comb, ds_features_comb[model_loop_p], how = 'left', on = ['Variable'])
    
    df_features_comb['sum'] = df_features_comb[['sum_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['count'] = df_features_comb[['count_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['total'] = df_features_comb[['total_'+x for x in model_prefix]].sum(axis = 1)
    df_features_comb['pct'] = df_features_comb['count'] / df_features_comb['total'] * 100
    df_features_comb['mean'] = df_features_comb['sum'] / df_features_comb['count']
    df_features_comb.sort_values(['count'], ascending = False, inplace = True)
    df_features_comb = df_features_comb[df_features_comb['mean'] > 0]
    df_features_comb.reset_index(drop = True, inplace = True)

    df_features_comb.to_csv(fig_repo+'C_INDEX_SHAP_features_table_comb'+fig_prefix+'.csv')
    df_features_comb.to_html(fig_repo+'C_INDEX_SHAP_features_table_comb'+fig_prefix+'.html')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function to generate outputs for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def generate_surv_outputs_comparison(ds_load) :

    list_perm = []
    for loop_file_repo in file_repo_prefix_loop :
        for loop_ml_prefix in ml_prefix :
                list_perm.append( [loop_file_repo] + [loop_ml_prefix] )
    
    def gen_outputs(loop) :
        file_repo_prefix = list_perm[loop][0]
        loop_ml_prefix = list_perm[loop][1]
        print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        model_surv = ds_load['model_surv'][ list_perm[loop][0] ][ list_perm[loop][1] ]
        best_surv_model = ds_load['best_surv_model'][ list_perm[loop][0] ][ list_perm[loop][1] ]

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    C-index of the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        ml_colors = ['#7E539C', '#0075B5', '#00AB9E', '#87C9AC', '#9BB531', '#FFDC60', '#F9AF1F', '#F59D9B', '#744700']
        model_c_index( model = model_surv
                    ,best_model = best_surv_model
                    ,model_prefix = ls_ml_models
                    ,model_name = ls_ml_model_name
                    ,model_colors = ml_colors
                    ,fig_repo = file_repo+file_repo_prefix+set_outcome+'/Comparison/'
                    ,fig_prefix = loop_ml_prefix )
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Selected features    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        surv_model_features( model = model_surv
                            ,model_prefix = ls_ml_models
                            ,model_name = ls_ml_model_name
                            ,fig_features = data_info[['Variable', 'Description']]
                            ,fig_repo = file_repo+file_repo_prefix+set_outcome+'/Features by Methods/'
                            ,fig_prefix = loop_ml_prefix )

    n_jobs_tmp = len(list_perm)
    Parallel(n_jobs = n_jobs_tmp)(
        delayed( gen_outputs )(loop = loop)
        for loop in range(len(list_perm)) )
    
    # for loop in range(len(list_perm)) :
    #     gen_outputs(loop = loop)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Combining features from SHAP values    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def surv_features_combining( repo_prefix
                            ,fig_repo
                            ,fig_repo_out
                            ,fig_outcome
                            ,fig_prefix
                            ,fig_features
                            ,list_ml_models
                            ,cutoff
                            ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the overall selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_overall = fig_features
    for repo_prefix_loop in repo_prefix :
        ds_features_overall = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'C_INDEX_SHAP_features_table_comb'+fig_prefix+'.html' )[0]
        ds_features_overall = ds_features_overall[['Variable', 'Description', 'pct']]
        ds_features_overall.rename(columns = {'pct': repo_prefix_loop.replace('/', '')}, inplace = True)
        
        df_features_overall = pd.merge(df_features_overall, ds_features_overall, how = 'left', on = ['Variable', 'Description'])

    
    df_features_overall = df_features_overall[ df_features_overall.sum(axis = 1) > 0 ]
    df_features_overall['Average of %'] = df_features_overall.mean(axis = 1)
    df_features_overall.sort_values(by = ['Average of %'], ascending = False, inplace = True)
    df_features_overall.reset_index(drop = True, inplace = True)

    df_features_overall.to_csv(fig_repo_out+'Cross_cohort_average_features_table'+fig_prefix+'.csv')
    df_features_overall.to_html(fig_repo_out+'Cross_cohort_average_features_table'+fig_prefix+'.html')


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_best = fig_features
    df_features_best_comb = fig_features
    for repo_prefix_loop in repo_prefix :

        list_features_cindex = []
        for list_ml_models_loop in list_ml_models :
            ds_features_best_cindex = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'C_INDEX_SHAP_features_table'+fig_prefix+'_'+list_ml_models_loop+'.html' )[0]
            ds_features_best_cindex = ds_features_best_cindex[ ds_features_best_cindex['Unnamed: 1'] == 'C-index' ]
            ds_features_best_cindex.drop(['sum', 'count', 'mean', 'total'], axis = 1, inplace = True)
            ds_features_best_cindex_2 = pd.DataFrame(ds_features_best_cindex.max()).reset_index()
            features_cindex = ds_features_best_cindex.max(axis = 1).tolist()[0]
            list_features_cindex.append(features_cindex)
            best_method = ds_features_best_cindex_2[ ds_features_best_cindex_2[0] == features_cindex ]['index'].tolist()[0]

            ds_features_best = pd.read_html( fig_repo+repo_prefix_loop+fig_outcome+'/Features by Methods/'+'SHAP_features_table'+fig_prefix+'_'+list_ml_models_loop+'.html' )[0]
            ds_features_best = ds_features_best[['Variable', 'Description', best_method]]
            ds_features_best.rename(columns = {best_method: list_ml_models_loop}, inplace = True)

            df_features_best = pd.merge(df_features_best, ds_features_best, how = 'left', on = ['Variable', 'Description'])
    
        df_features_best = df_features_best[ df_features_best.sum(axis = 1) > 0 ]
        best_algmn = list_ml_models[ list_features_cindex.index( max(list_features_cindex) ) ]
        df_features_best = df_features_best[['Variable', 'Description', best_algmn]]
        df_features_best[best_algmn] = np.where( df_features_best[best_algmn] > cutoff, df_features_best[best_algmn], np.NaN )
        df_features_best.rename(columns = {best_algmn: repo_prefix_loop.replace('/', '') }, inplace = True)
    
        df_features_best_comb = pd.merge(df_features_best_comb, df_features_best, how = 'left', on = ['Variable', 'Description'] )
    
    df_features_best_comb['count'] = df_features_best_comb[[x.replace('/', '') for x in repo_prefix]].count(axis = 1)
    df_features_best_comb = df_features_best_comb[ df_features_best_comb['count'] > 1 ]
    df_features_best_comb.sort_values(by = ['count', 'Description'], ascending = [False, True], inplace = True)
    df_features_best_comb.reset_index(drop = True, inplace = True)

    df_features_best_comb.to_csv(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+'.csv')
    df_features_best_comb.to_html(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+'.html')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function to generate table with C-index    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def generate_tables_cindex() :

    for loop_ml_prefix in ml_prefix :

        tbl_c_index_overall_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_c_index_none_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_c_index_norm_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})
        tbl_c_index_influential_comb = pd.DataFrame({'Algorithm': ls_ml_model_name})

        for loop_file_repo in file_repo_prefix_loop :

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_ml = dict()
            best_ml_model = dict()

            ds_c_index_overall_comb = pd.DataFrame()
            ds_c_index_none_comb = pd.DataFrame()
            ds_c_index_norm_comb = pd.DataFrame()
            ds_c_index_influential_comb = pd.DataFrame()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                model_ml[ ls_ml_models[ml_loop] ] = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                # Overall C-index
                max_avg_c_index = best_ml_model[ ls_ml_models[ml_loop] ].best_model_cIndex_test
                idx_max_avg_c_index = best_ml_model[ ls_ml_models[ml_loop] ].all_model_cIndex_test.index(max_avg_c_index)
                sd_c_index = statistics.stdev(best_ml_model[ ls_ml_models[ml_loop] ].all_model_cIndex_cv[idx_max_avg_c_index])
                holdout_c_index = best_ml_model[ ls_ml_models[ml_loop] ].best_model_cIndex_val
                cohort_name = ' (' + file_repo_prefix.replace('/', '') + ')'

                ds_c_index_overall = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                               ,'Average AUC'+cohort_name: [max_avg_c_index]
                                               ,'SD AUC'+cohort_name: [sd_c_index]
                                               ,'Hold-out AUC'+cohort_name: [holdout_c_index] })
                ds_c_index_overall_comb = pd.concat([ds_c_index_overall_comb, ds_c_index_overall], axis = 0)

                # C-index by methods
                model_combine = model_ml[ ls_ml_models[ml_loop] ]

                model_keys = []
                model_combine_c_index = []
                for none_loop in range(len(model_combine)) :
                    model_keys += list(model_combine[none_loop].keys())
                    model_combine_c_index += [model_combine[none_loop][model_keys[none_loop]].best_model_cIndex_test]
                
                # C-index without normalization
                model_none_keys = [i for i in range(len(model_keys)) if 'none' in model_keys[i]]
                if len(model_none_keys) > 0 :
                    model_c_index_none = [model_combine_c_index[i] for i in model_none_keys]
                    max_c_index_none = max(model_c_index_none)
                    idx_model_none = model_none_keys[ model_c_index_none.index(max_c_index_none) ]
                    model_none_method = model_keys[ idx_model_none ]
                    model_none = model_combine[idx_model_none][model_none_method]
                    sd_none_c_index = statistics.stdev( model_none.all_model_cIndex_cv[ model_none.all_model_cIndex_test.index(model_none.best_model_cIndex_test) ] )

                    ds_c_index_none = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                                    ,'Method'+cohort_name: [model_none_method]
                                                    ,'Average C-index'+cohort_name: [model_none.best_model_cIndex_test]
                                                    ,'SD C-index'+cohort_name: [sd_none_c_index]
                                                    ,'Hold-out C-index'+cohort_name: [model_none.best_model_cIndex_val]
                                                    })
                    ds_c_index_none_comb = pd.concat([ds_c_index_none_comb, ds_c_index_none], axis = 0)
                else :
                    pass

                # C-index with normalization
                model_norm_keys = [i for i in range(len(model_keys)) if 'none' not in model_keys[i]]
                if len(model_norm_keys) > 0 :
                    model_c_index_norm = [model_combine_c_index[i] for i in model_norm_keys]
                    max_c_index_norm = max(model_c_index_norm)
                    idx_model_norm = model_norm_keys[ model_c_index_norm.index(max_c_index_norm) ]
                    model_norm_method = model_keys[ idx_model_norm ]
                    model_norm = model_combine[idx_model_norm][model_norm_method]
                    sd_norm_c_index = statistics.stdev( model_norm.all_model_cIndex_cv[ model_norm.all_model_cIndex_test.index(model_norm.best_model_cIndex_test) ] )

                    ds_c_index_norm = pd.DataFrame({'Algorithm': [ls_ml_model_name[ml_loop]]
                                                ,'Method'+cohort_name: [model_norm_method]
                                                ,'Average C-index'+cohort_name: [model_norm.best_model_cIndex_test]
                                                ,'SD C-index'+cohort_name: [sd_norm_c_index]
                                                ,'Hold-out C-index'+cohort_name: [model_norm.best_model_cIndex_val] })
                    ds_c_index_norm_comb = pd.concat([ds_c_index_norm_comb, ds_c_index_norm], axis = 0)
                
                else :
                    pass
            
            # Combine the tables from different cohort
            tbl_c_index_overall_comb = pd.merge(tbl_c_index_overall_comb, ds_c_index_overall_comb, how = 'left', on = ['Algorithm'])
            tbl_c_index_overall_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Overall'+loop_ml_prefix+'.csv')
            tbl_c_index_overall_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Overall'+loop_ml_prefix+'.html')

            if len(model_none_keys) > 0 :
                tbl_c_index_none_comb = pd.merge(tbl_c_index_none_comb, ds_c_index_none_comb, how = 'left', on = ['Algorithm'])
                tbl_c_index_none_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Without_Normalization'+loop_ml_prefix+'.csv')
                tbl_c_index_none_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Without_Normalization'+loop_ml_prefix+'.html')
            else :
                pass

            if len(model_norm_keys) > 0 :
                tbl_c_index_norm_comb = pd.merge(tbl_c_index_norm_comb, ds_c_index_norm_comb, how = 'left', on = ['Algorithm'])
                tbl_c_index_norm_comb.to_csv(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Normalization'+loop_ml_prefix+'.csv')
                tbl_c_index_norm_comb.to_html(file_repo+'CROSS_COHORT/'+set_outcome+'/'+'Table_C_INDEX_Normalization'+loop_ml_prefix+'.html')
            else :
                pass


##############################################################################################################################################################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_surv_models_avg_cv_comparison(ana_type = "") :

    model_vars_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_vars_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :

            #******************** Check *********************
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_vars = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                if (ana_type == "_unnormalized") & (loop_file_repo in file_repo_prefix_multi) :
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_cIndex']

                    # Unnormalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_cIndex': [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected

                    # Optimal unnormalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select
                
                elif (ana_type == "_normalized") & (loop_file_repo in file_repo_prefix_multi):
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_cIndex']

                    # Normalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_cIndex': [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected

                    # Optimal Normalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select

                else :
                    # List of 'model_name', 'num_vars', 'model_avg_cIndex', 'model_selected_features', 'idx_optimal' of the algorithm
                    model_vars[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                    # Model with highest average C-index score
                    best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            
            model_vars_load[loop_ml_prefix] = model_vars
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_vars_comb[loop_file_repo] = model_vars_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    
    return({'model_vars' : model_vars_comb
            ,'best_ml_model' : best_ml_model_comb })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected of the optimal compact model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def surv_avg_cv_features_combining( repo_prefix
                                   ,fig_repo_out
                                   ,fig_prefix
                                   ,fig_prefix_add
                                   ,fig_features
                                   ,list_ml_models
                               ):
     
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    df_features_best = fig_features.copy()
    df_features_info = pd.DataFrame({'Variable': ['avg_score', 'num_vars', 'algorithm']
                                     ,'Description': ['Average score in CV', 'Number of features', 'Algorithm'] })
    
    df_table_score = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })
    df_table_method = pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal'] })

    df_features_optimal_model = fig_features.copy()

    df_feats_perm_importance = fig_features.copy()

    # df_features_pct = fig_features.copy()
    # df_features_rank = fig_features.copy()

    ls_ds_optimal_features_cohort = dict()
    
    for repo_prefix_loop in repo_prefix[0] + repo_prefix[1] :

        ls_optimal_model_name = []
        ls_optimal_model_nvars = []
        ls_optimal_model_important = []
        ls_optimal_avg_cIndex = []
        ls_optimal_sd_cv = []
        ls_ds_optimal_features = []
        ls_optimal_holdout = []

        # Optimal model within algorithms
        ls_features_mtd = []
        ls_number_mtd = []

        ls_cnt_feats_train = []
        ls_cnt_vars = []
        ls_selected_features = []

        ls_ds_optimal_features_cohort[repo_prefix_loop] = dict()

        for list_ml_models_loop in list_ml_models :
            
            ls_model_vars = ds_perm_avg_cv_compare['model_vars'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]
            ls_best_ml_model = ds_perm_avg_cv_compare['best_ml_model'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]

            # The method of the optimal model (highest average AUC score in cross-validation)
            idx_optimal = ls_model_vars['model_avg_cIndex'].index( max(ls_model_vars['model_avg_cIndex']) )

            optimal_model_name = ls_model_vars['model_name'][ idx_optimal ]
            optimal_model_nvars = ls_model_vars['num_vars'][ idx_optimal ]
            optimal_avg_cIndex = ls_model_vars['model_avg_cIndex'][ idx_optimal ]
            # optimal_sd_cv = statistics.stdev(ls_best_ml_model.all_model_cIndex_cv[ ls_best_ml_model.all_model_cIndex_test.index( max(ls_best_ml_model.all_model_cIndex_test) ) ])
            optimal_sd_cv = statistics.stdev(ls_best_ml_model.all_model_cIndex_cv[0]['kfold_cIndex'])
            optimal_holdout = ls_best_ml_model.best_model_cIndex_val

            #### List of features
            # num_vars = ls_model_vars['num_vars'][ idx_optimal ]
            # model_features = ls_model_vars['model_selected_features'][ idx_optimal ]
            ml_ds_validate = pd.DataFrame({ls_best_ml_model.var_time: ls_best_ml_model.time_validate
                                           ,ls_best_ml_model.var_status: ls_best_ml_model.status_validate})
            ml_ds_training = pd.DataFrame({ls_best_ml_model.var_time: ls_best_ml_model.time_training
                                           ,ls_best_ml_model.var_status: ls_best_ml_model.status_training})
            _, y_training_validate = get_x_y( pd.concat([ml_ds_training, ml_ds_validate], axis = 0)
                                             ,attr_labels = [ls_best_ml_model.var_status, ls_best_ml_model.var_time], pos_label = 1 )
            perm = abs(permutation_importance(ls_best_ml_model.best_model
                                              ,pd.concat([ls_best_ml_model.X_training, ls_best_ml_model.X_validate], axis = 0)
                                              ,y_training_validate
                                              ,random_state = ls_best_ml_model.seed_number).importances_mean)
            # Features with predictive impact in the model
            model_features = [ ls_model_vars['model_selected_features'][ idx_optimal ][x] for x in range(len(perm)) if perm[x] != 0 ]
            num_vars = len(model_features)

            # Permutation importance of the features
            model_features_important = [ perm[x] for x in range(len(perm)) if perm[x] != 0 ]
            model_features_tmp = [x.replace('_0','').replace('_1','').replace('_2','').replace('_3','').replace('_4','')\
                                              .replace('_5','').replace('_6','').replace('_7','').replace('_8','').replace('_9','')\
                                                for x in model_features]
            df_model_features_important = pd.DataFrame({'Variable':model_features_tmp, 'perm_important': model_features_important})
            df_model_features_important['rank'] = df_model_features_important['perm_important'].abs().rank(ascending=False)

            # Features with predictive impact in the model
            model_selected_features = [x.replace('_0', '').replace('_1', '').replace('_2', '').replace('_3', '').replace('_4', '').replace('_5', '').replace('_6', '').replace('_7', '').replace('_8', '').replace('_9', '') for x in model_features ]
            model_selected_features = np.unique(model_selected_features).tolist()
            
            # The features of the optimal model for each cohort
            kfold_candidates = ls_best_ml_model.all_model_cIndex_cv[0]['kfold_candidates']
            model_features_cv = []
            for kk in range(len(kfold_candidates)) :
                unique_kfold_candidates = [x.replace('_0', '').replace('_1', '').replace('_2', '').replace('_3', '').replace('_4', '').replace('_5', '').replace('_6', '').replace('_7', '').replace('_8', '').replace('_9', '') for x in kfold_candidates[kk] ]
                unique_kfold_candidates = np.unique(unique_kfold_candidates).tolist()
                model_features_cv += unique_kfold_candidates
            
            frequency_dict = {}
            for element in model_features_cv:
                if element in frequency_dict:
                    frequency_dict[element] += 1
                else:
                    frequency_dict[element] = 1
            # Percentage of selected features in each cross-validation
            ds_optimal_features = pd.DataFrame({'Variable': list(frequency_dict.keys()), repo_prefix_loop: [x/ls_best_ml_model.k_fold*100 for x in list(frequency_dict.values()) ] })

            # The selected features for each cohort
            for mtd_loop in range(len(ls_model_vars['model_name'])) :
                # List of selected features
                ls_features_mtd += np.unique([x.replace('_0','').replace('_1','').replace('_2','').replace('_3','').replace('_4','')\
                                              .replace('_5','').replace('_6','').replace('_7','').replace('_8','').replace('_9','')\
                                                for x in ls_model_vars['model_selected_features'][mtd_loop]]).tolist()
            cnt_vars = Counter(ls_features_mtd)
            ls_number_mtd += [len(ls_model_vars['model_name'])]
        
            # Append the list
            ls_optimal_model_name.append( optimal_model_name )
            # ls_optimal_model_nvars.append( num_vars )
            ls_optimal_model_nvars.append( num_vars )               # Number of predictive features (based on permutation importance)
            ls_optimal_model_important.append( df_model_features_important ) # List of predictors and permutation importance
            ls_optimal_avg_cIndex.append( optimal_avg_cIndex )
            ls_optimal_sd_cv.append( optimal_sd_cv )
            ls_ds_optimal_features.append( ds_optimal_features )
            ls_optimal_holdout.append( optimal_holdout )
            ls_cnt_vars.append( cnt_vars )                          # Number of features (count it as without encoding)
            ls_cnt_feats_train.append( len(perm) )                  # Number of features trained
            ls_selected_features.append(model_selected_features)

            ls_ds_optimal_features_cohort[repo_prefix_loop][list_ml_models_loop] = ds_optimal_features


        # Index of ls_optimal_model_nvars with maximum average score
        idx_model = ls_optimal_avg_cIndex.index( max(ls_optimal_avg_cIndex) )
        
        #### Optimal model (highest average AUC score in cross-validation)
        # selected features in each cross-validation
        ml_model_nvars = ls_optimal_model_nvars[idx_model]
        ml_avg_cIndex = ls_optimal_avg_cIndex[idx_model]
        ml_features = ls_ds_optimal_features[idx_model]
        ml_info = pd.DataFrame({'Description': ['Average score in CV', 'Number of features', 'Algorithm']
                                     ,repo_prefix_loop: [ml_avg_cIndex, ml_model_nvars, list_ml_models[idx_model]] })
        
        # Selected features of the optimal model
        opt_ml_selected_features = pd.DataFrame({'Variable': ls_selected_features[idx_model], repo_prefix_loop: [1 for x in range(len(ls_selected_features[idx_model]))] })
        df_features_optimal_model = pd.merge(df_features_optimal_model, opt_ml_selected_features, how = 'left', on = ['Variable'])

        # The data frame listed the features of the optimal model
        df_features_best = pd.merge(df_features_best, ml_features, how = 'left', on = ['Variable'])
        df_features_info = pd.merge(df_features_info, ml_info, how = 'left', on = ['Description'])

        # Summary table of the average score, hold-out score and method for each algorithm
        df_table_score = pd.merge(df_table_score, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_cIndex + [ ls_optimal_avg_cIndex[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cv + [ ls_optimal_sd_cv[idx_model] ]
                                                                ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                }), how = 'left', on = ['Algorithm'] )
        
        df_table_method = pd.merge(df_table_method, pd.DataFrame({'Algorithm': ls_ml_model_name + ['optimal']
                                                                  ,repo_prefix_loop.replace('/', '_')+'method': ls_optimal_model_name + [ ls_optimal_model_name[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'avg_score': ls_optimal_avg_cIndex + [ ls_optimal_avg_cIndex[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'sd_cv': ls_optimal_sd_cv + [ ls_optimal_sd_cv[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'hold_out': ls_optimal_holdout + [ ls_optimal_holdout[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_predictive': ls_optimal_model_nvars + [ ls_optimal_model_nvars[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_train' : ls_cnt_feats_train + [ ls_cnt_feats_train[idx_model] ]
                                                                  ,repo_prefix_loop.replace('/', '_')+'nvars_unique_predictive': [len(x) for x in ls_selected_features] + [ len(ls_selected_features[idx_model]) ]
                                                                  }), how = 'left', on = ['Algorithm'] )
        
        # Ranking of the predictors based on permutation importance
        df_feats_perm_importance1 = ls_optimal_model_important[idx_model]
        df_feats_perm_importance1 = df_feats_perm_importance1.groupby('Variable')['rank'].mean().reset_index()
        df_feats_perm_importance2 = pd.merge(fig_features, df_feats_perm_importance1, on = ['Variable'], how = 'left')
        df_feats_perm_importance2 = df_feats_perm_importance2[df_feats_perm_importance2['rank']>=0]
        df_feats_perm_importance2.rename(columns = {'rank': repo_prefix_loop}, inplace = True)
        df_feats_perm_importance = pd.merge(df_feats_perm_importance, df_feats_perm_importance2, on = ['Variable', 'Description'], how = 'left')
    
    # To summarize the average of feature selection in CV
    ml_features_algorithm = fig_features.copy()
    for repo_prefix_loop in repo_prefix[0]+repo_prefix[1] :

        ds_algm = fig_features.copy()
        for list_ml_models_loop in list_ml_models :
            ls_features_algm = ls_ds_optimal_features_cohort[repo_prefix_loop][list_ml_models_loop]
            ls_features_algm = ls_features_algm.rename(columns={repo_prefix_loop:str(list_ml_models_loop)})
            ds_algm = pd.merge(ds_algm, ls_features_algm,on = ['Variable'], how = 'left')
        
        # Average the average across algorithms
        ds_algm = ds_algm.fillna(0)
        ds_algm['avg_'+repo_prefix_loop] = ds_algm[list_ml_models].mean(axis = 1)
        ds_algm.drop(list_ml_models, axis = 1, inplace = True)
        ds_algm.rename(columns = {'avg_'+repo_prefix_loop: repo_prefix_loop}, inplace = True)
        ds_algm = ds_algm[ ds_algm[repo_prefix_loop] != 0 ]

        ml_features_algorithm = pd.merge(ml_features_algorithm, ds_algm, on = ['Variable', 'Description'], how = 'left')

    # Average across cohort
    ml_features_algorithm = ml_features_algorithm.fillna(0)
    ml_features_algorithm['avg_single'] = ml_features_algorithm[repo_prefix[0]].mean(axis = 1)
    ml_features_algorithm['avg_all'] = ml_features_algorithm[repo_prefix[1]].mean(axis = 1)
    ml_features_algorithm = ml_features_algorithm[ (ml_features_algorithm['avg_single'] != 0) | (ml_features_algorithm['avg_all'] != 0) ]
    ml_features_algorithm.sort_values(by = ['avg_single', 'avg_all'], ascending = False, inplace = True)
    ml_features_algorithm.reset_index(drop = True, inplace = True)



    # The data listed features of the optimal model, average score in CV, number of features and algorithm across cohort analyses
    ml_features_comb = pd.concat([df_features_info, df_features_best], axis = 0)
    ml_features_comb['count_single'] = ml_features_comb[[x for x in repo_prefix[0] ]].count(axis = 1)                        # Count for single cohort analyses
    ml_features_comb['count_all'] = ml_features_comb[[x for x in repo_prefix[0]+repo_prefix[1] ]].count(axis = 1)            # Count for all the cohort analyses
    ml_features_comb = ml_features_comb[ ml_features_comb['count_all'] > 0 ]
    ml_features_comb['avg_single'] = ml_features_comb[[x for x in repo_prefix[0] ]].sum(axis = 1)                           # Average % for single cohort analyses
    ml_features_comb['avg_single'] = pd.to_numeric(ml_features_comb['avg_single'], errors='coerce') / len(repo_prefix[0])
    ml_features_comb['avg_all'] = ml_features_comb[[x for x in repo_prefix[0]+repo_prefix[1] ]].sum(axis = 1)               # Average % for all the cohort analyses
    ml_features_comb['avg_all'] = pd.to_numeric(ml_features_comb['avg_all'], errors='coerce') / len(repo_prefix[0]+repo_prefix[1])

    ml_features_comb['avg_single'] = np.where(ml_features_comb['Variable'].isin(['algorithm', 'avg_score', 'num_vars']), 200, ml_features_comb['avg_single'])
    ml_features_comb['avg_all'] = np.where(ml_features_comb['Variable'].isin(['algorithm', 'avg_score', 'num_vars']), 200, ml_features_comb['avg_all'])
    
    ml_features_comb.sort_values(by=['avg_single', 'count_single', 'avg_all', 'count_all'], ascending=False, inplace = True)
    ml_features_comb.reset_index(drop = True, inplace = True)

    # The data listed the features with predictive impact in the optimal model
    df_features_optimal_model['count_single'] = df_features_optimal_model[[x for x in repo_prefix[0] ]].count(axis = 1)                # Count for single cohort analyses
    df_features_optimal_model['count_all'] = df_features_optimal_model[[x for x in repo_prefix[0]+repo_prefix[1] ]].count(axis = 1)    # Count for all the cohort analyses
    df_features_optimal_model = df_features_optimal_model[ df_features_optimal_model['count_all'] > 0 ]
    df_features_optimal_model.sort_values(by='count_single', ascending=False, inplace = True)
    df_features_optimal_model.reset_index(drop = True, inplace = True)

    # Save the table into .csv and .html
    df_table_score.to_csv(fig_repo_out+'Table_C_INDEX_Overall'+fig_prefix+fig_prefix_add+'.csv')
    df_table_score.to_html(fig_repo_out+'Table_C_INDEX_Overall'+fig_prefix+fig_prefix_add+'.html')

    df_table_method.to_csv(fig_repo_out+'Table_C_INDEX_Overall_Method'+fig_prefix+fig_prefix_add+'.csv')
    df_table_method.to_html(fig_repo_out+'Table_C_INDEX_Overall_Method'+fig_prefix+fig_prefix_add+'.html')

    df_features_optimal_model.to_csv(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+fig_prefix_add+'.csv')
    df_features_optimal_model.to_html(fig_repo_out+'Cross_cohort_features_table'+fig_prefix+fig_prefix_add+'.html')

    ml_features_comb.to_csv(fig_repo_out+'Cross_cohort_features_CV_table'+fig_prefix+fig_prefix_add+'.csv')
    ml_features_comb.to_html(fig_repo_out+'Cross_cohort_features_CV_table'+fig_prefix+fig_prefix_add+'.html')

    ml_features_algorithm.to_csv(fig_repo_out+'Cross_cohort_features_CV_table_algorithms'+fig_prefix+fig_prefix_add+'.csv')
    ml_features_algorithm.to_html(fig_repo_out+'Cross_cohort_features_CV_table_algorithms'+fig_prefix+fig_prefix_add+'.html')

    return({'df_feats_perm_importance': df_feats_perm_importance        # Rank of the predictors based on permutation importance
            })