#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Cross Cohort Analysis                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_Y4', 'NP4DYSKI2_bs_year']
ml_prefix = '_dyski'
ml_prefix_add = ['', '_excl']
file_repo_prefix_single = ['LUXPARK/', 'PPMI/', 'ICEBERG/']
file_repo_prefix_multi = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']

ml_ana_cv_type = ['', '_algorithms']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Combining the features from classification and time-to-event analysis (Single and Multi=cohort separately)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for ml_ana_cv_type_loop in ml_ana_cv_type :
    df_class_surv = dict()
    for ml_prefix_add_loop in ml_prefix_add :
        # To load the csv file
        ds_features_class = pd.read_csv(file_repo+'/CROSS_COHORT/'+set_outcome[0]+'/Cross_cohort_features_CV_table'+ml_ana_cv_type_loop+ml_prefix+'_y4'+ml_prefix_add_loop+'.csv')
        ds_features_surv = pd.read_csv(file_repo+'/CROSS_COHORT/'+set_outcome[1]+'/Cross_cohort_features_CV_table'+ml_ana_cv_type_loop+ml_prefix+ml_prefix_add_loop+'.csv')

        df_class_surv[ml_prefix_add_loop] = dict()
        for file_repo_ana in [file_repo_prefix_single, file_repo_prefix_multi] :
            # Extract the percentage of feature selection in CV
            ds_class = ds_features_class[['Variable', 'Description']+file_repo_ana]
            ds_surv = ds_features_surv[['Variable', 'Description']+file_repo_ana]

            ds_class = ds_class[ ~ds_class['Variable'].isin(['avg_score', 'algorithm', 'num_vars']) ]
            ds_surv = ds_surv[ ~ds_surv['Variable'].isin(['avg_score', 'algorithm', 'num_vars']) ]

            # Convert columns to numeric
            ds_class[file_repo_ana] = ds_class[file_repo_ana].apply(pd.to_numeric, errors = 'coerce')
            ds_surv[file_repo_ana] = ds_surv[file_repo_ana].apply(pd.to_numeric, errors = 'coerce')

            # Replace NaN with 0 in all row
            ds_class.fillna(0, inplace = True)
            ds_surv.fillna(0, inplace = True)

            # Average of % of feature selection in CV across cohort
            ds_class['average_pct_class'+ml_prefix_add_loop] = ds_class[file_repo_ana].mean(axis = 1)
            ds_surv['average_pct_surv'+ml_prefix_add_loop] = ds_surv[file_repo_ana].mean(axis = 1)

            # To combine the features
            ds_class_surv = pd.merge(data_info[['Variable', 'Description']], ds_class[['Variable', 'average_pct_class'+ml_prefix_add_loop]], on = 'Variable', how = 'left')
            ds_class_surv = pd.merge(ds_class_surv, ds_surv[['Variable', 'average_pct_surv'+ml_prefix_add_loop]], on = 'Variable', how = 'left')

            # To keep the variable where no missing in average_pct_class or average_pct_surv
            ds_class_surv = ds_class_surv[ (~ds_class_surv['average_pct_class'+ml_prefix_add_loop].isnull()) | (~ds_class_surv['average_pct_surv'+ml_prefix_add_loop].isnull()) ]
            ds_class_surv = ds_class_surv[ (ds_class_surv['average_pct_class'+ml_prefix_add_loop] != 0) | (ds_class_surv['average_pct_surv'+ml_prefix_add_loop] != 0) ]

            # Calculate the average of 'average_pct_class' and 'average_pct_surv'
            ds_class_surv['average_pct'+ml_prefix_add_loop] = ds_class_surv[['average_pct_class'+ml_prefix_add_loop, 'average_pct_surv'+ml_prefix_add_loop]].mean(axis = 1)

            # Reset the index
            ds_class_surv.reset_index(drop = True, inplace = True)

            # Keep the output data
            if file_repo_ana == file_repo_prefix_single :
                file_repo_ana_name = 'single'
            elif file_repo_ana == file_repo_prefix_multi :
                file_repo_ana_name = 'multi'
            df_class_surv[ml_prefix_add_loop][file_repo_ana_name] = ds_class_surv


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Comparing the features on ml_prefix_add    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    for ana_type in ['single', 'multi'] :
        for xloop in range(len(ml_prefix_add)) :
            if xloop == 0 :
                ds_summary_table = pd.merge(data_info[['Variable', 'Description']], df_class_surv[ml_prefix_add[xloop]][ana_type], on = ['Variable', 'Description'], how = 'left')
            else :
                ds_summary_table = pd.merge(ds_summary_table, df_class_surv[ml_prefix_add[xloop]][ana_type], on = ['Variable', 'Description'], how = 'left')

        # To remove the rows if all are missing
        ds_summary_table['check'] = ds_summary_table[ds_summary_table.columns.tolist()[2:]].sum(axis = 1)
        ds_summary_table = ds_summary_table[ds_summary_table['check'] != 0]
        ds_summary_table.drop(['check'], axis = 1, inplace = True)

        # List of 'average_pct'
        ds_summary_table['avg_pct_overall'] = ds_summary_table[['average_pct'+ x for x in ml_prefix_add]].mean(axis = 1)
        ds_summary_table.sort_values(by = 'avg_pct_overall', ascending = False, inplace = True)
        ds_summary_table.reset_index(drop = True, inplace = True)

        # Export the table into both classification and time-to-event analysis output folder
        for set_outcome_loop in set_outcome:
            ds_summary_table.to_csv(file_repo+'CROSS_COHORT/'+set_outcome_loop+'/Selected_Features_CV_summary_'+set_outcome[0]+'_'+set_outcome[1]+'_'+ana_type+ml_ana_cv_type_loop+'.csv')
            ds_summary_table.to_html(file_repo+'CROSS_COHORT/'+set_outcome_loop+'/Selected_Features_CV_summary_'+set_outcome[0]+'_'+set_outcome[1]+'_'+ana_type+ml_ana_cv_type_loop+'.html')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Ranking scores of the predictors    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Load the predictors' rank from optimal classification model
class_rank = load_object(filename = "Ranking_features_optimal_models", file_repo = file_repo+'CROSS_COHORT/'+set_outcome[0]+'/')

# Load the predictors' rank from optimal time-to-event model
surv_rank = load_object(filename = "Ranking_features_optimal_models", file_repo = file_repo+'CROSS_COHORT/'+set_outcome[1]+'/')

df_predictors_rank = []
# List of predictors and ranking (permutation importance) for `ml_prefix_add` of classification and time-to-event model
for ml_prefix_loop in ml_prefix_add:
    # List of predictors and ranking from the optimal Classification model
    df_class_rank = class_rank[''][ml_prefix+'_y4'+ml_prefix_loop][['Variable', 'Description', 'ALL/']]
    df_class_rank = df_class_rank[~df_class_rank['ALL/'].isna()]

    # List of predictors and ranking from the optimal time-to-event model
    df_surv_rank = surv_rank[''][ml_prefix+ml_prefix_loop][['Variable', 'Description', 'ALL/']]
    df_surv_rank = df_surv_rank[~df_surv_rank['ALL/'].isna()]

    df_predictors_rank.append( df_class_rank )
    df_predictors_rank.append( df_surv_rank )

# Combining the data frame
tbl_predictors_rank = pd.concat(df_predictors_rank, ignore_index=True)

# Calculate the average of 'ALL/' grouped by 'Variable' and 'Description'
tbl_predictors_rank_avg = tbl_predictors_rank.groupby(['Variable', 'Description'])['ALL/'].mean().reset_index()
tbl_predictors_rank_avg.sort_values(['ALL/'], inplace = True)
tbl_predictors_rank_avg.reset_index(drop = True, inplace = True)

# Export the table into both classification and time-to-event analysis output folder
tbl_predictors_rank_avg.to_csv(file_repo+'CROSS_COHORT/'+set_outcome[0]+'/Predictors_Average_Ranking_'+set_outcome[0]+'_'+set_outcome[1]+'.csv')
tbl_predictors_rank_avg.to_csv(file_repo+'CROSS_COHORT/'+set_outcome[1]+'/Predictors_Average_Ranking_'+set_outcome[0]+'_'+set_outcome[1]+'.csv')