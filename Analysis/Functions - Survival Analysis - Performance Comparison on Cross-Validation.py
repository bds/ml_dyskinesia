#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load data for performance comparison    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_models_avg_cv_comparison(ana_type = "") :

    model_vars_comb = dict()
    best_ml_model_comb = dict()

    for loop_file_repo in file_repo_prefix_loop :
        print('loop_file_repo : '+ loop_file_repo)

        model_vars_load = dict()
        best_ml_model_load = dict()

        for loop_ml_prefix in ml_prefix :
            print('loop_ml_prefix : '+ loop_ml_prefix)

            file_repo_prefix = loop_file_repo
            print('Outcome: '+ set_outcome + ' ; Cohort: ' + file_repo_prefix + ' ; Model Type: ' + loop_ml_prefix)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_vars = dict()
            best_ml_model = dict()

            for ml_loop in range(len(ls_ml_models)) :

                ml_model_prefix = ls_ml_models[ml_loop] + loop_ml_prefix

                if (ana_type == "_unnormalized") & (loop_file_repo in file_repo_prefix_multi) :
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_cIndex']

                    # Unnormalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_cIndex': [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected
                    
                    # Optimal unnormalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select
                
                elif (ana_type == "_normalized") & (loop_file_repo in file_repo_prefix_multi):
                    # Load the model
                    ml_loaded = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    ml_loaded_name = ml_loaded['model_name']
                    ml_loaded_avg_score_cv = ml_loaded['model_avg_cIndex']

                    # Normalized model
                    idx_ml_loaded_name_select = [x for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    ml_loaded_name_select = [ml_loaded_name[x] for x in range(len(ml_loaded_name)) if 'none' not in ml_loaded_name[x]]
                    list_avg_score_cv = [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                    idx_optimal = list_avg_score_cv.index( max(list_avg_score_cv) )
                    ml_selected = dict({'model_name': [ml_loaded['model_name'][x] for x in idx_ml_loaded_name_select]
                                        ,'num_vars': [ml_loaded['num_vars'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_avg_cIndex': [ml_loaded['model_avg_cIndex'][x] for x in idx_ml_loaded_name_select]
                                        ,'model_selected_features': [ml_loaded['model_selected_features'][x] for x in idx_ml_loaded_name_select]
                                        ,'idx_optimal': idx_optimal
                                        })
                    model_vars[ ls_ml_models[ml_loop] ] = ml_selected

                    # Optimal Normalized model
                    best_ml_loaded = load_object(filename = "model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
                    best_ml_loaded_keys = [list(best_ml_loaded[x].keys())[0] for x in range(len(best_ml_loaded))]
                    idx_best_ml_loaded = best_ml_loaded_keys.index(ml_loaded_name_select[idx_optimal])
                    best_ml_select = best_ml_loaded[idx_best_ml_loaded][ ml_loaded_name_select[idx_optimal] ]
                    best_ml_model[ ls_ml_models[ml_loop] ] = best_ml_select
                
                else :
                    # List of 'model_name', 'num_vars', 'model_avg_cIndex', 'model_selected_features', 'idx_optimal' of the algorithm
                    model_vars[ ls_ml_models[ml_loop] ] = load_object(filename = "compact_ml_vars_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

                    # Model with highest average C-index score
                    best_ml_model[ ls_ml_models[ml_loop] ] = load_object(filename = "ml_model_"+ml_model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
            

            model_vars_load[loop_ml_prefix] = model_vars
            best_ml_model_load[loop_ml_prefix] = best_ml_model
        
        model_vars_comb[loop_file_repo] = model_vars_load
        best_ml_model_comb[loop_file_repo] = best_ml_model_load
    
    return({'model_vars' : model_vars_comb
            ,'best_ml_model' : best_ml_model_comb })



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Feature selected of the optimal compact model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def avg_cv_performance_comparison( repo_prefix
                                  ,fig_repo_out
                                  ,fig_prefix
                                  ,fig_prefix_add
                                  ,list_ml_models
                                  ):
     
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Import HTML file of the best selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    list_optimal_cv_score = []
    list_optimal_avg_cv_score = []
    for repo_prefix_loop in repo_prefix :

        ls_optimal_cv_score = []
        ls_optimal_avg_cv_score = []
        for list_ml_models_loop in list_ml_models :
            
            # To load the model
            ls_model_vars = ds_perm_avg_cv_compare['model_vars'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]
            ls_best_ml_model = ds_perm_avg_cv_compare['best_ml_model'][repo_prefix_loop][loop_ml_prefix][list_ml_models_loop]

            # Cross-validated performance metric of the optimal model
            optimal_cv_score = ls_best_ml_model.all_model_cIndex_cv[0]['kfold_cIndex']
            ls_optimal_cv_score.append( optimal_cv_score )

            # Average of cross-validated performance metric of the optimal model
            optimal_avg_cv_score = ls_best_ml_model.best_model_cIndex_test
            ls_optimal_avg_cv_score.append( optimal_avg_cv_score )
            
        # Transform ls_optimal_cv_score into array
        array_optimal_cv_score = np.array(ls_optimal_cv_score)
        
        # Optimal model with highest average performance metrics
        idx_best_model = ls_optimal_avg_cv_score.index( max(ls_optimal_avg_cv_score) )
        list_optimal_cv_score.append( ls_optimal_cv_score[idx_best_model] )
        list_optimal_avg_cv_score.append( ls_optimal_avg_cv_score[idx_best_model] )

        # Comparing predictive performance by using Bayesian Signed-Rank Test for each models in the same cohort
        comparison_results = []
        comparison_models = []
        comparison_probs = []
        # To compare the cross-validated performance metrics of 2 models
        for i in range(len(array_optimal_cv_score)):
            for j in range(len(array_optimal_cv_score)):
                print('i vs j: '+ str(i) + ' vs ' + str(j))

                # Comparison the performance metric of model_i and model_j
                results = two_on_multiple(array_optimal_cv_score[i], array_optimal_cv_score[j])
                comparison_results.append(results)

                # Model_i or Model_j has better performance metric based on Bayesian probability
                if (results[0] > results[2]) & (i > j) :
                    compare_results = 1
                    compare_probs = results[0]
                elif (results[0] < results[2]) & (i > j) :
                    compare_results = 2
                    compare_probs = results[2]
                else :
                    compare_results = 0
                    compare_probs = results[1]
                
                comparison_models.append(compare_results)
                comparison_probs.append(compare_probs)
        
        # To transform the list into n x n array
        num_ml_models = len(list_ml_models)
        comparison_models_array = np.array(comparison_models).reshape((num_ml_models, num_ml_models))
        comparison_probs_array = np.array(comparison_probs).reshape((num_ml_models, num_ml_models))

        # Create figure and gridspec layout
        fig = plt.figure(figsize=(10, 10))
        gs = gridspec.GridSpec(2, 1, height_ratios=[6, 4], hspace=0.15)
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])

        # Set white background for the inner plots
        ax1.set_facecolor('white')
        ax2.set_facecolor('white')

        # Create boxplot of cross-validated C-index scores
        boxplot = ax1.boxplot(array_optimal_cv_score.T, patch_artist=True)
        colors = 'lightblue'  # Using a single color for all boxes
        for patch in boxplot['boxes']:
            patch.set_facecolor(colors)

        # Remove x-axis tick labels
        ax1.set_xticks(np.arange(1, comparison_models_array.shape[1] + 1))
        ax1.set_xticklabels( ls_ml_model_name2 )

        # ax1.set_xticklabels([f'Model {i+1}' for i in range(array_optimal_cv_score.shape[0])])
        ax1.set_ylabel('Cross-validated C-index')

        # Create markers plot based on comparison_models_array and comparison_probs_array
        for i in range(1, comparison_models_array.shape[0]):
            for j in range(comparison_models_array.shape[1]):
                model_idx = comparison_models_array[i, j]
                if i > j:
                    prob_val = f'{comparison_probs_array[i, j]:.2f}'
                else:
                    prob_val = ''
                marker = '<' if model_idx == 1 else '^' if model_idx == 2 else ','
                color = 'red' if model_idx == 1 else 'green' if model_idx == 2 else 'none'
                if marker:
                    ax2.scatter(j, comparison_models_array.shape[0] - i, marker=marker, color=color, s=100)
                    ax2.text(j+0.3, comparison_models_array.shape[0] - i - 0.2, prob_val, ha='center', va='bottom', fontsize=12)

        ax2.set_xticks([])
        ax2.set_xticklabels([])

        ax2.set_yticks(np.arange(1, comparison_models_array.shape[0]))
        ax2.set_yticklabels( [ls_ml_model_name[len(ls_ml_model_name) - lbl_i - 1] for lbl_i in range(len(ls_ml_model_name)-1)] )

        # Add box around both plots with black line
        for ax in [ax1, ax2]:
            for spine in ax.spines.values():
                spine.set_visible(True)
                spine.set_color('black')  # Set box color
                spine.set_linewidth(0.5)  # Set box line width

        plt.tight_layout()
        plt.savefig(file_repo+repo_prefix_loop+set_outcome+'/Comparison_CV_C_Index'+fig_prefix+fig_prefix_add+'.png', bbox_inches='tight', dpi=300)


    # Comparing predictive performance by using Bayesian Signed-Rank Test for each models across cohorts
    comparison_results = []
    comparison_models = []
    comparison_probs = []

    # Transform ls_optimal_cv_score into array
    array_optimal_cv_score = np.array(list_optimal_cv_score)
    
    # To compare the cross-validated performance metrics of 2 models
    for i in range(len(array_optimal_cv_score)):
        for j in range(len(array_optimal_cv_score)):
            print('i vs j: '+ str(i) + ' vs ' + str(j))

            # Comparison the performance metric of model_i and model_j
            results = two_on_multiple(array_optimal_cv_score[i], array_optimal_cv_score[j])
            comparison_results.append(results)

            # Model_i or Model_j has better performance metric based on Bayesian probability
            if (results[0] > results[2]) & (i > j) :
                compare_results = 1
                compare_probs = results[0]
            elif (results[0] < results[2]) & (i > j) :
                compare_results = 2
                compare_probs = results[2]
            else :
                compare_results = 0
                compare_probs = results[1]
            
            comparison_models.append(compare_results)
            comparison_probs.append(compare_probs)
    
    # To transform the list into n x n array
    num_ml_models = len(repo_prefix)
    comparison_models_array = np.array(comparison_models).reshape((num_ml_models, num_ml_models))
    comparison_probs_array = np.array(comparison_probs).reshape((num_ml_models, num_ml_models))

    # Create figure and gridspec layout
    fig = plt.figure(figsize=(10, 10))
    gs = gridspec.GridSpec(2, 1, height_ratios=[6, 4], hspace=0.15)
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])

    # Set white background for the inner plots
    ax1.set_facecolor('white')
    ax2.set_facecolor('white')

    # Create boxplot of cross-validated C-index
    boxplot = ax1.boxplot(array_optimal_cv_score.T, patch_artist=True)
    colors = 'lightblue'  # Using a single color for all boxes
    for patch in boxplot['boxes']:
        patch.set_facecolor(colors)

    # Remove x-axis tick labels
    ax1.set_xticks(np.arange(1, comparison_models_array.shape[1] + 1))
    ax1.set_xticklabels( file_repo_prefix_name2 )

    # ax1.set_xticklabels([f'Model {i+1}' for i in range(array_optimal_cv_score.shape[0])])
    ax1.set_ylabel('Cross-validated C-index')

    # Create markers plot based on comparison_models_array and comparison_probs_array
    for i in range(1, comparison_models_array.shape[0]):
        for j in range(comparison_models_array.shape[1]):
            model_idx = comparison_models_array[i, j]
            if i > j:
                prob_val = f'{comparison_probs_array[i, j]:.2f}'
            else:
                prob_val = ''
            marker = '<' if model_idx == 1 else '^' if model_idx == 2 else ','
            color = 'red' if model_idx == 1 else 'green' if model_idx == 2 else 'none'
            if marker:
                ax2.scatter(j, comparison_models_array.shape[0] - i, marker=marker, color=color, s=100)
                ax2.text(j+0.3, comparison_models_array.shape[0] - i - 0.2, prob_val, ha='center', va='bottom', fontsize=12)

    ax2.set_xticks([])
    ax2.set_xticklabels([])

    ax2.set_yticks(np.arange(1, comparison_models_array.shape[0]))
    ax2.set_yticklabels( [file_repo_prefix_name[len(file_repo_prefix_name) - lbl_i - 1] for lbl_i in range(len(file_repo_prefix_name)-1)] )

    # Add box around both plots with black line
    for ax in [ax1, ax2]:
        for spine in ax.spines.values():
            spine.set_visible(True)
            spine.set_color('black')  # Set box color
            spine.set_linewidth(0.5)  # Set box line width

    plt.savefig(fig_repo_out+'/Comparison_CV_C_Index_Across_Cohorts'+fig_prefix+fig_prefix_add+'.png', bbox_inches='tight', dpi=300)
    
