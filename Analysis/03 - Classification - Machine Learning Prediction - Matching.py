#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Categorical variables to be encoded    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_category = data_variables[data_variables['Variable Type'].isin(['Ordinal', 'Nominal'])]['PPMI Variable']
vars_category = [ x for x in vars_category if x in data_info[data_info['Type'] == 'Baseline']['Variable'].tolist() ]

# Not to perform one-hot encoding in CatBoost
if model_set == 'catboost':
    vars_category = []


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To perform the model prediction    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Function - Machine Learning Classification
def func_ml_model_class(i) :
    
    mtd_name = ls_mtd_name[i]
    print('Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name)

    model_class = dict()

    ml_model_class = ml_classification( data_repo = file_data+file_repo_prefix+set_outcome+'/'+ls_mtd_normalized[i]+'/'
                                        ,vars_ml = vars_ml_set
                                        ,outcome = set_outcome
                                        ,normalize_method = ls_mtd_normalized[i]
                                        ,n_jobs = n_jobs
                                        ,ml_model_set = model_set
                                        ,max_features = [1, 2, 3, 4, 5, 10, 15, 20]
                                        ,undersampling = False
                                        ,matching = ls_mtd_matching[i]
                                        ,feature_selection = ls_mtd_feat_selection[i]
                                        ,vars_encoding = vars_category )
    ml_model_class.classifier()
    model_class[mtd_name] = ml_model_class

    return(model_class)


# To Perform Machine Learning Classification
time_start = datetime.now()
model_classifier = Parallel(n_jobs = n_jobs)(
    delayed( func_ml_model_class )(i = i)
    for i in range(len(ls_mtd_normalized)) )
time_end = datetime.now()
print("Total running time: " + str(time_end - time_start))

save_object(data = model_classifier, filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

model_classifier = load_object(filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
