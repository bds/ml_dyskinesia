#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                      Loading PPMI Data                                                                      ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


def preprocess_data_ppmi():
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Subject Characteristics    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Patient Status
    ppmi_patient_status = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Patient Status/Participant_Status.csv")

    # Genetic Status
    ppmi_genetic_status = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Genetic Status/iu_genetic_consensus_20220310.csv")

    # Subject Demographics
    ppmi_cohort_history = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Subject Demographics/Subject_Cohort_History.csv")
    ppmi_age_at_visit = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Subject Demographics/Age_at_visit.csv")
    ppmi_demographics = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Subject Demographics/Demographics.csv")
    ppmi_socio_economics = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Subject Demographics/Socio-Economics.csv")

    # Family History
    ppmi_family_history = pd.read_csv("Analysis/Data/PPMI/Subject Characteristics/Family History/Family_History.csv")


    #~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Biospecimen    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Biosample Inventory
    ppmi_iusm_assay = pd.read_csv("Analysis/Data/PPMI/Biospecimen/Biosample Inventory/IUSM_ASSAY_DEV_CATALOG.csv")

    # Lab Collection Procedures
    ppmi_blood_chemistry = pd.read_csv("Analysis/Data/PPMI/Biospecimen/Lab Collection Procedures/Blood_Chemistry___Hematology.csv")
    ppmi_lumbar_puncture = pd.read_csv("Analysis/Data/PPMI/Biospecimen/Lab Collection Procedures/Lumbar_Puncture.csv")
    ppmi_research_biospecimens = pd.read_csv("Analysis/Data/PPMI/Biospecimen/Lab Collection Procedures/Research_Biospecimens.csv")


    #~~~~~~~~~~~~~~~~~~~~~#
    ####    Imaging    ####
    #~~~~~~~~~~~~~~~~~~~~~#

    # DatSCAN Imaging
    ppmi_datscan_analysis = pd.read_csv("Analysis/Data/PPMI/Imaging/DatSCAN Imaging/DaTScan_Analysis.csv")
    ppmi_datscan_visual = pd.read_csv("Analysis/Data/PPMI/Imaging/DatSCAN Imaging/DaTScan_Visual_Interpretation_Results.csv")

    # Diffusion Imaging
    ppmi_dti_regions = pd.read_csv("Analysis/Data/PPMI/Imaging/Diffusion Imaging/DTI_Regions_of_Interest.csv")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Medical History    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Medical
    ppmi_concomitant_medication = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Concomitant_Medication_Log.csv")
    ppmi_freezing_falls = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Determination_of_Freezing_and_Falls.csv")
    ppmi_parkinsonism = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Features_of_Parkinsonism.csv")
    ppmi_general_physical_exam = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/General_Physical_Exam.csv")
    ppmi_ledd_medication = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/LEDD_Concomitant_Medication_Log.csv")
    ppmi_clinical_features = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Other_Clinical_Features.csv")
    ppmi_pd_surgery = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Surgery_for_PD_Log.csv")
    ppmi_vital_signs = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/Vital_Signs.csv")
    ppmi_pd_diagnosis = pd.read_csv("Analysis/Data/PPMI/Medical History/Medical/PD_Diagnosis_History.csv")

    # Neurological Exam
    ppmi_neurological_exam = pd.read_csv("Analysis/Data/PPMI/Medical History/Neurological Exam/Neurological_Exam.csv")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Assessments    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # MDS-UPDRS
    ppmi_mds_1 = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/MDS-UPDRS_Part_I.csv")
    ppmi_mds_1_2 = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/MDS-UPDRS_Part_I_Patient_Questionnaire.csv")
    ppmi_mds_2 = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/MDS_UPDRS_Part_II__Patient_Questionnaire.csv")
    ppmi_mds_3 = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/MDS_UPDRS_Part_III.csv")
    ppmi_mds_4 = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/MDS-UPDRS_Part_IV__Motor_Complications.csv")
    ppmi_schwab_england = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/Modified_Schwab___England_Activities_of_Daily_Living.csv")
    ppmi_qol_lower = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/Neuro_QoL__Lower_Extremity_Function__Mobility__-_Short_Form.csv")
    ppmi_qol_upper = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/Neuro_QoL__Upper_Extremity_Function_-_Short_Form.csv")
    ppmi_motor_function = pd.read_csv("Analysis/Data/PPMI/Motor Assessments/MDS-UPDRS/Participant_Motor_Function_Questionnaire.csv")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Non-Motor Assessments    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Autonomic Tests
    ppmi_scopa_aut = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Autonomic Tests/SCOPA-AUT.csv")

    # Cognition
    ppmi_cognitive_cat = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Cognition/Cognitive_Categorization.csv")
    ppmi_trail_making = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Cognition/Trail_Making_A_and_B.csv")

    # Neurobehavioral Tests
    ppmi_geriatric_depression = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neurobehavioral Tests/Geriatric_Depression_Scale__Short_Version_.csv")
    ppmi_quip = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neurobehavioral Tests/QUIP-Current-Short.csv")

    # Neuropsychological Tests
    ppmi_benton = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Benton_Judgement_of_Line_Orientation.csv")
    ppmi_hopkins = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Hopkins_Verbal_Learning_Test_-_Revised.csv")
    ppmi_lns = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Letter_-_Number_Sequencing.csv")
    ppmi_semantic_fluency = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Modified_Semantic_Fluency.csv")
    ppmi_moca = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Montreal_Cognitive_Assessment__MoCA_.csv")
    ppmi_sdm = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Neuropsychological Tests/Symbol_Digit_Modalities_Test.csv")

    # Olfactory Tests
    ppmi_upsit = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Olfactory Tests/University_of_Pennsylvania_Smell_Identification_Test__UPSIT_.csv")

    # Sleep Disorder Tests
    ppmi_epworth = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Sleep Disorder Tests/Epworth_Sleepiness_Scale.csv")
    ppmi_rem = pd.read_csv("Analysis/Data/PPMI/Non-motor Assessments/Sleep Disorder Tests/REM_Sleep_Behavior_Disorder_Questionnaire.csv")

    # Follow Up of Persons with Neurologic Disease
    ppmi_alcohol = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Alcohol.csv")
    ppmi_caffeine = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Caffeine.csv")
    ppmi_head_injury = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Head_Injury.csv")
    ppmi_pesticides_work = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Pesticides_at_Work.csv")
    ppmi_pesticides_non_work = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Pesticides_Non-Work.csv")
    ppmi_smoking = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Smoking_History.csv")
    ppmi_toxicant = pd.read_csv("Analysis/Data/PPMI/PPMI FOUND/Follow Up of Persons with Neurologic Disease/FOUND_RFQ_Toxicant_History.csv")

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Data Collected in Prior EDC    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ppmi_telephone_fu = pd.read_csv("Analysis/Data/PPMI/Data Collected in Prior EDC/Telephone_Follow-up-Archived.csv")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                                                                      Processing PPMI Data                                                                      ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Visiting Date    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vars_infodate = ['PATNO', 'EVENT_ID', 'INFODT']
    data_infodate = pd.concat( [ppmi_demographics[vars_infodate]
                                ,ppmi_family_history[vars_infodate]
                                ,ppmi_socio_economics[vars_infodate]
                                ,ppmi_family_history[vars_infodate]
                                ,ppmi_concomitant_medication[vars_infodate]
                                ,ppmi_freezing_falls[vars_infodate]
                                ,ppmi_parkinsonism[vars_infodate]
                                ,ppmi_general_physical_exam[vars_infodate]
                                ,ppmi_ledd_medication[vars_infodate]
                                ,ppmi_clinical_features[vars_infodate]
                                ,ppmi_pd_surgery[vars_infodate]
                                ,ppmi_vital_signs[vars_infodate]
                                ,ppmi_neurological_exam[vars_infodate]
                                ,ppmi_mds_1[vars_infodate]
                                ,ppmi_mds_1_2[vars_infodate]
                                ,ppmi_mds_2[vars_infodate]
                                ,ppmi_mds_3[vars_infodate]
                                ,ppmi_mds_4[vars_infodate]
                                ,ppmi_schwab_england[vars_infodate]
                                ,ppmi_qol_lower[vars_infodate]
                                ,ppmi_qol_upper[vars_infodate]
                                ,ppmi_motor_function[vars_infodate]
                                ,ppmi_scopa_aut[vars_infodate]
                                ,ppmi_cognitive_cat[vars_infodate]
                                ,ppmi_trail_making[vars_infodate]
                                ,ppmi_geriatric_depression[vars_infodate]
                                ,ppmi_quip[vars_infodate]
                                ,ppmi_benton[vars_infodate]
                                ,ppmi_hopkins[vars_infodate]
                                ,ppmi_lns[vars_infodate]
                                ,ppmi_semantic_fluency[vars_infodate]
                                ,ppmi_moca[vars_infodate]
                                ,ppmi_sdm[vars_infodate]
                                ,ppmi_upsit[vars_infodate]
                                ,ppmi_epworth[vars_infodate]
                                ,ppmi_rem[vars_infodate]], axis = 0)
    data_infodate = data_infodate.drop_duplicates()
    data_infodate['visit_date'] = pd.to_datetime('01/' + data_infodate['INFODT'])  # format = '%b%Y'
    data_infodate = data_infodate.groupby(['PATNO', 'EVENT_ID'])['visit_date'].min().reset_index()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Subject Characteristics    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # PATNO, COHORT, COHORT_DEFINITION, ENROLL_DATE, ENROLL_AGE, CONCOHORT, CONCOHORT_DEFINITION, ENRLPINK1, ENRLPRKN, ENRLSRDC, ENRLHPSM, ENRLRBD, ENRLLRRK2, ENRLSNCA, ENRLGBA
    ppmi_patient_status = ppmi_patient_status[~ppmi_patient_status['ENROLL_STATUS'].isin(['Excluded', 'Withdrew', 'Declined', 'declined', 'excluded', 'screen_failed', 'withdrew', 'screened'])]
    data_ppmi = ppmi_patient_status[['PATNO', 'COHORT', 'COHORT_DEFINITION', 'ENROLL_DATE', 'ENROLL_AGE', 'CONCOHORT', 'CONCOHORT_DEFINITION'
                                    ,'ENRLPINK1', 'ENRLPRKN', 'ENRLSRDC', 'ENRLHPSM', 'ENRLRBD', 'ENRLLRRK2', 'ENRLSNCA', 'ENRLGBA']]
    
    # ENROLL_DATE
    data_ppmi['ENROLL_DATE'] = pd.to_datetime('01/'+data_ppmi['ENROLL_DATE'], format = '%d/%m/%Y')

    # + APPRDX
    # data_ppmi = pd.merge(data_ppmi, ppmi_cohort_history[['PATNO', 'APPRDX']], how = 'left', on = ['PATNO'])

    # + GBA_POS, LRRK2_POS
    data_ppmi = pd.merge(data_ppmi, ppmi_genetic_status[['PATNO', 'GBA_POS', 'LRRK2_POS']], how = 'left', on = ['PATNO'])

    # + BIRTHDT, SEX, HANDED
    data_ppmi = pd.merge(data_ppmi, ppmi_demographics[['PATNO', 'BIRTHDT', 'SEX', 'HANDED']], how = 'left', on = ['PATNO'])

    # + EVENT_ID
    list_event_id = ['BL', 'R01', 'R04', 'R06', 'R08', 'R10', 'R12', 'R13', 'R14', 'R15', 'R16', 'R17', 'R18', 'R19'
                    ,'SC', 'V02', 'V04', 'V05', 'V06', 'V08', 'V10', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20']
    list_patno = data_ppmi['PATNO'].tolist()
    list_patno = list_patno * len(list_event_id)
    list_event_id = list_event_id * data_ppmi.shape[0]
    list_event_id.sort()
    data_event_id = pd.DataFrame({'PATNO': list_patno, 'EVENT_ID': list_event_id})
    data_ppmi = pd.merge(data_ppmi, data_event_id, how = 'left', on = ['PATNO'])
    data_ppmi = pd.merge(data_ppmi, data_infodate, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + EDUCYRS
    data_ppmi = pd.merge(data_ppmi, ppmi_socio_economics[['PATNO', 'EVENT_ID', 'EDUCYRS']], how = 'left', on = ['PATNO', 'EVENT_ID'])
    ppmi_socio_economics2 = ppmi_socio_economics[ppmi_socio_economics['EVENT_ID'].isin(['BL', 'SC'])][['PATNO', 'EDUCYRS']].groupby(['PATNO'])['EDUCYRS'].max().reset_index().rename(columns={'EDUCYRS': 'EDUCYRS2'})
    data_ppmi = pd.merge(data_ppmi, ppmi_socio_economics2, how = 'left', on = 'PATNO')
    data_ppmi['EDUCYRS'] = np.where(data_ppmi['EDUCYRS'].isnull(), data_ppmi['EDUCYRS2'], data_ppmi['EDUCYRS'])
    data_ppmi.drop(['EDUCYRS2'], axis = 1, inplace = True)

    # + ANYFAMPD
    ppmi_family_history = ppmi_family_history.groupby(['PATNO'])['ANYFAMPD'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_family_history, how = 'left', on = ['PATNO'])


    #~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Biospecimen    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~#

    ppmi_iusm_assay = ppmi_iusm_assay.groupby(['ALIAS_ID', 'CLINICAL_EVENT', 'TYPE'])['AVERAGE_HEMOGLOBIN'].mean().reset_index()
    ppmi_iusm_assay_cerebrospinal = ppmi_iusm_assay[ppmi_iusm_assay['TYPE'] == 'Cerebrospinal Fluid'][['ALIAS_ID', 'CLINICAL_EVENT', 'AVERAGE_HEMOGLOBIN']].rename(columns={'ALIAS_ID': 'PATNO', 'CLINICAL_EVENT': 'EVENT_ID', 'AVERAGE_HEMOGLOBIN': 'AVERAGE_HEMOGLOBIN_CEREBROSPINAL'})
    ppmi_iusm_assay_serum = ppmi_iusm_assay[ppmi_iusm_assay['TYPE'] == 'Serum'][['ALIAS_ID', 'CLINICAL_EVENT', 'AVERAGE_HEMOGLOBIN']].rename(columns={'ALIAS_ID': 'PATNO', 'CLINICAL_EVENT': 'EVENT_ID', 'AVERAGE_HEMOGLOBIN': 'AVERAGE_HEMOGLOBIN_SERUM'})
    ppmi_iusm_assay_plasma = ppmi_iusm_assay[ppmi_iusm_assay['TYPE'] == 'Plasma'][['ALIAS_ID', 'CLINICAL_EVENT', 'AVERAGE_HEMOGLOBIN']].rename(columns={'ALIAS_ID': 'PATNO', 'CLINICAL_EVENT': 'EVENT_ID', 'AVERAGE_HEMOGLOBIN': 'AVERAGE_HEMOGLOBIN_PLASMA'})

    # + AVERAGE_HEMOGLOBIN_CEREBROSPINAL
    data_ppmi = pd.merge(data_ppmi, ppmi_iusm_assay_cerebrospinal, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + AVERAGE_HEMOGLOBIN_SERUM
    data_ppmi = pd.merge(data_ppmi, ppmi_iusm_assay_serum, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + AVERAGE_HEMOGLOBIN_PLASMA
    data_ppmi = pd.merge(data_ppmi, ppmi_iusm_assay_plasma, how = 'left', on = ['PATNO', 'EVENT_ID'])



    ppmi_blood_chemistry = ppmi_blood_chemistry[['PATNO', 'EVENT_ID', 'LTSTNAME', 'LSIRES']]
    ppmi_blood_chemistry = ppmi_blood_chemistry[~ppmi_blood_chemistry['LSIRES'].isin( ['<0.6', '<3', '<4', 'Anisocytosis', 'Borderline', 'Dohle bodies p', 'Macrocytic', 'Microcytic'
                                                                                        ,'Negative', 'No Review Requ', 'No Review Required', 'Normocytic', 'Not ident. w/i'
                                                                                        ,'Poikilocytosis', 'Polychromatoph', 'Repeat', 'Smudge cells'
                                                                                        ,'Target Cells', 'Target Cells Present', 'Unscheduled', 'Yes'] )]
    ppmi_blood_chemistry['LSIRES'] = ppmi_blood_chemistry['LSIRES'].map(float)
    ppmi_blood_chemistry = ppmi_blood_chemistry.groupby(['PATNO', 'EVENT_ID', 'LTSTNAME'])['LSIRES'].mean().reset_index()

    # + LSIRES_ALT
    ppmi_blood_chemistry_1 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'ALT (SGPT)'].rename(columns = {'LSIRES': 'LSIRES_ALT'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_1, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_APTT
    ppmi_blood_chemistry_2 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'APTT-QT'].rename(columns = {'LSIRES': 'LSIRES_APTT'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_2, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_AST
    ppmi_blood_chemistry_3 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'AST (SGOT)'].rename(columns = {'LSIRES': 'LSIRES_AST'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_3, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_albumin
    ppmi_blood_chemistry_4 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Albumin-QT'].rename(columns = {'LSIRES': 'LSIRES_albumin'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_4, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_alkaline
    ppmi_blood_chemistry_5 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Alkaline Phosphatase-QT'].rename(columns = {'LSIRES': 'LSIRES_alkaline'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_5, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_basophils
    ppmi_blood_chemistry_6 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Basophils'].rename(columns = {'LSIRES': 'LSIRES_basophils'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_6, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_basophils_pct
    ppmi_blood_chemistry_7 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Basophils (%)'].rename(columns = {'LSIRES': 'LSIRES_basophils_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_7, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_calcium
    ppmi_blood_chemistry_8 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Calcium (EDTA)'].rename(columns = {'LSIRES': 'LSIRES_calcium'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_8, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_creatinine
    ppmi_blood_chemistry_9 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Creatinine (Rate Blanked)'].rename(columns = {'LSIRES': 'LSIRES_creatinine'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_9, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_eosinophils
    ppmi_blood_chemistry_10 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Eosinophils'].rename(columns = {'LSIRES': 'LSIRES_eosinophils'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_10, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_eosinophils_pct
    ppmi_blood_chemistry_11 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Eosinophils (%)'].rename(columns = {'LSIRES': 'LSIRES_eosinophils_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_11, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_hematocrit
    ppmi_blood_chemistry_12 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Hematocrit'].rename(columns = {'LSIRES': 'LSIRES_hematocrit'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_12, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_hemoglobin
    ppmi_blood_chemistry_13 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Hemoglobin'].rename(columns = {'LSIRES': 'LSIRES_hemoglobin'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_13, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_lymphocytes
    ppmi_blood_chemistry_14 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Lymphocytes'].rename(columns = {'LSIRES': 'LSIRES_lymphocytes'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_14, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_lymphocytes_pct
    ppmi_blood_chemistry_15 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Lymphocytes (%)'].rename(columns = {'LSIRES': 'LSIRES_lymphocytes_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_15, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_monocytes
    ppmi_blood_chemistry_16 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Monocytes'].rename(columns = {'LSIRES': 'LSIRES_monocytes'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_16, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_monocytes_pct
    ppmi_blood_chemistry_17 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Monocytes (%)'].rename(columns = {'LSIRES': 'LSIRES_monocytes_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_17, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_neutrophils
    ppmi_blood_chemistry_18 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Neutrophils'].rename(columns = {'LSIRES': 'LSIRES_neutrophils'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_18, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_neutrophils_pct
    ppmi_blood_chemistry_19 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Neutrophils (%)'].rename(columns = {'LSIRES': 'LSIRES_neutrophils_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_19, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_platelets
    ppmi_blood_chemistry_20 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Platelets'].rename(columns = {'LSIRES': 'LSIRES_platelets'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_20, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_prothrombin
    ppmi_blood_chemistry_21 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Prothrombin Time'].rename(columns = {'LSIRES': 'LSIRES_prothrombin'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_21, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_rbc
    ppmi_blood_chemistry_22 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'RBC'].rename(columns = {'LSIRES': 'LSIRES_rbc'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_22, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_rbc_morphology
    ppmi_blood_chemistry_23 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'RBC Morphology'].rename(columns = {'LSIRES': 'LSIRES_rbc_morphology'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_23, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_bicarbonate
    ppmi_blood_chemistry_24 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Bicarbonate'].rename(columns = {'LSIRES': 'LSIRES_serum_bicarbonate'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_24, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_chloride
    ppmi_blood_chemistry_25 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Chloride'].rename(columns = {'LSIRES': 'LSIRES_serum_chloride'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_25, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_glucose
    ppmi_blood_chemistry_26 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Glucose'].rename(columns = {'LSIRES': 'LSIRES_serum_glucose'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_26, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_potassium
    ppmi_blood_chemistry_27 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Potassium'].rename(columns = {'LSIRES': 'LSIRES_serum_potassium'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_27, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_sodium
    ppmi_blood_chemistry_28 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Sodium'].rename(columns = {'LSIRES': 'LSIRES_serum_sodium'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_28, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_serum_uric
    ppmi_blood_chemistry_29 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Serum Uric Acid'].rename(columns = {'LSIRES': 'LSIRES_serum_uric'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_29, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_total_bilirubin
    ppmi_blood_chemistry_30 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Total Bilirubin'].rename(columns = {'LSIRES': 'LSIRES_total_bilirubin'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_30, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_total_protein
    ppmi_blood_chemistry_31 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Total Protein'].rename(columns = {'LSIRES': 'LSIRES_total_protein'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_31, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_urea_nitrogen
    ppmi_blood_chemistry_32 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Urea Nitrogen'].rename(columns = {'LSIRES': 'LSIRES_urea_nitrogen'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_32, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_wbc
    ppmi_blood_chemistry_33 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'WBC'].rename(columns = {'LSIRES': 'LSIRES_wbc'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_33, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_atypical_lymphocytes
    ppmi_blood_chemistry_34 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Atypical Lymphocytes'].rename(columns = {'LSIRES': 'LSIRES_atypical_lymphocytes'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_34, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_albumin_bcg
    ppmi_blood_chemistry_35 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Albumin-BCG'].rename(columns = {'LSIRES': 'LSIRES_albumin_bcg'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_35, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_creatinine_2dp
    ppmi_blood_chemistry_36 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Creatinine(Rate Blanked)-2dp'].rename(columns = {'LSIRES': 'LSIRES_creatinine_2dp'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_36, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_mcv
    ppmi_blood_chemistry_37 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'MCV'].rename(columns = {'LSIRES': 'LSIRES_mcv'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_37, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_bands
    ppmi_blood_chemistry_38 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Bands'].rename(columns = {'LSIRES': 'LSIRES_bands'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_38, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_bands_pct
    ppmi_blood_chemistry_39 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Bands (%)'].rename(columns = {'LSIRES': 'LSIRES_bands_pct'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_39, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_metamyelocyte
    ppmi_blood_chemistry_40 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Metamyelocyte'].rename(columns = {'LSIRES': 'LSIRES_metamyelocyte'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_40, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_myelocyte
    ppmi_blood_chemistry_41 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Myelocyte'].rename(columns = {'LSIRES': 'LSIRES_myelocyte'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_41, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_bhcg
    ppmi_blood_chemistry_42 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'B-hCG, Quantitative'].rename(columns = {'LSIRES': 'LSIRES_bhcg'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_42, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_aptt_fsl
    ppmi_blood_chemistry_43 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'APTT-FSL'].rename(columns = {'LSIRES': 'LSIRES_aptt_fsl'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_43, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + LSIRES_nucleated_red_blood
    ppmi_blood_chemistry_44 = ppmi_blood_chemistry[ppmi_blood_chemistry['LTSTNAME'] == 'Nucleated Red Blood Cells'].rename(columns = {'LSIRES': 'LSIRES_nucleated_red_blood'}).drop(['LTSTNAME'], axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_blood_chemistry_44, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + WBCRSLT, RBCRSLT, TOPRRSLT, TGLCRSLT
    data_ppmi = pd.merge(data_ppmi, ppmi_lumbar_puncture[['PATNO', 'EVENT_ID', 'WBCRSLT', 'RBCRSLT', 'TOPRRSLT', 'TGLCRSLT']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + UT1SPNRT, PLASPNRT, BSSPNRT
    data_ppmi = pd.merge(data_ppmi, ppmi_research_biospecimens[['PATNO', 'EVENT_ID', 'UT1SPNRT', 'PLASPNRT', 'BSSPNRT']], how = 'left', on = ['PATNO', 'EVENT_ID'])


    #~~~~~~~~~~~~~~~~~~~~~#
    ####    Imaging    ####
    #~~~~~~~~~~~~~~~~~~~~~#

    # + DATSCAN_CAUDATE_R, DATSCAN_CAUDATE_L, DATSCAN_PUTAMEN_R, DATSCAN_PUTAMEN_L, DATSCAN_PUTAMEN_R_ANT, DATSCAN_PUTAMEN_L_ANT
    data_ppmi = pd.merge(data_ppmi, ppmi_datscan_analysis[['PATNO', 'EVENT_ID', 'DATSCAN_CAUDATE_R', 'DATSCAN_CAUDATE_L', 'DATSCAN_PUTAMEN_R', 'DATSCAN_PUTAMEN_L', 'DATSCAN_PUTAMEN_R_ANT', 'DATSCAN_PUTAMEN_L_ANT']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DATSCAN_VISINTRP_num (at baseline)
    ppmi_datscan_visual['DATSCAN_VISINTRP_num'] = np.where(ppmi_datscan_visual['DATSCAN_VISINTRP'] == 'positive', 1, np.where(ppmi_datscan_visual['DATSCAN_VISINTRP'] == 'negative', 0, np.nan))
    ppmi_datscan_visual['DATSCAN_DATE'] = pd.to_datetime('01/'+ppmi_datscan_visual['DATSCAN_DATE'], format = '%d/%m/%Y')
    ppmi_datscan_visual.rename(columns = {'DATSCAN_DATE': 'ENROLL_DATE'}, inplace = True)
    ppmi_datscan_visual = pd.merge(ppmi_datscan_visual, data_ppmi[['PATNO', 'ENROLL_DATE']].drop_duplicates(), how = 'inner', on = ['PATNO', 'ENROLL_DATE'])


    data_ppmi = pd.merge(data_ppmi, ppmi_datscan_visual[['PATNO', 'DATSCAN_VISINTRP_num']], how = 'left', on = ['PATNO'])

    # + ROI1, ROI2, ROI3, ROI4, ROI5, ROI6, REF1, REF2
    ppmi_dti_regions = ppmi_dti_regions.groupby(['PATNO'])['ROI1', 'ROI2', 'ROI3', 'ROI4', 'ROI5', 'ROI6', 'REF1', 'REF2'].mean().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_dti_regions[['PATNO', 'ROI1', 'ROI2', 'ROI3', 'ROI4', 'ROI5', 'ROI6', 'REF1', 'REF2']], how = 'left', on = ['PATNO'])


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Medical History    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # + med_anxiety
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 1)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_anxiety'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_anxiety'] = np.where( ~data_ppmi['med_anxiety'].isnull(), data_ppmi['med_anxiety'], 0 )

    # + med_atrial
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 2)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_atrial'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_atrial'] = np.where( ~data_ppmi['med_atrial'].isnull(), data_ppmi['med_atrial'], 0 )

    # + med_benign
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 3)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_benign'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_benign'] = np.where( ~data_ppmi['med_benign'].isnull(), data_ppmi['med_benign'], 0 )

    # + med_cogn_dysfunction
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 4)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_cogn_dysfunction'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_cogn_dysfunction'] = np.where( ~data_ppmi['med_cogn_dysfunction'].isnull(), data_ppmi['med_cogn_dysfunction'], 0 )

    # + med_heart_failure
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 5)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_heart_failure'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_heart_failure'] = np.where( ~data_ppmi['med_heart_failure'].isnull(), data_ppmi['med_heart_failure'], 0 )

    # + med_constipation
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 6)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_constipation'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_constipation'] = np.where( ~data_ppmi['med_constipation'].isnull(), data_ppmi['med_constipation'], 0 )

    # + med_cad
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 7)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_cad'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_cad'] = np.where( ~data_ppmi['med_cad'].isnull(), data_ppmi['med_cad'], 0 )

    # + med_day_sleep
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 8)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_day_sleep'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_day_sleep'] = np.where( ~data_ppmi['med_day_sleep'].isnull(), data_ppmi['med_day_sleep'], 0 )

    # + med_delusions
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 9)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_delusions'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_delusions'] = np.where( ~data_ppmi['med_delusions'].isnull(), data_ppmi['med_delusions'], 0 )

    # + med_depression
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 10)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_depression'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_depression'] = np.where( ~data_ppmi['med_depression'].isnull(), data_ppmi['med_depression'], 0 )

    # + med_diabetes
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 11)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_diabetes'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_diabetes'] = np.where( ~data_ppmi['med_diabetes'].isnull(), data_ppmi['med_diabetes'], 0 )

    # + med_gred
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 12)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_gred'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_gred'] = np.where( ~data_ppmi['med_gred'].isnull(), data_ppmi['med_gred'], 0 )

    # + med_hyperlipidemia
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 13)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_hyperlipidemia'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_hyperlipidemia'] = np.where( ~data_ppmi['med_hyperlipidemia'].isnull(), data_ppmi['med_hyperlipidemia'], 0 )

    # + med_hypertension
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 14)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_hypertension'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_hypertension'] = np.where( ~data_ppmi['med_hypertension'].isnull(), data_ppmi['med_hypertension'], 0 )

    # + med_insomnia
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 15)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_insomnia'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_insomnia'] = np.where( ~data_ppmi['med_insomnia'].isnull(), data_ppmi['med_insomnia'], 0 )

    # + med_nausea
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 16)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_nausea'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_nausea'] = np.where( ~data_ppmi['med_nausea'].isnull(), data_ppmi['med_nausea'], 0 )

    # + med_pain
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 17)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_pain'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_pain'] = np.where( ~data_ppmi['med_pain'].isnull(), data_ppmi['med_pain'], 0 )

    # + med_rem
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 18)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_rem'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_rem'] = np.where( ~data_ppmi['med_rem'].isnull(), data_ppmi['med_rem'], 0 )

    # + med_restless_leg
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 19)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_restless_leg'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_restless_leg'] = np.where( ~data_ppmi['med_restless_leg'].isnull(), data_ppmi['med_restless_leg'], 0 )

    # + med_sexual_dysfunction
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 20)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_sexual_dysfunction'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_sexual_dysfunction'] = np.where( ~data_ppmi['med_sexual_dysfunction'].isnull(), data_ppmi['med_sexual_dysfunction'], 0 )

    # + med_drooling
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 21)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_drooling'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_drooling'] = np.where( ~data_ppmi['med_drooling'].isnull(), data_ppmi['med_drooling'], 0 )

    # + med_supplements
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 22)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_supplements'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_supplements'] = np.where( ~data_ppmi['med_supplements'].isnull(), data_ppmi['med_supplements'], 0 )

    # + med_thyroid
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 23)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_thyroid'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_thyroid'] = np.where( ~data_ppmi['med_thyroid'].isnull(), data_ppmi['med_thyroid'], 0 )

    # + med_vitamins
    ppmi_concomitant_medication_1 = ppmi_concomitant_medication[(ppmi_concomitant_medication['EVENT_ID'] == 'BL') & (ppmi_concomitant_medication['CMINDC'] == 24)][['PATNO']].drop_duplicates()
    ppmi_concomitant_medication_1['med_vitamins'] = 1
    data_ppmi = pd.merge(data_ppmi, ppmi_concomitant_medication_1, how = 'left', on = 'PATNO')
    data_ppmi['med_vitamins'] = np.where( ~data_ppmi['med_vitamins'].isnull(), data_ppmi['med_vitamins'], 0 )

    # + FRZGT1W, FLNFR1W, FRZGT12M, FLNFR12M
    data_ppmi = pd.merge(data_ppmi, ppmi_freezing_falls[['PATNO', 'EVENT_ID', 'FRZGT1W', 'FLNFR1W', 'FRZGT12M', 'FLNFR12M']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + FEATBRADY, FEATPOSINS, FEATRIGID, FEATTREMOR
    ppmi_parkinsonism = ppmi_parkinsonism[['PATNO', 'EVENT_ID', 'FEATBRADY', 'FEATPOSINS', 'FEATRIGID', 'FEATTREMOR']]
    ppmi_parkinsonism['FEATBRADY'] = np.where( ppmi_parkinsonism['FEATBRADY'].isin([0, 1]), ppmi_parkinsonism['FEATBRADY'], np.nan )
    ppmi_parkinsonism['FEATPOSINS'] = np.where( ppmi_parkinsonism['FEATPOSINS'].isin([0, 1]), ppmi_parkinsonism['FEATPOSINS'], np.nan )
    ppmi_parkinsonism['FEATRIGID'] = np.where( ppmi_parkinsonism['FEATRIGID'].isin([0, 1]), ppmi_parkinsonism['FEATRIGID'], np.nan )
    ppmi_parkinsonism['FEATTREMOR'] = np.where( ppmi_parkinsonism['FEATTREMOR'].isin([0, 1]), ppmi_parkinsonism['FEATTREMOR'], np.nan )
    data_ppmi = pd.merge(data_ppmi, ppmi_parkinsonism[['PATNO', 'EVENT_ID', 'FEATBRADY', 'FEATPOSINS', 'FEATRIGID', 'FEATTREMOR']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_skin
    ppmi_general_physical_exam['EVENT_ID'] = np.where( ppmi_general_physical_exam['EVENT_ID'] == 'SC', 'BL', ppmi_general_physical_exam['EVENT_ID'] )

    ppmi_general_physical_exam1 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PECAT'] == 'Skin' ]
    ppmi_general_physical_exam1['physical_exam_skin'] = np.where( ppmi_general_physical_exam1['ABNORM'].isin([0, 1]), ppmi_general_physical_exam1['ABNORM'], np.nan )
    ppmi_general_physical_exam1 = ppmi_general_physical_exam1.groupby(['PATNO', 'EVENT_ID'])['physical_exam_skin'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam1, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_head_neck_lymphatic
    ppmi_general_physical_exam2 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PECAT'] == 'Head/Neck/Lymphatic' ]
    ppmi_general_physical_exam2['physical_exam_head_neck_lymphatic'] = np.where( ppmi_general_physical_exam2['ABNORM'].isin([0, 1]), ppmi_general_physical_exam2['ABNORM'], np.nan )
    ppmi_general_physical_exam2 = ppmi_general_physical_exam2.groupby(['PATNO', 'EVENT_ID'])['physical_exam_head_neck_lymphatic'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam2, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_eyes
    ppmi_general_physical_exam3 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 3 ]
    ppmi_general_physical_exam3['physical_exam_eyes'] = np.where( ppmi_general_physical_exam3['ABNORM'].isin([0, 1]), ppmi_general_physical_exam3['ABNORM'], np.nan )
    ppmi_general_physical_exam3 = ppmi_general_physical_exam3.groupby(['PATNO', 'EVENT_ID'])['physical_exam_eyes'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam3, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_ear_nose_throat
    ppmi_general_physical_exam4 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 4 ]
    ppmi_general_physical_exam4['physical_exam_ear_nose_throat'] = np.where( ppmi_general_physical_exam4['ABNORM'].isin([0, 1]), ppmi_general_physical_exam4['ABNORM'], np.nan )
    ppmi_general_physical_exam4 = ppmi_general_physical_exam4.groupby(['PATNO', 'EVENT_ID'])['physical_exam_ear_nose_throat'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam4, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_lung
    ppmi_general_physical_exam5 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 5 ]
    ppmi_general_physical_exam5['physical_exam_lung'] = np.where( ppmi_general_physical_exam5['ABNORM'].isin([0, 1]), ppmi_general_physical_exam5['ABNORM'], np.nan )
    ppmi_general_physical_exam5 = ppmi_general_physical_exam5.groupby(['PATNO', 'EVENT_ID'])['physical_exam_lung'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam5, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_cardio
    ppmi_general_physical_exam6 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 6 ]
    ppmi_general_physical_exam6['physical_exam_cardio'] = np.where( ppmi_general_physical_exam6['ABNORM'].isin([0, 1]), ppmi_general_physical_exam6['ABNORM'], np.nan )
    ppmi_general_physical_exam6 = ppmi_general_physical_exam6.groupby(['PATNO', 'EVENT_ID'])['physical_exam_cardio'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam6, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_abdomen
    ppmi_general_physical_exam7 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 7 ]
    ppmi_general_physical_exam7['physical_exam_abdomen'] = np.where( ppmi_general_physical_exam7['ABNORM'].isin([0, 1]), ppmi_general_physical_exam7['ABNORM'], np.nan )
    ppmi_general_physical_exam7 = ppmi_general_physical_exam7.groupby(['PATNO', 'EVENT_ID'])['physical_exam_abdomen'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam7, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_musculoskeletal
    ppmi_general_physical_exam8 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 8 ]
    ppmi_general_physical_exam8['physical_exam_musculoskeletal'] = np.where( ppmi_general_physical_exam8['ABNORM'].isin([0, 1]), ppmi_general_physical_exam8['ABNORM'], np.nan )
    ppmi_general_physical_exam8 = ppmi_general_physical_exam8.groupby(['PATNO', 'EVENT_ID'])['physical_exam_musculoskeletal'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam8, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_neurological
    ppmi_general_physical_exam9 = ppmi_general_physical_exam[ (ppmi_general_physical_exam['PESEQ'] == 9) | (ppmi_general_physical_exam['PESEQ'] == 12) ]
    ppmi_general_physical_exam9['physical_exam_neurological'] = np.where( ppmi_general_physical_exam9['ABNORM'].isin([0, 1]), ppmi_general_physical_exam9['ABNORM'], np.nan )
    ppmi_general_physical_exam9 = ppmi_general_physical_exam9.groupby(['PATNO', 'EVENT_ID'])['physical_exam_neurological'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam9, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + physical_exam_psychiatric
    ppmi_general_physical_exam10 = ppmi_general_physical_exam[ ppmi_general_physical_exam['PESEQ'] == 10 ]
    ppmi_general_physical_exam10['physical_exam_psychiatric'] = np.where( ppmi_general_physical_exam10['ABNORM'].isin([0, 1]), ppmi_general_physical_exam10['ABNORM'], np.nan )
    ppmi_general_physical_exam10 = ppmi_general_physical_exam10.groupby(['PATNO', 'EVENT_ID'])['physical_exam_psychiatric'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_general_physical_exam10, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + levodopa_enroll
    list_levodopa = [x for x in np.unique(ppmi_ledd_medication['LEDTRT']).tolist() if ('Levodopa' in x) | ('LEVODOPA' in x) ]
    ppmi_ledd_medication1 = ppmi_ledd_medication.copy()
    ppmi_ledd_medication1['start_date_levodopa'] = pd.to_datetime('01/' + ppmi_ledd_medication1['STARTDT'], format = '%d/%m/%Y')
    ppmi_ledd_medication1['stop_date_levodopa'] = pd.to_datetime('01/' + ppmi_ledd_medication1['STOPDT'], format = '%d/%m/%Y')
    ppmi_ledd_medication1_min = ppmi_ledd_medication1[ppmi_ledd_medication1['LEDTRT'].isin(list_levodopa)].groupby(['PATNO'])[['start_date_levodopa']].min().reset_index()
    ppmi_ledd_medication1_max = ppmi_ledd_medication1[ppmi_ledd_medication1['LEDTRT'].isin(list_levodopa)].groupby(['PATNO'])[['stop_date_levodopa']].max().reset_index()

    ppmi_ledd_medication1_log = pd.merge(data_ppmi[['PATNO', 'ENROLL_DATE']], ppmi_ledd_medication1_min, how = 'left', on = ['PATNO'])
    ppmi_ledd_medication1_log = pd.merge(ppmi_ledd_medication1_log, ppmi_ledd_medication1_max, how = 'left', on = ['PATNO'])
    ppmi_ledd_medication1_log['levodopa_enroll'] = np.where( ((ppmi_ledd_medication1_log['ENROLL_DATE'] >= ppmi_ledd_medication1_log['start_date_levodopa'])
                                                                & (ppmi_ledd_medication1_log['ENROLL_DATE'] <= ppmi_ledd_medication1_log['stop_date_levodopa'])
                                                                & (~ppmi_ledd_medication1_log['start_date_levodopa'].isnull())
                                                                & (~ppmi_ledd_medication1_log['stop_date_levodopa'].isnull()) )
                                                            | ((~ppmi_ledd_medication1_log['start_date_levodopa'].isnull())
                                                                & (ppmi_ledd_medication1_log['stop_date_levodopa'].isnull())
                                                                & (ppmi_ledd_medication1_log['ENROLL_DATE'] >= ppmi_ledd_medication1_log['start_date_levodopa'] ) ), 1, 0 )
    ppmi_ledd_medication1_log_dose = ppmi_ledd_medication1_log.copy()
    ppmi_ledd_medication1_log = ppmi_ledd_medication1_log[['PATNO', 'levodopa_enroll']]
    ppmi_ledd_medication1_log = ppmi_ledd_medication1_log.drop_duplicates()
    ppmi_ledd_medication1_log.reset_index(drop=True, inplace = True)
    data_ppmi = pd.merge(data_ppmi, ppmi_ledd_medication1_log, how = 'left', on = ['PATNO'])

    # levodopa_enroll_dose
    ppmi_ledd_medication1_log_dose = ppmi_ledd_medication1_log_dose[ ppmi_ledd_medication1_log_dose['levodopa_enroll'] == 1 ]
    ppmi_ledd_medication1_log_dose.drop(['levodopa_enroll'], axis = 1, inplace = True)
    ppmi_ledd_medication1_log_dose.drop_duplicates(inplace = True)
    ppmi_ledd_medication1_dose = ppmi_ledd_medication1[ ppmi_ledd_medication1['LEDTRT'].isin(list_levodopa) ][['PATNO', 'start_date_levodopa', 'stop_date_levodopa', 'LEDD']]
    dose_remove = [x for x in ppmi_ledd_medication1_dose['LEDD'].astype('str').tolist() if 'x 0.33' in x]
    ppmi_ledd_medication1_dose['levodopa_enroll_dose'] = np.where(ppmi_ledd_medication1_dose['LEDD'].isin(dose_remove), np.nan, ppmi_ledd_medication1_dose['LEDD'])
    ppmi_ledd_medication1_dose['levodopa_enroll_dose'] = ppmi_ledd_medication1_dose['levodopa_enroll_dose'].map(float)
    ppmi_ledd_medication1_dose = ppmi_ledd_medication1_dose.groupby(['PATNO'])[['levodopa_enroll_dose']].min().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_ledd_medication1_dose, how = 'left', on = ['PATNO'])

    # + amantadine_enroll
    list_amantadine = [x for x in np.unique(ppmi_ledd_medication['LEDTRT']).tolist() if ('Amantadin' in x) | ('Amantadine' in x) | ('AMANDTADINE' in x) | ('AMANTDINE' in x) | ('AMANTADINE' in x) | ('AMANTADIN' in x) | ('AMANTADINA' in x) | ('AMANTIDINE' in x) ]
    ppmi_ledd_medication2 = ppmi_ledd_medication.copy()
    ppmi_ledd_medication2['start_date_amantadine'] = pd.to_datetime('01/' + ppmi_ledd_medication2['STARTDT'], format = '%d/%m/%Y')
    ppmi_ledd_medication2['stop_date_amantadine'] = pd.to_datetime('01/' + ppmi_ledd_medication2['STOPDT'], format = '%d/%m/%Y')
    ppmi_ledd_medication2_min = ppmi_ledd_medication2[ppmi_ledd_medication2['LEDTRT'].isin(list_amantadine)].groupby(['PATNO'])[['start_date_amantadine']].min().reset_index()
    ppmi_ledd_medication2_max = ppmi_ledd_medication2[ppmi_ledd_medication2['LEDTRT'].isin(list_amantadine)].groupby(['PATNO'])[['stop_date_amantadine']].max().reset_index()
    ppmi_ledd_medication2_log = pd.merge(data_ppmi[['PATNO', 'ENROLL_DATE']], ppmi_ledd_medication2_min, how = 'left', on = ['PATNO'])
    ppmi_ledd_medication2_log = pd.merge(ppmi_ledd_medication2_log, ppmi_ledd_medication2_max, how = 'left', on = ['PATNO'])
    ppmi_ledd_medication2_log['amantadine_enroll'] = np.where( ((ppmi_ledd_medication2_log['ENROLL_DATE'] >= ppmi_ledd_medication2_log['start_date_amantadine'])
                                                                & (ppmi_ledd_medication2_log['ENROLL_DATE'] <= ppmi_ledd_medication2_log['stop_date_amantadine'])
                                                                & (~ppmi_ledd_medication2_log['start_date_amantadine'].isnull())
                                                                & (~ppmi_ledd_medication2_log['stop_date_amantadine'].isnull()) )
                                                            | ((~ppmi_ledd_medication2_log['start_date_amantadine'].isnull())
                                                                & (ppmi_ledd_medication2_log['stop_date_amantadine'].isnull())
                                                                & (ppmi_ledd_medication2_log['ENROLL_DATE'] >= ppmi_ledd_medication2_log['start_date_amantadine'] ) ), 1, 0 )
    ppmi_ledd_medication2_log = ppmi_ledd_medication2_log[['PATNO', 'amantadine_enroll']]
    ppmi_ledd_medication2_log = ppmi_ledd_medication2_log.drop_duplicates()
    ppmi_ledd_medication2_log.reset_index(drop=True, inplace = True)
    data_ppmi = pd.merge(data_ppmi, ppmi_ledd_medication2_log, how = 'left', on = ['PATNO'])

    # + FEATACTTRM, FEATALCREP, FEATALNLMB, FEATANTCOL, FEATANXITY, FEATAPATHY, FEATBWLDYS, FEATCLRLEV, FEATCOGFLC, FEATCRTSNS, FEATDCRARM, FEATDELHAL, FEATDEPRES
    # + FEATDIMOLF, FEATDYSART, FEATDYSKIN, FEATDYSPHG, FEATDYSTNA, FEATGZEPAL, FEATINSPST, FEATLMBAPX, FEATMCRGRA, FEATMTRFLC, FEATMYCLNS, FEATNEURSS, FEATNOLEVO
    # + FEATPOSHYP, FEATPST3YR, FEATPYRTCT, FEATSBDERM, FEATSEXDYS, FEATSHGAIT, FEATSTPPOS, FEATSUGRBD, FEATURNDYS, FEATWDGAIT
    vars_clinical_features = ['FEATACTTRM', 'FEATALCREP', 'FEATALNLMB', 'FEATANTCOL', 'FEATANXITY', 'FEATAPATHY', 'FEATBWLDYS', 'FEATCLRLEV', 'FEATCOGFLC', 'FEATCRTSNS', 'FEATDCRARM'
                            ,'FEATDELHAL', 'FEATDEPRES', 'FEATDIMOLF', 'FEATDYSART', 'FEATDYSKIN', 'FEATDYSPHG', 'FEATDYSTNA', 'FEATGZEPAL', 'FEATINSPST', 'FEATLMBAPX', 'FEATMCRGRA'
                            ,'FEATMTRFLC', 'FEATMYCLNS', 'FEATNEURSS', 'FEATNOLEVO', 'FEATPOSHYP', 'FEATPST3YR', 'FEATPYRTCT', 'FEATSBDERM', 'FEATSEXDYS', 'FEATSHGAIT', 'FEATSTPPOS'
                            ,'FEATSUGRBD', 'FEATURNDYS', 'FEATWDGAIT']
    for var_loop in vars_clinical_features:
        ppmi_clinical_features[var_loop] = np.where( ppmi_clinical_features[var_loop].isin([0, 1]), ppmi_clinical_features[var_loop], np.nan )

    data_ppmi = pd.merge(data_ppmi, ppmi_clinical_features[['PATNO', 'EVENT_ID']+vars_clinical_features], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + surgery_dbs
    ppmi_pd_surgery_dbs = ppmi_pd_surgery[['PATNO', 'EVENT_ID', 'PDSURGTP']]
    ppmi_pd_surgery_dbs['surgery_dbs'] = np.where( ppmi_pd_surgery_dbs['PDSURGTP'] == 1, 1, 0 )
    ppmi_pd_surgery_dbs = ppmi_pd_surgery_dbs.groupby(['PATNO', 'EVENT_ID'])['surgery_dbs'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_pd_surgery_dbs, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + surgery_levodopa
    ppmi_pd_surgery_levo = ppmi_pd_surgery[['PATNO', 'EVENT_ID', 'PDSURGTP']]
    ppmi_pd_surgery_levo['surgery_levodopa'] = np.where( ppmi_pd_surgery_levo['PDSURGTP'] == 2, 1, 0 )
    ppmi_pd_surgery_levo = ppmi_pd_surgery_levo.groupby(['PATNO', 'EVENT_ID'])['surgery_levodopa'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_pd_surgery_levo, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + WGTKG, HTCM, SYSSUP, DIASUP, HRSUP, SYSSTND, DIASTND, HRSTND
    data_ppmi = pd.merge(data_ppmi, ppmi_vital_signs[['PATNO', 'EVENT_ID', 'WGTKG', 'HTCM', 'SYSSUP', 'DIASUP', 'HRSUP', 'SYSSTND', 'DIASTND', 'HRSTND']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + MTRRSP, CORDRSP, SENRSP, RFLXRSP, GAITRSP, MNTLRSP, CNRSP
    vars_neurological = ['MTRRSP', 'CORDRSP', 'SENRSP', 'RFLXRSP', 'GAITRSP', 'MNTLRSP', 'CNRSP']
    for var_loop in vars_neurological:
        ppmi_neurological_exam[var_loop] = np.where( ppmi_neurological_exam[var_loop].isin([0, 1]), ppmi_neurological_exam[var_loop], np.nan )
    ppmi_neurological_exam = ppmi_neurological_exam.groupby(['PATNO', 'EVENT_ID'])[vars_neurological].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_neurological_exam, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NP2SPCH, NP2SALV, NP2SWAL, NP2EAT, NP2DRES, NP2HYGN, NP2HWRT, NP2HOBB, NP2TURN, NP2TRMR, NP2RISE, NP2WALK, NP2FREZ, NP2PTOT
    ppmi_mds_2 = ppmi_mds_2[['PATNO', 'EVENT_ID', 'NP2SPCH', 'NP2SALV', 'NP2SWAL', 'NP2EAT', 'NP2DRES', 'NP2HYGN', 'NP2HWRT', 'NP2HOBB', 'NP2TURN', 'NP2TRMR', 'NP2RISE', 'NP2WALK', 'NP2FREZ', 'NP2PTOT']]
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_2, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NP3SPCH, NP3FACXP, NP3RIGN, NP3RIGRU, NP3RIGLU, NP3RIGRL, NP3RIGLL, NP3FTAPR, NP3FTAPL, NP3HMOVR, NP3HMOVL, NP3PRSPR, NP3PRSPL, NP3TTAPR, NP3TTAPL
    # + NP3LGAGR, NP3LGAGL, NP3RISNG, NP3GAIT, NP3FRZGT, NP3PSTBL, NP3POSTR, NP3BRADY, NP3PTRMR, NP3PTRML, NP3KTRMR, NP3KTRML, NP3RTARU, NP3RTALU, NP3RTARL
    # + NP3RTALL, NP3RTALJ, NP3RTCON
    # + NP3TOT
    vars_mds3 = ['NP3SPCH', 'NP3FACXP', 'NP3RIGN', 'NP3RIGRU', 'NP3RIGLU', 'NP3RIGRL', 'NP3RIGLL', 'NP3FTAPR', 'NP3FTAPL', 'NP3HMOVR', 'NP3HMOVL', 'NP3PRSPR', 'NP3PRSPL', 'NP3TTAPR', 'NP3TTAPL'
                ,'NP3LGAGR', 'NP3LGAGL', 'NP3RISNG', 'NP3GAIT', 'NP3FRZGT', 'NP3PSTBL', 'NP3POSTR', 'NP3BRADY', 'NP3PTRMR', 'NP3PTRML', 'NP3KTRMR', 'NP3KTRML', 'NP3RTARU', 'NP3RTALU', 'NP3RTARL'
                ,'NP3RTALL', 'NP3RTALJ', 'NP3RTCON']
    for var_loop in vars_mds3 :
        ppmi_mds_3[var_loop] = ppmi_mds_3[var_loop].replace('UR', np.nan).map(float)

    ppmi_mds_3_off = ppmi_mds_3[ppmi_mds_3['PAG_NAME'].isin(['NUPDR3OF', 'NUPDRS3'])]
    ppmi_mds_3_off = ppmi_mds_3_off.groupby(['PATNO', 'EVENT_ID'])[vars_mds3+['NP3TOT']].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_3_off, how = 'left', on = ['PATNO', 'EVENT_ID'])


    ppmi_mds_3_on = ppmi_mds_3[ (ppmi_mds_3['PAG_NAME'] == 'NUPDR3ON') | (ppmi_mds_3['PAG_NAME'] == 'NUPDRS3A') ]
    vars_mds3_on = [x+'_ON' for x in vars_mds3+['NP3TOT'] ]
    for var_loop in vars_mds3+['NP3TOT'] :
        ppmi_mds_3_on.rename(columns = {var_loop: var_loop+'_ON'}, inplace = True)
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_3_on[ ['PATNO', 'EVENT_ID']+vars_mds3_on ], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DYSKPRES, NHY
    ppmi_mds_3['NHY'] = ppmi_mds_3['NHY'].replace('UR', np.nan).map(float)
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_3.groupby(['PATNO', 'EVENT_ID'])['DYSKPRES', 'NHY'].max().reset_index(), how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + PDDXDT
    vars_pd_diagnosis = ['DXTREMOR', 'DXRIGID', 'DXBRADY', 'DXPOSINS']
    for var_loop in vars_pd_diagnosis:
        ppmi_pd_diagnosis[var_loop] = np.where( ppmi_pd_diagnosis[var_loop].isin([0, 1]), ppmi_pd_diagnosis[var_loop], np.nan )
    data_ppmi = pd.merge(data_ppmi, ppmi_pd_diagnosis[['PATNO', 'PDDXDT']+vars_pd_diagnosis], how = 'left', on = ['PATNO'])


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Assessments    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # + NP1COG, NP1HALL, NP1DPRS, NP1ANXS, NP1APAT, NP1DDS
    vars_mds1 = ['NP1COG', 'NP1HALL', 'NP1DPRS', 'NP1ANXS', 'NP1APAT', 'NP1DDS']
    for var_loop in vars_mds1 :
        ppmi_mds_1[var_loop] = ppmi_mds_1[var_loop].replace('UR', np.nan).map(float)
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_1[['PATNO', 'EVENT_ID']+vars_mds1], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NP1SLPN, NP1SLPD, NP1PAIN, NP1URIN, NP1CNST, NP1LTHD, NP1FATG
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_1_2[['PATNO', 'EVENT_ID', 'NP1SLPN', 'NP1SLPD', 'NP1PAIN', 'NP1URIN', 'NP1CNST', 'NP1LTHD', 'NP1FATG']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NP1RTOT
    ppmi_mds_1_tot = pd.merge(ppmi_mds_1[['PATNO', 'EVENT_ID', 'NP1RTOT']], ppmi_mds_1[['PATNO', 'EVENT_ID', 'NP1RTOT']], how = 'outer', on = ['PATNO', 'EVENT_ID'])
    ppmi_mds_1_tot['NP1RTOT'] = ppmi_mds_1_tot[['NP1RTOT_x', 'NP1RTOT_y']].sum(axis = 1)
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_1_tot[['PATNO', 'EVENT_ID', 'NP1RTOT']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NP4WDYSK, NP4DYSKI, NP4OFF, NP4FLCTI, NP4FLCTX, NP4DYSTN
    # + NP4WDYSKDEN, NP4WDYSKNUM, NP4WDYSKPCT, NP4OFFDEN, NP4OFFPCT, NP4DYSTNDEN, NP4DYSTNPCT, NP4TOT
    vars_mds4_1 = ['NP4WDYSK', 'NP4DYSKI', 'NP4OFF', 'NP4FLCTI', 'NP4FLCTX', 'NP4DYSTN']
    vars_mds4_2 = ['NP4WDYSKDEN', 'NP4WDYSKNUM', 'NP4WDYSKPCT', 'NP4OFFDEN', 'NP4OFFPCT', 'NP4DYSTNDEN', 'NP4DYSTNPCT', 'NP4TOT']
    for var_loop in vars_mds4_1 :
        ppmi_mds_4[var_loop] = ppmi_mds_4[var_loop].replace('UR', np.nan).map(float)
    data_ppmi = pd.merge(data_ppmi, ppmi_mds_4[['PATNO', 'EVENT_ID']+vars_mds4_1+vars_mds4_2], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + MSEADLG
    data_ppmi = pd.merge(data_ppmi, ppmi_schwab_england[['PATNO', 'EVENT_ID', 'MSEADLG']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NQMOB37, NQMOB30, NQMOB26, NQMOB32, NQMOB25, NQMOB33, NQMOB31, NQMOB28
    ppmi_qol_lower = ppmi_qol_lower[['PATNO', 'EVENT_ID', 'NQMOB37', 'NQMOB30', 'NQMOB26', 'NQMOB32', 'NQMOB25', 'NQMOB33', 'NQMOB31', 'NQMOB28']]
    data_ppmi = pd.merge(data_ppmi, ppmi_qol_lower, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + NQUEX29, NQUEX20, NQUEX44, NQUEX36, NQUEX30, NQUEX28, NQUEX33, NQUEX37
    ppmi_qol_upper = ppmi_qol_upper[['PATNO', 'EVENT_ID', 'NQUEX29', 'NQUEX20', 'NQUEX44', 'NQUEX36', 'NQUEX30', 'NQUEX28', 'NQUEX33', 'NQUEX37']]
    data_ppmi = pd.merge(data_ppmi, ppmi_qol_upper, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + TRBUPCHR, WRTSMLR, VOICSFTR, POORBAL, FTSTUCK, LSSXPRSS, ARMLGSHK, TRBBUTTN, SHUFFLE, MVSLOW
    vars_motor_function = ['TRBUPCHR', 'WRTSMLR', 'VOICSFTR', 'POORBAL', 'FTSTUCK', 'LSSXPRSS', 'ARMLGSHK', 'TRBBUTTN', 'SHUFFLE', 'MVSLOW']
    for var_loop in vars_motor_function :
        ppmi_motor_function[var_loop] = np.where(ppmi_motor_function[var_loop].isin([0, 1]), ppmi_motor_function[var_loop], np.nan)
    data_ppmi = pd.merge(data_ppmi, ppmi_motor_function[['PATNO', 'EVENT_ID']+vars_motor_function], how = 'left', on = ['PATNO', 'EVENT_ID'])



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Non-Motor Assessments    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # + SCAU1, SCAU2, SCAU3, SCAU4, SCAU5, SCAU6, SCAU7, SCAU8, SCAU9, SCAU10, SCAU11, SCAU12, SCAU13, SCAU14, SCAU15, SCAU16, SCAU17, SCAU18
    # + SCAU19, SCAU20, SCAU21, SCAU22, SCAU23, SCAU24, SCAU25
    vars_scopa = ['SCAU1', 'SCAU2', 'SCAU3', 'SCAU4', 'SCAU5', 'SCAU6', 'SCAU7', 'SCAU8', 'SCAU9', 'SCAU10', 'SCAU11', 'SCAU12', 'SCAU13', 'SCAU14'
                ,'SCAU15', 'SCAU16', 'SCAU17', 'SCAU18', 'SCAU19', 'SCAU20', 'SCAU21', 'SCAU22', 'SCAU23', 'SCAU24', 'SCAU25']
    for var_loop in vars_scopa :
        ppmi_scopa_aut[var_loop] = np.where( ppmi_scopa_aut[var_loop].isin([0, 1, 2, 3]), ppmi_scopa_aut[var_loop], np.nan )
    ppmi_scopa_aut = ppmi_scopa_aut[['PATNO', 'EVENT_ID']+vars_scopa]
    ppmi_scopa_aut['scopa_gi'] = ppmi_scopa_aut[['SCAU1', 'SCAU2', 'SCAU3', 'SCAU4', 'SCAU5', 'SCAU6', 'SCAU7']].sum(axis = 1)
    ppmi_scopa_aut['scopa_gi'] = np.where( (ppmi_scopa_aut['SCAU1'].isnull()) & (ppmi_scopa_aut['SCAU2'].isnull()) & (ppmi_scopa_aut['SCAU3'].isnull()) &\
                                        (ppmi_scopa_aut['SCAU4'].isnull()) & (ppmi_scopa_aut['SCAU5'].isnull()) & (ppmi_scopa_aut['SCAU6'].isnull()) & (ppmi_scopa_aut['SCAU7'].isnull()),
                                        np.nan, ppmi_scopa_aut['scopa_gi'])

    ppmi_scopa_aut['scopa_ur'] = ppmi_scopa_aut[['SCAU9', 'SCAU10', 'SCAU11', 'SCAU12', 'SCAU13']].sum(axis = 1)
    ppmi_scopa_aut['scopa_ur'] = np.where( (ppmi_scopa_aut['SCAU9'].isnull()) & (ppmi_scopa_aut['SCAU10'].isnull()) & (ppmi_scopa_aut['SCAU11'].isnull()) & (ppmi_scopa_aut['SCAU12'].isnull()) &\
                                        (ppmi_scopa_aut['SCAU13'].isnull()), np.nan, ppmi_scopa_aut['scopa_ur'] )

    ppmi_scopa_aut['scopa_cv'] = ppmi_scopa_aut[['SCAU14', 'SCAU15', 'SCAU16']].sum(axis = 1)
    ppmi_scopa_aut['scopa_cv'] = np.where( (ppmi_scopa_aut['SCAU14'].isnull()) & (ppmi_scopa_aut['SCAU15'].isnull()) & (ppmi_scopa_aut['SCAU16'].isnull()), np.nan, ppmi_scopa_aut['scopa_cv'] )

    ppmi_scopa_aut['scopa_therm'] = ppmi_scopa_aut[['SCAU17', 'SCAU18', 'SCAU20', 'SCAU21']].sum(axis = 1)
    ppmi_scopa_aut['scopa_therm'] = np.where( (ppmi_scopa_aut['SCAU17'].isnull()) & (ppmi_scopa_aut['SCAU18'].isnull()) & (ppmi_scopa_aut['SCAU20'].isnull()) &\
                                            (ppmi_scopa_aut['SCAU21'].isnull()) , np.nan, ppmi_scopa_aut['scopa_therm'] )

    ppmi_scopa_aut['scopa_pupilo'] = ppmi_scopa_aut[['SCAU19']]

    ppmi_scopa_aut['scopa_sex'] = np.where(ppmi_scopa_aut['SCAU22']>0, ppmi_scopa_aut['SCAU22']-1, 0) +\
                                    np.where(ppmi_scopa_aut['SCAU23']>0, ppmi_scopa_aut['SCAU23']-1, 0) +\
                                    np.where(ppmi_scopa_aut['SCAU24']>0, ppmi_scopa_aut['SCAU24']-1, 0) +\
                                    np.where(ppmi_scopa_aut['SCAU25']>0, ppmi_scopa_aut['SCAU25']-1, 0)
    ppmi_scopa_aut['scopa_sex'] = np.where( ( (ppmi_scopa_aut['SCAU22'].isnull()) & (ppmi_scopa_aut['SCAU23'].isnull()) ) |\
                                        ( (ppmi_scopa_aut['SCAU24'].isnull()) & (ppmi_scopa_aut['SCAU25'].isnull()) ), np.nan, ppmi_scopa_aut['scopa_sex'] )

    data_ppmi = pd.merge(data_ppmi, ppmi_scopa_aut, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + COGDECLN, FNCDTCOG, COGSTATE
    data_ppmi = pd.merge(data_ppmi, ppmi_cognitive_cat[['PATNO', 'EVENT_ID', 'COGDECLN', 'FNCDTCOG', 'COGSTATE']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + TMTACMPL, TMTASEC, TMTBCMPL, TMTBSEC
    data_ppmi = pd.merge(data_ppmi, ppmi_trail_making[['PATNO', 'EVENT_ID', 'TMTACMPL', 'TMTASEC', 'TMTBCMPL', 'TMTBSEC']], how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + GDSSATIS, GDSDROPD, GDSEMPTY, GDSBORED, GDSGSPIR, GDSAFRAD, GDSHAPPY, GDSHLPLS, GDSHOME, GDSMEMRY, GDSALIVE, GDSWRTLS, GDSENRGY, GDSHOPLS, GDSBETER
    ppmi_geriatric_depression = ppmi_geriatric_depression[['PATNO', 'EVENT_ID', 'GDSSATIS', 'GDSDROPD', 'GDSEMPTY', 'GDSBORED', 'GDSGSPIR','GDSAFRAD', 'GDSHAPPY'
                            ,'GDSHLPLS', 'GDSHOME', 'GDSMEMRY', 'GDSALIVE', 'GDSWRTLS', 'GDSENRGY', 'GDSHOPLS', 'GDSBETER']]
    data_ppmi = pd.merge(data_ppmi, ppmi_geriatric_depression, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + TMGAMBLE, CNTRLGMB, TMSEX, CNTRLSEX, TMBUY, CNTRLBUY, TMEAT, CNTRLEAT
    ppmi_quip = ppmi_quip[['PATNO', 'EVENT_ID', 'TMGAMBLE', 'CNTRLGMB', 'TMSEX', 'CNTRLSEX', 'TMBUY', 'CNTRLBUY', 'TMEAT', 'CNTRLEAT']]
    data_ppmi = pd.merge(data_ppmi, ppmi_quip, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + JLO_TOTRAW
    ppmi_benton = ppmi_benton[['PATNO', 'EVENT_ID', 'JLO_TOTRAW']].drop_duplicates().groupby(['PATNO', 'EVENT_ID'])['JLO_TOTRAW'].max().reset_index()
    data_ppmi = pd.merge(data_ppmi, ppmi_benton[['PATNO', 'EVENT_ID', 'JLO_TOTRAW']].drop_duplicates(), how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DVT_TOTAL_RECALL, DVT_DELAYED_RECALL, DVT_RETENTION, DVT_RECOG_DISC_INDEX
    ppmi_hopkins = ppmi_hopkins[['PATNO', 'EVENT_ID', 'DVT_TOTAL_RECALL', 'DVT_DELAYED_RECALL', 'DVT_RETENTION', 'DVT_RECOG_DISC_INDEX']]
    data_ppmi = pd.merge(data_ppmi, ppmi_hopkins, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DVS_LNS
    ppmi_lns = ppmi_lns[['PATNO', 'EVENT_ID', 'DVS_LNS']]
    data_ppmi = pd.merge(data_ppmi, ppmi_lns, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DVS_SFTANIM, DVT_SFTANIM
    ppmi_semantic_fluency = ppmi_semantic_fluency[['PATNO', 'EVENT_ID', 'DVS_SFTANIM', 'DVT_SFTANIM']]
    data_ppmi = pd.merge(data_ppmi, ppmi_semantic_fluency, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + MCAVFNUM, MCATOT
    ppmi_moca = ppmi_moca[['PATNO', 'EVENT_ID', 'MCAVFNUM', 'MCATOT']]
    data_ppmi = pd.merge(data_ppmi, ppmi_moca, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + SDMTOTAL, DVSD_SDM, DVT_SDM
    ppmi_sdm = ppmi_sdm[['PATNO', 'EVENT_ID', 'SDMTOTAL', 'DVSD_SDM', 'DVT_SDM']]
    data_ppmi = pd.merge(data_ppmi, ppmi_sdm, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + UPSIT_PRCNTGE
    ppmi_upsit = ppmi_upsit[['PATNO', 'EVENT_ID', 'UPSIT_PRCNTGE']]
    data_ppmi = pd.merge(data_ppmi, ppmi_upsit, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + ESS1, ESS2, ESS3, ESS4, ESS5, ESS6, ESS7, ESS8
    ppmi_epworth = ppmi_epworth[['PATNO', 'EVENT_ID', 'ESS1', 'ESS2', 'ESS3', 'ESS4', 'ESS5', 'ESS6', 'ESS7', 'ESS8']]
    data_ppmi = pd.merge(data_ppmi, ppmi_epworth, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + DRMVIVID, DRMAGRAC, DRMNOCTB, SLPLMBMV, SLPINJUR, DRMVERBL, DRMFIGHT, DRMUMV, DRMOBJFL, MVAWAKEN, DRMREMEM, SLPDSTRB
    # + STROKE, HETRA, PARKISM, RLS, NARCLPSY, DEPRS, EPILEPSY, BRNINFM
    ppmi_rem = ppmi_rem[['PATNO', 'EVENT_ID', 'DRMVIVID', 'DRMAGRAC', 'DRMNOCTB', 'SLPLMBMV', 'SLPINJUR', 'DRMVERBL', 'DRMFIGHT', 'DRMUMV', 'DRMOBJFL', 'MVAWAKEN', 'DRMREMEM', 'SLPDSTRB'
                        ,'STROKE', 'HETRA', 'PARKISM', 'RLS', 'NARCLPSY', 'DEPRS', 'EPILEPSY', 'BRNINFM']]
    data_ppmi = pd.merge(data_ppmi, ppmi_rem, how = 'left', on = ['PATNO', 'EVENT_ID'])

    # + alq1, alq2, alq4
    # + alq3_age
    vars_alcohol = ['alq1', 'alq2', 'alq4']
    for var_loop in vars_alcohol:
        ppmi_alcohol[var_loop] = np.where(ppmi_alcohol[var_loop].isin([0, 1]), ppmi_alcohol[var_loop], np.nan)
    ppmi_alcohol.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_alcohol = ppmi_alcohol[['PATNO']+vars_alcohol+['alq3_age']]
    data_ppmi = pd.merge(data_ppmi, ppmi_alcohol, how = 'left', on = ['PATNO'])

    # + cfqa1, cfqb1, cfqbd1, cfqc1, cfqcd1, cfqd0, cfqd1, cfqdd1, cfqe0, cfqe1, cfqed1
    # cfqa2age, cfqb2age, cfqbd2age, cfqc2age, cfqcd2age, cfqd2age, cfqdd2age, cfqe2age, cfqed2age
    vars_caffeine = ['cfqa1', 'cfqb1', 'cfqbd1', 'cfqc1', 'cfqcd1', 'cfqd0', 'cfqd1', 'cfqdd1', 'cfqe0', 'cfqe1', 'cfqed1']
    for var_loop in vars_caffeine :
        ppmi_caffeine[var_loop] = np.where(ppmi_caffeine[var_loop].isin([0, 1]), ppmi_caffeine[var_loop], np.nan)
    ppmi_caffeine.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_caffeine = ppmi_caffeine[['PATNO']+vars_caffeine+['cfqa2age', 'cfqb2age', 'cfqbd2age', 'cfqc2age', 'cfqcd2age', 'cfqd2age', 'cfqdd2age', 'cfqe2age', 'cfqed2age']]
    data_ppmi = pd.merge(data_ppmi, ppmi_caffeine, how = 'left', on = ['PATNO'])

    # + hiq2, hiqa1_age
    ppmi_head_injury['hiq2'] = np.where( (ppmi_head_injury['hiq2'].isnull()) & (ppmi_head_injury['hiq1'] == 0), 0, np.where( ppmi_head_injury['hiq2'] < 999, ppmi_head_injury['hiq2'], np.nan ) )
    ppmi_head_injury.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_head_injury = ppmi_head_injury[['PATNO', 'hiq2', 'hiqa1_age']]
    data_ppmi = pd.merge(data_ppmi, ppmi_head_injury, how = 'left', on = ['PATNO'])

    # + pwlabelintro1
    ppmi_pesticides_work['pwlabelintro1'] = np.where( ppmi_pesticides_work['pwlabelintro1'].isin([0, 1]), ppmi_pesticides_work['pwlabelintro1'], np.nan )
    ppmi_pesticides_work.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_pesticides_work = ppmi_pesticides_work[['PATNO', 'pwlabelintro1']]
    data_ppmi = pd.merge(data_ppmi, ppmi_pesticides_work, how = 'left', on = ['PATNO'])

    # + phintro
    ppmi_pesticides_non_work['phintro'] = np.where( ppmi_pesticides_non_work['phintro'].isin([0, 1]), ppmi_pesticides_non_work['phintro'], np.nan )
    ppmi_pesticides_non_work.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_pesticides_non_work = ppmi_pesticides_non_work[['PATNO', 'phintro']]
    data_ppmi = pd.merge(data_ppmi, ppmi_pesticides_non_work, how = 'left', on = ['PATNO'])

    # + smq1, smq3_age
    ppmi_smoking['smq3_age']
    ppmi_smoking['smq1'] = np.where( ppmi_smoking['smq1'].isin([0, 1]), ppmi_smoking['smq1'], np.nan )
    ppmi_smoking.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_smoking = ppmi_smoking[['PATNO', 'smq1']]
    data_ppmi = pd.merge(data_ppmi, ppmi_smoking, how = 'left', on = ['PATNO'])

    # + tx1, tx2, tx3, tx4
    vars_toxicant = ['tx1', 'tx2', 'tx3', 'tx4']
    for var_loop in vars_toxicant :
        ppmi_toxicant[var_loop] = np.where( ppmi_toxicant[var_loop].isin([0, 1]), ppmi_toxicant[var_loop], np.nan )
    ppmi_toxicant.rename(columns = {'patno': 'PATNO'}, inplace = True)
    ppmi_toxicant = ppmi_toxicant[['PATNO']+vars_toxicant]
    data_ppmi = pd.merge(data_ppmi, ppmi_toxicant, how = 'left', on = ['PATNO'])

    # + ALIVE
    data_ppmi = pd.merge(data_ppmi, ppmi_telephone_fu[['PATNO', 'ALIVE']].groupby(['PATNO'])['ALIVE'].max().reset_index(), how = 'left', on = 'PATNO')

    # 'LEDD' from ppmi_ledd_medication
    data_ledd = ppmi_ledd_medication[~ppmi_ledd_medication['LEDD'].isna()][['PATNO', 'EVENT_ID', 'INFODT', 'LEDD']]

    return data_ppmi, data_infodate, data_ledd


data_ppmi, data_infodate, data_ledd = preprocess_data_ppmi()