#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Normalized the Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

normalize_data <- function( var_patno = 'PATNO'                       # Variable of the patient ID
                           ,var_outcome = 'NP4DYSKI2_Y4'              # Variable of the outcome
                           ,method = ""                               # Normalized method
                                                                      # method = "" or method = "none" : No normalization
                                                                      # method = "quantile"            : Quantile normalization
                                                                      # method = "mean"                : Mean-centering normalization
                                                                      # method = "ratioa"              : Ratio-A normalization
                                                                      # method = "ratiog"              : Ratio-G normalization
                                                                      # method = "standardize"         : Standardization
                                                                      # method = "sva"                 : Surrogate variable analysis (SVA)
                                                                      # method = "combat"              : ComBat normalization
                                                                      # method = "mcombat"             : M-ComBat normalization
                           ,log_ratio_num_var = 0                     # Number of top variance variable (if method = logratio)
                           ,run_addon = FALSE                         # To define whether to run the addon
                           ,B_loop = 20                               # The loop number
                           ,k_loop = 5                                # The loop number for repeated cross-validation
                           ,m_loop = 3                                # The loop number for nested cross-validation
                           ,ds_prefix = ""                            # The prefix of the data
                           ,seed = 890701                             # Seed number
                           ,undersampling = FALSE                     # Data with undersampling
                           ,vars_continuous = c()                     # Continuous variables to be normalized
                           ,repo_data_in = "Analysis/Pickle Data/"    # File repository for the input data
                           ,repo_data_out = "Analysis/Pickle Data/"   # File repository for the normalized data
                           ){
  
  # Set seed number
  set.seed(seed)
  
  # Pandas data frame
  pd <- import("pandas")
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  ####    Normalization each set of data independently    ####
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  list_data <- c("training", "validate")
  
  for (BLoop in 1:B_loop){
    for (kLoop in 1:k_loop){
      list_data <- c(list_data, paste0("test_B", BLoop, "_k", kLoop))
      list_data <- c(list_data, paste0("train_B", BLoop, "_k", kLoop))
      for (mLoop in 1: m_loop){
        list_data <- c(list_data, paste0("test_B", BLoop, "_k", kLoop, "_m", mLoop))
        list_data <- c(list_data, paste0("train_B", BLoop, "_k", kLoop, "_m", mLoop))
      }
    }
  }
  
  
  for(list_pickle in list_data){
    
    # To read the validation data
    pickle_validate <- pd$read_pickle(paste0(repo_data_in, ds_prefix , list_pickle,".pickle"))
    
    # Batch number of the validation data
    batch_validate <- ifelse(str_detect(pickle_validate[[var_patno]], "LUXPARK") == TRUE, 1
                             ,ifelse(str_detect(pickle_validate[[var_patno]], "PPMI") == TRUE, 2
                                     ,ifelse(str_detect(pickle_validate[[var_patno]], "ICEBERG") == TRUE, 3, 4)))
    
    # If any batch contains only 1 subject, allocate it to the batch with the most subjects
    if( min(table(batch_validate)) == 1 ){
      min_batch = names(table(batch_validate)[ table(batch_validate) == 1 ])
      max_batch = names(table(batch_validate)[ table(batch_validate) == max(table(batch_validate)) ])
      batch_validate[ which(batch_validate %in% as.integer(min_batch)) ] = as.integer(max_batch)
    }
    batch_validate <- factor(batch_validate)
    
    # Column name of the validation data
    validate_colnames <- names(pickle_validate)
    
    # List of continuous variables
    if( length(vars_continuous) > 0 ){
      pickle_validate_num <- data.table(pickle_validate[,validate_colnames[validate_colnames %in% vars_continuous]])
      pickle_validate_cha <- data.table(pickle_validate[,validate_colnames[!validate_colnames %in% vars_continuous]])
    }
    
    if(method == "quantile"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Quantile normalization batch effect adjustment:
      validate_params <- bapred::qunormtrain( x = pickle_validate_num[, ..norm_colnames] )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xnorm))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
      
    } else if(method == "mean"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Mean-centering normalization batch effect adjustment:
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "meancenter" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "ratioa"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Ratio-A normalization batch effect adjustment
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "ratioa" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "ratiog"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Ratio-G normalization batch effect adjustment
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "ratiog" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "standardize"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Standardize normalization batch effect adjustment
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "standardize" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "sva"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # SVA normalization batch effect adjustment
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "sva" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "combat"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Variable with zero variance
      var_all <- apply(pickle_validate_num[, ..norm_colnames]
                       ,2, var)
      var_excl <- names(var_all[var_all == 0])
      
      # ComBat normalization batch effect adjustment
      norm_colnames_2 <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome, var_excl)]
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames_2])
      
      validate_params <- bapred::ba( x = pickle_validate_mat
                                     ,y = pickle_validate[[var_outcome]]
                                     ,batch = batch_validate
                                     ,method = "combat" )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      if(length(var_excl) > 0){
        for(exclLoop in var_excl){
          validate_normalized[[exclLoop]] <- pickle_validate_num[[exclLoop]]
        }
      }
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "mcombat"){
      
      ### Validation data
      # Column names of continuous variables to be normalized
      norm_colnames <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome)]
      
      # Variable with zero variance
      var_all <- apply(pickle_validate_num[, ..norm_colnames]
                       ,2, var)
      var_excl <- names(var_all[var_all == 0])
      
      # M-ComBat normalization batch effect adjustment
      norm_colnames_2 <- names(pickle_validate_num)[!names(pickle_validate_num) %in% c(var_patno, var_outcome, var_excl)]
      pickle_validate_mat <- data.matrix(pickle_validate_num[, ..norm_colnames_2])
      
      validate_params <- M.COMBAT( x = pickle_validate_mat
                                   ,batch = batch_validate
                                   ,center = 1 )
      
      # Batch effect adjustment on the validation set
      validate_normalized <- cbind(pickle_validate_cha, data.table(validate_params$xadj))
      if(length(var_excl) > 0){
        for(exclLoop in var_excl){
          validate_normalized[[exclLoop]] <- pickle_validate_num[[exclLoop]]
        }
      }
      validate_normalized <- validate_normalized[, ..validate_colnames]
      
    } else if(method == "logratio"){
      
      ### Validation data
      # Log-ratio pairwise variables
      validate_normalized <- log_ratio_vars( data = pickle_validate[,validate_colnames[!validate_colnames %in% c(var_patno, var_outcome)]]
                                             ,num_var = 0)
      colnames_normalized <- names(validate_normalized)
      validate_normalized[[var_patno]] <- pickle_validate[[var_patno]]
      validate_normalized[[var_outcome]] <- pickle_validate[[var_outcome]]
      colnames_normalized <- c(var_patno, colnames_normalized, var_outcome)
      validate_normalized <- validate_normalized[, ..colnames_normalized]
      
    }
    
    # To save the training and testing data into pickle
    py_save_object(validate_normalized
                   ,filename = paste0(repo_data_out, ds_prefix, list_pickle,".pickle") )
  }
  
  # To return a message
  return(paste0("Done generating normalized data - ", toupper(method), " - ", var_outcome))
  
}

