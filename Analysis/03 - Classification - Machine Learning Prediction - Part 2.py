#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                         Optimal Model - Highest average AUC scores                                                         ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Performance metric - Highest average AUC score    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_model_auc = []
ls_model_name = []

for loop_i in range(len(model_classifier)) :
    name_key = list( model_classifier[loop_i].keys() )[0]
    ls_model_name.append( name_key )
    ls_model_auc.append( model_classifier[loop_i][name_key].best_model_auc_test )  # Best model has highest average AUC score

idx_best = ls_model_auc.index( max(ls_model_auc) )
best_ml_model = model_classifier[idx_best][ls_model_name[idx_best]]
if best_ml_model.ml_model_set != 'mean' :
    best_ml_model.performance_metric()
else :
    pass


#~~~~~~~~~~~~~~~~~~~~~~~~~#
####    SHAP values    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~#

if best_ml_model.ml_model_set != 'mean' :
    # if model_prefix == 'greedy_dyski_y4' :
    #     shap_fig_size = [12, 10]
    # else:
    #     shap_fig_size = [12, 5]
    # shap_fig_size = [10, 5]
    shap_fig_size = [12, 6]

    shap_ml_model = shap_values(model = best_ml_model, fig_repo = file_repo+file_repo_prefix+set_outcome+'/', fig_name = model_prefix, fig_features = data_info_all, fig_size = shap_fig_size)
    
    best_ml_model.shap_values = shap_ml_model['shap_values']
    best_ml_model.feature_shap_values = shap_ml_model['feature_shap_values']
    best_ml_model.feature_importance = shap_ml_model['feature_importance']
else:
    pass


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Save the best model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

save_object(data = best_ml_model, filename = "ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
best_ml_model = load_object(filename = "ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                          Optimal Model - Highest average AUC scores and least number of variables                                          ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Performance metric - Highest average AUC score & less complexity    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_measure_nvars(loop_i) :
    name_key = list( model_classifier[loop_i].keys() )[0]
    ls_model_name = name_key
    try :
        ls_model_auc = model_classifier[loop_i][name_key].best_model_auc_test
    except :
        ls_model_auc = model_classifier[loop_i][name_key][name_key].best_model_auc_test

    cols_X = model_classifier[loop_i][name_key].X_validate.columns.tolist()

    # # CatBoost, XGBoost, GBoost, AdaBoost
    # if (model_set == 'catboost') | (model_set == 'xgboost') | (model_set == 'gboost') | (model_set == 'adaboost') :
    #     perm = model_classifier[loop_i][name_key].best_model.feature_importances_

    # else :
    #     perm = abs( permutation_importance(model_classifier[loop_i][name_key].best_model
    #                                        ,model_classifier[loop_i][name_key].X_validate
    #                                        ,model_classifier[loop_i][name_key].y_validate
    #                                        ,scoring = 'balanced_accuracy'
    #                                        ,random_state = model_classifier[loop_i][name_key].seed_number ).importances_mean )
    
    # num_vars = len([x for x in perm if x != 0])
    # select_feats = [cols_X[x] for x in range(len(perm)) if perm[x] != 0]

    select_feats = model_classifier[loop_i][name_key].best_model_features
    num_vars = len(select_feats)
    
    ls_num_vars = num_vars

    return({'ls_model_name': ls_model_name
            ,'ls_model_auc': ls_model_auc
            ,'ls_num_vars': ls_num_vars
            ,'ls_features': select_feats
            })

n_jobs_nvars = min([n_jobs, len(model_classifier)])
measure_nvars = Parallel(n_jobs = n_jobs_nvars)(
    delayed( func_measure_nvars )(loop_i = loop_i)
    for loop_i in range(len(model_classifier)) )

ls_model_name = []
ls_model_auc = []
ls_num_vars = []
ls_features = []
for loop_i in range(len(model_classifier)) :
    ls_model_name.append( measure_nvars[loop_i]['ls_model_name'] )
    ls_model_auc.append( measure_nvars[loop_i]['ls_model_auc'] )
    ls_num_vars.append( measure_nvars[loop_i]['ls_num_vars'] )
    ls_features.append( measure_nvars[loop_i]['ls_features'] )

# Index - highest average AUC score
idx_best = [x for x in range(len(ls_model_auc)) if ls_model_auc[x] == max(ls_model_auc)]

# Index - between 1 standard deviation from the highest average AUC score
idx_std = [x for x in range(len(ls_model_auc)) if (ls_model_auc[x] > max(ls_model_auc) - statistics.stdev(ls_model_auc))
           & (ls_model_auc[x] < max(ls_model_auc) + statistics.stdev(ls_model_auc)) ]
if len(idx_std) == 0 :
    idx_std = [x for x in range(len(ls_model_auc)) if (ls_model_auc[x] >= max(ls_model_auc) - statistics.stdev(ls_model_auc))
               & (ls_model_auc[x] <= max(ls_model_auc) + statistics.stdev(ls_model_auc)) ]

# Index - between 1 standard deviation with least number of variables (but more than 1 variable)
idx_num_vars = [x for x in idx_std if ls_num_vars[x] > 1]
if len(idx_num_vars) == 0 :
    idx_num_vars = [x for x in idx_std]

# Index - optimal model
idx_optimal = [x for x in idx_num_vars if ls_num_vars[x] == min([ls_num_vars[x] for x in idx_num_vars])]

if len(idx_optimal) > 1 :
    ls_auc_optimal = [ls_model_auc[x] for x in idx_optimal]
    if len(ls_auc_optimal) > 1 :
        max_auc_optimal = max(ls_auc_optimal)
    else :
        max_auc_optimal = ls_auc_optimal[0]
    
    idx_optimal = [x for x in idx_optimal if ls_model_auc[x] == max_auc_optimal][0]
else :
    idx_optimal = idx_optimal[0]

# The optimal model with AUC within 1 SD from maximum of average AUC and minimal number of variables
best_compact_ml_model = model_classifier[idx_optimal][ls_model_name[idx_optimal]]
if best_compact_ml_model.ml_model_set != 'mean' :
    best_compact_ml_model.performance_metric()
else :
    pass

# To list the name of the methods, number of variables and average AUC scores
compact_ml_model_vars = dict({'model_name': ls_model_name
                              ,'num_vars': ls_num_vars
                              ,'model_avg_auc': ls_model_auc
                              ,'model_selected_features': ls_features
                              ,'idx_optimal': idx_optimal
                              })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Save the best model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

save_object(data = best_compact_ml_model, filename = "compact_ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
best_compact_ml_model = load_object(filename = "compact_ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )

save_object(data = compact_ml_model_vars, filename = "compact_ml_vars_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
compact_ml_model_vars = load_object(filename = "compact_ml_vars_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Delete the variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

del model_classifier, best_ml_model, best_compact_ml_model, compact_ml_model_vars