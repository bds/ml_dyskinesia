#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Levodopa-induced dyskinesia (NP4DYSKI2_Y4)                                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_luxpark_impute = split_impute(data = data_cohort_luxpark.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                                  ,outcome = 'NP4DYSKI2_Y4'
                                  ,bs_outcome = 'NP4DYSKI2'
                                  ,vars_cat = vars_category_common
                                  ,save_data_repo = file_data+'LUXPARK/NP4DYSKI2_Y4/NONE/' )
data_luxpark_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_ppmi_impute = split_impute(data = data_cohort_ppmi.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                               ,outcome = 'NP4DYSKI2_Y4'
                               ,bs_outcome = 'NP4DYSKI2'
                               ,vars_cat = vars_category_common
                               ,save_data_repo = file_data+'PPMI/NP4DYSKI2_Y4/NONE/' )
data_ppmi_impute.split_n_impute(n_jobs = n_jobs)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - ICEBERG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_iceberg_impute = split_impute(data = data_cohort_iceberg.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                                  ,outcome = 'NP4DYSKI2_Y4'
                                  ,bs_outcome = 'NP4DYSKI2'
                                  ,vars_cat = vars_category_common
                                  ,save_data_repo = file_data+'ICEBERG/NP4DYSKI2_Y4/NONE/' )
data_iceberg_impute.split_n_impute(n_jobs = n_jobs)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI + LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_cohort_ppmi_luxpark = pd.concat([data_cohort_ppmi, data_cohort_luxpark])
data_loo_impute = split_impute(data = data_cohort_ppmi_luxpark.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                              ,val_data = data_cohort_iceberg.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                              ,outcome = 'NP4DYSKI2_Y4'
                              ,bs_outcome = 'NP4DYSKI2'
                              ,vars_cat = vars_category_common
                              ,save_data_repo = file_data+'LEAVE_ONE_OUT/NP4DYSKI2_Y4/NONE/' )
data_loo_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - ICEBERG + LuxPARK    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_cohort_iceberg_luxpark = pd.concat([data_cohort_iceberg, data_cohort_luxpark])
data_loo_impute = split_impute(data = data_cohort_iceberg_luxpark.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                              ,val_data = data_cohort_ppmi.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                              ,outcome = 'NP4DYSKI2_Y4'
                              ,bs_outcome = 'NP4DYSKI2'
                              ,vars_cat = vars_category_common
                              ,save_data_repo = file_data+'LEAVE_ONE_OUT2/NP4DYSKI2_Y4/NONE/' )
data_loo_impute.split_n_impute(n_jobs = n_jobs)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Split Data & Missing Values Imputation - PPMI + LuxPARK + ICEBERG    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
data_comb_impute = split_impute(data = data_cohort_comb.set_index(['PATNO'])[vars_baseline_dyski+['NP4DYSKI2_Y4']]
                               ,outcome = 'NP4DYSKI2_Y4'
                               ,bs_outcome = 'NP4DYSKI2'
                               ,vars_cat = vars_category_common
                               ,save_data_repo = file_data+'ALL/NP4DYSKI2_Y4/NONE/' )
data_comb_impute.split_n_impute(n_jobs = n_jobs)
