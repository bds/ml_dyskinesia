#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                         Load packages                                                                        ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from dcurves import dca
from typing import Optional, Iterable
import random
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm
from numpy import ndarray
from sklearn.calibration import calibration_curve
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from lifelines import KaplanMeierFitter


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                         Load packages                                                                        ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - SurvivalEVAL.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                            Function - Plot Decision Curve Analysis                                                           ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

"""
This module houses plotting functions used in the user-facing plot_graphs() 
function to plot net-benefit scores and net interventions avoided.
"""

def _get_colors(num_colors=None, seed_state=880209):
    """
    Generate a random tuple of colors of length num_colors

    Parameters
    ----------
    num_colors : int
        Number of colors to be outputted in tuple form

    Returns
    -------
    tuple
    """
    random.seed(seed_state)
    return [f"#{format(random.randint(0, 0xFFFFFF), '06x')}" for _ in range(num_colors)]


def _plot_net_benefit(
    plot_df: pd.DataFrame,
    xy_limits: bool = True,
    color_names: Iterable = None,
    show_grid: bool = True,
    show_legend: bool = True,
    smoothed_data: Optional[dict] = None,  # Corrected parameter
) -> None:
    """
    Plot net benefit values against threshold probability values. Can use pre-computed smoothed data if provided.

    Parameters
    ----------
    plot_df : pd.DataFrame
        Data containing threshold probability values and model columns of net benefit scores to be plotted.
    y_limits : Iterable[float], optional
        Tuple or list with two floats specifying the y-axis limits.
    color_names : Iterable[str], optional
        List of colors for each model line. Must match the number of unique models if provided.
    show_grid : bool, optional
        If True, display grid lines on the plot. Default is True.
    show_legend : bool, optional
        If True, display the legend on the plot. Default is True.
    smoothed_data : dict, optional
        Pre-computed smoothed data for each model. Keys are model names, and values are arrays with smoothed points.

    Raises
    ------
    ValueError
        If the input dataframe does not contain the required columns or if y_limits or color_names are incorrectly formatted.

    Returns
    -------
    None
    """

    # Validate input dataframe
    required_columns = ["threshold", "model", "net_benefit"]
    if not all(column in plot_df.columns for column in required_columns):
        raise ValueError(
            f"plot_df must contain the following columns: {', '.join(required_columns)}"
        )

    # Validate y_limits
    # if len(y_limits) != 2 or y_limits[0] >= y_limits[1]:
    #     raise ValueError("y_limits must contain two floats where the first is less than the second")

    # Validate color_names
    modelnames = plot_df["model"].unique()
    if color_names and len(color_names) != len(modelnames):
        raise ValueError("The length of color_names must match the number of unique models")

    # Plotting
    # Create a figure with a white background
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)  # Add a subplot to the figure
    ax.set_facecolor('white')  # Set the subplot's background color

    for idx, modelname in enumerate(plot_df["model"].unique()):
        color = color_names[idx]  # Directly use color from color_names by index
        model_df = plot_df[plot_df["model"] == modelname]
        if smoothed_data and modelname in smoothed_data:
            smoothed = smoothed_data[modelname]
            if not isinstance(smoothed, ndarray):
                raise ValueError(f"Smoothed data for '{modelname}' must be a NumPy array.")
            plt.plot(smoothed[:, 0], smoothed[:, 1], color=color, label=modelname)
        else:
            if modelname in ['None', 'All'] :
                plt.plot(model_df["threshold"], model_df["net_benefit"], color=color, label=modelname, linestyle=':')
            else:
                plt.plot(model_df["threshold"], model_df["net_benefit"], color=color, label=modelname)
    
    if xy_limits == True :
        plot_df_xy = plot_df[plot_df['net_benefit'] < 0][['model', 'threshold', 'net_benefit']].reset_index(drop =  True)
        idx_max_net_benefit = plot_df_xy.groupby('model')['net_benefit'].idxmax()
        max_net_benefit_df = plot_df_xy.loc[idx_max_net_benefit]
        
        y_limits = [-0.1, round(plot_df["net_benefit"].max() * 10) / 10]
        plt.xlim([0, round(max(max_net_benefit_df['threshold']), 1)])
    else:
        y_limits = [-0.5, round(plot_df["net_benefit"].max() * 10) / 10]
        plt.xlim([0, 1])

    plt.ylim(y_limits)
    if show_legend:
        plt.legend(fontsize=10)
    if show_grid:
        plt.grid(color="black", which="both", axis="both", linewidth="0.1")
    else:
        plt.grid(False)
    plt.xlabel("Threshold Probability")
    plt.ylabel("Net Benefit")
    plt.subplots_adjust(top=0.95)

    # Add outer box to the plot
    for spine in ax.spines.values():
        spine.set_visible(True)
        spine.set_color('black')  # Set box color
        spine.set_linewidth(0.5)  # Set box line width

def _plot_net_intervention_avoided(
    plot_df: pd.DataFrame,
    xy_limits: bool = True,
    color_names: Iterable = None,
    show_grid: bool = True,
    show_legend: bool = True,
    smoothed_data: Optional[dict] = None  # Updated to accept smoothed data
) -> None:
    """
    Plot net interventions avoided values against threshold probability values. Can use pre-computed smoothed data if provided.

    Parameters
    ----------
    plot_df : pd.DataFrame
        Data containing threshold probability values and model columns of net interventions avoided scores to be plotted.
    y_limits : Iterable[float]
        Tuple or list with two floats specifying the y-axis limits.
    color_names : Iterable[str]
        List of colors for each model line. Must match the number of unique models if provided.
    show_grid : bool
        If True, display grid lines on the plot. Default is True.
    show_legend : bool
        If True, display the legend on the plot. Default is True.
    smoothed_data : dict, optional
        Pre-computed smoothed data for each model. Keys are model names, and values are arrays with smoothed points.

    Raises
    ------
    ValueError
        If the input dataframe does not contain the required columns or if y_limits or color_names are incorrectly formatted.

    Returns
    -------
    None
    """

    # Validate input dataframe
    required_columns = ["threshold", "model", "net_intervention_avoided"]
    if not all(column in plot_df.columns for column in required_columns):
        raise ValueError(
            f"plot_df must contain the following columns: {', '.join(required_columns)}"
        )

    # Validate y_limits
    # if len(y_limits) != 2 or y_limits[0] >= y_limits[1]:
    #     raise ValueError("y_limits must contain two floats where the first is less than the second")

    # Validate color_names
    modelnames = plot_df["model"].unique()
    if color_names and len(color_names) != len(modelnames):
        raise ValueError("The length of color_names must match the number of unique models")

    # Plotting
    # Create a figure with a white background
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)  # Add a subplot to the figure
    ax.set_facecolor('white')  # Set the subplot's background color
    for idx, modelname in enumerate(plot_df["model"].unique()):
        color = color_names[idx]  # Directly use color from color_names by index
        model_df = plot_df[plot_df["model"] == modelname]
        if model_df.empty:  # Skip plotting for empty DataFrames
            continue
        if smoothed_data and modelname in smoothed_data:
            smoothed = smoothed_data[modelname]
            if smoothed_data and modelname in smoothed_data:
                if not isinstance(smoothed, ndarray):
                    raise ValueError(f"Smoothed data for '{modelname}' must be a NumPy array.")
            plt.plot(smoothed[:, 0], smoothed[:, 1], color=color, label=modelname)
        else:
            if modelname in ['None', 'All'] :
                plt.plot(model_df["threshold"], model_df["net_intervention_avoided"], color=color, label=modelname, linestyle=':')
            else:
                plt.plot(model_df["threshold"], model_df["net_intervention_avoided"], color=color, label=modelname)

    y_limits = [0, round(plot_df[(plot_df["threshold"] <= 0.5) & (~np.isinf(plot_df["net_intervention_avoided"]))]["net_intervention_avoided"].max() * 10) / 10]
    plt.ylim(y_limits)
    plt.xlim([0, 0.5])
    if show_legend:
        plt.legend(fontsize=10)
    if show_grid:
        plt.grid(color="black", which="both", axis="both", linewidth="0.3")
    else:
        plt.grid(False)
    plt.xlabel("Threshold Probability")
    plt.ylabel("Net Reduction of Interventions")
    plt.subplots_adjust(top=0.95)

    # Add outer box to the plot
    for spine in ax.spines.values():
        spine.set_visible(True)
        spine.set_color('black')  # Set box color
        spine.set_linewidth(0.5)  # Set box line width


def plot_dca_graphs(
    plot_df: pd.DataFrame,
    graph_type: str = "net_benefit",
    # y_limits: Iterable = (-0.05, 1),
    color_names: Optional[Iterable] = None,
    show_grid: bool = True,
    show_legend: bool = True,
    smooth_frac: float = 0.0,  # Default to 0, indicating no smoothing unless specified
    file_name: Optional[str] = None,
    dpi: int = 100,
    list_ml_model_name: list = [],
    list_ml_models: list = []
) -> None:
    """
    Plot specified graph type for the given data, either net benefit or net interventions avoided,
    against threshold probabilities. Applies LOWESS smoothing if `smooth_frac` is greater than 0,
    excluding 'all' and 'none' models from smoothing. The smoothing will be more sensitive to local variations,
    keeping the smoothed lines closer to the original data points if `smooth_frac` is specified.

    Parameters
    ----------
    plot_df : pd.DataFrame
        DataFrame containing 'threshold', 'model', and either 'net_benefit' or 'net_intervention_avoided' columns.
    graph_type : str, optional
        Specifies the type of plot to generate. Valid options are 'net_benefit' or 'net_intervention_avoided'.
    y_limits : Iterable[float], optional
        Two-element iterable specifying the lower and upper bounds of the y-axis.
    color_names : Iterable[str], optional
        List of colors to use for each line in the plot. Must match the number of models in `plot_df`.
    show_grid : bool, optional
        If True, display grid lines on the plot. Default is True.
    show_legend : bool, optional
        If True, display the legend on the plot. Default is True.
    smooth_frac : float, optional
        Fraction of data points used when estimating each y-value in the smoothed line,
        making the smoothing more sensitive to local variations. Set to 0 for no smoothing. Default is 0.
    file_name : str, optional
        Path and file name where the figure will be saved. If None, the figure is not saved.
    dpi : int, optional
        Resolution of the saved figure in dots per inch.

    Raises
    ------
    ValueError
        If `graph_type` is not recognized.
        If `y_limits` does not contain exactly two elements or if the lower limit is not less than the upper limit.
        If `color_names` is provided but does not match the number of models in `plot_df`.
        If `smooth_frac` is not within the 0 to 1 range.
        If the input DataFrame is empty.

    Returns
    -------
    None
    """

    if plot_df.empty:
        raise ValueError("The input DataFrame is empty.")

    if graph_type not in ["net_benefit", "net_intervention_avoided"]:
        raise ValueError("graph_type must be 'net_benefit' or 'net_intervention_avoided'")

    # if len(y_limits) != 2 or y_limits[0] >= y_limits[1]:
    #     raise ValueError("y_limits must contain two floats where the first is less than the second")

    if not 0 <= smooth_frac <= 1:
        raise ValueError("smooth_frac must be between 0 and 1")

    modelnames = plot_df["model"].unique()
    if color_names is None:
        color_names = _get_colors(num_colors=len(modelnames))
    elif len(color_names) < len(modelnames):
        raise ValueError("color_names must match the number of unique models in plot_df")

    smoothed_data = {}
    if smooth_frac > 0:  # Apply smoothing only if smooth_frac is greater than 0
        lowess = sm.nonparametric.lowess
        for modelname in plot_df["model"].unique():
            # Skip 'all' and 'none' models from smoothing
            if modelname.lower() in ['all', 'none']:
                continue

            model_df = plot_df[plot_df["model"] == modelname]
            y_col = "net_benefit" if graph_type == "net_benefit" else "net_intervention_avoided"
            smoothed_data[modelname] = lowess(model_df[y_col], model_df["threshold"], frac=smooth_frac)

    plot_function = _plot_net_benefit if graph_type == "net_benefit" else _plot_net_intervention_avoided
    plot_function(
        plot_df=plot_df,
        # y_limits=y_limits,
        color_names=color_names,
        show_grid=show_grid,
        show_legend=show_legend,
        smoothed_data=smoothed_data if smooth_frac > 0 else None,  # Pass smoothed_data only if smoothing was applied
    )

    if file_name:
        try:
            plt.savefig(file_name, dpi=dpi)
        except Exception as e:
            print(f"Error saving figure: {e}")
    
    # Output individual DCA plot
    for dca_loop in range(len(list_ml_model_name)):
        plot_df_ind = plot_df[ plot_df['model'].isin([list_ml_model_name[dca_loop], 'All', 'None']) ]
        color_names_ind = [color_names[x] for x in [dca_loop, len(color_names)-2, len(color_names)-1]]

        plot_function = _plot_net_benefit if graph_type == "net_benefit" else _plot_net_intervention_avoided
        plot_function(
            plot_df=plot_df_ind,
            xy_limits=False,
            color_names=color_names_ind,
            show_grid=show_grid,
            show_legend=show_legend,
            smoothed_data=smoothed_data if smooth_frac > 0 else None,  # Pass smoothed_data only if smoothing was applied
        )
        if file_name:
            try:
                plt.savefig(file_name+'_'+list_ml_models[dca_loop], dpi=dpi)
            except Exception as e:
                print(f"Error saving figure: {e}")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                               Function - Survival Probability Calibration                                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def survival_probability_calibration_values(predicted_values, duration, event, time_point):
    """
    Smoothed calibration curves for time-to-event models. This is analogous to
    calibration curves for classification models, extended to handle survival probabilities
    and censoring. Produces a matplotlib figure and some metrics.

    We want to calibrate our model's prediction of :math:`P(T < \text{t0})` against the observed frequencies.

    Parameters
    -------------

    model:
        a fitted lifelines regression model to be evaluated
    df: DataFrame
        a DataFrame - if equal to the training data, then this is an in-sample calibration. Could also be an out-of-sample
        dataset.
    t0: float
        the time to evaluate the probability of event occurring prior at.

    Returns
    ----------

    https://onlinelibrary.wiley.com/doi/full/10.1002/sim.8570

    """
    # Cut data into deciles based on the predicted survival probabilities
    val_data = pd.DataFrame({'predicted_values': predicted_values, 'duration': duration, 'event': event})
    if len(np.unique(predicted_values)) > 1 :
        val_data['decile'] = pd.qcut(val_data['predicted_values'], 20, labels=False, duplicates='drop')
    else :
        val_data['decile'] = 1

    # Calculate observed survival probabilities in each decile
    observed_probabilities = []
    for decile in np.unique(val_data['decile']) :
        subset = val_data[val_data['decile'] == decile]
        kmf = KaplanMeierFitter()
        kmf.fit(subset['duration'], subset['event'])
        observed_probabilities.append( kmf.predict(time_point) )
    
    predicted_probabilities = val_data.groupby('decile')['predicted_values'].mean().values
    return( predicted_probabilities, np.array(observed_probabilities) )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                              Function - Decision Curve Analysis (Hold-out)                                                              ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Function - measure the area under the net benefit (area and threshold)
def area_net_benefit_threshold_measure(data, step) :
    area_nb_sum = 0
    area_nb_sum2 = 0
    ls_threshold = []
    list_area_nb = data['net_benefit'].tolist()
    list_threshold = data['threshold'].tolist()
    for ii in range(data.shape[0] - 1) :
        area_nb_sum2 = area_nb_sum2 + ( list_area_nb[ii] + list_area_nb[ii+1] )*step/2
        if (list_area_nb[ii] + list_area_nb[ii+1]) > 0 :
            area_nb_sum = area_nb_sum + ( list_area_nb[ii] + list_area_nb[ii+1] )*step/2
            ls_threshold = ls_threshold + [list_threshold[ii], list_threshold[ii+1]]
        else:
            pass
    if len(ls_threshold) == 0:
        ls_threshold = [0]
    return(area_nb_sum, area_nb_sum2, ls_threshold)

# DCA without resampling
def measure_aunbc(data, step, model_nb1, model_nb2) :
    """
    Compute the area under the net benefit curve
    -----------
    Parameters:
    data: data.frame - true and predicted outcome.
    model_nb1: name of model 1
    model_nb2: name of model 2
    step: step of width of the threshold probability
    -----------
    Returns:
    list: difference of area under net benefit curve of two models
    """
    # Measurement of the area of the net benefit
    df_area_nb1 = data[ data['model'] == model_nb1 ]
    nb1 = df_area_nb1['net_benefit'].tolist()

    df_area_nb2 = data[ data['model'] == model_nb2 ]
    nb2 = df_area_nb2['net_benefit'].tolist()

    area_nb_sum1 = 0
    area_nb_sum2 = 0
    for ii in range(len(nb1) - 1) :
        if (nb1[ii] + nb1[ii+1]) > 0 :
            area_nb_sum1 = area_nb_sum1 + ( nb1[ii] + nb1[ii+1] )*step/2
        else:
            pass

        if (nb2[ii] + nb2[ii+1]) > 0 :
            area_nb_sum2 = area_nb_sum2 + ( nb2[ii] + nb2[ii+1] )*step/2
        else:
            pass
    
    return(area_nb_sum2 - area_nb_sum1)

# Function - measure the difference of area under the net benefit (positive)
def area_net_benefit_diff(data, step, model_nb1, model_nb2) :
    # Statistics for each bootstrapping resamples
    num_resamples = len(data)
    list_diff_area = []
    for jj in range(num_resamples) :
        data_nb_model = data[jj]
        # Measurement of the area of the net benefit
        list_diff_area.append( measure_aunbc(data = data_nb_model, step = step, model_nb1 = model_nb1, model_nb2 = model_nb2) )
        
    return(list_diff_area)


# Bootstrapping for DCA
def bootstrap_dca(data, num_resamples, type_ana_set, models, time_point = 4, n_jobs_dca = 20):
    """
    Perform bootstrap resampling and compute the net benefit (DCA).
    -----------
    Parameters:
    data: data.frame - true and predicted outcome.
    num_resamples (int): Number of bootstrap resamples.
    -----------
    Returns:
    list: list of data frame with net benefit by model
    """
    data = data.reset_index(drop = True)
    n = data.shape[0]
    np.random.seed(890701)
    bootstrap_samples = np.random.choice(data.index, (num_resamples, n), replace = True)

    def bootstrap_dca_resamples(jj) :
        bootstrap_data = data.iloc[bootstrap_samples[jj]]
        # To measure the net benefits in each threshold
        if type_ana_set == 'classification' :
            dca_model = dca( data = bootstrap_data
                            ,outcome = 'true_labels'
                            ,modelnames = models )
        else :
            dca_model = dca( data = bootstrap_data
                            ,outcome = 'true_labels'
                            ,modelnames = models
                            ,time = time_point
                            ,time_to_outcome_col='true_time' )
        return(dca_model)
    
    time_start = datetime.now()
    data_dca_model = Parallel(n_jobs = n_jobs_dca)(
        delayed( bootstrap_dca_resamples )(jj = jj)
        for jj in range(num_resamples) )
    time_end = datetime.now()
    print("Total running time (DCA): " + str(time_end - time_start))

    return(data_dca_model)


# Bootstrapping hypothesis testing p-value
def bootstrap_hypothesis_testing_p_value(observed_statistic, bootstrap_statistics) :
    """
    Calculate the p-value based on bootstrap statistics.
    -----------
    Parameters:
    observed_statistic (float): The observed statistic from the original data.
    bootstrap_statistics (np.ndarray): Array of bootstrap statistics.
    -----------
    Returns:
    float: The p-value.
    """
    abs_diff = np.abs(bootstrap_statistics - observed_statistic)
    abs_observed = np.abs(observed_statistic)
    p_value = np.mean(abs_diff > abs_observed)
    return(p_value)


class dca_hold_out:
    """
    type_ana_set : str
        Type of analysis: classification or survival
    repo_prefix : list
        The model repository of the cohort analysis
    fig_repo_out : str
        The output repository
    fig_prefix : str
        The prefix of the name of the output
    fig_prefix_add : str
        The second prefix of the name of the output
    list_ml_models : list
        The list of the algorithms
    list_ml_model_name : list
        The list of the name of the algorithms
    dpi : int
        Resolution of the saved figure in dots per inch.
    """
    def __init__(self
                 ,type_ana_set
                 ,repo_prefix
                 ,fig_repo_out
                 ,fig_prefix
                 ,list_ml_models
                 ,list_ml_model_name
                 ,fig_prefix_add = ""
                 ,dpi = 100 ):

        self.type_ana_set = type_ana_set
        self.repo_prefix = repo_prefix
        self.fig_repo_out = fig_repo_out
        self.fig_prefix = fig_prefix
        self.fig_prefix_add = fig_prefix_add
        self.list_ml_models = list_ml_models
        self.list_ml_model_name = list_ml_model_name
        self.dpi = dpi
    
    def load_models(self, time_point = 4) :
        """
        ------
        Return
        ------
        list_best_model: list of the best model for each algorithm
        data_best_model: the true and predicted outcome for each algorithm
        """
        self.time_point = time_point
        # To load the best model for each algorithm
        list_best_model = dict()
        for loop_list_ml_models in self.list_ml_models :
            # Name of the algorithm
            loop_list_ml_models_name = self.list_ml_model_name[ self.list_ml_models.index(loop_list_ml_models) ]

            # Load the best model
            best_model = load_object(filename = "ml_model_"+loop_list_ml_models+loop_ml_prefix, file_repo = self.repo_prefix )
            list_best_model[loop_list_ml_models] = best_model

            # Predicted values
            if self.type_ana_set == 'classification' :
                predicted_values = best_model.best_model_proba

                # Create the data frame
                if loop_list_ml_models == self.list_ml_models[0] :
                    data_best_model = pd.DataFrame({'true_labels' : best_model.y_validate
                                                    ,loop_list_ml_models_name : predicted_values })
                else :
                    data_best_model[loop_list_ml_models_name] = predicted_values
            else :
                # if min(best_model.best_model_predictions) < 0 :
                #     predicted_values = best_model.best_model_predictions - min(best_model.best_model_predictions)
                # else :
                #     predicted_values = best_model.best_model_predictions
                if loop_list_ml_models in ['lsvm', 'nlsvm'] :
                    predicted_values_ = svm_predict_survival_function()
                    predicted_values_.fit(svm_model = best_model.best_model, X = best_model.X_training, y = best_model.y_training)
                    predicted_surv_values = predicted_values_.predict_survival_function(X = best_model.X_validate)
                else :
                    predicted_surv_values = best_model.best_model.predict_survival_function(best_model.X_validate)
                
                # Predicted survival probability at time_point
                list_time = predicted_surv_values[0].x
                abs_diff = np.abs(list_time - time_point)
                min_index = np.argmin(abs_diff)
                closest_value = list_time[min_index]
                idx_time = list_time.tolist().index(closest_value)
                # df_surv_proba = pd.DataFrame({'time': list_time})
                predicted_values_tmp = []
                for surv_proba_loop in range(len(predicted_surv_values)) :
                    # df_surv_proba[surv_proba_loop] = predicted_surv_values[surv_proba_loop].y
                    predicted_values_tmp.append( predicted_surv_values[surv_proba_loop].y[idx_time] )
                # df_surv_proba.set_index('time', inplace = True)
                predicted_values = [1 - val for val in predicted_values_tmp]

                # Create the data frame
                if loop_list_ml_models == self.list_ml_models[0] :
                    data_best_model = pd.DataFrame({'true_labels' : best_model.status_validate
                                                    ,'true_time': best_model.time_validate
                                                    ,loop_list_ml_models_name : predicted_values })
                else :
                    data_best_model[loop_list_ml_models_name] = predicted_values

        self.list_best_model = list_best_model
        self.data_best_model = data_best_model
        
    def dca_net_plots(self) :
        """
        time : int or float
            Time of interest in years, used in Survival DCA
        ------
        Return
        ------
        To generate the net benefits vs threshold plot
        """

        # To measure the net benefits in each threshold
        if self.type_ana_set == 'classification' :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name )
        else :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name
                            ,time = self.time_point
                            ,time_to_outcome_col='true_time' )
        dca_model['model'] = np.where( dca_model['model'] == 'all', 'All', np.where( dca_model['model'] == 'none', 'None', dca_model['model'] ) )
        
        # To generate the plot of net benefit vs threshold probability
        plot_dca_graphs( plot_df = dca_model
                    ,graph_type = 'net_benefit'
                    ,file_name = self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Net_Benefit'+self.fig_prefix+self.fig_prefix_add
                    ,show_grid = False
                    ,show_legend = True
                    ,dpi = self.dpi
                    ,list_ml_model_name = self.list_ml_model_name
                    ,list_ml_models = self.list_ml_models )
        
        # To generate the plot of net intervention avoided vs threshold probability
        plot_dca_graphs( plot_df = dca_model
                    ,graph_type = 'net_intervention_avoided'
                    ,file_name = self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Net_Intervention'+self.fig_prefix+self.fig_prefix_add
                    ,show_grid = False
                    ,show_legend = True
                    ,dpi = self.dpi
                    ,list_ml_model_name = self.list_ml_model_name
                    ,list_ml_models = self.list_ml_models )
        
        # Calculate the rank of 'net_benefit' and 'net_intervention_avoided' in for each model in each threshold
        dca_model['rank_net_benefit'] = dca_model.groupby('threshold')['net_benefit'].rank(ascending=False)
        dca_model['rank_net_intervention_avoided'] = dca_model.groupby('threshold')['net_intervention_avoided'].rank(ascending=False)

        # Average of 'net_benefit' and 'net_intervention_avoided
        tbl_average_net_benefits = pd.DataFrame({'model': dca_model['model'].unique()})
        tbl_average_net_intervention_avoided = pd.DataFrame({'model': dca_model['model'].unique()})
        for threshold_loop in [round(0.1 + 0.1 * i, 1) for i in range(int((0.9 - 0.1) / 0.1) + 1)] :
            # Filter the threshold cutoff and remove the missing ranking
            dca_model_net_benefits = dca_model[ (~dca_model['net_benefit'].isna()) & (dca_model['threshold'] <= threshold_loop) ]
            dca_model_net_intervention_avoided = dca_model[ (~dca_model['net_intervention_avoided'].isna()) & (dca_model['threshold'] <= threshold_loop) ]
            # Average of the ranking
            average_net_benefits = dca_model_net_benefits.groupby('model')[['rank_net_benefit']].mean()
            average_net_intervention_avoided = dca_model_net_intervention_avoided.groupby('model')[['rank_net_intervention_avoided']].mean()
            # Reset the index and rename the column name
            average_net_benefits.reset_index(inplace=True)
            average_net_benefits.rename(columns = {'rank_net_benefit': 'net_benefit_'+str(threshold_loop)}, inplace=True)
            average_net_intervention_avoided.reset_index(inplace=True)
            average_net_intervention_avoided.rename(columns = {'rank_net_intervention_avoided': 'net_intervention_avoided_'+str(threshold_loop)}, inplace=True)
            # Append the table
            tbl_average_net_benefits = pd.merge(tbl_average_net_benefits, average_net_benefits, on = ['model'], how = 'left')
            tbl_average_net_intervention_avoided = pd.merge(tbl_average_net_intervention_avoided, average_net_intervention_avoided, on = ['model'], how = 'left')
        
        # Ranking of the average of 'net_benefit'
        columns_net_benefits = [x for x in tbl_average_net_benefits.columns if x not in ['model']]
        columns_net_benefits_rank = ['rank_'+x for x in columns_net_benefits]
        rename_dict = dict(zip(columns_net_benefits, columns_net_benefits_rank))
        tbl_rank_net_benefits = tbl_average_net_benefits[columns_net_benefits].rank()
        tbl_rank_net_benefits.rename(columns = rename_dict, inplace = True)
        table_average_net_benefits = pd.concat([tbl_average_net_benefits, tbl_rank_net_benefits], axis = 1)
        table_average_net_benefits.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Net_Benefits'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Ranking of the average of 'net_intervention_avoided'
        columns_net_intervention_avoided = [x for x in tbl_average_net_intervention_avoided.columns if x not in ['model']]
        columns_net_intervention_avoided_rank = ['rank_'+x for x in columns_net_intervention_avoided]
        rename_dict = dict(zip(columns_net_intervention_avoided, columns_net_intervention_avoided_rank))
        tbl_rank_net_intervention_avoided = tbl_average_net_intervention_avoided[columns_net_intervention_avoided].rank()
        tbl_rank_net_intervention_avoided.rename(columns = rename_dict, inplace = True)
        table_average_net_intervention_avoided = pd.concat([tbl_average_net_intervention_avoided, tbl_rank_net_intervention_avoided], axis = 1)
        table_average_net_intervention_avoided.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Net_Intervention_Avoided'+self.fig_prefix+self.fig_prefix_add+'.csv')


    def dca_area_net_benefit(self) :
        """
        self
        ------
        Return
        ------
        To generate the area under net benefits vs threshold
        """

        # To measure the net benefits in each threshold
        if self.type_ana_set == 'classification' :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name )
        else :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name
                            ,time = self.time_point
                            ,time_to_outcome_col='true_time' )
        dca_model['model'] = np.where( dca_model['model'] == 'all', 'All', np.where( dca_model['model'] == 'none', 'None', dca_model['model'] ) )

        # Calculate the step
        step = dca_model['threshold'][1] - dca_model['threshold'][0]

        # Area under the net benefit (positive) for each model
        area_nb_comb = []
        area_nb_all_comb = []
        min_threshold = []
        max_threshold = []
        for model_nb in ['All'] + self.list_ml_model_name :
            # Measurement of the area of the net benefit
            df_area_nb = dca_model[ dca_model['model'] == model_nb ]
            area_nb_sum, area_nb_sum_all, ls_threshold = area_net_benefit_threshold_measure(data = df_area_nb, step = step)
            area_nb_comb.append( area_nb_sum )
            area_nb_all_comb.append( area_nb_sum_all )
            min_threshold.append( min(ls_threshold) )
            max_threshold.append( max(ls_threshold) )
        
        tbl_area_nb = pd.DataFrame({'Model': ['All'] + self.list_ml_model_name
                                    ,'Area net benefit': area_nb_comb
                                    ,'Area net benefit (All)': area_nb_all_comb
                                    ,'Difference Area to All': area_nb_comb - area_nb_comb[0]
                                    ,'Threshold (min)': min_threshold
                                    ,'Threshold (max)': max_threshold })
        tbl_area_nb.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Area_Net_Benefit'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Measure the net benefit in each bootstrapping resamples
        bootstrap_nb = bootstrap_dca(data = self.data_best_model, num_resamples = 1000, type_ana_set = self.type_ana_set, models = self.list_ml_model_name, time_point = self.time_point)

        # Bootsrapping p-value for comparing two models
        list_model_1 = []
        list_model_2 = []
        list_p_value = []
        list_all_model = self.list_ml_model_name
        for i_nb1 in range(len(list_all_model)) :
            for i_nb2 in range(len(list_all_model)) :
                if (i_nb1 != i_nb2) & (i_nb1 < i_nb2) :
                    # Model name
                    model_nb1 = list_all_model[i_nb1]
                    model_nb2 = list_all_model[i_nb2]

                    # Compute bootstrapping statistics (difference of area under the net benefit curve)
                    bootstrap_stat = area_net_benefit_diff(data = bootstrap_nb, step = step, model_nb1 = model_nb1, model_nb2 = model_nb2)

                    # Calculate p-value
                    observed_stat = measure_aunbc(data = dca_model, step = step, model_nb1 = model_nb1, model_nb2 = model_nb2)
                    p_value = bootstrap_hypothesis_testing_p_value(observed_statistic = observed_stat, bootstrap_statistics = bootstrap_stat)

                    # Append the results
                    list_model_1.append(model_nb1)
                    list_model_2.append(model_nb2)
                    list_p_value.append(p_value)
                else :
                    pass
        
        # Adjusted p-values by performing multiple testing corrections (Benjamini-Hochberg)
        _, pvals_bonferroni, _, _ = multipletests(list_p_value, alpha=0.05, method='bonferroni')
        
        # Output table
        tbl_bootstrap_p_value = pd.DataFrame({'Model 1': list_model_1
                                              ,'Model 2': list_model_2
                                              ,'p-value': list_p_value
                                              ,'adj p-value': pvals_bonferroni})
        tbl_bootstrap_p_value.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Area_Net_Benefit_Bootstrap_P_Values'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Plot of the area under the net benefit (bar plot)
        fig = plt.figure(figsize=(12, 6))
        ax = fig.add_subplot(111)  # Add a subplot to the figure
        ax.set_facecolor('white')  # Set the subplot's background color

        # Excluding the 'ALL'
        tbl_area_nb_excl = tbl_area_nb[tbl_area_nb['Model'] != 'All'].reset_index(drop = True)
        tbl_area_nb_excl['Model'] = tbl_area_nb_excl['Model'].str.replace('-', '-\n').replace(' ', '\n')
        tbl_bootstrap_p_value_excl = tbl_bootstrap_p_value[tbl_bootstrap_p_value['Model 1'] != 'All'].reset_index(drop = True)
        tbl_bootstrap_p_value_excl['Model 1'] = tbl_bootstrap_p_value_excl['Model 1'].str.replace('-', '-\n').replace(' ', '\n')
        tbl_bootstrap_p_value_excl['Model 2'] = tbl_bootstrap_p_value_excl['Model 2'].str.replace('-', '-\n').replace(' ', '\n')

        # Plot the area under the net benefit
        models = tbl_area_nb_excl['Model']
        areas = tbl_area_nb_excl['Area net benefit']
        colors = ['#6a76c4' if value >= 0 else '#cf5b89' for value in tbl_area_nb_excl['Area net benefit (All)']]
        bars = ax.bar(models, areas, color=colors)

        # Adding labels to each bar
        for bar, diff in zip(bars, tbl_area_nb_excl['Difference Area to All']):
            height = max(areas)/20
            arrow = '\u2191' if diff > 0 else '\u2193'
            ax.text(
                bar.get_x() + bar.get_width() / 2,
                height,
                f'{arrow} {diff:.3f}',
                ha='center',
                va='bottom',
                fontsize=8 )

        # Highlight significant comparisons with horizontal lines above the bars
        significant_comparisons = tbl_bootstrap_p_value_excl[tbl_bootstrap_p_value_excl['adj p-value'] < 0.05]

        # Calculate the average height for each group of Model 1
        avg_heights = {}
        for model1 in significant_comparisons['Model 1'].drop_duplicates(keep='first'):
            avg_heights[model1] = max(areas) + (significant_comparisons['Model 1'].drop_duplicates(keep='first').tolist().index(model1) / 100) + (max(areas) / 15)
            
        # Plotting significant comparisons
        for i, (idx, row) in enumerate(significant_comparisons.iterrows()):
            model1 = row['Model 1']
            model2 = row['Model 2']
            idx1 = models[models == model1].index[0]
            idx2 = models[models == model2].index[0]
            x1 = bars[idx1].get_x() + bars[idx1].get_width() / 2
            x2 = bars[idx2].get_x() + bars[idx2].get_width() / 2
            y = avg_heights[model1]
            ax.plot([x1, x2], [y, y], "k-", lw=1)
            ax.plot([x1, x1], [y - max(areas)/40, y], "k-", lw=1)
            ax.plot([x2, x2], [y - max(areas)/40, y], "k-", lw=1)

            # Add "***" below the line if p-value < 0.001
            if row['adj p-value'] < 0.001:
                ax.text(x2-0.5, y - max(areas)/25, "***", ha='center', size = 10)
            elif row['adj p-value'] < 0.01:
                # Add "**" below the line if p-value < 0.01
                ax.text(x2-0.5, y - max(areas)/25, "**", ha='center', size = 10)
            elif row['adj p-value'] < 0.05:
                # Add "*" below the line if p-value < 0.05
                ax.text(x2-0.5, y - max(areas)/25, "*", ha='center', size = 10)
            else :
                pass

        # Adding labels and title
        ax.set_ylabel("Area under net benefit", fontsize=10)
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=10)
        plt.subplots_adjust(top=0.95)  # Reduce the top margin (1 is the default)

        # Adding custom legend
        custom_lines = [
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='*     Adj. p-value < 0.05'),
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='**   Adj. p-value < 0.01'),
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='*** Adj. p-value < 0.001') ]
        ax.legend(handles=custom_lines, loc='upper left', fontsize=10)

        # Add outer box to the plot
        for spine in ax.spines.values():
            spine.set_visible(True)
            spine.set_color('black')  # Set box color
            spine.set_linewidth(0.5)  # Set box line width
        
        # Save the plot
        plt.savefig(self.fig_repo_out+'Decision Curve Analysis/DCA_Hold_Out_Area_Net_Benefit'+self.fig_prefix+self.fig_prefix_add, dpi=self.dpi)

        

    def calibration_plots(self) :
        """
        ------
        Return
        ------
        To generate calibration plot
        """
        
        # List of models
        clf_list = list(zip(self.list_best_model.values(), self.list_ml_model_name))

        # Set colors
        color_names = _get_colors(num_colors=len(self.list_ml_model_name))

        if self.type_ana_set == 'classification' :
            # Calibration plot
            fig = plt.figure(figsize=(12, 8))
            ax = fig.add_subplot(111)  # Add a subplot to the figure
            ax.set_facecolor('white')  # Set the subplot's background color

            colors = plt.get_cmap("Dark2")
            calibration_displays = {}

            for i, (clf, name) in enumerate(clf_list):
                prob_pos = clf.best_model_proba
                fraction_of_positives, mean_predicted_value = calibration_curve(clf.y_validate, prob_pos, n_bins = 10)

                # Calibration plot
                ax.plot(mean_predicted_value, fraction_of_positives, marker='o', label=name, color=color_names[i])

            ax.plot([0, 1], [0, 1], linestyle='--', color='black', label='Perfectly Calibrated')  # Diagonal line
            ax.set_xlabel('Mean Predicted Probability')
            ax.set_ylabel('Fraction of Positives')
            ax.set_title('Calibration Curve')
            ax.legend(fontsize=10)
            ax.grid()

            plt.tight_layout()

            # Add outer box to the plot
            for spine in ax.spines.values():
                spine.set_visible(True)
                spine.set_color('black')  # Set box color
                spine.set_linewidth(0.5)  # Set box line width
                
            # Table of the calibration slopes for each model
            list_calibration_slope = []
            list_mse = []
            for i, (clf, name) in enumerate(clf_list):
                # Calibration slope
                lr = LinearRegression()
                lr.fit(clf.best_model_proba.reshape(-1, 1), clf.y_validate)
                calibration_slope = lr.coef_[0]
                list_calibration_slope.append(calibration_slope)

                # Mean squared error (MSE) to the perfect calibration
                mse = mean_squared_error(clf.best_model_proba, clf.y_validate)
                list_mse.append(mse)
            
            df_calibration_slope = pd.DataFrame({'Model': self.list_ml_model_name
                                                ,'Calibration slope' : list_calibration_slope
                                                ,'MSE' : list_mse })
            df_calibration_slope['ABS(1 - slope)'] = abs(1 - df_calibration_slope['Calibration slope'])
            df_calibration_slope['Rank ABS(1 - slope)'] = df_calibration_slope['ABS(1 - slope)'].rank()
            df_calibration_slope['Rank MSE'] = df_calibration_slope['MSE'].rank()
        
        else :
            # Calibration plot
            fig = plt.figure(figsize=(10, 8))
            ax = fig.add_subplot(111)  # Add a subplot to the figure
            ax.set_facecolor('white')  # Set the subplot's background color

            colors = plt.get_cmap("Dark2")
            calibration_displays = {}

            for i, (clf, name) in enumerate(clf_list):
                # prob_pos = clf.best_model_proba
                if name in ['NLSVM', 'LSVM'] :
                    predicted_values_ = svm_predict_survival_function()
                    predicted_values_.fit(svm_model = clf.best_model, X = clf.X_training, y = clf.y_training)
                    predicted_surv_values = predicted_values_.predict_survival_function(X = clf.X_validate)
                else :
                    predicted_surv_values = clf.best_model.predict_survival_function(clf.X_validate)
                
                # Predicted survival probability at time_point
                list_time = predicted_surv_values[0].x
                abs_diff = np.abs(list_time - self.time_point)
                min_index = np.argmin(abs_diff)
                closest_value = list_time[min_index]
                idx_time = list_time.tolist().index(closest_value)

                predicted_values = []
                for surv_proba_loop in range(len(predicted_surv_values)) :
                    predicted_values.append( predicted_surv_values[surv_proba_loop].y[idx_time] )

                # Perform calibration
                predicted_probabilities, observed_probabilities = survival_probability_calibration_values(predicted_values
                                                                                                          ,duration = clf.time_validate
                                                                                                          ,event = clf.status_validate
                                                                                                          ,time_point = self.time_point)
                
                # Calibration plot
                ax.plot(predicted_probabilities, observed_probabilities, marker='o', label=name, color=color_names[i])

            ax.plot([0, 1], [0, 1], linestyle='--', color='black', label='Perfectly Calibrated')  # Diagonal line
            ax.set_xlabel('Mean Predicted Probability')
            ax.set_ylabel('Fraction of Positives')
            ax.set_title('Calibration Curve')
            ax.legend()
            ax.grid()

            plt.tight_layout()

            # Add outer box to the plot
            for spine in ax.spines.values():
                spine.set_visible(True)
                spine.set_color('black')  # Set box color
                spine.set_linewidth(0.5)  # Set box line width
                
            # Table of the calibration slopes for each model
            list_calibration_slope = []
            list_mse = []
            list_d_cal = []
            for i, (clf, name) in enumerate(clf_list):
                # prob_pos = clf.best_model_proba
                if name in ['NLSVM', 'LSVM'] :
                    predicted_values_ = svm_predict_survival_function()
                    predicted_values_.fit(svm_model = clf.best_model, X = clf.X_training, y = clf.y_training)
                    predicted_surv_values = predicted_values_.predict_survival_function(X = clf.X_validate)
                else :
                    predicted_surv_values = clf.best_model.predict_survival_function(clf.X_validate)
                
                # Predicted survival probability at time_point
                list_time = predicted_surv_values[0].x
                abs_diff = np.abs(list_time - self.time_point)
                min_index = np.argmin(abs_diff)
                closest_value = list_time[min_index]
                idx_time = list_time.tolist().index(closest_value)
                predicted_values_tmp = []
                for surv_proba_loop in range(len(predicted_surv_values)) :
                    predicted_values_tmp.append( predicted_surv_values[surv_proba_loop].y[idx_time] )
                prob_pos = [1 - val for val in predicted_values_tmp]
                prob_pos_array = np.array(prob_pos)

                # Observed survival probability
                kmf = KaplanMeierFitter()
                kmf.fit(clf.time_training, event_observed = clf.status_training)
                observed_values = kmf.predict(clf.time_validate)
                prob_obs_array = np.array(observed_values.reset_index()['KM_estimate'])

                # Calibration slope
                lr = LinearRegression()
                lr.fit(prob_pos_array.reshape(-1, 1), prob_obs_array)
                calibration_slope = lr.coef_[0]
                list_calibration_slope.append(calibration_slope)

                # Mean squared error (MSE) to the perfect calibration
                mse = mean_squared_error(prob_pos, prob_obs_array)
                list_mse.append(mse)

                # D-Calibration (SurvivalEVAL package)
                survival_curves = pd.DataFrame({'time': predicted_surv_values[0].x})
                for surv_loop in range(len(predicted_surv_values)) :
                    surv_vals = predicted_surv_values[surv_loop]
                    survival_curves = pd.merge(survival_curves
                                            ,pd.DataFrame({'time': predicted_surv_values[0].x, surv_loop: surv_vals.y})
                                            ,on = ['time'], how = 'left')
                survival_curves.set_index('time', inplace=True)

                eval = LifelinesEvaluator(survival_curves, clf.time_validate, clf.status_validate,
                          clf.time_training, clf.status_training)
                # Calculate the D-Calibration score. Null hypothesis: Perfect calibration
                # [1]: The p-value of the D-Calibration test.
                # [2]: The binning histogram of the D-Calibration test.
                d_cal = eval.d_calibration()
                list_d_cal.append(d_cal[0])
                # Compute the one calibration score for a given set of predictions and true event times.
                # [1]: The one calibration score.
                # [2]: The observed probabilities in each bin.
                # [3]: The expected probabilities in each bin.
                one_cal = eval.one_calibration(target_time=self.time_point)
            
            df_calibration_slope = pd.DataFrame({'Model': self.list_ml_model_name
                                                ,'Calibration slope' : list_calibration_slope
                                                ,'MSE' : list_mse
                                                ,'D-calibration (p-values)': list_d_cal })
            df_calibration_slope['ABS(1 - slope)'] = abs(1 - df_calibration_slope['Calibration slope'])
            df_calibration_slope['Rank ABS(1 - slope)'] = df_calibration_slope['ABS(1 - slope)'].rank()
            df_calibration_slope['Rank MSE'] = df_calibration_slope['MSE'].rank()
            df_calibration_slope['Rank D-calibration'] = df_calibration_slope['D-calibration (p-values)'].rank(ascending = False)

        # Save the plot and table for the calibration analysis
        plt.savefig(self.fig_repo_out+'Decision Curve Analysis/Calibration_Hold_Out'+self.fig_prefix+self.fig_prefix_add, dpi=self.dpi)
        df_calibration_slope.to_csv(self.fig_repo_out+'Decision Curve Analysis/Calibration_Slope'+self.fig_prefix+self.fig_prefix_add+'.csv')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                          Function - Decision Curve Analysis (Cross-Validation)                                                          ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class dca_cross_validation:
    """
    type_ana_set : str
        Type of analysis: classification or survival
    repo_prefix : list
        The model repository of the cohort analysis
    fig_repo_out : str
        The output repository
    fig_prefix : str
        The prefix of the name of the output
    fig_prefix_add : str
        The second prefix of the name of the output
    list_ml_models : list
        The list of the algorithms
    list_ml_model_name : list
        The list of the name of the algorithms
    dpi : int
        Resolution of the saved figure in dots per inch.
    """
    def __init__(self
                 ,type_ana_set
                 ,repo_prefix
                 ,fig_repo_out
                 ,fig_prefix
                 ,list_ml_models
                 ,list_ml_model_name
                 ,fig_prefix_add = ""
                 ,dpi = 100 ):

        self.type_ana_set = type_ana_set
        self.repo_prefix = repo_prefix
        self.fig_repo_out = fig_repo_out
        self.fig_prefix = fig_prefix
        self.fig_prefix_add = fig_prefix_add
        self.list_ml_models = list_ml_models
        self.list_ml_model_name = list_ml_model_name
        self.dpi = dpi
    
    def load_models(self, time_point = 4) :
        """
        ------
        Return
        ------
        list_best_model: list of the best model for each algorithm
        data_best_model: the true and predicted outcome for each algorithm
        """
        self.time_point = time_point
        # To load the best model for each algorithm
        list_best_model = dict()
        for loop_list_ml_models in self.list_ml_models :
            # Name of the algorithm
            loop_list_ml_models_name = self.list_ml_model_name[ self.list_ml_models.index(loop_list_ml_models) ]

            # Load the best model
            best_model = load_object(filename = "ml_model_"+loop_list_ml_models+loop_ml_prefix, file_repo = self.repo_prefix )
            list_best_model[loop_list_ml_models] = best_model

            # Predicted values
            if self.type_ana_set == 'classification' :
                # The candidate features and the hyperparameter for each fold of the cross-validation
                # 'kfold_auc_m', 'kfold_features_m', 'kfold_perm_imp_m', 'kfold_opt_hp_m', 'kfold_candidates', 'kfold_auc'
                list_hpt_kfold_nest = []
                list_kfold_nest_feats = []
                for kfold_loop in range(len(best_model.all_model_auc_cv[0]['kfold_auc_m'])) :
                    # Optimal hyperparameter for each fold (with highest AUC scores in the nested loop)
                    ls_kfold_nest_score = best_model.all_model_auc_cv[0]['kfold_auc_m'][kfold_loop]
                    ls_avg_kfold_nest_score = [ mean(ls_kfold_nest_score[x]) for x in list(ls_kfold_nest_score.keys()) ]
                    ls_hpt_kfold_nest = list(ls_kfold_nest_score.keys())[ ls_avg_kfold_nest_score.index(max(ls_avg_kfold_nest_score)) ]
                    list_hpt_kfold_nest.append(ls_hpt_kfold_nest)

                    # Candidate features for each fold (hyperparameter with highest AUC scores in the nested loop)
                    ls_kfold_nest_feats = best_model.all_model_auc_cv[0]['kfold_features_m'][kfold_loop]
                    ls_opt_kfold_nest_feats = ls_kfold_nest_feats[ls_hpt_kfold_nest]
                    unique_feats = set() # To extract the list of unique features correspond to the optimal hyperparameter
                    for sublist in ls_opt_kfold_nest_feats:
                        unique_feats.update(sublist)
                    ls_kfold_nest_feats = list(unique_feats)
                    list_kfold_nest_feats.append(ls_kfold_nest_feats)
                
                # To extract the training and testing data in each fold of the cross-validation
                # To train the model and obtaine the predicted values
                cv_predictions = []
                for BLoop in range(best_model.B_loop) :
                    for kLoop in range(best_model.k_loop) :
                        # Training set
                        ds_train = load_object(filename = best_model.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1), file_repo = best_model.data_repo)
                        # Testing set
                        ds_test = load_object(filename = best_model.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+best_model.addon_Xtest, file_repo = best_model.data_repo)
                        # Undersampling in the training set
                        if best_model.undersampling == True :
                            if len(ds_train[best_model.outcome].value_counts().unique().tolist()) > 1 :
                                ds_train = underSamplingBinary(data = ds_train, var_outcome = best_model.outcome, targetBalance = 0.5)
                            else:
                                pass
                        else :
                            pass
                        
                        # To drop the patient ID and outcome columns
                        X_train = ds_train.drop([best_model.vars_patid, best_model.outcome], axis = 1)
                        y_train = ds_train[best_model.outcome]

                        X_test = ds_test.drop([best_model.vars_patid, best_model.outcome], axis = 1)
                        y_test = ds_test[best_model.outcome]

                        # One-hot encoding
                        X_train, X_test = func_one_hot_encoding(ds_Xtrain = X_train, ds_Xtest = X_test, vars_encode = best_model.vars_encoding)

                        # To keep to candidate features in the training and testing data
                        if len(list_kfold_nest_feats[kLoop]) > 0 :
                            vars_ml_ana = [x for x in list_kfold_nest_feats[kLoop] if (x in X_train.columns.tolist()) & (x in X_test.columns.tolist()) ]
                            X_train, X_test = X_train[vars_ml_ana], X_test[vars_ml_ana]
                        else :
                            vars_train_test = np.unique(X_train.columns.tolist() + X_test.columns.tolist()).tolist()
                            vars_ml_ana = [x for x in best_model.vars_ml if x in vars_train_test ]
                            vars_ml_ana = [x for x in vars_ml_ana if x in X_train.columns.tolist()]
                            vars_ml_ana = [x for x in vars_ml_ana if x in X_test.columns.tolist()]
                            X_train, X_test = X_train[vars_ml_ana], X_test[vars_ml_ana]

                        if best_model.ml_model_set == 'figs' :
                            ml_model = FIGSClassifier(max_rules = list_hpt_kfold_nest[kLoop] )
                        elif best_model.ml_model_set == 'catboost' :
                            ml_model = CatBoostClassifier(iterations = 10, learning_rate = 1, loss_function='Logloss', verbose=True, random_seed = best_model.seed_number, max_depth = list_hpt_kfold_nest[kLoop])
                        elif best_model.ml_model_set == 'hs' :
                            ensemble = FIGSClassifier(max_rules = list_hpt_kfold_nest[kLoop])
                            ml_model = HSTreeClassifier(estimator_ = ensemble )
                        elif best_model.ml_model_set == 'gosdt' :
                            ml_model = OptimalTreeClassifier(random_state = best_model.seed_number, balance = True, regularization = list_hpt_kfold_nest[kLoop])
                        elif best_model.ml_model_set == 'greedy' :
                            ml_model = GreedyTreeClassifier(max_depth = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        elif best_model.ml_model_set == 'xgboost' :
                            ml_model = XGBClassifier(random_state = best_model.seed_number, max_depth = list_hpt_kfold_nest[kLoop], learning_rate = 1, n_jobs = 2)
                        elif best_model.ml_model_set == 'c45' :
                            ml_model = C45TreeClassifier(max_rules = list_hpt_kfold_nest[kLoop])
                        elif best_model.ml_model_set == 'adaboost' :
                            ml_model = AdaBoostClassifier(n_estimators = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        elif best_model.ml_model_set == 'gboost' :
                            ml_model = GradientBoostingClassifier(n_estimators = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        else :
                            print('ERROR: PLEASE UPDATE THE ALGORITHM IN THE SCRIPT !!!')

                        if best_model.ml_model_set in ['figs', 'hs', 'greedy', 'c45'] :
                            ml_model.fit(X_train, y_train, feature_names = X_train.columns.tolist() )
                            ml_preds = ml_model.predict_proba(X_test)[:,1]
                        else:
                            ml_model.fit(X_train, y_train)
                            ml_preds = ml_model.predict_proba(X_test)[:,1]

                        # To store the prediction
                        df_predictions = pd.DataFrame({'patientid' : ds_test[best_model.vars_patid]
                                                       ,'true_labels' : ds_test[best_model.outcome]
                                                       ,loop_list_ml_models_name : ml_preds })
                        cv_predictions.append( df_predictions )
                
                # Concatenate predictions from all folds
                df_predictions = pd.concat(cv_predictions)
                # Calculate mean prediction per patient
                df_mean_predictions = df_predictions.groupby(['patientid', 'true_labels'])[loop_list_ml_models_name].mean().reset_index()

                # Create the data frame
                data_best_model_ = pd.DataFrame({'patientid' : df_mean_predictions['patientid']
                                                ,'true_labels' : df_mean_predictions['true_labels']
                                                ,loop_list_ml_models_name : df_mean_predictions[loop_list_ml_models_name] })
                if loop_list_ml_models == self.list_ml_models[0] :
                    data_best_model = data_best_model_.copy()
                else :
                    data_best_model = pd.merge(data_best_model, data_best_model_, on = ['patientid', 'true_labels'], how = 'left')
            else :
                # The candidate features and the hyperparameter for each fold of the cross-validation
                # 'kfold_cIndex_m', 'kfold_features_m', 'kfold_perm_imp_m', 'kfold_opt_hp_m', 'kfold_candidates', 'kfold_cIndex'
                list_hpt_kfold_nest = []
                list_kfold_nest_feats = []
                for kfold_loop in range(len(best_model.all_model_cIndex_cv[0]['kfold_cIndex_m'])) :
                    # Optimal hyperparameter for each fold (with highest C-index in the nested loop)
                    ls_kfold_nest_score = best_model.all_model_cIndex_cv[0]['kfold_cIndex_m'][kfold_loop]
                    ls_avg_kfold_nest_score = [ mean(ls_kfold_nest_score[x]) for x in list(ls_kfold_nest_score.keys()) ]
                    ls_hpt_kfold_nest = list(ls_kfold_nest_score.keys())[ ls_avg_kfold_nest_score.index(max(ls_avg_kfold_nest_score)) ]
                    list_hpt_kfold_nest.append(ls_hpt_kfold_nest)

                    # Candidate features for each fold (hyperparameter with highest C-index in the nested loop)
                    ls_kfold_nest_feats = best_model.all_model_cIndex_cv[0]['kfold_features_m'][kfold_loop]
                    ls_opt_kfold_nest_feats = ls_kfold_nest_feats[ls_hpt_kfold_nest]
                    unique_feats = set() # To extract the list of unique features correspond to the optimal hyperparameter
                    for sublist in ls_opt_kfold_nest_feats:
                        unique_feats.update(sublist)
                    ls_kfold_nest_feats = list(unique_feats)
                    list_kfold_nest_feats.append(ls_kfold_nest_feats)
                
                # To extract the training and testing data in each fold of the cross-validation
                # To train the model and obtaine the predicted values
                cv_predictions = []
                for BLoop in range(best_model.BLoop) :
                    for kLoop in range(best_model.k_fold) :
                        # Training set
                        ds_train = load_object(filename = best_model.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1), file_repo = best_model.data_repo)
                        # Testing set
                        ds_test = load_object(filename = best_model.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+best_model.addon_Xtest, file_repo = best_model.data_repo)
                        # Undersampling in the training set
                        if best_model.undersampling == True :
                            if len(ds_train[best_model.var_status].value_counts().unique().tolist()) > 1 :
                                ds_train = underSamplingBinary(data = ds_train, var_outcome = best_model.var_status, targetBalance = 0.5)
                            else:
                                pass
                        else :
                            pass
                        
                        # Training and testing set to fit the survival format
                        vars_ml_ana = [x for x in best_model.vars_ml if x in ds_train.columns.tolist() ]
                        vars_ml_ana = [x for x in vars_ml_ana if x in ds_test.columns.tolist()]
                        X_train, y_train = get_x_y(data_frame = ds_train[vars_ml_ana + [best_model.var_status, best_model.var_time]]
                                                   ,attr_labels = [best_model.var_status, best_model.var_time], pos_label = 1 )
                        X_test, y_test = get_x_y(data_frame = ds_test[vars_ml_ana + [best_model.var_status, best_model.var_time]]
                                                 ,attr_labels = [best_model.var_status, best_model.var_time], pos_label = 1 )

                        # One-hot encoding
                        X_train, X_test = func_one_hot_encoding(ds_Xtrain = X_train, ds_Xtest = X_test, vars_encode = best_model.vars_encoding)

                        # To keep to candidate features in the training and testing data
                        if len(list_kfold_nest_feats[kLoop]) > 1 :
                            vars_ml_ana = [x for x in list_kfold_nest_feats[kLoop] if (x in X_train.columns.tolist()) & (x in X_test.columns.tolist()) ]
                            X_train, X_test = X_train[vars_ml_ana], X_test[vars_ml_ana]
                        else :
                            vars_ml_ana = [x for x in best_model.vars_ml if x in X_train.columns.tolist() ]
                            vars_ml_ana = [x for x in vars_ml_ana if x in X_test.columns.tolist()]
                            X_train, X_test = X_train[vars_ml_ana], X_test[vars_ml_ana]

                        if best_model.surv_model_set == 'rsf' :
                            surv_model = RandomSurvivalForest(max_depth = list_hpt_kfold_nest[kLoop], n_estimators = 20, n_jobs = 2, random_state = best_model.seed_number)
                        elif best_model.surv_model_set == 'gboost' :
                            surv_model = GradientBoostingSurvivalAnalysis(max_depth = list_hpt_kfold_nest[kLoop], n_estimators = 20, learning_rate=1.0, random_state = best_model.seed_number)
                        elif best_model.surv_model_set == 'cwgboost' :
                            surv_model = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        elif best_model.surv_model_set == 'coxph' :
                            surv_model = CoxPHSurvivalAnalysis(alpha = list_hpt_kfold_nest[kLoop])
                        elif best_model.surv_model_set == 'coxnet' :
                            surv_model = CoxnetSurvivalAnalysis(l1_ratio = list_hpt_kfold_nest[kLoop], alpha_min_ratio=0.01, fit_baseline_model = True)
                        elif best_model.surv_model_set == 'extra' :
                            surv_model = ExtraSurvivalTrees(max_depth = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        elif best_model.surv_model_set == 'fastsvm' :
                            surv_model = FastSurvivalSVM(alpha = list_hpt_kfold_nest[kLoop], max_iter = 20, tol = 1e-4, random_state = best_model.seed_number)
                        elif best_model.surv_model_set == 'naive_svm' :
                            surv_model = NaiveSurvivalSVM(alpha = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number, tol=0.001, penalty='l1', max_iter=20)
                        elif best_model.surv_model_set == 'surv_tree' :
                            surv_model = SurvivalTree(max_depth = list_hpt_kfold_nest[kLoop], random_state = best_model.seed_number)
                        else :
                            print('ERROR: PLEASE UPDATE THE ' + loop_list_ml_models_name + ' ALGORITHM IN THE SCRIPT !!!')

                        # surv_model.fit(X_train, y_train)
                        # ml_preds = surv_model.predict(X_test)
                        if loop_list_ml_models in ['lsvm', 'nlsvm'] :
                            surv_model.fit(X_train, y_train)
                            ml_preds_ = svm_predict_survival_function()
                            ml_preds_.fit(svm_model = surv_model, X = X_train, y = y_train)
                            ml_preds = ml_preds_.predict_survival_function(X = X_test)
                        else :
                            surv_model.fit(X_train, y_train)
                            ml_preds = surv_model.predict_survival_function(X_test)

                        # Predicted survival probability at time point
                        list_time = ml_preds[0].x
                        abs_diff = np.abs(list_time - time_point)
                        min_index = np.argmin(abs_diff)
                        closest_value = list_time[min_index]
                        idx_time = list_time.tolist().index(closest_value)
                        predicted_values_tmp = []
                        for surv_proba_loop in range(len(ml_preds)) :
                            predicted_values_tmp.append( ml_preds[surv_proba_loop].y[idx_time] )
                        predicted_values = [1 - val for val in predicted_values_tmp]

                        # To store the prediction
                        df_predictions = pd.DataFrame({'patientid' : ds_test['PATNO']
                                                       ,'true_labels' : ds_test[best_model.var_status]
                                                       ,'true_time' : ds_test[best_model.var_time]
                                                       ,loop_list_ml_models_name : predicted_values })
                        cv_predictions.append( df_predictions )
                
                # Concatenate predictions from all folds
                df_predictions = pd.concat(cv_predictions)
                # Calculate mean prediction per patient
                df_mean_predictions = df_predictions.groupby(['patientid', 'true_labels', 'true_time'])[loop_list_ml_models_name].mean().reset_index()

                # Adjusting the predicted risk score
                if df_mean_predictions[loop_list_ml_models_name].min() < 0 :
                    df_mean_predictions[loop_list_ml_models_name] = df_mean_predictions[loop_list_ml_models_name] - df_mean_predictions[loop_list_ml_models_name].min()
                else :
                    pass

                # Create the data frame
                data_best_model_ = pd.DataFrame({'patientid' : df_mean_predictions['patientid']
                                                ,'true_labels' : df_mean_predictions['true_labels']
                                                ,'true_time' : df_mean_predictions['true_time']
                                                ,loop_list_ml_models_name : df_mean_predictions[loop_list_ml_models_name] })
                if loop_list_ml_models == self.list_ml_models[0] :
                    data_best_model = data_best_model_.copy()
                else :
                    data_best_model = pd.merge(data_best_model, data_best_model_, on = ['patientid', 'true_labels', 'true_time'], how = 'left')

        self.list_best_model = list_best_model
        self.data_best_model = data_best_model
        
    def dca_net_plots(self) :
        """
        time : int or float
            Time of interest in years, used in Survival DCA
        ------
        Return
        ------
        To generate the net benefits vs threshold plot
        """

        # To measure the net benefits in each threshold
        if self.type_ana_set == 'classification' :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name )
        else :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name
                            ,time = self.time_point
                            ,time_to_outcome_col='true_time' )
        
        dca_model['model'] = np.where( dca_model['model'] == 'all', 'All', np.where( dca_model['model'] == 'none', 'None', dca_model['model'] ) )
        
        # To generate the plot of net benefit vs threshold probability
        plot_dca_graphs( plot_df = dca_model
                    ,graph_type = 'net_benefit'
                    ,file_name = self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Net_Benefit'+self.fig_prefix+self.fig_prefix_add
                    ,show_grid = False
                    ,show_legend = True
                    ,dpi = self.dpi
                    ,list_ml_model_name = self.list_ml_model_name
                    ,list_ml_models = self.list_ml_models )
        
        # To generate the plot of net intervention avoided vs threshold probability
        plot_dca_graphs( plot_df = dca_model
                    ,graph_type = 'net_intervention_avoided'
                    ,file_name = self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Net_Intervention'+self.fig_prefix+self.fig_prefix_add
                    ,show_grid = False
                    ,show_legend = True
                    ,dpi = self.dpi
                    ,list_ml_model_name = self.list_ml_model_name
                    ,list_ml_models = self.list_ml_models )
        
        # Calculate the rank of 'net_benefit' and 'net_intervention_avoided' in for each model in each threshold
        dca_model['rank_net_benefit'] = dca_model.groupby('threshold')['net_benefit'].rank(ascending=False)
        dca_model['rank_net_intervention_avoided'] = dca_model.groupby('threshold')['net_intervention_avoided'].rank(ascending=False)

        # Average of 'net_benefit' and 'net_intervention_avoided
        tbl_average_net_benefits = pd.DataFrame({'model': dca_model['model'].unique()})
        tbl_average_net_intervention_avoided = pd.DataFrame({'model': dca_model['model'].unique()})
        for threshold_loop in [round(0.1 + 0.1 * i, 1) for i in range(int((0.9 - 0.1) / 0.1) + 1)] :
            # Filter the threshold cutoff and remove the missing ranking
            dca_model_net_benefits = dca_model[ (~dca_model['net_benefit'].isna()) & (dca_model['threshold'] <= threshold_loop) ]
            dca_model_net_intervention_avoided = dca_model[ (~dca_model['net_intervention_avoided'].isna()) & (dca_model['threshold'] <= threshold_loop) ]
            # Average of the ranking
            average_net_benefits = dca_model_net_benefits.groupby('model')[['rank_net_benefit']].mean()
            average_net_intervention_avoided = dca_model_net_intervention_avoided.groupby('model')[['rank_net_intervention_avoided']].mean()
            # Reset the index and rename the column name
            average_net_benefits.reset_index(inplace=True)
            average_net_benefits.rename(columns = {'rank_net_benefit': 'net_benefit_'+str(threshold_loop)}, inplace=True)
            average_net_intervention_avoided.reset_index(inplace=True)
            average_net_intervention_avoided.rename(columns = {'rank_net_intervention_avoided': 'net_intervention_avoided_'+str(threshold_loop)}, inplace=True)
            # Append the table
            tbl_average_net_benefits = pd.merge(tbl_average_net_benefits, average_net_benefits, on = ['model'], how = 'left')
            tbl_average_net_intervention_avoided = pd.merge(tbl_average_net_intervention_avoided, average_net_intervention_avoided, on = ['model'], how = 'left')
        
        # Ranking of the average of 'net_benefit'
        columns_net_benefits = [x for x in tbl_average_net_benefits.columns if x not in ['model']]
        columns_net_benefits_rank = ['rank_'+x for x in columns_net_benefits]
        rename_dict = dict(zip(columns_net_benefits, columns_net_benefits_rank))
        tbl_rank_net_benefits = tbl_average_net_benefits[columns_net_benefits].rank()
        tbl_rank_net_benefits.rename(columns = rename_dict, inplace = True)
        table_average_net_benefits = pd.concat([tbl_average_net_benefits, tbl_rank_net_benefits], axis = 1)
        table_average_net_benefits.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Net_Benefits'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Ranking of the average of 'net_intervention_avoided'
        columns_net_intervention_avoided = [x for x in tbl_average_net_intervention_avoided.columns if x not in ['model']]
        columns_net_intervention_avoided_rank = ['rank_'+x for x in columns_net_intervention_avoided]
        rename_dict = dict(zip(columns_net_intervention_avoided, columns_net_intervention_avoided_rank))
        tbl_rank_net_intervention_avoided = tbl_average_net_intervention_avoided[columns_net_intervention_avoided].rank()
        tbl_rank_net_intervention_avoided.rename(columns = rename_dict, inplace = True)
        table_average_net_intervention_avoided = pd.concat([tbl_average_net_intervention_avoided, tbl_rank_net_intervention_avoided], axis = 1)
        table_average_net_intervention_avoided.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Net_Intervention_Avoided'+self.fig_prefix+self.fig_prefix_add+'.csv')


    def dca_area_net_benefit(self) :
        """
        self
        ------
        Return
        ------
        To generate the area under net benefits vs threshold
        """

        # To measure the net benefits in each threshold
        if self.type_ana_set == 'classification' :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name )
        else :
            dca_model = dca( data = self.data_best_model
                            ,outcome = 'true_labels'
                            ,modelnames = self.list_ml_model_name
                            ,time = self.time_point
                            ,time_to_outcome_col='true_time' )
        dca_model['model'] = np.where( dca_model['model'] == 'all', 'All', np.where( dca_model['model'] == 'none', 'None', dca_model['model'] ) )

        # Calculate the step
        step = dca_model['threshold'][1] - dca_model['threshold'][0]

        # Area under the net benefit (positive) for each model
        area_nb_comb = []
        area_nb_all_comb = []
        min_threshold = []
        max_threshold = []
        for model_nb in ['All'] + self.list_ml_model_name :
            # Measurement of the area of the net benefit
            df_area_nb = dca_model[ dca_model['model'] == model_nb ]
            area_nb_sum, area_nb_sum_all, ls_threshold = area_net_benefit_threshold_measure(data = df_area_nb, step = step)
            area_nb_comb.append( area_nb_sum )
            area_nb_all_comb.append( area_nb_sum_all )
            min_threshold.append( min(ls_threshold) )
            max_threshold.append( max(ls_threshold) )
        
        tbl_area_nb = pd.DataFrame({'Model': ['All'] + self.list_ml_model_name
                                    ,'Area net benefit': area_nb_comb
                                    ,'Area net benefit (All)': area_nb_all_comb
                                    ,'Difference Area to All': area_nb_comb - area_nb_comb[0]
                                    ,'Threshold (min)': min_threshold
                                    ,'Threshold (max)': max_threshold })
        tbl_area_nb.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Area_Net_Benefit'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Measure the net benefit in each bootstrapping resamples
        bootstrap_nb = bootstrap_dca(data = self.data_best_model, num_resamples = 1000, type_ana_set = self.type_ana_set, models = self.list_ml_model_name, time_point = self.time_point)

        # Bootsrapping p-value for comparing two models
        list_model_1 = []
        list_model_2 = []
        list_p_value = []
        list_all_model = self.list_ml_model_name
        for i_nb1 in range(len(list_all_model)) :
            for i_nb2 in range(len(list_all_model)) :
                if (i_nb1 != i_nb2) & (i_nb1 < i_nb2) :
                    # Model name
                    model_nb1 = list_all_model[i_nb1]
                    model_nb2 = list_all_model[i_nb2]

                    # Compute bootstrapping statistics (difference of area under the net benefit curve)
                    bootstrap_stat = area_net_benefit_diff(data = bootstrap_nb, step = step, model_nb1 = model_nb1, model_nb2 = model_nb2)

                    # Calculate p-value
                    observed_stat = measure_aunbc(data = dca_model, step = step, model_nb1 = model_nb1, model_nb2 = model_nb2)
                    p_value = bootstrap_hypothesis_testing_p_value(observed_statistic = observed_stat, bootstrap_statistics = bootstrap_stat)

                    # Append the results
                    list_model_1.append(model_nb1)
                    list_model_2.append(model_nb2)
                    list_p_value.append(p_value)
                else :
                    pass
        
        # Adjusted p-values by performing multiple testing corrections (Benjamini-Hochberg)
        _, pvals_bonferroni, _, _ = multipletests(list_p_value, alpha=0.05, method='bonferroni')
        
        # Output table
        tbl_bootstrap_p_value = pd.DataFrame({'Model 1': list_model_1
                                              ,'Model 2': list_model_2
                                              ,'p-value': list_p_value
                                              ,'adj p-value': pvals_bonferroni})
        tbl_bootstrap_p_value.to_csv(self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Area_Net_Benefit_Bootstrap_P_Values'+self.fig_prefix+self.fig_prefix_add+'.csv')

        # Plot of the area under the net benefit (bar plot)
        fig = plt.figure(figsize=(12, 6))
        ax = fig.add_subplot(111)  # Add a subplot to the figure
        ax.set_facecolor('white')  # Set the subplot's background color

        # Excluding the 'ALL'
        tbl_area_nb_excl = tbl_area_nb[tbl_area_nb['Model'] != 'All'].reset_index(drop = True)
        tbl_area_nb_excl['Model'] = tbl_area_nb_excl['Model'].str.replace('-', '-\n').replace(' ', '\n')
        tbl_bootstrap_p_value_excl = tbl_bootstrap_p_value[tbl_bootstrap_p_value['Model 1'] != 'All'].reset_index(drop = True)
        tbl_bootstrap_p_value_excl['Model 1'] = tbl_bootstrap_p_value_excl['Model 1'].str.replace('-', '-\n').replace(' ', '\n')
        tbl_bootstrap_p_value_excl['Model 2'] = tbl_bootstrap_p_value_excl['Model 2'].str.replace('-', '-\n').replace(' ', '\n')

        # Plot the area under the net benefit
        models = tbl_area_nb_excl['Model']
        areas = tbl_area_nb_excl['Area net benefit']
        colors = ['#6a76c4' if value >= 0 else '#cf5b89' for value in tbl_area_nb_excl['Area net benefit (All)']]
        bars = ax.bar(models, areas, color=colors)

        # Adding labels to each bar
        for bar, diff in zip(bars, tbl_area_nb_excl['Difference Area to All']):
            height = max(areas)/20
            arrow = '\u2191' if diff > 0 else '\u2193'
            ax.text(
                bar.get_x() + bar.get_width() / 2,
                height,
                f'{arrow} {diff:.3f}',
                ha='center',
                va='bottom',
                fontsize=8 )

        # Highlight significant comparisons with horizontal lines above the bars
        significant_comparisons = tbl_bootstrap_p_value_excl[tbl_bootstrap_p_value_excl['adj p-value'] < 0.05]

        # Calculate the average height for each group of Model 1
        avg_heights = {}
        for model1 in significant_comparisons['Model 1'].drop_duplicates(keep='first'):
            avg_heights[model1] = max(areas) + (significant_comparisons['Model 1'].drop_duplicates(keep='first').tolist().index(model1) / 100) + (max(areas) / 15)
            
        # Plotting significant comparisons
        for i, (idx, row) in enumerate(significant_comparisons.iterrows()):
            model1 = row['Model 1']
            model2 = row['Model 2']
            idx1 = models[models == model1].index[0]
            idx2 = models[models == model2].index[0]
            x1 = bars[idx1].get_x() + bars[idx1].get_width() / 2
            x2 = bars[idx2].get_x() + bars[idx2].get_width() / 2
            y = avg_heights[model1]
            ax.plot([x1, x2], [y, y], "k-", lw=1)
            ax.plot([x1, x1], [y - max(areas)/40, y], "k-", lw=1)
            ax.plot([x2, x2], [y - max(areas)/40, y], "k-", lw=1)

            # Add "***" below the line if p-value < 0.001
            if row['adj p-value'] < 0.001:
                ax.text(x2-0.5, y - max(areas)/25, "***", ha='center', size = 10)
            elif row['adj p-value'] < 0.01:
                # Add "**" below the line if p-value < 0.01
                ax.text(x2-0.5, y - max(areas)/25, "**", ha='center', size = 10)
            elif row['adj p-value'] < 0.05:
                # Add "*" below the line if p-value < 0.05
                ax.text(x2-0.5, y - max(areas)/25, "*", ha='center', size = 10)
            else :
                pass

        # Adding labels and title
        ax.set_ylabel("Area under net benefit", fontsize=10)
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=10)
        plt.subplots_adjust(top=0.95)  # Reduce the top margin (1 is the default)

        # Adding custom legend
        custom_lines = [
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='*     Adj. p-value < 0.05'),
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='**   Adj. p-value < 0.01'),
            plt.Line2D([0], [0], color="black", linestyle="-", lw=1, label='*** Adj. p-value < 0.001') ]
        ax.legend(handles=custom_lines, loc='upper left', fontsize=10)

        # Add outer box to the plot
        for spine in ax.spines.values():
            spine.set_visible(True)
            spine.set_color('black')  # Set box color
            spine.set_linewidth(0.5)  # Set box line width
        
        # Save the plot
        plt.savefig(self.fig_repo_out+'Decision Curve Analysis/DCA_CV_Area_Net_Benefit'+self.fig_prefix+self.fig_prefix_add, dpi=self.dpi)
