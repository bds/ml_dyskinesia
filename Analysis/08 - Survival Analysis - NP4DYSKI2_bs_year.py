#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Machine Learning Survival Analysis    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# To load the file for the labels of the baseline variables in the model
outcome_define = 'NP4DYSKI2_bs_year'
exec( open("Analysis/01 - Data Processing - Labels of Baseline Variables.py").read() )

# Label at x-axis for SHAP values plot
shap_x_label = ['No dyskinesia', 'Dyskinesia']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Machine Learning Survival Analysis - Dyskinesias [NP4DYSKI2_bs_year]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Random Survival Forest (RSF)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - RSF - NP4DYSKI2_bs_year.py").read() )

# Gradient Boosting (GBOOST)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - GBOOST - NP4DYSKI2_bs_year.py").read() )

# Component-wise Gradient Boosting (CWGBOOST)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - CWGBOOST - NP4DYSKI2_bs_year.py").read() )

# Penalised Cox Proportional Hazards Model (COXPH)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - COXPH - NP4DYSKI2_bs_year.py").read() )

# Extra Survival Trees
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - EXTRA - NP4DYSKI2_bs_year.py").read() )

# Linear Survival Support Vector Machine (LSVM)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - LSVM - NP4DYSKI2_bs_year.py").read() )

# Naive version of linear Survival Support Vector Machine (NLSVM)
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - NLSVM - NP4DYSKI2_bs_year.py").read() )

# Survival Trees
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/08 - Survival - TREE - NP4DYSKI2_bs_year.py").read() )

# Predictive Performance Evaluation
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/09 - Survival Analysis - Performance Comparison - NP4DYSKI2_bs_year.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/09 - Survival Analysis - Performance Comparison on Cross-Validation - NP4DYSKI2_bs_year.py").read() )
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/09 - Survival Analysis - C Indices Comparison (One-Shot Nonparametric Approach - NP4DYSKI2_bs_year.py").read() )

# Stability of the Optimal Predictive Model
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/09 - Survival Analysis - Stability - NP4DYSKI2_bs_year.py").read() )

# Hazard Ration Measurement
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/10 - Survival Analysis - Hazards Ratio - NP4DYSKI2_bs_year.py").read() )

# Decision Curve Analysis
exec( open("Analysis/Prediction/NP4DYSKI2_bs_year/11 - Survival Analysis - Decision Curve Analysis - NP4DYSKI2_bs_year.py").read() )