#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                           Optimal Model - Highest average C index                                                          ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Performance metric    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_model_cIndex = []
ls_model_name = []

for loop_i in range(len(model_risk)) :
    name_key = list( model_risk[loop_i].keys() )[0]
    ls_model_name.append( name_key )
    ls_model_cIndex.append( model_risk[loop_i][name_key].best_model_cIndex_test )   # Optimal model with highest average c-index

idx_best = ls_model_cIndex.index( max(ls_model_cIndex) )
best_ml_model = model_risk[idx_best][ls_model_name[idx_best]]
#### best_ml_model.performance_metric()


#~~~~~~~~~~~~~~~~~~~~~~~~~#
####    SHAP values    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~#

if model_set != 'median_time' :
    try :
        shap_fig_size = [12, 6] #[10, 5] # [12, 5]
        shap_surv = shap_values_survival(model = best_ml_model
                                        ,fig_repo = file_repo+file_repo_prefix+set_outcome[0]+'/'
                                        ,fig_name = model_prefix
                                        ,fig_features = data_info_all
                                        ,fig_size = shap_fig_size
                                        ,max_evals_shap = None )

        best_ml_model.shap_values = shap_surv['shap_values']
        best_ml_model.feature_importance = shap_surv['feature_importance']
    except Exception :
        print("Not running SHAP values due to IndexError")
else :
    pass


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Save the best model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

save_object(data = best_ml_model, filename = "ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )
best_ml_model = load_object(filename = "ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                          Optimal Model - Highest average AUC scores and least number of variables                                          ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Performance metric - Highest average AUC score & less complexity    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_measure_nvars_surv(loop_i) :
    name_key = list( model_risk[loop_i].keys() )[0]
    ls_model_name = name_key
    ls_model_cIndex = model_risk[loop_i][name_key].best_model_cIndex_test

    cols_X = model_risk[loop_i][name_key].X_validate.columns.tolist()

    select_feats = model_risk[loop_i][name_key].best_model_features
    num_vars = len(select_feats)
    ls_num_vars = num_vars

    return({'ls_model_name': ls_model_name
            ,'ls_model_cIndex': ls_model_cIndex
            ,'ls_num_vars': ls_num_vars
            ,'ls_features': select_feats
            })

if model_set == 'extra' :
    n_jobs_nvars = 1
else:
    n_jobs_nvars = min([n_jobs, len(model_risk)])

measure_nvars = Parallel(n_jobs = n_jobs_nvars)(
    delayed( func_measure_nvars_surv )(loop_i = loop_i)
    for loop_i in range(len(model_risk)) )

ls_model_name = []
ls_model_cIndex = []
ls_num_vars = []
ls_features = []
for loop_i in range(len(model_risk)) :
    ls_model_name.append( measure_nvars[loop_i]['ls_model_name'] )
    ls_model_cIndex.append( measure_nvars[loop_i]['ls_model_cIndex'] )
    ls_num_vars.append( measure_nvars[loop_i]['ls_num_vars'] )
    ls_features.append( measure_nvars[loop_i]['ls_features'] )


# Index - highest average C-index
idx_best = [x for x in range(len(ls_model_cIndex)) if ls_model_cIndex[x] == max(ls_model_cIndex)]

# Index - between 1 standard deviation from the highest average C-index
idx_std = [x for x in range(len(ls_model_cIndex)) if (ls_model_cIndex[x] > max(ls_model_cIndex) - statistics.stdev(ls_model_cIndex))
           & (ls_model_cIndex[x] < max(ls_model_cIndex) + statistics.stdev(ls_model_cIndex)) ]
if len(idx_std) == 0 :
    idx_std = [x for x in range(len(ls_model_cIndex)) if (ls_model_cIndex[x] >= max(ls_model_cIndex) - statistics.stdev(ls_model_cIndex))
               & (ls_model_cIndex[x] <= max(ls_model_cIndex) + statistics.stdev(ls_model_cIndex)) ]

# Index - between 1 standard deviation with least number of variables (but more than 1 variable)
idx_num_vars = [x for x in idx_std if ls_num_vars[x] > 1]
if len(idx_num_vars) == 0 :
    idx_num_vars = [x for x in idx_std]

idx_optimal = [x for x in idx_num_vars if ls_num_vars[x] == min([ls_num_vars[x] for x in idx_num_vars])]
# if len(idx_optimal) > 1 :
#     idx_optimal = [x for x in idx_num_vars if ls_num_vars[x] == min([ls_num_vars[x] for x in idx_num_vars])]

if len(idx_optimal) > 1 :
    ls_cIndex_optimal = [ls_model_cIndex[x] for x in idx_optimal]
    if len(ls_cIndex_optimal) > 1 :
        max_cIndex_optimal = max(ls_cIndex_optimal)
    else :
        max_cIndex_optimal = ls_cIndex_optimal[0]
    
    idx_optimal = [x for x in idx_optimal if ls_model_cIndex[x] == max_cIndex_optimal][0]
else :
    idx_optimal = idx_optimal[0]

# The optimal model with AUC within 1 SD from maximum of average AUC and minimal number of variables
best_compact_ml_model = model_risk[idx_optimal][ls_model_name[idx_optimal]]

# To list the name of the methods, number of variables and average AUC scores
compact_ml_model_vars = dict({'model_name': ls_model_name
                              ,'num_vars': ls_num_vars
                              ,'model_avg_cIndex': ls_model_cIndex
                              ,'model_selected_features': ls_features
                              ,'idx_optimal': idx_optimal
                              })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Save the best model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

save_object(data = best_compact_ml_model, filename = "compact_ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )
best_compact_ml_model = load_object(filename = "compact_ml_model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )

save_object(data = compact_ml_model_vars, filename = "compact_ml_vars_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )
compact_ml_model_vars = load_object(filename = "compact_ml_vars_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome[0]+'/' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Delete the variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

del model_risk, best_ml_model, best_compact_ml_model, compact_ml_model_vars
