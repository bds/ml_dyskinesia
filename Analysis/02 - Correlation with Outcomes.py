#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Correlation of the ordinal outcomes with explanatory variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ data: dataset for the correlation analysis
# @ outcome: outcome variable (ordinal)
# @ data_var_info: data with variable description (Only 3 variables are allowed in the data)
#                  first column: name of the variable;
#                  second column: description of the variable
#                  third column: type of the variable (nominal, ordinal, continuous)

def corr_w_outcome(data, outcome, data_var_info):
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  #   Defining type of variables   #
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  df_corr = data.copy()
  vars_corr = df_corr.columns
  
  # Type of the variables
  df_type = pd.DataFrame(df_corr.dtypes, columns = ['var_type']).reset_index()
  df_type['var_type'] = df_type['var_type'].astype(str)
  
  # Number of unique values in the variables
  df_nunique = pd.DataFrame(df_corr.round().nunique(), columns = ['nunique']).reset_index()
  
  # Difference of min and max of the values in the variables
  df_diff = pd.DataFrame(df_corr.round().max() - df_corr.round().min(), columns = ['diff']).reset_index()
  
  # Merging df_type, df_nunique, df_diff, variable description, and type of the variable
  df_var_type = df_type.merge(df_nunique, how = 'left', on = ['index'])\
                       .merge(df_diff, how = 'left', on = ['index'])\
                       .merge(data_var_info, how = 'left',
                              left_on = ['index'], right_on = [data_var_info.columns[0]])
  
  # Type of outcome variable
  outcome_type = list(df_var_type[ df_var_type['index'] == outcome ]['Type'])
  outcome_lvl = list(df_var_type[ df_var_type['index'] == outcome ]['nunique'])
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  #   Spearman rank correlation   #
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  out_corr_1 = [spearmanr(df_corr[outcome], df_corr[list(vars_corr)[x1]])[0] for x1 in range(0, len(vars_corr))]
  out_corr_p_1 = [spearmanr(df_corr[outcome], df_corr[vars_corr[x1]])[1] for x1 in range(0, len(vars_corr))]
  tbl_corr_1 = pd.DataFrame({'Variable': vars_corr,
                             'Correlation_spea': out_corr_1,
                             'p_value_spea': out_corr_p_1})
  tbl_corr_1 = tbl_corr_1.round(decimals = 3)
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  #   Kendall's rank correlation   #
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  out_corr_2 = [kendalltau(df_corr[outcome], df_corr[list(vars_corr)[x2]])[0] for x2 in range(0, len(vars_corr))]
  out_corr_p_2 = [kendalltau(df_corr[outcome], df_corr[list(vars_corr)[x2]])[1] for x2 in range(0, len(vars_corr))]
  tbl_corr_2 = pd.DataFrame({'Variable': vars_corr,
                             'Correlation_kend': out_corr_2,
                             'p_value_kend': out_corr_p_2})
  tbl_corr_2 = tbl_corr_2.round(decimals = 3)
  
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  #   Point-biserial correlation   #
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  out_corr_3 = [pointbiserialr(df_corr[outcome], df_corr[list(vars_corr)[x3]])[0] for x3 in range(0, len(vars_corr))]
  out_corr_p_3 = [pointbiserialr(df_corr[outcome], df_corr[list(vars_corr)[x3]])[1] for x3 in range(0, len(vars_corr))]
  tbl_corr_3 = pd.DataFrame({'Variable': vars_corr,
                             'Correlation_point': out_corr_3,
                             'p_value_point': out_corr_p_3})
  tbl_corr_3 = tbl_corr_3.round(decimals = 3)
  
  
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  #   Merging correlation tables from 3 methods   #
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
  
  tbl_corr = tbl_corr_1.merge(tbl_corr_2, how = 'left', on = ['Variable'])\
                       .merge(tbl_corr_3, how = 'left', on = ['Variable'])\
                       .merge(df_var_type, how = 'left', on = ['Variable'])
  
  
  # Spearman rank correlation
  tbl_corr['Correlation'] = tbl_corr['Correlation_spea']
  tbl_corr['p-value'] = tbl_corr['p_value_spea']
  
  # Kendall's rank correlation
  tbl_corr.loc[ ((outcome_type == ['Continuous'])
                    & (tbl_corr['Type'] == 'Ordinal')
                    & (tbl_corr['nunique'] < 5) )  |
                ((outcome_type == ['Ordinal'])
                    & (outcome_lvl < [5])
                    & (tbl_corr['Type'] == 'Continuous'))  |
                ((outcome_type == ['Ordinal'])
                    & (outcome_lvl < [5])
                    & (tbl_corr['Type'] == 'Ordinal')
                    & (tbl_corr['nunique'] < 5))  , 'Correlation' ] = tbl_corr['Correlation_kend']
  
  tbl_corr.loc[ ((outcome_type == ['Continuous'])
                    & (tbl_corr['Type'] == 'Ordinal')
                    & (tbl_corr['nunique'] < 5) )  |
                ((outcome_type == ['Ordinal'])
                    & (outcome_lvl < [5])
                    & (tbl_corr['Type'] == 'Continuous'))  |
                ((outcome_type == ['Ordinal'])
                    & (outcome_lvl < [5])
                    & (tbl_corr['Type'] == 'Ordinal')
                    & (tbl_corr['nunique'] < 5))  , 'p-value' ] = tbl_corr['p_value_kend']
  
  # Point-biserial correlation
  tbl_corr.loc[ ((outcome_type == ['Continuous'])
                    & (tbl_corr['Type'] == 'Nominal'))  |
                ((outcome_type == ['Nominal'])
                    & (tbl_corr['Type'] == 'Continuous')), 'Correlation'] = tbl_corr['Correlation_point']
  
  tbl_corr.loc[ ((outcome_type == ['Continuous'])
                    & (tbl_corr['Type'] == 'Nominal'))  |
                ((outcome_type == ['Nominal'])
                    & (tbl_corr['Type'] == 'Continuous')), 'p-value'] = tbl_corr['p_value_point']
  
  
  # To keep selected variables
  vars_keep = ['Variable', 'Description', 'Correlation', 'p-value']
  tbl_corr = tbl_corr[vars_keep]
  tbl_corr = tbl_corr[tbl_corr['Variable'] != outcome]
  tbl_corr.sort_values( by = ['Correlation', 'p-value'], ascending = [False, True], axis = 0, inplace = True)
  
  
  # Return the output
  return(tbl_corr)