#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                    Function - Spearman Correlation Analysis                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from scipy.stats import spearmanr
from scipy import stats

def spearman_corr(data, vars, outcome) :
    # Processing the data
    data_corr = data[ ~data[outcome].isnull() ]

    # Spearman correlation of non-missing variables
    for var1 in [outcome]+vars :
        for var2 in [outcome]+vars :
            

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    NP4DYSKI2_Y4 - Dyskinesia    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Cross-cohort
spearman_corr( data = data_cohort_comb, vars = vars_baseline_dyski, outcome = 'NP4DYSKI2_Y4' )