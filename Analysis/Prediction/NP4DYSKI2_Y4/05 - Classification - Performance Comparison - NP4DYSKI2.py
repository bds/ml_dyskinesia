#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - Classification - Performance Comparison.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Cross Cohort Analysis                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_Y4'
set_outcome_baseline = 'NP4DYSKI2'
ml_prefix = ['_dyski_y4', '_dyski_y4_excl']
file_repo_prefix_single = ['LUXPARK/', 'PPMI/', 'ICEBERG/']
file_repo_prefix_multi = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_loop = file_repo_prefix_single + file_repo_prefix_multi


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['adaboost', 'greedy', 'catboost', 'c45', 'figs', 'gosdt', 'gboost', 'hs', 'xgboost']
ls_ml_model_name = ['AdaBoost', 'CART', 'CatBoost', 'C4.5', 'FIGS', 'GOSDT-GUESSES', 'Gradient Boosting', 'Hierarchical Shrinkage', 'XGBoost']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Type of normalization    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_ana_type = ["", "_unnormalized", "_normalized"]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of predictive features    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

df_feats_imp = dict()
for ml_ana_type_loop in ls_ml_ana_type :
    ds_perm_avg_cv_compare = load_models_avg_cv_comparison(ana_type = ml_ana_type_loop)

    # To combine the predictive features from all the cohorts (@file_repo_prefix_loop)
    df_feats_imp[ml_ana_type_loop] = dict()
    for loop_ml_prefix in ml_prefix :
        avg_cv_features = avg_cv_features_combining( repo_prefix = [file_repo_prefix_single, file_repo_prefix_multi]
                                                    ,fig_repo_out = file_repo+'CROSS_COHORT/'+set_outcome+'/'
                                                    ,fig_prefix = loop_ml_prefix
                                                    ,fig_prefix_add = ml_ana_type_loop
                                                    ,fig_features = data_info[['Variable', 'Description']]
                                                    ,list_ml_models = ls_ml_models )
        df_feats_imp[ml_ana_type_loop][loop_ml_prefix] = avg_cv_features['df_feats_perm_importance']

save_object(data = df_feats_imp, filename = "Ranking_features_optimal_models", file_repo = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
