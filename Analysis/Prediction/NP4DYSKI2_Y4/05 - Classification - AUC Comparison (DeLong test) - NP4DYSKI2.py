#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - DeLong test.py").read() )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Model Performance Analysis                                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_Y4'
ml_prefix = ['_dyski_y4', '_dyski_y4_excl']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_multi = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['adaboost', 'greedy', 'catboost', 'c45', 'figs', 'gosdt', 'gboost', 'hs', 'xgboost']
ls_ml_model_name = ['AdaBoost', 'CART', 'CatBoost', 'C4.5', 'FIGS', 'GOSDT-GUESSES', 'Gradient Boosting', 'Hierarchical Shrinkage', 'XGBoost']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    AUC comparison (DeLong test)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

auc_delong_test()
auc_normalized_delong_test()
auc_all_normalized_delong_test()

