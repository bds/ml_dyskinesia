from scipy.stats import shapiro, ttest_ind, mannwhitneyu
from lifelines.statistics import logrank_test

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Set outcome [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

outcomes = ['NP4DYSKI2', 'NP4DYSKI2_Y4', 'NP4DYSKI2_bs_year', 'NP4DYSKI2_status']
events = ['LID-', 'LID+']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Data processing    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# PPMI
data_ppmi_ledd = data_cohort_ppmi[['levodopa', 'LEDD']+outcomes]
data_ppmi_ledd = data_ppmi_ledd[ ~data_ppmi_ledd[outcomes[1]].isna() ]
data_ppmi_ledd = data_ppmi_ledd[ (data_ppmi_ledd['levodopa'] == 1) & (~data_ppmi_ledd['LEDD'].isna()) ]

# LuxPARK
data_luxpark_ledd = data_cohort_luxpark[['levodopa', 'LEDD']+outcomes]
data_luxpark_ledd = data_luxpark_ledd[ ~data_luxpark_ledd[outcomes[1]].isna() ]
data_luxpark_ledd = data_luxpark_ledd[ (data_luxpark_ledd['levodopa'] == 1) & (~data_luxpark_ledd['LEDD'].isna()) ]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Significance test of LEDD for EVENT+ and EVENT-    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# LEDD is 2 groups
ledd_group_0 = data_luxpark_ledd[data_luxpark_ledd[outcomes[1]] == 0]['LEDD']
ledd_group_1 = data_luxpark_ledd[data_luxpark_ledd[outcomes[1]] == 1]['LEDD']

# Check for normality using Shapiro-Wilk test
stat_0, p_LEDD_0 = shapiro( ledd_group_0 )
stat_1, p_LEDD_1 = shapiro( ledd_group_1 )

# Determine if data is normally distributed (typically p > 0.05)
is_LEDD_normal_0 = p_LEDD_0 > 0.05
is_LEDD_normal_1 = p_LEDD_1 > 0.05

# Perform t-test if both groups are normally distributed
if is_LEDD_normal_0 and is_LEDD_normal_1 :
    ledd_stat, ledd_p_value = ttest_ind(ledd_group_0, ledd_group_1)
else :
    # Perform Mann-Whitney U test if data is not normally distributed
    ledd_stat, ledd_p_value = mannwhitneyu(ledd_group_0, ledd_group_1)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Summary table    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Keep the result into table
summary_table = pd.DataFrame({'statistics': ['n'
                             ,'mean'
                             ,'SD'
                             ,'median'
                             ,'min'
                             ,'max' ]
              ,'All': [data_luxpark_ledd.shape[0]
                       ,data_luxpark_ledd['LEDD'].mean()
                       ,data_luxpark_ledd['LEDD'].std()
                       ,data_luxpark_ledd['LEDD'].median()
                       ,data_luxpark_ledd['LEDD'].min()
                       ,data_luxpark_ledd['LEDD'].max() ]
              ,events[0]: [len(ledd_group_0)
                           ,ledd_group_0.mean()
                           ,ledd_group_0.std()
                           ,ledd_group_0.median()
                           ,ledd_group_0.min()
                           ,ledd_group_0.max() ]
              ,events[1]: [len(ledd_group_1)
                           ,ledd_group_1.mean()
                           ,ledd_group_1.std()
                           ,ledd_group_1.median()
                           ,ledd_group_1.min()
                           ,ledd_group_1.max() ]
              ,'Significant (p-value)': [ledd_p_value]+['']*5
              })

summary_table.to_csv(file_repo+'LUXPARK/'+outcomes[1]+'/LEDD/'+'Descriptive_Log_Rank_LEDD'+'.csv')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Log-rank test grouped by LEDD    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# list of tables
list_tbl = []

# LEDD cutoff
cutoff = [200, 300, 350, 400, 450, 500]

for cutoff_loop in cutoff :
    # Separate the groups based on LEDD cutoff
    ledd_group_low = data_luxpark_ledd[ data_luxpark_ledd['LEDD'] < cutoff_loop ]
    ledd_group_high = data_luxpark_ledd[ data_luxpark_ledd['LEDD'] >= cutoff_loop ]

    # Perform the log-rank test
    logrank_pvals = logrank_test(durations_A = ledd_group_low[outcomes[2]]
                                 ,durations_B = ledd_group_high[outcomes[2]]
                                 ,event_observed_A = ledd_group_low[outcomes[3]]
                                 ,event_observed_B = ledd_group_high[outcomes[3]] ).p_value
    
    # Initialize the KaplanMeierFitter
    kmf = KaplanMeierFitter()

    # Plotting the Kaplan-Meier curves
    fig = plt.figure(figsize=(10, 3))
    ax = fig.add_subplot(111)  # Add a subplot to the figure
    ax.set_facecolor('white')  # Set the subplot's background color

    # Fit the data for LEDD group and plot
    kmf.fit(ledd_group_low[outcomes[2]], event_observed = ledd_group_low[outcomes[3]], label = 'LEDD < '+str(cutoff_loop))
    kmf.plot_survival_function()
    kmf.fit(ledd_group_high[outcomes[2]], event_observed = ledd_group_high[outcomes[3]], label = 'LEDD $\geq$ '+str(cutoff_loop))
    kmf.plot_survival_function()

    # Customize the plot
    plt.xlabel('Years')
    plt.ylabel('Survival Probability')
    plt.legend(title = 'LEDD subgroup')
    plt.grid(False)

    # Add outer box to the plot
    for spine in ax.spines.values():
        spine.set_visible(True)
        spine.set_color('black')  # Set box color
        spine.set_linewidth(0.5)  # Set box line width
    # Save the KM plots
    plt.savefig(file_repo+'LUXPARK/'+outcomes[2]+'/LEDD/'+'KM_Plot_LEDD_'+str(cutoff_loop), dpi = 300)
    
    # Keep the result into table
    summary_tbl = pd.DataFrame({'statistics': ['cutoff'
                                ,'n'
                                ,'mean'
                                ,'SD'
                                ,'median'
                                ,'min'
                                ,'max' ]
                ,'All': [cutoff_loop
                        ,data_luxpark_ledd.shape[0]
                        ,data_luxpark_ledd[outcomes[2]].mean()
                        ,data_luxpark_ledd[outcomes[2]].std()
                        ,data_luxpark_ledd[outcomes[2]].median()
                        ,data_luxpark_ledd[outcomes[2]].min()
                        ,data_luxpark_ledd[outcomes[2]].max() ]
                ,'LEDD < cutoff': [''
                            ,ledd_group_low.shape[0]
                            ,ledd_group_low[outcomes[2]].mean()
                            ,ledd_group_low[outcomes[2]].std()
                            ,ledd_group_low[outcomes[2]].median()
                            ,ledd_group_low[outcomes[2]].min()
                            ,ledd_group_low[outcomes[2]].max() ]
                ,'LEDD >= cutoff': [''
                            ,ledd_group_high.shape[0]
                            ,ledd_group_high[outcomes[2]].mean()
                            ,ledd_group_high[outcomes[2]].std()
                            ,ledd_group_high[outcomes[2]].median()
                            ,ledd_group_high[outcomes[2]].min()
                            ,ledd_group_high[outcomes[2]].max() ]
                ,'Log-rank (p-value)': [logrank_pvals]+['']*6
                })
    list_tbl.append( summary_tbl )

# Save the summary table
summary_table = pd.concat(list_tbl)
summary_table.to_csv(file_repo+'LUXPARK/'+outcomes[2]+'/LEDD/'+'Descriptive_Log_Rank_LEDD'+'.csv')
