#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import matplotlib.gridspec as gridspec
exec( open("Analysis/Functions - Bayesian Signed-Rank Test.py").read() )
exec( open("Analysis/Functions - Survival Analysis - Performance Comparison on Cross-Validation.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Cross Cohort Analysis                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_bs_year'
set_outcome_baseline = 'NP4DYSKI2'
ml_prefix = ['_dyski', '_dyski_excl']
file_repo_prefix_single = ['LUXPARK/', 'PPMI/', 'ICEBERG/']
file_repo_prefix_multi = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_loop = file_repo_prefix_single + file_repo_prefix_multi
file_repo_prefix_name = ['LuxPARK', 'PPMI', 'ICEBERG', 'Cross-cohort', 'Leave-ICEBERG-out', 'Leave-PPMI-out']
file_repo_prefix_name2 = ['LuxPARK', 'PPMI', 'ICEBERG', 'Cross-cohort', 'Leave-\nICEBERG-out', 'Leave-\nPPMI-out']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'coxph', 'rsf', 'tree']
ls_ml_model_name = ['CW-GBoost', 'Extra Survival', 'Survival GBoost','LSVM', 'NLSVM', 'Penalized Cox', 'Survival RF', 'Survival Trees']
ls_ml_model_name2 = ['CW-GBoost', 'Extra\nSurvival', 'Survival\nGBoost','LSVM', 'NLSVM', 'Penalized\nCox', 'Survival\nRF', 'Survival\nTrees']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Type of normalization    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_ana_type = ["", "_unnormalized", "_normalized"]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of predictive features    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for ml_ana_type_loop in ls_ml_ana_type :
    ds_perm_avg_cv_compare = load_models_avg_cv_comparison(ana_type = ml_ana_type_loop)

    # To combine the predictive features from all the cohorts (@file_repo_prefix_loop)
    for loop_ml_prefix in ml_prefix :
        avg_cv_performance_comparison( repo_prefix = file_repo_prefix_loop
                                      ,fig_repo_out = file_repo+'CROSS_COHORT/'+set_outcome+'/'
                                      ,fig_prefix = loop_ml_prefix
                                      ,fig_prefix_add = ml_ana_type_loop
                                      ,list_ml_models = ls_ml_models )
