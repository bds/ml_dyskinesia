#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - One-Shot Nonparametric Approach.py").read() )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Model Performance Analysis                                                               ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_bs_year'
ml_prefix = ['_dyski', '_dyski_excl']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_multi = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'coxph', 'rsf', 'tree']
ls_ml_model_name = ['Component-wise Gradient Boosting', 'Extra Survival Trees', 'Gradient Boosting'
                    ,'Linear SVM', 'Naive Linear SVM', 'Penalised Cox', 'Random Survival Forest', 'Survival Trees']

ls_ml_models_comb = ls_ml_models
ls_ml_model_name_comb = ls_ml_model_name

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    C indices comparison (One-shot nonparametric approach)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

one_shot_nonparametric_test()
normalized_one_shot_nonparametric_test()
all_normalized_one_shot_nonparametric_test()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    C indices comparison (No normalization vs. normalization)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ml_prefix = ['_dyski', '_dyski_excl']
ml_prefix_label = ['Comprehensive', 'Refined']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'Cross-cohort', 'Leave-ICEBERG-out', 'Leave-PPMI-out']

unbiased_reduced_cindex()