#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - Survival Analysis - Performance Comparison (Max AUC and Min Features).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Cross Cohort Analysis                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_bs_year'
ml_prefix = ['_dyski', '_dyski_excl', '_dyski_cf1', '_dyski_cf2']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'coxph', 'rsf', 'tree']
ls_ml_model_name = ['Component-wise Gradient Boosting', 'Extra Survival Trees', 'Gradient Boosting'
                    ,'Linear SVM', 'Naive Linear SVM', 'Penalised Cox', 'Random Survival Forest', 'Survival Trees']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of predictive features    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ds_perm_compact_compare = load_surv_models_compact_comparison()

# To combine the predictive features from all the cohorts (@file_repo_prefix_loop)
for loop_ml_prefix in ml_prefix :
    compact_surv_features_combining( repo_prefix = file_repo_prefix_loop
                                    ,fig_repo_out = file_repo+'CROSS_COHORT/'+set_outcome+'/'
                                    ,fig_prefix = loop_ml_prefix
                                    ,fig_features = data_info[['Variable', 'Description']]
                                    ,list_ml_models = ls_ml_models )