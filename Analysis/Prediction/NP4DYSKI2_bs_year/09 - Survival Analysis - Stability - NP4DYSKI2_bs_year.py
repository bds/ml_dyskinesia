#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                       Time-to-event - Dyskinesias (Year 4)                                     ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_bs_year'

ls_model_prefix = ['_dyski', '_dyski_excl']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
list_model_set = ['cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'coxph', 'rsf', 'tree']
list_model_name = ['Component-wise Gradient Boosting', 'Extra Survival Trees', 'Gradient Boosting'
                    ,'Linear SVM', 'Naive Linear SVM', 'Penalised Cox', 'Random Survival Forest', 'Survival Trees']

for model_prefix_loop in ls_model_prefix :
    pd_stability = pd.DataFrame({'Model': list_model_name})
    for repo_prefix_loop in file_repo_prefix_loop :
        std_kfold_score_model = []
        for model_set_loop in list_model_set :
            # To load the optimal model
            optimal_model = load_object(filename = "ml_model_" + model_set_loop + model_prefix_loop, file_repo = file_pickle+repo_prefix_loop+set_outcome+'/')

            # Optimal hyperparameter in each fold
            kfold_optimal = optimal_model.all_model_cIndex_cv[0]['kfold_opt_hp_m']
            # Performance score for each fold with optimal hyperparameter
            kfold_optimal_score_all = []
            for kfold_optimal_loop in range(len(kfold_optimal)) :
                kfold_optimal_score_all += optimal_model.all_model_cIndex_cv[0]['kfold_cIndex_m'][kfold_optimal_loop][ kfold_optimal[kfold_optimal_loop] ]
            
            # Standard deviation of the performance score for each fold with optimal hyperparameter
            std_kfold_score = statistics.stdev(kfold_optimal_score_all)
            std_kfold_score_model.append(std_kfold_score)
        
        # List of standard deviation of all the model in each particular cohort analysis
        pd_std_kfold_score = pd.DataFrame({'Model': list_model_name, repo_prefix_loop: std_kfold_score_model})
        pd_stability = pd.merge(pd_stability, pd_std_kfold_score, on = 'Model', how = 'left')
    
    pd_stability.to_html('Analysis/Plots/CROSS_COHORT/'+set_outcome+'/'+'C_Index_Cross_Validation_Stability'+model_prefix_loop+'.html')
    pd_stability.to_csv('Analysis/Plots/CROSS_COHORT/'+set_outcome+'/'+'C_Index_Cross_Validation_Stability'+model_prefix_loop+'.csv')