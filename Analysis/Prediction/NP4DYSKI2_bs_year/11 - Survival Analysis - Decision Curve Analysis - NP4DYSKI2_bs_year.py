#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Loading Functions                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - Decision Curve Analysis.py").read() )
exec( open("Analysis/Functions - Risk Scores Based Predicted Survival Function.py").read() )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Cross Cohort Analysis                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Cross cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

type_ana = 'survival'
set_outcome = 'NP4DYSKI2_bs_year'
set_outcome_baseline = 'NP4DYSKI2'
ml_prefix = ['_dyski', '_dyski_excl']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
file_repo_prefix_name = ['LuxPARK', 'PPMI', 'ICEBERG', 'Cross-cohort', 'Leave-ICEBERG-out', 'Leave-PPMI-out']
file_repo_prefix_name2 = ['LuxPARK', 'PPMI', 'ICEBERG', 'Cross-cohort', 'Leave-\nICEBERG-out', 'Leave-\nPPMI-out']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of machine learning algorithms    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_ml_models = ['cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'coxph', 'rsf', 'tree']
ls_ml_model_name = ['CW-GBoost', 'Extra Survival', 'Survival GBoost','LSVM', 'NLSVM', 'Penalized Cox', 'Survival RF', 'Survival Trees']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    List of predictive features    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_ml_prefix in ml_prefix :
    for loop_repo_cohort in file_repo_prefix_loop :
        # Decision curve analysis on the hold-out test set
        dca_hold_out_ana = dca_hold_out( type_ana_set = type_ana
                                        ,repo_prefix = file_pickle+loop_repo_cohort+'/'+set_outcome+'/'
                                        ,fig_repo_out = file_repo+loop_repo_cohort+'/'+set_outcome+'/'
                                        ,fig_prefix = loop_ml_prefix
                                        ,fig_prefix_add = ""
                                        ,list_ml_models = ls_ml_models
                                        ,list_ml_model_name = ls_ml_model_name
                                        ,dpi = 300 )
        dca_hold_out_ana.load_models(time_point = 4)
        dca_hold_out_ana.dca_net_plots()        # Decision curve analysis plot
        dca_hold_out_ana.dca_area_net_benefit() # Area under the net benefit
        dca_hold_out_ana.calibration_plots()    # Calibration plot and calibration slope
        
        # Decision curve analysis on the cross-validation
        dca_cv_ana = dca_cross_validation( type_ana_set = type_ana
                                          ,repo_prefix = file_pickle+loop_repo_cohort+'/'+set_outcome+'/'
                                          ,fig_repo_out = file_repo+loop_repo_cohort+'/'+set_outcome+'/'
                                          ,fig_prefix = loop_ml_prefix
                                          ,fig_prefix_add = ""
                                          ,list_ml_models = ls_ml_models
                                          ,list_ml_model_name = ls_ml_model_name
                                          ,dpi = 300 )
        dca_cv_ana.load_models(time_point = 4)
        dca_cv_ana.dca_net_plots()              # Decision curve analysis plot
        dca_cv_ana.dca_area_net_benefit()       # Area under the net benefit
