#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                       Time-to-event - Dyskinesias (Year 4)                                     ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']
label_outcome = 'Levodopa-induced dyskinesia'

ls_model_prefix = ['_dyski', '_dyski_excl']
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/', 'ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']
list_model_set = ['coxph', 'cwgboost', 'extra', 'gboost', 'lsvm', 'nlsvm', 'rsf', 'tree']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load function of hazard ratio measurement    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

outcome_define = set_outcome[0]
exec( open("Analysis/01 - Data Processing - Labels of Baseline Variables.py").read() )
exec( open("Analysis/Functions - Hazard Ratio of Machine Learning Survival.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To load the optimal model and train the model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# List of outcome, cohort, and algorithm
ls_BLoop = []
for loop_file_repo in file_repo_prefix_loop :
    for loop_model_set in list_model_set :
        for loop_model_prefix in ls_model_prefix :

            loop_optimal_model = load_object(filename = "ml_model_" + loop_model_set + loop_model_prefix, file_repo = file_pickle+loop_file_repo+set_outcome[0]+'/')
            ls_BLoop.append( [loop_file_repo, loop_model_set, loop_model_prefix, loop_optimal_model] )

# To measure the Hazard Ratio for each cohort and algorithm
HR_BLoop = Parallel(n_jobs = 1)(
    delayed( func_HR_measure )(BLoop = BLoop)
    for BLoop in range(len(ls_BLoop)) )

# To save the table to Hazard Ratio
for BLoop in range(len(ls_BLoop)) :

    loop_file_repo = ls_BLoop[BLoop][0]
    loop_model_set = ls_BLoop[BLoop][1]
    loop_model_prefix = ls_BLoop[BLoop][2]

    # Data with hazards ratio
    data_HR = HR_BLoop[BLoop]['data_HR']

    # Save the table of hazards ratio
    data_HR.to_csv(file_repo+loop_file_repo+set_outcome[0]+'/Hazard Ratio/'+'Hazard_Ratio_'+loop_model_set+loop_model_prefix+'.csv')
    data_HR.to_html(file_repo+loop_file_repo+set_outcome[0]+'/Hazard Ratio/'+'Hazard_Ratio_'+loop_model_set+loop_model_prefix+'.html')

    # Kaplan-Meier plot
    KM_plots = HR_BLoop[BLoop]['KM_plot']
    for fig_save in list(KM_plots.keys()) :
        KM_plots[fig_save].savefig(file_repo+loop_file_repo+set_outcome[0]+'/Hazard Ratio/KM_'+loop_model_set+loop_model_prefix+'_'+fig_save+'.png', bbox_inches='tight', dpi=300)