#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                         Random Undersampling (Binary)                                         #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Random undersampling is the process of randomly removing instances from the majority class or classes.

# @ data: Dataset to be implemented random undersampling (pd.DataFrame)
# @ var_outcome: Outcome variable (string)
# @ targetBalance: Proportion of majority class (vs minority class) to be selected

def underSamplingBinary(data, var_outcome, targetBalance = 0.5):

    # Define majority/ minority classes
    yMajority = data[var_outcome].value_counts().idxmax()
    yMinority = data[var_outcome].value_counts().idxmin()

    # Create separate majority and minority lists
    majority = data[ data[var_outcome] == yMajority ].values.tolist()
    minority = data[ data[var_outcome] == yMinority ].values.tolist()

    # While the length of the majority is larger than the targeted balance
    # We randomly remove one instance from the majority dataset
    np.random.seed(890701)
    random_range = np.random.randint(low = 1, high = len(majority), size = len(majority))
    df_random = pd.DataFrame({'random_number' : [x for x in range(len(majority))]
                              ,'random_range' : random_range })
    df_random.sort_values(['random_range', 'random_number'], inplace = True)
    df_random.reset_index(drop = True, inplace = True)

    while len(majority) / ( len(minority) + len(majority) ) > targetBalance :
        majority = [majority[x] for x in df_random['random_number'][0:len(minority)].tolist()]
        # majority.pop(random.randrange(len(majority)))
    
    # Take the original column names
    columnNames = list(data.columns)
    newDataset = minority + majority
    newDataset = pd.DataFrame(newDataset, columns = columnNames)

    # To return the output
    return(newDataset)
