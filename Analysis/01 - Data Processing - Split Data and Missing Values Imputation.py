#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                             Levodopa-induced dyskinesia (NP4DYSKI2_Y4)                                                             ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Categorical Variables    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Common variables
vars_category_common = data_variables[data_variables['Variable Type'].isin(['Ordinal', 'Nominal', 'Binary'])]['PPMI Variable']
vars_category_common = [ x for x in vars_category_common if x in data_info[data_info['Type'] == 'Baseline']['Variable'].tolist() ]

# LUXPARK
vars_category_luxpark = data_variables_luxpark[data_variables_luxpark['Variable Type'].isin(['Ordinal', 'Nominal', 'Binary'])]['LuxPARK Variable']
vars_category_luxpark = [ x for x in vars_category_luxpark if x in data_info_luxpark[data_info_luxpark['Type'] == 'Baseline']['Variable'].tolist() ]

# PPMI
vars_category_ppmi = data_variables_ppmi[data_variables_ppmi['Variable Type'].isin(['Ordinal', 'Nominal', 'Binary'])]['PPMI Variable']
vars_category_ppmi = [ x for x in vars_category_ppmi if x in data_info_ppmi[data_info_ppmi['Type'] == 'Baseline']['Variable'].tolist() ]

# ICEBERG
vars_category_iceberg = data_variables_iceberg[data_variables_iceberg['Variable Type'].isin(['Ordinal', 'Nominal', 'Binary'])]['ICEBERG Variable']
vars_category_iceberg = [ x for x in vars_category_iceberg if x in data_info_iceberg[data_info_iceberg['Type'] == 'Baseline']['Variable'].tolist() ]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                         Classification                                                                             ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Levodopa-induced dyskinesia (NP4DYSKI2_Y4)
exec( open("Analysis/01 - Data Processing - Split Data and Missing Values Imputation - NP4DYSKI2_Y4.py").read() )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                       Surviva Analysis                                                                             ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Levodopa-induced dyskinesia year and status (NP4DYSKI2_bs_year & NP4DYSKI2_status)
exec( open("Analysis/01 - Data Processing - Split Data and Missing Values Imputation - NP4DYSKI2_bs_year.py").read() )
