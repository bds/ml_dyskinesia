#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                    Load Spearman Correlation Function                                                                    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Spearman Correlation Function    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/Functions - Spearman Correlation to Outcome.py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                NP4DYSKI2_Y4 - Dyskinesia in 4 years follow-up                                                            ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Set Outcome    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = 'NP4DYSKI2_Y4'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Correlation coefficient estimation    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Spearman correlation of features with binary outcome variable
corr_outcome = Spearman_pb_outcome(data = [data_cohort_luxpark[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_comb[vars_baseline_dyski+[set_outcome]]
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]], axis = 0)
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]], axis = 0) ]
                                ,data_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'LUXPARK + PPMI + ICEBERG', 'LUXPARK + PPMI', 'LUXPARK + ICEBERG']
                                ,outcome = set_outcome
                                ,vars_name = data_info_all_set
                                ,repo_output = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
corr_outcome.measure()

# Spearman correlation of two continuous/ordinal variables
corr_features = Correlation_features(data = [data_cohort_luxpark[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_comb[vars_baseline_dyski+[set_outcome]]
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]], axis = 0)
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]], axis = 0) ]
                                ,data_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'LUXPARK + PPMI + ICEBERG', 'LUXPARK + PPMI', 'LUXPARK + ICEBERG']
                                ,outcome = set_outcome
                                ,vars_name = data_info_all_set
                                ,repo_output = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
corr_features.measure()

# Point-biserial correlation of binary vs continuous variables
pb_corr_features = Point_biserial_features(data = [data_cohort_luxpark[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_comb[vars_baseline_dyski+[set_outcome]]
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]], axis = 0)
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]], axis = 0) ]
                                ,data_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'LUXPARK + PPMI + ICEBERG', 'LUXPARK + PPMI', 'LUXPARK + ICEBERG']
                                ,outcome = set_outcome
                                ,vars_name = data_info_all_set
                                ,repo_output = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
pb_corr_features.measure()

# Kendall's tau correlation of two ordinal variables
kt_corr_features = Kendall_tau_features(data = [data_cohort_luxpark[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_comb[vars_baseline_dyski+[set_outcome]]
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]], axis = 0)
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]], axis = 0) ]
                                ,data_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'LUXPARK + PPMI + ICEBERG', 'LUXPARK + PPMI', 'LUXPARK + ICEBERG']
                                ,outcome = set_outcome
                                ,vars_name = data_info_all_set
                                ,repo_output = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
kt_corr_features.measure()

# Matthews correlation coefficient (MCC) of two binary variables
mcc_corr_features = MCC_features(data = [data_cohort_luxpark[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]
                                        ,data_cohort_comb[vars_baseline_dyski+[set_outcome]]
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_ppmi[vars_baseline_dyski+[set_outcome]]], axis = 0)
                                        ,pd.concat([data_cohort_luxpark[vars_baseline_dyski+[set_outcome]], data_cohort_iceberg[vars_baseline_dyski+[set_outcome]]], axis = 0) ]
                                ,data_name = ['LUXPARK', 'PPMI', 'ICEBERG', 'LUXPARK + PPMI + ICEBERG', 'LUXPARK + PPMI', 'LUXPARK + ICEBERG']
                                ,outcome = set_outcome
                                ,vars_name = data_info_all_set
                                ,repo_output = file_repo+'CROSS_COHORT/'+set_outcome+'/' )
mcc_corr_features.measure()