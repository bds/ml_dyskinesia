#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Missing values imputation in cohorts                                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# LUXPARK
data_unimputed = data_cohort_luxpark[vars_baseline+['NP4DYSKI2_Y4']]
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_luxpark_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'LUXPARK/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()

# PPMI
data_unimputed = data_cohort_ppmi[vars_baseline+['NP4DYSKI2_Y4']]
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_ppmi_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'PPMI/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()

# ICEBERG
data_unimputed = data_cohort_iceberg[vars_baseline+['NP4DYSKI2_Y4']]
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_iceberg_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'ICEBERG/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()

# LUXPARK + PPMI
data_unimputed = pd.concat([ data_cohort_luxpark[vars_baseline+['NP4DYSKI2_Y4']], data_cohort_ppmi[vars_baseline+['NP4DYSKI2_Y4']] ], axis = 0)
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
data_unimputed.reset_index(drop = True, inplace = True)
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_luxpark_ppmi_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'LEAVE_ONE_OUT/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()

# LUXPARK + ICEBERG
data_unimputed = pd.concat([ data_cohort_luxpark[vars_baseline+['NP4DYSKI2_Y4']], data_cohort_iceberg[vars_baseline+['NP4DYSKI2_Y4']] ], axis = 0)
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
data_unimputed.reset_index(drop = True, inplace = True)
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_luxpark_iceberg_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'LEAVE_ONE_OUT2/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()


# LUXPARK + PPMI + ICEBERG
data_unimputed = data_cohort_comb[vars_baseline+['NP4DYSKI2_Y4']]
data_unimputed = data_unimputed[~data_unimputed['NP4DYSKI2_Y4'].isnull()]
data_unimputed.reset_index(drop = True, inplace = True)
imp_cohort = impute_cohort(data = data_unimputed
                           ,data_name = 'data_cohort_comb_imputed'
                           ,outcome = 'NP4DYSKI2_Y4'
                           ,save_data_repo = file_data+'ALL/NP4DYSKI2_Y4/'
                           ,vars_cat = vars_category_common )
imp_cohort.split_n_impute()