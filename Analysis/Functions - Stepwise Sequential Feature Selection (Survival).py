def processSubset_surv(X, y, X2, y2, feature_set, estimator) :

    ml_model = estimator
    try :
        ml_model.fit(X[feature_set], y)
        c_index = ml_model.score(X2[feature_set], y2)
    except Exception :
        c_index = 0
    return({'model': ml_model, 'predictor': feature_set, 'c-index': c_index})


def features_surv_order(X, y, X2, y2, estimator, continuos_first) :

    if continuos_first == True :
        vars_conti = [x for x in X.columns if len(np.unique(X[x])) > 2 ]
    else :
        vars_conti = X.columns.tolist()

    features = []
    c_index = []
    for p in X.columns :
        if p in vars_conti :
            results = processSubset_surv(X = X, y = y, X2 = X2, y2 = y2, feature_set = [p], estimator = estimator)
            features.append(results['predictor'][0])
            c_index.append(results['c-index'])
        else :
            features.append( p )
            c_index.append( 0 )
    
    df_features = pd.DataFrame({'features': features, 'c-index': c_index})
    df_features.sort_values(by = 'c-index', ascending = False, inplace = True)
    df_features.reset_index(drop = True, inplace = True)

    return({ 'features': df_features['features'].tolist(), 'c-index': df_features['c-index'].tolist() })


def forward_surv(X, y, X2, y2, predictors, best_predictors, estimator) :

    remaining_predictors = [p for p in X.columns if p not in predictors]
    if len(remaining_predictors) > 0 :
        results = []
        for p in remaining_predictors :
            results.append( processSubset_surv(X = X, y = y, X2 = X2, y2 = y2, feature_set = best_predictors + [p], estimator = estimator) )
        models = pd.DataFrame(results)

        best_model = models.loc[models['c-index'].argmax()]
    else :
        best_model = {'model': estimator, 'predictor': best_predictors, 'c-index': 0}
        predictors = best_predictors
        
    return({ 'best_model': best_model, 'predictors': predictors })



def backward_surv(X, y, X2, y2, best_predictors, estimator):
    
    results = []
    for combo in itertools.combinations(best_predictors, len(best_predictors) - 1) :
        results.append( processSubset_surv(X = X, y = y, X2 = X2, y2 = y2, feature_set = best_predictors, estimator = estimator) )
    models = pd.DataFrame(results)
   
    best_model = models.loc[models['c-index'].argmax()]
    return( best_model )



def stepwise_surv_model(ds_X_train, ds_y_train, ds_X_test, ds_y_test, estimator, max_iter, continuos_first ) :

    # Variables with > 1 unique level
    vars_keep = [x for x in ds_X_train.columns if len(np.unique(ds_X_train[x])) > 1 ]
    ds_X_train = ds_X_train[vars_keep]
    ds_X_test = ds_X_test[vars_keep]

    Stepmodels = pd.DataFrame(columns = ['c-index', 'predictor', 'model'])

    fo = features_surv_order(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, estimator = estimator, continuos_first = continuos_first)
    predictors_order = fo['features']

    ds_X_train = ds_X_train[predictors_order]
    ds_X_test = ds_X_test[predictors_order]

    predictors = [predictors_order[0]]
    Smodel_before = fo['c-index'][0]
    best_predictors = [predictors_order[0]]

    cnt = 0
    max_iter_cnt = 0
    while (cnt < 1) & (max_iter_cnt <= max_iter) & (max_iter_cnt <= ds_X_train.shape[1]) :
        max_iter_cnt += 1
        Forward_result = forward_surv(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, predictors = predictors, best_predictors = best_predictors, estimator = estimator)
        # print('forward')
        # Stepmodels.loc[icol] = Forward_result['best_model']
        Stepmodels = Forward_result['best_model']
        predictors = Forward_result['best_model']["predictor"]

        # Check if there is anything to remove
        Backward_result = backward_surv(X = ds_X_train, y = ds_y_train, X2 = ds_X_test, y2 = ds_y_test, best_predictors = best_predictors, estimator = estimator)

        if Backward_result['c-index'] > Forward_result['best_model']['c-index'] :
            Stepmodels = Backward_result
            best_predictors = Backward_result['predictor']
            Smodel_before = Stepmodels['c-index']
            # print('backward')
        
        # if Stepmodels['c-index'] == Smodel_before :
        #     cnt = 1
        
        if Stepmodels['c-index'] > Smodel_before :
            Smodel_before = Stepmodels['c-index']
            best_predictors = Stepmodels['predictor']
        else:
            cnt = 1
    
    return( {'c-index': Smodel_before, 'predictors': best_predictors } )


def surv_stepwise_cv(ds_X, ds_y, ds_status, estimator = RandomSurvivalForest(max_depth = 5), repeat = 1, seed_number = 890701, max_iter = 25, cv = 1, continuos_first = False) :

    selected_features = []
    selected_features_c_index = []

    for B_loop in range(repeat) :

        if cv == 1 :
            kfold_split = StratifiedShuffleSplit(n_splits = cv, random_state = seed_number + 100*B_loop, test_size = 0.33)
        else :
            kfold_split = StratifiedShuffleSplit(n_splits = cv, random_state = seed_number + 100*B_loop, test_size = 1/cv)

        def func_features_cv_k( k, index ):
            train_index, test_index = index
            X_k_train, X_k_test = ds_X.iloc[train_index].reset_index(drop = True) , ds_X.iloc[test_index].reset_index(drop = True)
            y_k_train, y_k_test = ds_y[train_index], ds_y[test_index]

            model_predictors = stepwise_surv_model(ds_X_train = X_k_train, ds_y_train = y_k_train, ds_X_test = X_k_test, ds_y_test = y_k_test, estimator = estimator, max_iter = max_iter, continuos_first = continuos_first )
            best_predictors = model_predictors['predictors']
            c_index_predictors = model_predictors['c-index']

            return({'best_predictors': best_predictors, 'c_index_predictors': c_index_predictors})
        
        features_cv_k = Parallel(n_jobs = 1)(
            delayed( func_features_cv_k )(k = k, index = (train_index, test_index) )
            for k, (train_index, test_index) in enumerate(kfold_split.split(ds_X, ds_status)) )
        
        for cv_k in range(len(features_cv_k)):
            selected_features.append( features_cv_k[0]['best_predictors'] )
            selected_features_c_index.append( features_cv_k[0]['c_index_predictors'] )

    mean_c_index = np.percentile(selected_features_c_index, 75)
    idx_c_index = [x for x in range(len(selected_features_c_index)) if selected_features_c_index[x] >= mean_c_index]
    ls_stepwise_features = [selected_features[x] for x in idx_c_index]
    stepwise_features = []
    for sfts in ls_stepwise_features :
        stepwise_features += sfts
    stepwise_features = np.unique(stepwise_features).tolist()

    return(stepwise_features)
