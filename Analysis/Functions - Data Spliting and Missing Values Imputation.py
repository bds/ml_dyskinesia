def func_round_variables(data) :

    # Nominal, ordinal and binary variables in PPMI
    data_vars_type = data_variables[ ~pd.isna(data_variables['Variable Type']) ][['PPMI Variable', 'Variable Type']]
    data_vars_nominal = data_vars_type[ (data_vars_type['Variable Type'] == 'Nominal') &\
        (data_vars_type['PPMI Variable'].isin(data_info[data_info['Type'] == 'Baseline']['Variable'].tolist())) ]['PPMI Variable'].tolist()
    data_vars_ordinal = data_vars_type[ (data_vars_type['Variable Type'] == 'Ordinal') &\
        (data_vars_type['PPMI Variable'].isin(data_info[data_info['Type'] == 'Baseline']['Variable'].tolist())) ]['PPMI Variable'].tolist()
    data_vars_binary = data_vars_type[ (data_vars_type['Variable Type'] == 'Binary') &\
        (data_vars_type['PPMI Variable'].isin(data_info[data_info['Type'] == 'Baseline']['Variable'].tolist())) ]['PPMI Variable'].tolist()
    
    # Nominal, ordinal and binary variables in LuxPARK
    data_vars_type2 = data_variables_luxpark[ ~pd.isna(data_variables_luxpark['Variable Type']) ][['LuxPARK Variable', 'Variable Type']]
    data_vars_nominal2 = data_vars_type2[ (data_vars_type2['Variable Type'] == 'Nominal') &\
        (data_vars_type2['LuxPARK Variable'].isin(data_info_luxpark[data_info_luxpark['Type'] == 'Baseline']['Variable'].tolist())) ]['LuxPARK Variable'].tolist()
    data_vars_ordinal2 = data_vars_type2[ (data_vars_type2['Variable Type'] == 'Ordinal') &\
        (data_vars_type2['LuxPARK Variable'].isin(data_info_luxpark[data_info_luxpark['Type'] == 'Baseline']['Variable'].tolist())) ]['LuxPARK Variable'].tolist()
    data_vars_binary2 = data_vars_type2[ (data_vars_type2['Variable Type'] == 'Binary') &\
        (data_vars_type2['LuxPARK Variable'].isin(data_info_luxpark[data_info_luxpark['Type'] == 'Baseline']['Variable'].tolist())) ]['LuxPARK Variable'].tolist()
    
    # Nominal, ordinal and binary variables in ICEBERG
    data_vars_type3 = data_variables_iceberg[ ~pd.isna(data_variables_iceberg['Variable Type']) ][['ICEBERG Variable', 'Variable Type']]
    data_vars_nominal3 = data_vars_type3[ (data_vars_type3['Variable Type'] == 'Nominal') &\
        (data_vars_type3['ICEBERG Variable'].isin(data_info_iceberg[data_info_iceberg['Type'] == 'Baseline']['Variable'].tolist())) ]['ICEBERG Variable'].tolist()
    data_vars_ordinal3 = data_vars_type3[ (data_vars_type3['Variable Type'] == 'Ordinal') &\
        (data_vars_type3['ICEBERG Variable'].isin(data_info_iceberg[data_info_iceberg['Type'] == 'Baseline']['Variable'].tolist())) ]['ICEBERG Variable'].tolist()
    data_vars_binary3 = data_vars_type3[ (data_vars_type3['Variable Type'] == 'Binary') &\
        (data_vars_type3['ICEBERG Variable'].isin(data_info_iceberg[data_info_iceberg['Type'] == 'Baseline']['Variable'].tolist())) ]['ICEBERG Variable'].tolist()
    
    list_vars_nominal = [x for x in np.unique(data_vars_nominal + data_vars_nominal2 + data_vars_nominal3) if x in data.columns]
    list_vars_ordinal = [x for x in np.unique(data_vars_ordinal + data_vars_ordinal2 + data_vars_ordinal3) if x in data.columns]
    list_vars_binary = [x for x in np.unique(data_vars_binary + data_vars_binary2 + data_vars_binary3) if x in data.columns]

    # Rounding off nominal variables
    for var_nominal in list_vars_nominal :
        data[var_nominal] = data[var_nominal].round()

        if var_nominal in ['gen', 'gender', 'sexe'] :
            # 1, 2
            data[var_nominal] = np.where(data[var_nominal] < 1, 1, np.where(data[var_nominal] > 2, 2, data[var_nominal]))

        elif var_nominal in ['tremor_predominant', 'rigidity_predominant', 'HANDED', 'coffee'] :
            # 1, 2, 3
            data[var_nominal] = np.where(data[var_nominal] < 1, 1, np.where(data[var_nominal] > 3, 3, data[var_nominal]))

        elif var_nominal in ['med_anticholinergic'] :
            # 0, 1, 2, 3
            data[var_nominal] = np.where(data[var_nominal] < 0, 0, np.where(data[var_nominal] > 3, 3, data[var_nominal]))

        elif var_nominal in ['alcohol'] :
            # 1, 2, 3, 4
            data[var_nominal] = np.where(data[var_nominal] < 1, 1, np.where(data[var_nominal] > 4, 4, data[var_nominal]))

        elif var_nominal in ['final_diag_parkinsonism'] :
            # 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
            data[var_nominal] = np.where(data[var_nominal] < 1, 1, np.where(data[var_nominal] > 11, 11, data[var_nominal]))

        elif var_nominal in ['med_antidepressants'] :
            # 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            data[var_nominal] = np.where(data[var_nominal] < 0, 0, np.where(data[var_nominal] > 20, 20, data[var_nominal]))

        else :
            # 0, 1
            data[var_nominal] = np.where(data[var_nominal] < 0, 0, np.where(data[var_nominal] > 1, 1, data[var_nominal]))
    
    # Rounding off ordinal variables
    for var_ordinal in list_vars_ordinal :
        data[var_ordinal] = data[var_ordinal].round()

        if var_ordinal in ['NHY', 'hoehn_and_yahr_staging', 'hy'] :
            # 0, 1, 2, 3, 4, 5
            data[var_ordinal] = np.where(data[var_ordinal] < 0, 0, np.where(data[var_ordinal]>5, 5, data[var_ordinal]))
        else :
            # 0, 1, 2, 3, 4
            data[var_ordinal] = np.where(data[var_ordinal] < 0, 0, np.where(data[var_ordinal]>4, 4, data[var_ordinal]))
    
    # Rounding off binary variables
    for var_binary in list_vars_binary :
        data[var_binary] = np.where(data[var_binary] < 0, 0, np.where(data[var_binary] > 1, 1, data[var_binary]))
    
    return(data)




def train_test_split_bs(ds_X
                     ,ls_y
                     ,test_size
                     ,stratify_bs
                     ,random_state ) :
    """
    Split arrays or matrices into random train and test subsets.

    Parameters
    ----------
    ds_X : pandas dataframes for X (patient ID as index)
    ls_y : array for the outcome
    test_size : float (proportion)
    stratify_bs : variables to be stratified
    random_state: int
        Controls the shuffling applied to the data before applying the split
    """

    # Reset the index of ds_X
    ds_X = ds_X.reset_index(inplace = False, drop = False)

    # number of subjects for the test set
    num_test = round(ds_X.shape[0] * test_size)

    #####   Testing set    ####
    # Samples for only the test set (remove stratify_bs = 1)
    cohort_X_0 = ds_X[ ds_X[stratify_bs] != 1 ]
    indices_0 = cohort_X_0.index.values
    cohort_y_0 = [ ls_y[x] for x in indices_0]

    if num_test > cohort_X_0.shape[0] :
        # If the num_test is greater than the number of rows where baseline outcome != 1,
        # then split by test_size (%) then exclude baseline outcome != 1
        # Otherwise, split the hold-out test set based on number of rows (33% of whole data)
        cohort_array_0_ = [item.split('_')[0] for item in ds_X['PATNO'].tolist()]
        cohort_array_1_ = sorted( set( cohort_array_0_ ) )
        cohort_array_2_ = {item: index + 1 for index, item in enumerate(cohort_array_1_)}
        cohort_array_ = [cohort_array_2_[item] for item in cohort_array_0_]
        stratified_array_ = [int(str(int(a)) + str(int(b))) for a, b in zip(cohort_array_, ls_y)]
        X_train_0_, X_test_0_, y_train_0_, y_test_0_ = train_test_split(ds_X, ls_y, test_size = test_size, stratify = stratified_array_, random_state = random_state)
        X_test_0_ = X_test_0_[ X_test_0_[stratify_bs] != 1 ]
        
        X_train_indices = [x for x in ds_X.index.values if x not in list(X_test_0_.index.values)]
        X_test_indices = [x for x in ds_X.index.values if x in list(X_test_0_.index.values)]
        X_train_0, X_test_0 = ds_X.iloc[ X_train_indices ], ds_X.iloc[ X_test_indices ]
        y_train_0, y_test_0 = [ls_y[i] for i in X_train_indices], [ls_y[i] for i in X_test_indices]
    else :
        # To create an array which need to be stratified during the splitting (by cohort and outcome)
        cohort_array_0 = [item.split('_')[0] for item in cohort_X_0['PATNO'].tolist()]
        cohort_array_1 = sorted( set( cohort_array_0 ) )
        cohort_array_2 = {item: index + 1 for index, item in enumerate(cohort_array_1)}
        cohort_array = [cohort_array_2[item] for item in cohort_array_0]
        stratified_array = [int(str(int(a)) + str(int(b))) for a, b in zip(cohort_array, cohort_y_0)]

        X_train_0, X_test_0, y_train_0, y_test_0 = train_test_split(cohort_X_0, cohort_y_0, test_size = num_test, stratify = stratified_array, random_state = random_state)
    
    X_train_indices = [x for x in ds_X.index.values if x not in list(X_test_0.index.values)]
    X_train_all = ds_X.iloc[X_train_indices]
    y_train_all = [ls_y[i] for i in X_train_indices]

    X_train_all.set_index(['PATNO'], inplace = True)
    X_test_0.set_index(['PATNO'], inplace = True)

    return(X_train_all, X_test_0, y_train_all, y_test_0)





class split_impute:
    """
    split_impute -- To split the data and impute missing values (missingness < 50%)
    Parameters
    ----------
    data : pd.DataFrame
        Data for splitting into training and testing
    val_data : pd.DataFrame (optional)
        Validating data
    outcome : str
        Outcome variable
    B_data : int
        Number of Bootstrapping
    prop_train : float
        Proportion of the training set
    k_fold : int
        k-fold cross-validation
    pct_miss : float
        Proportion of the missingness cutoff to be imputed (max 0.5)
    undersampling : boolean
        To perform undersampling on training set
    save_data_repo : str
        Repository of the saved data
    ----------------------------------
    |      Training     | Validation |  <- repeated B times on Training
    ----------------------------------
    |       k-fold      |
    ---------------------
    |  inner m-fold  |
    ------------------
    """

    def __init__(self, data, outcome, bs_outcome, save_data_repo, vars_cat, val_data = pd.DataFrame(), B_data = 1, prop_train = 0.67, k_fold = 5, m_fold = 3, pct_miss = 0.5, ds_prefix = ""): #, undersampling = False):
        self.data = data
        self.outcome = outcome
        self.bs_outcome = bs_outcome
        self.vars_cat = vars_cat
        self.val_data = val_data
        self.B_data = B_data
        self.prop_train = prop_train
        self.k_fold = k_fold
        self.m_fold = m_fold
        self.pct_miss = pct_miss
        # self.undersampling = undersampling
        self.save_data_repo = save_data_repo
        self.ds_prefix = ds_prefix
    

    def split_n_impute(self, seed_number = 890701, n_jobs = 10):
        """
        To split the data into training (with k-fold cross-validation) and testing set
        To impute the missing values into each data separately
        Parameters
        ----------
        seed_number : int
            Random seed
        Returns
        saved_data : Pickle Data
            DataFrame with missing value imputation
        ----------
        """

        # To split the data into training and hold-out set
        ds = self.data[ ~self.data[self.outcome].isnull() ]

        # To split the data into training and testing set
        y = ds[self.outcome].tolist()
        X = ds.drop([self.outcome], axis = 1)

        if self.val_data.shape[0] == 0 :
            X_train, X_test, y_train, y_test = train_test_split_bs(ds_X = X, ls_y = y, test_size = 1-self.prop_train, stratify_bs = self.bs_outcome, random_state = seed_number)
            
        else :
            X_train = X
            y_train = y

            ds_val = self.val_data.copy()
            ds_val = ds_val[ ~ds_val[self.outcome].isnull() ]
            # Remove baseline outcome = 1 (self.bs_outcome)
            ds_val = ds_val[ ds_val[self.bs_outcome] != 1 ]

            X_test = ds_val.drop([self.outcome], axis = 1)
            y_test = ds_val[self.outcome].tolist()

        # Check if X_train and X_test has > 50% missing value
        # If the missingness < pct_miss (50%) include into the analysis
        vars_excl = []
        for cols_loop in X_train.columns.tolist() :
            if X_train[cols_loop].isnull().sum() / X_train.shape[0] >= self.pct_miss :
                vars_excl = vars_excl + [cols_loop]
        
        for cols_loop in X_test.columns.tolist() :
            if X_test[cols_loop].isnull().sum() / X_test.shape[0] >= self.pct_miss :
                vars_excl = vars_excl + [cols_loop]
        
        # To keep the data with self.bs_outcome when missingness of self.bs_outcome > self.pct_miss
        X_train_bs_keep = X_train.copy()
        X_test_bs_keep = X_test.copy()

        vars_excl = list(set(vars_excl))
        if len(vars_excl) > 0:
            X_train.drop(vars_excl, axis = 1, inplace = True)
            X_test.drop(vars_excl, axis = 1, inplace = True)

        # Categorical variables to be imputed
        vars_cat_imp = [x for x in X_train.columns.tolist() if x in self.vars_cat ]

        # for B_loop in range(self.B_data) :
        def func_split_n_impute( B_loop ) :
            
            kfold_split = StratifiedShuffleSplit(n_splits = self.k_fold, random_state = seed_number + 100*B_loop, test_size = 1/self.k_fold)

            vars_miss_k = []
            # vars_miss_k_undersamp = []

            # To create a list for stratifying by outcome and cohort
            stratify_array_0 = [item.split('_')[0] for item in X_train.index.values]
            stratify_array_1 = sorted(set(stratify_array_0))
            stratify_array_2 = {item: index + 1 for index, item in enumerate(stratify_array_1)}
            stratify_array = [stratify_array_2[item] for item in stratify_array_0]
            y_train_stratify = [int(str(int(a)) + str(int(b)) ) for a, b in zip(stratify_array, y_train )]

            # Outer loop cross-validation
            # for k, (train_index, test_index) in enumerate( kfold_split.split(X_train, y_train) ):
            for k, (train_index, test_index) in enumerate( kfold_split.split(X_train, y_train_stratify) ):

                X_k_train, X_k_test = X_train.iloc[train_index], X_train.iloc[test_index]
                y_k_train, y_k_test = np.array(y_train)[train_index], np.array(y_train)[test_index]

                # To keep the data with self.bs_outcome when missingness of self.bs_outcome > self.pct_miss
                X_k_train_bs_keep, X_k_test_bs_keep = X_train_bs_keep.iloc[train_index], X_train_bs_keep.iloc[test_index]

                # Variables in the training and testing set with missingness < self.pct_miss
                # vars_pct_miss = X_k_train.isna().sum() / X_k_train.shape[0]
                # vars_miss = vars_pct_miss[ vars_pct_miss < self.pct_miss ].reset_index()['index'].tolist()
                # vars_miss_k.append(vars_miss)

                vars_train_pct_miss = X_k_train.isna().sum() / X_k_train.shape[0]
                vars_train_miss = vars_train_pct_miss[ vars_train_pct_miss < self.pct_miss ].reset_index()['index'].tolist()
                vars_test_pct_miss = X_k_test.isna().sum() / X_k_test.shape[0]
                vars_test_miss = vars_test_pct_miss[ vars_test_pct_miss < self.pct_miss ].reset_index()['index'].tolist()
                vars_miss_ = list(set(vars_train_miss).intersection(vars_test_miss))
                vars_miss = [x for x in X_k_train.columns if x in vars_miss_]
                vars_miss_k.append(vars_miss)

                # Categorical variables in the training set
                vars_cat_imp_k = [x for x in X_k_train[vars_miss].columns.tolist() if x in vars_cat_imp ]

                # Missing value imputation (training data in k-fold cross-validation)
                imp_br_train = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
                imp_br_train.fit(X_k_train[vars_miss])
                imp_X_k_train = pd.DataFrame(imp_br_train.transform(X_k_train[vars_miss]), columns = vars_miss, index = X_k_train.index)
                imp_X_k_train[self.outcome] = y_k_train
                imp_X_k_train = imp_X_k_train.reset_index()

                imp_br_train_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(), max_iter = 10, tol = 1e-3)
                imp_br_train_cat.fit(X_k_train[vars_cat_imp_k])
                imp_X_k_train_cat = pd.DataFrame(imp_br_train_cat.transform(X_k_train[vars_cat_imp_k]), columns = vars_cat_imp_k, index = X_k_train.index)
                imp_X_k_train_cat = imp_X_k_train_cat.reset_index()
                for cat_imp in vars_cat_imp_k :
                    imp_X_k_train[cat_imp] = imp_X_k_train_cat[cat_imp]

                # imp_X_k_train_round = func_round_variables(imp_X_k_train)
                save_object(data = imp_X_k_train, filename = self.ds_prefix+'train_B'+str(B_loop+1)+'_k'+str(k+1), file_repo = self.save_data_repo)

                # Missing value imputation (testing data in k-fold cross-validation)
                keep_index_1 = X_k_test.copy()
                keep_index_2 = keep_index_1.reset_index()
                
                if self.bs_outcome in keep_index_2.columns.tolist() :
                    keep_index_3 = keep_index_2[ keep_index_2[self.bs_outcome] != 1 ] # Remove baseline outcome == 1 (self.bs_outcome)
                    keep_index = keep_index_3.index.values
                else :
                    # To keep the data with self.bs_outcome when missingness of self.bs_outcome > self.pct_miss
                    # To ensure self.bs_outcome == 1 is excluded from the testing set
                    X_k_test_bs_keep_ = X_k_test_bs_keep.reset_index(drop = True)
                    keep_index_3 = X_k_test_bs_keep_[ X_k_test_bs_keep_[self.bs_outcome] != 1 ] # Remove baseline outcome == 1 (self.bs_outcome)
                    keep_index = keep_index_3.index.values

                X_k_test = X_k_test.iloc[ keep_index ]
                y_k_test = y_k_test[ keep_index ]

                imp_br_test = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
                imp_br_test.fit(X_k_test[vars_miss])
                imp_X_k_test = pd.DataFrame(imp_br_test.transform(X_k_test[vars_miss]), columns = vars_miss, index = X_k_test.index)
                imp_X_k_test[self.outcome] = y_k_test
                imp_X_k_test = imp_X_k_test.reset_index()

                if 4*5 >= X_k_test[vars_cat_imp_k].shape[0] :
                    n_cluster = int(np.floor(X_k_test[vars_cat_imp_k].shape[0] / 4))
                else :
                    n_cluster = 5

                imp_br_test_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(n_neighbors = n_cluster), max_iter = 10, tol = 1e-3)
                imp_br_test_cat.fit(X_k_test[vars_cat_imp_k])
                imp_X_k_test_cat = pd.DataFrame(imp_br_test_cat.transform(X_k_test[vars_cat_imp_k]), columns = vars_cat_imp_k, index = X_k_test.index)
                imp_X_k_test_cat = imp_X_k_test_cat.reset_index()
                for cat_imp in vars_cat_imp_k :
                    imp_X_k_test[cat_imp] = imp_X_k_test_cat[cat_imp]

                save_object(data = imp_X_k_test, filename = self.ds_prefix+'test_B'+str(B_loop+1)+'_k'+str(k+1), file_repo = self.save_data_repo)


                # Nested cross-validation
                vars_m_miss_k = []
                mfold_split = StratifiedShuffleSplit(n_splits = self.m_fold, random_state = seed_number + 100*k, test_size = 1/self.m_fold)

                # To create a list for stratifying by outcome and cohort
                stratify_k_array_0 = [item.split('_')[0] for item in X_k_train.index.values]
                stratify_k_array_1 = sorted(set(stratify_k_array_0))
                stratify_k_array_2 = {item: index + 1 for index, item in enumerate(stratify_k_array_1)}
                stratify_k_array = [stratify_k_array_2[item] for item in stratify_k_array_0]
                y_train_stratify_k = [int(str(int(a)) + str(int(b)) ) for a, b in zip(stratify_k_array, y_k_train)]

                # for m, (k_train_index, k_test_index) in enumerate( mfold_split.split(X_k_train, y_k_train) ):
                for m, (k_train_index, k_test_index) in enumerate( mfold_split.split(X_k_train, y_train_stratify_k) ):
                    # X_k_m_train, X_k_m_test = X_k_train.iloc[k_train_index], X_k_train.iloc[k_test_index]
                    # y_k_m_train, y_k_m_test = np.array(y_k_train)[k_train_index], np.array(y_k_train)[k_test_index]
                    X_k_m_train, X_k_m_test = X_k_train[vars_miss].iloc[k_train_index], X_k_train[vars_miss].iloc[k_test_index]
                    y_k_m_train, y_k_m_test = np.array(y_k_train)[k_train_index], np.array(y_k_train)[k_test_index]

                    # To keep the data with self.bs_outcome when missingness of self.bs_outcome > self.pct_miss
                    X_k_m_train_bs_keep, X_k_m_test_bs_keep = X_k_train_bs_keep.iloc[k_train_index], X_k_train_bs_keep.iloc[k_test_index]

                    # Variables in the training and testing set with missingness < self.pct_miss
                    vars_m_train_pct_miss = X_k_m_train.isna().sum() / X_k_m_train.shape[0]
                    vars_m_train_miss = vars_m_train_pct_miss[ vars_m_train_pct_miss < self.pct_miss ].reset_index()['index'].tolist()
                    vars_m_test_pct_miss = X_k_m_test.isna().sum() / X_k_m_test.shape[0]
                    vars_m_test_miss = vars_m_test_pct_miss[ vars_m_test_pct_miss < self.pct_miss ].reset_index()['index'].tolist()
                    vars_m_miss_ = list(set(vars_m_train_miss).intersection(vars_m_test_miss))
                    vars_m_miss = [x for x in X_k_m_train if x in vars_m_miss_]
                    vars_m_miss_k.append(vars_m_miss)

                    # Categorical variables in the training set
                    vars_cat_imp_k_m = [x for x in X_k_m_train[vars_m_miss].columns.tolist() if x in vars_cat_imp ]

                    # Missing value imputation (training data in k-fold cross-validation)
                    imp_br_m_train = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
                    imp_br_m_train.fit(X_k_m_train[vars_m_miss])
                    imp_X_k_m_train = pd.DataFrame(imp_br_m_train.transform(X_k_m_train[vars_m_miss]), columns = vars_m_miss, index = X_k_m_train.index)
                    imp_X_k_m_train[self.outcome] = y_k_m_train
                    imp_X_k_m_train = imp_X_k_m_train.reset_index()

                    imp_br_train_m_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(), max_iter = 10, tol = 1e-3)
                    imp_br_train_m_cat.fit(X_k_m_train[vars_cat_imp_k_m])
                    imp_X_k_m_train_cat = pd.DataFrame(imp_br_train_m_cat.transform(X_k_m_train[vars_cat_imp_k_m]), columns = vars_cat_imp_k_m, index = X_k_m_train.index)
                    imp_X_k_m_train_cat = imp_X_k_m_train_cat.reset_index()
                    for cat_imp_m in vars_cat_imp_k_m :
                        imp_X_k_m_train[cat_imp_m] = imp_X_k_m_train_cat[cat_imp_m]

                    save_object(data = imp_X_k_m_train, filename = self.ds_prefix+'train_B'+str(B_loop+1)+'_k'+str(k+1)+'_m'+str(m+1), file_repo = self.save_data_repo)

                    # Missing value imputation (testing data in nested cross-validation)
                    keep_index_m1 = X_k_m_test.copy()
                    keep_index_m2 = keep_index_m1.reset_index()
                    
                    if self.bs_outcome in keep_index_m2.columns.tolist() :
                        keep_index_m3 = keep_index_m2[ keep_index_m2[self.bs_outcome] != 1 ] # Remove baseline outcome == 1 (self.bs_outcome)
                        keep_index_m = keep_index_m3.index.values
                    else :
                        # To keep the data with self.bs_outcome when missingness of self.bs_outcome > self.pct_miss
                        # To ensure self.bs_outcome == 1 is excluded from the testing set
                        X_k_m_test_bs_keep_ = X_k_m_test_bs_keep.reset_index(drop = True)
                        keep_index_m3 = X_k_m_test_bs_keep_[ X_k_m_test_bs_keep_[self.bs_outcome] != 1 ] # Remove baseline outcome == 1 (self.bs_outcome)
                        keep_index_m = keep_index_m3.index.values
                    
                    X_k_m_test = X_k_m_test.iloc[ keep_index_m ]
                    y_k_m_test = y_k_m_test[ keep_index_m ]

                    imp_br_m_test = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
                    imp_br_m_test.fit(X_k_m_test[vars_m_miss])
                    imp_X_k_m_test = pd.DataFrame(imp_br_m_test.transform(X_k_m_test[vars_m_miss]), columns = vars_m_miss, index = X_k_m_test.index)
                    imp_X_k_m_test[self.outcome] = y_k_m_test
                    imp_X_k_m_test = imp_X_k_m_test.reset_index()

                    if 4*5 >= X_k_m_test[vars_cat_imp_k_m].shape[0] :
                        n_k_cluster = int(np.floor(X_k_m_test[vars_cat_imp_k_m].shape[0] / 4))
                    else :
                        n_k_cluster = 5
                    
                    imp_br_m_test_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(n_neighbors = n_k_cluster), max_iter = 10, tol = 1e-3)
                    imp_br_m_test_cat.fit(X_k_m_test[vars_cat_imp_k_m])
                    imp_X_k_m_test_cat = pd.DataFrame(imp_br_m_test_cat.transform(X_k_m_test[vars_cat_imp_k_m]), columns = vars_cat_imp_k_m, index = X_k_m_test.index)
                    imp_X_k_m_test_cat = imp_X_k_m_test_cat.reset_index()
                    for cat_imp_m in vars_cat_imp_k_m :
                        imp_X_k_m_test[cat_imp_m] = imp_X_k_m_test_cat[cat_imp_m]

                    save_object(data = imp_X_k_m_test, filename = self.ds_prefix+'test_B'+str(B_loop+1)+'_k'+str(k+1)+'_m'+str(m+1), file_repo = self.save_data_repo)
                
            
            return(vars_miss_k) # vars_m_miss_k
        

        # Training and testing data for k-fold cross-validation with B_data times
        ls_vars = Parallel(n_jobs = n_jobs)(
            delayed( func_split_n_impute )(B_loop = B_loop)
            for B_loop in range(self.B_data) )
        list_vars = []
        for loop in range(len(ls_vars)) :
            for loop2 in range(len(ls_vars[0])) :
                list_vars = list_vars + ls_vars[loop][loop2]
        
        list_vars_all = list( np.unique(list_vars) )
        list_vars_keep = [ x for x in X_test.columns.tolist() if x in list_vars_all ]
        vars_cat_imp_keep = [x for x in list_vars_keep if x in vars_cat_imp ]

        # Missing value imputation (training data)
        imp_br_train = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
        imp_br_train.fit(X_train[list_vars_keep])
        array_imp_br_train = imp_br_train.transform(X_train[list_vars_keep])
        imp_X_train = pd.DataFrame(array_imp_br_train, columns = X_train[list_vars_keep].columns.tolist(), index = X_train.index)
        imp_X_train[self.outcome] = y_train
        imp_X_train = imp_X_train.reset_index()

        imp_br_train_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(), max_iter = 10, tol = 1e-3)
        imp_br_train_cat.fit(X_train[vars_cat_imp_keep])
        imp_X_train_cat = pd.DataFrame(imp_br_train_cat.transform(X_train[vars_cat_imp_keep]), columns = vars_cat_imp_keep, index = X_train.index)
        imp_X_train_cat = imp_X_train_cat.reset_index()
        for cat_imp in vars_cat_imp_keep :
            imp_X_train[cat_imp] = imp_X_train_cat[cat_imp]

        # imp_X_train_round = func_round_variables(imp_X_train)
        save_object(data = imp_X_train, filename = self.ds_prefix+'training', file_repo = self.save_data_repo)

        imp_br_val = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
        imp_br_val.fit(X_test[list_vars_keep])
        array_imp_br_val = imp_br_val.transform(X_test[list_vars_keep])
        imp_X_test = pd.DataFrame(array_imp_br_val, columns = X_test[list_vars_keep].columns.tolist(), index = X_test.index)
        imp_X_test[self.outcome] = y_test
        imp_X_test = imp_X_test.reset_index()

        # Check if any categorical variable has only 1 level
        check_cat = pd.DataFrame( X_test[vars_cat_imp_keep].nunique() ).reset_index()
        check_cat = check_cat[ check_cat[0] == 1 ]['index'].tolist()
        vars_cat_imp_keep_val = [x for x in list_vars_keep if (x in vars_cat_imp) & (x not in check_cat) ]

        if len(check_cat) > 0:
            imp_mean = IterativeImputer(random_state=seed_number)
            imp_mean.fit(X_test[check_cat])
            imp_X_test_mean = pd.DataFrame(imp_mean.transform(X_test[check_cat]), columns = check_cat, index = X_test.index)
            imp_X_test_mean = imp_X_test_mean.reset_index()
            for cat_imp in check_cat :
                imp_X_test[cat_imp] = imp_X_test_mean[cat_imp]

        imp_br_val_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(), max_iter = 10, tol = 1e-3)
        imp_br_val_cat.fit(X_test[vars_cat_imp_keep_val])
        imp_X_test_cat = pd.DataFrame(imp_br_val_cat.transform(X_test[vars_cat_imp_keep_val]), columns = vars_cat_imp_keep_val, index = X_test.index)
        imp_X_test_cat = imp_X_test_cat.reset_index()
        for cat_imp in vars_cat_imp_keep_val :
            imp_X_test[cat_imp] = imp_X_test_cat[cat_imp]

        # imp_X_test_round = func_round_variables(imp_X_test)
        save_object(data = imp_X_test, filename = self.ds_prefix+'validate', file_repo = self.save_data_repo)




class impute_cohort:
    def __init__(self, data, data_name, outcome, vars_cat, save_data_repo, pct_miss = 0.5):
        self.data = data
        self.data_name = data_name
        self.outcome = outcome
        self.pct_miss = pct_miss
        self.vars_cat = vars_cat
        self.save_data_repo = save_data_repo

    def split_n_impute(self, seed_number = 890701):

        data = self.data

        # Check if data has > 50% missing value
        vars_excl = []
        for cols_loop in data.columns.tolist() :
            if data[cols_loop].isnull().sum() / data.shape[0] > 0.5 :
                vars_excl = vars_excl + [cols_loop]
        
        if len(vars_excl) > 0:
            data.drop(vars_excl, axis = 1, inplace = True)

        # Categorical variables to be imputed
        vars_cat_imp = [x for x in data.columns.tolist() if x in self.vars_cat ]

        # Missing value imputation (Continuous variables)
        imp_br_data = IterativeImputer(random_state = seed_number, estimator = BayesianRidge(), max_iter = 10, tol = 1e-3)
        imp_br_data.fit(data)
        array_imp_br_data = imp_br_data.transform(data)
        imp_data = pd.DataFrame(array_imp_br_data, columns = data.columns.tolist(), index = data.index)
        imp_data = imp_data.reset_index()

        # Missing value imputation (Categorical variables)
        imp_br_data_cat = IterativeImputer(random_state = seed_number, estimator = KNeighborsClassifier(), max_iter = 10, tol = 1e-3)
        imp_br_data_cat.fit(data[vars_cat_imp])
        imp_br_data_cat = pd.DataFrame(imp_br_data_cat.transform(data[vars_cat_imp]), columns = vars_cat_imp, index = data.index)
        imp_br_data_cat = imp_br_data_cat.reset_index()
        for cat_imp in vars_cat_imp :
            imp_data[cat_imp] = imp_br_data_cat[cat_imp]
        
        imp_data[self.outcome] = data[self.outcome]
        imp_data.drop(['index'], axis = 1, inplace = True)

        save_object(data = imp_data, filename = self.data_name, file_repo = self.save_data_repo)

