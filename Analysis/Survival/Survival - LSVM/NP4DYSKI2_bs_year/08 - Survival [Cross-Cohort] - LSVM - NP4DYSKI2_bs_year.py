#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                        Linear Survival Support Vector Machine (SVM) - Dyskinesias                             ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']
list_model_prefix = ['lsvm_dyski'] * 3 + ['lsvm_dyski_excl'] * 3
file_repo_prefix_loop = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/'] * 2
list_vars_ml_set = [vars_baseline_dyski] * 3 + [vars_baseline_dyski_excl] * 3

model_set = 'fastsvm'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/08 - Survival Analysis - Method Settings (Multiple Cohorts).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Single cohort)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set, model_prefix in zip(file_repo_prefix_loop, list_vars_ml_set, list_model_prefix) :
    file_repo_prefix = loop_file_repo
    print('Outcome (Multiple Cohorts): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction.py").read() )

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction - Part 2.py").read() )
    