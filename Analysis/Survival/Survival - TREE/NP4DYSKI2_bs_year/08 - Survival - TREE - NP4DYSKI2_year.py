#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                            Survival Tree - Dyskinesias                                        ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_year', 'NP4DYSKI2_status']
model_prefix = 'tree_dyski'
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/']
list_vars_ml_set = [vars_baseline, vars_baseline, vars_baseline]

# file_repo_prefix_loop = ['LUXPARK/']
# list_vars_ml_set = [vars_baseline]

model_set = 'surv_tree'

print('Outcome (Single Cohort): '+ set_outcome[0] + ' (' + model_prefix + ')')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/08 - Survival Analysis - Method Settings (Single Cohort).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Single cohort)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set in zip(file_repo_prefix_loop, list_vars_ml_set) :
    file_repo_prefix = loop_file_repo
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction.py").read() )

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction - Part 2.py").read() )