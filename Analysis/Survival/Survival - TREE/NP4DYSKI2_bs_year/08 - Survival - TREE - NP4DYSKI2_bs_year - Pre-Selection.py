#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                            Survival Tree - Dyskinesias                                        ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']
list_model_prefix = ['tree_dyski_pre1']*3 + ['tree_dyski_pre2']*3 + ['tree_dyski_pre3']*3 + ['tree_dyski_pre4']*3 +\
    ['tree_dyski_cf1']*3 + ['tree_dyski_cf2']*3
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/']*6
list_vars_ml_set = [vars_select_dyski_bs_year_1]*3 + [vars_select_dyski_bs_year_2]*3 + [vars_select_dyski_bs_year_3]*3 + [vars_select_dyski_bs_common]*3 +\
    [vars_select_dyski_bs_confounder_1]*3 + [vars_select_dyski_bs_confounder_2]*3

model_set = 'surv_tree'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/08 - Survival Analysis - Method Settings (Single Cohort).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Single cohort)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set, model_prefix in zip(file_repo_prefix_loop, list_vars_ml_set, list_model_prefix) :
    file_repo_prefix = loop_file_repo
    print('Outcome (Single Cohort): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction.py").read() )

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction - Part 2.py").read() )