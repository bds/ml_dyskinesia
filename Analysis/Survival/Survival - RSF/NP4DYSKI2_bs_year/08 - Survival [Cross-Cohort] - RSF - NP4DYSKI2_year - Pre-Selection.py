#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                      Random Survival Forest - Dyskinesias                                     ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_year', 'NP4DYSKI2_status']
list_model_prefix = ['rsf_dyski_pre1']*3 + ['rsf_dyski_pre2']*3 + ['rsf_dyski_pre3']*3 + ['rsf_dyski_pre4']*3
file_repo_prefix_loop = ['ALL/', 'LEAVE_ONE_OUT/', 'LEAVE_ONE_OUT2/']*4
list_vars_ml_set = [vars_select_dyski_year_1]*3 + [vars_select_dyski_year_2]*3 + [vars_select_dyski_year_3]*3 + [vars_select_dyski_bs_common]*3

model_set = 'rsf'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/08 - Survival Analysis - Method Settings (Multiple Cohorts).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Single cohort)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set, model_prefix in zip(file_repo_prefix_loop, list_vars_ml_set, list_model_prefix) :
    file_repo_prefix = loop_file_repo
    print('Outcome (Multiple Cohorts): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction.py").read() )

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction - Part 2.py").read() )