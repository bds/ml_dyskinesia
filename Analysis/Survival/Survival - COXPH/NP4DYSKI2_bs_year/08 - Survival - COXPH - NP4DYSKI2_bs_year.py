#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
###################################                                  Cox Proportional Hazards Model - Dyskinesias                                 ###################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Outcome setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

set_outcome = ['NP4DYSKI2_bs_year', 'NP4DYSKI2_status']
list_model_prefix = ['coxph_dyski'] * 3 + ['coxph_dyski_excl'] * 3
file_repo_prefix_loop = ['LUXPARK/', 'PPMI/', 'ICEBERG/'] * 2
list_vars_ml_set = [vars_baseline_dyski] * 3 + [vars_baseline_dyski_excl] * 3

model_set = 'coxnet'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (Single cohort) [to be updated]    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

exec( open("Analysis/08 - Survival Analysis - Method Settings (Single Cohort).py").read() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To initiate machine learning prediction (Single cohort)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

for loop_file_repo, vars_ml_set, model_prefix in zip(file_repo_prefix_loop, list_vars_ml_set, list_model_prefix) :
    file_repo_prefix = loop_file_repo
    print('Outcome Single Cohorts): '+ set_outcome[0] + ' (' + model_prefix + ')')
    print('Cohort: '+file_repo_prefix)

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction.py").read() )

    exec( open("Analysis/08 - Survival Analysis - Machine Learning Prediction - Part 2.py").read() )