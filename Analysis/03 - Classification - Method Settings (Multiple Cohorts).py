#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Method setting (to be updated)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ls_mtd_normalized = []
ls_mtd_undersamping = []
ls_mtd_addon = []
ls_mtd_feat_selection = []
ls_mtd_parallel = []
ls_mtd_name = []

# mtd_normalized = ['NONE', 'COMBAT', 'MCOMBAT', 'MEAN', 'QUANTILE', 'RATIOA', 'STANDARDIZE', 'LOGRATIO_100', 'LOGRATIO_200', 'LOGRATIO_300', 'LOGRATIO_400', 'LOGRATIO_500']
mtd_normalized = ['NONE', 'COMBAT', 'MCOMBAT', 'MEAN', 'QUANTILE', 'RATIOA', 'STANDARDIZE']
mtd_undersamping = [False, True]
# mtd_addon = ['', '_addon']
mtd_addon = ['']
# mtd_feat_selection = ['', 'rfe', 'backward', 'weight', 'boruta']
if model_set in ['figs', 'hs', 'gosdt', 'c45'] :
    mtd_feat_selection = ['', 'stepwise']
else :
    mtd_feat_selection = ['', 'rfe', 'stepwise']

mtd_influential = False

for method_normalized in mtd_normalized :
    for method_undersamping in mtd_undersamping :
        for method_addon in mtd_addon :
            for method_feat_selection in mtd_feat_selection :
                
                if ( (method_normalized == 'NONE') | (method_normalized == 'LOGRATIO_0') | (method_normalized == 'LOGRATIO_100') | (method_normalized == 'LOGRATIO_200') |\
                    (method_normalized == 'LOGRATIO_300') | (method_normalized == 'LOGRATIO_400') | (method_normalized == 'LOGRATIO_500') ) & (method_addon == '_addon')  :
                    # No addon normalization on the no normalization and log-ratio
                    pass

                elif (method_normalized == 'LOGRATIO_0') & (method_feat_selection == '') :
                    pass

                else :

                    if(method_undersamping == False):
                        mtd_undersamp = ''

                    elif(method_undersamping == True):
                        mtd_undersamp = '_undersampling'
                    else:
                        pass

                    if method_feat_selection == '' :
                        m_feat_selection = ''
                    else :
                        m_feat_selection = '_' + method_feat_selection
                    
                    if method_feat_selection in ['stepwise'] :
                        method_parallel = False
                    elif method_feat_selection in ['rfe'] :
                        method_parallel = False
                    else :
                        method_parallel = True
                    
                    ls_mtd_normalized.append( method_normalized )
                    ls_mtd_undersamping.append( method_undersamping )
                    ls_mtd_addon.append( method_addon )
                    ls_mtd_feat_selection.append( method_feat_selection )
                    ls_mtd_parallel.append( method_parallel )

                    mtd_name = method_normalized.lower() + mtd_undersamp + method_addon + m_feat_selection
                    ls_mtd_name.append( mtd_name )
                    # print('mtd_name: '+mtd_name)