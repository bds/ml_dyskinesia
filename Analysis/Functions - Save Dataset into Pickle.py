#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Save dataset into pickle    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import pickle


def save_object(data, filename, file_repo):
    try:
        with open(file_repo + filename + ".pickle", "wb") as f:
            pickle.dump(data, f, protocol = pickle.HIGHEST_PROTOCOL)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Loading the pickle    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def load_object(filename, file_repo):
    try:
        with open(file_repo + filename + ".pickle", "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)
 