#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####   Drop variables with at least 50% missingness and zero variance    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_comb(data):
  
  # # To keep the variables with missingness less than 50%
  prop_miss = data.isnull().sum() / data.shape[0]
  vars_keep = prop_miss[prop_miss < 0.5].index.values
  data = data[ list(vars_keep) ]
  
  # # To drop the variables with zero variance
  list_zero_var = pd.melt(data.agg(['nunique']))
  vars_zero_var = list(list_zero_var[list_zero_var['value'] <= 1]['variable'])
  data = data.drop(vars_zero_var, axis = 1)

  return(data)
