#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Load the packages    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from sksurv.ensemble import RandomSurvivalForest
from sksurv.ensemble import ComponentwiseGradientBoostingSurvivalAnalysis
from sksurv.ensemble import GradientBoostingSurvivalAnalysis
from sksurv.ensemble import ExtraSurvivalTrees

from sksurv.linear_model import CoxPHSurvivalAnalysis
from sksurv.linear_model import CoxnetSurvivalAnalysis

from sksurv.svm import FastSurvivalSVM
from sksurv.svm import FastKernelSurvivalSVM
from sksurv.svm import MinlipSurvivalAnalysis
from sksurv.svm import NaiveSurvivalSVM

from sksurv.tree import SurvivalTree

from sksurv.metrics import cumulative_dynamic_auc

from lifelines import KaplanMeierFitter
from lifelines.utils import concordance_index

from xgbse.converters import convert_to_structured


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - One-hot encoding    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_one_hot_encoding(ds_Xtrain, ds_Xtest, vars_encode = []) :

    if len(vars_encode) > 0 :

        list_vars_encoding = [x for x in ds_Xtrain.columns if x in vars_encode]

        for eLoop in range(len(list_vars_encoding)) :
            
            ds_Xtrain[ list_vars_encoding[eLoop] ] = ds_Xtrain[ list_vars_encoding[eLoop] ].map(int)
            ds_Xtrain = pd.get_dummies(ds_Xtrain, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])

            ds_Xtest[ list_vars_encoding[eLoop] ] = ds_Xtest[ list_vars_encoding[eLoop] ].map(int)
            ds_Xtest = pd.get_dummies(ds_Xtest, columns = [ list_vars_encoding[eLoop] ], prefix = [ list_vars_encoding[eLoop] ])
            
        # Encoded variables that are missed in ds_Xtest
        col_Xtrain = ds_Xtrain.columns.tolist()
        col_Xtest = ds_Xtest.columns.tolist()
        col_encode_add = list(set(col_Xtrain) - set(col_Xtest))

        if len(col_encode_add) > 0 :
            for col_add in col_encode_add :
                ds_Xtest[col_add] = 0
            
        else :
            pass

        ds_Xtest = ds_Xtest[col_Xtrain]
    
    return( ds_Xtrain, ds_Xtest )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Feature selection    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def func_feature_selection(ds_Xtrain, ds_Xtest, ds_y_train, ds_status_train, feature_selection = 'none', fs_estimator = DecisionTreeClassifier(), set_continuos_first = False):
  
    if( feature_selection == 'stepwise' ) :

        # Stepwise (forward and backward) feature selection
        selector_columns = surv_stepwise_cv(ds_X = ds_Xtrain, ds_y = ds_y_train, ds_status = ds_status_train, estimator = fs_estimator, cv = 1, continuos_first = set_continuos_first)

        if len(selector_columns) < 2:
            selector_columns = ds_Xtrain.columns.tolist()

        ds_Xtrain = ds_Xtrain[selector_columns]
        ds_Xtest = ds_Xtest[selector_columns]
    
    else:
        pass

    return(ds_Xtrain, ds_Xtest)


def _get_x_y_survival(dataset, col_event, col_time, val_outcome):
    if col_event is None or col_time is None:
        y = None
        x_frame = dataset
    else:
        y = np.empty(dtype=[(col_event, bool), (col_time, np.float64)], shape=dataset.shape[0])
        y[col_event] = (dataset[col_event] == val_outcome).values
        y[col_time] = dataset[col_time].values

        x_frame = dataset.drop([col_event, col_time], axis=1)

    return x_frame, y

def get_x_y(data_frame, attr_labels, pos_label=None):
    if len(attr_labels) != 2:
        raise ValueError("expected sequence of length two for attr_labels, but got %d" % len(attr_labels))
    if pos_label is None:
        raise ValueError("pos_label needs to be specified if survival=True")
    return _get_x_y_survival(data_frame, attr_labels[0], attr_labels[1], pos_label)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Function - Survival Analysis    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class ml_survival_cox:
    """
    ml_survival_cox - Survival analysis in machine learning
    Parameters
    ----------
    data_repo : str
        Repository of the processed dataset
    var_time : str
        variable of time-to-event
    var_status : str
        variable of the event status
    vars_ml : list
        List of variables to be included in the prediction
    normalize_method : str
        Normalization method
    addon_Xtest : boolean
        To define whether the Xtest is processed with addon normalization
    undersampling : boolean
        To define whether to perform the undersampling
    feature_selection : str
        Feature selection:
        ['']
    vars_encoding : list
        List of categorical variables for one-hot encoding
    surv_model_set : str
        Survival analysis model:
        ['rsf', 'gboost', 'cwgboost', 'aft_gboost', 'aft_cwgboost', 'coxph', 'coxnet', 'extra', 'fastsvm', 'kernel_svm', 'minlip', 'naive_svm', 'surv_tree']
        rsf - Random survival forest
        gboost - Gradient Boosting
        cwgboost - Component-wise Gradient Boosting
        aft_gboost - Accelerated Failure Time (AFT) Model in Gradient Boosting
        aft_cwgboost - Accelerated Failure Time (AFT) Model in Component-wise Gradient Boosting
        coxph - Cox proportional hazards model
        coxnet - Cox proportional hazards model with ridge, lasso, elastic net penalty
        extra - Extra Survival Trees
        fastsvm - Linear Survival Support Vector Machine (SVM)
        kernel_svm - Fast Kernel Survival Support Vector Machine (SVM)
        minlip - Minimal Lipschitz Survival Analysis
        naive_svm - Naive version of linear Survival Support Vector Machine
        surv_tree - Survival Tree
    max_features : int
        For regularization of the model
    regularization : int
        For regularization of the model
    BLoop : int
        Number of Bootstrapping
    k_fold : int
        k-fold cross-validation
    m_loop : int
        Number of m for m-fold nested cross-validation
    seed_number : int
        To define the seed number
    prop_train : float
        Proportion of the training set
    """

    def __init__(self
                 ,data_repo
                 ,var_time
                 ,var_status
                 ,vars_ml
                 ,normalize_method
                 ,addon_Xtest
                 ,undersampling
                 ,feature_selection
                 ,surv_model_set
                 ,vars_encoding = []
                 ,max_features = [10]
                 ,regularization = [0.0001, 0.001, 0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0]
                 ,BLoop = 1
                 ,k_fold = 5
                 ,m_loop = 3
                 ,seed_number = 890701
                 ,n_jobs = 2
                 ,pct_miss = 0.5
                 ,prop_train = 0.67
                 ,pickle_prefix = "" ) :

        self.data_repo = data_repo
        self.var_time = var_time
        self.var_status = var_status
        self.vars_ml = vars_ml
        self.normalize_method = normalize_method
        self.addon_Xtest = addon_Xtest
        self.undersampling = undersampling
        self.feature_selection = feature_selection
        self.surv_model_set = surv_model_set
        self.vars_encoding = vars_encoding
        self.max_features = max_features
        self.regularization = regularization
        self.BLoop = BLoop
        self.k_fold = k_fold
        self.m_loop = m_loop
        self.seed_number = seed_number
        self.n_jobs = n_jobs
        self.pct_miss = pct_miss
        self.prop_train = prop_train
        self.pickle_prefix = pickle_prefix


    def measure(self) :
        """
        Fits the survival model
        Returns
        -------

        """

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read Validation and Training and Testing set    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # ----------------------------------
        # |      Training     | Validation |  <- repeated B times on Training
        # ----------------------------------
        # |       k-fold      |
        # ---------------------

        ds_validate = load_object(filename = self.pickle_prefix+'validate'+self.addon_Xtest, file_repo = self.data_repo)
        ds_training = load_object(filename = self.pickle_prefix+'training', file_repo = self.data_repo)

        vars_ml_ana = [x for x in self.vars_ml if x in ds_training.columns.tolist() ]

        # Undersampling in the training set
        if self.undersampling == True :
            ds_training = underSamplingBinary(data = ds_training, var_outcome = self.var_status, targetBalance = 0.5)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Validation and Training set    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # Validation
        X_validate, y_validate = get_x_y(data_frame = ds_validate[vars_ml_ana + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )
        time_validate, status_validate = ds_validate[self.var_time], ds_validate[self.var_status]
        
        # Training
        X_training, y_training = get_x_y(data_frame = ds_training[vars_ml_ana + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )
        time_training, status_training = ds_training[self.var_time], ds_training[self.var_status]
        

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Regularization    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if ( self.surv_model_set in ['coxph', 'coxnet', 'cwgboost'] ) & (len(self.max_features) > 1) :
            self.max_features = self.regularization
        elif (self.surv_model_set in ['fastsvm', 'naive_svm']) :
            self.max_features = self.max_features[0:4]
        else :
            pass


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To train the survival model repeated BLoop times    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # To repeat 'BLoop' times
        def func_cv_loop(BLoop) :

            kfold_cIndex_m = []
            kfold_features_m = []
            kfold_perm_imp_m = []
            kfold_opt_hp_m =[]
            kfold_candidates = []
            kfold_cIndex = []

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To read the training and testing data in k-fold cross-validation    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            for kLoop in range(self.k_fold) :
                # print('kLoop : '+str(kLoop))

                # Training set
                ds_train = load_object(filename = self.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1), file_repo = self.data_repo)

                # Testing set
                ds_test = load_object(filename = self.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+self.addon_Xtest, file_repo = self.data_repo)

                vars_ml_ana = [x for x in self.vars_ml if x in ds_train.columns.tolist() ]
                vars_ml_ana = [x for x in vars_ml_ana if x in ds_test.columns.tolist()]

                # Undersampling in the training set
                if self.undersampling == True :
                    ds_train = underSamplingBinary(data = ds_train, var_outcome = self.var_status, targetBalance = 0.5)
                else :
                    pass


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Training and testing set    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                X_train, y_train = get_x_y(data_frame = ds_train[vars_ml_ana + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )
                X_test, y_test = get_x_y(data_frame = ds_test[vars_ml_ana + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )

                time_train, status_train = ds_train[self.var_time], ds_train[self.var_status]
                time_test, status_test = ds_test[self.var_time], ds_test[self.var_status]

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    One-hot encoding    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                vars_encoding = [x for x in self.vars_encoding if x in X_train.columns.tolist() ]

                X_train, X_test = func_one_hot_encoding(ds_Xtrain = X_train, ds_Xtest = X_test, vars_encode = vars_encoding)


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Load the data for nested cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                ls_X_train_m = dict()
                ls_X_test_m = dict()
                ls_y_train_m = dict()
                ls_y_test_m = dict()

                ls_time_train_m = dict()
                ls_time_test_m = dict()
                ls_status_train_m = dict()
                ls_status_test_m = dict()
                for mLoop in range(self.m_loop) :
                    # print('mLoop : '+str(mLoop))
                    # Load the train and test set for the nested cross-validation
                    train_m = load_object(filename = self.pickle_prefix+'train_B'+str(BLoop+1)+'_k'+str(kLoop+1)+'_m'+str(mLoop+1), file_repo = self.data_repo)
                    test_m = load_object(filename = self.pickle_prefix+'test_B'+str(BLoop+1)+'_k'+str(kLoop+1)+'_m'+str(mLoop+1), file_repo = self.data_repo)

                    # Undersampling in the training set
                    if self.undersampling == True :
                        train_m = underSamplingBinary(data = train_m, var_outcome = self.var_status, targetBalance = 0.5)
                    else :
                        pass

                    # List of variables from the features
                    # vars_train_test = np.unique(train_m.columns.tolist() + test_m.columns.tolist()).tolist()
                    vars_train_test = list( set(train_m.columns.tolist()) & set(test_m.columns.tolist()) )
                    vars_ml_ana_m = [x for x in self.vars_ml if x in vars_train_test ]

                    # Training and testing set
                    X_train_m, y_train_m = get_x_y(data_frame = train_m[vars_ml_ana_m + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )
                    X_test_m, y_test_m = get_x_y(data_frame = test_m[vars_ml_ana_m + [self.var_status, self.var_time]], attr_labels = [self.var_status, self.var_time], pos_label = 1 )

                    time_train_m, status_train_m = train_m[self.var_time], train_m[self.var_status]
                    time_test_m, status_test_m = test_m[self.var_time], test_m[self.var_status]

                    # One-hot encoding
                    X_train_m, X_test_m = func_one_hot_encoding(ds_Xtrain = X_train_m, ds_Xtest = X_test_m, vars_encode = self.vars_encoding)

                    ls_X_train_m[mLoop] = X_train_m
                    ls_X_test_m[mLoop] = X_test_m
                    ls_y_train_m[mLoop] = y_train_m
                    ls_y_test_m[mLoop] = y_test_m

                    ls_time_train_m[mLoop] = time_train_m
                    ls_time_test_m[mLoop] = time_test_m
                    ls_status_train_m[mLoop] = status_train_m
                    ls_status_test_m[mLoop] = status_test_m
                
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Hyperparameters for each nested cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ls_hp_m = []
                for mLoop in range(self.m_loop) :
                    for hp in self.max_features :
                        ls_hp_m.append( [mLoop, hp] )


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    Function - To fit the model with hyperparameters (k-fold)    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                if self.surv_model_set == 'rsf' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = RandomSurvivalForest(max_depth = hp, n_estimators = 20, n_jobs = 2, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Random Survival Forest
                        surv_model = RandomSurvivalForest(max_depth = hp, n_estimators = 20, n_jobs = 2, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = RandomSurvivalForest(max_depth = opt_hp_m, n_estimators = 20, n_jobs = 2, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'gboost' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = GradientBoostingSurvivalAnalysis(max_depth = hp, n_estimators = 20, learning_rate=1.0, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Gradient Boosting
                        surv_model = GradientBoostingSurvivalAnalysis(max_depth = hp, n_estimators = 20, learning_rate=1.0, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = GradientBoostingSurvivalAnalysis(max_depth = opt_hp_m, n_estimators = 20, learning_rate=1.0, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'cwgboost' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Component-wise Gradient Boosting
                        surv_model = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=hp, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=opt_hp_m, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'coxph' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = CoxPHSurvivalAnalysis(alpha = hp)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Cox proportional hazards model
                        surv_model = CoxPHSurvivalAnalysis(alpha = hp)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = CoxPHSurvivalAnalysis(alpha = opt_hp_m)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'coxnet' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        try :
                            fs_estimator_set = CoxnetSurvivalAnalysis(l1_ratio = hp, alpha_min_ratio=0.01)
                            X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                        ,ds_Xtest = ls_X_test_m[m]
                                                                        ,ds_y_train = ls_y_train_m[m]
                                                                        ,ds_status_train = ls_status_train_m[m]
                                                                        ,feature_selection = self.feature_selection
                                                                        ,fs_estimator = fs_estimator_set )
                            # To fit Cox proportional hazards model with ridge, lasso, elastic net penalty
                            surv_model = CoxnetSurvivalAnalysis(l1_ratio = hp, alpha_min_ratio=0.01, fit_baseline_model = True)
                            surv_model.fit(X_train_m2, ls_y_train_m[m])
                            cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                            # list of features
                            perm = abs( permutation_importance(surv_model
                                        ,X_test_m2
                                        ,ls_y_test_m[m]
                                        ,random_state = self.seed_number ).importances_mean )
                            select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                            perm_imp_m = [x for x in perm if x != 0]
                        except Exception :
                            cIndex = 0
                            select_features_m = ['']
                            perm_imp_m = [0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = CoxnetSurvivalAnalysis(l1_ratio = opt_hp_m, alpha_min_ratio=0.01, fit_baseline_model = True)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)

                
                elif self.surv_model_set == 'extra' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = ExtraSurvivalTrees(max_depth = hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Extra Survival Trees
                        surv_model = ExtraSurvivalTrees(max_depth = hp, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 1)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = ExtraSurvivalTrees(max_depth = opt_hp_m, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'fastsvm' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = FastSurvivalSVM(alpha = hp, max_iter = 20, tol = 1e-4, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Random Survival Forest
                        surv_model = FastSurvivalSVM(alpha = hp, max_iter = 20, tol = 1e-4, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = len(ls_hp_m))(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = FastSurvivalSVM(alpha = opt_hp_m, max_iter = 20, tol = 1e-4, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'naive_svm' :

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]
                        fs_estimator_set = NaiveSurvivalSVM(alpha = hp, random_state = self.seed_number, tol=0.001, penalty='l1', max_iter=20)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Random Survival Forest
                        surv_model = NaiveSurvivalSVM(alpha = hp, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = len(ls_hp_m))(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = NaiveSurvivalSVM(alpha = opt_hp_m, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)

                elif self.surv_model_set == 'surv_tree':

                    def func_model_max_features(n_feats) :
                        m = ls_hp_m[n_feats][0]
                        hp = ls_hp_m[n_feats][1]

                        fs_estimator_set = SurvivalTree(max_depth = hp, random_state = self.seed_number)
                        X_train_m2, X_test_m2 = func_feature_selection(ds_Xtrain = ls_X_train_m[m]
                                                                    ,ds_Xtest = ls_X_test_m[m]
                                                                    ,ds_y_train = ls_y_train_m[m]
                                                                    ,ds_status_train = ls_status_train_m[m]
                                                                    ,feature_selection = self.feature_selection
                                                                    ,fs_estimator = fs_estimator_set )
                        # To fit Random Survival Forest
                        surv_model = SurvivalTree(max_depth = hp, random_state = self.seed_number)
                        surv_model.fit(X_train_m2, ls_y_train_m[m])
                        cIndex = surv_model.score(X_test_m2, ls_y_test_m[m])

                        # list of features
                        perm = abs( permutation_importance(surv_model
                                    ,X_test_m2
                                    ,ls_y_test_m[m]
                                    ,random_state = self.seed_number ).importances_mean )
                        select_features_m = [X_test_m2.columns[x] for x in range(len(perm)) if perm[x] != 0]
                        perm_imp_m = [x for x in perm if x != 0]

                        return({'cIndex': cIndex, 'select_features_m': select_features_m, 'perm_imp_m': perm_imp_m})
                    
                    cIndex_max_features = Parallel(n_jobs = 3)(
                            delayed( func_model_max_features )(n_feats = n_feats)
                            for n_feats in range(len(ls_hp_m)) )
                    
                    # Average score of each hyperparameter
                    ls_cIndex_m = {}
                    ls_features_m = {}
                    ls_perm_imp_m = {}
                    for Lhp, LcIndex in zip(ls_hp_m, cIndex_max_features):
                        Lkey = Lhp[1]
                        if Lkey not in ls_cIndex_m:
                            ls_cIndex_m[Lkey] = []
                            ls_features_m[Lkey] = []
                            ls_perm_imp_m[Lkey] = []
                        ls_cIndex_m[Lkey].append(LcIndex['cIndex'])
                        ls_features_m[Lkey].append(LcIndex['select_features_m'])
                        ls_perm_imp_m[Lkey].append(LcIndex['perm_imp_m'])
                    
                    ls_avg_cIndex_m = []
                    for avg_m in list(ls_cIndex_m.keys()) :
                        ls_avg_cIndex_m.append( mean(ls_cIndex_m[avg_m]) )
                    
                    # Optimal hyperparameter will be applied into X_train, X_test, y_train, y_test
                    opt_hp_m = opt_hp_m = self.max_features[ls_avg_cIndex_m.index(max(ls_avg_cIndex_m))]
                    candidate_features = list(set(item for sublist in ls_features_m[opt_hp_m] for item in sublist))
                    candidate_features = [x for x in candidate_features if x in X_train.columns.tolist()]
                    candidate_features = [x for x in candidate_features if x in X_test.columns.tolist()]

                    if len(candidate_features) == 0:
                        candidate_features_tmp = [x for x in X_train.columns.tolist()]
                        candidate_features_tmp = [x for x in candidate_features_tmp if x in X_test.columns.tolist()]
                    else :
                        candidate_features_tmp = candidate_features

                    surv_model = SurvivalTree(max_depth = opt_hp_m, random_state = self.seed_number)
                    surv_model.fit(X_train[candidate_features_tmp], y_train)
                    cIndex = surv_model.score(X_test[candidate_features_tmp], y_test)
                
                elif self.surv_model_set == 'median_time':

                    # Calculate the median survival time from the training set
                    median_survival_time = np.median(time_train)

                    # Assign predictions for the testing set
                    trivial_predictions = np.where(time_test >= median_survival_time, 1, 0)

                    # Measure the concordance index
                    c_index = concordance_index(np.array(time_test), trivial_predictions, status_test)

                    cIndex = []
                    for n_feats in self.max_features :
                        cIndex.append( c_index )

                else :
                    pass


                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                ####    C-index from k-fold cross-validation    ####
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                # kfold_cIndex.append( cIndex )
                kfold_cIndex_m.append( ls_cIndex_m )
                kfold_features_m.append( ls_features_m )
                kfold_perm_imp_m.append( ls_perm_imp_m )
                kfold_opt_hp_m.append( opt_hp_m )
                kfold_candidates.append( candidate_features )
                kfold_cIndex.append( cIndex )
            

            return({'kfold_cIndex_m': kfold_cIndex_m
                    ,'kfold_features_m': kfold_features_m
                    ,'kfold_perm_imp_m': kfold_perm_imp_m
                    ,'kfold_opt_hp_m': kfold_opt_hp_m
                    ,'kfold_candidates': kfold_candidates
                    ,'kfold_cIndex': kfold_cIndex
                    })
        

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Run the model in BLoop    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        
        kfold_cIndex_BLoop = Parallel(n_jobs = 1)(
            delayed( func_cv_loop )(BLoop = BLoop)
            for BLoop in range(self.BLoop) )
        
        cIndex_overall_list = []
        opt_hp_list = []
        candidates_list = []
        for cIndex_overall_loop in range(len(kfold_cIndex_BLoop)) :
            cIndex_overall_list += kfold_cIndex_BLoop[cIndex_overall_loop]['kfold_cIndex']
            opt_hp_list += kfold_cIndex_BLoop[cIndex_overall_loop]['kfold_opt_hp_m']
            candidates_list += kfold_cIndex_BLoop[cIndex_overall_loop]['kfold_candidates']

        # The optimal hyperparameter with highest AUC score at repeated cross-validation
        best_model_cIndex_cv = max(cIndex_overall_list)
        best_model_cIndex_cv_idx = cIndex_overall_list.index(best_model_cIndex_cv)
        best_max_depth = opt_hp_list[best_model_cIndex_cv_idx]
        best_candidates = candidates_list[best_model_cIndex_cv_idx]
        


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Retrain the model with the max_depth with highest average c-index    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    One-hot encoding    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        
        vars_encoding = [x for x in self.vars_encoding if x in X_training.columns.tolist() ]
        X_training, X_validate = func_one_hot_encoding(ds_Xtrain = X_training, ds_Xtest = X_validate, vars_encode = vars_encoding)

        if len(best_candidates) > 0 :
            best_candidates = [x for x in best_candidates if x in X_training.columns.tolist()]
            best_candidates = [x for x in best_candidates if x in X_validate.columns.tolist()]
            X_training = X_training[best_candidates]
            X_validate = X_validate[best_candidates]
        else :
            pass

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Retrain the model    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        if self.surv_model_set == 'rsf' :

            # To fit Random Survival Forest
            best_model = RandomSurvivalForest(max_depth = best_max_depth, n_estimators = 20, n_jobs = 2, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)
            preds = best_model.predict(X_validate)
        
        elif self.surv_model_set == 'gboost' :

            # To fit Gradient Boosting
            best_model = GradientBoostingSurvivalAnalysis(max_depth = best_max_depth, n_estimators = 20, learning_rate=1.0, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
        
        elif self.surv_model_set == 'cwgboost' :

            # To fit Component-wise Gradient Boosting
            best_model = ComponentwiseGradientBoostingSurvivalAnalysis(n_estimators = 20, learning_rate=best_max_depth, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
        
        elif self.surv_model_set == 'aft_gboost' :

            # To fit Accelerated Failure Time (AFT) Model in Gradient Boosting
            best_model = GradientBoostingSurvivalAnalysis(max_depth = best_max_depth, n_estimators = 20, learning_rate=1.0, random_state = self.seed_number, loss="ipcwls")
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            
        elif self.surv_model_set == 'coxph' :

            # To fit Cox proportional hazards model
            best_model = CoxPHSurvivalAnalysis(alpha = best_max_depth)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            # best_model_cIndex_val = concordance_index_censored(status_validate == 1, time_validate, preds)[0]

        elif self.surv_model_set == 'coxnet' :

            # To fit Cox proportional hazards model with ridge, lasso, elastic net penalty
            best_model = CoxnetSurvivalAnalysis(l1_ratio = best_max_depth, alpha_min_ratio=0.01, fit_baseline_model = True)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            # best_model_cIndex_val = concordance_index_censored(status_validate == 1, time_validate, preds)[0]
        
        elif self.surv_model_set == 'extra' :

            # To fit Extra Survival Trees
            best_model = ExtraSurvivalTrees(max_depth = best_max_depth, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            # best_model_cIndex_val = concordance_index_censored(status_validate == 1, time_validate, preds)[0]
        
        elif self.surv_model_set == 'fastsvm' :

            # To fit Linear Survival Support Vector Machine (SVM)
            best_model = FastSurvivalSVM(alpha = best_max_depth, max_iter = 20, tol = 1e-4, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
        
        elif self.surv_model_set == 'naive_svm' :

            # To fit Naive version of linear Survival Support Vector Machine (SVM)
            best_model = NaiveSurvivalSVM(alpha = best_max_depth, random_state = self.seed_number, tol=0.001, penalty='l1', max_iter=20)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            # best_model_cIndex_val = concordance_index_censored(status_validate == 1, time_validate, preds)[0]
        
        elif self.surv_model_set == 'surv_tree' :
            
            # To fit Survival Tree
            best_model = SurvivalTree(max_depth = best_max_depth, random_state = self.seed_number)
            best_model.fit(X_training, y_training)
            best_model_cIndex_val = best_model.score(X_validate, y_validate)

            preds = best_model.predict(X_validate)
            # best_model_cIndex_val = concordance_index_censored(status_validate == 1, time_validate, preds)[0]
        
        elif self.surv_model_set == 'median_time':
            
            # # Calculate the median survival time from the training set
            median_survival_time = np.median(time_training)

            # # Assign predictions for the testing set
            preds = np.where(time_validate >= median_survival_time, 1, 0)

            # # Measure the concordance index
            best_model_cIndex_val = concordance_index(np.array(time_validate), preds, status_validate)

            best_model = []

        else :
            pass

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Return best model information    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        self.best_model = best_model                                  # The final model trained on the entire training set
        self.best_model_cIndex_val = best_model_cIndex_val            # Hold-out C-index
        self.best_model_cIndex_test = mean(cIndex_overall_list)       # Average C-index in the repeated cross-validation
        self.best_model_max_features = best_max_depth                 # Optimal hyperparameter used to train the final model
        self.best_model_features = best_candidates                    # Candidate features used to train the final model
        self.best_model_predictions = preds                           # Predicted risk based on hold-out test set

        self.all_model_cIndex_test = cIndex_overall_list               # List C-index on repeated cross-validation
        self.all_model_cIndex_cv = kfold_cIndex_BLoop                  # List of C-index, hyperparameter and candidate features in nested CV

        # self.features_importance = df_features_importance

        self.X_validate = X_validate
        self.y_validate = y_validate
        self.time_validate = time_validate
        self.status_validate = status_validate

        self.X_training = X_training
        self.y_training = y_training
        self.time_training = time_training
        self.status_training = status_training

        self.ds_validate = ds_validate
        self.ds_training = ds_training

        return({ 'best_model': best_model
                ,'best_model_cIndex_val': best_model_cIndex_val
                ,'best_model_cIndex_test': best_model_cIndex_cv
                ,'best_model_max_features': best_max_depth
                ,'best_model_features': best_candidates

                ,'all_model_cIndex_test': cIndex_overall_list
                ,'all_model_cIndex_cv': kfold_cIndex_BLoop
                })

    def performance_metric(self) :
        """
        Performance metric of the best model
        Parameters
        ----------
        """
        print("performance_metric")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    SHAP values for survival model    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def shap_values_survival(model, fig_repo, fig_name, fig_features, max_vars = 15, fig_size = [12, 5], max_evals_shap = None) :
    """
    SHAP values of the survival model
    Parameters
    ----------
    fig_repo : Repository of the figure
    fig_name : Filename of the figure
    fig_features : DataFrame of feature names
    max_vars : Maximum number of features to be shown in the SHAP values plot
    fig_size : Figure size
    ----------
    Returns
    shap_values : SHAP values of the survival model
    feature_shap_values : Feature names
    feature_importance : Feature importance based on SHAP values

    fig_beeswarm : Beeswarm plot for SHAP values
    """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Validation data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    X_validate = pd.concat([model.X_validate, model.X_training], axis = 0)
    # y_validate = pd.concat([model.y_validate, model.y_training], axis = 0)
    # model.time_validate
    # model.status_validate


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    SHAP values of the model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Fits the explainer
    explainer = shap.Explainer(model.best_model.predict, X_validate)

    # Calculate the SHAP values - It takes some times
    if max_evals_shap == None :
        max_evals = X_validate.shape[1] * 2 + 2500
    else :
        max_evals = max_evals_shap

    shap_values = explainer(X_validate, max_evals = max_evals)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Feature names    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    ls_fig_features = pd.merge( pd.DataFrame({'Variable': shap_values.feature_names}), fig_features, how = 'left', on = ['Variable'] )['Description'].tolist()
    shap_values.feature_names = ls_fig_features

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Feature importance (SHAP values)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vals = np.abs(shap_values.values).mean(0)
    feature_importance = pd.DataFrame(list(zip(X_validate.columns.tolist(), shap_values.feature_names, vals)), columns = ['col_name', 'variable', 'feature_importance_vals'])
    feature_importance = feature_importance[feature_importance['feature_importance_vals'] != 0]
    feature_importance.sort_values(by = ['feature_importance_vals'], ascending = False, inplace = True)
    feature_importance = feature_importance.reset_index().drop(['index'], axis = 1)

    # idx_shap = [idx for idx in range(len(vals)) if vals[idx] > 0]
    sorted_idx_shap = [i for i in np.argsort(vals)[::-1]]
    idx_shap = [x for x in sorted_idx_shap if vals[x] > 0][0:max_vars]

    shap_values.values = shap_values.values[:, idx_shap]
    shap_values.data = shap_values.data[:, idx_shap]
    shap_values.feature_names = [shap_values.feature_names[x] for x in idx_shap]

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Beeswarm plot of SHAP values    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Maximum number of features to be shown
    num_vars = min(max_vars, feature_importance.shape[0])

    # Adjusting the 'shap_x_label' label
    if (num_vars > 10) & (num_vars <= 15) :
        adjust_text_y = -1.8 + (15-num_vars)*0.05
    elif (num_vars >3) & (num_vars <=10) :
        adjust_text_y = -1.8 + (15-num_vars)*0.1
    elif num_vars == 3 :
        adjust_text_y = -0.5
    elif num_vars == 2 :
        adjust_text_y = -0.4
    elif num_vars == 1 :
        adjust_text_y = -0.25
    else :
        adjust_text_y = -0.75
    
    # SHAP values plot
    plt.figure()
    plt.style.context("seaborn-white")
    plt.axes().set_facecolor("#F8F8F7")
    fig_beeswarm = shap.plots.beeswarm(shap_values, max_display = num_vars, plot_size = fig_size, show = False)
    plt.annotate(shap_x_label[0]
                ,xy = (plt.xlim()[0], -1)
                ,xytext = (plt.xlim()[0], plt.ylim()[0]+adjust_text_y)
                ,fontsize = 10
                ,ha = 'center'
                ,va = 'bottom')
    plt.annotate(shap_x_label[1]
                ,xy = (plt.xlim()[1], -1)
                ,xytext = (plt.xlim()[1], plt.ylim()[0]+adjust_text_y)
                ,fontsize = 10
                ,ha = 'center'
                ,va = 'bottom')
    plt.savefig(fig_repo+'shap_beeswarm_'+fig_name+'.png', bbox_inches='tight', dpi=1500) # dpi=300
    plt.clf()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return model information    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    return({'shap_values' : shap_values
            ,'feature_shap_values' : X_validate.columns.tolist()
            ,'feature_importance' : feature_importance })
