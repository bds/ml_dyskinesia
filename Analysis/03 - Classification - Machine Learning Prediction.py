#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Categorical variables to be encoded    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

vars_category = data_variables[data_variables['Variable Type'].isin(['Ordinal', 'Nominal'])]['PPMI Variable']
vars_category = [ x for x in vars_category if x in data_info[data_info['Type'] == 'Baseline']['Variable'].tolist() ]

# Not to perform one-hot encoding in CatBoost
if model_set == 'catboost':
    vars_category = []

# Set the max_features
ls_mtd_max_features_ = [1, 2, 3, 4, 5, 10] #, 15, 20
if len(vars_ml_set) <= 20 :
    ls_mtd_max_features_ = [x for x in ls_mtd_max_features_ if len(vars_ml_set) >= x]
else :
    pass


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To perform the model prediction    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# Function - Machine Learning Classification
def func_ml_model_class(i) :
    
    mtd_name = ls_mtd_name[i]
    print('Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name)

    if mtd_name != 'stability' :
        B_loop_set = 1
        pickle_prefix_set = ""
        ls_mtd_max_features = ls_mtd_max_features_
    else :
        B_loop_set = B_loop_set_
        pickle_prefix_set = pickle_prefix_set_
        ls_mtd_max_features = ls_mtd_max_features_cv

    model_class = dict()

    time_start = datetime.now()
    ml_model_class = ml_classification( data_repo = file_data+file_repo_prefix+set_outcome+'/'+ls_mtd_normalized[i]+'/'
                                        ,vars_ml = vars_ml_set
                                        ,outcome = set_outcome
                                        ,normalize_method = ls_mtd_normalized[i]
                                        ,n_jobs = 1 # n_jobs
                                        ,B_loop = B_loop_set
                                        ,parallel_params = False # ls_mtd_parallel[i]
                                        ,ml_model_set = model_set
                                        ,max_features = ls_mtd_max_features
                                        ,addon_Xtest = ls_mtd_addon[i]
                                        ,undersampling = ls_mtd_undersamping[i]
                                        ,feature_selection = ls_mtd_feat_selection[i]
                                        ,vars_encoding = vars_category
                                        ,influential = mtd_influential
                                        ,pickle_prefix = pickle_prefix_set )
    ml_model_class.classifier()
    model_class[mtd_name] = ml_model_class

    # save_object(data = ml_model_class, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    time_end = datetime.now()

    print('[Done] Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name + " Total running time: " + str(time_end - time_start))
    print('[Done] Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name + " Current time: " + str(time_end))

    return(model_class)


# Index of NOT STEPWISE
idx_joblib = [ii for ii in range(len(ls_mtd_feat_selection)) if ls_mtd_feat_selection[ii] not in ['stepwise', 'rfe']]
idx_forloop = [ii for ii in range(len(ls_mtd_feat_selection)) if ls_mtd_feat_selection[ii] in ['stepwise']]
idx_forloop2 = [ii for ii in range(len(ls_mtd_feat_selection)) if ls_mtd_feat_selection[ii] in ['rfe']]

idx_all = idx_joblib + idx_forloop + idx_forloop2


run_joblib = True


#~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Running All    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~#

if len(idx_all) > 0 :
    # To Perform Machine Learning Classification (ALL)
    time_start = datetime.now()

    if run_joblib == True :
        n_jobs_classifier = min([n_jobs, len(idx_all)])
        print('n_jobs_classifier: '+str(n_jobs_classifier))

        model_classifier = Parallel(n_jobs = n_jobs_classifier, backend="multiprocessing")(
            delayed( func_ml_model_class )(i = i)
            for i in idx_all )
    else :
        model_classifier = []
        for i in idx_all :
            ml_classifier_tmp = func_ml_model_class(i = i)
            model_classifier.append( ml_classifier_tmp )
            mtd_name = ls_mtd_name[i]
            save_object(data = ml_classifier_tmp, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )

    time_end = datetime.now()
    print("Total running time (ALL): " + str(time_end - time_start))

    # To save the individual separately
    for idx in range(len(model_classifier)) :
        mtd_name = list(model_classifier[idx].keys())[0]
        save_model = model_classifier[idx][mtd_name]
        save_object(data = save_model, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    del model_classifier


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Excluding Stepwise & RFE    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

if len(idx_joblib) > 0 :
    # To Perform Machine Learning Classification (NOT STEPWISE & RFE)
    time_start = datetime.now()

    if run_joblib == True :
        n_jobs_classifier = min([n_jobs, len(idx_joblib)])
        print('n_jobs_classifier: '+str(n_jobs_classifier))

        model_classifier = Parallel(n_jobs = n_jobs_classifier, backend="multiprocessing")(
            delayed( func_ml_model_class )(i = i)
            for i in idx_joblib )
    else :
        model_classifier = []
        for i in idx_joblib :
            ml_classifier_tmp = func_ml_model_class(i = i)
            model_classifier.append( ml_classifier_tmp )
            mtd_name = ls_mtd_name[i]
            save_object(data = ml_classifier_tmp, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )

    time_end = datetime.now()
    print("Total running time (NOT STEPWISE & RFE): " + str(time_end - time_start))

    # To save the individual separately
    for idx in range(len(model_classifier)) :
        mtd_name = list(model_classifier[idx].keys())[0]
        save_model = model_classifier[idx][mtd_name]
        save_object(data = save_model, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    del model_classifier


#~~~~~~~~~~~~~~~~~~~~~~#
####    Stepwise    ####
#~~~~~~~~~~~~~~~~~~~~~~#

# To Perform Machine Learning Classification (STEPWISE)
if len(idx_forloop) > 0 :
    time_start = datetime.now()
    print('STEPWISE')

    if run_joblib == True :
        n_jobs_classifier = min([n_jobs, len(idx_forloop)])
        print('n_jobs_classifier: '+str(n_jobs_classifier))

        model_classifier = Parallel(n_jobs = n_jobs_classifier, backend="multiprocessing")(
            delayed( func_ml_model_class )(i = i)
            for i in idx_forloop )
    else :
        model_classifier = []
        for i in idx_forloop :
            ml_classifier_tmp = func_ml_model_class(i = i)
            model_classifier.append( ml_classifier_tmp )
            mtd_name = ls_mtd_name[i]
            save_object(data = ml_classifier_tmp, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    
    time_end = datetime.now()
    print("Total running time (STEPWISE): " + str(time_end - time_start))

    # To save the individual separately
    for idx in range(len(model_classifier)) :
        mtd_name = list(model_classifier[idx].keys())[0]
        save_model = model_classifier[idx][mtd_name]
        save_object(data = save_model, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    del model_classifier


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Recursive Feature Elimination (RFE)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# To Perform Machine Learning Classification (RFE)
if len(idx_forloop2) > 0 :
    time_start = datetime.now()
    print('Recursive Feature Elimination (RFE)')
    
    if run_joblib == True :
        n_jobs_classifier = min([n_jobs, len(idx_forloop2)])
        print('n_jobs_classifier: '+str(n_jobs_classifier))

        model_classifier = Parallel(n_jobs = n_jobs_classifier, backend="multiprocessing")(
            delayed( func_ml_model_class )(i = i)
            for i in idx_forloop2 )
    else :
        model_classifier = []
        for i in idx_forloop2 :
            ml_classifier_tmp = func_ml_model_class(i = i)
            model_classifier.append( ml_classifier_tmp )
            mtd_name = ls_mtd_name[i]
            save_object(data = ml_classifier_tmp, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    
    time_end = datetime.now()
    print("Total running time (RFE): " + str(time_end - time_start))

    # To save the individual separately
    for idx in range(len(model_classifier)) :
        mtd_name = list(model_classifier[idx].keys())[0]
        save_model = model_classifier[idx][mtd_name]
        save_object(data = save_model, filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/' )
    del model_classifier


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Combining models    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

model_classifier = []
# To combine the models
for mtd_name in ls_mtd_name :

    ml_classifier = dict()
    print('Save Cohort: '+file_repo_prefix+' ; Method: ' + mtd_name)
    ml_model_class = load_object(filename = "model_"+model_prefix+'_'+mtd_name, file_repo = file_pickle+file_repo_prefix+set_outcome+'/temp/')

    if isinstance(ml_model_class, dict) :
        ml_model_class = ml_model_class[list(ml_model_class.keys())[0]]

    ml_classifier[mtd_name] = ml_model_class
    model_classifier.append( ml_classifier )

save_object(data = model_classifier, filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
model_classifier = load_object(filename = "model_"+model_prefix, file_repo = file_pickle+file_repo_prefix+set_outcome+'/' )
