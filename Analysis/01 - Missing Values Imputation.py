#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Missing Values Imputation (MICE)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def miss_imputation(data):

    global data_variables, data_info

    train_mice = data.copy(deep = True)

    # variables with missing values
    miss_var = data.isnull().sum().reset_index()\
                  .rename(columns = {0: "miss_sum"})
    miss_var = miss_var[miss_var['miss_sum'] > 0]

    # To impute only variable in 'Info'
    miss_var_info = miss_var[ miss_var['index'].isin( list(data_info['Variable']) ) ]

    # MICE imputation
    mice_imputer = IterativeImputer()

    for x in list(miss_var_info['index']):
      train_mice[x] = mice_imputer.fit_transform(train_mice[[x]])
    
    return(train_mice)