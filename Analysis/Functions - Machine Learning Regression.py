#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                            Machine Learning Regression                                            ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ data_repo: repository of the processed dataset
# @ data_prefix: to define the prefix of the data name
# @ outcome: outcome variable
# @ normalize_method: normalization method
# @ n_jobs: number of job for parallel computing (default: n_jobs = 2)
# @ B_loop: number of loop for Monte Carlo cross-validation
# @ k_loop: number of k for k-fold cross-validation
# @ ml_model_set: machine learning model, ['figs', 'catboost', 'hs', 'tpot', 'greedy', 'xgboost', 'tao', 'adaboost']
# @ max_features: maximum features to be included in the model
# @ feature_selection: to perform feature selection before the prediction, [None, "boruta_cat", "boruta_xgb", 'gboost']
# @ feature_selected: to select important features or important + tentative feature (boruta_cat, boruta_xgb), [None, 'important', 'both']
# @ addon_Xtest: to define whether the Xtest is processed with addon normalization
# @ undersampling: to define whether to perform the undersampling
# @ seed_number: to define the seed number for classifier
# @ vars_excl: variables to be excluded for the prediction, e.g. patient ID

def regression_ml( data_repo
                  ,outcome
                  ,prefix_out
                  ,normalize_method
                  ,data_prefix = ""
                  ,B_loop = 100
                  ,k_loop = 5
                  ,ml_model_set = 'figs'
                  ,max_features = 4
                  ,feature_selection = None
                  ,feature_selected = None
                  ,addon_Xtest = False
                  ,seed_number = 890701
                  ,vars_excl = ['PATNO'] ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the feature name    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    feature_names_all = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_feature_names'
                                   ,file_repo = data_repo+normalize_method.upper()+'/' )
    feature_names_id = [ feature_names_all.index(feature) for feature in feature_names_all if feature not in vars_excl ]
    feature_names = [feature_names_all[i] for i in feature_names_id]

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To determine the addon for testing data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # To determine the addon in Xtest data
    if addon_Xtest == True:
        addon_name = '_addon'
    else:
        addon_name = ''

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To read the validation data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    X_validate = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_X_validate'+addon_name
                            ,file_repo = data_repo+normalize_method.upper()+'/' )
    if addon_Xtest == True:
        X_validate = X_validate[:, feature_names_id]
    else:
        X_validate = X_validate.iloc[:, feature_names_id]
    
    y_validate = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_y_validate'
                            ,file_repo = data_repo+normalize_method.upper()+'/' )
    
    
    best_model_val = []
    best_rmse_val = []
    best_explained_variance_val = []
    best_max_error_val = []
    best_mean_abs_error_val = []
    best_median_abs_error_val = []
    best_r2_val = []
    best_act_pred_y_val = []
    best_B_X_y_batch = []
    best_permutation_importance = []

    feature_names_B = []
    # feature_names_B_id = []

    for BLoop in range(1, B_loop + 1):
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read the training data    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        X_train_val = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_B'+str(BLoop)+'_Xtrain' # +addon_name
                                ,file_repo = data_repo+normalize_method.upper()+'/')[:, feature_names_id]
        y_train_val = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_B'+str(BLoop)+'_ytrain'
                                ,file_repo = data_repo+normalize_method.upper()+'/')

        kfold_split = KFold(n_splits = k_loop, shuffle = True, random_state = seed_number)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    To read the validation data    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        X_test_val = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_B'+str(BLoop)+'_Xtest'+addon_name
                                ,file_repo = data_repo+normalize_method.upper()+'/')[:, feature_names_id]
        y_test_val = load_object(filename = normalize_method+'_'+prefix_out+data_prefix+'_B'+str(BLoop)+'_ytest'
                                ,file_repo = data_repo+normalize_method.upper()+'/')


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        ####    Feature selection    ####
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        feature_names_b = feature_names

        if feature_selection == 'boruta_cat':
            X_train_df = pd.DataFrame(data = X_train_val, columns = feature_names)
            y_train_df = np.array(y_train_val)
            X_test_val_df = pd.DataFrame(data = X_test_val, columns = feature_names)

            # Boruta-SHAP feature selection
            # If classification is False it is a Regression problem
            Feature_Selector = BorutaShap(model = CatBoostRegressor(), importance_measure = 'shap', classification = True)
            Feature_Selector.fit(X_train_df, y_train_df, n_trials = 25, random_state = seed_number) # n_trials = 50

            if feature_selected == 'important':
                # Important features
                feature_names_b = list(Feature_Selector.Subset().columns)

                # X_train with important features
                X_train_boruta_shap = Feature_Selector.Subset()
                X_train_val = np.array(X_train_boruta_shap)
                X_test_val = np.array(X_test_val_df[feature_names_b])
                
            elif feature_selected == 'both':
                # Display features to be removed
                features_to_remove = Feature_Selector.features_to_remove

                # Important + tentative features
                feature_names_b = X_train_df.columns[ ~X_train_df.columns.isin(features_to_remove) ]

                # X_train with important + tentative features
                X_train_boruta_shap = X_train_df.drop(columns = features_to_remove)
                X_train_val = np.array(X_train_boruta_shap)
                X_test_val = np.array(X_test_val_df[feature_names_b])
            
            
        
        if feature_selection == 'boruta_xgb':
            X_train_df = pd.DataFrame(data = X_train_val, columns = feature_names)
            y_train_df = np.array(y_train_val)
            X_test_val_df = pd.DataFrame(data = X_test_val, columns = feature_names)

            # Boruta-SHAP feature selection
            # If classification is False it is a Regression problem
            Feature_Selector = BorutaShap(model = XGBRegressor(), importance_measure = 'shap', classification = True)
            Feature_Selector.fit(X_train_df, y_train_df, n_trials = 25, sample = False, train_or_test = 'test', normalize = True, verbose = True, random_state = seed_number) # n_trials = 50

            if feature_selected == 'important':
                # Important features
                feature_names_b = list(Feature_Selector.Subset().columns)

                # X_train with important features
                X_train_boruta_shap = Feature_Selector.Subset()
                X_train_val = np.array(X_train_boruta_shap)
                X_test_val = np.array(X_test_val_df[feature_names_b])
                
            elif feature_selected == 'both':
                # Display features to be removed
                features_to_remove = Feature_Selector.features_to_remove

                # Important + tentative features
                feature_names_b = X_train_df.columns[ ~X_train_df.columns.isin(features_to_remove) ]

                # X_train with important + tentative features
                X_train_boruta_shap = X_train_df.drop(columns = features_to_remove)
                X_train_val = np.array(X_train_boruta_shap)
                X_test_val = np.array(X_test_val_df[feature_names_b])
            
            

        feature_names_B.append( feature_names_b )

        rmse = []
        model_kfold = []

        if len(feature_names_b) > 0:

            if ml_model_set == 'tpot':
                # To fit Tree-based Pipeline Optimization Tool (TPOT)
                ml_model = TPOTRegressor(generations = 5, population_size = 20, cv = k_loop, verbosity = 2, scoring = 'neg_mean_squared_error') # roc_auc
                ml_model.fit(X_train_val, y_train_val)

            else:
                for train_index, test_index in kfold_split.split(X_train_val, y_train_val):

                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                    ####    To read the data for k-fold validation    ####
                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                    
                    X_train, X_test = X_train_val[train_index, ], X_train_val[test_index, ]
                    y_train, y_test = np.array(y_train_val)[train_index], np.array(y_train_val)[test_index]

                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                    ####    To fit the model (k-fold)    ####
                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                    if ml_model_set == 'figs':
                        # To fit FIGS model
                        ml_model = FIGSRegressor(max_rules = max_features )#, random_state = seed_number)  # Initialize a model
                        ml_model.fit(X_train, y_train, feature_names = feature_names_b)
                    
                    elif ml_model_set == 'catboost':
                        # To fit CatBoost model
                        ml_model = CatBoostRegressor(iterations = 10, learning_rate = 0.1, depth=2, verbose=True)# , max_depth = max_features)
                        ml_model.fit(X_train, y_train)

                    elif ml_model_set == 'hs':
                        # To fit Hierarchical Shrinkage (HS)
                        ml_model = HSTreeRegressor(estimator_ = DecisionTreeRegressor(max_leaf_nodes = max_features))
                        ml_model.fit(X_train, y_train)# , feature_names = feature_names_b)
                    
                    elif ml_model_set == 'greedy':
                        ml_model = GreedyTreeRegressor(max_depth = max_features)
                        ml_model.fit(X_train, y_train, feature_names = feature_names_b)
                    
                    elif ml_model_set == 'xgboost':
                        ml_model = XGBRegressor(max_depth = max_features)
                        ml_model.fit(X_train, y_train)
                    
                    elif ml_model_set == 'tao':
                        ml_model = TaoTreeRegressor()
                        ml_model.fit(X_train, y_train, feature_names = feature_names_b)
                    
                    elif ml_model_set == 'adaboost':
                        ml_model = AdaBoostRegressor(n_estimators = max_features, random_state = seed_number)
                        ml_model.fit(X_train, y_train)
                    
                    elif ml_model_set == 'gboost':
                        ml_model = GradientBoostingRegressor(n_estimators = max_features, random_state = seed_number)
                        ml_model.fit(X_train, y_train)
                    

                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                    ####    To predict the outcome (k-fold)    ####
                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                    # Predict the outcome
                    ml_preds = ml_model.predict(X_test)   # Discrete predictions: shape is (n_test, 1)

                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                    ####    Mean ssquare error (k-fold)    ####
                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

                    # To append the RMSE for each k-fold cross-validation
                    rmse_ = mean_squared_error(y_test, ml_preds, squared = False)
                    rmse.append(rmse_)

                    # To append the model for each k-fold cross-validation
                    model_kfold.append(ml_model)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Best model from k-fold    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            # To determine the best model with smallest RMSE from k-fold validation
            if ml_model_set == 'tpot':
                ml_kfold_best = ml_model
                best_model_val.append(ml_model)
            else:
                ml_kfold_best = model_kfold[ rmse.index(min(rmse)) ]
                best_model_val.append(ml_kfold_best)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To validate the best k-fold model    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            model_preds = ml_kfold_best.predict(X_test_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    RMSE score (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            # To append the RMSE for each validation
            rmse_val = mean_squared_error(y_test_val, model_preds, squared = False)
            best_rmse_val.append(rmse_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Explained variance (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            explained_variance_val = explained_variance_score(y_test_val, model_preds)
            best_explained_variance_val.append(explained_variance_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Max error (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            max_error_val = max_error(y_test_val, model_preds)
            best_max_error_val.append(max_error_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Mean absolute error (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            
            mean_abs_error_val = mean_absolute_error(y_test_val, model_preds)
            best_mean_abs_error_val.append(mean_abs_error_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Median absolute error (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            
            median_abs_error_val = median_absolute_error(y_test_val, model_preds)
            best_median_abs_error_val.append(median_abs_error_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    R-squared (Bootstrapping)    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            r2_val = r2_score(y_test_val, model_preds)
            best_r2_val.append(r2_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    Actual y and predicted y    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            # To keep actual y and predicted y in a list for redisual plot
            act_pred_y_val = [y_test_val, model_preds]
            best_act_pred_y_val.append(act_pred_y_val)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            ####    To calculate the permutation importance    ####
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

            if (ml_model_set == 'catboost') | (ml_model_set == 'xgboost') | (ml_model_set == 'gboost') | (ml_model_set == 'adaboost'):
                perm_importance = ml_kfold_best.feature_importances_

            else:
                perm_importance = permutation_importance(ml_kfold_best, X_test_val, y_test_val, scoring = 'neg_root_mean_squared_error').importances_mean

            best_permutation_importance.append( perm_importance )

        else:
            best_rmse_val.append( [] )
            best_model_val.append( [] )
            best_explained_variance_val.append( [] )
            best_max_error_val.append( [] )
            best_mean_abs_error_val.append( [] )
            best_median_abs_error_val.append( [] )
            best_r2_val.append( [] )
            best_act_pred_y_val.append( [] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Stability of the model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    perm_importance_df = pd.DataFrame({'feature': feature_names})

    feature_names_B2 = [ x for x in feature_names_B if len(x) > 0 ] # To drop the empty element in case of no feature is selected from feature selection

    for stb_i in range(len(best_permutation_importance)):
        perm_importance_df = pd.merge( perm_importance_df, pd.DataFrame({'feature': feature_names_B2[stb_i], 'B_'+str(stb_i): best_permutation_importance[stb_i]}), how = 'left' )

    perm_importance_array = np.array(perm_importance_df.fillna(0).drop('feature', axis = 1))
    
    # Overall mean of permutation importance of each feature
    overall_mean_features = np.mean(perm_importance_array, axis = 1)

    rho_stability = []
    pval_stability = []
    for stb_i in range(len(best_permutation_importance)):
        rho, pval = spearmanr(overall_mean_features[overall_mean_features != 0], perm_importance_array[:,stb_i][overall_mean_features != 0])
        if np.isnan(rho) == True:
            rho = 0
        if np.isnan(pval) == True:
            pval = 1
        rho_stability.append(rho)
        pval_stability.append(pval)
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    RMSE of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_rmse = []
    best_model_rmse.append( best_rmse_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Best model from validation    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model = []
    best_model.append( best_model_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Explained variance of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    best_model_explained_variance = []
    best_model_explained_variance.append( best_explained_variance_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Max error of the best model    #####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_max_error = []
    best_model_max_error.append( best_max_error_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mean absolute error of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_mean_abs_error = []
    best_model_mean_abs_error.append( best_mean_abs_error_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Median absolute error of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_median_abs_error = []
    best_model_median_abs_error.append( best_median_abs_error_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    R-squared of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_r2 = []
    best_model_r2.append( best_r2_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Actual y and predicted y of the best model    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    best_model_act_pred_y = []
    best_model_act_pred_y.append( best_act_pred_y_val[ best_rmse_val.index(min(best_rmse_val)) ] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    X_train, X_test, y_train, y_train, batch_train, batch_test    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # To keep these data for evaluating the performance of batch effect normalization
    best_model_X_y_batch = best_rmse_val.index(min(best_rmse_val)) + 1   # The number of B


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                                              Validating the best model                                              ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Selected features    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    feature_names_selected = feature_names_B[ best_rmse_val.index(min(best_rmse_val)) ]

    X_validate_selected = pd.DataFrame(X_validate, columns = feature_names)
    X_validate_selected = X_validate_selected[feature_names_selected]

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Predict the outcome from validating set    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Predict the outcome
    best_validate_model_preds = best_model[0].predict(X_validate_selected)   # Discrete predictions: shape is (n_test, 1)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    RMSE score (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # To append the RMSE score for each validation
    best_model_rmse.append( mean_squared_error(y_validate, best_validate_model_preds, squared = False) )
    # To append the RMSE score for each looping (B_loop)
    best_model_rmse.append( best_rmse_val )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Explained variance (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    val_model_explained_variance = explained_variance_score(y_validate, best_validate_model_preds)
    best_model_explained_variance.append(val_model_explained_variance)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Maximum error (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    val_model_max_error = max_error(y_validate, best_validate_model_preds)
    best_model_max_error.append(val_model_max_error)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mean absolute error (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    val_model_mean_abs_error = mean_absolute_error(y_validate, best_validate_model_preds)
    best_model_mean_abs_error.append(val_model_mean_abs_error)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Median absolute error (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    val_model_median_abs_error = median_absolute_error(y_validate, best_validate_model_preds)
    best_model_median_abs_error.append(val_model_median_abs_error)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    R-squared (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    val_model_r2 = r2_score(y_validate, best_validate_model_preds)
    best_model_r2.append(val_model_r2)
    best_model_r2.append(best_r2_val)   # R-squared for each looping (B_loop)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Actual y and predicted y (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # To keep actual y and predicted y in a list for redisual plot
    val_model_act_pred_y = [y_validate, best_validate_model_preds]
    best_model_act_pred_y.append(val_model_act_pred_y)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Permutation importance (Validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    if (ml_model_set == 'catboost') | (ml_model_set == 'xgboost'):
        val_model_perm = best_model[0].feature_importances_
        val_model_perm2 = val_model_perm

    else:
        val_model_perm = PermutationImportance(best_model[0], random_state = seed_number, scoring = 'neg_root_mean_squared_error').fit(X_validate_selected, y_validate)
        val_model_perm2 = val_model_perm.feature_importances_
    
    tbl_val_model_perm = pd.DataFrame({'Feature': feature_names_selected
                                      ,'Weight': val_model_perm2 })
    tbl_val_model_perm = tbl_val_model_perm[tbl_val_model_perm['Weight'] != 0]
    tbl_val_model_perm.sort_values(['Weight'], ascending = False, inplace = True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    SHAP values (validating)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Fits the explainer
    explainer = shap.Explainer(best_model[0].predict, X_validate_selected)
    # Calculate the SHAP values - It takes some time
    shap_values = explainer(X_validate_selected)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return best model information    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    return({'best_model': best_model                                                 # 0
           ,'best_model_rmse': best_model_rmse                                       # 1 ;  [0]: tested; [1]: validated (final); [2]: each B_loop looping
           ,'best_model_explained_variance': best_model_explained_variance           # 2
           ,'best_model_max_error': best_model_max_error                             # 3
           ,'best_model_mean_abs_error': best_model_mean_abs_error                   # 4
           ,'best_model_median_abs_error': best_model_median_abs_error               # 5
           ,'best_model_r2': best_model_r2                                           # 6 ;  [0]: tested; [1]: validated (final); [2]: each B_loop looping
           ,'best_model_act_pred_y': best_model_act_pred_y                           # 7 ;  [0]: tested; [1]: validated (final)
           ,'best_model_X_y_batch': best_model_X_y_batch                             # 8
           ,'rho_stability': rho_stability                                           # 9
           ,'tbl_val_model_perm': tbl_val_model_perm                                 # 10
           ,'shap_values': shap_values                                               # 11
           ,'feature_names': feature_names_selected                                  # 12
          })


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                         To Extract the Best Model                                         ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ models : the name of the trained model objects
# @ stability_xlabel: the xlabel of the boxplot of stability
# @ stability_title: the title of the boxplot of stability
# @ filename: the filename

def extract_best_reg_model(models
                          ,stability_xlabel
                          ,fig_title
                          ,filename
                          ,file_repo
                          ,dataname
                          ,addon_name
                          ,datarepo
                          ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                   Extracting best model information                   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # To extract the model with lowest RMSE score
    rmse_models = []
    for i in range(len(models)):
        rmse_models.append( models[i]['best_model_rmse'][1] )
    
    # Index of the model with lowest RMSE
    id_rmse = rmse_models.index( min(rmse_models) )

    # Best model
    best_model = models[id_rmse]['best_model']

    # RMSE of the best model
    best_model_rmse = models[id_rmse]['best_model_rmse'][1]

    # Explained variance of the best model
    best_model_explained_variance = models[id_rmse]['best_model_explained_variance']
    
    # Maximum error of the best model
    best_model_max_error = models[id_rmse]['best_model_max_error']

    # Mean absolute error of the best model
    best_model_mean_abs_error = models[id_rmse]['best_model_mean_abs_error']

    # Median absolute error of the best model
    best_model_median_abs_error = models[id_rmse]['best_model_median_abs_error']

    # R-squared of the best model
    best_model_r2 = models[id_rmse]['best_model_r2']

    # Actual and predicted y of the best model
    best_model_act_pred_y = models[id_rmse]['best_model_act_pred_y'][1]

    # The B of the X, y, batch of the best model
    best_model_X_y_batch = models[id_rmse]['best_model_X_y_batch']

    # Stability of the looping for the best model
    rho_stability = models[id_rmse]['rho_stability']

    # Permutation importance of the best model
    tbl_val_model_perm = models[id_rmse]['tbl_val_model_perm']

    # SHAP values of the best model
    shap_values = models[id_rmse]['shap_values']

    # Feature names
    feature_names = models[id_rmse]['feature_names']


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                   Boxplot of the stability                   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    y_data = []
    for j in range(len(models)):
        y_data.append( models[j]['rho_stability'] )
    
    colors = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, len(models))]
    # colors = ['rgba(93, 164, 214, 0.5)', 'rgba(255, 144, 14, 0.5)', 'rgba(44, 160, 101, 0.5)'
    #          ,'rgba(255, 65, 54, 0.5)', 'rgba(207, 114, 255, 0.5)', 'rgba(127, 96, 0, 0.5)']

    fig = go.Figure()
    for xd, yd, cls in zip(stability_xlabel, y_data, colors):
        fig.add_trace(go.Box(
             y = yd
            ,name = xd
            ,boxpoints = 'all'
            ,jitter = 0.5
            ,whiskerwidth = 0.2
            ,fillcolor = cls
            ,marker_size = 2
            ,line_width = 1 ) )
    fig.update_layout(
         title = 'Stability of '+fig_title
        ,yaxis = dict(
             autorange = False
            ,showgrid = True
            ,zeroline = True
            ,dtick = 5
            ,gridcolor = 'rgb(255, 255, 255)'
            ,gridwidth = 0.2
            ,zerolinecolor = 'rgb(255, 255, 255)'
            ,zerolinewidth = 0.2,
        ),
        margin = dict(
            l = 40, r = 30, b = 80, t = 100
        )
        ,yaxis_range = [-1, 1]
        ,paper_bgcolor = 'rgb(243, 243, 243)'
        ,plot_bgcolor = 'rgb(243, 243, 243)'
        ,showlegend = False )
    fig.update_yaxes(dtick = 0.2)
    fig_html = py.plot(fig, filename = file_repo+'stability'+filename+'.html', auto_open = False)



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                   Boxplot of the RMSE score (looping)                   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    rmse_data = []
    for j in range(len(models)):
        rmse_data.append( models[j]['best_model_rmse'][2] )
    
    colors = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, len(models))]

    fig_rmse = go.Figure()
    for xd, yd, cls in zip(stability_xlabel, rmse_data, colors):
        fig_rmse.add_trace(go.Box(
             y = yd
            ,name = xd
            ,boxpoints = 'all'
            ,jitter = 0.5
            ,whiskerwidth = 0.2
            ,fillcolor = cls
            ,marker_size = 2
            ,line_width = 1 ) )
    fig_rmse.update_layout(
         title = 'RMSE score of '+fig_title
        ,yaxis = dict(
             autorange = False
            ,showgrid = True
            ,zeroline = True
            ,dtick = 5
            ,gridcolor = 'rgb(255, 255, 255)'
            ,gridwidth = 0.2
            ,zerolinecolor = 'rgb(255, 255, 255)'
            ,zerolinewidth = 0.2,
        ),
        margin = dict(
            l = 40, r = 30, b = 80, t = 100
        )
        ,yaxis_range = [-1, 1]
        ,paper_bgcolor = 'rgb(243, 243, 243)'
        ,plot_bgcolor = 'rgb(243, 243, 243)'
        ,showlegend = False )
    fig_rmse.update_yaxes(dtick = 0.2)
    fig_rmse_html = py.plot(fig_rmse, filename = file_repo+'rmse_loop'+filename+'.html', auto_open = False)



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####                   Boxplot of the R-squared (looping)                   ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    r2_data = []
    r2_min = []
    r2_max = []
    for j in range(len(models)):
        r2_data.append( models[j]['best_model_r2'][2] )
        r2_min.append( min(models[j]['best_model_r2'][2]) )
        r2_max.append( max(models[j]['best_model_r2'][2]) )
    
    colors = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, len(models))]

    fig_r2 = go.Figure()
    for xd, yd, cls in zip(stability_xlabel, r2_data, colors):
        fig_r2.add_trace(go.Box(
             y = yd
            ,name = xd
            ,boxpoints = 'all'
            ,jitter = 0.5
            ,whiskerwidth = 0.2
            ,fillcolor = cls
            ,marker_size = 2
            ,line_width = 1 ) )
    fig_r2.update_layout(
         title = 'R-squared of '+fig_title
        ,yaxis = dict(
             autorange = False
            ,showgrid = True
            ,zeroline = True
            ,dtick = 5
            ,gridcolor = 'rgb(255, 255, 255)'
            ,gridwidth = 0.2
            ,zerolinecolor = 'rgb(255, 255, 255)'
            ,zerolinewidth = 0.2,
        ),
        margin = dict(
            l = 40, r = 30, b = 80, t = 100
        )
        ,yaxis_range = [ round(min(r2_min)-0.1, 1) , round(max(r2_max)+0.1, 1) ]
        ,paper_bgcolor = 'rgb(243, 243, 243)'
        ,plot_bgcolor = 'rgb(243, 243, 243)'
        ,showlegend = False )
    fig_r2.update_yaxes(dtick = 0.2)
    fig_r2_html = py.plot(fig_r2, filename = file_repo+'r2_loop'+filename+'.html', auto_open = False)



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Feature importance (SHAP values)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vals = np.abs(shap_values.values).mean(0)
    feature_importance = pd.DataFrame(list(zip(feature_names, vals)),
                                      columns=['col_name','feature_importance_vals'])
    feature_importance = feature_importance[feature_importance['feature_importance_vals'] != 0]
    feature_importance.sort_values(by=['feature_importance_vals'], ascending=False, inplace=True)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Bar plot of SHAP values    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    plt.figure()
    plt.subplots_adjust(left=0.35, right=0.9, top=0.9, bottom=0.2)
    fig_bar = shap.plots.bar(shap_values, max_display = feature_importance.shape[0]+1, show = False )
    plt.savefig(file_repo+'shap_bar'+filename+'.png')


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Beeswarm plot of SHAP values    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    plt.figure()
    fig_beeswarm = shap.plots.beeswarm(shap_values, color = plt.get_cmap('cool'), max_display = feature_importance.shape[0]+1, plot_size = [25, 15], show = False)
    plt.savefig(file_repo+'shap_beeswarm'+filename+'.png')


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Boxplot Predicted y vs actual y    #####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    act_y = best_model_act_pred_y[0]
    pred_y = best_model_act_pred_y[1]

    act_y_ls = list(np.unique(act_y))

    pred_y_data = []
    for y_val in act_y_ls:
        pred_y_data.append( y_val - pred_y[act_y == y_val] )

    colors = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, len(pred_y_data))]

    fig_box_y = go.Figure()
    for xd, yd, cls in zip(act_y_ls, pred_y_data, colors):
        fig_box_y.add_trace(go.Box(
             y = yd
            ,name = xd
            ,boxpoints = 'all'
            ,jitter = 0.5
            ,whiskerwidth = 0.2
            ,fillcolor = cls
            ,marker_size = 2
            ,line_width = 1 ) )
    fig_box_y.update_layout(
         title = 'Predicted y of '+fig_title
        ,yaxis = dict(
             autorange = False
            ,showgrid = True
            ,zeroline = True
            ,dtick = 5
            ,gridcolor = 'rgb(255, 255, 255)'
            ,gridwidth = 0.2
            ,zerolinecolor = 'rgb(255, 255, 255)'
            ,zerolinewidth = 0.2,
        ),
        margin = dict(
            l = 40, r = 30, b = 80, t = 100
        )
        ,yaxis_range = [ round(min(act_y - pred_y)-0.1, 1) , round(max(act_y - pred_y)+0.1, 1) ]
        ,paper_bgcolor = 'rgb(243, 243, 243)'
        ,plot_bgcolor = 'rgb(243, 243, 243)'
        ,showlegend = False )
    fig_box_y.update_yaxes(dtick = 0.2)
    fig_box_y_html = py.plot(fig_box_y, filename = file_repo+'box_pred_y_loop'+filename+'.html', auto_open = False)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Density plot of predicted y vs feature    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    feature_names_all = load_object(filename = dataname+'_feature_names', file_repo = datarepo )

    X_validate = load_object(filename = dataname+'_X_validate'+addon_name, file_repo = datarepo )
    y_validate = load_object(filename = dataname+'_y_validate', file_repo = datarepo )

    X_train_val = load_object(filename = dataname+'_B'+str(best_model_X_y_batch)+'_Xtrain', file_repo = datarepo )
    y_train_val = load_object(filename = dataname+'_B'+str(best_model_X_y_batch)+'_ytrain', file_repo = datarepo )

    X_test_val = load_object(filename = dataname+'_B'+str(best_model_X_y_batch)+'_Xtest'+addon_name, file_repo = datarepo)
    y_test_val = load_object(filename = dataname+'_B'+str(best_model_X_y_batch)+'_ytest', file_repo = datarepo)

    fig_len = len(feature_importance['col_name'])

    if fig_len > 0:
        fig_dens = make_subplots(rows = fig_len, cols = 3)

        color_hist = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, max([len(np.unique(y_validate)), len(np.unique(y_train_val)), len(np.unique(y_test_val))]) )]

        for fea_num in range(len(feature_importance['col_name'])):
            fea_name = list(feature_importance['col_name'])[fea_num]
            feat_id = [feature_names_all.index(feature) for feature in feature_names_all if feature in fea_name]

            
            for y1_val in np.unique(y_validate):
                if fea_num > 0:
                    set_showlegend = False
                else:
                    set_showlegend = True

                # dt_X_validate = pd.concat([pd.DataFrame({'y': y_validate}), X_validate], axis = 1)
                dt_X_validate = pd.concat([pd.DataFrame({'y': y_validate}), pd.DataFrame(X_validate, columns = feature_names_all) ], axis = 1)
                ds_X_validate = np.array(dt_X_validate[ dt_X_validate['y'] == y1_val ][fea_name])
                fig_dens.add_trace( go.Histogram(x = ds_X_validate, name = y1_val, legendgroup = y1_val, nbinsx = 20
                                ,marker_color = color_hist[round(y1_val)]
                                ,showlegend = set_showlegend ), row = fea_num+1, col = 1 )
                fig_dens.update_layout(barmode='overlay')
                fig_dens.update_traces(opacity=0.75)

            for y2_val in np.unique(y_train_val):
                if y2_val in y_validate:
                    set_showlegend = False
                else:
                    set_showlegend = True
                
                if fea_num > 0:
                    set_showlegend = False

                dt_X_train = pd.concat([pd.DataFrame({'y': y_train_val}), pd.DataFrame(X_train_val, columns = feature_names_all)], axis = 1)
                ds_X_train_val = np.array(dt_X_train[ dt_X_train['y'] == y2_val ][fea_name])
                fig_dens.add_trace( go.Histogram(x = ds_X_train_val, name = y2_val, legendgroup = y2_val, nbinsx = 20
                                ,marker_color = color_hist[round(y2_val)]
                                ,showlegend = set_showlegend), row = fea_num+1, col = 2 )
                fig_dens.update_layout(barmode='overlay')
                fig_dens.update_traces(opacity=0.75)

            for y3_val in np.unique(y_test_val):
                if (y3_val in y_validate) | (y3_val in y_train_val):
                    set_showlegend = False
                else:
                    set_showlegend = True
                
                if fea_num > 0:
                    set_showlegend = False

                dt_X_test = pd.concat([pd.DataFrame({'y': y_test_val}), pd.DataFrame(X_test_val, columns = feature_names_all)], axis = 1)
                ds_X_test_val = np.array(dt_X_test[ dt_X_test['y'] == y3_val ][fea_name])
                fig_dens.add_trace( go.Histogram(x = ds_X_test_val, name = y3_val, legendgroup = y3_val, nbinsx = 20
                                ,marker_color = color_hist[round(y3_val)]
                                ,showlegend = set_showlegend), row = fea_num+1, col = 3 )
                fig_dens.update_layout(barmode='overlay')
                fig_dens.update_traces(opacity=0.75)

        fig_dens_html = py.plot(fig_dens, filename = file_repo+'histogram_x_y_loop'+filename+'.html', auto_open = False)

    return({'best_model': best_model
           ,'best_model_rmse': best_model_rmse
           ,'best_model_rmse_loop': rmse_data
           ,'best_model_explained_variance': best_model_explained_variance
           ,'best_model_max_error': best_model_max_error
           ,'best_model_mean_abs_error': best_model_mean_abs_error
           ,'best_model_median_abs_error': best_model_median_abs_error
           ,'best_model_r2': best_model_r2
           ,'best_model_act_pred_y': best_model_act_pred_y
           ,'best_model_X_y_batch': best_model_X_y_batch
           ,'rho_stability': rho_stability
           ,'tbl_val_model_perm': tbl_val_model_perm
           ,'shap_values': shap_values
           ,'feature_importance': feature_importance
          })
