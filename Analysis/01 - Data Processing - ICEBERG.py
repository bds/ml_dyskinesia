#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    To Import ICEBERG Data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_iceberg = pd.read_csv("Analysis/Data/Iceberg/C1374ICEBERGPromoteu_DATA_2023-02-07_1447.csv")
data_withdrawals = pd.read_excel("Analysis/Data/Iceberg/ICEBERG_MedConditionWithdrawals.xlsx")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    ICEBERG Data Processing    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class preprocess_iceberg:
        
    def __init__(self, data, patno_exclude, vars_info):
        
        global data_variables

        self.data_variables = data_variables
        self.data = data
        self.patno_exclude = patno_exclude
        self.vars_info = vars_info


    def process_iceberg(self):

        data_variables = self.data_variables
        data = self.data
        patno_exclude = self.patno_exclude

        # Last follow-up date or death date
        data_fu = data[['num_sujet', 'date_visite']].copy()
        # data_fu['date_visite'] = pd.to_datetime(data_fu['date_visite'], format = '%Y-%m-%d')
        data_fu['date_visite'] = pd.to_datetime(data_fu['date_visite'], format = '%d/%m/%Y')
        data_fu = data_fu.groupby(['num_sujet']).agg('max').reset_index()
        data_fu.rename( columns = {'date_visite': 'LAST_FOLLOW_UP_DATE'}, inplace = True)

        # Inclusion criteria
        # statue_inclusion = 4 (Parkinson idiopathique), 5 (Parkinson avec mutation)
        # diag = 1 (Parkinson's disease)
        # groupe = 4 (Parkinson idiopathique), 5 (Parkinson avec mutation)
        data = data[  (data['statut_inclusion'] == 4)
                    | (data['statut_inclusion'] == 5)
                    | (data['diag'] == 1)
                    | (data['groupe'] == 4)
                    | (data['groupe'] == 5) ]

        # No alert for inclusion criteria
        data = data[ ( data['crit_age'] != 0 )          &   ( data['crit_mms'] != 0 )       &   ( data['crit_contrac'] != 0 )       &\
                        ( data['crit_ss'] != 0 )           &   ( data['crit_consent'] != 0 )   &   ( data['crit_diag_mp'] != 0 )       &\
                        ( data['crit_diag_duree'] != 0 )   &   ( data['crit_mutation'] != 0 )  &   ( data['crit_risq_exneur'] != 0 )   &\
                        ( data['crit_risq_park'] != 0 )    &   ( data['crit_irbd'] != 0 )      &   ( data['crit_app'] != 0 )           &\
                        ( data['crit_tem_exneur'] != 0 ) ]

        # No alert for exclusion criteria
        data = data[ ( data['crit_proteg'] != 1 )       &   ( data['crit_irm'] != 1 )       &   ( data['crit_psychiat'] != 1 )      &\
                        ( data['crit_pronost'] != 1 )      &   ( data['crit_defic'] != 1 )     &   ( data['crit_allait'] != 1 )        &\
                        ( data['crit_rbm'] != 1 )          &   ( data['crit_spect'] != 1 )     &   ( data['crit_neurolep_sec'] != 1 )  &\
                        ( data['crit_pps'] != 1 )          &   ( data['crit_neurolep_6m'] != 1 ) & ( data['crit_res_anoma_bio'] != 1 ) ]

        # To exclude the patients withdrawl earlier
        data = data[ ~data['num_sujet'].isin(patno_exclude)]

        # Baseline visit
        data = data[ data['visit_num'] == 0 ]

        # To define a variable to be same as PPMI cohort
        data['APPRDX'] = 1

        # Date of visit
        # data['date_visite'] = pd.to_datetime(data['date_visite'], format = '%Y-%m-%d')
        data['date_visite'] = pd.to_datetime(data['date_visite'], format = '%d/%m/%Y')
        data['YEAR'] = pd.DatetimeIndex(data['date_visite']).year

        # Convert to float64
        def isfloat(num):
            try:
                float(num)
                return True
            except ValueError:
                return False

        data['WGTKG'] = data['poids'].apply( lambda x: float(x) if isfloat(x) else np.nan )
        data['HTCM'] = data['taille'].apply( lambda x: float(x) if isfloat(x) else np.nan )

        # Duration of PD from diagnosis to enrollment (Year)
        # data['duration'] = pd.to_datetime(data['date_visite_diag'], format = '%Y-%m-%d') - data['date_visite']
        data['duration'] = (data['date_visite'] - pd.to_datetime(data['date_diag'], format = '%d/%m/%Y')) / timedelta(days=365.25)

        # Age at enrollment
        data['age'] = ( pd.to_datetime(data['date_diag'], format = '%d/%m/%Y') - pd.to_datetime(data['ddn_a'].map(int).astype('str')+'-01-01', format = '%Y-%m-%d') ) / timedelta(days=365.25)

        # Family History of PD
        data['fampd_old'] = data['mp_fam']
        # data['fampd_old'] = np.where( data['fampd_old'] == 0, 2, data['fampd_old'] )

        # MDS-UPDRS Total Score
        data['updrs_totscore'] = data['tot_mds1'] + data['total_mds2'] + data['calc_scmds3_off'] + data['total_mds4']
        data['updrs_totscore_on'] = data['tot_mds1'] + data['total_mds2'] + data['calc_scmds3_on'] + data['total_mds4']

        # SCOPA-AUT Sexual Dysfunction Score
        data['scopa_sex'] = np.where( data['scop_sc_sex_h'].isna(), data['scop_sc_sex_f'], data['scop_sc_sex_h'] )

        # Categorical REM Sleep Behavior Disorder
        data['rem_cat'] = data['rbd'].apply(lambda x: 0 if (x==0) else (1 if (x==1) else np.nan ) )

        # Modified Schwab & England ADL Score
        data['MSEADLG'] = data['se'].apply(lambda x: 100 if x==1 else \
                                                    (90 if x==2 else \
                                                    (80 if x==3 else \
                                                    (70 if x==4 else \
                                                    (60 if x==5 else \
                                                    (50 if x==6 else \
                                                    (40 if x==7 else \
                                                    (30 if x==8 else \
                                                    (20 if x==9 else \
                                                    (10 if x==10 else \
                                                    (0 if x==11 else np.nan) ))))))))) )

        # LRRK2 Mutation
        data.loc[ data['mutation_parkinson'] == 2, 'LRRK2' ] = 1
        data.loc[ (data['mutation_parkinson'] != 2) & (~data['mutation_parkinson'].isnull()), 'LRRK2' ] = 0

        # GBA Mutation
        data.loc[ data['mutation_parkinson'] == 1, 'GBA' ] = 1
        data.loc[ (data['mutation_parkinson'] != 1) & (~data['mutation_parkinson'].isnull()), 'GBA' ] = 0

        # Levodopa treatment
        data['levodopa'] = np.where( (data['mds_3c_on'] == 1) | (data['mds_3c_off'] == 1), 1, np.where( (data['mds_3c_on'] == 0) | (data['mds_3c_off'] == 0), 0, np.nan ) )

        # Initial motor symptom - Bradykinesia
        data['DXBRADY'] = data['prem_sign___2']

        # Initial motor symptom - Resting Tremor
        data['DXTREMOR'] = data['prem_sign___1']

        # Initial motor symptom - Rigidity
        data['DXRIGID'] = data['prem_sign___3']

        # Death status
        data['DEATH_STATUS'] = data['motif_sortie___5']

        # Last follow-up date
        data = pd.merge(data, data_fu, how = 'left', on = ['num_sujet'])

        # Trail Making Part A complete in max time
        data['TMTACMPL'] = data['temps_a_tmt'].apply(lambda x: 1 if x <= 300 else (0 if x > 300 else np.nan))

        # Follow-up days
        data['FOLLOW_UP_DAYS'] = data['LAST_FOLLOW_UP_DATE'] - data['date_visite']

        # Dyskinesias : mds_4_2, mds_4_1
        data['dyskin'] = data['dyskin'].map(float)
        data.loc[ (data['mds_4_2'] == 0) & (data['mds_4_1'] == 0) & (data['dyskin'] == 0), 'NP4DYSKI2' ] = 0
        data.loc[ (data['mds_4_2'] > 0) | (data['mds_4_1'] > 0) | (data['dyskin'] == 1), 'NP4DYSKI2' ] = 1

        # Motor fluctuation : NP4FLCTX2
        data['NP4FLCTX2'] = np.where( (data['mds_4_3'] > 0) | (data['mds_4_4'] > 0) | (data['mds_4_5'] > 0) | (data['fluct_motr'] == 1), 1
                                     ,np.where( (data['mds_4_3'] == 0) | (data['mds_4_4'] == 0) | (data['mds_4_5'] == 0) | (data['fluct_motr'] == 0), 0, np.nan ) )

        # Impact of cognitive impairment in daily living
        data['NP1COG2'] = np.where( data['mds_1_1'].isin([0, 1]), 0, np.where( data['mds_1_1'].isin([2, 3, 4]), 1, np.nan ) )

        # Gait disorder
        data['GAITRSP'] = data['prem_sign___8']

        # Rigidity or bradykinesia - GAITRSP =  1 or DXRIGID = 1 or DXBRADY = 1
        data['DXGRB'] = np.where( (data['DXRIGID'] == 1) | (data['DXBRADY'] == 1), 1
                                 ,np.where( (data['DXRIGID'] == 0) | (data['DXBRADY'] == 0), 0, np.nan ) )
        
        # Motor fluctuation composite score
        # data['fluctuation'] = np.where( (~data['mds_4_3'].isnull()) & (~data['mds_4_4'].isnull()) & (~data['mds_4_5'].isnull())
        #                                ,data['mds_4_3'] + data['mds_4_4'] + data['mds_4_5'], np.nan )
        data['fluctuation_mean'] = data[['mds_4_3', 'mds_4_4', 'mds_4_5']].mean(axis = 1)
        data['tmp_mds_4_3'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['mds_4_3'].isnull() ), data['fluctuation_mean'], data['mds_4_3'] )
        data['tmp_mds_4_4'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['mds_4_4'].isnull() ), data['fluctuation_mean'], data['mds_4_4'] )
        data['tmp_mds_4_5'] = np.where( ( ~data['fluctuation_mean'].isnull() ) & ( data['mds_4_5'].isnull() ), data['fluctuation_mean'], data['mds_4_5'] )
        data['fluctuation'] = np.where( ~data['fluctuation_mean'].isnull(), data[['tmp_mds_4_3', 'tmp_mds_4_4', 'tmp_mds_4_5']].sum(axis = 1), np.nan )
        
        # MDS-UPDRS III - Rigidity
        # Missing value imputation: average of MDS-UPDRS III Rigidity
        data['rigid_mean'] = data[['mds_3_3_1_on', 'mds_3_3_2_on', 'mds_3_3_3_on', 'mds_3_3_4_on', 'mds_3_3_5_on']].mean(axis = 1)
        data['mds_3_3_1_on'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['mds_3_3_1_on'].isnull() ), data['rigid_mean'], data['mds_3_3_1_on'] )
        data['mds_3_3_2_on'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['mds_3_3_2_on'].isnull() ), data['rigid_mean'], data['mds_3_3_2_on'] )
        data['mds_3_3_3_on'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['mds_3_3_3_on'].isnull() ), data['rigid_mean'], data['mds_3_3_3_on'] )
        data['mds_3_3_4_on'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['mds_3_3_4_on'].isnull() ), data['rigid_mean'], data['mds_3_3_4_on'] )
        data['mds_3_3_5_on'] = np.where( ( ~data['rigid_mean'].isnull() ) & ( data['mds_3_3_5_on'].isnull() ), data['rigid_mean'], data['mds_3_3_5_on'] )

        # Rigidity upper extremities score
        data['rigidity_upper'] = np.where( ~data['rigid_mean'].isnull(), data[['mds_3_3_2_on', 'mds_3_3_3_on']].sum(axis = 1), np.nan )

        # Rigidity lower extremities score
        data['rigidity_lower'] = np.where( ~data['rigid_mean'].isnull(), data[['mds_3_3_4_on', 'mds_3_3_5_on']].sum(axis = 1), np.nan )

        # Total rigidity score
        data['rigidity_sum'] = np.where( ~data['rigid_mean'].isnull()
                                        ,data[['mds_3_3_1_on', 'mds_3_3_2_on', 'mds_3_3_3_on', 'mds_3_3_4_on', 'mds_3_3_5_on']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Bradykinesia
        # NP3FACXP_ON - mds_3_2_on
        # NP3FTAPL_ON - mds_3_4_2_on
        # NP3FTAPR_ON - mds_3_4_1_on
        # NP3HMOVL_ON - mds_3_5_2_on
        # NP3HMOVR_ON - mds_3_5_1_on
        # NP3PRSPL_ON - mds_3_6_2_on
        # NP3PRSPR_ON - mds_3_6_1_on
        # NP3TTAPL_ON - mds_3_7_2_on
        # NP3TTAPR_ON - mds_3_7_1_on
        # NP3LGAGL_ON - mds_3_8_2_on
        # NP3LGAGR_ON - mds_3_8_1_on
        # NP3BRADY_ON - mds_3_14_on

        data['brady_mean'] = data[['mds_3_2_on', 'mds_3_4_2_on', 'mds_3_4_1_on', 'mds_3_5_2_on', 'mds_3_5_1_on', 'mds_3_6_2_on', 'mds_3_6_1_on', 'mds_3_7_2_on', 'mds_3_7_1_on'
                                   ,'mds_3_8_2_on', 'mds_3_8_1_on', 'mds_3_14_on']].mean(axis = 1)
        data['tmp_NP3FACXP_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_2_on'].isnull() ), data['brady_mean'], data['mds_3_2_on'] )
        data['tmp_NP3FTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_4_2_on'].isnull() ), data['brady_mean'], data['mds_3_4_2_on'] )
        data['tmp_NP3FTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_4_1_on'].isnull() ), data['brady_mean'], data['mds_3_4_1_on'] )
        data['tmp_NP3HMOVL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_5_2_on'].isnull() ), data['brady_mean'], data['mds_3_5_2_on'] )
        data['tmp_NP3HMOVR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_5_1_on'].isnull() ), data['brady_mean'], data['mds_3_5_1_on'] )
        data['tmp_NP3PRSPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_6_2_on'].isnull() ), data['brady_mean'], data['mds_3_6_2_on'] )
        data['tmp_NP3PRSPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_6_1_on'].isnull() ), data['brady_mean'], data['mds_3_6_1_on'] )
        data['tmp_NP3TTAPL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_7_2_on'].isnull() ), data['brady_mean'], data['mds_3_7_2_on'] )
        data['tmp_NP3TTAPR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_7_1_on'].isnull() ), data['brady_mean'], data['mds_3_7_1_on'] )
        data['tmp_NP3LGAGL_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_8_2_on'].isnull() ), data['brady_mean'], data['mds_3_8_2_on'] )
        data['tmp_NP3LGAGR_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_8_1_on'].isnull() ), data['brady_mean'], data['mds_3_8_1_on'] )
        data['tmp_NP3BRADY_ON'] = np.where( ( ~data['brady_mean'].isnull() ) & ( data['mds_3_14_on'].isnull() ), data['brady_mean'], data['mds_3_14_on'] )

        # Bradykinesia score
        data['bradykinesia'] = np.where( ~data['brady_mean'].isnull()
                                        ,data[['tmp_NP3FACXP_ON', 'tmp_NP3FTAPL_ON', 'tmp_NP3FTAPR_ON', 'tmp_NP3HMOVL_ON', 'tmp_NP3HMOVR_ON', 'tmp_NP3PRSPL_ON'
                                               , 'tmp_NP3PRSPR_ON', 'tmp_NP3TTAPL_ON', 'tmp_NP3TTAPR_ON', 'tmp_NP3LGAGL_ON', 'tmp_NP3LGAGR_ON', 'tmp_NP3BRADY_ON']].sum(axis = 1)
                                        ,np.nan )

        # MDS-UPDRS III : Axial symptoms
        # NP2WALK - mds_2_12
        # NP2FREZ - mds_2_13
        # NP3SPCH_ON - mds_3_1_on
        # NP3RISNG_ON - mds_3_9_on
        # NP3FRZGT_ON - mds_3_11_on
        # NP3PSTBL_ON - mds_3_12_on
        # NP3POSTR_ON - mds_3_13_on
        
        data['axial_mean'] = data[['mds_2_12', 'mds_2_13', 'mds_3_1_on', 'mds_3_9_on', 'mds_3_11_on', 'mds_3_12_on', 'mds_3_13_on']].mean(axis = 1)
        data['tmp_NP2WALK'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_2_12'].isnull() ), data['axial_mean'], data['mds_2_12'] )
        data['tmp_NP2FREZ'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_2_13'].isnull() ), data['axial_mean'], data['mds_2_13'] )
        data['tmp_NP3SPCH_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_3_1_on'].isnull() ), data['axial_mean'], data['mds_3_1_on'] )
        data['tmp_NP3RISNG_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_3_9_on'].isnull() ), data['axial_mean'], data['mds_3_9_on'] )
        data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_3_11_on'].isnull() ), data['axial_mean'], data['mds_3_11_on'] )
        data['tmp_NP3PSTBL_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_3_12_on'].isnull() ), data['axial_mean'], data['mds_3_12_on'] )
        data['tmp_NP3POSTR_ON'] = np.where( ( ~data['axial_mean'].isnull() ) & ( data['mds_3_13_on'].isnull() ), data['axial_mean'], data['mds_3_13_on'] )

        # Axial symptoms score
        data['axial'] = np.where( ~data['axial_mean'].isnull()
                                        ,data[['tmp_NP2WALK', 'tmp_NP2FREZ', 'tmp_NP3SPCH_ON', 'tmp_NP3RISNG_ON', 'tmp_NP3FRZGT_ON', 'tmp_NP3PSTBL_ON', 'tmp_NP3POSTR_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Axial symptoms (selective)
        data['axial2_mean'] = data[['mds_2_12', 'mds_3_9_on', 'mds_3_12_on', 'mds_3_13_on']].mean(axis = 1)
        data['tmp2_NP2WALK'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['mds_2_12'].isnull() ), data['axial2_mean'], data['mds_2_12'] )
        data['tmp2_NP3RISNG_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['mds_3_9_on'].isnull() ), data['axial2_mean'], data['mds_3_9_on'] )
        data['tmp2_NP3PSTBL_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['mds_3_12_on'].isnull() ), data['axial2_mean'], data['mds_3_12_on'] )
        data['tmp2_NP3POSTR_ON'] = np.where( ( ~data['axial2_mean'].isnull() ) & ( data['mds_3_13_on'].isnull() ), data['axial2_mean'], data['mds_3_13_on'] )

        # Axial symptoms score
        data['axial2'] = np.where( ~data['axial2_mean'].isnull()
                                        ,data[['tmp2_NP2WALK', 'tmp2_NP3RISNG_ON', 'tmp2_NP3PSTBL_ON', 'tmp2_NP3POSTR_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # MDS-UPDRS III : Freezing of gait
        # NP2FREZ - mds_2_13
        # NP3FRZGT_ON - mds_3_11_on

        data['gait_mean'] = data[['mds_2_13', 'mds_3_11_on']].mean(axis = 1)
        data['tmp_NP2FREZ'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['mds_2_13'].isnull() ), data['gait_mean'], data['mds_2_13'] )
        data['tmp_NP3FRZGT_ON'] = np.where( ( ~data['gait_mean'].isnull() ) & ( data['mds_3_11_on'].isnull() ), data['gait_mean'], data['mds_3_11_on'] )

        # Freezing of gait score
        data['freezing_gait'] = np.where( ~data['gait_mean'].isnull()
                                        ,data[['tmp_NP2FREZ', 'tmp_NP3FRZGT_ON']].sum(axis = 1)
                                        ,np.nan )
        
        # Tremor
        # NP3RTALJ_ON - mds_3_17_5_on
        # NP3RTALL_ON - mds_3_17_4_on
        # NP3RTALU_ON - mds_3_17_2_on
        # NP3RTARL_ON - mds_3_17_3_on
        # NP3RTARU_ON - mds_3_17_1_on

        # NP2TRMR - mds_2_10
        # NP3PTRML_ON - mds_3_15_2_on
        # NP3PTRMR_ON - mds_3_15_1_on
        # NP3KTRML_ON - mds_3_16_2_on
        # NP3KTRMR_ON - mds_3_16_1_on
        # NP3RTCON_ON - mds_3_18_on

        # Mean of tremor is only based on average from MDS-UPDRS 3.17
        data['tremor_mean'] = data[['mds_3_17_5_on', 'mds_3_17_4_on', 'mds_3_17_2_on', 'mds_3_17_3_on', 'mds_3_17_1_on']].mean(axis = 1)
        data['tmp_NP2TRMR'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_2_10'].isnull() ), data['tremor_mean'], data['mds_2_10'] )
        data['tmp_NP3PTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_15_2_on'].isnull() ), data['tremor_mean'], data['mds_3_15_2_on'] )
        data['tmp_NP3PTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_15_1_on'].isnull() ), data['tremor_mean'], data['mds_3_15_1_on'] )
        data['tmp_NP3KTRML_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_16_2_on'].isnull() ), data['tremor_mean'], data['mds_3_16_2_on'] )
        data['tmp_NP3KTRMR_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_16_1_on'].isnull() ), data['tremor_mean'], data['mds_3_16_1_on'] )
        data['tmp_NP3RTALJ_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_17_5_on'].isnull() ), data['tremor_mean'], data['mds_3_17_5_on'] )
        data['tmp_NP3RTALL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_17_4_on'].isnull() ), data['tremor_mean'], data['mds_3_17_4_on'] )
        data['tmp_NP3RTALU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_17_2_on'].isnull() ), data['tremor_mean'], data['mds_3_17_2_on'] )
        data['tmp_NP3RTARL_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_17_3_on'].isnull() ), data['tremor_mean'], data['mds_3_17_3_on'] )
        data['tmp_NP3RTARU_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_17_1_on'].isnull() ), data['tremor_mean'], data['mds_3_17_1_on'] )
        data['tmp_NP3RTCON_ON'] = np.where( ( ~data['tremor_mean'].isnull() ) & ( data['mds_3_18_on'].isnull() ), data['tremor_mean'], data['mds_3_18_on'] )

        # Tremor score
        data['tremor'] = np.where( ~data['tremor_mean'].isnull()
                                        ,data[['tmp_NP2TRMR', 'tmp_NP3PTRML_ON', 'tmp_NP3PTRMR_ON', 'tmp_NP3KTRML_ON', 'tmp_NP3KTRMR_ON', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON'
                                               ,'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                               ,np.nan )
        
        # Rest tremor score
        data['rest_tremor_score'] = np.where( ~data['tremor_mean'].isnull()
                                             ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON'
                                                    ,'tmp_NP3RTARU_ON', 'tmp_NP3RTCON_ON']].sum(axis = 1)
                                             ,np.nan )
        
        # Rest tremor amplitude score
        data['tremor_amplitude'] = np.where( ~data['tremor_mean'].isnull()
                                             ,data[['tmp_NP2TRMR', 'tmp_NP3RTALJ_ON', 'tmp_NP3RTALL_ON', 'tmp_NP3RTALU_ON', 'tmp_NP3RTARL_ON', 'tmp_NP3RTARU_ON']].sum(axis = 1)
                                             ,np.nan )
        
        # Dyskinesia composite score
        data['dyski_mean'] = data[['mds_4_1', 'mds_4_2']].mean(axis = 1)
        data['tmp_mds_4_1'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['mds_4_1'].isnull() ), data['dyski_mean'], data['mds_4_1'] )
        data['tmp_mds_4_2'] = np.where( ( ~data['dyski_mean'].isnull() ) & ( data['mds_4_2'].isnull() ), data['dyski_mean'], data['mds_4_2'] )
        data['dyski_score'] = np.where( ~data['dyski_mean'].isnull(), data[['tmp_mds_4_1', 'tmp_mds_4_2']].sum(axis = 1), np.nan )

        # Mild cognitive impairment based on MoCA score
        data['MoCA26'] = np.where( (~data['total_moca'].isnull()) & (data['total_moca'] < 26), 1
                                  ,np.where( (~data['total_moca'].isnull()), 0, data['total_moca'] ) )

        self.data_process = data
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    ICEBERG Data Processing - To add non-common variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def process_iceberg_all(self):
            
            data = self.data_process.copy()
            vars_info = self.vars_info


            # Mutation
            data['LRRK2'] = np.where( data['mutation_parkinson'] == 2, 1, np.where( ~data['mutation_parkinson'].isnull(), 0, np.nan ) )
            data['GBA'] = np.where( data['mutation_parkinson'] == 1, 1, np.where( ~data['mutation_parkinson'].isnull(), 0, np.nan ) )

            # Improvement at affected side by levodopa
            data['improvement_levodopa'] = np.where( data['amel_dop'] == 1, 1, np.where( ~data['amel_dop'].isnull(), 0, np.nan ) )

            # Weight loss in the past year
            data['weight_loss'] = np.where( data['perte_poids'] == '1', 1, np.where( data['perte_poids'] == '0', 0, np.nan ) )

            # Heart Rate
            data['fc_couch'] = data['fc_couch'].replace('NI', np.nan).map(float)
            data['fc_imm'] = data['fc_imm'].map(float)
            data['fc_1min'] = data['fc_imm'].map(float)
            data['fc_3min'] = data['fc_imm'].map(float)
            data['fc_5min'] = data['fc_imm'].map(float)
            data['fc_10min'] = data['fc_imm'].map(float)

            # Systolic Blood Pressure (SBP)
            data['pas_couch'] = data['pas_couch'].replace('NI', np.nan).map(float)
            data['pas_immed'] = data['pas_immed'].map(float)
            data['pas_1min'] = data['pas_1min'].map(float)
            data['pas_3min'] = data['pas_3min'].replace('NI', np.nan).map(float)
            data['pas_5min'] = data['pas_5min'].replace('NI', np.nan).map(float)
            data['pas_10min'] = data['pas_10min'].map(float)

            # Diastolic Blood Pressure (DBP)
            data['pad_couch'] = data['pad_couch'].replace('NI', np.nan).map(float)
            data['pad_immed'] = data['pad_immed'].map(float)
            data['pad_1min'] = data['pad_1min'].map(float)
            data['pad_3min'] = data['pad_3min'].replace('NI', np.nan).map(float)
            data['pad_5min'] = data['pad_5min'].replace('NI', np.nan).map(float)
            data['pad_10min'] = data['pad_10min'].map(float)

            # Apathy task (average of % success)
            data['avg_success_apathy_task'] = data[['jc_1_reus', 'jc_3_reus', 'ss_1_reus', 'ss_3_reus']].mean(axis = 1)
            
            # Gait assessment score
            data['total_score_gait_off'] = data[['gabs_ss_score_1_7_off', 'gabs_ss_score_8_24_off']].sum(axis = 1)
            data['total_score_gait_on'] = data[['sous_score_items_1_7_on', 'gabs_ss_score_8_24_on']].sum(axis = 1)
            
            # Current Symptom - Motor fluctuation
            data['fluct_motr'] = data['fluct_motr'].map(float)

            # Current Symptom - Gait Disorder
            data['tr_marche'] = data['tr_marche'].map(float)

            # Current Symptom - Freezing
            data['freezing_quest'] = data['freezing_quest'].map(float)

            # Current Symptom - Falls
            data['chutes'] = data['chutes'].map(float)

            # Current Symptom - Cognitive Impairment
            data['tr_cogn'] = data['tr_cogn'].map(float)

            # Current Symptom - Hallucinations
            data['halluc'] = data['halluc'].map(float)

            # Family history - iRBD
            data['irbd_fam'] = data['irbd_fam'].replace('NI', np.nan).map(float)

            # Exposure (environment)
            data['expo_insect'] = np.where( data['expo_insect'] == 9, 0, data['expo_insect'] )
            data['expo_fongic'] = np.where( data['expo_fongic'] == 9, 0, data['expo_fongic'] )
            data['expo_herbic'] = np.where( data['expo_herbic'] == 9, 0, data['expo_herbic'] )
            data['expo_pestic'] = np.where( data['expo_pestic'] == 9, 0, data['expo_pestic'] )
            data['expo_radio'] = np.where( data['expo_radio'] == 9, 0, data['expo_radio'] )
            
            # History of head trauma
            data['tc'] = data['tc'].replace('NI', np.nan).map(float)
            data['tc_pc'] = data['tc_pc'].replace('NI', np.nan).map(float)
            data['hist_head_trauma'] = np.where( (data['tc'] == 1) | (data['tc_pc'] == 1), 1, np.where( (data['tc'] == 0) | (data['tc_pc'] == 0), 0, np.nan ) )

            # Smoker
            data['tabac'] = data['tabac'].replace('NI', np.nan).map(float)
            data['smoking'] = np.where( (data['tabac'] == 2) | (data['tabac'] == 3) | (data['cigarettes'] == 1) | (data['pipe'] == 1) | (data['cigares'] == 1), 1
                                       ,np.where( (data['tabac'] == 1) | (data['cigarettes'] == 0) | (data['pipe'] == 0) | (data['cigares'] == 0), 0, np.nan ) )
            
            # Coffee consumption
            data['coffee'] = data['cafe'].replace('NI', np.nan).map(float)

            # Alcohol consumption
            data['alcool'] = data['alcool'].replace('NI', np.nan).map(float)
            data['alcohol'] = np.where( data['alcool'] == 1, 1, np.where( (data['alcool'] == 2) | (data['alcool'] == 3), 2, np.where( data['alcool'] == 4, 3, np.where( data['alcool'] == 5, 4, np.nan ) ) ) )

            # Cholesterol
            data['cholest_tot'] = data['cholest_tot'].map(float)
            data['cholest_hdl'] = data['cholest_hdl'].map(float)
            data['cholest_ldl'] = data['cholest_ldl'].map(float)
            data['rapct_chdl'] = data['rapct_chdl'].map(float)

            # Triglycerides
            data['triglyc'] = data['triglyc'].map(float)

            # Uric acid
            data['uric'] = data['uric'].replace('NI', np.nan).map(float)

            # RBD
            data['rbd'] = data['rbd'].replace('NI', np.nan).map(float)

            # Benton judgement of line orientation score
            data['score_benton'] = data['score_benton'].map(float).value_counts()

            # Weight (kg), height (cm), BMI (kg/m2)
            data['poids'] = data['poids'].replace('NI', np.nan).map(float)
            data['taille'] = data['taille'].replace('NI', np.nan).map(float)
            data['imc'] = data['imc'].map(float)

            # To rename the variable
            data.rename( columns = {'num_sujet': 'PATNO'}, inplace = True )

            # To keep the selected variables
            vars_keep = vars_info['ICEBERG Variable'].dropna()
            vars_keep = vars_keep[~vars_keep.isin(['statut_inclusion', 'diag', 'alert_crit_incl', 'alert_crit_nonincl', 'groupe', 'visit_num'])]
            data = data[vars_keep]

            # To return the processed data
            self.data_process_add = data
            return data

    

    def rename_iceberg(self):

        data = self.data_process.copy()
        data_variables = self.data_variables

        # To rename the variable
        data.rename( columns = { 'visit_num': 'EVENT_ID'\
                                ,'num_sujet': 'PATNO'\
                                ,'date_visite': 'visit_date'\
                                ,'sexe': 'gen'\
                                ,'imc': 'BMI'\
                                ,'mds_1_4': 'NP1ANXS'\
                                ,'mds_1_5': 'NP1APAT'\
                                ,'mds_1_1': 'NP1COG'\
                                ,'mds_1_6': 'NP1DDS'\
                                ,'mds_1_3': 'NP1DPRS'\
                                ,'mds_1_2': 'NP1HALL'\
                                ,'mds_1_11': 'NP1CNST'\
                                ,'mds_1_13': 'NP1FATG'\
                                ,'mds_1_12': 'NP1LTHD'\
                                ,'mds_1_9': 'NP1PAIN'\
                                ,'mds_1_8': 'NP1SLPD'\
                                ,'mds_1_7': 'NP1SLPN'\
                                ,'mds_1_10': 'NP1URIN'\
                                ,'mds_2_5': 'NP2DRES'\
                                ,'mds_2_4': 'NP2EAT'\
                                ,'mds_2_13': 'NP2FREZ'\
                                ,'mds_2_8': 'NP2HOBB'\
                                ,'mds_2_7': 'NP2HWRT'\
                                ,'mds_2_6': 'NP2HYGN'\
                                ,'mds_2_11': 'NP2RISE'\
                                ,'mds_2_2': 'NP2SALV'\
                                ,'mds_2_1': 'NP2SPCH'\
                                ,'mds_2_3': 'NP2SWAL'\
                                ,'mds_2_10': 'NP2TRMR'\
                                ,'mds_2_9': 'NP2TURN'\
                                ,'mds_2_12': 'NP2WALK'\
                                ,'mds_3_14_off': 'NP3BRADY'\
                                ,'mds_3_2_off': 'NP3FACXP'\
                                ,'mds_3_11_off': 'NP3FRZGT'\
                                ,'mds_3_4_2_off': 'NP3FTAPL'\
                                ,'mds_3_4_1_off': 'NP3FTAPR'\
                                ,'mds_3_10_off': 'NP3GAIT'\
                                ,'mds_3_5_2_off': 'NP3HMOVL'\
                                ,'mds_3_5_1_off': 'NP3HMOVR'\
                                ,'mds_3_16_2_off': 'NP3KTRML'\
                                ,'mds_3_16_1_off': 'NP3KTRMR'\
                                ,'mds_3_8_2_off': 'NP3LGAGL'\
                                ,'mds_3_8_1_off': 'NP3LGAGR'\
                                ,'mds_3_13_off': 'NP3POSTR'\
                                ,'mds_3_6_2_off': 'NP3PRSPL'\
                                ,'mds_3_6_1_off': 'NP3PRSPR'\
                                ,'mds_3_12_off': 'NP3PSTBL'\
                                ,'mds_3_15_2_off': 'NP3PTRML'\
                                ,'mds_3_15_1_off': 'NP3PTRMR'\
                                ,'mds_3_3_5_off': 'NP3RIGLL'\
                                ,'mds_3_3_3_off': 'NP3RIGLU'\
                                ,'mds_3_3_1_off': 'NP3RIGN'\
                                ,'mds_3_3_4_off': 'NP3RIGRL'\
                                ,'mds_3_3_2_off': 'NP3RIGRU'\
                                ,'mds_3_9_off': 'NP3RISNG'\
                                ,'mds_3_17_5_off': 'NP3RTALJ'\
                                ,'mds_3_17_4_off': 'NP3RTALL'\
                                ,'mds_3_17_2_off': 'NP3RTALU'\
                                ,'mds_3_17_3_off': 'NP3RTARL'\
                                ,'mds_3_17_1_off': 'NP3RTARU'\
                                ,'mds_3_18_off': 'NP3RTCON'\
                                ,'mds_3_1_off': 'NP3SPCH'\
                                ,'mds_3_7_2_off': 'NP3TTAPL'\
                                ,'mds_3_7_1_off': 'NP3TTAPR'\
                                ,'mds_3_14_on': 'NP3BRADY_ON'\
                                ,'mds_3_2_on': 'NP3FACXP_ON'\
                                ,'mds_3_11_on': 'NP3FRZGT_ON'\
                                ,'mds_3_4_2_on': 'NP3FTAPL_ON'\
                                ,'mds_3_4_1_on': 'NP3FTAPR_ON'\
                                ,'mds_3_10_on': 'NP3GAIT_ON'\
                                ,'mds_3_5_2_on': 'NP3HMOVL_ON'\
                                ,'mds_3_5_1_on': 'NP3HMOVR_ON'\
                                ,'mds_3_16_2_on': 'NP3KTRML_ON'\
                                ,'mds_3_16_1_on': 'NP3KTRMR_ON'\
                                ,'mds_3_8_2_on': 'NP3LGAGL_ON'\
                                ,'mds_3_8_1_on': 'NP3LGAGR_ON'\
                                ,'mds_3_13_on': 'NP3POSTR_ON'\
                                ,'mds_3_6_2_on': 'NP3PRSPL_ON'\
                                ,'mds_3_6_1_on': 'NP3PRSPR_ON'\
                                ,'mds_3_12_on': 'NP3PSTBL_ON'\
                                ,'mds_3_15_2_on': 'NP3PTRML_ON'\
                                ,'mds_3_15_1_on': 'NP3PTRMR_ON'\
                                ,'mds_3_3_5_on': 'NP3RIGLL_ON'\
                                ,'mds_3_3_3_on': 'NP3RIGLU_ON'\
                                ,'mds_3_3_1_on': 'NP3RIGN_ON'\
                                ,'mds_3_3_4_on': 'NP3RIGRL_ON'\
                                ,'mds_3_3_2_on': 'NP3RIGRU_ON'\
                                ,'mds_3_9_on': 'NP3RISNG_ON'\
                                ,'mds_3_17_5_on': 'NP3RTALJ_ON'\
                                ,'mds_3_17_4_on': 'NP3RTALL_ON'\
                                ,'mds_3_17_2_on': 'NP3RTALU_ON'\
                                ,'mds_3_17_3_on': 'NP3RTARL_ON'\
                                ,'mds_3_17_1_on': 'NP3RTARU_ON'\
                                ,'mds_3_18_on': 'NP3RTCON_ON'\
                                ,'mds_3_1_on': 'NP3SPCH_ON'\
                                ,'mds_3_7_2_on': 'NP3TTAPL_ON'\
                                ,'mds_3_7_1_on': 'NP3TTAPR_ON'\
                                ,'mds_4_2': 'NP4DYSKI'\
                                ,'mds_4_6': 'NP4DYSTN'\
                                ,'mds_4_4': 'NP4FLCTI'\
                                ,'mds_4_5': 'NP4FLCTX'\
                                ,'mds_4_3': 'NP4OFF'\
                                ,'mds_4_1': 'NP4WDYSK'\
                                ,'tot_mds1': 'updrs1_score'\
                                ,'total_mds2': 'updrs2_score'\
                                ,'calc_scmds3_off': 'updrs3_score'\
                                ,'calc_scmds3_on': 'updrs3_score_on'\
                                ,'total_mds4': 'updrs4_score'\
                                ,'hy': 'NHY'\
                                ,'total_moca': 'moca'\
                                ,'scop_scgastro': 'scopa_gi'\
                                ,'scop_scurin': 'scopa_ur'\
                                ,'scop_sc_cv': 'scopa_cv'\
                                ,'scop_sc_thermo': 'scopa_therm'\
                                ,'scop_sc_tot': 'scopa'\
                                ,'score_benton': 'bjlot'\
                    }, inplace = True )

        # To define the data type
        data['dt_type'] = 'ICEBERG'

        # To keep the selected variables (but exclude 'LEDD')
        list_vars_keep = [x for x in data_variables['PPMI Variable'].dropna().tolist() if x not in ['LEDD']]
        data = data[list_vars_keep]

        # To return the processed data
        return data




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    ICEBERG Data Processing - NP4DYSKI (mds_4_2), NP1DPRS (mds_1_3), NP1COG (mds_1_1)    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_iceberg_outcome(data, baseline_data):
    
    # Inclusion criteria
    # statue_inclusion = 4 (Parkinson idiopathique), 5 (Parkinson avec mutation)
    # diag = 1 (Parkinson's disease)
    # groupe = 4 (Parkinson idiopathique), 5 (Parkinson avec mutation)
    data = data[  (data['statut_inclusion'] == 4)
                | (data['statut_inclusion'] == 5)
                | (data['diag'] == 1)
                | (data['groupe'] == 4)
                | (data['groupe'] == 5) ]
    
    # No alert for inclusion criteria
    data = data[ ( data['crit_age'] != 0 )          &   ( data['crit_mms'] != 0 )       &   ( data['crit_contrac'] != 0 )       &\
                 ( data['crit_ss'] != 0 )           &   ( data['crit_consent'] != 0 )   &   ( data['crit_diag_mp'] != 0 )       &\
                 ( data['crit_diag_duree'] != 0 )   &   ( data['crit_mutation'] != 0 )  &   ( data['crit_risq_exneur'] != 0 )   &\
                 ( data['crit_risq_park'] != 0 )    &   ( data['crit_irbd'] != 0 )      &   ( data['crit_app'] != 0 )           &\
                 ( data['crit_tem_exneur'] != 0 ) ]
    
    # No alert for exclusion criteria
    data = data[ ( data['crit_proteg'] != 1 )       &   ( data['crit_irm'] != 1 )       &   ( data['crit_psychiat'] != 1 )      &\
                 ( data['crit_pronost'] != 1 )      &   ( data['crit_defic'] != 1 )     &   ( data['crit_allait'] != 1 )        &\
                 ( data['crit_rbm'] != 1 )          &   ( data['crit_spect'] != 1 )     &   ( data['crit_neurolep_sec'] != 1 )  &\
                 ( data['crit_pps'] != 1 )          &   ( data['crit_neurolep_6m'] != 1 ) & ( data['crit_res_anoma_bio'] != 1 ) ]
    
    # MoCA scores
    data.rename(columns = {'total_moca': 'moca'}, inplace = True)

    # Cognitive impairment in daily living [NP1COG = mds_1_1]
    # 0 - No impact to daily life due to cognitive impairment   : NP1COG == 0 | NP1COG == 1
    # 1 - Daily life impact due to cognitive impairment         : NP1COG >= 2
    data['NP1COG2'] = np.where( data['mds_1_1'].isin([0, 1]), 0, np.where( data['mds_1_1'].isin([2, 3, 4]), 1, np.nan ) )
    # NP1COG2 is not derived in baseline_data
    # baseline_data['NP1COG2'] = np.where( baseline_data['NP1COG'].isin([0, 1]), 0, np.where( baseline_data['NP1COG'].isin([2, 3, 4]), 1, np.nan ) )

    # Mild cognitive impairment based on MoCA score
    data['MoCA26'] = np.where( (~data['moca'].isnull()) & (data['moca'] < 26), 1
                              ,np.where( (~data['moca'].isnull()), 0, data['moca'] ) )
    
    # To keep variables with necessary variables
    vars_keep1 = ['num_sujet', 'visit_num'
                  ,'mds_4_1', 'mds_4_2', 'mds_4_3', 'mds_4_4', 'mds_4_5'
                  ,'mds_1_1', 'mds_1_3'
                  ,'dyskin'
                  ,'fluct_motr'
                  ,'moca'
                  ,'NP1COG2'
                  ,'MoCA26' ]
    vars_keep2 = ['date_visite', 'date_diag']
    data = data[ vars_keep1 + vars_keep2 ]

    # Dyskinesias
    data.loc[ (data['mds_4_2'] == 0) & (data['mds_4_1'] == 0) & (data['dyskin'] == 0) , 'mds_4_1'] = 0
    data.loc[ (data['mds_4_2'] > 0) | (data['mds_4_1'] > 0) | (data['dyskin'] == 1), 'mds_4_1'] = 1
    data.rename(columns = {'mds_4_1': 'NP4DYSKI2'}, inplace = True)

    # Motor fluctuation : NP4FLCTX2
    # data['NP4FLCTX2'] = np.where( (data['mds_4_5'] > 0) | (data['fluct_motr'] == 1), 1, np.where( (data['mds_4_5'] == 0) | (data['fluct_motr'] == 0), 0, np.nan ) )
    data['NP4FLCTX2'] = np.where( (data['mds_4_3'] > 0) | (data['mds_4_4'] > 0) | (data['mds_4_5'] > 0)
                                 ,1
                                 ,np.where( (data['mds_4_3'] == 0) | (data['mds_4_4'] == 0) | (data['mds_4_5'] == 0), 0, np.nan ) )
    
    # Motor fluctuation composite score
    data['fluctuation'] = np.where( (~data['mds_4_3'].isnull()) & (~data['mds_4_4'].isnull()) & (~data['mds_4_5'].isnull())
                                   ,data['mds_4_3'] + data['mds_4_4'] + data['mds_4_5'], np.nan )

    # To rename the patient ID
    data.rename(columns = {'num_sujet': 'PATNO'}, inplace = True)
    data.drop(['dyskin'], axis = 1, inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Date of baseline visit    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    vars_data_keep = ['NP4DYSKI2', 'NP4FLCTX2', 'fluctuation', 'moca', 'NP1COG2', 'MoCA26']
    
    data_bs = baseline_data[['PATNO', 'NP4DYSKI', 'NP1DPRS', 'NP1COG']+vars_data_keep]

    # Dyskinesias
    # data_bs.loc[ (baseline_data['NP4DYSKI'] == 0) & (baseline_data['NP4WDYSK'] == 0) , 'NP4DYSKI2'] = 0
    # data_bs.loc[ (baseline_data['NP4DYSKI'] > 0) | (baseline_data['NP4WDYSK'] > 0) , 'NP4DYSKI2'] = 1

    data_all = pd.merge(data_bs[['PATNO']], data, how = 'left', on = ['PATNO'])
    
    data_all_keep = data_all.copy()
    data_all = data_all[['PATNO', 'visit_num', 'mds_4_2', 'mds_1_3', 'mds_1_1']+vars_data_keep]

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 1    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y1 = data_all[ data_all['visit_num'] == 1 ]
    data_y1.drop(['visit_num'], axis = 1, inplace = True)
    
    # rename the variables
    data_y1.rename(columns = {'mds_4_2': 'NP4DYSKI_Y1'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y1'
                             ,'mds_1_3': 'NP1DPRS_Y1'
                             ,'mds_1_1': 'NP1COG_Y1'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y1'
                             ,'fluctuation': 'fluctuation_Y1'
                             ,'moca': 'moca_Y1'
                             ,'NP1COG2': 'NP1COG2_Y1'
                             ,'MoCA26': 'MoCA26_Y1' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 2    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y2 = data_all[ data_all['visit_num'] == 2 ]
    data_y2.drop(['visit_num'], axis = 1, inplace = True)
    
    # rename the variables
    data_y2.rename(columns = {'mds_4_2': 'NP4DYSKI_Y2'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y2'
                             ,'mds_1_3': 'NP1DPRS_Y2'
                             ,'mds_1_1': 'NP1COG_Y2'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y2'
                             ,'fluctuation': 'fluctuation_Y2'
                             ,'moca': 'moca_Y2'
                             ,'NP1COG2': 'NP1COG2_Y2'
                             ,'MoCA26': 'MoCA26_Y2' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 3    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y3 = data_all[ data_all['visit_num'] == 3 ]
    data_y3.drop(['visit_num'], axis = 1, inplace = True)
    
    # rename the variables
    data_y3.rename(columns = {'mds_4_2': 'NP4DYSKI_Y3'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y3'
                             ,'mds_1_3': 'NP1DPRS_Y3'
                             ,'mds_1_1': 'NP1COG_Y3'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y3'
                             ,'fluctuation': 'fluctuation_Y3'
                             ,'moca': 'moca_Y3'
                             ,'NP1COG2': 'NP1COG2_Y3'
                             ,'MoCA26': 'MoCA26_Y3' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 4    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y4 = data_all[ data_all['visit_num'] == 4 ]
    data_y4.drop(['visit_num'], axis = 1, inplace = True)
    
    # rename the variables
    data_y4.rename(columns = {'mds_4_2': 'NP4DYSKI_Y4'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y4'
                             ,'mds_1_3': 'NP1DPRS_Y4'
                             ,'mds_1_1': 'NP1COG_Y4'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y4'
                             ,'fluctuation': 'fluctuation_Y4'
                             ,'moca': 'moca_Y4'
                             ,'NP1COG2': 'NP1COG2_Y4'
                             ,'MoCA26': 'MoCA26_Y4' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Outcome in Year 5    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_y5 = data_all[ data_all['visit_num'] == 5 ]
    data_y5.drop(['visit_num'], axis = 1, inplace = True)
    
    # rename the variables
    data_y5.rename(columns = {'mds_4_2': 'NP4DYSKI_Y5'
                             ,'NP4DYSKI2': 'NP4DYSKI2_Y5'
                             ,'mds_1_3': 'NP1DPRS_Y5'
                             ,'mds_1_1': 'NP1COG_Y5'
                             ,'NP4FLCTX2': 'NP4FLCTX2_Y5'
                             ,'fluctuation': 'fluctuation_Y5'
                             ,'moca': 'moca_Y5'
                             ,'NP1COG2': 'NP1COG2_Y5'
                             ,'MoCA26': 'MoCA26_Y5' }
                             ,inplace = True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    To merge the variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    data_comb = pd.merge(data_bs, data_y1, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y2, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y3, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y4, how = 'left', on = ['PATNO'])
    data_comb = pd.merge(data_comb, data_y5, how = 'left', on = ['PATNO'])
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####   Original variable of the follow-up variables    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_ORI'] = data_comb['NP4DYSKI2']
    data_comb['NP4DYSKI2_Y1_ORI'] = data_comb['NP4DYSKI2_Y1']
    data_comb['NP4DYSKI2_Y2_ORI'] = data_comb['NP4DYSKI2_Y2']
    data_comb['NP4DYSKI2_Y3_ORI'] = data_comb['NP4DYSKI2_Y3']
    data_comb['NP4DYSKI2_Y4_ORI'] = data_comb['NP4DYSKI2_Y4']
    data_comb['NP4DYSKI2_Y5_ORI'] = data_comb['NP4DYSKI2_Y5']

    data_comb['NP4FLCTX2_ORI'] = data_comb['NP4FLCTX2']
    data_comb['NP4FLCTX2_Y1_ORI'] = data_comb['NP4FLCTX2_Y1']
    data_comb['NP4FLCTX2_Y2_ORI'] = data_comb['NP4FLCTX2_Y2']
    data_comb['NP4FLCTX2_Y3_ORI'] = data_comb['NP4FLCTX2_Y3']
    data_comb['NP4FLCTX2_Y4_ORI'] = data_comb['NP4FLCTX2_Y4']
    data_comb['NP4FLCTX2_Y5_ORI'] = data_comb['NP4FLCTX2_Y5']

    data_comb['NP1COG2_ORI'] = data_comb['NP1COG2']
    data_comb['NP1COG2_Y1_ORI'] = data_comb['NP1COG2_Y1']
    data_comb['NP1COG2_Y2_ORI'] = data_comb['NP1COG2_Y2']
    data_comb['NP1COG2_Y3_ORI'] = data_comb['NP1COG2_Y3']
    data_comb['NP1COG2_Y4_ORI'] = data_comb['NP1COG2_Y4']
    data_comb['NP1COG2_Y5_ORI'] = data_comb['NP1COG2_Y5']

    data_comb['MoCA26_ORI'] = data_comb['MoCA26']
    data_comb['MoCA26_Y1_ORI'] = data_comb['MoCA26_Y1']
    data_comb['MoCA26_Y2_ORI'] = data_comb['MoCA26_Y2']
    data_comb['MoCA26_Y3_ORI'] = data_comb['MoCA26_Y3']
    data_comb['MoCA26_Y4_ORI'] = data_comb['MoCA26_Y4']
    data_comb['MoCA26_Y5_ORI'] = data_comb['MoCA26_Y5']

    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Calculate the maximum of the outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    data_comb['NP4DYSKI2_Y1'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1']].max(axis = 1)
    data_comb['NP4FLCTX2_Y1'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1']].max(axis = 1)
    data_comb['NP1COG2_Y1']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1']].max(axis = 1)
    data_comb['MoCA26_Y1']   = data_comb[['MoCA26'  , 'MoCA26_Y1']].max(axis = 1)

    data_comb['NP4DYSKI2_Y2'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2']].max(axis = 1)
    data_comb['NP4FLCTX2_Y2'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2']].max(axis = 1)
    data_comb['NP1COG2_Y2']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2']].max(axis = 1)
    data_comb['MoCA26_Y2']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2']].max(axis = 1)

    data_comb['NP4DYSKI2_Y3'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3']].max(axis = 1)
    data_comb['NP4FLCTX2_Y3'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3']].max(axis = 1)
    data_comb['NP1COG2_Y3']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3']].max(axis = 1)
    data_comb['MoCA26_Y3']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3']].max(axis = 1)

    data_comb['NP4DYSKI2_Y4'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4']].max(axis = 1)
    data_comb['NP4FLCTX2_Y4'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4']].max(axis = 1)
    data_comb['NP1COG2_Y4']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4']].max(axis = 1)
    data_comb['MoCA26_Y4']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4']].max(axis = 1)

    data_comb['NP4DYSKI2_Y5'] = data_comb[['NP4DYSKI2', 'NP4DYSKI2_Y1', 'NP4DYSKI2_Y2', 'NP4DYSKI2_Y3', 'NP4DYSKI2_Y4', 'NP4DYSKI2_Y5']].max(axis = 1)
    data_comb['NP4FLCTX2_Y5'] = data_comb[['NP4FLCTX2', 'NP4FLCTX2_Y1', 'NP4FLCTX2_Y2', 'NP4FLCTX2_Y3', 'NP4FLCTX2_Y4', 'NP4FLCTX2_Y5']].max(axis = 1)
    data_comb['NP1COG2_Y5']   = data_comb[['NP1COG2'  , 'NP1COG2_Y1'  , 'NP1COG2_Y2'  , 'NP1COG2_Y3'  , 'NP1COG2_Y4'  , 'NP1COG2_Y5']].max(axis = 1)
    data_comb['MoCA26_Y5']   = data_comb[['MoCA26'  , 'MoCA26_Y1'  , 'MoCA26_Y2'  , 'MoCA26_Y3'  , 'MoCA26_Y4'  , 'MoCA26_Y5']].max(axis = 1)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ###    Follow-up binary outcome    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # If the maximum value of the follow-up outcome is 0 and no follow-up record, keep it as missing
    data_comb['NP4DYSKI2_Y1'] = np.where( (data_comb['NP4DYSKI2_Y1']==0) & (data_comb['NP4DYSKI2_Y1_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y1'] )
    data_comb['NP4FLCTX2_Y1'] = np.where( (data_comb['NP4FLCTX2_Y1']==0) & (data_comb['NP4FLCTX2_Y1_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y1'] )
    data_comb['NP1COG2_Y1'] = np.where( (data_comb['NP1COG2_Y1']==0) & (data_comb['NP1COG2_Y1_ORI'].isnull()), np.nan, data_comb['NP1COG2_Y1'] )
    data_comb['MoCA26_Y1'] = np.where( (data_comb['MoCA26_Y1']==0) & (data_comb['MoCA26_Y1_ORI'].isnull()), np.nan, data_comb['MoCA26_Y1'] )

    data_comb['NP4DYSKI2_Y2'] = np.where( (data_comb['NP4DYSKI2_Y2']==0) & (data_comb['NP4DYSKI2_Y2_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y2'] )
    data_comb['NP4FLCTX2_Y2'] = np.where( (data_comb['NP4FLCTX2_Y2']==0) & (data_comb['NP4FLCTX2_Y2_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y2'] )
    data_comb['NP1COG2_Y2'] = np.where( (data_comb['NP1COG2_Y2']==0) & (data_comb['NP1COG2_Y2_ORI'].isnull()), np.nan, data_comb['NP1COG2_Y2'] )
    data_comb['MoCA26_Y2'] = np.where( (data_comb['MoCA26_Y2']==0) & (data_comb['MoCA26_Y2_ORI'].isnull()), np.nan, data_comb['MoCA26_Y2'] )

    data_comb['NP4DYSKI2_Y3'] = np.where( (data_comb['NP4DYSKI2_Y3']==0) & (data_comb['NP4DYSKI2_Y3_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y3'] )
    data_comb['NP4FLCTX2_Y3'] = np.where( (data_comb['NP4FLCTX2_Y3']==0) & (data_comb['NP4FLCTX2_Y3_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y3'] )
    data_comb['NP1COG2_Y3'] = np.where( (data_comb['NP1COG2_Y3']==0) & (data_comb['NP1COG2_Y3_ORI'].isnull()), np.nan, data_comb['NP1COG2_Y3'] )
    data_comb['MoCA26_Y3'] = np.where( (data_comb['MoCA26_Y3']==0) & (data_comb['MoCA26_Y3_ORI'].isnull()), np.nan, data_comb['MoCA26_Y3'] )

    data_comb['NP4DYSKI2_Y4'] = np.where( (data_comb['NP4DYSKI2_Y4']==0) & (data_comb['NP4DYSKI2_Y4_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y4'] )
    data_comb['NP4FLCTX2_Y4'] = np.where( (data_comb['NP4FLCTX2_Y4']==0) & (data_comb['NP4FLCTX2_Y4_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y4'] )
    data_comb['NP1COG2_Y4'] = np.where( (data_comb['NP1COG2_Y4']==0) & (data_comb['NP1COG2_Y4_ORI'].isnull()), np.nan, data_comb['NP1COG2_Y4'] )
    data_comb['MoCA26_Y4'] = np.where( (data_comb['MoCA26_Y4']==0) & (data_comb['MoCA26_Y4_ORI'].isnull()), np.nan, data_comb['MoCA26_Y4'] )

    data_comb['NP4DYSKI2_Y5'] = np.where( (data_comb['NP4DYSKI2_Y5']==0) & (data_comb['NP4DYSKI2_Y5_ORI'].isnull()), np.nan, data_comb['NP4DYSKI2_Y5'] )
    data_comb['NP4FLCTX2_Y5'] = np.where( (data_comb['NP4FLCTX2_Y5']==0) & (data_comb['NP4FLCTX2_Y5_ORI'].isnull()), np.nan, data_comb['NP4FLCTX2_Y5'] )
    data_comb['NP1COG2_Y5'] = np.where( (data_comb['NP1COG2_Y5']==0) & (data_comb['NP1COG2_Y5_ORI'].isnull()), np.nan, data_comb['NP1COG2_Y5'] )
    data_comb['MoCA26_Y5'] = np.where( (data_comb['MoCA26_Y5']==0) & (data_comb['MoCA26_Y5_ORI'].isnull()), np.nan, data_comb['MoCA26_Y5'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Last observation carried forward (LOCF)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried forward for outcome > 0 (Yes)
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2'] > 0), 'NP4DYSKI2_Y1'] = data_comb['NP4DYSKI2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2'] > 0), 'NP4FLCTX2_Y1'] = data_comb['NP4FLCTX2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull())   & (data_comb['NP1COG2'] > 0)  , 'NP1COG2_Y1']   = data_comb['NP1COG2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull())   & (data_comb['MoCA26'] > 0)  , 'MoCA26_Y1']   = data_comb['MoCA26']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] > 0), 'NP4DYSKI2_Y2'] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] > 0), 'NP4FLCTX2_Y2'] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull())   & (data_comb['NP1COG2_Y1'] > 0)  , 'NP1COG2_Y2']   = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull())   & (data_comb['MoCA26_Y1'] > 0)  , 'MoCA26_Y2']   = data_comb['MoCA26_Y1']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y2'] > 0), 'NP4DYSKI2_Y3'] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y2'] > 0), 'NP4FLCTX2_Y3'] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull())   & (data_comb['NP1COG2_Y2'] > 0)  , 'NP1COG2_Y3']   = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull())   & (data_comb['MoCA26_Y2'] > 0)  , 'MoCA26_Y3']   = data_comb['MoCA26_Y2']
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y3'] > 0), 'NP4DYSKI2_Y4'] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y3'] > 0), 'NP4FLCTX2_Y4'] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull())   & (data_comb['NP1COG2_Y3'] > 0)  , 'NP1COG2_Y4']   = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull())   & (data_comb['MoCA26_Y3'] > 0)  , 'MoCA26_Y4']   = data_comb['MoCA26_Y3']
    
    # Year 5
    data_comb.loc[ (data_comb['NP4DYSKI2_Y5'].isnull()) & (data_comb['NP4DYSKI2_Y4'] > 0), 'NP4DYSKI2_Y5'] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y5'].isnull()) & (data_comb['NP4FLCTX2_Y4'] > 0), 'NP4FLCTX2_Y5'] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y5'].isnull())   & (data_comb['NP1COG2_Y4'] > 0)  , 'NP1COG2_Y5']   = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y5'].isnull())   & (data_comb['MoCA26_Y4'] > 0)  , 'MoCA26_Y5']   = data_comb['MoCA26_Y4']
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Next Observation Carried Backward (NOCB)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    # Only carried backward for outcome = 0 (NO)
    
    # Year 4
    data_comb.loc[ (data_comb['NP4DYSKI2_Y4'].isnull()) & (data_comb['NP4DYSKI2_Y5'] == 0), 'NP4DYSKI2_Y4' ] = data_comb['NP4DYSKI2_Y5']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y4'].isnull()) & (data_comb['NP4FLCTX2_Y5'] == 0), 'NP4FLCTX2_Y4' ] = data_comb['NP4FLCTX2_Y5']
    data_comb.loc[ (data_comb['NP1COG2_Y4'].isnull()) & (data_comb['NP1COG2_Y5'] == 0), 'NP1COG2_Y4' ] = data_comb['NP1COG2_Y5']
    data_comb.loc[ (data_comb['MoCA26_Y4'].isnull()) & (data_comb['MoCA26_Y5'] == 0), 'MoCA26_Y4' ] = data_comb['MoCA26_Y5']
    
    # Year 3
    data_comb.loc[ (data_comb['NP4DYSKI2_Y3'].isnull()) & (data_comb['NP4DYSKI2_Y4'] == 0), 'NP4DYSKI2_Y3' ] = data_comb['NP4DYSKI2_Y4']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y3'].isnull()) & (data_comb['NP4FLCTX2_Y4'] == 0), 'NP4FLCTX2_Y3' ] = data_comb['NP4FLCTX2_Y4']
    data_comb.loc[ (data_comb['NP1COG2_Y3'].isnull()) & (data_comb['NP1COG2_Y4'] == 0), 'NP1COG2_Y3' ] = data_comb['NP1COG2_Y4']
    data_comb.loc[ (data_comb['MoCA26_Y3'].isnull()) & (data_comb['MoCA26_Y4'] == 0), 'MoCA26_Y3' ] = data_comb['MoCA26_Y4']
    
    # Year 2
    data_comb.loc[ (data_comb['NP4DYSKI2_Y2'].isnull()) & (data_comb['NP4DYSKI2_Y3'] == 0), 'NP4DYSKI2_Y2' ] = data_comb['NP4DYSKI2_Y3']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y2'].isnull()) & (data_comb['NP4FLCTX2_Y3'] == 0), 'NP4FLCTX2_Y2' ] = data_comb['NP4FLCTX2_Y3']
    data_comb.loc[ (data_comb['NP1COG2_Y2'].isnull()) & (data_comb['NP1COG2_Y3'] == 0), 'NP1COG2_Y2' ] = data_comb['NP1COG2_Y3']
    data_comb.loc[ (data_comb['MoCA26_Y2'].isnull()) & (data_comb['MoCA26_Y3'] == 0), 'MoCA26_Y2' ] = data_comb['MoCA26_Y3']
    
    # Year 1
    data_comb.loc[ (data_comb['NP4DYSKI2_Y1'].isnull()) & (data_comb['NP4DYSKI2_Y2'] == 0), 'NP4DYSKI2_Y1' ] = data_comb['NP4DYSKI2_Y2']
    data_comb.loc[ (data_comb['NP4FLCTX2_Y1'].isnull()) & (data_comb['NP4FLCTX2_Y2'] == 0), 'NP4FLCTX2_Y1' ] = data_comb['NP4FLCTX2_Y2']
    data_comb.loc[ (data_comb['NP1COG2_Y1'].isnull()) & (data_comb['NP1COG2_Y2'] == 0), 'NP1COG2_Y1' ] = data_comb['NP1COG2_Y2']
    data_comb.loc[ (data_comb['MoCA26_Y1'].isnull()) & (data_comb['MoCA26_Y2'] == 0), 'MoCA26_Y1' ] = data_comb['MoCA26_Y2']
    
    # Baseline (to replace to data_ppmi)
    data_comb.loc[ (data_comb['NP4DYSKI2'].isnull()) & (data_comb['NP4DYSKI2_Y1'] == 0), 'NP4DYSKI2' ] = data_comb['NP4DYSKI2_Y1']
    data_comb.loc[ (data_comb['NP4FLCTX2'].isnull()) & (data_comb['NP4FLCTX2_Y1'] == 0), 'NP4FLCTX2' ] = data_comb['NP4FLCTX2_Y1']
    data_comb.loc[ (data_comb['NP1COG2'].isnull()) & (data_comb['NP1COG2_Y1'] == 0), 'NP1COG2' ] = data_comb['NP1COG2_Y1']
    data_comb.loc[ (data_comb['MoCA26'].isnull()) & (data_comb['MoCA26_Y1'] == 0), 'MoCA26' ] = data_comb['MoCA26_Y1']



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Additional Outcome - NP4DYSKI2_LEDD    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    patno_ledd = baseline_data[ baseline_data['levodopa'] == 1 ]['PATNO'].tolist()
    data_comb['NP4DYSKI2_LEDD'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y1'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y1'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y2'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y2'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y3'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y3'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y4'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y4'], np.nan )
    data_comb['NP4DYSKI2_LEDD_Y5'] = np.where( data_comb['PATNO'].isin(patno_ledd) ,data_comb['NP4DYSKI2_Y5'], np.nan )
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4DYSKI2_status    : dyskinesia (1 - Yes; 0 - No)
    # 2. NP4DYSKI2_year      : Number of years from PD diagnosis to dyskinesia development
    # 3. excl_NP4DYSKI2_year : Exclude the patient with dyskinesia before or on baseline or no follow-up visit

    data_bs_date = baseline_data[['PATNO', 'visit_date']]
    data_bs_date.rename(columns = {'visit_date': 'ENROLL_DATE'}, inplace = True)

    data_all_keep['date_visite'] = pd.to_datetime(data_all_keep['date_visite'], format = '%d/%m/%Y')
    data_all_keep['date_diag'] = pd.to_datetime(data_all_keep['date_diag'], format = '%d/%m/%Y')
    data_all_dyski = data_all_keep[['PATNO', 'date_visite', 'date_diag', 'NP4DYSKI2']]
    data_all_dyski = data_all_dyski[ ~data_all_dyski['NP4DYSKI2'].isnull() ]
    data_all_dyski = pd.merge(data_all_dyski, data_bs_date, how = 'left', on = ['PATNO'])
    
    data_all_dyski['NP4DYSKI2_year'] = np.where( (~data_all_dyski['date_visite'].isnull()) & (~data_all_dyski['date_diag'].isnull()),
                                                (data_all_dyski['date_visite'] - data_all_dyski['date_diag'])/timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_year'].isnull()) & (data_all_dyski['NP4DYSKI2_year'] < 0), 'NP4DYSKI2_year' ] = 0

    # Minimum and maximum year of date_visite and ENROLL_DATE
    # If minimum or maximum year of date_visite and ENROLL_DATE = 0 and NP4DYSKI2 = 1 then excl_NP4DYSKI2_year = 1 (Yes)
    data_diff_year = data_all_dyski.copy()
    data_diff_year['diff_year'] = np.where( (~data_all_dyski['date_visite'].isnull()) & (~data_all_dyski['ENROLL_DATE'].isnull()),
                                                (data_all_dyski['date_visite'] - data_all_dyski['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4DYSKI2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4DYSKI2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to dyskinesia
    data_dyski_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ]
    data_dyski_1 = data_dyski_1.groupby(['PATNO']).min(['NP4DYSKI2_year']).reset_index()

    # Maximum days from date of onset to visit date (without dyskinesia)
    data_dyski_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_1['PATNO'])) ]
    data_dyski_0 = data_dyski_0.groupby(['PATNO']).max(['NP4DYSKI2_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_comb = pd.concat([data_dyski_0, data_dyski_1], axis = 0)
    data_dyski_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_comb.reset_index(drop = True, inplace = True)
    data_dyski_comb.rename(columns = {'NP4DYSKI2': 'NP4DYSKI2_status'}, inplace = True)
    data_dyski_comb['excl_NP4DYSKI2_year'] = np.where(data_dyski_comb['PATNO'].isin(patno_excl_NP4DYSKI2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_comb, how = 'left', on = ['PATNO'] )
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Dyskinesia Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4DYSKI2_bs_year      : Number of years from date of enrollment to dyskinesia development

    data_all_dyski['NP4DYSKI2_bs_year'] = np.where( (~data_all_dyski['date_visite'].isnull()) & (~data_all_dyski['ENROLL_DATE'].isnull()),
                                                (data_all_dyski['date_visite'] - data_all_dyski['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_dyski.loc[ (~data_all_dyski['NP4DYSKI2_bs_year'].isnull()) & (data_all_dyski['NP4DYSKI2_bs_year'] < 0), 'NP4DYSKI2_bs_year' ] = 0

    # Minimum days from baseline visit date to dyskinesia
    data_dyski_bs_1 = data_all_dyski[ data_all_dyski['NP4DYSKI2'] == 1 ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_1 = data_dyski_bs_1.groupby(['PATNO']).min(['NP4DYSKI2_bs_year']).reset_index()

    # Maximum days from baseline visit date to dyskinesia
    data_dyski_bs_0 = data_all_dyski[ (data_all_dyski['NP4DYSKI2'] == 0) & (~data_all_dyski['PATNO'].isin(data_dyski_bs_1['PATNO'])) ][['PATNO', 'NP4DYSKI2_bs_year']].reset_index(drop = True)
    data_dyski_bs_0 = data_dyski_bs_0.groupby(['PATNO']).max(['NP4DYSKI2_bs_year']).reset_index()

    # Combine the data_dyski_0 and data_dyski_1
    data_dyski_bs_comb = pd.concat([data_dyski_bs_0, data_dyski_bs_1], axis = 0)
    data_dyski_bs_comb.sort_values(['PATNO'], inplace = True)
    data_dyski_bs_comb.reset_index(drop = True, inplace = True)
    data_dyski_bs_comb = data_dyski_bs_comb[['PATNO', 'NP4DYSKI2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_dyski_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP4FLCTX2_status    : Motor fluctuations (1 - Yes; 0 - No)
    # 2. NP4FLCTX2_year      : Number of years from PD diagnosis to motor fluctuations development
    # 3. excl_NP4FLCTX2_year : Exclude the patient with motor fluctuations before or on baseline or no follow-up visit

    # data_bs_date = baseline_data[['PATNO', 'visit_date']]
    # data_bs_date.rename(columns = {'visit_date': 'ENROLL_DATE'}, inplace = True)

    # data_all_keep['date_visite'] = pd.to_datetime(data_all_keep['date_visite'], format = '%d/%m/%Y')
    # data_all_keep['date_diag'] = pd.to_datetime(data_all_keep['date_diag'], format = '%d/%m/%Y')

    data_all_flctx = data_all_keep[['PATNO', 'date_visite', 'date_diag', 'NP4FLCTX2']]
    data_all_flctx = data_all_flctx[ ~data_all_flctx['NP4FLCTX2'].isnull() ]
    data_all_flctx = pd.merge(data_all_flctx, data_bs_date, how = 'left', on = ['PATNO'])
    
    data_all_flctx['NP4FLCTX2_year'] = np.where( (~data_all_flctx['date_visite'].isnull()) & (~data_all_flctx['date_diag'].isnull()),
                                                (data_all_flctx['date_visite'] - data_all_flctx['date_diag'])/timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_year'].isnull()) & (data_all_flctx['NP4FLCTX2_year'] < 0), 'NP4FLCTX2_year' ] = 0

    # Minimum and maximum year of date_visite and ENROLL_DATE
    # If minimum or maximum year of date_visite and ENROLL_DATE = 0 and NP4FLCTX2 = 1 then excl_NP4FLCTX2_year = 1 (Yes)
    data_diff_year = data_all_flctx.copy()
    data_diff_year['diff_year'] = np.where( (~data_all_flctx['date_visite'].isnull()) & (~data_all_flctx['ENROLL_DATE'].isnull()),
                                                (data_all_flctx['date_visite'] - data_all_flctx['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP4FLCTX2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP4FLCTX2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to motor fluctuations
    data_flctx_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ]
    data_flctx_1 = data_flctx_1.groupby(['PATNO']).min(['NP4FLCTX2_year']).reset_index()

    # Maximum days from date of onset to visit date (without motor fluctuations)
    data_flctx_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_1['PATNO'])) ]
    data_flctx_0 = data_flctx_0.groupby(['PATNO']).max(['NP4FLCTX2_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_comb = pd.concat([data_flctx_0, data_flctx_1], axis = 0)
    data_flctx_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_comb.reset_index(drop = True, inplace = True)
    data_flctx_comb.rename(columns = {'NP4FLCTX2': 'NP4FLCTX2_status'}, inplace = True)
    data_flctx_comb['excl_NP4FLCTX2_year'] = np.where(data_flctx_comb['PATNO'].isin(patno_excl_NP4FLCTX2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_comb, how = 'left', on = ['PATNO'] )
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Motor Fluctuations Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP4FLCTX2_bs_year      : Number of years from date of enrollment to motor fluctuations development

    data_all_flctx['NP4FLCTX2_bs_year'] = np.where( (~data_all_flctx['date_visite'].isnull()) & (~data_all_flctx['ENROLL_DATE'].isnull()),
                                                (data_all_flctx['date_visite'] - data_all_flctx['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_flctx.loc[ (~data_all_flctx['NP4FLCTX2_bs_year'].isnull()) & (data_all_flctx['NP4FLCTX2_bs_year'] < 0), 'NP4FLCTX2_bs_year' ] = 0

    # Minimum days from baseline visit date to motor fluctuations
    data_flctx_bs_1 = data_all_flctx[ data_all_flctx['NP4FLCTX2'] == 1 ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_1 = data_flctx_bs_1.groupby(['PATNO']).min(['NP4FLCTX2_bs_year']).reset_index()

    # Maximum days from baseline visit date to motor fluctuations
    data_flctx_bs_0 = data_all_flctx[ (data_all_flctx['NP4FLCTX2'] == 0) & (~data_all_flctx['PATNO'].isin(data_flctx_bs_1['PATNO'])) ][['PATNO', 'NP4FLCTX2_bs_year']].reset_index(drop = True)
    data_flctx_bs_0 = data_flctx_bs_0.groupby(['PATNO']).max(['NP4FLCTX2_bs_year']).reset_index()

    # Combine the data_flctx_0 and data_flctx_1
    data_flctx_bs_comb = pd.concat([data_flctx_bs_0, data_flctx_bs_1], axis = 0)
    data_flctx_bs_comb.sort_values(['PATNO'], inplace = True)
    data_flctx_bs_comb.reset_index(drop = True, inplace = True)
    data_flctx_bs_comb = data_flctx_bs_comb[['PATNO', 'NP4FLCTX2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_flctx_bs_comb, how = 'left', on = ['PATNO'] )



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive impairment in Daily Living Outcome (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. NP1COG2_status    : Cognitive impairment in daily living impact (1 - Yes; 0 - No)
    # 2. NP1COG2_year      : Number of years from PD diagnosis to cognitive impairment in daily living impact
    # 3. excl_NP1COG2_year : Exclude the patient with cognitive impairment in daily living impact before or on baseline or no follow-up visit

    data_all_cogn = data_all_keep[['PATNO', 'date_visite', 'date_diag', 'NP1COG2']]
    data_all_cogn = data_all_cogn[ ~data_all_cogn['NP1COG2'].isnull() ]
    data_all_cogn = pd.merge(data_all_cogn, data_bs_date, how = 'left', on = ['PATNO'])
    
    data_all_cogn['NP1COG2_year'] = np.where( (~data_all_cogn['date_visite'].isnull()) & (~data_all_cogn['date_diag'].isnull()),
                                                (data_all_cogn['date_visite'] - data_all_cogn['date_diag'])/timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_year'].isnull()) & (data_all_cogn['NP1COG2_year'] < 0), 'NP1COG2_year' ] = 0

    # Minimum and maximum year of date_visite and ENROLL_DATE
    # If minimum or maximum year of date_visite and ENROLL_DATE = 0 and NP1COG2 = 1 then excl_NP1COG2_year = 1 (Yes)
    data_diff_year = data_all_cogn.copy()
    data_diff_year['diff_year'] = np.where( (~data_all_cogn['date_visite'].isnull()) & (~data_all_cogn['ENROLL_DATE'].isnull()),
                                                (data_all_cogn['date_visite'] - data_all_cogn['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['NP1COG2'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_NP1COG2_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to cognitive impairment in daily living impact
    data_cogn_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ]
    data_cogn_1 = data_cogn_1.groupby(['PATNO']).min(['NP1COG2_year']).reset_index()

    # Maximum days from date of onset to visit date (without cognitive impairment in daily living impact)
    data_cogn_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_1['PATNO'])) ]
    data_cogn_0 = data_cogn_0.groupby(['PATNO']).max(['NP1COG2_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_comb = pd.concat([data_cogn_0, data_cogn_1], axis = 0)
    data_cogn_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_comb.reset_index(drop = True, inplace = True)
    data_cogn_comb.rename(columns = {'NP1COG2': 'NP1COG2_status'}, inplace = True)
    data_cogn_comb['excl_NP1COG2_year'] = np.where(data_cogn_comb['PATNO'].isin(patno_excl_NP1COG2_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_comb, how = 'left', on = ['PATNO'] )
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Cognitive Impairment in Daily Living Impact Outcome (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. NP1COG2_bs_year      : Number of years from date of enrollment to cognitive impairment in daily living

    data_all_cogn['NP1COG2_bs_year'] = np.where( (~data_all_cogn['date_visite'].isnull()) & (~data_all_cogn['ENROLL_DATE'].isnull()),
                                                (data_all_cogn['date_visite'] - data_all_cogn['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_cogn.loc[ (~data_all_cogn['NP1COG2_bs_year'].isnull()) & (data_all_cogn['NP1COG2_bs_year'] < 0), 'NP1COG2_bs_year' ] = 0

    # Minimum days from baseline visit date to cognitive impairment in daily living
    data_cogn_bs_1 = data_all_cogn[ data_all_cogn['NP1COG2'] == 1 ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_1 = data_cogn_bs_1.groupby(['PATNO']).min(['NP1COG2_bs_year']).reset_index()

    # Maximum days from baseline visit date to cognitive impairment in daily living
    data_cogn_bs_0 = data_all_cogn[ (data_all_cogn['NP1COG2'] == 0) & (~data_all_cogn['PATNO'].isin(data_cogn_bs_1['PATNO'])) ][['PATNO', 'NP1COG2_bs_year']].reset_index(drop = True)
    data_cogn_bs_0 = data_cogn_bs_0.groupby(['PATNO']).max(['NP1COG2_bs_year']).reset_index()

    # Combine the data_cogn_0 and data_cogn_1
    data_cogn_bs_comb = pd.concat([data_cogn_bs_0, data_cogn_bs_1], axis = 0)
    data_cogn_bs_comb.sort_values(['PATNO'], inplace = True)
    data_cogn_bs_comb.reset_index(drop = True, inplace = True)
    data_cogn_bs_comb = data_cogn_bs_comb[['PATNO', 'NP1COG2_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_cogn_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild cognitive impairment (Calculate from PD diagnosis date)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 1. MoCA26_status    : Mild cognitive impairment (1 - Yes; 0 - No)
    # 2. MoCA26_year      : Number of years from PD diagnosis to mild cognitive impairment
    # 3. excl_MoCA26_year : Exclude the patient with mild cognitive impairment before or on baseline or no follow-up visit

    data_all_moca = data_all_keep[['PATNO', 'date_visite', 'date_diag', 'MoCA26']]
    data_all_moca = data_all_moca[ ~data_all_moca['MoCA26'].isnull() ]
    data_all_moca = pd.merge(data_all_moca, data_bs_date, how = 'left', on = ['PATNO'])
    
    data_all_moca['MoCA26_year'] = np.where( (~data_all_moca['date_visite'].isnull()) & (~data_all_moca['date_diag'].isnull()),
                                                (data_all_moca['date_visite'] - data_all_moca['date_diag'])/timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_year'].isnull()) & (data_all_moca['MoCA26_year'] < 0), 'MoCA26_year' ] = 0

    # Minimum and maximum year of date_visite and ENROLL_DATE
    # If minimum or maximum year of date_visite and ENROLL_DATE = 0 and NP1COG2 = 1 then excl_NP1COG2_year = 1 (Yes)
    data_diff_year = data_all_moca.copy()
    data_diff_year['diff_year'] = np.where( (~data_all_moca['date_visite'].isnull()) & (~data_all_moca['ENROLL_DATE'].isnull()),
                                                (data_all_moca['date_visite'] - data_all_moca['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    min_diff_year = data_diff_year[ (data_diff_year['diff_year'] <= 0) & (data_diff_year['MoCA26'] == 1) ]
    max_diff_year = data_diff_year[['PATNO', 'diff_year']].groupby(['PATNO']).max(['diff_year']).reset_index()
    max_diff_year = max_diff_year[ max_diff_year['diff_year'] <= 0 ]
    patno_excl_MoCA26_year = np.unique(min_diff_year['PATNO'].tolist() + max_diff_year['PATNO'].tolist())

    # Minimum days from date of onset to cognitive impairment in daily living impact
    data_moca_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ]
    data_moca_1 = data_moca_1.groupby(['PATNO']).min(['MoCA26_year']).reset_index()

    # Maximum days from date of onset to visit date (without cognitive impairment in daily living impact)
    data_moca_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_1['PATNO'])) ]
    data_moca_0 = data_moca_0.groupby(['PATNO']).max(['MoCA26_year']).reset_index()

    # Combine the data_moca_0 and data_moca_1
    data_moca_comb = pd.concat([data_moca_0, data_moca_1], axis = 0)
    data_moca_comb.sort_values(['PATNO'], inplace = True)
    data_moca_comb.reset_index(drop = True, inplace = True)
    data_moca_comb.rename(columns = {'MoCA26': 'MoCA26_status'}, inplace = True)
    data_moca_comb['excl_MoCA26_year'] = np.where(data_moca_comb['PATNO'].isin(patno_excl_MoCA26_year), 1, 0)
    
    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_comb, how = 'left', on = ['PATNO'] )
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Mild Cognitive Impairment (Calculate from date of enrollment)    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # Create new columns
    # 2. MoCA26_bs_year      : Number of years from date of enrollment to mild cognitive impairment

    data_all_moca['MoCA26_bs_year'] = np.where( (~data_all_moca['date_visite'].isnull()) & (~data_all_moca['ENROLL_DATE'].isnull()),
                                                (data_all_moca['date_visite'] - data_all_moca['ENROLL_DATE'])/timedelta(days=365.25), np.nan )
    data_all_moca.loc[ (~data_all_moca['MoCA26_bs_year'].isnull()) & (data_all_moca['MoCA26_bs_year'] < 0), 'MoCA26_bs_year' ] = 0

    # Minimum days from baseline visit date to mild cognitive impairment
    data_moca_bs_1 = data_all_moca[ data_all_moca['MoCA26'] == 1 ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_1 = data_moca_bs_1.groupby(['PATNO']).min(['MoCA26_bs_year']).reset_index()

    # Maximum days from baseline visit date to mild cognitive impairment
    data_moca_bs_0 = data_all_moca[ (data_all_moca['MoCA26'] == 0) & (~data_all_moca['PATNO'].isin(data_moca_bs_1['PATNO'])) ][['PATNO', 'MoCA26_bs_year']].reset_index(drop = True)
    data_moca_bs_0 = data_moca_bs_0.groupby(['PATNO']).max(['MoCA26_bs_year']).reset_index()

    # Combine the data_moca_bs_0 and data_moca_bs_1
    data_moca_bs_comb = pd.concat([data_moca_bs_0, data_moca_bs_1], axis = 0)
    data_moca_bs_comb.sort_values(['PATNO'], inplace = True)
    data_moca_bs_comb.reset_index(drop = True, inplace = True)
    data_moca_bs_comb = data_moca_bs_comb[['PATNO', 'MoCA26_bs_year']]

    # Combined the new variables into `data_comb`
    data_comb = pd.merge( data_comb, data_moca_bs_comb, how = 'left', on = ['PATNO'] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    ####    Return the processed data    ####
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    
    return(data_comb)
