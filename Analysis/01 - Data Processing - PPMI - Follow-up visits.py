#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                         Function - Extract Follow-up Characteristics and outcome                                                         ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def process_ppmi_followup(data
                          ,vars
                          ,outcome
                          ,var_patid
                          ,var_event
                          ,event_out
                          ,varname_outcome
                          ,varname_status
                          ,varname_duration
                          ) :
    """
    @data               : data
    @vars               : baseline variables
    @outcome            : outcome variable
    @var_patid          : variable for patient id
    @var_event          : variable for event_id
    @event_out          : event_id for baseline to follow-up (year-4)
    @varname_outcome    : variable name of the outcome
    @varname_status     : variable name of the outcome status
    @varname_duration   : variable name of the time from baseline to outcome
    """

    # Convert duration of PD from diagnosis to visit_date, from month to year
    data_duration = data[[var_patid, var_event, 'visit_date', 'PDDXDT']]
    data_duration['duration2'] = ( data_duration['visit_date'] - pd.to_datetime('01/' + data_duration['PDDXDT'])) / timedelta(days=365.25)

    data_duration['duration2'] = np.where( (~data_duration['visit_date'].isnull()) & (~data_duration['PDDXDT'].isnull())
                                          ,( data_duration['visit_date'] - pd.to_datetime('01/' + data_duration['PDDXDT'])) / timedelta(days=365.25)
                                          ,np.nan )
    data_duration = data_duration[ ~data_duration['duration2'].isnull() ].reset_index(drop = True)
    data_duration = data_duration[[var_patid, var_event, 'duration2']]

    # Baseline data
    ppmi_bs = preprocess_ppmi(data = data, data_date = data_infodate, vars_info = data_variables_ppmi, event_yr = event_out[0])
    ppmi_bs.process_ppmi()
    ppmi_bs_derived = ppmi_bs.rename_ppmi()
    ppmi_bs_derived.rename(columns = {'visit_date' : 'ENROLL_DATE'}, inplace = True)
    ppmi_bs_derived = pd.merge(ppmi_bs_derived, data_duration, how = 'left', on = [var_patid, var_event])
    ppmi_bs_derived['duration'] = ppmi_bs_derived['duration2']
    ppmi_bs_derived = ppmi_bs_derived[ list(np.unique([var_patid, var_event] + vars + [outcome])) + ['ENROLL_DATE'] ]
    ppmi_bs_derived = ppmi_bs_derived[ ~ppmi_bs_derived[outcome].isnull() ].reset_index(drop = True)

    # Year-1 data
    ppmi_y1 = preprocess_ppmi(data = data, data_date = data_infodate, vars_info = data_variables_ppmi, event_yr = event_out[1])
    ppmi_y1.process_ppmi()
    ppmi_y1_derived = ppmi_y1.rename_ppmi()
    ppmi_y1_derived = ppmi_y1_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    ppmi_y1_derived = ppmi_y1_derived[ ~ppmi_y1_derived[outcome].isnull() ].reset_index(drop = True)
    ppmi_y1_derived.rename(columns = {outcome: 'out_y1', 'visit_date': 'date_y1'}, inplace = True)

    # Year-2 data
    ppmi_y2 = preprocess_ppmi(data = data, data_date = data_infodate, vars_info = data_variables_ppmi, event_yr = event_out[2])
    ppmi_y2.process_ppmi()
    ppmi_y2_derived = ppmi_y2.rename_ppmi()
    ppmi_y2_derived = ppmi_y2_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    ppmi_y2_derived = ppmi_y2_derived[ ~ppmi_y2_derived[outcome].isnull() ].reset_index(drop = True)
    ppmi_y2_derived.rename(columns = {outcome: 'out_y2', 'visit_date': 'date_y2'}, inplace = True)

    # Year-3 data
    ppmi_y3 = preprocess_ppmi(data = data, data_date = data_infodate, vars_info = data_variables_ppmi, event_yr = event_out[3])
    ppmi_y3.process_ppmi()
    ppmi_y3_derived = ppmi_y3.rename_ppmi()
    ppmi_y3_derived = ppmi_y3_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    ppmi_y3_derived = ppmi_y3_derived[ ~ppmi_y3_derived[outcome].isnull() ].reset_index(drop = True)
    ppmi_y3_derived.rename(columns = {outcome: 'out_y3', 'visit_date': 'date_y3'}, inplace = True)

    # Year-4 data
    ppmi_y4 = preprocess_ppmi(data = data, data_date = data_infodate, vars_info = data_variables_ppmi, event_yr = event_out[4])
    ppmi_y4.process_ppmi()
    ppmi_y4_derived = ppmi_y4.rename_ppmi()
    ppmi_y4_derived = ppmi_y4_derived[ [var_patid] + [outcome] + ['visit_date'] ]
    ppmi_y4_derived = ppmi_y4_derived[ ~ppmi_y4_derived[outcome].isnull() ].reset_index(drop = True)
    ppmi_y4_derived.rename(columns = {outcome: 'out_y4', 'visit_date': 'date_y4'}, inplace = True)

    # Outcome from Baseline to year-4 follow-up
    # The follow-up outcome is the first event within 4-year
    ppmi_outcome = pd.merge( ppmi_bs_derived[[var_patid, outcome, 'ENROLL_DATE']], ppmi_y1_derived, how = 'left', on = [var_patid] )
    ppmi_outcome = pd.merge( ppmi_outcome, ppmi_y2_derived, how = 'left', on = [var_patid] )
    ppmi_outcome = pd.merge( ppmi_outcome, ppmi_y3_derived, how = 'left', on = [var_patid] )
    ppmi_outcome = pd.merge( ppmi_outcome, ppmi_y4_derived, how = 'left', on = [var_patid] )
    ppmi_outcome[varname_outcome] = np.where( (ppmi_outcome[outcome] == 1) |
                                                (ppmi_outcome['out_y1'] == 1) |
                                                (ppmi_outcome['out_y2'] == 1) |
                                                (ppmi_outcome['out_y3'] == 1) |
                                                (ppmi_outcome['out_y4'] == 1), 1, ppmi_outcome['out_y4'] )
    
    # @varname_status = 1
    if outcome == 'NP4DYSKI2' :
        data_all = data[ data['COHORT'] == 1 ]
        data_all['NP4DYSKI2'] = np.where( (data_all['NP4DYSKI'] > 0) | (data_all['NP4WDYSK'] > 0) | (data_all['NP4WDYSKDEN'] > 0) | (data_all['FEATDYSKIN'] == 1) | (data_all['DYSKPRES'] == 1), 1
                                     , np.where( (data_all['NP4DYSKI'] == 0) | (data_all['NP4WDYSK'] == 0) | (data_all['NP4WDYSKDEN'] == 0) | (data_all['FEATDYSKIN'] == 0) | (data_all['DYSKPRES'] == 0), 0, np.nan ) )
    
    elif outcome == 'NP4FLCTX2' :
        data_all = data[ data['COHORT'] == 1 ]
        data_all['NP4FLCTX2'] = np.where( (data_all['NP4OFF'] > 0) | data_all['NP4FLCTI'] > 0 | (data_all['NP4FLCTX'] > 0) | (data_all['FEATMTRFLC'] == 1), 1
                                         ,np.where( (data_all['NP4OFF'] == 0) | (data_all['NP4FLCTI'] == 0) | (data_all['NP4FLCTX'] == 0) | (data_all['FEATMTRFLC'] == 0), 0, np.nan ) )

    else :
        print("! ! ! Please update the data manipulation ! ! !")

    data_all_out = pd.merge(data_all[[var_patid, 'visit_date', outcome]], ppmi_bs_derived[[var_patid, 'ENROLL_DATE']], how = 'left', on = [var_patid])
    data_all_out = data_all_out[ (~data_all_out['visit_date'].isnull()) & (data_all_out['visit_date'] >= data_all_out['ENROLL_DATE']) ]

    # outcome == 1
    data_out_1 = data_all_out[ data_all_out[outcome] == 1 ][[var_patid, 'visit_date']]
    data_out1 = data_out_1.groupby(var_patid).min().reset_index()
    data_out1.reset_index(drop = True, inplace = True)
    data_out1[varname_status] = 1

    # outcome == 0
    data_out_0 = data_all_out[ data_all_out[outcome] == 0 ][[var_patid, 'visit_date']]
    data_out0 = data_out_0.groupby(var_patid).max().reset_index()
    data_out0 = data_out0[ ~data_out0[var_patid].isin(data_out1[var_patid]) ]
    data_out0.reset_index(drop = True, inplace = True)
    data_out0[varname_status] = 0

    # combine data_out1 and data_out0
    data_out_comb = pd.concat([data_out1, data_out0], axis = 0).reset_index(drop = True)
    data_out_comb = pd.merge(data_out_comb, ppmi_bs_derived[[var_patid, 'ENROLL_DATE']], how = 'left', on = [var_patid])

    # Duration Outcome (Calculate from date of enrollment)
    data_out_comb[varname_duration] = (data_out_comb['visit_date'] - data_out_comb['ENROLL_DATE'])/timedelta(days=365.25)
    
    # To keep the observation if outcome variables are not missing
    ppmi_outcome = pd.merge(ppmi_outcome, data_out_comb[[var_patid, varname_status, varname_duration]])
    ppmi_outcome = ppmi_outcome[ (~ppmi_outcome[varname_outcome].isnull()) & (~ppmi_outcome[varname_duration].isnull()) ][[var_patid, varname_outcome, varname_status, varname_duration]]
    ppmi_outcome.reset_index(drop = True, inplace = True)

    # Merge the follow-up outcome from ppmi_outcome
    ppmi_comb_derived = pd.merge( ppmi_bs_derived, ppmi_outcome, on = [var_patid], how = 'left' )

    # Keep the data with no missing values
    ppmi_comb_derived['count_miss'] = ppmi_comb_derived.isnull().sum(axis = 1)
    ppmi_comb_derived = ppmi_comb_derived[ ppmi_comb_derived['count_miss'] == 0 ]
    ppmi_comb_derived.drop(['count_miss', 'ENROLL_DATE'], axis = 1, inplace = True)
    ppmi_comb_derived.reset_index(drop = True, inplace = True)

    # Return the processed data
    return( ppmi_comb_derived )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Outcome : NP4DYSKI2_Y4                                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-1 vs Year-5 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y1.shape : (0, 15)
# data_ppmi_y1 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R01', 'R04', 'R06', 'R08', 'R10']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y1.shape : (0, 15)
# data_ppmi_y1 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['V02', 'V05', 'V07', 'V09', 'V11']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y1.shape : (184, 15)
data_ppmi_y1 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V04', 'V06', 'V08', 'V10', 'V12']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-2 vs Year-6 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y2.shape : (0, 15)
# data_ppmi_y2 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R04', 'R06', 'R08', 'R10', 'R12']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y2.shape : (194, 15)
data_ppmi_y2 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V06', 'V08', 'V10', 'V12', 'V13']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-3 vs Year-7 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y3.shape : (0, 15)
# data_ppmi_y3 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R06', 'R08', 'R10', 'R12', 'R13']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y3.shape : (209, 15)
data_ppmi_y3 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V08', 'V10', 'V12', 'V13', 'V14']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-4 vs Year-8 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y4.shape : (0, 15)
# data_ppmi_y4 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R08', 'R10', 'R12', 'R13', 'R14']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y4.shape : (189, 15)
data_ppmi_y4 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V10', 'V12', 'V13', 'V14', 'V15']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-5 vs Year-9 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y5.shape : (0, 15)
# data_ppmi_y5 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R10', 'R12', 'R13', 'R14', 'R15']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y5.shape : (175, 15)
data_ppmi_y5 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V12', 'V13', 'V14', 'V15', 'V16']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-6 vs Year-10 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y6.shape : (0, 15)
# data_ppmi_y6 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R12', 'R13', 'R14', 'R15', 'R16']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y6.shape : (119, 15)
data_ppmi_y6 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V13', 'V14', 'V15', 'V16', 'V17']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-7 vs Year-11 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y7.shape : (0, 15)
# data_ppmi_y7 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R13', 'R14', 'R15', 'R16', 'R17']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y7.shape : (63, 15)
data_ppmi_y7 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V14', 'V15', 'V16', 'V17', 'V18']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-8 vs Year-12 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y8.shape : (0, 15)
# data_ppmi_y8 = process_ppmi_followup( data = data_ppmi
#                                      ,vars = vars_select_dyski_bs_common
#                                      ,outcome = 'NP4DYSKI2'
#                                      ,var_patid = 'PATNO'
#                                      ,var_event = 'EVENT_ID'
#                                      ,event_out = ['R14', 'R15', 'R16', 'R17', 'R18']
#                                      ,varname_outcome = 'NP4DYSKI2_Y4'
#                                      ,varname_status = 'NP4DYSKI2_status'
#                                      ,varname_duration = 'NP4DYSKI2_bs_year' )

# data_ppmi_y8.shape : (22, 15)
data_ppmi_y8 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_dyski_bs_common
                                     ,outcome = 'NP4DYSKI2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V15', 'V16', 'V17', 'V18', 'V19']
                                     ,varname_outcome = 'NP4DYSKI2_Y4'
                                     ,varname_status = 'NP4DYSKI2_status'
                                     ,varname_duration = 'NP4DYSKI2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Update the EVENT_ID    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_ppmi_y1['EVENT_ID'] = 'Y1'
data_ppmi_y2['EVENT_ID'] = 'Y2'
data_ppmi_y3['EVENT_ID'] = 'Y3'
data_ppmi_y4['EVENT_ID'] = 'Y4'
data_ppmi_y5['EVENT_ID'] = 'Y5'
data_ppmi_y6['EVENT_ID'] = 'Y6'
data_ppmi_y7['EVENT_ID'] = 'Y7'
data_ppmi_y8['EVENT_ID'] = 'Y8'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI data at follow-up as baseline    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_followup_ppmi_dyski = pd.concat([data_ppmi_y1
                                      ,data_ppmi_y2
                                      ,data_ppmi_y3
                                      ,data_ppmi_y4
                                      ,data_ppmi_y5
                                      ,data_ppmi_y6
                                      ,data_ppmi_y7
                                      ,data_ppmi_y8], axis = 0)
data_followup_ppmi_dyski['PATNO'] = 'PPMI_' + data_followup_ppmi_dyski['EVENT_ID'] + '_' + data_followup_ppmi_dyski['PATNO'].astype('str')
data_followup_ppmi_dyski.reset_index(drop = True, inplace = True)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                Outcome : NP4FLCTX2_Y4                                                                ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-1 vs Year-5 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y1.shape : (182, 15)
data_ppmi_y1 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V04', 'V06', 'V08', 'V10', 'V12']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-2 vs Year-6 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y2.shape : (173, 15)
data_ppmi_y2 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V06', 'V08', 'V10', 'V12', 'V13']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-3 vs Year-7 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y3.shape : (156, 15)
data_ppmi_y3 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V08', 'V10', 'V12', 'V13', 'V14']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-4 vs Year-8 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y4.shape : (97, 15)
data_ppmi_y4 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V10', 'V12', 'V13', 'V14', 'V15']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-5 vs Year-9 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y5.shape : (89, 15)
data_ppmi_y5 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V12', 'V13', 'V14', 'V15', 'V16']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-6 vs Year-10 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y6.shape : (65, 15)
data_ppmi_y6 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V13', 'V14', 'V15', 'V16', 'V17']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-7 vs Year-11 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y7.shape : (23, 15)
data_ppmi_y7 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V14', 'V15', 'V16', 'V17', 'V18']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-8 vs Year-12 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y8.shape : (3, 15)
data_ppmi_y8 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V15', 'V16', 'V17', 'V18', 'V19']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Year-9 vs Year-13 data    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# data_ppmi_y9.shape : (1, 15)
data_ppmi_y9 = process_ppmi_followup( data = data_ppmi
                                     ,vars = vars_select_flctx_bs_common
                                     ,outcome = 'NP4FLCTX2'
                                     ,var_patid = 'PATNO'
                                     ,var_event = 'EVENT_ID'
                                     ,event_out = ['V16', 'V17', 'V18', 'V19', 'V20']
                                     ,varname_outcome = 'NP4FLCTX2_Y4'
                                     ,varname_status = 'NP4FLCTX2_status'
                                     ,varname_duration = 'NP4FLCTX2_bs_year' )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Update the EVENT_ID    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_ppmi_y1['EVENT_ID'] = 'Y1'
data_ppmi_y2['EVENT_ID'] = 'Y2'
data_ppmi_y3['EVENT_ID'] = 'Y3'
data_ppmi_y4['EVENT_ID'] = 'Y4'
data_ppmi_y5['EVENT_ID'] = 'Y5'
data_ppmi_y6['EVENT_ID'] = 'Y6'
data_ppmi_y7['EVENT_ID'] = 'Y7'
data_ppmi_y8['EVENT_ID'] = 'Y8'
data_ppmi_y9['EVENT_ID'] = 'Y9'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    PPMI data at follow-up as baseline    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

data_followup_ppmi_flctx = pd.concat([data_ppmi_y1
                                      ,data_ppmi_y2
                                      ,data_ppmi_y3
                                      ,data_ppmi_y4
                                      ,data_ppmi_y5
                                      ,data_ppmi_y6
                                      ,data_ppmi_y7
                                      ,data_ppmi_y8
                                      ,data_ppmi_y9 ], axis = 0)
data_followup_ppmi_flctx['PATNO'] = 'PPMI_' + data_followup_ppmi_flctx['EVENT_ID'] + '_' + data_followup_ppmi_flctx['PATNO'].astype('str')
data_followup_ppmi_flctx.reset_index(drop = True, inplace = True)