#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Heatmap of correlation    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ data: data for the graphing
# @ filename: the filename of the html
# @ height: the height of the chart in html
# @ width: the width of the chart in html

def plot_heatmap(data, filename, height = 1000, width = 1000):
  df_corr = data.corr()
  fig = go.Figure()
  fig.add_trace(
    go.Heatmap(
      x = df_corr.columns,
      y = df_corr.index,
      z = np.array(df_corr)
    )
  )
  
  fig.update_layout(autosize = False, height = height, width = width, hovermode = "y unified")
  fig_html = py.plot(fig, filename = filename, auto_open = False)
  
  return(fig_html)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Bar chart with line plot    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ data: data for the graphing
# @ filename: the filename of the html
# @ var_bar_x: variable of x-axis for bar chart
# @ var_bar_y: variable of y-axis for bar chart
# @ name_bar: name of the variable for bar chart
# @ name_line: name of the variable for line chart
# @ height: the height of the chart in html
# @ width: the width of the chart in html

def plot_butterfly( data
                   ,filename
                   ,var_bar_x
                   ,var_bar_y
                   ,title
                   ,name_bar
                   ,name_line
                   ,height = 700
                   ,width = 1000 ):
    
    # Calculate the percentage of var_bar_x
    data_bar_cnt = data.groupby([var_bar_y])[var_bar_x].count().reset_index(name = 'counts')
    data_bar_size = data.groupby([var_bar_y])[var_bar_x].size().reset_index(name = 'total')
    data_bar = data_bar_size.merge(data_bar_cnt, how = 'left', on = [var_bar_y])
    data_bar['pct'] = data_bar['counts'] / data_bar['total'] * 100
    data_bar['pct'] = data_bar['pct'].round(decimals = 2)
    data_bar = data_bar.sort_values([var_bar_y])
    
    
    # Creatomg two subplots
    fig = make_subplots( rows = 1
                        ,cols = 2
                        ,specs = [[{}, {}]]
                        ,shared_xaxes = True
                        ,shared_yaxes = False
                        ,vertical_spacing = 0.001 )
    
    # Bar chart (left)
    fig.append_trace( go.Bar(
         x = data_bar['pct']
        ,y = data_bar[var_bar_y].astype('str')
        ,marker = dict(
             color = 'rgba(50, 171, 96, 0.6)'
            ,line = dict(
                 color = 'rgba(50, 171, 96, 1.0)'
                ,width = 1
            )
        )
        ,name = name_bar
        ,orientation = 'h'
    ), 1, 1)
    
    
    # Line plot (right)
    fig.append_trace( go.Scatter(
         x = data_bar['counts']
        ,y = data_bar[var_bar_y].astype('str')
        ,mode = 'lines+markers'
        ,line_color = 'rgb(128, 0, 128)'
        ,name = name_line
    ), 1, 2 )
    
    
    fig.update_layout(
         title = title
        ,yaxis = dict(
             showgrid = False
            ,showline = False
            ,showticklabels = True
            ,domain = [0, 0.85]
        )
        ,yaxis2 = dict(
             showgrid = False
            ,showline = True
            ,showticklabels = False
            ,linecolor = 'rgba(102, 102, 102, 0.8)'
            ,linewidth = 2
            ,domain = [0, 0.85]
        )
        ,xaxis = dict(
             zeroline = False
            ,showline = False
            ,showticklabels = True
            ,showgrid = True
            ,domain = [0, 0.42]
        )
        ,xaxis2 = dict(
             zeroline = False
            ,showline = False
            ,showticklabels = True
            ,showgrid = True
            ,domain = [0.47, 1]
            ,side = 'top'
            ,dtick = round(max(data_bar['counts'])/100)*10
        )
        ,legend = dict(x = 0.029, y = 1.038, font_size = 10)
        ,margin = dict(l = 100, r = 20, t = 70, b = 70)
        ,paper_bgcolor = 'rgba(248, 248, 255)'
        ,plot_bgcolor = 'rgba(248, 248, 255)'
    )
    
    annotations = []
    
    y_bar = list( data_bar['pct'] )
    y_line = list( data_bar['counts'] )
    x_d = list( data_bar[var_bar_y].astype('str') )
    
    line_gap = 10/max(y_line)*100
    bar_gap = 10/max(y_bar)*100
    
    # Adding labels
    for y_r, y_l, xd in zip(y_line, y_bar, list(range(len(x_d))) ):
        # Labeling the scatter plot
        annotations.append( dict(
             xref = 'x2'
            ,yref = 'y2'
            ,y = xd
            ,x = y_r + line_gap
            ,text = '{:,}'.format(y_r)
            ,font = dict(  family = 'Arial'
                          ,size = 12
                          ,color = 'rgb(128, 0, 128)')
            ,showarrow = False ) )
          
        # Labeling the bar net worth
        annotations.append( dict(
             xref = 'x1'
            ,yref = 'y1'
            ,y = xd
            ,x = y_l + bar_gap
            ,text = str(y_l) + '%'
            ,font = dict(  family = 'Arial'
                          ,size = 12
                          ,color = 'rgb(50, 171, 96)')
            ,showarrow = False ) )
        
    fig.update_layout( annotations = annotations, height = height, width = width )
    
    fig_html = py.plot(fig, filename = filename, auto_open = False)
    
    return(fig_html)





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####    Stacked bar chart with line plot    ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# @ data: data for the graphing
# @ filename: the filename of the html
# @ var_bar_x: variable of x-axis for bar chart
# @ var_bar_y: variable of y-axis for bar chart
# @ lbl_bar_x: label of x-axis for bar chart
# @ name_bar: name of the variable for bar chart
# @ name_line: name of the variable for line chart
# @ height: the height of the chart in html
# @ width: the width of the chart in html

# data = data_cohort_impute
# filename = file_repo + "Bar_with_Line_Depression_by_Year.html"
# var_bar_x = 'gds_cat2'
# var_bar_y = 'YEAR'
# lbl_bar_x = {0: '0-4: No depression'
#             ,1: '5-8: Mild depression'
#             ,2: '9-11: Moderate depression'
#             ,3: '12-15: Severe depression'}
# title = "Depression of Parkinson's patients by year"
# name_bar = 'Percentage of Depression'
# name_line = "Number of Parkinson's patients with depression"

def plot_butterfly_test( data
                   ,filename
                   ,var_bar_x
                   ,var_bar_y
                   ,lbl_bar_x
                   ,title
                   ,name_bar
                   ,name_line
                   ,height = 700
                   ,width = 1000 ):
    
    # Calculate the percentage of var_bar_x
    data_bar_cnt = data.groupby([var_bar_y, var_bar_x])[var_bar_x].count().reset_index(name = 'counts')
    data_bar_size = data.groupby([var_bar_y])[var_bar_x].size().reset_index(name = 'total')
    data_bar = data_bar_size.merge(data_bar_cnt, how = 'left', on = [var_bar_y])
    data_bar['pct'] = data_bar['counts'] / data_bar['total'] * 100
    data_bar['pct'] = data_bar['pct'].round(decimals = 2)
    data_bar = data_bar.sort_values([var_bar_y, var_bar_x])
    
    
    y_data = list(data_bar[var_bar_y].astype('int').sort_values().astype('str').unique())
    x_list = data_bar[[var_bar_y, var_bar_x, 'pct']].pivot( index = var_bar_y
                                                           ,columns = var_bar_x
                                                           ,values = 'pct' )
    # x_list = x_list.replace(0, np.nan)
    x_data = [list(x_list.iloc[i,:]) for i in range(0, x_list.shape[0]) ]
    
    lbl_list = pd.DataFrame( list( lbl_bar_x.items() ), columns = [var_bar_x, 'label'] ).sort_values([var_bar_x])
    top_labels = list(lbl_list['label'])
    
    
    colors = ['rgba(38, 24, 74, 0.8)', 'rgba(71, 58, 131, 0.8)',
          'rgba(122, 120, 168, 0.8)', 'rgba(164, 163, 204, 0.85)',
          'rgba(190, 192, 213, 1)']
          
    
    fig = go.Figure()

    for i in range(0, len(x_data[0])):
        for xd, yd in zip(x_data, y_data):
            fig.add_trace(go.Bar(
                x=[xd[i]], y=[yd],
                orientation='h',
                marker=dict(
                    color=colors[i],
                    line=dict(color='rgb(248, 248, 249)', width=1)
                )
            ))
    
    fig.update_layout(
        xaxis=dict(
            showgrid=False,
            showline=False,
            showticklabels=False,
            zeroline=False,
            domain=[0.15, 1]
        ),
        yaxis=dict(
            showgrid=False,
            showline=False,
            showticklabels=False,
            zeroline=False,
        ),
        barmode='stack',
        paper_bgcolor='rgb(248, 248, 255)',
        plot_bgcolor='rgb(248, 248, 255)',
        margin=dict(l=20, r=10, t=80, b=80),
        showlegend=False,
    )
    
    annotations = []
    
    for yd, xd in zip(y_data, x_data):
        # labeling the y-axis
        annotations.append(dict( xref = 'paper'
                                ,yref = 'paper'
                                ,x = 0.14
                                ,y = 1/len(y_data)*(y_data.index(yd) ) + 0.5/len(y_data),
                                xanchor='right',
                                text=str(yd),
                                font=dict(family='Arial', size=14,
                                          color='rgb(67, 67, 67)'),
                                showarrow=False, align='right'))
    
        # labeling the first percentage of each bar (x_axis)
        # annotations.append(dict( xref='x', yref='paper'
        #                         ,x=xd[0] / 2
        #                         ,y=1/len(y_data)*(y_data.index(yd) ) + 0.5/len(y_data),
        #                         text=str(xd[0]) + '%',
        #                         font=dict(family='Arial', size=14,
        #                                   color='rgb(248, 248, 255)'),
        #                         showarrow=False))
                                
        # labeling the first Likert scale (on the top)
        if yd == y_data[-1]:
            annotations.append(dict(xref='x', yref='paper'
                                    ,x = 0
                                    ,y=1.1,
                                    text=top_labels[0],
                                    font=dict(family='Arial', size=14,
                                              color='rgb(67, 67, 67)'),
                                    showarrow=False))

        space = max(xd)/len(top_labels)
        
        for i in range(1, len(xd)):
                # labeling the rest of percentages for each bar (x_axis)
                # annotations.append(dict(xref='x', yref='paper',
                #                         x=space + (xd[i]/2), y=1/len(y_data)*(y_data.index(yd) ) + 0.5/len(y_data),
                #                         text=str(xd[i]) + '%',
                #                         font=dict(family='Arial', size=14,
                #                                   color='rgb(248, 248, 255)'),
                #                         showarrow=False))
                # labeling the Likert scale
                if yd == y_data[-1]:
                    annotations.append(dict(xref='x', yref='paper',
                                            x=space + i, y=1.1,
                                            text=top_labels[i],
                                            font=dict(family='Arial', size=14,
                                                      color='rgb(67, 67, 67)'),
                                            showarrow=False))
                space += max(xd)/len(top_labels)
    
    fig.update_layout(annotations=annotations)

    fig.show()
        
    fig.update_layout( annotations = annotations, height = height, width = width )
    
    fig_html = py.plot(fig, filename = filename, auto_open = False)
    
    return(fig_html)
    
