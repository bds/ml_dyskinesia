#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                 Outcome: NP4DYSKI2_Y4                                                                 ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP4DYSKI2_Y4() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP4DYSKI2_Y4    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with dyskinesia within 4 years or follow-up record of dyskinesia in year-4
    cnt_outcome_luxpark = data_cohort_luxpark[ ~data_cohort_luxpark['NP4DYSKI2_Y4'].isnull() ].shape[0]
    cnt_outcome_ppmi = data_cohort_ppmi[ ~data_cohort_ppmi['NP4DYSKI2_Y4'].isnull() ].shape[0]
    cnt_outcome_iceberg = data_cohort_iceberg[ ~data_cohort_iceberg['NP4DYSKI2_Y4'].isnull() ].shape[0]

    print('')
    print("Patients with dyskinesia within 4 years or follow-up record of dyskinesia in year-4")
    print("LUXPARK : " + str(cnt_outcome_luxpark) + ' (' + str(cnt_outcome_luxpark/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_outcome_ppmi) + ' (' + str(cnt_outcome_ppmi/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_outcome_iceberg) + ' (' + str(cnt_outcome_iceberg/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = data_cohort_luxpark[ data_cohort_luxpark['NP4DYSKI2_Y4'] == 1 ].shape[0]
    cnt_dyski_ppmi = data_cohort_ppmi[ data_cohort_ppmi['NP4DYSKI2_Y4'] == 1 ].shape[0]
    cnt_dyski_iceberg = data_cohort_iceberg[ data_cohort_iceberg['NP4DYSKI2_Y4'] == 1 ].shape[0]

    print('')
    print("Patients with dyskinesia occurrence within 4 yeas")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/cnt_outcome_luxpark*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/cnt_outcome_ppmi*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/cnt_outcome_iceberg*100) + '%)' )

cohort_tables_NP4DYSKI2_Y4()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                 Outcome: NP4DYSKI2_year                                                                 ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP4DYSKI2_year() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP4DYSKI2_year    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with PD diagnosis date and dyskinesia occurrence after baseline visit
    ds_1 = data_cohort_luxpark[ (~data_cohort_luxpark['NP4DYSKI2_year'].isnull()) & (~data_cohort_luxpark['NP4DYSKI2_status'].isnull()) & (data_cohort_luxpark['excl_NP4DYSKI2_year'] != 1) ]
    ds_2 = data_cohort_ppmi[ (~data_cohort_ppmi['NP4DYSKI2_year'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_status'].isnull()) & (data_cohort_ppmi['excl_NP4DYSKI2_year'] != 1) ]
    ds_3 = data_cohort_iceberg[ (~data_cohort_iceberg['NP4DYSKI2_year'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_status'].isnull()) & (data_cohort_iceberg['excl_NP4DYSKI2_year'] != 1) ]

    print('')
    print("Patients with PD diagnosis date and dyskinesia occurrence after baseline visit")
    print("LUXPARK : " + str(ds_1.shape[0]) + ' (' + str(ds_1.shape[0]/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(ds_2.shape[0]) + ' (' + str(ds_2.shape[0]/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(ds_3.shape[0]) + ' (' + str(ds_3.shape[0]/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = ds_1[ ds_1['NP4DYSKI2_status'] == 1 ].shape[0]
    cnt_dyski_ppmi = ds_2[ ds_2['NP4DYSKI2_status'] == 1 ].shape[0]
    cnt_dyski_iceberg = ds_3[ ds_3['NP4DYSKI2_status'] == 1 ].shape[0]

    print('')
    print("Patients with dyskinesia occurrence (event) in the follow-up visits")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/ds_1.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/ds_2.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/ds_3.shape[0]*100) + '%)' )

cohort_tables_NP4DYSKI2_year()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                 Outcome: NP4DYSKI2_bs_year                                                                 ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP4DYSKI2_bs_year() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP4DYSKI2_bs_year    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with PD diagnosis date and dyskinesia occurrence after baseline visit
    ds_1 = data_cohort_luxpark[ (~data_cohort_luxpark['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_luxpark['NP4DYSKI2_status'].isnull()) & (~data_cohort_luxpark['NP4DYSKI2_Y4'].isnull()) ]
    ds_2 = data_cohort_ppmi[ (~data_cohort_ppmi['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_status'].isnull()) & (~data_cohort_ppmi['NP4DYSKI2_Y4'].isnull()) ]
    ds_3 = data_cohort_iceberg[ (~data_cohort_iceberg['NP4DYSKI2_bs_year'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_status'].isnull()) & (~data_cohort_iceberg['NP4DYSKI2_Y4'].isnull()) ]

    print('')
    print("Patients with dyskinesia within 4 years or follow-up record of dyskinesia in year-4")
    print("LUXPARK : " + str(ds_1.shape[0]) + ' (' + str(ds_1.shape[0]/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(ds_2.shape[0]) + ' (' + str(ds_2.shape[0]/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(ds_3.shape[0]) + ' (' + str(ds_3.shape[0]/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = ds_1[ ds_1['NP4DYSKI2_status'] == 1 ].shape[0]
    cnt_dyski_ppmi = ds_2[ ds_2['NP4DYSKI2_status'] == 1 ].shape[0]
    cnt_dyski_iceberg = ds_3[ ds_3['NP4DYSKI2_status'] == 1 ].shape[0]

    print('')
    print("Patients with dyskinesia occurrence (event) in the follow-up visits")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/ds_1.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/ds_2.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/ds_3.shape[0]*100) + '%)' )

cohort_tables_NP4DYSKI2_bs_year()





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                 Outcome: NP4FLCTX2_Y4                                                                 ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP4FLCTX2_Y4() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP4FLCTX2_Y4    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with motor fluctuations within 4 years or follow-up record of motor fluctuations in year-4
    cnt_outcome_luxpark = data_cohort_luxpark[ ~data_cohort_luxpark['NP4FLCTX2_Y4'].isnull() ].shape[0]
    cnt_outcome_ppmi = data_cohort_ppmi[ ~data_cohort_ppmi['NP4FLCTX2_Y4'].isnull() ].shape[0]
    cnt_outcome_iceberg = data_cohort_iceberg[ ~data_cohort_iceberg['NP4FLCTX2_Y4'].isnull() ].shape[0]

    print('')
    print("Patients with motor fluctuations within 4 years or follow-up record of motor fluctuations in year-4")
    print("LUXPARK : " + str(cnt_outcome_luxpark) + ' (' + str(cnt_outcome_luxpark/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_outcome_ppmi) + ' (' + str(cnt_outcome_ppmi/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_outcome_iceberg) + ' (' + str(cnt_outcome_iceberg/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = data_cohort_luxpark[ data_cohort_luxpark['NP4FLCTX2_Y4'] == 1 ].shape[0]
    cnt_dyski_ppmi = data_cohort_ppmi[ data_cohort_ppmi['NP4FLCTX2_Y4'] == 1 ].shape[0]
    cnt_dyski_iceberg = data_cohort_iceberg[ data_cohort_iceberg['NP4FLCTX2_Y4'] == 1 ].shape[0]

    print('')
    print("Patients with motor fluctuations occurrence within 4 yeas")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/cnt_outcome_luxpark*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/cnt_outcome_ppmi*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/cnt_outcome_iceberg*100) + '%)' )

cohort_tables_NP4FLCTX2_Y4()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                 Outcome: NP4FLCTX2_bs_year                                                                 ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP4FLCTX2_bs_year() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP4FLCTX2_bs_year    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with PD diagnosis date and motor fluctuations occurrence after baseline visit
    ds_1 = data_cohort_luxpark[ (~data_cohort_luxpark['NP4FLCTX2_bs_year'].isnull()) & (~data_cohort_luxpark['NP4FLCTX2_status'].isnull()) & (~data_cohort_luxpark['NP4FLCTX2_Y4'].isnull()) ]
    ds_2 = data_cohort_ppmi[ (~data_cohort_ppmi['NP4FLCTX2_bs_year'].isnull()) & (~data_cohort_ppmi['NP4FLCTX2_status'].isnull()) & (~data_cohort_ppmi['NP4FLCTX2_Y4'].isnull()) ]
    ds_3 = data_cohort_iceberg[ (~data_cohort_iceberg['NP4FLCTX2_bs_year'].isnull()) & (~data_cohort_iceberg['NP4FLCTX2_status'].isnull()) & (~data_cohort_iceberg['NP4FLCTX2_Y4'].isnull()) ]

    print('')
    print("Patients with motor fluctuation within 4 years or follow-up record of motor fluctuations in year-4")
    print("LUXPARK : " + str(ds_1.shape[0]) + ' (' + str(ds_1.shape[0]/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(ds_2.shape[0]) + ' (' + str(ds_2.shape[0]/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(ds_3.shape[0]) + ' (' + str(ds_3.shape[0]/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = ds_1[ ds_1['NP4FLCTX2_status'] == 1 ].shape[0]
    cnt_dyski_ppmi = ds_2[ ds_2['NP4FLCTX2_status'] == 1 ].shape[0]
    cnt_dyski_iceberg = ds_3[ ds_3['NP4FLCTX2_status'] == 1 ].shape[0]

    print('')
    print("Patients with motor fluctuations occurrence (event) in the follow-up visits")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/ds_1.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/ds_2.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/ds_3.shape[0]*100) + '%)' )

cohort_tables_NP4FLCTX2_bs_year()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Outcome: NP1COG2_Y4                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP1COG2_Y4() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP1COG2_Y4    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with cognitive impairment impact in daily living within 4 years or follow-up record of cognitive impairment on MDS-UPDRS in year-4
    cnt_outcome_luxpark = data_cohort_luxpark[ ~data_cohort_luxpark['NP1COG2_Y4'].isnull() ].shape[0]
    cnt_outcome_ppmi = data_cohort_ppmi[ ~data_cohort_ppmi['NP1COG2_Y4'].isnull() ].shape[0]
    cnt_outcome_iceberg = data_cohort_iceberg[ ~data_cohort_iceberg['NP1COG2_Y4'].isnull() ].shape[0]

    print('')
    print("Patients with cognitive impairment impact in daily living within 4 years or follow-up record of cognitive impairment on MDS-UPDRS in year-4")
    print("LUXPARK : " + str(cnt_outcome_luxpark) + ' (' + str(cnt_outcome_luxpark/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_outcome_ppmi) + ' (' + str(cnt_outcome_ppmi/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_outcome_iceberg) + ' (' + str(cnt_outcome_iceberg/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = data_cohort_luxpark[ data_cohort_luxpark['NP1COG2_Y4'] == 1 ].shape[0]
    cnt_dyski_ppmi = data_cohort_ppmi[ data_cohort_ppmi['NP1COG2_Y4'] == 1 ].shape[0]
    cnt_dyski_iceberg = data_cohort_iceberg[ data_cohort_iceberg['NP1COG2_Y4'] == 1 ].shape[0]

    print('')
    print("Patients with cognitive impairment impact in daily living within 4 yeas")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/cnt_outcome_luxpark*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/cnt_outcome_ppmi*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/cnt_outcome_iceberg*100) + '%)' )

cohort_tables_NP1COG2_Y4()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                  Outcome: NP1COG2_bs_year                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_NP1COG2_bs_year() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: NP1COG2_bs_year    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with PD diagnosis date and cognitive impairment impact in daily living occurrence after baseline visit
    ds_1 = data_cohort_luxpark[ (~data_cohort_luxpark['NP1COG2_bs_year'].isnull()) & (~data_cohort_luxpark['NP1COG2_status'].isnull()) & (~data_cohort_luxpark['NP1COG2_Y4'].isnull()) ]
    ds_2 = data_cohort_ppmi[ (~data_cohort_ppmi['NP1COG2_bs_year'].isnull()) & (~data_cohort_ppmi['NP1COG2_status'].isnull()) & (~data_cohort_ppmi['NP1COG2_Y4'].isnull()) ]
    ds_3 = data_cohort_iceberg[ (~data_cohort_iceberg['NP1COG2_bs_year'].isnull()) & (~data_cohort_iceberg['NP1COG2_status'].isnull()) & (~data_cohort_iceberg['NP1COG2_Y4'].isnull()) ]

    print('')
    print("Patients with cognitive impairment impact in daily living within 4 years or follow-up record of cognitive impairment on MDS-UPDRS in year-4")
    print("LUXPARK : " + str(ds_1.shape[0]) + ' (' + str(ds_1.shape[0]/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(ds_2.shape[0]) + ' (' + str(ds_2.shape[0]/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(ds_3.shape[0]) + ' (' + str(ds_3.shape[0]/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_dyski_luxpark = ds_1[ ds_1['NP1COG2_status'] == 1 ].shape[0]
    cnt_dyski_ppmi = ds_2[ ds_2['NP1COG2_status'] == 1 ].shape[0]
    cnt_dyski_iceberg = ds_3[ ds_3['NP1COG2_status'] == 1 ].shape[0]

    print('')
    print("Patients with cognitive impairment impact in daily living (event) in the follow-up visits")
    print("LUXPARK : " + str(cnt_dyski_luxpark) + ' (' + str(cnt_dyski_luxpark/ds_1.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_dyski_ppmi) + ' (' + str(cnt_dyski_ppmi/ds_2.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_dyski_iceberg) + ' (' + str(cnt_dyski_iceberg/ds_3.shape[0]*100) + '%)' )

cohort_tables_NP1COG2_bs_year()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                   Outcome: MoCA26_Y4                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_MoCA26_Y4() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: MoCA26_Y4    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with mild cognitive impairment within 4 years or follow-up record of mild cognitive impairment (MoCA < 26) in year-4
    cnt_outcome_luxpark = data_cohort_luxpark[ ~data_cohort_luxpark['MoCA26_Y4'].isnull() ].shape[0]
    cnt_outcome_ppmi = data_cohort_ppmi[ ~data_cohort_ppmi['MoCA26_Y4'].isnull() ].shape[0]
    cnt_outcome_iceberg = data_cohort_iceberg[ ~data_cohort_iceberg['MoCA26_Y4'].isnull() ].shape[0]

    print('')
    print("Patients with mild cognitive impairment within 4 years or follow-up record of mild cognitive impairment (MoCA < 26) in year-4")
    print("LUXPARK : " + str(cnt_outcome_luxpark) + ' (' + str(cnt_outcome_luxpark/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_outcome_ppmi) + ' (' + str(cnt_outcome_ppmi/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_outcome_iceberg) + ' (' + str(cnt_outcome_iceberg/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_moca_luxpark = data_cohort_luxpark[ data_cohort_luxpark['MoCA26_Y4'] == 1 ].shape[0]
    cnt_moca_ppmi = data_cohort_ppmi[ data_cohort_ppmi['MoCA26_Y4'] == 1 ].shape[0]
    cnt_moca_iceberg = data_cohort_iceberg[ data_cohort_iceberg['MoCA26_Y4'] == 1 ].shape[0]

    print('')
    print("Patients with mild cognitive impairment within 4 yeas")
    print("LUXPARK : " + str(cnt_moca_luxpark) + ' (' + str(cnt_moca_luxpark/cnt_outcome_luxpark*100) + '%)' )
    print("PPMI : " + str(cnt_moca_ppmi) + ' (' + str(cnt_moca_ppmi/cnt_outcome_ppmi*100) + '%)' )
    print("ICEBERG : " + str(cnt_moca_iceberg) + ' (' + str(cnt_moca_iceberg/cnt_outcome_iceberg*100) + '%)' )

cohort_tables_MoCA26_Y4()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
####                                                                   Outcome: MoCA26_bs_year                                                                  ####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def cohort_tables_MoCA26_bs_year() :
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")
    print("####    Outcome: MoCA26_bs_year    ####")
    print("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#")

    # Patients with Parkinson's disease (PD)
    print("Patients with Parkinson's disease (PD)")
    print("LUXPARK : " + str(data_cohort_luxpark.shape[0]) )
    print("PPMI : " + str(data_cohort_ppmi.shape[0]) )
    print("ICEBERG : " + str(data_cohort_iceberg.shape[0]) )

    # Patients with PD diagnosis date and mild cognitive impairment after baseline visit
    ds_1 = data_cohort_luxpark[ (~data_cohort_luxpark['MoCA26_bs_year'].isnull()) & (~data_cohort_luxpark['MoCA26_status'].isnull()) & (~data_cohort_luxpark['MoCA26_Y4'].isnull()) ]
    ds_2 = data_cohort_ppmi[ (~data_cohort_ppmi['MoCA26_bs_year'].isnull()) & (~data_cohort_ppmi['MoCA26_status'].isnull()) & (~data_cohort_ppmi['MoCA26_Y4'].isnull()) ]
    ds_3 = data_cohort_iceberg[ (~data_cohort_iceberg['MoCA26_bs_year'].isnull()) & (~data_cohort_iceberg['MoCA26_status'].isnull()) & (~data_cohort_iceberg['MoCA26_Y4'].isnull()) ]

    print('')
    print("Patients with mild cognitive impairment within 4 years or follow-up record of mild cognitive impairment (MoCA < 26) in year-4")
    print("LUXPARK : " + str(ds_1.shape[0]) + ' (' + str(ds_1.shape[0]/data_cohort_luxpark.shape[0]*100) + '%)' )
    print("PPMI : " + str(ds_2.shape[0]) + ' (' + str(ds_2.shape[0]/data_cohort_ppmi.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(ds_3.shape[0]) + ' (' + str(ds_3.shape[0]/data_cohort_iceberg.shape[0]*100) + '%)' )

    cnt_moca_luxpark = ds_1[ ds_1['MoCA26_status'] == 1 ].shape[0]
    cnt_moca_ppmi = ds_2[ ds_2['MoCA26_status'] == 1 ].shape[0]
    cnt_moca_iceberg = ds_3[ ds_3['MoCA26_status'] == 1 ].shape[0]

    print('')
    print("Patients with mild cognitive impairment (event) in the follow-up visits")
    print("LUXPARK : " + str(cnt_moca_luxpark) + ' (' + str(cnt_moca_luxpark/ds_1.shape[0]*100) + '%)' )
    print("PPMI : " + str(cnt_moca_ppmi) + ' (' + str(cnt_moca_ppmi/ds_2.shape[0]*100) + '%)' )
    print("ICEBERG : " + str(cnt_moca_iceberg) + ' (' + str(cnt_moca_iceberg/ds_3.shape[0]*100) + '%)' )

cohort_tables_MoCA26_bs_year()